let zdata =
    { "panels": [{ "index": 0, "paperType": "A4", "height": 297, "width": 210, "paperHeader": 70.5, "paperFooter": 789, "printElements": [{ "options": { "left": 181.5, "top": 21, "height": 9.75, "width": 244.5, "title": "绍兴电力设备有限公司", "fontSize": 20.25, "fontWeight": "bolder", "textAlign": "center", "textContentVerticalAlign": "middle", "borderLeft": "solid", "borderColor": "#ffffff" }, "printElementType": { "type": "text" } }, { "options": { "left": 183, "top": 49.5, "height": 9.75, "width": 241.5, "title": "加工明细表", "fontSize": 18.75, "fontWeight": "bolder", "textAlign": "center", "textContentVerticalAlign": "middle" }, "printElementType": { "type": "text" } }, { "options": { "left": 102, "top": 90, "height": 9.75, "width": 400, "field": "PT_Num", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 28.5, "top": 90, "height": 9.75, "width": 70.5, "title": "下达单号：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 102, "top": 114, "height": 9.75, "width": 400, "field": "customer_name", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 28.5, "top": 114, "height": 9.75, "width": 70.5, "title": "客户名称：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 102, "top": 138, "height": 9.75, "width": 400, "field": "C_ProjectName", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 28.5, "top": 138, "height": 9.75, "width": 70.5, "title": "工程全称：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 102, "top": 160.5, "height": 9.75, "width": 400, "field": "TD_TypeName", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 28.5, "top": 160.5, "height": 9.75, "width": 70.5, "title": "塔型名称：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 102, "top": 193.5, "height": 9.75, "width": 400, "field": "dzm", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 28.5, "top": 193.5, "height": 9.75, "width": 70.5, "title": "钢印号：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 102, "top": 210, "height": 9.75, "width": 400, "field": "PT_DHmemo", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 28.5, "top": 210, "height": 9.75, "width": 70.5, "title": "套用塔型：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 15, "top": 226.5, "height": 51, "width": 544.5, "field": "tb", "fontSize": 12, "tableBodyRowHeight": 28, "columns": [[{ "title": "<div style='text-align: center;border:none;'><a href='#p_fm2' style='text-decoration:underline;color:red;margin-right:20px; '>呼高段数汇总见下页</a>呼高段数汇总</div>", "field": "hz", "width": 544.5, "align": "left", "colspan": 1, "rowspan": 1, "checked": true, "columnId": "hz" }]] }, "printElementType": { "title": "表格", "type": "tableCustom" } }, { "options": { "left": 262.5, "top": 799.5, "height": 9.75, "width": 60, "field": "Auditor", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 461.5, "top": 799.5, "height": 9.75, "width": 90, "field": "idate", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 411, "top": 799.5, "height": 9.75, "width": 40.5, "title": "日期：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 192, "top": 801, "height": 9.75, "width": 40.5, "title": "审核：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 79.5, "top": 801, "height": 9.75, "width": 60, "field": "Writer", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 18, "top": 801, "height": 9.75, "width": 40.5, "title": "编制：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 28.5, "top": 177, "height": 9.75, "width": 70.5, "title": "材料标准：", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }, { "options": { "left": 102, "top": 177, "height": 9.75, "width": 399, "field": "DtMD_sType", "fontSize": 12, "fontWeight": "bold" }, "printElementType": { "type": "text" } }], "paperNumberLeft": 565, "paperNumberTop": 819, "leftOffset": 15 }] }

let zdata2 = {
    "panels": [{
        "index": 0,
        "paperType": "A4",
        "height": 297,
        "width": 210,
        "paperHeader": 70.5,
        "paperFooter": 796.5,
        "printElements": [{
            "options": {
                "left": 490.5,
                "top": 21,
                "height": 9.75,
                "width": 85.5,
                "field": "PT_Num",
                "fontSize": 12,
                "fontWeight": "bold"
            }, "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 89.5,
                "top": 19.5,
                "height": 9.75,
                "width": 422.5,
                "field": "customer_name",
                "fontSize": 20.25,
                "fontWeight": "bold",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 102,
                "top": 42,
                "height": 9.75,
                "width": 400,
                "field": "C_ProjectName",
                "fontSize": 17.25,
                "fontWeight": "bold",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 19.5,
                "top": 81,
                "height": 48,
                "width": 544.5,
                "field": "tb",
                "columns": [[{
                    "title": "呼高段数汇总",
                    "field": "hz",
                    "width": 544.5,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "hz"
                }]]
            }, "printElementType": { "title": "表格", "type": "tableCustom" }
        }],
        "leftOffset": 15,
        "paperNumberLeft": 500,
        "paperNumberTop": 819
    }]
};

let mx1data = {
    "panels": [{
        "index": 0,
        "paperType": "A4",
        "height": 297,
        "width": 210,
        "paperHeader": 117,
        //"paperFooter": 811.5,
        "paperFooter": 760,
        "printElements": [{
            "options": {
                "left": 184.5,
                "top": 18,
                "height": 9.75,
                "width": 231,
                "title": "绍兴电力设备有限公司",
                "fontSize": 20.25,
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 211.5,
                "top": 48,
                "height": 9.75,
                "width": 175.5,
                "title": "原材料汇总清单",
                "fontSize": 21.75,
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 463.5,
                "top": 49.5,
                "height": 9.75,
                "width": 87,
                "title": "SDS 1009-02",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 16.5, "top": 100, "height": 9.75, "width": 39, "title": "塔型：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 66,
                "top": 100,
                "height": 9.75,
                "width": 120,
                "field": "TD_TypeName",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 220, "top": 100, "height": 9.75, "width": 37.5, "title": "钢印号：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 270,
                "top": 100,
                "height": 9.75,
                "width": 120,
                "field": "gyh",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 400, "top": 100, "height": 9.75, "width": 47.5, "title": "材料标准：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 450,
                "top": 100,
                "height": 9.75,
                "width": 120,
                "field": "DtMD_sType",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 400, "top": 72, "height": 9.75, "width": 47.5, "title": "套用塔型：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 450,
                "top": 72,
                "height": 9.75,
                "width": 120,
                "field": "PT_DHmemo",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 16.5, "top": 72, "height": 9.75, "width": 37.5, "title": "工程：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 66,
                "top": 72,
                "height": 9.75,
                "width": 420,
                "field": "C_ProjectName",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 20,
                "top": 129,
                "height": 43.5,
                "width": 550,
                "field": "tb",
                "fontSize": 11,
                "tableBodyRowBorder": "noBorder",
                "tableBodyCellBorder": "noBorder",
                "columns": [[{
                    "title": "规格",
                    "field": "gg",
                    "width": 60,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "gg",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.2rem'>" + (value || '') + "</div>"
                        }
                    }
                }, {
                    "title": "材质",
                    "field": "cz",
                    "width": 42.954290557496265,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "cz",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }

                    }
                }, {
                    "title": "重量",
                    "field": "zl",
                    "width": 70,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zl",
                    "formatter": function (value, row) {

                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                }, {
                    "title": "螺栓件重量",
                    "field": "lszl",
                    "width": 70,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "lszl",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                }, {
                    "title": "电焊件重量",
                    "field": "dhzl",
                    "width": 70,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "dhzl",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                }, {
                    "title": "总计孔数",
                    "field": "zjks",
                    "width": 50,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zjks",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                }, {
                    "title": "制弯", //制弯
                    "field": "hq",
                    "width": 50,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "hq",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                }, {
                    "title": "切角",
                    "field": "qj",
                    "width": 60,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "qj",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                }, {
                    "title": "铲背",
                    "field": "cb",
                    "width": 50,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "cb",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                }, {
                    "title": "清根",
                    "field": "qg",
                    "width": 50,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "qg",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                },
                {
                    "title": "打扁",
                    "field": "yb",
                    "width": 50,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "yb",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                },
                {
                    "title": "开合角",
                    "field": "khj",
                    "width": 50,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "khj",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                },


                /**/
                {
                    "title": "割豁",
                    "field": "gh",
                    "width": 50,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "gh",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='font-size: 1.3rem'>" + (value || '') + "</div>"
                        }
                    }
                },

                {
                    "title": "总计数量",
                    "field": "zjzl",
                    "width": 50,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zjzl",
                    "formatter": function (value, row) {
                        if (row['gg'] == '合计' || row['gg'] == '总记录') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + (value || '') + "</div>";
                        } else {
                            return "<div style='border-right: none;font-size: 1.3rem'>" + (value || '') + "</div>";
                        }

                    }
                }]]
            }, "printElementType": { "title": "表格", "type": "tableCustom" }
        }
        ],
        "leftOffset": 10,
        "paperNumberLeft": 500,
        "paperNumberTop": 819,
        'paperNumberFormat': '第 paperNo 页，共 paperCount 页'
    }]
};

let mx2data = {
    "panels": [{
        "index": 0,
        "paperType": "A4",
        "height": 297,
        "width": 210,
        "paperHeader": 120,
        "paperFooter": 760,
        "printElements": [{
            "options": {
                "left": 184.5,
                "top": 18,
                "height": 9.75,
                "width": 231,
                "title": "绍兴电力设备有限公司",
                "fontSize": 20.25,
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 207,
                "top": 42,
                "height": 9.75,
                "width": 175.5,
                "title": "加工明细表1",
                "fontSize": 21.75,
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": { "left": 195, "top": 58.5, "height": 9, "width": 214.5 },
            "printElementType": { "type": "hline" }
        }, {
            "options": {
                "left": 243,
                "top": 64.5,
                "height": 9.75,
                "width": 120,
                "field": "idate",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": { "left": 16.5, "top": 100, "height": 9.75, "width": 39, "title": "塔型：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 66,
                "top": 100,
                "height": 9.75,
                "width": 120,
                "field": "TD_TypeName",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 220, "top": 100, "height": 9.75, "width": 37.5, "title": "钢印号：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 270,
                "top": 100,
                "height": 9.75,
                "width": 120,
                "field": "dzm",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 400, "top": 100, "height": 9.75, "width": 47.5, "title": "材料标准：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 450,
                "top": 100,
                "height": 9.75,
                "width": 120,
                "field": "DtMD_sType",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 400, "top": 82, "height": 9.75, "width": 47.5, "title": "套用塔型：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 450,
                "top": 82,
                "height": 9.75,
                "width": 120,
                "field": "PT_DHmemo",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 16.5, "top": 82, "height": 9.75, "width": 37.5, "title": "工程：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 66,
                "top": 82,
                "height": 9.75,
                "width": 420,
                "field": "C_ProjectName",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 20,
                "top": 129,
                "height": 43.5,
                "width": 550,
                "field": "tb",
                "tableBodyRowBorder": "noBorder",
                "tableBodyCellBorder": "noBorder",
                "columns": [[{
                    "title": "序号",
                    "field": "xh",
                    "width": 25,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "xh",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "部件编号",
                    "field": "bjbh",
                    "width": 55,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "bjbh",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        // return "<div style='font-size: 1.3rem'>" + value + "</div>"
                        return "<div >" + value + "</div>"
                    }
                }, {
                    "title": "材质",
                    "field": "cz",
                    "width": 25,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "cz",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        // return "<div style='font-size: 1.3rem'>" + value + "</div>"
                        return "<div >" + value + "</div>"
                    }
                }, {
                    "title": "规格",
                    "field": "gg",
                    "width": 40,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "gg",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + value + "</div>";
                        }
                        // return "<div style='font-size: 1.3rem'>" + value + "</div>"
                        return "<div >" + value + "</div>"
                    }
                }, {
                    "title": "长度",
                    "field": "cd",
                    "width": 35,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "cd",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + value + " 件</div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "宽度",
                    "field": "kd",
                    "width": 25,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "kd",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;white-space:nowrap;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size: 1.3rem'>" + value + " kg</div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "数量",
                    "field": "sl",
                    "width": 35,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "sl",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "配料方式",
                    "field": "pl",
                    "width": 90,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "pl",
                    // "formatter":function (value,row) {
                    //     // console.log(row)
                    //     if(row['hb']==0){
                    //         return "<div style='height:100%;background-color: #fff;position:relative;top:2px;'></div>";
                    //     }else{
                    //         return "111";
                    //     }
                    // },
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    //     if (row['hb'] == 0) {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "top": "5px",
                    //             // "border-bottom":"1px solid red"
                    //         };
                    //     }
                    //     ;
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        if (row['hb'] == 0) {
                            return "<div style='border-bottom: none;font-size: 1.3rem'></div>";
                        }
                        // return "<div style='font-size: 1.3rem'>" + value + "</div>"
                        return "<div >" + value + "</div>"
                    }
                }, {
                    "title": "备注",
                    "field": "bz",
                    "width": 75,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "bz",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem;text-align: left'></div>";
                        }
                        return "<div style='font-size: 1.3rem;text-align: left'>" + value + "</div>"
                    }
                },
                //
                // {
                //     "title": "电焊",
                //     "field": "hq",
                //     "width": 5,
                //     "align": "center",
                //     "colspan": 1,
                //     "rowspan": 1,
                //     "checked": true,
                //     "columnId": "hq",
                //     "formatter": function (value, row) {
                //         if (row['gg'] == '小计' || row['gg'] == '合计') {
                //             return "<div style='border-right: none'></div>";
                //         }
                //         return "<div>" + 1 + "</div>";
                //     }
                // },
                // {
                //     "title": "制弯",
                //     "field": "hq",
                //     "width": 5,
                //     "align": "center",
                //     "colspan": 1,
                //     "rowspan": 1,
                //     "checked": true,
                //     "columnId": "hq",
                //     "formatter": function (value, row) {
                //         if (row['gg'] == '小计' || row['gg'] == '合计') {
                //             return "<div style='border-right: none'></div>";
                //         }
                //         return "<div>" + 1 + "</div>";
                //     }
                // },
                // {
                //     "title": "切角切肢",
                //     "field": "hq",
                //     "width": 5,
                //     "align": "center",
                //     "colspan": 1,
                //     "rowspan": 1,
                //     "checked": true,
                //     "columnId": "hq",
                //     "formatter": function (value, row) {
                //         if (row['gg'] == '小计' || row['gg'] == '合计') {
                //             return "<div style='border-right: none'></div>";
                //         }
                //         return "<div>" + 1 + "</div>";
                //     }
                // },
                // {
                //     "title": "铲背",
                //     "field": "hq",
                //     "width": 5,
                //     "align": "center",
                //     "colspan": 1,
                //     "rowspan": 1,
                //     "checked": true,
                //     "columnId": "hq",
                //     "formatter": function (value, row) {
                //         if (row['gg'] == '小计' || row['gg'] == '合计') {
                //             return "<div style='border-right: none'></div>";
                //         }
                //         return "<div>" + 1 + "</div>";
                //     }
                // },{
                //     "title": "清根",
                //     "field": "hq",
                //     "width": 5,
                //     "align": "center",
                //     "colspan": 1,
                //     "rowspan": 1,
                //     "checked": true,
                //     "columnId": "hq",
                //     "formatter": function (value, row) {
                //         if (row['gg'] == '小计' || row['gg'] == '合计') {
                //             return "<div style='border-right: none'></div>";
                //         }
                //         return "<div>" + 1 + "</div>";
                //     }
                // },
                // {
                //     "title": "打扁",
                //     "field": "hq",
                //     "width": 5,
                //     "align": "center",
                //     "colspan": 1,
                //     "rowspan": 1,
                //     "checked": true,
                //     "columnId": "hq",
                //     "formatter": function (value, row) {
                //         if (row['gg'] == '小计' || row['gg'] == '合计') {
                //             return "<div style='border-right: none'></div>";
                //         }
                //         return "<div>" + 1 + "</div>";
                //     }
                // },
                // {
                //     "title": "开合角",
                //     "field": "hq",
                //     "width": 5,
                //     "align": "center",
                //     "colspan": 1,
                //     "rowspan": 1,
                //     "checked": true,
                //     "columnId": "hq",
                //     "formatter": function (value, row) {
                //         if (row['gg'] == '小计' || row['gg'] == '合计') {
                //             return "<div style='border-right: none'></div>";
                //         }
                //         return "<div>" + 1 + "</div>";
                //     }
                // },
                // {
                //     "title": "钻孔",
                //     "field": "hq",
                //     "width": 5,
                //     "align": "center",
                //     "colspan": 1,
                //     "rowspan": 1,
                //     "checked": true,
                //     "columnId": "hq",
                //     "formatter": function (value, row) {
                //         if (row['gg'] == '小计' || row['gg'] == '合计') {
                //             return "<div style='border-right: none'></div>";
                //         }
                //         return "<div>" + 1 + "</div>";
                //     }
                // },
                //     {
                //     "title": "工艺",
                //     "field": "gy",
                //     "width": 55,
                //     "align": "center",
                //     "colspan": 1,
                //     "rowspan": 1,
                //     "checked": true,
                //     "columnId": "gy",
                //     // "styler": function (value, row) {
                //     //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                //     //         return {
                //     //             "background-color": "#fff",
                //     //             "position": "relative",
                //     //             "left": "5px"
                //     //         };
                //     //     }
                //     // }
                //     "formatter": function (value, row) {
                //         if (row['gg'] == '小计' || row['gg'] == '合计') {
                //             return "<div style='border-right: none'></div>";
                //         }
                //         return "<div>" + value + "</div>";
                //     }
                // },
                {
                    "title": "重量",
                    "field": "zl",
                    "width": 45,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zl",
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        return "<div style='border-right: none;font-size: 1.3rem'>" + value + "</div>";
                    }
                }]]
            }, "printElementType": { "title": "表格", "type": "tableCustom" }
        },

        {
            "options": { "left": 16, "top": 783, "height": 9.75, "width": 40, "title": "制表：" },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 66, "top": 783, "height": 9.75, "width": 50, "field": "zbr" },
            "printElementType": { "type": "text" }
        },

        {
            "options": { "left": 136, "top": 783, "height": 9.75, "width": 40, "title": "审核：" },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 176, "top": 783, "height": 9.75, "width": 50, "field": "shr" },
            "printElementType": { "type": "text" }
        },

        {
            "options": { "left": 256, "top": 783, "height": 9.75, "width": 40, "title": "经办人：" },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 296, "top": 783, "height": 9.75, "width": 50, "field": "jbr：" },
            "printElementType": { "type": "text" }
        },

        {
            "options": { "left": 376, "top": 783, "height": 9.75, "width": 40, "title": "业务员：" },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 416, "top": 783, "height": 9.75, "width": 50, "field": "ywy：" },
            "printElementType": { "type": "text" }
        },

        {
            "options": {
                "left": 39,
                "top": 193.5,
                "height": 9.75,
                "width": 508.5,
                "field": "hj",
                "fontSize": 10.5,
                "fontWeight": "bold",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            },
            "printElementType": {
                "type": "text"
            }
        }],
        "leftOffset": 10,
        "paperNumberLeft": 500,
        "paperNumberTop": 819,
        'paperNumberFormat': '第 paperNo 页，共 paperCount 页'
    }]
};

let mx3data = {
    "panels": [{
        "index": 0,
        "paperType": "A4",
        "height": 297,
        "width": 210,
        "paperHeader": 120,
        "paperFooter": 760,
        "printElements": [{
            "options": {
                "left": 184.5,
                "top": 18,
                "height": 9.75,
                "width": 231,
                "title": "绍兴电力设备有限公司",
                "fontSize": 20.25,
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 207,
                "top": 42,
                "height": 9.75,
                "width": 175.5,
                "title": "加工明细表2",
                "fontSize": 21.75,
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": { "left": 195, "top": 58.5, "height": 9, "width": 214.5 },
            "printElementType": { "type": "hline" }
        }, {
            "options": {
                "left": 243,
                "top": 64.5,
                "height": 9.75,
                "width": 120,
                "field": "idate",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": { "type": "text" }
        }, {
            "options": { "left": 16.5, "top": 100, "height": 9.75, "width": 39, "title": "塔型：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 66,
                "top": 100,
                "height": 9.75,
                "width": 120,
                "field": "TD_TypeName",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 220, "top": 100, "height": 9.75, "width": 37.5, "title": "钢印号：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 270,
                "top": 100,
                "height": 9.75,
                "width": 120,
                "field": "dzm",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 400, "top": 100, "height": 9.75, "width": 47.5, "title": "材料标准：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 450,
                "top": 100,
                "height": 9.75,
                "width": 120,
                "field": "DtMD_sType",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 400, "top": 82, "height": 9.75, "width": 47.5, "title": "套用塔型：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 450,
                "top": 82,
                "height": 9.75,
                "width": 120,
                "field": "PT_DHmemo",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 16.5, "top": 82, "height": 9.75, "width": 37.5, "title": "工程：" },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 66,
                "top": 82,
                "height": 9.75,
                "width": 420,
                "field": "C_ProjectName",
                "fontSize": 12,
                "fontWeight": "bold"
            },
            "printElementType": { "type": "text" }
        }, {
            "options": {
                "left": 20,
                "top": 129,
                "height": 43.5,
                "width": 550,
                "field": "tb",
                "tableBodyRowBorder": "noBorder",
                "tableBodyCellBorder": "noBorder",
                "columns": [[{
                    "title": "序号",
                    "field": "xh",
                    "width": 25,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "xh",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "部件编号",
                    "field": "bjbh",
                    "width": 40,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "bjbh",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        // return "<div style='font-size: 1.3rem'>" + value + "</div>"
                        return "<div >" + value + "</div>"
                    }
                }, {
                    "title": "材质",
                    "field": "cz",
                    "width": 25,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "cz",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        // return "<div style='font-size: 1.3rem'>" + value + "</div>"
                        return "<div >" + value + "</div>"
                    }
                }, {
                    "title": "规格",
                    "field": "gg",
                    "width": 40,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "gg",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;font-weight: bold;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'>" + value + "</div>";
                        }
                        // return "<div style='font-size: 1.2rem'>" + value + "</div>"
                        return "<div >" + value + "</div>"
                    }
                }, {
                    "title": "长度",
                    "field": "cd",
                    "width": 30,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "cd",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;font-weight: bold;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'>" + value + " 件</div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "宽度",
                    "field": "kd",
                    "width": 30,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "kd",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;white-space:nowrap;font-weight: bold;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'>" + value + " kg</div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "数量",
                    "field": "sl",
                    "width": 25,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "sl",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "重量",
                    "field": "zl",
                    "width": 40,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zl",
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "总孔数",
                    "field": "zjks",
                    "width": 35,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zjks",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        return "<div style='font-size: 1.3rem'>" + value + "</div>"
                    }
                }, {
                    "title": "工艺",
                    "field": "gy",
                    "width": 80,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "gy",
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        // return "<div style='font-size: 1.3rem'>" + value + "</div>"
                        return "<div >" + value + "</div>"
                    }
                }, {
                    "title": "备注",
                    "field": "bz",
                    "width": 75,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "bz",
                    // "styler": function (value, row) {
                    //     if (row['gg'] == '小计' || row['gg'] == '合计') {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "left": "5px"
                    //         };
                    //     }
                    // }
                    "formatter": function (value, row) {
                        if (row['gg'] == '小计' || row['gg'] == '合计') {
                            return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-size: 1.3rem'></div>";
                        }
                        return "<div style='border-right: none;font-size: 1.3rem'>" + value + "</div>";
                    }
                }

                ]]
            }, "printElementType": { "title": "表格", "type": "tableCustom" }
        },

        {
            "options": { "left": 16, "top": 783, "height": 9.75, "width": 40, "title": "制表：" },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 66, "top": 783, "height": 9.75, "width": 50, "field": "zbr" },
            "printElementType": { "type": "text" }
        },

        {
            "options": { "left": 136, "top": 783, "height": 9.75, "width": 40, "title": "审核：" },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 176, "top": 783, "height": 9.75, "width": 50, "field": "shr" },
            "printElementType": { "type": "text" }
        },

        {
            "options": { "left": 256, "top": 783, "height": 9.75, "width": 40, "title": "经办人：" },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 296, "top": 783, "height": 9.75, "width": 50, "field": "jbr：" },
            "printElementType": { "type": "text" }
        },

        {
            "options": { "left": 376, "top": 783, "height": 9.75, "width": 40, "title": "业务员：" },
            "printElementType": { "type": "text" }
        }, {
            "options": { "left": 416, "top": 783, "height": 9.75, "width": 50, "field": "ywy：" },
            "printElementType": { "type": "text" }
        },
        {
            "options": {
                "left": 39,
                "top": 193.5,
                "height": 9.75,
                "width": 508.5,
                "field": "hj",
                "fontSize": 10.5,
                "fontWeight": "bold",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            },
            "printElementType": {
                "type": "text"
            }
        }],
        "leftOffset": 10,
        "paperNumberLeft": 500,
        "paperNumberTop": 819,
        'paperNumberFormat': '第 paperNo 页，共 paperCount 页'
    }]
};

let mx4data = {
    "panels": [{
        "index": 0,
        "paperType": "A4",
        "height": 297,
        "width": 210,
        "paperHeader": 105,
        "paperFooter": 760.5,
        "printElements": [{
            "options": {
                "left": 0,
                "top": 43.5,
                "height": 22.5,
                "width": 594,
                "title": "放样原始材料表",
                "fontSize": 18,
                "textDecoration": "underline",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 234,
                "top": 87,
                "height": 12,
                "width": 63,
                "field": "dw",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 73.5,
                "top": 87,
                "height": 12,
                "width": 118.5,
                "field": "tx",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 34.5,
                "top": 87,
                "height": 12,
                "width": 37.5,
                "title": "塔型：",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 453,
                "top": 87,
                "height": 12,
                "width": 36,
                "title": "日期：",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 489,
                "top": 87,
                "height": 12,
                "width": 76.5,
                "field": "rq",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 15,
                "top": 105,
                "height": 277.5,
                "width": 564,
                "field": "tb",
                "textAlign": "center",
                "tableFooterRepeat": "page",
                "fontSize": 9.75,
                "tableHeaderRowHeight": 24,
                "tableBodyRowHeight": 24,
                "columns": [
                    [{
                        "title": "零件编号",
                        "field": "ljbh",
                        "width": 43.510088149316935,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "ljbh"
                    }, {
                        "title": "材质",
                        "field": "cz",
                        "width": 34.68039290238309,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "cz"
                    }, {
                        "title": "规格",
                        "field": "gg",
                        "width": 58.86369381918181,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "gg"
                    }, {
                        "title": "长度(mm)",
                        "field": "cd",
                        "width": 29.78511931086724,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "cd"
                    }, {
                        "title": "宽度(mm)",
                        "field": "kd",
                        "width": 29.943758311424666,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "kd"
                    }, {
                        "title": "单段件数",
                        "field": "djjs",
                        "width": 26.783602146801986,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "djjs"
                    }, {
                        "title": "总数量",
                        "field": "zsl",
                        "width": 26.783602146801986,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "zsl"
                    }, {
                        "title": "单基重量",
                        "field": "djzl",
                        "width": 40.479127855480286,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "djzl"
                    }, {
                        "title": "孔数",
                        "field": "ks",
                        "width": 31.27677287287164,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "ks"
                    }, {
                        "title": "电焊",
                        "field": "dh",
                        "width": 15.110042888162253,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "dh"
                    }, {
                        "title": "制弯",
                        "field": "wq",
                        "width": 15.155631540807713,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "wq"
                    }, {
                        "title": "切角",
                        "field": "qj",
                        "width": 15.10448713729342,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "qj"
                    }, {
                        "title": "铲背",
                        "field": "cb",
                        "width": 15.112842833157504,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "cb"
                    }, {
                        "title": "清根",
                        "field": "qg",
                        "width": 15.151340730108354,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "qg"
                    }, {
                        "title": "打扁",
                        "field": "db",
                        "width": 15.192322362346387,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "db"
                    }, {
                        "title": "开合角",
                        "field": "khj",
                        "width": 22.696963762249545,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "khj"
                    }, {
                        "title": "割豁",
                        "field": "DtMD_GeHuo",
                        "width": 15.19586406317535,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "DtMD_GeHuo"
                    }, {
                        "title": "钻孔",
                        "field": "zk",
                        "width": 15.19586406317535,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "zk"
                    }, {
                        "title": "备注",
                        "field": "bz",
                        "width": 97.98494931437186,
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "bz"
                    }]
                ]
            },
            "printElementType": {
                "title": "表格",
                "type": "tableCustom"
            }
        }, {
            "options": {
                "left": 15,
                "top": 765,
                "height": 9,
                "width": 565.5
            },
            "printElementType": {
                "type": "hline"
            }
        }, {
            "options": {
                "left": 238.5,
                "top": 774,
                "height": 9.75,
                "width": 34.5,
                "title": "审核：",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 273,
                "top": 774,
                "height": 9.75,
                "width": 90,
                "field": "sh",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 70.5,
                "top": 774,
                "height": 9.75,
                "width": 97.5,
                "field": "zb",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 442.5,
                "top": 774,
                "height": 9.75,
                "width": 120,
                "field": "page",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }, {
            "options": {
                "left": 37.5,
                "top": 774,
                "height": 9.75,
                "width": 33,
                "title": "制表：",
                "fontSize": 10.5
            },
            "printElementType": {
                "type": "text"
            }
        }],
        "paperNumberLeft": 486,
        "paperNumberTop": 772.5,
        "paperNumberDisabled": false,
        "paperNumberFormat": "第 paperNo 页，共 paperCount 页"
    }]
};

let mxdatas = {
    zdata,
    zdata2,
    mx1data,
    mx2data,
    mx3data,
    mx4data
}