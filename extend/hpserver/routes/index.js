var express = require('express');
var router = express.Router();
const hpesec = require('../js/hpesec')
const resjson = function () {
  return {
    'err': 1,
    'msg': '',
    'data': ''
  }
};

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/hpesec', async function (req, res, next) {
  let par = req.body;
  let r = await hpesec.hpesec(par);
  res.send(r);
});

module.exports = router;
