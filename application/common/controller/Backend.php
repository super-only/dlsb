<?php

namespace app\common\controller;

use app\admin\library\Auth;
use app\admin\model\chain\fastening\BoltStoreStock;
use app\admin\model\chain\lofting\DhCooperateDetail;
use app\admin\model\chain\lofting\DhCooperatekDetail;
use app\admin\model\chain\material\StoreState;
use app\admin\model\jichu\ch\InventoryMaterial;
use app\admin\model\jichu\ch\Measurement;
use think\Config;
use think\Controller;
use think\Hook;
use think\Lang;
use think\Loader;
use think\Model;
use think\Session;
use fast\Tree;
use think\Validate;

/**
 * 后台控制器基类
 */
class Backend extends Controller
{
    const WG_URL = 'http://192.168.5.200:6789/proxy';
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = [];

    /**
     * 无需鉴权的方法,但需要登录
     * @var array
     */
    protected $noNeedRight = [];

    /**
     * 布局模板
     * @var string
     */
    protected $layout = 'default';

    /**
     * 权限控制类
     * @var Auth
     */
    protected $auth = null;

    /**
     * 模型对象
     * @var \think\Model
     */
    protected $model = null;

    /**
     * 快速搜索时执行查找的字段
     */
    protected $searchFields = 'id';

    /**
     * 是否是关联查询
     */
    protected $relationSearch = false;

    /**
     * 是否开启数据限制
     * 支持auth/personal
     * 表示按权限判断/仅限个人
     * 默认为禁用,若启用请务必保证表中存在admin_id字段
     */
    protected $dataLimit = false;

    /**
     * 数据限制字段
     */
    protected $dataLimitField = 'admin_id';

    /**
     * 数据限制开启时自动填充限制字段值
     */
    protected $dataLimitFieldAutoFill = true;

    /**
     * 是否开启Validate验证
     */
    protected $modelValidate = false;

    /**
     * 是否开启模型场景验证
     */
    protected $modelSceneValidate = false;

    /**
     * Multi方法可批量修改的字段
     */
    protected $multiFields = 'status';

    /**
     * Selectpage可显示的字段
     */
    protected $selectpageFields = '*';

    /**
     * 前台提交过来,需要排除的字段数据
     */
    protected $excludeFields = "";

    /**
     * 导入文件首行类型
     * 支持comment/name
     * 表示注释或字段名
     */
    protected $importHeadType = 'comment';

    /**
     * 引入后台控制器的traits
     */
    use \app\admin\library\traits\Backend;

    public function _initialize()
    {
        $modulename = $this->request->module();
        $controllername = Loader::parseName($this->request->controller());
        $actionname = strtolower($this->request->action());

        $path = str_replace('.', '/', $controllername) . '/' . $actionname;

        // 定义是否Addtabs请求
        !defined('IS_ADDTABS') && define('IS_ADDTABS', input("addtabs") ? true : false);

        // 定义是否Dialog请求
        !defined('IS_DIALOG') && define('IS_DIALOG', input("dialog") ? true : false);

        // 定义是否AJAX请求
        !defined('IS_AJAX') && define('IS_AJAX', $this->request->isAjax());

        // 检测IP是否允许
        check_ip_allowed();

        $this->auth = Auth::instance();

        // 设置当前请求的URI
        $this->auth->setRequestUri($path);
        // 检测是否需要验证登录
        if (!$this->auth->match($this->noNeedLogin)) {
            //检测是否登录
            if (!$this->auth->isLogin()) {
                Hook::listen('admin_nologin', $this);
                $url = Session::get('referer');
                $url = $url ? $url : $this->request->url();
                if (in_array($this->request->pathinfo(), ['/', 'index/index'])) {
                    $this->redirect('index/login', [], 302, ['referer' => $url]);
                    exit;
                }
                $this->error(__('Please login first'), url('index/login', ['url' => $url]));
            }
            // 判断是否需要验证权限
            if (!$this->auth->match($this->noNeedRight)) {
                // 判断控制器和方法是否有对应权限
                if (!$this->auth->check($path)) {
                    Hook::listen('admin_nopermission', $this);
                    $this->error(__('You have no permission'), '');
                }
            }
        }

        // 非选项卡时重定向
        if (!$this->request->isPost() && !IS_AJAX && !IS_ADDTABS && !IS_DIALOG && input("ref") == 'addtabs') {
            $url = preg_replace_callback("/([\?|&]+)ref=addtabs(&?)/i", function ($matches) {
                return $matches[2] == '&' ? $matches[1] : '';
            }, $this->request->url());
            if (Config::get('url_domain_deploy')) {
                if (stripos($url, $this->request->server('SCRIPT_NAME')) === 0) {
                    $url = substr($url, strlen($this->request->server('SCRIPT_NAME')));
                }
                $url = url($url, '', false);
            }
            if($url<>'/admin.php'){
                $this->redirect('index/index', [], 302, ['referer' => $url]);
                exit;
            }
            
        }

        // 设置面包屑导航数据
        $breadcrumb = [];
        if (!IS_DIALOG && !config('fastadmin.multiplenav') && config('fastadmin.breadcrumb')) {
            $breadcrumb = $this->auth->getBreadCrumb($path);
            array_pop($breadcrumb);
        }
        $this->view->breadcrumb = $breadcrumb;

        // 如果有使用模板布局
        if ($this->layout) {
            $this->view->engine->layout('layout/' . $this->layout);
        }

        // 语言检测
        $lang = strip_tags($this->request->langset());

        $site = Config::get("site");

        $upload = \app\common\model\Config::upload();

        // 上传信息配置后
        Hook::listen("upload_config_init", $upload);

        // 配置信息
        $config = [
            'site'           => array_intersect_key($site, array_flip(['name', 'indexurl', 'cdnurl', 'version', 'timezone', 'languages'])),
            'upload'         => $upload,
            'modulename'     => $modulename,
            'controllername' => $controllername,
            'actionname'     => $actionname,
            'jsname'         => 'backend/' . str_replace('.', '/', $controllername),
            'moduleurl'      => rtrim(url("/{$modulename}", '', false), '/'),
            'language'       => $lang,
            'referer'        => Session::get("referer")
        ];
        $config = array_merge($config, Config::get("view_replace_str"));

        Config::set('upload', array_merge(Config::get('upload'), $upload));

        // 配置信息后
        Hook::listen("config_init", $config);
        //加载当前控制器语言包
        $this->loadlang($controllername);
        //渲染站点配置
        $this->assign('site', $site);
        //渲染配置信息
        $this->assign('config', $config);
        //渲染权限对象
        $this->assign('auth', $this->auth);
        //渲染管理员对象
        $this->assign('admin', Session::get('admin'));
    }

    /**
     * 加载语言文件
     * @param string $name
     */
    protected function loadlang($name)
    {
        $name = Loader::parseName($name);
        Lang::load(APP_PATH . $this->request->module() . '/lang/' . $this->request->langset() . '/' . str_replace('.', '/', $name) . '.php');
    }

    /**
     * 渲染配置信息
     * @param mixed $name  键名或数组
     * @param mixed $value 值
     */
    protected function assignconfig($name, $value = '')
    {
        $this->view->config = array_merge($this->view->config ? $this->view->config : [], is_array($name) ? $name : [$name => $value]);
    }

    /**
     * 生成查询所需要的条件,排序方式
     * @param mixed   $searchfields   快速查询的字段
     * @param boolean $relationSearch 是否关联查询
     * @return array
     */
    protected function buildparams($searchfields = null, $relationSearch = null)
    {
        $searchfields = is_null($searchfields) ? $this->searchFields : $searchfields;
        $relationSearch = is_null($relationSearch) ? $this->relationSearch : $relationSearch;
        $search = $this->request->get("search", '');
        $filter = $this->request->get("filter", '');
        $op = $this->request->get("op", '', 'trim');
        $sort = $this->request->get("sort", !empty($this->model) && $this->model->getPk() ? $this->model->getPk() : 'id');
        $order = $this->request->get("order", "DESC");
        $offset = $this->request->get("offset/d", 0);
        $limit = $this->request->get("limit/d", 999999);
        //新增自动计算页码
        $page = $limit ? intval($offset / $limit) + 1 : 1;
        if ($this->request->has("page")) {
            $page = $this->request->get("page/d", 1);
        }
        $this->request->get([config('paginate.var_page') => $page]);
        $filter = (array)json_decode($filter, true);
        $op = (array)json_decode($op, true);
        $filter = $filter ? $filter : [];
        $where = [];
        $alias = [];
        $bind = [];
        $name = '';
        $aliasName = '';
        if (!empty($this->model) && $this->relationSearch) {
            $name = $this->model->getTable();
            $alias[$name] = Loader::parseName(basename(str_replace('\\', '/', get_class($this->model))));
            $aliasName = $alias[$name] . '.';
        }
        $sortArr = explode(',', $sort);
        foreach ($sortArr as $index => & $item) {
            $item = stripos($item, ".") === false ? $aliasName . trim($item) : $item;
        }
        unset($item);
        $sort = implode(',', $sortArr);
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            $where[] = [$aliasName . $this->dataLimitField, 'in', $adminIds];
        }
        if ($search) {
            $searcharr = is_array($searchfields) ? $searchfields : explode(',', $searchfields);
            foreach ($searcharr as $k => &$v) {
                $v = stripos($v, ".") === false ? $aliasName . $v : $v;
            }
            unset($v);
            $where[] = [implode("|", $searcharr), "LIKE", "%{$search}%"];
        }
        $index = 0;
        foreach ($filter as $k => $v) {
            if (in_array($k, ["is_check","SO_Flag_use","ASF_Weight","ASF_Count"])!== false) continue;
            if (!preg_match('/^[a-zA-Z0-9_\-\.]+$/', $k)) {
                continue;
            }
            $sym = isset($op[$k]) ? $op[$k] : '=';
            
        
            if (stripos($k, ".") === false) {
                $k = $aliasName . $k;
            }
            $v = !is_array($v) ? trim($v) : $v;
            $sym = strtoupper(isset($op[$k]) ? $op[$k] : $sym);
            //null和空字符串特殊处理
            if (!is_array($v)) {
                if (in_array(strtoupper($v), ['NULL', 'NOT NULL'])) {
                    $sym = strtoupper($v);
                }
                if (in_array($v, ['""', "''"])) {
                    $v = '';
                    $sym = '=';
                }
            }
            switch ($sym) {
                case '=1000':
                    $where[] = [$k, "=", $v*1000];
                    break;
                case '=':
                case '<>':
                    $where[] = [$k, $sym, (string)$v];
                    break;
                case 'LIKE':
                case 'NOT LIKE':
                case 'LIKE %...%':
                case 'NOT LIKE %...%':
                    $where[] = [$k, trim(str_replace('%...%', '', $sym)), "%{$v}%"];
                    break;
                case 'LIKE %':
                    $where[] = [$k, "LIKE", "{$v}%"];
                    break;
                case '>':
                case '>=':
                case '<':
                case '<=':
                    $where[] = [$k, $sym, intval($v)];
                    break;
                case 'FINDIN':
                case 'FINDINSET':
                case 'FIND_IN_SET':
                    $v = is_array($v) ? $v : explode(',', str_replace(' ', ',', $v));
                    $findArr = array_values($v);
                    foreach ($findArr as $idx => $item) {
                        $bindName = "item_" . $index . "_" . $idx;
                        $bind[$bindName] = $item;
                        $where[] = "FIND_IN_SET(:{$bindName}, `" . str_replace('.', '`.`', $k) . "`)";
                    }
                    break;
                case 'IN':
                case 'IN(...)':
                case 'NOT IN':
                case 'NOT IN(...)':
                    $where[] = [$k, str_replace('(...)', '', $sym), is_array($v) ? $v : explode(',', $v)];
                    break;
                case 'BETWEEN':
                case 'NOT BETWEEN':
                    $arr = array_slice(explode(',', $v), 0, 2);
                    if (stripos($v, ',') === false || !array_filter($arr, function($v){
                        return $v != '' && $v !== false && $v !== null;
                    })) {
                        continue 2;
                    }
                    //当出现一边为空时改变操作符
                    if ($arr[0] === '') {
                        $sym = $sym == 'BETWEEN' ? '<=' : '>';
                        $arr = $arr[1];
                    } elseif ($arr[1] === '') {
                        $sym = $sym == 'BETWEEN' ? '>=' : '<';
                        $arr = $arr[0];
                    }
                    $where[] = [$k, $sym, $arr];
                    break;
                case 'RANGE':
                case 'NOT RANGE':
                    $v = str_replace(' - ', ',', $v);
                    $arr = array_slice(explode(',', $v), 0, 2);
                    if (stripos($v, ',') === false || !array_filter($arr)) {
                        continue 2;
                    }
                    //当出现一边为空时改变操作符
                    if ($arr[0] === '') {
                        $sym = $sym == 'RANGE' ? '<=' : '>';
                        $arr = $arr[1];
                    } elseif ($arr[1] === '') {
                        $sym = $sym == 'RANGE' ? '>=' : '<';
                        $arr = $arr[0];
                    }
                    $tableArr = explode('.', $k);
                    if (count($tableArr) > 1 && $tableArr[0] != $name && !in_array($tableArr[0], $alias) && !empty($this->model)) {
                        //修复关联模型下时间无法搜索的BUG
                        $relation = Loader::parseName($tableArr[0], 1, false);
                        $alias[$this->model->$relation()->getTable()] = $tableArr[0];
                    }
                    $where[] = [$k, str_replace('RANGE', 'BETWEEN', $sym) . ' TIME', $arr];
                    break;
                case 'NULL':
                case 'IS NULL':
                case 'NOT NULL':
                case 'IS NOT NULL':
                    $where[] = [$k, strtolower(str_replace('IS ', '', $sym))];
                    break;
                default:
                    break;
            }
            $index++;
        }
        if (!empty($this->model)) {
            $this->model->alias($alias);
        }
        $model = $this->model;
        $where = function ($query) use ($where, $alias, $bind, &$model) {
            if (!empty($model)) {
                $model->alias($alias);
                $model->bind($bind);
            }
            foreach ($where as $k => $v) {
                if (is_array($v)) {
                    call_user_func_array([$query, 'where'], $v);
                } else {
                    $query->where($v);
                }
            }
        };
        return [$where, $sort, $order, $offset, $limit, $page, $alias, $bind];
    }

    /**
     * 获取数据限制的管理员ID
     * 禁用数据限制时返回的是null
     * @return mixed
     */
    protected function getDataLimitAdminIds()
    {
        if (!$this->dataLimit) {
            return null;
        }
        if ($this->auth->isSuperAdmin()) {
            return null;
        }
        $adminIds = [];
        if (in_array($this->dataLimit, ['auth', 'personal'])) {
            $adminIds = $this->dataLimit == 'auth' ? $this->auth->getChildrenAdminIds(true) : [$this->auth->id];
        }
        return $adminIds;
    }

    /**
     * Selectpage的实现方法
     *
     * 当前方法只是一个比较通用的搜索匹配,请按需重载此方法来编写自己的搜索逻辑,$where按自己的需求写即可
     * 这里示例了所有的参数，所以比较复杂，实现上自己实现只需简单的几行即可
     *
     */
    protected function selectpage()
    {
        //设置过滤方法
        $this->request->filter(['trim', 'strip_tags', 'htmlspecialchars']);

        //搜索关键词,客户端输入以空格分开,这里接收为数组
        $word = (array)$this->request->request("q_word/a");
        //当前页
        $page = $this->request->request("pageNumber");
        //分页大小
        $pagesize = $this->request->request("pageSize");
        //搜索条件
        $andor = $this->request->request("andOr", "and", "strtoupper");
        //排序方式
        $orderby = (array)$this->request->request("orderBy/a");
        //显示的字段
        $field = $this->request->request("showField");
        //主键
        $primarykey = $this->request->request("keyField");
        //主键值
        $primaryvalue = $this->request->request("keyValue");
        //搜索字段
        $searchfield = (array)$this->request->request("searchField/a");
        //自定义搜索条件
        $custom = (array)$this->request->request("custom/a");
        //是否返回树形结构
        $istree = $this->request->request("isTree", 0);
        $ishtml = $this->request->request("isHtml", 0);
        if ($istree) {
            $word = [];
            $pagesize = 999999;
        }
        $order = [];
        foreach ($orderby as $k => $v) {
            $order[$v[0]] = $v[1];
        }
        $field = $field ? $field : 'name';

        //如果有primaryvalue,说明当前是初始化传值
        if ($primaryvalue !== null) {
            $where = [$primarykey => ['in', $primaryvalue]];
            $pagesize = 999999;
        } else {
            $where = function ($query) use ($word, $andor, $field, $searchfield, $custom) {
                $logic = $andor == 'AND' ? '&' : '|';
                $searchfield = is_array($searchfield) ? implode($logic, $searchfield) : $searchfield;
                $searchfield = str_replace(',', $logic, $searchfield);
                $word = array_filter(array_unique($word));
                if (count($word) == 1) {
                    $query->where($searchfield, "like", "%" . reset($word) . "%");
                } else {
                    $query->where(function ($query) use ($word, $searchfield) {
                        foreach ($word as $index => $item) {
                            $query->whereOr(function ($query) use ($item, $searchfield) {
                                $query->where($searchfield, "like", "%{$item}%");
                            });
                        }
                    });
                }
                if ($custom && is_array($custom)) {
                    foreach ($custom as $k => $v) {
                        if (is_array($v) && 2 == count($v)) {
                            $query->where($k, trim($v[0]), $v[1]);
                        } else {
                            $query->where($k, '=', $v);
                        }
                    }
                }
            };
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            $this->model->where($this->dataLimitField, 'in', $adminIds);
        }
        $list = [];
        $total = $this->model->where($where)->count();
        if ($total > 0) {
            if (is_array($adminIds)) {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $fields = is_array($this->selectpageFields) ? $this->selectpageFields : ($this->selectpageFields && $this->selectpageFields != '*' ? explode(',', $this->selectpageFields) : []);

            //如果有primaryvalue,说明当前是初始化传值,按照选择顺序排序
            if ($primaryvalue !== null && preg_match("/^[a-z0-9_\-]+$/i", $primarykey)) {
                $primaryvalue = array_unique(is_array($primaryvalue) ? $primaryvalue : explode(',', $primaryvalue));
                //修复自定义data-primary-key为字符串内容时，给排序字段添加上引号
                $primaryvalue = array_map(function ($value) {
                    return '\'' . $value . '\'';
                }, $primaryvalue);

                $primaryvalue = implode(',', $primaryvalue);

                $this->model->orderRaw("FIELD(`{$primarykey}`, {$primaryvalue})");
            } else {
                $this->model->order($order);
            }

            $datalist = $this->model->where($where)
                ->page($page, $pagesize)
                ->select();

            foreach ($datalist as $index => $item) {
                unset($item['password'], $item['salt']);
                if ($this->selectpageFields == '*') {
                    $result = [
                        $primarykey => isset($item[$primarykey]) ? $item[$primarykey] : '',
                        $field      => isset($item[$field]) ? $item[$field] : '',
                    ];
                } else {
                    $result = array_intersect_key(($item instanceof Model ? $item->toArray() : (array)$item), array_flip($fields));
                }
                $result['pid'] = isset($item['pid']) ? $item['pid'] : (isset($item['parent_id']) ? $item['parent_id'] : 0);
                $list[] = $result;
            }
            if ($istree && !$primaryvalue) {
                $tree = Tree::instance();
                $tree->init(collection($list)->toArray(), 'pid');
                $list = $tree->getTreeList($tree->getTreeArray(0), $field);
                if (!$ishtml) {
                    foreach ($list as &$item) {
                        $item = str_replace('&nbsp;', ' ', $item);
                    }
                    unset($item);
                }
            }
        }
        //这里一定要返回有list这个字段,total是可选的,如果total<=list的数量,则会隐藏分页按钮
        return json(['list' => $list, 'total' => $total]);
    }

    /**
     * 刷新Token
     */
    protected function token()
    {
        $token = $this->request->param('__token__');

        //验证Token
        if (!Validate::make()->check(['__token__' => $token], ['__token__' => 'require|token'])) {
            $this->error(__('Token verification error'), '', ['__token__' => $this->request->token()]);
        }

        //刷新Token
        $this->request->token();
    }

    /**
     * 部门类别
     * @param type int 默认0全部 1仅有效情况
     */
    protected function deptType($type=0)
    {
        $where = [];
        if($type) $where = ["Valid"=>1];
        $order = ["ParentNum"=>"ASC","DD_Num"=>"ASC"];
        $deptModel = (new \app\admin\model\jichu\jg\Deptdetail())
            ->where($where)
            ->order($order)
            ->select();
        $deptList = $deptTree = [];
        foreach($deptModel as $v){
            $deptList[$v["DD_Num"]] = $v["DD_Name"];
            $deptTree[$v["DD_Num"]] = [
                "id" => $v["DD_Num"],
                "parent" => $v["ParentNum"]==""?"#":$v["ParentNum"],
                "text" => $v['DD_Name']."(".$v['DD_Num'].")",
                "state" => [
                    "opened" => false,
                    "disabled" => false,
                    "selected" => false
                ]
            ];
        }
        return [$deptList,array_values($deptTree)];
    }


    /**
     * 产品类别
     */
    protected function productType()
    {
        // $proList = ["0" => "紧固件", "1" => "产成品", "2" => "原材料"];
        $proList = ["0" => "原材料", "1" => "产成品", "2" => "紧固件"];
        return $proList;
    }

    /**
     * 出入库类别
     */
    protected function stockType()
    {
        $stockList = ["0" => "出库", "1" => "入库"];
        return $stockList;
    }

    /**
     * 产品类别和出入库类别死数据
     */
    protected function typeCode()
    {
        // $codeList = [
        //     "0" => [
        //         "0" => "05",
        //         "1" => "06"
        //     ],
        //     "1" => [
        //         "0" => "02",
        //         "1" => "01"
        //     ],
        //     "2" => [
        //         "0" => "03",
        //         "1" => "04"
        //     ]
        // ];
        $codeList = [
            "0" => [
                "0" => "03",
                "1" => "04"
            ],
            "1" => [
                "0" => "02",
                "1" => "01"
            ],
            "2" => [
                "0" => "05",
                "1" => "06"
            ]
        ];
        return $codeList;
    }

    /**
     * 是否默认的列表
     */
    protected function keyList()
    {
        $defaultKey = ["ST_IFDefault","PT_IFDefault","st_selected","bm_default","ct_default","BD_Default"];
        return $defaultKey;
    }

    /**
     * 客户类型list
     * @param type int 默认0全部 1仅有效情况
     */
    protected function saleType($type=0)
    {
        $where = [];
        if($type) $where = ["Valid"=>1];
        $order = ["ST_IFDefault"=>"DESC","ST_Num"=>"ASC"];
        $saleModel = (new \app\admin\model\jichu\yw\SaleType())
            ->field("ST_Num,ST_Name")
            ->where($where)
            ->order($order)
            ->select();
        // $saleList = [""=>"请选择"];
        $saleList = $saleTree = [];
        foreach($saleModel as $v){
            $saleList[$v["ST_Num"]] = $v["ST_Name"];
            $saleTree[$v["ST_Num"]] = [
                "id" => $v["ST_Num"],
                "parent" => "#",
                "text" => $v['ST_Name']
            ];
        }
        return [$saleList,$saleTree];

    }

    /**
     * 地区list
     * @param type int 默认0全部 1仅有效情况
     */
    protected function areaList($type=0)
    {
        $areaList = ["[全部]"=>"[全部]"];
        $where = [];
        if($type) $where = ["Valid"=>1];
        $areaModel = (new \app\admin\model\jichu\wl\AreaDetail())
            ->field("AD_Num,AD_Name")
            ->where($where)
            ->order("AD_Num ASC")
            ->select();
        foreach($areaModel as $v){
            $areaList[$v["AD_Name"]] = $v["AD_Name"];
        }
        return $areaList;
    }

    /**
     * customertList
     * @param type int 默认0全部 1仅有效情况
     */
    protected function customerList($type=0)
    {
        $customerList = [""=>"请选择"];
        $customerTree = [];
        $where = [];
        if($type) $where = ["Valid"=>1];
        $customerModel = (new \app\admin\model\jichu\wl\Customerclass())
            ->field("CC_Num,CC_Name")
            ->where($where)
            ->order("CC_Num ASC")
            ->select();
        foreach($customerModel as $v){
            $customerList[$v["CC_Num"]] = $v["CC_Name"];
            $customerTree[$v["CC_Num"]] = [
                "id" => $v["CC_Num"],
                "parent" => "#",
                "text" => $v['CC_Name']
            ];
        }
        return [$customerList,$customerTree];
    }

    /**
     * customertList
     * @param type int 默认0全部 1仅有效情况
     */
    protected function vendorList($type=0,$where = [])
    {
        $classList = $classTree = [];
        if($type) $where["Valid"] = 1;
        $customerModel = (new \app\admin\model\jichu\wl\VendorClass())
            ->field("VC_Num,VC_Name")
            ->where($where)
            ->order("VC_Num asc")
            ->select();
        foreach($customerModel as $v){
            $v["VC_Num"] = trim($v["VC_Num"]);
            $v["VC_Name"] = trim($v["VC_Name"]);
            $classList[$v["VC_Num"]] = $v["VC_Name"];
            $classTree[$v["VC_Num"]] = [
                "id" => $v["VC_Num"],
                "parent" => "#",
                "text" => $v['VC_Name']
            ];
        }
        return [$classList,$classTree];
    }

    /**
     * customertList
     * @param type int 默认0全部 1仅有效情况
     */
    protected function vendorNameList($type=0,$where = [])
    {
        $classList = [];
        if($type) $where["Valid"] = 1;
        $customerModel = (new \app\admin\model\jichu\wl\Vendor())
            ->field("V_Num,V_Name")
            ->where($where)
            ->order("V_Num ASC")
            ->select();
        foreach($customerModel as $v){
            $v["V_Name"] = trim($v["V_Name"]);
            $classList[$v["V_Name"]] = $v["V_Name"];
        }
        return $classList;
    }

    /**
     * employeeList
     * @param type int 默认0全部 1仅有效情况
     */
    protected function employeeList($type=0)
    {
        $employeeList = [];
        $where = [];
        if($type) $where = ["Valid"=>1];
        $employeeModel = (new \app\admin\model\jichu\jg\Employee())
            ->field("E_Num,E_Name,DD_Num")
            ->where($where)
            ->order("CC_Num ASC")
            ->select();
        foreach($employeeModel as $v){
            $employeeList[$v["E_Num"]] = $v["E_Name"];
        }
        return $employeeList;
    }

    /**
     * purchaseList
     * @param type int 默认0全部 1仅有效情况
     */
    protected function purchaseList($type=0)
    {
        $employeeList = [];
        $where = [];
        if($type) $where = ["Valid"=>1];
        $employeeModel = (new \app\admin\model\jichu\yw\PurchaseType())
            ->field("PT_Num,PT_Name")
            ->where($where)
            ->order("PT_IFDefault DESC,PT_Num ASC")
            ->select();
        foreach($employeeModel as $v){
            $employeeList[$v["PT_Num"]] = $v["PT_Name"];
        }
        return $employeeList;
    }

    /**
     * shipplingList
     * @param type int 默认0全部 1仅有效情况
     */
    protected function shipplingList($type=0)
    {
        $employeeList = [];
        $where = [];
        if($type) $where = ["Valid"=>1];
        $employeeModel = (new \app\admin\model\jichu\yw\ShippingType())
            ->field("ST_Num,ST_Name")
            ->where($where)
            ->order("st_selected DESC,ST_Num ASC")
            ->select();
        foreach($employeeModel as $v){
            $employeeList[$v["ST_Num"]] = $v["ST_Name"];
        }
        return $employeeList;
    }

    /**
     * vendorNumList
     * @param type int 默认0全部 1仅有效情况
     */
    protected function vendorNumList($type=0)
    {
        $employeeList = [];
        $where = [];
        if($type) $where = ["Valid"=>1];
        $employeeModel = (new \app\admin\model\jichu\wl\Vendor())
            ->field("V_Num,V_Name")
            ->where($where)
            ->order("V_Num ASC")
            ->select();
        foreach($employeeModel as $v){
            $employeeList[$v["V_Num"]] = $v["V_Name"];
        }
        return $employeeList;
    }

    /**
     * inventoryList
     * 原材料的列表
     */
    protected function inventoryList()
    {
        $inventoryList = (new \app\admin\model\jichu\ch\InventoryMaterial())
            ->field("IM_Class")
            ->where("IM_Class","<>","")
            ->group("IM_Class")
            ->select();
        $arr = [];
        foreach($inventoryList as $v){
            $arr[$v["IM_Class"]] = $v["IM_Class"];
        }
        return $arr;
    }

    protected function inventoryNumList($type=0)
    {
        $inventoryList = (new \app\admin\model\jichu\ch\InventoryMaterial())
            ->field("IM_Class,IM_Num,IM_Spec,IM_PerWeight")
            ->where("IM_Class","<>","")
            ->select();
        $arr = $arr_second = [];
        foreach($inventoryList as $k=>$v){
            $arr[$v["IM_Class"].'-'.$v["IM_Spec"]] = $v["IM_Num"];
            $arr_second[$v["IM_Class"].'-'.$v["IM_Spec"]] = [
                "IM_Num" => $v["IM_Num"],
                "IM_PerWeight" => $v["IM_PerWeight"]
            ];
        }
        if($type) return $arr_second;
        else return $arr;
    }

    /**
     * wareclassList
     * 仓库的列表
     */
    protected function wareclassList($where = [],$type=0)
    {
        if(empty($where)) $where = [
            "ParentNum" => ["=","01"],
            "WC_Name" => ["<>",""]
        ];
        $inventoryList = (new \app\admin\model\jichu\ch\WareClass())
            ->field("WC_Name,WC_Num")
            ->where($where)
            ->group("WC_Name")
            ->select();
        $arr = [""=>"请选择"];
        foreach($inventoryList as $v){
            if($type==2) $arr[$v["WC_Name"]] = $v["WC_Num"];
            else if($type==1) $arr[$v["WC_Num"]] = $v["WC_Name"];
            else $arr[$v["WC_Name"]] = $v["WC_Name"];
        }
        return $arr;
    }

    /**
     * limberList
     * 材质分类的列表
     */
    protected function limberList()
    {
        $inventoryList = (new \app\admin\model\jichu\ch\Limber())
            ->field("L_Name")
            ->where("L_Name","<>","")
            ->group("L_Name")
            ->select();
        $arr = [];
        foreach($inventoryList as $v){
            $arr[$v["L_Name"]] = $v["L_Name"];
        }
        return $arr;
    }

    protected function getLimber()
    {
        $list = $kj_list = [];
        $row = (new \app\admin\model\jichu\ch\Limber())->field("L_Name,L_SuperNum")->where("L_Valid",1)->order("L_Name ASC")->group("L_Name")->select();
        foreach($row as $v){
            $list[$v["L_Name"]] = $v["L_Name"];
            if($v["L_SuperNum"]) $kj_list[$v["L_SuperNum"]] = $v["L_Name"];
        }
        return [$list,$kj_list];
    }
    /**
     * dtmerialList
     */
    protected function dtmerialList()
    {
        $list = [
            1=>"1-新放样",
            2=>"2-套用塔",
            3=>"3-国外PDF",
            4=>"4-国外CAD"
        ];
        return $list;
    }

    /**
     * customertList
     * @param type int 默认0全部 1仅有效情况
     */
    protected function wjInventClassList($type=0)
    {
        $classList = $classTree = [];
        $where = [];
        if($type) $where = ["Valid"=>1];
        $wj_inventory_sort_list = (new \app\admin\model\chain\machine\WjInventorySort())
            ->field("IS_Num,IS_Name,ParentNum")
            ->where($where)
            ->order("IS_Num ASC")
            ->select();
        foreach($wj_inventory_sort_list as $v){
            $v["IS_Num"] = trim($v["IS_Num"]);
            $v["IS_Name"] = trim($v["IS_Name"]);
            $classList[$v["IS_Num"]] = $v["IS_Name"];
            $classTree[$v["IS_Num"]] = [
                "id" => $v["IS_Num"],
                "parent" => $v["ParentNum"]?trim($v["ParentNum"]):"#",
                "text" => $v['IS_Name']
            ];
        }
        return [$classList,$classTree];
    }

    /**
     * 对应IM_Class_Num
     */
    public function imClassNumList()
    {
        $data = [];
        $list = (new InventoryMaterial())->field("IM_Class,IM_Class_Num")->where("length(ParentNum)","=",2)->select();
        foreach($list as $k=>$v){
            $data[$v["IM_Class"]] = $v["IM_Class_Num"];
        }
        return $data;
    }

    /**
     * 对应检验的选择
     */
    public function processList()
    {
        $selectdata = $data = [];
        $list = (new \app\admin\model\quality\process\ProMatchType())->order("id")->select();
        foreach($list as $k=>$v){
            $data[$v["id"]] = $v->toArray();
            $selectdata[$v["id"]] = $v["name"];
        }
        return [$data,$selectdata];
    }

    /**
     * 对应measurement
     */
    public function measurementList()
    {
        $data = [];
        $list = (new Measurement())->field("M_Name")->where("M_Name","<>","")->select();
        foreach($list as $v){
            $data[$v["M_Name"]] = $v["M_Name"];
        }
        return $data;
    }

    /**
     * wjstoreyj
     * @param type int 默认0全部 1仅有效情况
     */
    protected function wjStoreYj()
    {
        $classList = $classTree = [];
        $where = [];
        $wj_store_balance_list = (new \app\admin\model\chain\machine\WjStoreBalance())->alias("wsb")
            ->join(["wareclass"=>"wc"],"wsb.WC_Num = wc.WC_Num")
            ->field("BL_Num,BL_Date,wsb.WC_Num,WC_Name")
            ->where($where)
            ->order("BL_Date DESC")
            ->group("BL_Num")
            ->select();
        foreach($wj_store_balance_list as $v){
            $classList[$v["BL_Num"]] = date("Y-m-d",strtotime($v["BL_Date"])).$v["WC_Name"];
            $classTree[$v["BL_Num"]] = [
                "id" => $v["BL_Num"],
                "parent" => "#",
                "text" => date("Y-m-d",strtotime($v["BL_Date"])).$v["WC_Name"]
            ];
        }
        return [$classList,$classTree];
    }
    
    public function getString(){
        $string = "{field: 'id', title: '', formatter: function(value,row,index){return ++index;}},
        {field: 'MTD_TestNo', title: '试验编号'},
        {field: 'IM_Class', title: '试样名称'},
        {field: 'CT_Num', title: '委托单编码'},
        {field: 'CTD_Pi', title: '进货批次'},
        {field: 'IM_Spec', title: '规格'},
        {field: 'L_Name', title: '材质'},
        {field: 'MGN_Length', title: '长度(m)'},
        {field: 'LuPiHao', title: '炉号'},
        {field: 'PiHao', title: '批号'},
        {field: 'MTD_C', title: 'C(%)'},
        {field: 'MTD_Si', title: 'Si(%)'},
        {field: 'MTD_Mn', title: 'Mn(%)'},
        {field: 'MTD_P', title: 'P(%)'},
        {field: 'MTD_S', title: 'S(%)'},
        {field: 'MTD_V', title: 'V(%)'},
        {field: 'MTD_Nb', title: 'Nb(%)'},
        {field: 'MTD_Ti', title: 'Ti(%)'},
        {field: 'MTD_Cr', title: 'Cr(%)'},
        {field: 'MTD_Czsl', title: '屈服强度(MPa)'},
        {field: 'MTD_Rm', title: '抗拉强度(MPa)'},
        {field: 'MTD_Percent', title: '伸长率(%)'},
        {field: 'MTD_Temperature', title: '冲击温度(℃)'},
        {field: 'MTD_Cjf', title: '冲击1'},
        {field: 'MTD_Cjs', title: '冲击2'},
        {field: 'MTD_Cjt', title: '冲击3'},
        {field: 'MTD_Wq', title: '弯曲试验'},
        {field: 'MTD_Wg', title: '尺寸外观'},
        {field: 'MTD_ChDate', title: '原材料出厂日期'},
        {field: 'MTD_Conclusion', title: '结论'},";
        preg_match_all("/d: \'\w+\'/",$string,$matchs);
        // preg_match_all('/\[\"(.*)\",\"\w+\"/',$string,$matchs);
        $arr = [];
        foreach($matchs[0] as $k=>$v){
            $arr[] = rtrim(ltrim($v,"d: '"),"'");
        }
        $string = implode(",",$arr);
        pri($string);
    }

    public function get_monthinfo_by_date($date){
        $ret = array();
        $timestamp = strtotime($date);
        $mdays = date('t', $timestamp);
        return array(
          'month_start_day' => date('Y-m-01 00:00:00', $timestamp),
          'month_end_day' => date('Y-m-'.$mdays." 23:59:59", $timestamp)
          );
      }

    //DC_Num
    public function updatedhk($ids=null)
    {
        if($ids){
            $dhmodel = new DhCooperatekDetail();
            $list = $dhmodel->alias("dhk")
                ->join(["dhcooperateksingle"=>"dhs"],"dhk.DCD_ID = dhs.DCD_ID")
                ->join(["fytxkdetail"=>"fd"],"dhs.DtMD_ID_PK = fd.DtMD_ID_PK")
                ->field("dhk.DCD_ID,sum(fd.DtMD_fUnitWeight*dhs.DHS_Count) as DCD_SWeight")
                ->where("DC_Num",$ids)
                ->group("dhk.DCD_ID")
                ->select();
            foreach($list as $k=>$v){
                $update = $dhmodel->update($v->toArray());
            }
        }
    }

    public function updatedh($ids=null)
    {
        if($ids){
            $dhmodel = new DhCooperateDetail();
            $list = $dhmodel->alias("dhk")
                ->join(["dhcooperatesingle"=>"dhs"],"dhk.DCD_ID = dhs.DCD_ID")
                ->join(["dtmaterialdetial"=>"fd"],"dhs.DtMD_ID_PK = fd.DtMD_ID_PK")
                ->field("dhk.DCD_ID,sum(fd.DtMD_fUnitWeight*dhs.DHS_Count) as DCD_SWeight")
                ->where("DC_Num",$ids)
                ->group("dhk.DCD_ID")
                ->select();
            foreach($list as $k=>$v){
                $update = $dhmodel->update($v->toArray());
            }
        }
    }

    protected function getStateList($list=[])
    {
        $state = $sectSaveList = [];
        $state_model = new StoreState();
        foreach($list as $v){
            $key = $v["IM_Num"].'-'.$v["L_Name"].'-'.round($v["SID_Length"],2).'-'.round($v["SID_Width"],2);
            if(!isset($sectSaveList[$key])){
                $sectSaveList[$key] = [
                    "IM_Num" => $v["IM_Num"],
                    "IM_Spec" => $v["IM_Spec"],
                    "L_Name" => $v["L_Name"],
                    "SID_Length" => $v["SID_Length"],
                    "SID_Width" => $v["SID_Width"],
                    "SS_WareHouse" => $v["SI_WareHouse"],
                    "SID_Count" => $v["SID_Count"],
                    "SID_Weight" => $v["SID_Weight"]
                ];
            }else{
                $sectSaveList[$key]["SID_Count"] += $v["SID_Count"];
                $sectSaveList[$key]["SID_Weight"] += $v["SID_Weight"];
            }
        }
        foreach($sectSaveList as $v){
            $where = [
                "IM_Num" => ["=",$v["IM_Num"]],
                "L_Name" => ["=",$v["L_Name"]],
                "SS_Length" => ["=",$v["SID_Length"]],
                "SS_Width" => ["=",$v["SID_Width"]],
                "SS_WareHouse" => ["=",$v["SS_WareHouse"]]
            ];
            $state_one = $state_model->where($where)->find();
            $edit_one = [];
            if($state_one){
                $edit_one = $state_one->toArray();
                $edit_one["SS_Count"] = $edit_one["SS_Count"]+$v["SID_Count"];
                $edit_one["SS_Weight"] = $edit_one["SS_Weight"]+$v["SID_Weight"];
            }else{
                $edit_one = [
                    "IM_Num" => $v["IM_Num"],
                    "IM_Spec" => $v["IM_Spec"],
                    "L_Name" => $v["L_Name"],
                    "SS_Length" => $v["SID_Length"],
                    "SS_Width" => $v["SID_Width"],
                    "SS_Count" => $v["SID_Count"],
                    "SS_Weight" => $v["SID_Weight"],
                    "SS_WareHouse" => $v["SS_WareHouse"]
                ];
            }
            $state[] = $edit_one;
        }
        return $state;
    }

    protected function getStateUpList($list=[])
    {
        $sectSaveList = $state = [];
        $state_model = new StoreState();
        $msg = "";
        foreach($list as $v){
            $key = $v["IM_Num"].'-'.$v["L_Name"].'-'.round($v["SID_Length"],2).'-'.round($v["SID_Width"],2);
            if(!isset($sectSaveList[$key])){
                $sectSaveList[$key] = [
                    "IM_Num" => $v["IM_Num"],
                    "IM_Spec" => $v["IM_Spec"],
                    "L_Name" => $v["L_Name"],
                    "SID_Length" => $v["SID_Length"],
                    "SID_Width" => $v["SID_Width"],
                    "SS_WareHouse" => $v["SI_WareHouse"],
                    "SID_Count" => $v["SID_Count"],
                    "SID_Weight" => $v["SID_Weight"]
                ];
            }else{
                $sectSaveList[$key]["SID_Count"] += $v["SID_Count"];
                $sectSaveList[$key]["SID_Weight"] += $v["SID_Weight"];
            }
        }
        foreach($sectSaveList as $v){
            $where = [
                "IM_Num" => ["=",$v["IM_Num"]],
                "L_Name" => ["=",$v["L_Name"]],
                "SS_Length" => ["=",$v["SID_Length"]],
                "SS_Width" => ["=",$v["SID_Width"]],
                "SS_WareHouse" => ["=",$v["SS_WareHouse"]]
            ];
            $state_one = $state_model->where($where)->find();
            $edit_one = [];
            if($state_one){
                $edit_one = $state_one->toArray();
                $edit_one["SS_Count"] = $edit_one["SS_Count"]-$v["SID_Count"];
                $edit_one["SS_Weight"] = $edit_one["SS_Weight"]-$v["SID_Weight"];
            }else{
                $msg = "库存有误，无法弃审";
            }
            $state[] = $edit_one;
        }
        return [$state,$msg];
    }

    protected function boltStoreNews()
    {
        $data = [];
        $list = (new BoltStoreStock())->alias("bss")->join(["inventorymaterial"=>"im"],"bss.IM_Num = im.IM_Num")->join(["Vendor"=>"v"],"v.V_Num = bss.V_Num")->field("V_ShortName,bss.BSS_ID,bss.IM_Length,bss.BSS_Place,bss.BSS_Sort,IM_Class,IM_LName,IM_Spec,IM_Measurement")->select();
        foreach($list as $k=>$v){
            $data[$v["BSS_ID"]] = $v->toArray();
        }
        return $data;
    }

    /**
     * machinelist
     * 机器设备的列表
     */
    protected function machineList($where = [])
    {
        $machineList = (new \app\admin\model\jichu\yw\DeviceModel());
        if(!empty($where)) $machineList=$machineList->where($where);
        $machineList = $machineList
            ->select();
        //0所有 1有效
        $arr = [0=>[],1=>[]];
        foreach($machineList as $v){
            $arr[0][$v["id"]] = $v["name"];
            if($v["valid"]) $arr[1][$v["id"]] = $v["name"];
        }
        return $arr;
    }

    protected function getImportViewData($data = [],$ids=0)
    {
        $biZhong = (new InventoryMaterial())->getIMPerWeight();
        $sectList = $detailList = [];
        foreach($data as $v){
            if($v[1]=='') continue;
            foreach($v as $pk=>$pv){
                $v[$pk] = trim($pv);
            }
            $Dts_Name = $DtMD_sPartsID = $DtMD_sStuff = $DtMD_sMaterial = "";
            $Dts_Name = $v[0];
            
            $sectList[$Dts_Name] = [
                "DtS_Name" => $Dts_Name,
                "DtM_iID_FK" => $ids,
                "DtS_sWriter" => $this->admin["nickname"],
                "DtS_dWriterTime" => date("Y-m-d H:i:s"),
                "DtS_dModifyTime" => date("Y-m-d H:i:s")
            ];
            $DtMD_sPartsID = $v[1];
            $DtMD_sMaterial = $v[2]==''?'Q235B':$v[2];
            $DtMD_sStuff = $v[3];
            $v[4] = str_replace("L","∠",$v[4]);
            $v[4] = str_replace("∟","∠",$v[4]);
            $v[4] = str_replace("X","*",$v[4]);
            $v[4] = str_replace("x","*",$v[4]);
            $v[4] = str_replace("Φ","ф",$v[4]);
            
            $length = $v[5]?$v[5]*0.001:0;
            $width = $v[6]?$v[6]*0.001:0;

            $area = $DtMD_sStuff!='钢板'?$length:$length*$width*abs($v[3]);

            $DtMD_fUnitWeight = 0;
            if($DtMD_sStuff=="圆钢"){
                preg_match_all("/\d+\.?\d*/",$v[4],$matches);
                $L = isset($matches[0][0])?$matches[0][0]:0;
                $DtMD_fUnitWeight = round($L*$L*0.00617,2);
            }else if($DtMD_sStuff=="钢管" or $DtMD_sStuff=="法兰"){
                preg_match_all("/\d+\.?\d*/",$v[4],$matches);
                $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                $r = isset($matches[0][1])?$matches[0][1]/2*0.001:0;
                if(strpos($v[4],"*")) $r = $R-(2*$r);
                $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
            }else if($DtMD_sStuff=="格栅板"){
                $rou = ["G255/40/100W"=>32.1,"G253/40/100W"=>21.3];
                $rou_one = isset($rou[$v[4]])?$rou[$v[4]]:0;
                $DtMD_fUnitWeight = round($rou_one * $length*$width,4);
            }else{
                $DtMD_fUnitWeight = (isset($biZhong[$DtMD_sStuff][$v[4]]))?round($biZhong[$DtMD_sStuff][$v[4]] * $area,4):0;
            }
            // pri($v[10],$DtMD_fUnitWeight,round($v[10]/$v[8],2),'');
            $detailList[$Dts_Name][] = [
                'DtMD_sPartsID' => $DtMD_sPartsID,
                'DtMD_sStuff' => $DtMD_sStuff,
                'DtMD_sMaterial' => $DtMD_sMaterial,
                'DtMD_sSpecification' => $v[4]?$v[4]:"",
                'DtMD_iLength' => $v[5]?$v[5]:0,
                'DtMD_fWidth' => $v[6]?$v[6]:0,
                'DtMD_iTorch' => $v[7]?$v[7]:0,
                'DtMD_iUnitCount' => $v[8]?$v[8]:0,
                'DtMD_fUnitWeight' => $v[10]==""?$DtMD_fUnitWeight:($v[8]?round($v[10]/$v[8],2):0),
                'DtMD_iUnitHoleCount' => $v[11]?$v[11]:0,
                'type' => (isset($v[12]) and $v[12])?$v[12]:"",
                'DtMD_iWelding' => $v[13]==""?0:$v[13],
                'DtMD_iFireBending' => $v[14]==""?0:$v[14],
                'DtMD_iCuttingAngle' => $v[15]==""?0:$v[15],
                'DtMD_fBackOff' => $v[16]==""?0:$v[16],
                'DtMD_iBackGouging' => $v[17]==""?0:$v[17],
                'DtMD_DaBian' => $v[18]==""?0:$v[18],
                'DtMD_KaiHeJiao' => $v[19]==""?0:$v[19],
                'DtMD_GeHuo' => $v[20]==""?0:$v[20],
                'DtMD_ZuanKong' => $v[21]==""?0:$v[21],
                'DtMD_sRemark' => (isset($v[22]) and $v[22])?$v[22]:""
            ];
        }
        return [$sectList,$detailList];
    }

    protected function getExportData($list){
        $fieldTot = ["DtMD_iWelding","DtMD_iFireBending","DtMD_iCuttingAngle","DtMD_fBackOff","DtMD_iBackGouging","DtMD_DaBian","DtMD_KaiHeJiao","DtMD_GeHuo","DtMD_ZuanKong"];
        foreach($list as $k=>$v){
            $list[$k]["DtMD_sSpecification"] = strtr($v["DtMD_sSpecification"],"*","x");
            foreach($fieldTot as $vv){
                $list[$k][$vv] = $v[$vv]==0?"":$v[$vv];
            }
        }
        
        $header = [
            ['段名', 'DtS_Name'],
            ['零件编号', 'DtMD_sPartsID'],
            ['材质', 'DtMD_sMaterial'],
            ['材料名称', 'DtMD_sStuff'],
            ['规格', 'DtMD_sSpecification'],
            ['长度(mm)', 'DtMD_iLength'],
            ['宽度(mm)', 'DtMD_fWidth'],
            ['厚度(mm)', 'DtMD_iTorch'],
            ['单基数量', 'DtMD_iUnitCount'],
            ['单件重量', 'DtMD_fUnitWeight'],
            ['总重', 'sum_weight'],
            ['孔数', 'DtMD_iUnitHoleCount'],
            ['类型', 'type'],
            ['电焊(0/1/2/3)', 'DtMD_iWelding'],
            ['制弯(0/1/2)', 'DtMD_iFireBending'],
            ['切角(0/1)', 'DtMD_iCuttingAngle'],
            ['铲背(0/1)', 'DtMD_fBackOff'],
            ['清根(0/1)', 'DtMD_iBackGouging'],
            ['打扁(0/1)', 'DtMD_DaBian'],
            ['开合角(0/1/2)', 'DtMD_KaiHeJiao'],
            ['割豁(0/1)', 'DtMD_GeHuo'],
            ['钻孔(0/1)', 'DtMD_ZuanKong'],
            ['备注', 'DtMD_sRemark']
        ];
        return [$list,$header];
    }

    // protected function getDetailData($params=[],$ids=0)
    // {
    //     $detailSaveList = $slabList = $keyValue = [];
    //     $key = "";
    //     $biZhong = (new InventoryMaterial())->getIMPerWeight();
    //     foreach($params as $v){
    //         $key = rtrim($v["name"],"[]");
    //         isset($keyValue[$key])?"":$keyValue[$key] = [];
    //         $keyValue[$key][] = $v["value"];
    //     }
    //     $bjhList = [];
    //     foreach($keyValue["DtMD_ID_PK"] as $k=>$v){
    //         if(isset($bjhList[$keyValue["DtMD_sPartsID"][$k]])) return json(["code"=>0,'msg'=>"部件号有重复，请进行修改！"]);
    //         else $bjhList[$keyValue["DtMD_sPartsID"][$k]] = $keyValue["DtMD_sPartsID"][$k];

    //         $Dts_Name = 0;
    //         $DtMD_sPartsID = $keyValue['DtMD_sPartsID'][$k];
    //         $Dts_Name = $DtMD_sStuff = $DtMD_sMaterial = "";
    //         if(substr($DtMD_sPartsID,0,2)=='00'){
    //             $Dts_Name = 0;
    //         }else{
    //             if(strpos($DtMD_sPartsID,"-") > 0){
    //                 $left_h_part = substr($DtMD_sPartsID,0,strpos($DtMD_sPartsID,"-"));
    //                 preg_match_all('/(\d+)|([^\d]+)/',$left_h_part,$matches);
    //                 $Dts_Name = $matches[0][0]??0;
    //             }else{
    //                 $part = (int)$DtMD_sPartsID;
    //                 if(strlen($part)<=2) $Dts_Name = (int)$part;
    //                 else if(strlen($part)==3) $Dts_Name = substr($part,0,1);
    //                 else $Dts_Name = substr($part,0,2);
    //             }
    //         }
    //         if(strlen($Dts_Name)>1){
    //             $Dts_Name = substr($Dts_Name,0,1)==0?substr($Dts_Name,1):$Dts_Name;
    //         }
    //         $sectList[$Dts_Name] = [
    //             "DtS_Name" => $Dts_Name,
    //             "DtM_iID_FK" => $ids,
    //             "DtS_sWriter" => $this->admin["nickname"],
    //             "DtS_dWriterTime" => date("Y-m-d H:i:s"),
    //             "DtS_dModifyTime" => date("Y-m-d H:i:s")
    //         ];
    //         $DtMD_sMaterial = $keyValue['DtMD_sMaterial'][$k]==''?'Q235B':$keyValue['DtMD_sMaterial'][$k];
    //         foreach([["L","∠"],["∟","∠"],["X","*"],["x","*"],["Φ","ф"]] as $sv){
    //             $keyValue['DtMD_sSpecification'][$k] = str_replace($sv[0],$sv[1],$keyValue['DtMD_sSpecification'][$k]);
    //         }
    //         $firstField = mb_substr(trim($keyValue['DtMD_sSpecification'][$k]),0,1);
    //         $DtMD_sStuff = "";
            
    //         if($firstField == '∠') $DtMD_sStuff = '角钢';
    //         elseif($firstField == '-') $DtMD_sStuff = '钢板';
    //         elseif($firstField == '[') $DtMD_sStuff = '槽钢';
    //         else{
    //             preg_match_all("/\d+\.?\d*/",$keyValue['DtMD_sSpecification'][$k],$matches);
    //             if(count($matches[0])==2) $DtMD_sStuff = '钢管';
    //             else $DtMD_sStuff = '圆钢';
    //         }
    //         $DtMD_sStuff = $keyValue['DtMD_sStuff'][$k]?$keyValue['DtMD_sStuff'][$k]:$DtMD_sStuff;
            
    //         $length = $keyValue['DtMD_iLength'][$k]?$keyValue['DtMD_iLength'][$k]*0.001:0;
    //         $width = $keyValue['DtMD_fWidth'][$k]?$keyValue['DtMD_fWidth'][$k]*0.001:0;
    //         $area = $DtMD_sStuff!='钢板'?$length:$length*$width*abs($keyValue['DtMD_sSpecification'][$k]);

    //         $DtMD_fUnitWeight = 0;
    //         if($DtMD_sStuff=="圆钢"){
    //             preg_match_all("/\d+\.?\d*/",$keyValue['DtMD_sSpecification'][$k],$matches);
    //             $L = isset($matches[0][0])?$matches[0][0]:0;
    //             $DtMD_fUnitWeight = round($L*$L*0.00617,2);
    //         }else if($DtMD_sStuff=="钢管"){
    //             preg_match_all("/\d+\.?\d*/",$keyValue['DtMD_sSpecification'][$k],$matches);
    //             $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
    //             $r = isset($matches[0][1])?$matches[0][1]/2*0.001:0;
    //             if(strpos($keyValue['DtMD_sSpecification'][$k],"*")) $r = $R-(2*$r);
    //             $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
    //         }else{
    //             $DtMD_fUnitWeight = (isset($biZhong[$DtMD_sStuff][$keyValue['DtMD_sSpecification'][$k]]))?round($biZhong[$DtMD_sStuff][$keyValue['DtMD_sSpecification'][$k]] * $area,4):0;
    //         }
    //         $detailSaveList[$k] = [
    //             'DtMD_ID_PK' => $v,
    //             'DtMD_iSectID_FK' => $ids,
    //             'DtMD_sPartsID' => $DtMD_sPartsID,
    //             'DtMD_sStuff' => $DtMD_sStuff,
    //             'DtMD_sMaterial' => $DtMD_sMaterial,
    //             'DtMD_sSpecification' => $keyValue['DtMD_sSpecification'][$k],
    //             'DtMD_iLength' => $keyValue['DtMD_iLength'][$k]?$keyValue["DtMD_iLength"][$k]:0,
    //             'DtMD_fWidth' => $keyValue['DtMD_fWidth'][$k]?$keyValue["DtMD_fWidth"][$k]:0,
    //             'DtMD_iTorch' => $keyValue['DtMD_iTorch'][$k]?$keyValue["DtMD_iTorch"][$k]:0,
    //             'DtMD_iUnitCount' => $keyValue["DtMD_iUnitCount"][$k]?$keyValue["DtMD_iUnitCount"][$k]:0,
    //             'DtMD_fUnitWeight' => $DtMD_fUnitWeight?$DtMD_fUnitWeight:$keyValue["DtMD_fUnitWeight"][$k],
    //             'DtMD_iUnitHoleCount' => $keyValue['DtMD_iUnitHoleCount'][$k]?$keyValue["DtMD_iUnitHoleCount"][$k]:0,
    //             'type' => $keyValue['type'][$k],
    //             'DtMD_iWelding' => $keyValue['DtMD_iWelding'][$k]?$keyValue['DtMD_iWelding'][$k]:0,
    //             'DtMD_iFireBending' => $keyValue['DtMD_iFireBending'][$k]?$keyValue['DtMD_iFireBending'][$k]:0,
    //             'DtMD_iCuttingAngle' => $keyValue['DtMD_iCuttingAngle'][$k]?$keyValue['DtMD_iCuttingAngle'][$k]:0,
    //             'DtMD_fBackOff' => $keyValue['DtMD_fBackOff'][$k]?$keyValue['DtMD_fBackOff'][$k]:0,
    //             'DtMD_iBackGouging' => $keyValue['DtMD_iBackGouging'][$k]?$keyValue['DtMD_iBackGouging'][$k]:0,
    //             'DtMD_DaBian' => $keyValue['DtMD_DaBian'][$k]?$keyValue['DtMD_DaBian'][$k]:0,
    //             'DtMD_KaiHeJiao' => $keyValue['DtMD_KaiHeJiao'][$k]?$keyValue['DtMD_KaiHeJiao'][$k]:0,
    //             'DtMD_GeHuo' => $keyValue['DtMD_GeHuo'][$k]?$keyValue['DtMD_GeHuo'][$k]:0,
    //             'DtMD_ZuanKong' => $keyValue['DtMD_ZuanKong'][$k]?$keyValue['DtMD_ZuanKong'][$k]:0,
    //             'DtMD_sRemark' => $keyValue['DtMD_sRemark'][$k],
    //         ];
    //         if($v) $slabList[] = $v;
    //         else unset($detailSaveList[$k]["DtMD_ID_PK"]);
    //     }
    //     return [$detailSaveList,$slabList];
    // }

    protected function _getGenerPiece($detail_list=[],$workmanship_list=[])
    {
        $save_list = [];
        $content = [
            "pt_num" => '',
            "sum_weight" => 0,
            "hole_number" => 0,
            "parts_id" => '',
            "stuff" => '',
            "specification" => '',
            "material" => '',
            "workmanship" => '',
            "surplus_sum_weight" => 0,
            "surplus_hole_number" => 0,
            "list" => []
        ];
        foreach($detail_list as $v){
            foreach($workmanship_list as $kk=>$vv){
                if($kk=='total'){
                    $num = 0;
                    foreach($vv[2] as $vvv){
                        if($v[$vvv]) $num++;
                    }
                    if($num){
                        $key = $v["material"].'-'.$v["specification"].'-'.$vv[0];
                        isset($save_list[$key])?"":$save_list[$key] = $content;
                        $save_list[$key]["pt_num"] = $v["PT_Num"];
                        $save_list[$key]["stuff"] = $v["stuff"];
                        $save_list[$key]["specification"] = $v["specification"];
                        $save_list[$key]["material"] = $v["material"];
                        $save_list[$key]["workmanship"] = $vv[0];
                        $save_list[$key]["list"][] = [
                            "pt_num" => $v["PT_Num"],
                            "sum_weight" => $v["sum_weight"]*$num,
                            "hole_number" => $v["hole_number"]*$num,
                            "parts_id" => $v["parts_id"],
                            "stuff" => $v["stuff"],
                            "specification" => $v["specification"],
                            "material" => $v["material"],
                            "surplus_sum_weight" => $v["sum_weight"]*$num,
                            "surplus_hole_number" => $v["hole_number"]*$num,
                            "workmanship" => $vv[0],
                        ];
                    }
                }else if($v[$kk]){
                    $key = $v["material"].'-'.$v["specification"].'-'.$vv[0];
                    isset($save_list[$key])?"":$save_list[$key] = $content;
                    $save_list[$key]["pt_num"] = $v["PT_Num"];
                    $save_list[$key]["stuff"] = $v["stuff"];
                    $save_list[$key]["specification"] = $v["specification"];
                    $save_list[$key]["material"] = $v["material"];
                    $save_list[$key]["workmanship"] = $vv[0];
                    $save_list[$key]["list"][] = [
                        "pt_num" => $v["PT_Num"],
                        "sum_weight" => $v["sum_weight"],
                        "hole_number" => $v["hole_number"],
                        "parts_id" => $v["parts_id"],
                        "stuff" => $v["stuff"],
                        "specification" => $v["specification"],
                        "material" => $v["material"],
                        "surplus_sum_weight" => $v["sum_weight"],
                        "surplus_hole_number" => $v["hole_number"],
                        "workmanship" => $vv[0],
                    ];
                }
            }

            $key = $v["material"].'-'.$v["specification"].'-1';
            isset($save_list[$key])?"":$save_list[$key] = $content;
            $save_list[$key]["pt_num"] = $v["PT_Num"];
            $save_list[$key]["stuff"] = $v["stuff"];
            $save_list[$key]["specification"] = $v["specification"];
            $save_list[$key]["material"] = $v["material"];
            $save_list[$key]["workmanship"] = 1;
            $flag = 0;
            if($v["stuff"]=="角钢"){
                preg_match_all("/\d+\.?\d?/",$v["specification"],$matches);
                (isset($matches[0][0]) and $matches[0][0]<50)?$flag = 1:"";
            }else if($v["stuff"]=="钢板"){
                if(in_array(abs($v["specification"]),[10,12,14])) $flag = 1;
            }
            $save_list[$key]["list"][] = [
                "pt_num" => $v["PT_Num"],
                "sum_weight" => $flag?2*$v["sum_weight"]:$v["sum_weight"],
                "hole_number" => $flag?2*$v["hole_number"]:$v["hole_number"],
                "parts_id" => $v["parts_id"],
                "stuff" => $v["stuff"],
                "specification" => $v["specification"],
                "material" => $v["material"],
                "surplus_sum_weight" =>  $flag?2*$v["sum_weight"]:$v["sum_weight"],
                "surplus_hole_number" => $flag?2*$v["hole_number"]:$v["hole_number"],
                "workmanship" => 1,
            ];
        }
        $end_save_list = [];
        foreach($save_list as $v){
            $sum_weight = $hole_number = 0;
            foreach($v["list"] as $kk=>$vv){
                $sum_weight += $vv['sum_weight'];
                $hole_number += $vv["hole_number"];
                $vv_key = $vv["parts_id"].'-'.$vv["stuff"].'-'.$vv["specification"].'-'.$vv["material"].'-'.$vv["workmanship"];
                $end_save_list[$vv_key] = $vv;
            }
            $v["surplus_sum_weight"] = $v["sum_weight"] = $sum_weight;
            $v["surplus_hole_number"] = $v["hole_number"] = $hole_number;
            unset($v["list"]);
            $v_key = $v["parts_id"].'-'.$v["stuff"].'-'.$v["specification"].'-'.$v["material"].'-'.$v["workmanship"];
            $end_save_list[$v_key] = $v;
        }
        return $end_save_list;
    }

    protected function _getMatflagType()
    {
        $data = [
            "角钢" => "01001",
            "钢板" => "01002",
            "圆钢" =>"01003",
            "钢管" =>"01004",
        ];
        return $data;
    }

    protected function _orderList()
    {
        return ["TTGX01"=>"下料","TTGX02"=>"制孔","TTGX07"=>"制弯","TTGX05"=>"焊接","TTGX04"=>"组装","TTGX06"=>"镀锌"];
    }

    protected function _machineList()
    {
        return [
            "TPP103" => "TPP103",
            "PP123M-L" => "PP123M-L",
            "PP123M-H" => "PP123M-H",
            "APM0708" => "APM0708",
            "BL1412C" => "BL1412C",
            "APM1412" => "APM1412",
            "BL2020" => "BL2020",
            "ADM3635-1" => "ADM3635-1",
            "APH2020" => "APH2020",
            "BL2532" => "BL2532",
            "ADM3635-2" => "ADM3635-2"
        ];
    }
    
    protected function _productTypeList($type=0)
    {
        if($type){
            return [
                "角钢塔"=>"角钢塔",
                '铁附件'=>'铁附件',
                "钢管杆"=>"钢管杆",
                "钢管塔"=>"钢管塔"
            ];
        }else{
            return [
                1=>"角钢塔",
                3=>'铁附件',
                4=>"钢管杆",
                5=>"钢管塔"
            ];
        }
    }

    protected function _technologyList(){
        return [
            "角钢塔" => "",
            "铁附件" => "-T",
            "钢管杆" => "-G",
            "钢管塔" => "-H"
        ];   
    }
    protected function _technologyEx(){
        return [
            "角钢塔" => "",
            "铁附件" => "tfj_",
            "钢管杆" => "ggg_",
            "钢管塔" => "ggt_"
        ];
    }
    protected function _technologyType(){
        return [
            "角钢塔" => 1,
            "铁附件" => 3,
            "钢管杆" => 4,
            "钢管塔" => 5
        ];
    }

    protected function _getBalanceModel(){
        $list = (new \app\admin\model\jichu\yw\BalanceMode())->order("BM_Num asc")->column("BM_Num,BM_Name");
        return $list;
    }
}
