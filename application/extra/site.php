<?php

return array (
  'name' => '特高压输电线路铁塔生产管控平台',
  'beian' => '',
  'cdnurl' => '',
  'vers' => 'V 1.0',
  "make" => "绍兴市捷智软件有限公司",
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' => 
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' => 
  array (
    'default' => 'Default',
    'page' => 'Page',
    'article' => 'Article',
    'test' => 'Test',
  ),
  'configgroup' => 
  array (
    'basic' => 'Basic',
    'email' => 'Email',
    'dictionary' => 'Dictionary',
    'user' => 'User',
    'example' => 'Example',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => 'smtp.qq.com',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => '10000',
  'mail_smtp_pass' => 'password',
  'mail_verify_type' => '2',
  'mail_from' => '10000@qq.com',
  'attachmentcategory' => 
  array (
    'category1' => 'Category1',
    'category2' => 'Category2',
    'custom' => 'Custom',
  ),
);
