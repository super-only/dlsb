<?php

namespace app\admin\model\chain\lofting;

use think\Model;


class PieceMain extends Model
{

    

    

    // 表名
    protected $table = 'piece_main';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    public function getTimeData($where=[],$viewWhere=[])
    {
        $data = [];
        $lastList = $this->alias("pm")
            ->join(["piece_detail"=>"pd"],"pm.id=pd.piece_id")
            ->field("SUBSTRING_INDEX(GROUP_CONCAT(pd.id ORDER BY pm.record_time),',',1) as id,pm.record_time,pm.`order`,pd.pt_num")
            ->where($where)
            ->group("pd.pt_num,pm.order")
            ->select();
        foreach($lastList as $k=>$v){
            $data[$v["pt_num"]][$v["order"]] = ["startTime"=>$v["record_time"],"endTime"=>"0000-00-00 00:00:00"];
        }
        $viewList = (new \app\admin\model\chain\lofting\PieceArrangeView())->field("pt_num,workmanship")->where($viewWhere)->where("ll_sum_weight=sj_sum_weight")->select();
        $viewData = [];
        foreach($viewList as $v){
            if($v["workmanship"] == 1) $viewData[$v["pt_num"]]["TTGX01"] = $viewData[$v["pt_num"]]["TTGX02"] = 1;
            else if($v["workmanship"] == 2) $viewData[$v["pt_num"]]["TTGX05"] = $viewData[$v["pt_num"]]["TTGX04"] = 1;
            else if($v["workmanship"] == 11) $viewData[$v["pt_num"]]["TTGX07"] = 1;
        }
        $endList = $this->alias("pm")
            ->join(["piece_detail"=>"pd"],"pm.id=pd.piece_id")
            ->field("SUBSTRING_INDEX(GROUP_CONCAT(pd.id ORDER BY pm.record_time desc),',',1) as id,pm.record_time,pm.`order`,pd.pt_num")
            ->where($where)
            ->group("pd.pt_num,pm.order")
            ->select();
        foreach($endList as $k=>$v){
            if(isset($viewData[$v["pt_num"]][$v["order"]])) $data[$v["pt_num"]][$v["order"]]["endTime"] = $v["record_time"];
        }
        return $data;


    }







}
