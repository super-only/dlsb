<?php

namespace app\admin\model\chain\lofting;

use think\Model;


class TaskSect extends Model
{

    

    

    // 表名
    protected $table = 'tasksect';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    public function sectAllocation($ids=null,$td_id=null,$nickname="")
    {
		$rows = $where = [];
        $list = (new \app\admin\model\chain\lofting\MergTypeName())->where("mTD_ID",$td_id)->select();
        foreach($list as $v){
            $rows[] = $v["TD_ID"];
        }
		if(!empty($rows)) $where["th.TD_ID"]= ["IN",$rows];
		ELSE $where["th.TD_ID"]= ["=",$td_id];
        $sectList = $this->alias("ts")
            ->join(["sectconfigdetail"=>"sd"],"sd.SCD_ID = ts.SCD_ID")
            ->join(["taskheight"=>"th"],"sd.TH_ID = th.TH_ID")
            ->field("ts.TS_Name as DtS_Name,CAST(ts.TS_Name AS UNSIGNED) as number,".$ids." as DtM_iID_FK,'".$nickname."' as DtS_sWriter,'".date("Y-m-d H:i:s")."' as DtS_dWriterTime")
            ->where($where)
            ->order("number ASC")
            ->select();
        $sectArr = [];
        foreach($sectList as $v){
            $sectArr[$v["DtS_Name"]] = $v->toArray();
            unset($sectArr[$v["DtS_Name"]]["number"]);
        }
        return $sectArr;
    }







}
