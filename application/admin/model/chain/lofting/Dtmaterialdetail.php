<?php

namespace app\admin\model\chain\lofting;

use think\Db;
use think\Model;


class Dtmaterialdetail extends Model
{

    

    

    // 表名
    protected $table = 'dtmaterialdetial';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    function __construct($data=[],$ex=''){
        $this->table = $ex."dtmaterialdetial";
        $this->setTable($this->table);
		parent::__construct($data,$ex);
	}

    public function getEndData($field,$ids,$tdId)
    {
        
        $detailList = Db::table($this->table)->alias("d")
        ->join(["pc_ncfile"=>"n"],["d.DtMD_sPartsID=n.PartName","n.TD_ID='".$tdId."'"],"left")
        ->field(implode(",",$field).",DtMD_iSectID_FK,CAST(DtMD_sPartsID AS SIGNED) as number,CASE WHEN locate('-',DtMD_sPartsID) THEN REPLACE(DtMD_sPartsID,'-','')*1000 ELSE DtMD_sPartsID END as number_1,
        case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
        SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
        when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
        when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
        when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
        when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
        else 0 end 	bjbhn,ifnull(n.datStream,'') as datStream")->where("DtMD_iSectID_FK",$ids)->order("bjbhn,number_1,number,DtMD_sPartsID ASC")->select();
        $number = $weight = $knumber = 0;
        $detailArr = $detailName = [];
        $isJudge = ["DtMD_fWidth","DtMD_iTorch","DtMD_iWelding","DtMD_iFireBending","DtMD_iCuttingAngle","DtMD_fBackOff","DtMD_iBackGouging","DtMD_DaBian","DtMD_KaiHeJiao","DtMD_ZuanKong","DtMD_iExcavationCount","DtMD_iAssembly","DtMD_ISZhuanYong"];
        foreach($detailList as $k=>$v){
            $v["DtMD_iUnitCount"] = round($v["DtMD_iUnitCount"],2);
            $detailArr[$k] = $v;
            foreach($isJudge as $vv){
                isset($v[$vv])?($detailArr[$k][$vv] = $v[$vv]?$v[$vv]:""):"";
            }
            $detailArr[$k]["is_int"] = 0;
            if(ctype_digit(''.$detailArr[$k]["DtMD_iUnitCount"]) and in_array($v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"], $detailName) == FALSE) $detailArr[$k]["is_int"] = 1;
            $detailArr[$k]["DtMD_fUnitWeight"] = round($detailArr[$k]["DtMD_fUnitWeight"],2);
            $detailArr[$k]["sumWeight"] = round($detailArr[$k]["DtMD_fUnitWeight"]*$detailArr[$k]["DtMD_iUnitCount"],2);
            $number += $detailArr[$k]["DtMD_iUnitCount"];
            $weight += round($detailArr[$k]["DtMD_fUnitWeight"]*$detailArr[$k]["DtMD_iUnitCount"],2);
            $knumber += $detailArr[$k]["DtMD_iUnitHoleCount"];
            $detailName[] = $v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"];
        }
        $count = count($detailArr);
        return [
            "detailArr" => $detailArr,
            "count" => $count,
            "number" => $number,
            "weight" => $weight,
            "knumber" => $knumber
        ];
    }







}
