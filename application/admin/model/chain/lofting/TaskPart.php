<?php

namespace app\admin\model\chain\lofting;

use think\Model;


class TaskPart extends Model
{

    

    

    // 表名
    protected $table = 'taskpart';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    public function sectAllocation($ids=null,$td_id=null,$nickname="")
    {
        $sectList = $this->alias("ts")
            ->join(["sectconfigdetail"=>"sd"],"sd.SCD_ID = ts.SCD_ID")
            ->join(["taskheight"=>"th"],"sd.TH_ID = th.TH_ID")
            ->field("ts.TS_Name as DtS_Name,CAST(ts.TS_Name AS UNSIGNED) as number,".$ids." as DtM_iID_FK,'".$nickname."' as DtS_sWriter,'".date("Y-m-d H:i:s")."' as DtS_dWriterTime")
            ->where("th.TD_ID",$td_id)
            ->group("number ASC")
            ->select();
        $sectArr = [];
        foreach($sectList as $v){
            $sectArr[$v["DtS_Name"]] = $v->toArray();
            unset($sectArr[$v["DtS_Name"]]["number"]);
        }
        return $sectArr;
    }







}
