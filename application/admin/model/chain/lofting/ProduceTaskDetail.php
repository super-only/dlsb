<?php

namespace app\admin\model\chain\lofting;

use think\Db;
use think\Model;


class ProduceTaskDetail extends Model
{

    

    

    // 表名
    protected $table = 'producetaskdetail';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];

    function __construct($data=[],$ex=''){
        $this->table = $ex."producetaskdetail";
        $this->setTable($this->table);
        $this->ex = $ex;
		parent::__construct($data,$ex);
	}
    
    public function getType($PT_Num='',$ex='')
    {
        $type = 0;
        $qkList = Db::table($this->table)->alias("ptd")
            ->join([$ex."dtmaterialdetial"=>"dtmd"],"ptd.DtMD_ID_PK = dtmd.DtMD_ID_PK")
            ->field("(case when DtMD_iWelding>0 then 1 else 0 end) as hj,(case when DtMD_iFireBending>0 then 1 else 0 end) as hq")
            ->where("ptd.PT_Num='".$PT_Num."' and (dtmd.DtMD_iWelding>0 or dtmd.DtMD_iFireBending>0)")
            ->group("hj,hq")
            ->select();
        foreach($qkList as $v){
            if($v["hj"] and $type!=3) $type = 1;
            if($v["hq"] and $type!=3) $type = 2;
            if($v["hj"] and $v["hq"]) $type = 3;
        }
        return $type;
    }
    







}
