<?php

namespace app\admin\model\chain\lofting;

use think\Db;
use think\Model;


class DhCooperateSingle extends Model
{

    

    

    // 表名
    protected $table = 'dhcooperatesingle';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    function __construct($data=[],$ex=''){
        $this->table = $ex."dhcooperatesingle";
        $this->setTable($this->table);
        $this->ex = $ex;
		parent::__construct($data,$ex);
	}
    

    public function detailList($where=[],$order=[],$ex='')
    {
        $list = Db::table($this->table)->alias("s")
        ->join([$ex."dtmaterialdetial"=>"d"],"s.DtMD_ID_PK = d.DtMD_ID_PK")
        ->field("s.DHS_ID,s.DCD_ID,s.DtMD_ID_PK,s.DHS_Count,d.DtMD_fWidth as DHS_Length,d.DtMD_iLength as DHS_Thick,s.DHS_Height,s.DHS_Grade,s.DHS_Memo,s.DHS_isM,d.DtMD_fUnitWeight,d.DtMD_sPartsID,d.DtMD_sStuff,d.DtMD_sMaterial,d.DtMD_sSpecification")
        ->where($where)
        ->order("s.DHS_ID ASC")
        ->select();
        return $list;
    }







}
