<?php

namespace app\admin\model\chain\lofting;

use think\Model;


class Library extends Model
{

    

    

    // 表名
    protected $table = 'fytxk';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];

    function __construct($data=[],$ex=''){
        $this->table = $ex."fytxk";
        $this->setTable($this->table);
		parent::__construct($data,$ex);
	}

    public function judRepete($DtM_sTypeName='',$ids=0)
    {
        if(!$DtM_sTypeName) return [0,"塔型不能为空"];
        $where = ["DtM_sTypeName"=>["=",$DtM_sTypeName]];
        if($ids) $where["DtM_iID_PK"] = ["<>",$ids];
        $one = $this->where($where)->find();
        if($one) return [0,"塔型名重复，操作失败"];
        else return [1,"成功"];
    }







}
