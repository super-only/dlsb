<?php

namespace app\admin\model\chain\material;

use think\Model;


class WorkReceiveDetail extends Model
{

    

    

    // 表名
    protected $table = 'workreceivedetail';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    public function getDetailData($ids=0,$produceWhere=[],$detailWhere=[])
    {
        if(!empty($produceWhere)){
            $produceTaskDetailModel = new \app\admin\model\chain\lofting\ProduceTaskDetail();
            $produceList = $produceTaskDetailModel->where($produceWhere)->group("PTD_Material,PTD_Specification")->column("concat(PTD_Material,',',PTD_Specification) as spec,round(sum(PTD_sumWeight)) as weight");
        }
        $detailList = $this->alias("d")
            ->join(["inventorymaterial"=>"im"],"d.IM_Num = im.IM_Num","left")
            ->join(["storestate"=>"ss"],$detailWhere,"left")
            ->field("d.WRD_ID,d.IM_Num,im.IM_PerWeight,d.IM_Class,d.IM_Spec,D_IM_Spec,d.L_Name,d.D_L_Name,
            REPLACE(d.D_L_Name,'Q','') as dlnum,
            case d.IM_Class when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when d.IM_Class='角钢' then 
            SUBSTR(REPLACE(d.D_IM_Spec,'∠',''),1,locate('*',REPLACE(d.D_IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(d.D_IM_Spec,'∠',''),locate('*',REPLACE(d.D_IM_Spec,'∠',''))+1,2)*1
            when (d.IM_Class='钢板' or d.IM_Class='法兰') then 
            REPLACE(d.D_IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh, 
            round(d.WRD_Length/1000,3) as WRD_Length,
            round(d.WRD_Width/1000,3) as WRD_Width,
            d.WRD_Unit,d.WRD_PlanCount,d.WRD_PlanWeight,d.WRD_RealCount,d.WRD_RealWeight,d.WRD_PlanTime,d.WRD_Place,d.WRD_Price,d.WRD_Memo,ss.SS_Count,ss.SS_Weight")
            ->where("d.WR_Num",$ids)
            ->where("d.IM_Class","<>",'钢板')
            ->group("d.WRD_ID")
            ->order("dlnum, bh, WRD_Length, IM_Num ASC")
            ->select();
        foreach($detailList as $k=>$v){
            $v["WRD_Length"] = round($v["WRD_Length"],3);
            $v["WRD_Width"] = round($v["WRD_Width"],2);
            $v["WRD_PlanCount"] = round($v["WRD_PlanCount"],2);
            $v["WRD_PlanWeight"] = round($v["WRD_PlanWeight"],2);
            $v["WRD_RealCount"] = round($v["WRD_RealCount"],2);
            $v["WRD_RealWeight"] = round($v["WRD_RealWeight"],2);
            $v["detailWeight"] = $produceList[$v["D_L_Name"].','.$v["D_L_Name"]]??0;
        }
        return $detailList?collection($detailList)->toArray():[];
    }







}
