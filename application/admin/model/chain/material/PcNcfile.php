<?php

namespace app\admin\model\chain\material;

use think\Model;


class PcNcfile extends Model
{

    

    

    // 表名
    protected $table = 'pc_ncfile';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    public function getFileUrl($nc_where = [])
    {
        $data = [];
        $list = $this->where($nc_where)->select();
        foreach($list as $k=>$v)
        {
            $data["".$v["PartName"]] = [
                "PartName" => $v["PartName"],
                "ID" => $v["ID"],
                "datStream" => $v["datStream"]
            ];
        }
        return $data;
    }





}
