<?php

namespace app\admin\model\chain\material;

use think\Model;


class Nesting extends Model
{

    

    

    // 表名
    protected $table = 'nesting';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    
    public function save($data = [], $where = [], $sequence = null)
    {
        $admin = \think\Session::get('admin');
        $data["writer"] = $admin["username"];
        return parent::save($data,$where,$sequence);
    }

    public function getNestingStock($where = [])
    {
        $list = $this->alias("n")
            ->join(["nesting_sub"=>"ns"],"n.id=ns.n_id")
            ->field("sum(count) as count,concat(stuff,',',material,',',specification) as value_key,length")
            ->where($where)
            ->group("stuff,material,specification,length")
            ->select();
        $data = [];
        foreach($list as $k=>$v){
            $length = round($v["length"],2);
            $data[$v["value_key"]][$length] = $v["count"];
        }
        return $data;
    }




}
