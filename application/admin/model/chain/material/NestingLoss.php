<?php

namespace app\admin\model\chain\material;

use think\Model;


class NestingLoss extends Model
{

    

    

    // 表名
    protected $table = 'nesting_loss';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    public function getLossData()
    {
        $list = $this->order("l_name asc")->select();
        $list = $list?collection($list)->toArray():[];
        return $list;
    }
    

    public function save($data = [], $where = [], $sequence = null)
    {
        $order = $this->order("order desc")->value("order");
        $data["order"] = $data["order"]?$data["order"]:$order+1;
        $data["loss"] = $data["loss"]?$data["loss"]:0;
        $data["tail"] = $data["tail"]?$data["tail"]:0;
        return parent::save($data,$where,$sequence);
    }

}
