<?php

namespace app\admin\model\chain\material;

use think\Model;


class PcScheduling extends Model
{

    

    

    // 表名
    protected $table = 'pc_scheduling';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    
    public function save($data = [], $where = [], $sequence = null)
    {
        $admin = \think\Session::get('admin');
        $data["writer"] = $admin["username"];
        return parent::save($data,$where,$sequence);
    }






}
