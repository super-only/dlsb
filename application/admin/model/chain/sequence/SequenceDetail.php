<?php

namespace app\admin\model\chain\sequence;

use think\Model;


class SequenceDetail extends Model
{

    

    

    // 表名
    protected $table = 'pc_sequence_detail';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    public function getDetailData($where)
    {
        $data = [];
        $list = $this->where($where)->select();
        foreach($list as $k=>$v){
            $data[$v["sNum"]][$v["orderId"]] = $v->toArray();
        }
        return $data;
    }
    







}
