<?php

namespace app\admin\model\chain\pack;

use think\Model;


class PrivatePack extends Model
{

    

    

    // 表名
    protected $table = 'privatepack';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    public function getPackList($where = []){
        $list = $this->alias("pp")
            ->join(["taskpart"=>"tp"],"pp.DtMD_ID_PK = tp.TP_ID")
            ->join(["dtmaterialdetial"=>"dd"],"tp.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->join(["dhcooperatesingle"=>"ds"],"pp.DCD_ID = ds.DCD_ID","LEFT")
            ->field("tp.TP_ID as DtMD_ID_PK,tp.DtMD_ID_PK as true_DtMD_ID_PK,tp.TP_SectName,tp.TP_PartsID,pp.PL_Count as TP_PackCount,round(dd.DtMD_fUnitWeight*pp.PL_Count,1) as DtMD_fWeight,dd.DtMD_iWelding as TP_Welding,dd.DtMD_iWelding as welding,(CASE WHEN pp.DCD_ID=0 THEN 0 ELSE ds.DHS_Count END) as welding_group,pp.DCD_ID,pp.PD_IsM,pp.PL_ID as id,1 as second_id,pp.SCD_ID,CAST(tp.TP_SectName AS UNSIGNED) AS number_1,CAST(tp.TP_PartsID AS UNSIGNED) AS number_2,round(dd.DtMD_fUnitWeight*pp.PL_Count,1) as 'total_weight',DtMD_sPartsID,DtMD_sStuff,DtMD_sMaterial,DtMD_sSpecification,DtMD_iLength,DtMD_fWidth,DtMD_iUnitCount,DtMD_fUnitWeight,DtMD_iUnitHoleCount,DtMD_iBoreholeCount,DtMD_iPerimeter,DtMD_iWelding,DtMD_iTorch,DtMD_iBending,DtMD_iCuttingAngle,DtMD_fBackOff,DtMD_iAperture,DtMD_iExcavationCount,DtMD_iFireBending,DtMD_iBackGouging,DtMD_iPressed,DtMD_iAssembly,DtMD_iAftertreatment,DtMD_iSectID_FK,DtMD_ZuanKong,DtMD_DaBian,TP_SectName as DtS_Name")
            ->where($where)
            ->group("id")
            ->order("pp.DCD_ID,pp.PD_IsM DESC,number_1,tp.TP_SectName,number_2,tp.TP_PartsID")
            ->select();
        return $list;
    }
    







}
