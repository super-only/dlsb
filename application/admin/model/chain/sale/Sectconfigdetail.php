<?php

namespace app\admin\model\chain\sale;

use PDO;
use think\Model;

class Sectconfigdetail extends Model
{
    // 表名
    protected $table = 'sectconfigdetail';

    public function getScdIdList($where=[]){
        $list = $this->alias("sect")
            ->join(["taskheight"=>"th"],"sect.TH_ID = th.TH_ID")
            ->field("sect.SCD_ID")
            ->where($where)
            ->select();
        $data = [];
        foreach($list as $v){
            $data[] = $v["SCD_ID"];
        }
        return $data;
    }
    
}
