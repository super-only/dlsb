<?php

namespace app\admin\model\chain\machine;

use think\Model;


class WjInventoryRecord extends Model
{

    

    

    // 表名
    protected $table = 'wjinventoryrecord';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    public function getMaterial()
    {
        $data = [];
        $list = $this->alias("a")->join(["wjinventorysort"=>"s"],"a.sortNum = s.IS_Num")->field("IS_Name,IR_Num,IR_Name,IR_Spec,IR_Unit,IR_Unit,IR_TaxRate")->order("IR_Num asc")->select();
        foreach($list as $v){
            $data[$v["IR_Num"]] = $v->toArray();
        }
        return $data;
    }

}
