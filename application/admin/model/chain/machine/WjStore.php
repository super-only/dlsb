<?php

namespace app\admin\model\chain\machine;

use think\Model;


class WjStore extends Model
{

    

    

    // 表名
    protected $table = 'wjstore';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    public function getPrice($where)
    {
        $data = [];
        $list = $this->field("IR_Num,S_Price")->where($where)->order("IR_Num asc")->select();
        foreach($list as $v){
            $data[$v["IR_Num"]] = round($v["S_Price"],3);
        }
        return $data;
    }

}
