<?php

namespace app\admin\model\quality\process;

use think\Model;


class GalvanizedTrue extends Model
{

    

    

    // 表名
    protected $table = 'galvanized_true';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    public function saveRange($row=null)
    {
        if(!$row) return [];
        $tdId = (new \app\admin\model\chain\lofting\ProduceTask())->where("PT_Num",$row["PT_Num"])->value("TD_ID");
        $time = (new \app\admin\model\chain\pack\Pack())->where("TD_ID",$tdId)->order("P_ID")->value("WriteDate");
        $time = strtotime("-2 day",strtotime($time));
        $base_temp = rand(433,439);
        $list = [];
        if($time){
            for($i=10;$i>0;$i--){
                $list[] = [
                    "ppm_id" => $row["id"],
                    "time" => date("Y-m-d H:i:s",$time-30*60*$i),
                    "temp"=> $base_temp+round((rand(0,100)/100),2)
                ];
            }
        }
        $result = $this->saveAll($list);
        return $result;
    }
    







}
