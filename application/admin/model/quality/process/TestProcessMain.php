<?php

namespace app\admin\model\quality\process;

use think\Model;


class TestProcessMain extends Model
{

    

    

    // 表名
    protected $table = 'test_process_main';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    
    public function save($data = [], $where = [], $sequence = null)
    {
        $admin = \think\Session::get('admin');
        $data["writer"] = $admin["username"];
        return parent::save($data,$where,$sequence);
    }
    







}
