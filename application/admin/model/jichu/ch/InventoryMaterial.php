<?php

namespace app\admin\model\jichu\ch;

use think\Model;


class InventoryMaterial extends Model
{

    

    

    // 表名
    protected $table = 'InventoryMaterial';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    public function getIMPerWeight(){
		$where = [
			"IM_Class" => ["IN",["法兰","钢板","角钢","槽钢"]]
		];
		$list = $this->field("IM_Spec,IM_PerWeight,IM_Class")->where($where)->select();
		$data = [];
		foreach($list as $v){
			if($v["IM_Class"] == "法兰") $v["IM_PerWeight"] = str_replace("φ","Φ",$v["IM_PerWeight"]);
			$data[$v["IM_Class"]][$v["IM_Spec"]] = $v["IM_PerWeight"];
		}
		return $data;
	}







}
