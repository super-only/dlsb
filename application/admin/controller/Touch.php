<?php

namespace app\admin\controller;

use think\Controller;




/**
 * 套料
 */
class Touch
{
    
    protected $mateList=[];
    protected $ck=[];
    protected $oldsl=[];
    protected $flag=0;
    protected $newcz="";
    protected $newgg="";
    protected $cnt=0;
    protected $newlyl=0;
    protected $zsl=0;
    protected $jl=[];
    protected $res=[];
    // public function __construct(
    //     //材料数组[部件号,材质,规格,长度,数量]
    //     $mateList = [],
    //     //该材料的库存量
    //     $ck = []
    // )
    // {
        
    //     $this->mateList = $this->arraySort($mateList, 3);
    //     $this->ck = $ck;
    // }
    
    public function main($mateList,$ck)
    {
        $this->oldsl = $this->jl = $this->res = [];
        $this->flag = $this->cnt = $this->newlyl = $this->zsl = 0;
        $this->newcz = $this->newgg = "";
        $this->ck = $ck;
        $this->mateList = $this->arraySort($mateList, 3);
        for($i = 0 ; $i < count($this->mateList) ; $i++) {
            $this->oldsl[$i] = $this->mateList[$i][4];
        }
        //BUG首位减不到0
        // $this->mateList[0][4]++;
        // $this->oldsl[0]++;

        $this->newcz = $this->mateList[0][1];
        $this->newgg = $this->mateList[0][2];
        $this->zsl = count($this->mateList);
    // }

    // public function main()
    // {
        

        for($i = 1 ; $i <= 1000 , $this->flag == 0; $i++){
            $this->newlyl = 0.98;
            $this->flag = 1;
            while($this->flag == 1) {
                $this->pd(0,0,1,0,0,-1);
                $this->newlyl-=0.01;
                if($this->newlyl<0.10){
                    break;
                } 
            }
            // if(flag == 1) {
            //     cout<<"无法匹配到结果"<<endl; 
            //     cout << "部件号 材质 规格 长度 数量" << endl;
            //     for(int i = 0 ; i < zsl ; i++) {
            //         if(newgg == cs[i].gg && newcz == cs[i].cz)
            //             cout << cs[i].bjh << " " << cs[i].cz << " " << cs[i].gg << " " << cs[i].cd << " " << cs[i].sl << " " << endl;
            //     }
            // }
        }
        // pri($this->res,$this->mateList,1);
        return $this->res;
    }
    /**
	 * 具体套料
	 * @param  $x 当前位置
	 * @param  $y 当前值
	 * @param  $ans 刀片数量
     * @param  $pdqg 判断有没有取过
	 * @param  $pdsl 判断取的数量有没有超过总量
	 * @param  $tp 特判200的料 
	 * @return 
	 */
    public function pd($x,$y,$ans,$pdqg,$pdsl,$tp)
    {
        
        if ($x >= $this->zsl || max($this->ck) <= $y || $pdqg > 4 || !$this->flag || $pdsl > $this->mateList[$x][3]){
            // var_dump($this->res);
            return false;
        }
        $sxczValue = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[6];
        if($sxczValue != -1){// && shl < 1.00 && yl >= wbsy(newgg)
            $this->flag = 0;
            //超过数量进行回退 
            for($i = 1;$i < $ans;$i++){//可能浪费时间，但是影响不大 
                $this->mateList[$this->weizhi($this->jl[$i]["zhi"])][4]--;
                //治标不治本，尝试新开一个变量（判断当前和之前是否一样） 
                if($this->mateList[$this->weizhi($this->jl[$i]["zhi"])][4]==0) {
                    for($j=1;$j<=$i;$j++){
                        $this->mateList[$this->weizhi($this->jl[$i]["zhi"])][4]++;
                    }
                    $this->flag = 1;
                    break;
                }
            }
            if($this->flag==1) return false;
            // pri($this->oldsl,$this->mateList[$i][3],1);
            // $fl = 1; 
            for($i = 0 ;$i <$this->zsl ;$i++) {
                if($this->oldsl[$i] != $this->mateList[$i][4]) {
                    // if(!$fl) {
                    //     $this->res[$this->cnt]['ppjg'] .= " + ";
                    // }
                    // $this->res[$this->cnt]['ppjg'] = $this->res[$this->cnt]['ppjg'] + " / " + $this->mateList[$i][3] + " * " + ($this->oldsl[$i] - $this->mateList[$i][4]);
                    // cs[i].bjh << " / " << cs[i].cd << " * " << (oldsl[i] - cs[i].sl)
                    // pri($this->mateList[$i][3],1);
                    if(!isset($this->res[$this->cnt]['ppjg'])) $this->res[$this->cnt]['ppjg'] = "";
                    $this->res[$this->cnt]['ppjg'] .= '+'.$this->mateList[$i][0] . "/" . $this->mateList[$i][3] . "*" . ($this->oldsl[$i] - $this->mateList[$i][4]);
                    // $fl = 0;
                    $this->oldsl[$i] = $this->mateList[$i][4];
                }
            }
            //存储数组 
            $this->res[$this->cnt]['cz'] = $this->newcz;
            $this->res[$this->cnt]['gg'] = $this->newgg;
            $this->res[$this->cnt]['cd'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[6];
            $this->res[$this->cnt]['ppjg'] = trim($this->res[$this->cnt]['ppjg'],"+");
            $this->res[$this->cnt]['dssh'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[0];
            $this->res[$this->cnt]['sycd'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[2];
            $this->res[$this->cnt]['yl'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[3];
            $this->res[$this->cnt]['sh'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[4];
            $this->res[$this->cnt++]['shl'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[5];
            // pri($this->res,1);
            // //结果展示 
            // printf("选择的材质%d\n",sxcz(ans,y,newgg,newlyl).ggcz);
            // printf("刀数损耗%d\n",dssh);
            // printf("使用长度%d\n",sycd);
            // printf("余量%d\n",yl);
            // printf("损耗%d\n",sh);
            // printf("损耗率%.5f\n",shl);
            // printf("利用率%.5f\n",lyl); 
            // $this->res[] = [

            // ];
        }
        if($this->mateList[$x][4] > $pdsl && $this->mateList[$x][2] == $this->newgg && $this->mateList[$x][1] == $this->newcz && ($tp == -1 || $this->mateList[$x][3] > 1500 && $tp == 2 || $this->mateList[$x][3] <= 1500 && $tp == 1)) {//newcd > y + $this->mateList[x].cd && 
            $this->jl[$ans]["zhi"] = $this->mateList[$x][3];
            if($this->pdtp($this->mateList[$x][2]) >= 200 && $this->pdtp($this->mateList[$x][2]) <= 300)
                $tp = ($this->mateList[$x][3] > 1500) ? 2 : 1;
            // pri($this->jl,$ans,$this->mateList,$x,1);
            if($this->jl[$ans-1]["zhi"]??0 != $this->mateList[$x][3]){
                $this->pd($x,$y+$this->mateList[$x][3],$ans+1,$pdqg+1,$pdsl+1,$tp);
            } else {
                $this->pd($x,$y+$this->mateList[$x][3],$ans+1,$pdqg,$pdsl+1,$tp);
            }
        }
        $this->pd($x+1,$y,$ans,$pdqg,0,$tp);
    }

    
    //判断规格计算切割损耗 
    public function qgsh($mp="")
    {
        preg_match_all("/\d+\.?\d*/",$mp,$matches);
        $x = $matches[0][0]?((float)$matches[0][0]):0;
        $y = $matches[0][1]?((float)$matches[0][1]):0;
        if($x >= 40 && $x <= 45){
            return 15;
        }
        if($x >= 50 && $x <= 63){
            return 0;
        }
        if($x >= 70 && $x <= 80){
            return 15;
        }
        if($x >= 90 && $x <= 100 && $y <= 8){
            return 0;
        }
        if($x >= 90 && $x <= 180 && $y <= 12){
            return 25;
        }
        if($x >= 125 && $x <= 200 && $y > 12){
            return 5;
        }
        if($x >= 200 && $x <= 250){
            return 5;
        }
        return 0;
    }

    //判断规格返回相应的尾部剩余
    public function wbsy($mp="")
    {
        preg_match_all("/\d+\.?\d*/",$mp,$matches);
        $x = $matches[0][0]??0;
        $y = $matches[0][1]??0;
        if($x >= 50 && $x <= 63){
            return 100;
        }
        if($x >= 70 && $x <= 80){
            return 100;
        }
        if($x >= 90 && $x <= 100 && $y <= 8){
            return 100;
        }
        if($x >= 90 && $x <= 180 && $y <= 12){
            return 100;
        }
        return 0;
    }

    //截取规格的角度 
    public function pdtp($mp="")
    {
        preg_match_all("/\d+\.?\d*/",$mp,$matches);
        $x = $matches[0][0]??0;
        return $x;
    }

    //查询当前值的位置
    public function weizhi($x)
    {
        for($i = 0;$i < $this->zsl;$i++) {
            if($x == $this->mateList[$i][3] && $this->mateList[$i][4] > 0) {
                return $i;
            }
        }
        return 0;
    }
    

    /**
	 * 筛选材质 
	 * @param  $ans 
	 * @param  $y
     * @param  $pdgg
	 * @param  $pdlyl
	 * @return 
	 */
    public function sxcz($ans,$y,$pdgg='',$pdlyl)
    {
        $dssh = $ans * $this->qgsh($pdgg);
        $sycd = $dssh + $y;
        
        for($i = 0; $i < count($this->ck) ; $i++) {
            $yl = $this->ck[$i] - $sycd;
            $sh = $dssh + $yl;
            $shl = $sh * 1.0 / $this->ck[$i] * 1.0;
            $lyl = 1.0 - $shl;
            if($lyl > $pdlyl && $shl < 1.0 && $yl >= $this->wbsy($pdgg)) {
                return [$dssh,$sycd,$yl,$sh,$shl,$lyl,$this->ck[$i]];
            }
        }
        return [$dssh,$sycd,$yl,$sh,$shl,$lyl,-1];
    }

    /**
	 * 二维数组某一项排序
	 * @param  $array
	 * @param  $keys
	 * @param  $sort
	 * @return 
	 */
	public function arraySort($array, $keys, $sort = SORT_DESC) {
	    $keysValue = [];
	    foreach ($array as $k => $v) {
	        $keysValue[$k] = $v[$keys];
	    }
	    array_multisort($keysValue, $sort, $array);
	    return $array;
	}
}
