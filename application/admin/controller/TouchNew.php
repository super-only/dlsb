<?php

namespace app\admin\controller;

use PDO;
use think\Controller;




/**
 * 套料
 */
class TouchNew
{
    //刀数 刀+1
    //刀数损耗 单刀损耗*刀数
    //部件使用长度 部件长度*数量
    //余量 单根长度-刀数损耗-部件使用长度
    //损耗 单根长度-部件使用长度
    //利用率(%) 损耗/总长*100
    //损耗率(%) 100-利用率
    public function main($mateList,$kcList)
    {
        
        // pri($kcList,'');
        $partList = [];
        if(empty($mateList) or empty($kcList)) return $partList;
        $spec = $mateList[0][4];
        $material = $mateList[0][3];
        preg_match_all("/\d+\.?\d*/",$spec,$matches);
        $x = $matches[0][0]?((float)$matches[0][0]):0;
        $y = $matches[0][1]?((float)$matches[0][1]):0;
        [$ds,$wb] = shAndWbData($x,$y,$material);
        while(!empty($mateList) and !empty($kcList)){
            krsort($kcList);
            $mateList = arraySort($mateList, 1, SORT_DESC);
            $mateList = array_values($mateList);
            $bcList = $this->_getPart($mateList,$kcList,$ds,$wb);
            // pri($bcList,1);
            if(!empty($bcList)){
                $bcList["material"] = $material;
                $bcList["spec"] = $spec;
                $partList[] = $bcList;
                foreach($bcList["part"] as $pv){
                    $mateList[$pv[0]][2] -= $pv[1];
                    if($mateList[$pv[0]][2]==0) unset($mateList[$pv[0]]);
                }
                // pri($bcList,$kcList,'');
                $kcList[$bcList['nextClosest']]--;
                if($kcList[$bcList['nextClosest']]==0) unset($kcList[$bcList['nextClosest']]);
            }
        }
        // pri($mateList,$kcList,1);
        return $partList;
    }

    protected function _getPart($mateList,$kcList,$ds,$wb)
    {
        if(empty($mateList) or empty($kcList)) return [];
        // foreach($mateList as $k=>$v){
            $k = 0;
            $v = $mateList[0];
            $cbList = [];
            for($i=$v[2];$i>0;$i--){
                $sjds = ($i+1);
                $dssh = $sjds*$ds;
                $bjsycd = $v[1]*$i;
                $length = $bjsycd+$dssh+$wb;
                $nextClosest = $this->getClosest($length,array_keys($kcList));
                if($nextClosest){
                    $sh = $nextClosest-$bjsycd;
                    $shv = round($sh/$nextClosest*100,2);
                    $lyv = 100-$shv;
                    $list = [
                        "mate" => $v[0].'/'.$v[1]."*".$i,
                        "part" => [[$k,$i]],
                        "sjds" => $sjds,
                        "dssh" => $dssh,
                        "bjsycd" => $bjsycd,
                        "length" => $length,
                        "nextClosest" => $nextClosest,
                        "sh" => $sh,
                        "shv" =>$shv,
                        "lyv" => $lyv
                    ];
                    if(empty($cbList)){
                        $cbList = $list;
                    }else if($lyv>$cbList["lyv"]){
                        $cbList = $list;
                    }
                    if($cbList["lyv"]>=97) return $cbList;
                    else{
                        foreach($mateList as $sk=>$sv){
                            if($sk==$k) continue;
                            for($si=$sv[2];$si>0;$si--){
                                $sjds = 1+$i+$si;
                                $dssh = $sjds*$ds;
                                $bjsycd = $v[1]*$i+$sv[1]*$si;
                                $length = $bjsycd+$dssh+$wb;
                                $nextClosest = $this->getClosest($length,array_keys($kcList));
                                if($nextClosest){
                                    $sh = $nextClosest-$bjsycd;
                                    $shv = round($sh/$nextClosest*100,2);
                                    $lyv = 100-$shv;
                                    $list = [
                                        "mate" => $v[0].'/'.$v[1]."*".$i."+".$sv[0].'/'.$sv[1]."*".$si,
                                        "part" => [[$k,$i],[$sk,$si]],
                                        "sjds" => $sjds,
                                        "dssh" => $dssh,
                                        "bjsycd" => $bjsycd,
                                        "length" => $length,
                                        "nextClosest" => $nextClosest,
                                        "sh" => $sh,
                                        "shv" =>$shv,
                                        "lyv" => $lyv
                                    ];
                                    if(empty($cbList)){
                                        $cbList = $list;
                                    }else if($lyv>$cbList["lyv"]){
                                        $cbList = $list;
                                    }
                                    if($cbList["lyv"]>=97) return $cbList;
                                    else{
                                        foreach($mateList as $tk=>$tv){
                                            if($tk==$k or $tk==$sk) continue;
                                            for($ti=$tv[2];$ti>0;$ti--){
                                                $sjds = 1+$i+$si+$ti;
                                                $dssh = $sjds*$ds;
                                                $bjsycd = $v[1]*$i+$sv[1]*$si+$tv[1]*$ti;
                                                $length = $bjsycd+$dssh+$wb;
                                                $nextClosest = $this->getClosest($length,array_keys($kcList));
                                                if($nextClosest){
                                                    $sh = $nextClosest-$bjsycd;
                                                    $shv = round($sh/$nextClosest*100,2);
                                                    $lyv = 100-$shv;
                                                    $list = [
                                                        "mate" => $v[0].'/'.$v[1]."*".$i."+".$sv[0].'/'.$sv[1]."*".$si."+".$tv[0].'/'.$tv[1]."*".$ti,
                                                        "part" => [[$k,$i],[$sk,$si],[$tk,$ti]],
                                                        "sjds" => $sjds,
                                                        "dssh" => $dssh,
                                                        "bjsycd" => $bjsycd,
                                                        "length" => $length,
                                                        "nextClosest" => $nextClosest,
                                                        "sh" => $sh,
                                                        "shv" =>$shv,
                                                        "lyv" => $lyv
                                                    ];
                                                    if(empty($cbList)){
                                                        $cbList = $list;
                                                    }else if($lyv>$cbList["lyv"]){
                                                        $cbList = $list;
                                                    }
                                                    if($cbList["lyv"]>=97) return $cbList;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    foreach($mateList as $sk=>$sv){
                        if($sk==$k) continue;
                        for($si=$sv[2];$si>0;$si--){
                            $sjds = 1+$i+$si;
                            $dssh = $sjds*$ds;
                            $bjsycd = $v[1]*$i+$sv[1]*$si;
                            $length = $bjsycd+$dssh+$wb;
                            $nextClosest = $this->getClosest($length,array_keys($kcList));
                            if($nextClosest){
                                $sh = $nextClosest-$bjsycd;
                                $shv = round($sh/$nextClosest*100,2);
                                $lyv = 100-$shv;
                                $list = [
                                    "mate" => $v[0].'/'.$v[1]."*".$i."+".$sv[0].'/'.$sv[1]."*".$si,
                                    "part" => [[$k,$i],[$sk,$si]],
                                    "sjds" => $sjds,
                                    "dssh" => $dssh,
                                    "bjsycd" => $bjsycd,
                                    "length" => $length,
                                    "nextClosest" => $nextClosest,
                                    "sh" => $sh,
                                    "shv" =>$shv,
                                    "lyv" => $lyv
                                ];
                                if(empty($cbList)){
                                    $cbList = $list;
                                }else if($lyv>$cbList["lyv"]){
                                    $cbList = $list;
                                }
                                if($cbList["lyv"]>=97) return $cbList;
                                else{
                                    foreach($mateList as $tk=>$tv){
                                        if($tk==$k or $tk==$sk) continue;
                                        for($ti=$tv[2];$ti>0;$ti--){
                                            $sjds = 1+$i+$si+$ti;
                                            $dssh = $sjds*$ds;
                                            $bjsycd = $v[1]*$i+$sv[1]*$si+$tv[1]*$ti;
                                            $length = $bjsycd+$dssh+$wb;
                                            $nextClosest = $this->getClosest($length,array_keys($kcList));
                                            if($nextClosest){
                                                $sh = $nextClosest-$bjsycd;
                                                $shv = round($sh/$nextClosest*100,2);
                                                $lyv = 100-$shv;
                                                $list = [
                                                    "mate" => $v[0].'/'.$v[1]."*".$i."+".$sv[0].'/'.$sv[1]."*".$si."+".$tv[0].'/'.$tv[1]."*".$ti,
                                                    "part" => [[$k,$i],[$sk,$si],[$tk,$ti]],
                                                    "sjds" => $sjds,
                                                    "dssh" => $dssh,
                                                    "bjsycd" => $bjsycd,
                                                    "length" => $length,
                                                    "nextClosest" => $nextClosest,
                                                    "sh" => $sh,
                                                    "shv" =>$shv,
                                                    "lyv" => $lyv
                                                ];
                                                if(empty($cbList)){
                                                    $cbList = $list;
                                                }else if($lyv>$cbList["lyv"]){
                                                    $cbList = $list;
                                                }
                                                if($cbList["lyv"]>=97) return $cbList;
                                            }
                                        }
                                    }
                                }
                            }
                            // }else{
                            //     foreach($mateList as $tk=>$tv){
                            //         if($tk==$k or $tk==$sk) continue;
                            //         for($ti=$tv[2];$ti>0;$ti--){
                            //             $sjds = 1+$i+$si+$ti;
                            //             $dssh = $sjds*$ds;
                            //             $bjsycd = $v[1]*$i+$sv[1]*$si+$tv[1]*$ti;
                            //             $length = $bjsycd+$dssh+$wb;
                            //             $nextClosest = $this->getClosest($length,array_keys($kcList));
                            //             if($nextClosest){
                            //                 $sh = $nextClosest-$bjsycd;
                            //                 $shv = round($sh/$nextClosest*100,2);
                            //                 $lyv = 100-$shv;
                            //                 $list = [
                            //                     "mate" => $v[0].'/'.$v[1]."*".$i."+".$sv[0].'/'.$sv[1]."*".$si."+".$tv[0].'/'.$tv[1]."*".$ti,
                            //                     "part" => [[$k,$i],[$sk,$si],[$tk,$ti]],
                            //                     "sjds" => $sjds,
                            //                     "dssh" => $dssh,
                            //                     "bjsycd" => $bjsycd,
                            //                     "length" => $length,
                            //                     "nextClosest" => $nextClosest,
                            //                     "sh" => $sh,
                            //                     "shv" =>$shv,
                            //                     "lyv" => $lyv
                            //                 ];
                            //                 if(empty($cbList)){
                            //                     $cbList = $list;
                            //                 }else if($lyv>$cbList["lyv"]){
                            //                     $cbList = $list;
                            //                 }
                            //                 if($cbList["lyv"]>=97) return $cbList;
                            //             }
                            //         }
                            //     }
                            // }
                        }
                    }
                }
            }
            return $cbList;
        // }
    }

    public function getClosest($search, $arr) {
        $closest = 0;
        foreach ($arr as $item) {
           if ((!$closest || abs($search - $closest) > abs($item - $search)) and (($item - $search)>0)){
              $closest = $item;
           }
        }
        return $closest;
     }
}
