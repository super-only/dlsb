<?php

namespace app\admin\controller;

// use app\admin\model\AuthRule;
use app\common\controller\Backend;
use think\Session;

/**
 * 后台首页
 * @internal
 */
class Technology extends Backend
{

    protected $noNeedLogin = ["*"];
    //角钢塔 '' 铁附件-T 钢管杆-G 钢管塔-H
    protected $technology_field = '角钢塔';
    protected $technology_type = 1;
    protected $technology_ex = "";
    protected $technology_xd = "";
    protected $technologyList = [
        "角钢塔" => "",
        "铁附件" => "-T",
        "钢管杆" => "-G",
        "钢管塔" => "-H"
    ];
    protected $technologyEx = [
        "角钢塔" => "",
        "铁附件" => "tfj_",
        "钢管杆" => "ggg_",
        "钢管塔" => "ggt_"
    ];
    protected $technologyType = [
        "角钢塔" => 1,
        "铁附件" => 3,
        "钢管杆" => 4,
        "钢管塔" => 5
    ];

    public function _initialize()
    {
        parent::_initialize();
        //移除HTML标签
        $this->request->filter('trim,strip_tags,htmlspecialchars');
    }

    public function changeTechnology()
    {
        $technology_field = $this->request->post("technology");
        if(!$technology_field) return json(["code"=>0,"msg"=>"有误，请重新选择"]);
        $technology_ex = $this->technologyEx[$technology_field];
        $technology_xd = $this->technologyList[$technology_field];
        $technology_type = $this->technologyType[$technology_field];
        Session::set('technology_field',$technology_field);
        Session::set('technology_ex',$technology_ex);
        Session::set('technology_xd',$technology_xd);
        Session::set('technology_type',$technology_type);
        // $authRuleModel = new AuthRule();
        // $list = $authRuleModel->where("pid",363)->column("id");
        // $pidList = [363];
        // foreach($list as $k=>$v){
        //     $pidList[] = $v;
        // }
        // $endList = $authRuleModel->where("pid","IN",$pidList)->column("id");
        // $this->technology_field = $technology_field;
        // $this->technology_ex = $technology_ex;
        // $this->technology_xd = $technology_xd;
        // pri($this,1);
        return json(["code"=>1,"msg"=>"成功"]);
    }

}
