<?php

namespace app\admin\controller\jichu\jg;

use app\common\controller\Backend;
use think\Validate;

/**
 * 部门档案
 *
 * @icon fa fa-circle-o
 */
class Deptdetail extends Backend
{
    
    /**
     * Deptdetail模型对象
     * @var \app\admin\model\jichu\jg\Deptdetail
     */
    protected $model = null;
    protected $deptList;
    protected $rule =   [
        'DD_Num'  => 'require',
        'DD_Name'   => 'require' 
    ];
    
    protected $message  =   [
        'DD_Num.require' => 'DD_Num必填',
        'DD_Name.require'     => '部门名称必填'
    ];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\jg\Deptdetail;
        $list = $this->deptType();
        $this->deptList = $list;
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $build = $this->model;
            $list = $build
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["ParentNum"] = $this->treeList[$v["ParentNum"]]["text"]??"";
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        $this->view->assign('treeList',$this->deptList[1]);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $result = false;
                    $result = $this->model->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were inserted'));
                    }
                }else{
                    $this->error($validate->getError());
                }
                
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = $this->model->field("DD_Num")->where("ParentNum","")->order("DD_Num DESC")->find();
        $DD_Num = $row?$row["DD_Num"]:0;
        $num = $DD_Num?"0".(++$DD_Num):'01';
        $deptlist = [""=>"请选择"];
        $deptlist = array_merge($deptlist,$this->deptList[0]);
        $this->view->assign('DD_Num',$num);
        $this->view->assign('deptType',$deptlist);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $result = false;
                    $result = $row->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were updated'));
                    }
                }else{
                    $this->error($validate->getError());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row["ParentName"] = $this->deptList[0][$row["ParentNum"]]??"无所属部门";
        $deptlist = [""=>"请选择"];
        array_merge($deptlist,$this->deptList[0]);
        $this->view->assign("row", $row);
        $this->view->assign('deptType',$deptlist);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();
            $empList = (new \app\admin\model\jichu\jg\Employee())->distinct(true)->field($pk)->where($pk, 'in', $ids)->select();
            $empArr = [];
            foreach($empList as $k=>$v){
                $empArr[] = $v[$pk];
            }
            $count = 0;
            foreach ($list as $v) {
                if(!in_array($v[$pk],$empArr)) $count += $v->delete();
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }


    /**
     * 读取公司树
     *
     * @internal
     */
    public function companytree()
    {
        $list = array_values($this->deptList[1]);
        return json($list);
    }

    /**
     * 获取编码
     */
    public function stockTypeCode()
    {
        $num = $this->request->post("num");
        $row = [];
        if($num){
            $where = [
                "ParentNum" => ["<>",$num],
                "ParentNum" => ["LIKE",$num."%"]
            ];
            $row = $this->model->field("DD_Num")->where($where)->order("DD_Num DESC")->find();
        }else{
            $row = $this->model->field("DD_Num")->where("ParentNum","")->order("DD_Num DESC")->find();
        }
        $DD_Num = $row?$row["DD_Num"]:0;
        $value = $DD_Num?"0".(++$DD_Num):$num.'01';
        return json(["code"=>1, "data"=>["DD_Num"=>$value], "msg"=>"success"]);
    }
}
