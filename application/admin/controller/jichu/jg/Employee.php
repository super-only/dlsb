<?php

namespace app\admin\controller\jichu\jg;

use app\common\controller\Backend;
use \Yurun\Util\Chinese;
use \Yurun\Util\Chinese\Pinyin;
use think\Validate;
use jianyan\excel\Excel;
// use think\Db;
// use think\exception\PDOException;
// use think\exception\ValidateException;
// use Exception;

/**
 * 员工信息
 *
 * @icon fa fa-circle-o
 */
class Employee extends Backend
{
    
    /**
     * Employee模型对象
     * @var \app\admin\model\jichu\jg\Employee
     */
    protected $model = null;
    protected $tree;
    protected $roleArr = [0=>"超级管理员",1=>"一般用户"];
    protected $sexArr = [0=>"女",1=>"男"];
    protected $stateArr = [0=>"离职",1=>"在职",2=>"开除",3=>"辞职"];
    protected $workArr = ["干部"=>"干部","固定工"=>"固定工","城镇合同"=>"城镇合同","农民合同"=>"农民合同","临时工"=>"临时工","季节工"=>"季节工","聘用工"=>"聘用工","全民职工"=>"全民职工","长期劳务派遣"=>"长期劳务派遣","短期劳务派遣"=>"短期劳务派遣","其他"=>"其他"];
    protected $residenceArr = ["城镇"=>"城镇","农村"=>"农村","非农"=>"非农"];
    protected $placeArr = ["本地"=>"本地","外地"=>"外地"];
    protected $marryArr = ["未婚"=>"未婚","已婚"=>"已婚","离异"=>"离异"];
    protected $policeArr = ["党员"=>"党员","团员"=>"团员","群众"=>"群众"];
    protected $studyArr = ["博士"=>"博士","研究生"=>"研究生","本科"=>"本科","大专"=>"大专","职高"=>"职高","中专"=>"中专","中技"=>"中技","技校"=>"技校","高中"=>"高中","初中"=>"初中","小学"=>"小学"];
    protected $companyModel = null;
    protected $companyList,$company;
    protected $rule =   [
        'E_Num'  => 'require',
        'DD_Num'  => 'require',
        'E_Name'  => 'require',
        'E_Sex'  => 'require'
    ];
    protected $message  =   [
        'E_Num.require'  => '员工编码必填',
        'DD_Name.require'  => '所在部门必选',
        'E_Name.require'  => '姓名必填',
        'E_Sex.require'  => '性别必填'
    ];
    protected $noNeedLogin = ["selectCrif"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\jg\Employee;
        $this->companyModel = new \app\admin\model\jichu\jg\Deptdetail;
        $companyArr = $this->companyModel->where("Valid",1)->field("DD_Num,DD_Name,ParentNum")->order("ParentNum,DD_Num ASC")->select();
        foreach($companyArr as $v){
            $this->companyList[$v["DD_Num"]] = [
                "id" => $v["DD_Num"],
                "name" => $v["DD_Name"],
                "title" => ($v["ParentNum"] and isset($this->companyList[$v["ParentNum"]]))?$this->companyList[$v["ParentNum"]]["title"].'-'.$v["DD_Name"]:$v["DD_Name"]
            ];
        }
        foreach($companyArr as $k=>$v){
            foreach($companyArr as $kk=>$vv){
                if($v["DD_Num"]==$vv["ParentNum"]) unset($companyArr[$k]);
            }
            if(isset($companyArr[$k])) $this->company[$v["DD_Num"]] = $this->companyList[$v["DD_Num"]]["title"];
        }
        $this->view->assign("roleArr",$this->roleArr);
        $this->view->assign("sexArr",$this->sexArr);
        $this->view->assign("stateArr",$this->stateArr);
        $this->view->assign("workArr",$this->workArr);
        $this->view->assign("residenceArr",$this->residenceArr);
        $this->view->assign("placeArr",$this->placeArr);
        $this->view->assign("marrayArr",$this->marryArr);
        $this->view->assign("policeArr",$this->policeArr);
        $this->view->assign("studyArr",$this->studyArr);
        $company = [""=>"请选择"];
        $company = array_merge($company,$this->company);
        $this->view->assign("company",$company);
        $this->assignconfig("company",$this->company);
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    public function import()
    {
        parent::import();
    }

     /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $this->tree = $this->request->get("tree", '');
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
                
            $rows = $list->items();
            foreach($rows as $k=>$v){
                foreach($v as $kk=>$vv){
                    if(in_array($kk,["E_EnterTime","e_workdate","E_Birthday","E_Formal","E_LeaveTime"])) $rows[$k][$kk] = $vv=="1900-01-01 00:00:00"?"":$vv;
                }
                unset($vv,$kk);
                switch ($v["Valid"])
                {
                    case 1:
                        $year = time()-strtotime($v["E_EnterTime"]);
                        break;
                    default:
                        $year = $v['E_LeaveTime']=="1900-01-01 00:00:00"?(time()-strtotime($v["E_EnterTime"])):(strtotime($v["E_LeaveTime"])-strtotime($v["E_EnterTime"]));
                }
                $rows[$k]["py"] = strtoupper(implode('',Chinese::toPinyin($v["E_Name"], Pinyin::CONVERT_MODE_PINYIN_FIRST)['pinyinFirst'][0]));
                $rows[$k]["year"] = diffDay($year);
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $params["E_PerNum"] = $params["E_Name"].$params["E_Num"];
                    $is_exit = $this->model->where("E_PerNum",$params["E_PerNum"])->find();
                    if($is_exit) $this->error('该员工编号已存在');
                    $result = false;
                    $result = $this->model->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were inserted'));
                    }
                }else{
                    $this->error($validate->getError());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $E_Num = "";
        $key = key($this->company);
        $maxNum = $this->model->field("E_Num")->where("DD_Num",$key)->order("E_Num DESC")->find();
        if($maxNum) $E_Num = $maxNum["E_Num"];
        $E_Num = $E_Num?$E_Num:$key.'001';
        $this->view->assign("E_Num",$E_Num);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $params["E_PerNum"] = $params["E_Name"].$params["E_Num"];
                    $result = false;
                    $result = $row->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were updated'));
                    }
                }else{
                    $this->error($validate->getError());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        foreach($row as $kk=>$vv){
            if(in_array($kk,["E_EnterTime","e_workdate","E_Birthday","E_Formal","E_LeaveTime"])) $row[$kk] = $vv=="1900-01-01 00:00:00"?"":$vv;
        }
        unset($vv,$kk);
        switch ($row["Valid"])
        {
            case 1:
                $year = time()-strtotime($row["E_EnterTime"]);
                break;
            default:
                $year = $row['E_LeaveTime']=="1900-01-01 00:00:00"?(time()-strtotime($row["E_EnterTime"])):(strtotime($row["E_LeaveTime"])-strtotime($row["E_EnterTime"]));
        }
        $row["year"] = diffDay($year);
        $row["DD_Name"] = $this->company[$row["DD_Num"]];
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 下拉联动修改客户编码
     */
    public function companyType()
    {
        $E_Num = "";
        $params = $this->request->post("num");
        if(isset($this->company[$params])){
            $key = $this->company[$params]?$params:key($this->company);
            $maxNum = $this->model->field("E_Num")->where("DD_Num",$key)->order("E_Num DESC")->find();
            if($maxNum) $E_Num = $maxNum["E_Num"];
            $E_Num = $E_Num?"0".($E_Num+1):$key.'001';
        }
        
        return json(["code"=>1,"data"=>["E_Num"=>$E_Num]]);
    }

    public function selectCrif($field='E_hjCerif')
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->where($field,"<>","")
                ->order($sort, $order)
                ->paginate($limit);
                
            $rows = $list->items();
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        $this->assignconfig("field",$field);
        return $this->view->fetch();
    }

    /**
     * 段和明细导入
     */
    public function importTypeCopy()
    {
        if ($this->request->isPost()) {
            $file = $this->request->post("filename");
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $data = [];
            $file = ROOT_PATH . DS . 'public' . DS . $file;
            try {
                $data = Excel::import($file, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($data)){
                    $this->error('空数据文件');
                }
                $data = array_slice($data,1);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $ddList = $this->company;
            $sexList = ["男"=>1,"女"=>0];
            $peoList = $this->model->select();
            $peoArr = $newPeoArr = [];
            foreach($peoList as $v){
                $peoArr[$v["E_Num"]] = $v->toArray();
            }
            foreach($data as $k=>$v){
                $key = $v[2];
                if(!$key) continue;
                // if(isset($peoArr[$key])) $newPeoArr[$key] = $peoArr[$key];
                $E_EnterTime = date_parse_from_format('n/j/Y',$v[7]);
                $E_EnterTime = mktime(0,0,0,$E_EnterTime['month'],$E_EnterTime['day'],$E_EnterTime['year']);
                $E_Birthday = date_parse_from_format('n/j/Y',$v[8]);
                $E_Birthday = mktime(0,0,0,$E_Birthday['month'],$E_Birthday['day'],$E_Birthday['year']);
                $E_LeaveTime = date_parse_from_format('n/j/Y',$v[18]);
                $E_LeaveTime = mktime(0,0,0,$E_LeaveTime['month'],$E_LeaveTime['day'],$E_LeaveTime['year']);
                // pri($v[7],$E_EnterTime,$E_Birthday,$E_LeaveTime,1);
                $newPeoArr[$key] = [
                    "DD_Num" => array_search("公司-".$v[0],$ddList),
                    "E_Level" => $v[1],
                    "E_Num" => $v[2],
                    "E_Name" => $v[3],
                    "E_PerNum" => $v[4],
                    "E_Sex" => $v[5]?$sexList[$v[5]]:0,
                    "E_IDCard" => $v[6],
                    "E_EnterTime" => date("Y-m-d",$E_EnterTime),
                    "E_Birthday" => date("Y-m-d",$E_Birthday),
                    "E_Address" => $v[9],
                    "E_Nation" => $v[10],
                    "E_Education" => $v[11],
                    "e_Degree" => $v[12],
                    "E_Specialized" => $v[13],
                    "E_College" => $v[14],
                    "E_Politics" => $v[15],
                    "E_IfMarry" => $v[16],
                    "E_FileNum" => $v[17],
                    "E_LeaveTime" => date("Y-m-d",$E_LeaveTime),
                    "E_bMobile" => $v[19],
                    "E_nMobile" => $v[20],
                    "E_Mobile" => $v[21],
                    "E_Txn" => $v[22],
                    "E_Tel" => $v[23],
                    "E_Car" => $v[24],
                    "E_CarNum" => $v[25],
                    "E_InsureNum" => $v[26],
                    "E_hjCerif" => $v[27]??'',
                ];
            }
            $result = $this->model->allowField(TRUE)->saveAll($newPeoArr);
            
            if ($result != false) {
                $this->success("成功导入".count($result)."条记录！");
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        return $this->view->fetch();
        
    }

    public function importType()
    {
        if ($this->request->isPost()) {
            $file = $this->request->post("filename");
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $data = [];
            $file = ROOT_PATH . DS . 'public' . DS . $file;
            try {
                $data = Excel::import($file, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($data)){
                    $this->error('空数据文件');
                }
                $data = array_slice($data,1);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $ddList = $this->company;
            $sexList = ["男"=>1,"女"=>0];
            $peoList = $this->model->select();
            $peoArr = $newPeoArr = [];
            foreach($peoList as $v){
                $key = $v["E_Name"].$v["E_IDCard"];
                $peoArr[$key] = $v->toArray();
            }
            foreach($data as $k=>$v){
                $key = $v[3].$v[6];
                if(!$key) continue;
                // if(isset($peoArr[$key])) $newPeoArr[$key] = $peoArr[$key];
                // $E_EnterTime = date_parse_from_format('n/j/Y',$v[7]);
                // $E_EnterTime = mktime(0,0,0,$E_EnterTime['month'],$E_EnterTime['day'],$E_EnterTime['year']);
                // $E_Birthday = date_parse_from_format('n/j/Y',$v[8]);
                // $E_Birthday = mktime(0,0,0,$E_Birthday['month'],$E_Birthday['day'],$E_Birthday['year']);
                // $E_LeaveTime = date_parse_from_format('n/j/Y',$v[18]);
                // $E_LeaveTime = mktime(0,0,0,$E_LeaveTime['month'],$E_LeaveTime['day'],$E_LeaveTime['year']);
                // pri($v[7],$E_EnterTime,$E_Birthday,$E_LeaveTime,1);
                $newPeoArr[$key] = isset($peoArr[$key])?$peoArr[$key]:[];
                $newPeoArr[$key]["DD_Num"] = array_search("公司-".$v[0],$ddList);
                $newPeoArr[$key]["E_Level"] = $v[1]?$v[1]:9;
                $newPeoArr[$key]["E_Num"] = $v[2]?$v[2]:'';
                $newPeoArr[$key]["E_Name"] = $v[3]?$v[3]:'';
                $newPeoArr[$key]["E_PerNum"] = $v[3].$v[2];
                $newPeoArr[$key]["E_Sex"] = $v[5]?$sexList[$v[5]]:0;
                $newPeoArr[$key]["E_IDCard"] = $v[6]?$v[6]:'';
                $newPeoArr[$key]["E_EnterTime"] = $v[7]?$v[7]:'1900-1-0 0:00:00';
                $newPeoArr[$key]["E_Birthday"] = $v[8]?$v[8]:'1900-1-0 0:00:00';
                $newPeoArr[$key]["E_Address"] = $v[9]?$v[9]:'';
                $newPeoArr[$key]["E_Nation"] = $v[10]?$v[10]:'';
                $newPeoArr[$key]["E_Education"] = $v[11]?$v[11]:'';
                $newPeoArr[$key]["e_Degree"] = $v[12]?$v[12]:'';
                $newPeoArr[$key]["E_Specialized"] = $v[13]?$v[13]:'';
                $newPeoArr[$key]["E_College"] = $v[14]?$v[14]:'';
                $newPeoArr[$key]["E_Politics"] = $v[15]?$v[15]:'';
                $newPeoArr[$key]["E_IfMarry"] = $v[16]?$v[16]:'';
                $newPeoArr[$key]["E_FileNum"] = $v[17]?$v[17]:'';
                $newPeoArr[$key]["Valid"] = strtotime($v[18])<time()?0:1;
                $newPeoArr[$key]["E_LeaveTime"] = $v[18]?$v[18]:'2099-12-31 0:00:00';
                $newPeoArr[$key]["E_bMobile"] = $v[19]?$v[19]:'';
                $newPeoArr[$key]["E_nMobile"] = $v[20]?$v[20]:'';
                $newPeoArr[$key]["E_Mobile"] = $v[22]?$v[22]:'';
                $newPeoArr[$key]["E_Txn"] = $v[23]?$v[23]:'';
                $newPeoArr[$key]["E_Tel"] = $v[24]?$v[24]:'';
                $newPeoArr[$key]["E_Car"] = $v[25]?$v[25]:'';
                $newPeoArr[$key]["E_CarNum"] = $v[26]?$v[26]:'';
                $newPeoArr[$key]["E_InsureNum"] = $v[27]?$v[27]:'';
            }
            $this->model->where("1=1")->delete();
            $result = $this->model->allowField(TRUE)->saveAll($newPeoArr,FALSE);
            if ($result != false) {
                $this->success("成功导入".count($result)."条记录！");
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        return $this->view->fetch();
    }

}
