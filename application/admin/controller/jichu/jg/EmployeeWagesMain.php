<?php

namespace app\admin\controller\jichu\jg;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 工资单
 *
 * @icon fa fa-circle-o
 */
class EmployeeWagesMain extends Backend
{
    
    /**
     * EmployeeWagesMain模型对象
     * @var \app\admin\model\jichu\jg\EmployeeWagesMain
     */
    protected $model = null,$typeList;

    public function _initialize()
    {
        parent::_initialize();
        $this->admin = \think\Session::get('admin');
        $this->model = new \app\admin\model\jichu\jg\EmployeeWagesMain;
        $this->detailModel = new \app\admin\model\jichu\jg\EmployeeWagesDetail;
        $this->typeList = [
            1 => "行政",
            2 => "导线",
            3 => "仓库",
            4 => "杂工"
        ];
        $this->view->assign("typeList",$this->typeList);
        $this->assignconfig("typeList",$this->typeList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $one = $this->model->where([
                    "type" => ["=",$params["type"]],
                    "cm_num" => ["=",$params["cm_num"]]
                ])->find();
                if($one) $this->error("已存在无法再次添加！");
                $result = false;
                Db::startTrans();
                try {
                    $params["writer"] = $this->admin["username"];
                    foreach([["，",","]] as $k=>$v){
                        $params["remark"] = str_replace($v[0],$v[1],$params["remark"]);
                    }
                    $remark = explode("\n",$params["remark"]);
                    unset($params["remark"]);
                    $this->model->insert($params);
                    $result = $this->model->getLastInsID();
                    $gzList = [];
                    foreach($remark as $k=>$v){
                        if($v){
                            $detail = explode(",",$v);
                            if($detail[0]){
                                $gzList[$k] = [
                                    "name" => $detail[0],
                                    "m_id" => $result,
                                    "wages" => $detail[1]?$detail[1]:0,
                                    "base_wages" => $detail[1]?$detail[1]:0,
                                ];
                            }
                        }
                    }
                    if(empty($remark)) throw new Exception("杂工信息不能为空");
                    $detailResult = $this->detailModel->allowField(true)->saveAll($gzList);
                    if(!$detailResult) throw new Exception("杂工信息有误，无法保存！");
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $flag = 1;
        if($row["auditor"]=="") $flag = 0;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $list = $this->detailModel->field("*,cast(right(name,5) as unsigned) as name_order")->where("m_id",$ids)->order("name_order,id")->select();
        $list = $list?collection($list)->toArray():[];
        $idList = $newIdList = [];
        foreach($list as $k=>$v){
            $idList[$v["id"]] = $v["id"];
            foreach($v as $kk=>$vv){;
                is_numeric($vv)?$list[$k][$kk] = round($vv,2):'';
            }
            $list[$k]["sj_money"] = $list[$k]["jb_wages"]+$list[$k]["wages"]+$list[$k]["post_subsidies"]+$list[$k]["car_stickers"]+$list[$k]["nutrition_subsidies"]+$list[$k]["night_meal_allowance"]+$list[$k]["safety_award"]+$list[$k]["other_stickers"]-$list[$k]["meals"];
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params["check"]) {
                $checkList = json_decode($params["check"],true);
                $saveList = [];
                $numberList = ["cm_day","jb_wages","wages","base_wages","communication_charges","post_subsidies","car_stickers","nutrition_subsidies","night_meal_allowance","safety_award","other_base","other_stickers","meals","other_deduction","social_insurance","personal_income_tax"];
                foreach($checkList as $k=>$v){
                    $v["m_id"] = $ids;
                    $v["id"] = $v["id"]??0;
                    if(!$v["id"]) unset($v["id"]);
                    else $newIdList[$v["id"]] = $v["id"];
                    foreach($numberList as $vv){
                        $v[$vv] = 0+($v[$vv]??0);
                    }
                    $saveList[$v["name"]] = $v;
                }
                $delData = array_diff(array_keys($idList),array_keys($newIdList));
                $delResult = $result = false;
                Db::startTrans();
                try {
                    if(!empty($delData)) $delResult = $this->detailModel->where("id","IN",$delData)->delete();
                    $result = $this->detailModel->allowField(true)->saveAll($saveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false or $delResult !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("flag",$flag);
        $this->assignconfig("flag",$flag);
        $this->assignconfig("list",$list);
        $this->assignconfig("idList",$idList);
        $this->view->assign("row", $row);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    /**
     * 审核
     */
    public function auditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->update([
                    "auditor" => $this->admin["username"],
                    "auditor_time" => date("Y-m-d H:i:s")
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function giveUp($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","<>","")->update([
                    "auditor" => '',
                    "auditor_time" => "0000-00-00 00:00:00"
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function addPeople($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\jichu\jg\Check())
                ->where([
                    "cm_num" => $row["cm_num"],
                    "type" => 1
                ])
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function tableSecd($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $list = $this->detailModel->where("m_id",$ids)->order("id")->select();
        $list = $list?collection($list)->toArray():[];
        $idList = $newIdList = [];
        foreach($list as $k=>$v){
            $idList[$v["id"]] = $v["id"];
            foreach($v as $kk=>$vv){;
                is_numeric($vv)?$list[$k][$kk] = round($vv,2):'';
            }
            $list[$k]["sj_money"] = round($list[$k]["jb_wages"]+$list[$k]["wages"]+$list[$k]["post_subsidies"]+$list[$k]["car_stickers"]+$list[$k]["nutrition_subsidies"]+$list[$k]["night_meal_allowance"]+$list[$k]["safety_award"]+$list[$k]["other_base"]-$list[$k]["meals"],2);
            $list[$k]["zb_wages"] = round($list[$k]["wages"]+$list[$k]["jb_wages"]-$list[$k]["base_wages"]-$list[$k]["communication_charges"]-$list[$k]["other_stickers"],2);
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params["check"]) {
                $checkList = json_decode($params["check"],true);
                $saveList = [];
                $numberList = ["cm_day","jb_wages","wages","base_wages","communication_charges","post_subsidies","car_stickers","nutrition_subsidies","night_meal_allowance","safety_award","other_base","other_stickers","meals","other_deduction","social_insurance","personal_income_tax"];
                foreach($checkList as $k=>$v){
                    foreach($numberList as $vv){
                        $v[$vv] = 0+($v[$vv]??0);
                    }
                    $saveList[$v["name"]] = $v;
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->detailModel->allowField(true)->saveAll($saveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->assignconfig("list",$list);
        $this->view->assign("row", $row);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->column($pk);
            if(empty($list)) $this->error(__('No rows were deleted'));
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk,"in",$list)->delete();
                $this->detailModel->where("m_id","in",$ids)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    // public function summary()
    // {
    //     $ids = $this->request->post("ids");
    //     $row = $this->model->where("id",$ids)->find();
    //     if (!$row) {
    //         $this->error(__('No Results were found'));
    //     }
    //     $start = substr($row["cm_num"],0,4)."-".substr($row["cm_num"],4,2)."-01 00:00:00";
    //     $end = date("Y-m-d H:i:s",strtotime("+1 month -1 seconds",strtotime($start)));

    //     $betweenTime = [$start,$end];
    //     $otherPriceViewModel = new \app\admin\model\jichu\jg\OtherPriceView();
    //     $otherPriceModel = new \app\admin\model\jichu\jg\OtherPrice();
    //     $one = $otherPriceModel->where([
    //         "record_time" => ["between",$betweenTime],
    //         "auditor" => ["=",""]
    //     ])->find();
    //     if($one) $this->error("该月份有未审核的情况，请先审核");

    //     $list = $otherPriceViewModel->where([
    //             "record_time" => ["between",$betweenTime],
    //         ])
    //         ->group("name")
    //         ->column("");
        


    // }
}
