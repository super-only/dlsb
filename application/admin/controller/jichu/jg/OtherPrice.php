<?php

namespace app\admin\controller\jichu\jg;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 其他工资
 *
 * @icon fa fa-circle-o
 */
class OtherPrice extends Backend
{
    
    /**
     * OtherPrice模型对象
     * @var \app\admin\model\jichu\jg\OtherPrice
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\jg\OtherPrice;
        $this->viewModel = new \app\admin\model\jichu\jg\OtherPriceView;
        $this->detailModel = new \app\admin\model\jichu\jg\OtherPriceDetail;
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->viewModel
                ->field('id,record_time,writer_time,writer,auditor_time,auditor,group_concat(name) as name,amount,remark')
                ->where($where)
                ->order($sort, $order)
                ->group("id")
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }
    
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $params["writer"] = $this->admin["username"];
                if(!$params["amount"]) $this->error("请填写正确金额");
                $peopleList = [];
                foreach(explode(",",$params["peoples"]) as $k=>$v){
                    if(is_numeric($v)) continue;
                    $peopleList[$v] = [
                        "name" => $v
                    ];
                }
                if(empty($peopleList)) $this->error("人员不能为空");
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->insertGetId($params);
                    foreach($peopleList as $k=>$v){
                        $peopleList[$k]["oid"] = $result;
                    }
                    $this->detailModel->allowField(true)->saveAll($peopleList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $treeList = $this->_getTreeData();
        $this->assignconfig("treeList",$treeList);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if(!$params["amount"]) $this->error("请填写正确金额");
                $peopleList = [];
                foreach(explode(",",$params["peoples"]) as $k=>$v){
                    if(is_numeric($v)) continue;
                    $peopleList[$v] = [
                        "oid" => $ids,
                        "name" => $v
                    ];
                }
                if(empty($peopleList)) $this->error("人员不能为空");

                $peoArr = $this->detailModel->where("oid",$ids)->column("name,id");
                $ydiffList = array_diff_key($peoArr,$peopleList);
                $ldiffList = array_diff_key($peopleList,$peoArr);
                $result = false;
                Db::startTrans();
                try {
                    $result = $row->allowField(true)->save($params);
                    if(!empty($ydiffList)) $this->detailModel->where("id","IN",$ydiffList)->delete();
                    if(!empty($ldiffList)) $this->detailModel->allowField(true)->saveAll($ldiffList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        $treeList = $this->_getTreeData([],$row['peoples']);
        $this->assignconfig("treeList",$treeList);
        return $this->view->fetch();
    }

    protected function _getTreeData($where = [],$select="")
    {
        $departTrue = $peoTrue = [];
        foreach(explode(",",$select) as $k=>$v){
            if(is_numeric($v)) $departTrue[$v] = $v;
            else $peoTrue[$v] = $v;
        }
        $departModel = new \app\admin\model\jichu\jg\Deptdetail();
        $departList = $departModel->where("DD_Num","<>","02")->where($where)->column("DD_Num,DD_Name");
        $treeList = [];
        foreach($departList as $k=>$v){
            $treeList[] = [
                "id" => $k,
                "parent" => "#",
                "text" => $v,
                "type" => "menu",
                "state" => ["selected"=>""]
            ];
        }
        $employeeModel = new \app\admin\model\jichu\jg\Employee();
        $employeeList = $employeeModel->where("Valid","=","1")->column("E_Num,E_PerNum,DD_Num");
        foreach($employeeList as $k=>$v){
            $treeList[] = [
                "id" => $v["E_PerNum"],
                "parent" => $v["DD_Num"],
                "text" => $v["E_PerNum"],
                "type" => "menu",
                "state" => ["selected"=>in_array($v["E_PerNum"], $peoTrue)]
            ];
        }
        return $treeList;
    }

    /**
     * 审核
     */
    public function auditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->update([
                    "auditor" => $this->admin["username"],
                    "auditor_time" => date("Y-m-d H:i:s")
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function unauditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","<>","")->update([
                    "auditor" => '',
                    "auditor_time" => "0000-00-00 00:00:00"
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->select();

            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
}
