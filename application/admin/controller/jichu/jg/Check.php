<?php

namespace app\admin\controller\jichu\jg;

use app\admin\model\jichu\jg\EmployeeWagesMain;
use app\admin\model\jichu\jg\EmployeeWagesDetail;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;

/**
 * 
 *
 * @icon fa fa-check
 */
class Check extends Backend
{
    
    /**
     * Check模型对象
     * @var \app\admin\model\jichu\jg\Check
     */
    protected $model = null,$admin;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\jg\Check;
        $this->wagesMainModel = new EmployeeWagesMain();
        $this->wagesDetailModel = new EmployeeWagesDetail();
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->field("*,concat(cm_num,'_',type) as idfield")
                ->where($where)
                ->order($sort, $order)
                ->group("cm_num,type")
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }
    
    protected function _getTreeData($where = [])
    {
        $departModel = new \app\admin\model\jichu\jg\Deptdetail();
        $departList = $departModel->where("DD_Num","<>","02")->where($where)->column("DD_Num,DD_Name");
        $treeList = [];
        foreach($departList as $k=>$v){
            $treeList[] = [
                "id" => $k,
                "parent" => "#",
                "text" => $v,
                "type" => "menu",
                "state" => ["selected"=>""]
            ];
        }
        $employeeModel = new \app\admin\model\jichu\jg\Employee();
        $employeeList = $employeeModel->where("Valid","=","1")->column("E_Num,E_PerNum,DD_Num");
        foreach($employeeList as $k=>$v){
            $treeList[] = [
                "id" => $v["E_PerNum"],
                "parent" => $v["DD_Num"],
                "text" => $v["E_PerNum"],
                "type" => "menu",
                "state" => ["selected"=>""]
            ];
        }
        return $treeList;
    }

    /**
     * 添加
     */
    public function add()
    {
        $employeeModel = new \app\admin\model\jichu\jg\Employee();
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $one = $this->model->where("cm_num",$params["cm_num"])->where("type",$params["type"])->find();
            if($one) $this->error("该年月该类型已经有考勤表，无法添加");

            $params = $this->preExcludeFields($params);
            if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                $params[$this->dataLimitField] = $this->auth->id;
            }
            if(!$params["rules"]) $this->error("请选择人员");
            if(!$params["cm_day"]) $this->error("请填写应出勤天数");
            $params["rules"] = explode(",",$params["rules"]);
            $holidays = isset($params["official_holiday"])?explode(",",$params["official_holiday"]):[];
            $days = date("t",strtotime($params["cm_num"]."01"));
            $saveList = [];
            for($i=1;$i<=$days;$i++){
                $week = date("N",strtotime($params["cm_num"].(str_pad($i,2,0,STR_PAD_LEFT))));
                foreach($params["rules"] as $k=>$v){
                    if(is_numeric($v)) continue;
                    $check = "/";
                    if(in_array($week,$holidays)){
                        $check = "√";
                    }
                    isset($saveList[$v])?$saveList[$v]["check"][] = ["date"=>$i,"check"=>$check]:$saveList[$v] = [
                        "cm_num" => $params["cm_num"],
                        "cm_day" => $params["cm_day"],
                        "name" => $v,
                        "check" => [["date"=>$i,"check"=>$check]],
                        "writer" => $this->admin["username"]
                    ];
                }
            }
            foreach($saveList as $k=>$v){
                $cq = $jb = 0;
                foreach($v["check"] as $kk=>$vv){
                    if(is_numeric($vv["check"])){
                        $cq++;
                        $jb+=(0+$vv["check"]);
                    }
                    elseif($vv["check"]=="/") $cq++;
                    elseif($vv["check"]=="半") $cq+=0.5;
                }
                // $cq = 0;
                // foreach($v["check"] as $kk=>$vv){
                //     $vv["check"]== "/" ? ($cq++) : '';
                // }
                $saveList[$k]["check"] = json_encode($v["check"]);
                $saveList[$k]["sj_day"] = $cq;
                $saveList[$k]["jb_hours"] = $jb;
                $saveList[$k]["type"] = $params["type"];
                // $saveList[$k]["total"] = $saveList[$k]["attendance"] = $cq;
            }
            if (!empty($saveList)) {
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->allowField(true)->saveAll($saveList,false);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $treeList = $this->_getTreeData();
        $this->assignconfig("treeList",$treeList);
        $official_holiday = (new \app\admin\model\chain\material\Configuresys())->where("SetName","official_holiday")->value("SetValue");
        $this->assign("official_holiday",$official_holiday);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        [$cm_num,$type] = explode("_",$ids);
        $ini_where = [
            "cm_num" => ["=",$cm_num],
            "type" => ["=",$type]
        ];
        $lists = $this->model->field("*,cast(right(name,5) as unsigned) as name_order")->where($ini_where)->order("name_order asc")->select();
        if(!$lists) {
            $this->error(__('No Results were found'));
        }
        $row = $detail = [];
        foreach($lists as $k=>$v){
            $row = [
                "cm_num" => $v["cm_num"],
                "cm_day" => $v["cm_day"],
                "type" => $type
            ];
            $detail[$v["id"]] = [
                "ids" => $v["id"],
                "id" => $k+1,
                "name" => $v["name"],
                "type" => $type,
                "sj_day" => round($v["sj_day"],2),
                "jb_hours" => round($v["jb_hours"],2)
            ];
            $checkList = json_decode($v["check"],true);
            foreach($checkList as $vv){
                $detail[$v["id"]]["day-".$vv["date"]] = $vv["check"];
            }
        }
        $flag = 1;
        if($lists[0]["auditor"]=="") $flag = 0;
        if ($this->request->isPost()) {
            if($flag) $this->error("已审核，无法修改");
            $params = $this->request->post("row/a");
            if ($params["check"]) {
                $addData = $editData = $delData = [];
                $checkReturnList = json_decode($params["check"],true);
                foreach($checkReturnList as $k=>$v){
                    $cq = $jb = 0;
                    if($v["ids"]){
                        $editData[$v["ids"]] = [];
                        foreach($v as $kk=>$vv){
                            $vv = trim($vv);
                            $vv = trim($vv,"<br>");
                            if(substr($kk,0,4)=="day-"){
                                if(!$vv) $this->error("出勤情况必填");
                                elseif(is_numeric($vv)){
                                    $cq++;
                                    $jb+=(0+$vv);
                                }
                                elseif($vv=="/") $cq++;
                                elseif($vv=="半") $cq+=0.5;
                                $editData[$v["ids"]]["check"][substr($kk,4)+0] = ["date"=>substr($kk,4)+0,"check"=>$vv];
                            }
                        }
                        ksort($editData[$v["ids"]]);
                        $editData[$v["ids"]]['check'] = json_encode(array_values($editData[$v["ids"]]['check']));
                        $editData[$v["ids"]]["id"] = $v["ids"];
                        $editData[$v["ids"]]["cm_day"] = $params["cm_day"];
                        $editData[$v["ids"]]["type"] = $type;
                        $editData[$v["ids"]]["sj_day"] = $cq;
                        $editData[$v["ids"]]["jb_hours"] = $jb;
                    }else{
                        $addData[$k] = [];
                        foreach($v as $kk=>$vv){
                            $vv = trim($vv);
                            if(substr($kk,0,4)=="day-"){
                                if(!$vv) $this->error("出勤情况必填");
                                elseif(is_numeric($vv)){
                                    $cq++;
                                    $jb+=(0+$vv);
                                }
                                elseif($vv=="/") $cq++;
                                elseif($vv=="半") $cq+=0.5;
                                $addData[$k]["check"][substr($kk,4)+0] = ["date"=>substr($kk,4)+0,"check"=>trim($vv)];
                            }
                        }
                        ksort($addData[$k]);
                        $addData[$k]['check'] = json_encode(array_values($addData[$k]['check']));
                        $addData[$k]["cm_num"] = $cm_num;
                        $addData[$k]["name"] = $v["name"];
                        $addData[$k]["writer"] = $this->admin["username"];
                        $addData[$k]["cm_day"] = $params["cm_day"];
                        $addData[$k]["sj_day"] = $cq;
                        $addData[$k]["type"] = $type;
                        $addData[$k]["jb_hours"] = $jb;
                    }
                }
                $delData = array_diff(array_keys($detail),array_keys($editData));
                $delResult = $editResult = $addResult = false;
                Db::startTrans();
                try {
                    if(!empty($delData)) $delResult = $this->model->where("id","IN",$delData)->delete();
                    $editResult = $this->model->allowField(true)->saveAll($editData);
                    if(!empty($addData)) $addResult = $this->model->allowField(true)->saveAll($addData,false);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($delResult !== false or $editResult !== false or $addResult !== false ) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $columns = [
            [
                ["checkbox"=> 'true', "field"=>"checkbox", "rowspan"=> 2, "align"=> 'center'],
                ["field"=> 'id', "title"=> '序号', "rowspan"=> 2, "align"=> 'center'],
                ["field"=> 'name', "title"=> '姓名', "rowspan"=> 2, "align"=> 'center'],
            ],[]
        ];
        $days = date("t",strtotime($cm_num."01"));
        $daysList = $daysWeekList = [];
        for($i=1;$i<=$days;$i++){
            $daysList[$i] = $i."号";
            $week = date("N",strtotime($cm_num.(str_pad($i,2,0,STR_PAD_LEFT))));
            $daysWeekList[$i] = $week;
            $columns[0][] = ["field"=> 'week-'.$i, "title"=> getWeek($week), "align"=> 'center'];
            $columns[1][] = ["field"=> 'day-'.$i, "title"=> $i, "align"=> 'center'];
        }
        if($lists[0]["auditor"]!=""){
            $columns[0][] = [
                "field"=> 'check', "title"=> '考勤', "colspan"=> 2, "align"=> 'center'
            ];
            $columns[1][] = [
                "field"=> 'sj_day', "title"=> '出勤', "align"=> 'center'
            ];
            $columns[1][] = [
                "field"=> 'jb_hours', "title"=> '加班', "align"=> 'center'
            ];
        }
        
        $fhList = $this->_getFhList();
        $this->view->assign("row",$row);
        $this->view->assign("flag",$flag);
        $this->assignconfig("flag",$flag);
        $this->assignconfig("fhList",build_select('fhList',$fhList,'',['class'=>'form-control']));
        $this->assignconfig("daysList",build_select('gx_day[]',$daysList,'',['class'=>'form-control',"multiple"=>'']));
        $this->assignconfig("daysWeekList",$daysWeekList);
        // $treeList = $this->_getTreeData();
        // $this->assignconfig("treeList",$treeList);
        $this->assignconfig("columns",$columns);
        $this->assignconfig("detail",array_values($detail));
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    protected function _getFhList()
    {
        $fhList = [
            "/" => "出勤",
            "⊕" => "病假",
            // "×" => "事假",
            // "※" => "产假",
            // "☆" => "婚嫁",
            // "▼" => "丧假",
            // "∧" => "探亲假",
            // "﹢" => "加班",
            "√" => "公休",
            // "∽" => "补休",
            // "○" => "旷工",
            // "Φ" => "迟到",
            "半" => "半天出勤",
        ];
        return $fhList;
    }

    public function addEditCheck($ids=null)
    {
        $fhList = $this->_getFhList();
        [$cm_num,$type] = explode("_",$ids);
        $days = date("t",strtotime($cm_num."01"));
        $daysList = $daysWeekList = [];
        for($i=1;$i<=$days;$i++){
            $daysList[$i] = $i."号";
            $week = date("N",strtotime($cm_num.(str_pad($i,2,0,STR_PAD_LEFT))));
            // $daysWeekList[$i] = $week;
        }
        $weekList = [
            "1" => "星期一",
            "2" => "星期二",
            "3" => "星期三",
            "4" => "星期四",
            "5" => "星期五",
            "6" => "星期六",
            "7" => "星期日",
        ];
        $assign = [
            "flList" => $fhList,
            "weekList" => $weekList,
            "daysList" => $daysList
        ];
        $this->view->assign($assign);
        $treeList = $this->_getTreeData();
        $this->assignconfig("treeList",$treeList);
        // $this->assignconfig("daysWeekList",$daysWeekList);
        return $this->view->fetch();
    }

    public function auditor()
    {
        $params = $this->request->post("num");
        [$cm_num,$type] = explode("_",$params);
        $ini_where = [
            "cm_num" => ["=",$cm_num],
            "type" => ["=",$type]
        ];
        $list = $this->model->where($ini_where)->select();
        $list = $list?collection($list)->toArray():[];
        foreach($list as $k=>$v){
            $list[$k]["auditor"] = $this->admin["username"];
            $list[$k]["auditor_time"] = date("Y-m-d H:i:s");
        }
        $result = 0;
        Db::startTrans();
        try {
            $result = $this->model->allowField(true)->saveAll($list);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result) {
            $this->success("审核成功");
        } else {
            $this->error("审核失败");
        }
    }

    public function giveUp()
    {
        $ids = $this->request->post("num");
        [$cm_num,$type] = explode("_",$ids);
        $ini_where = [
            "cm_num" => ["=",$cm_num],
            "type" => ["=",$type]
        ];
        $exist = $this->wagesMainModel->where($ini_where)->find();
        //要记得改一下规范
        if($exist) $this->error("已生成工资单，无法弃审修改！");

        $result = 0;
        Db::startTrans();
        try {
            $result = $this->model->where($ini_where)->update([
                "auditor" => '',
                "auditor_time" => "0000-00-00 00:00:00"
            ]);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result) {
            $this->success("弃审成功");
        } else {
            $this->error("弃审失败");
        }
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
            {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $cm_num = [];
            $type = '';
            if(!is_array($ids)){
                $ids = explode(",",$ids);
            }
            foreach($ids as $v){
                [$cm_num[],$type] = explode("_",$v);
            }
            $list = $this->model->where("cm_num", 'in', $cm_num)->where("type",$type)->where("auditor","=","")->select();
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function createMoney()
    {
        // $ids = $this->request->post("ids");
        $ids = $this->request->post("ids");
        [$cm_num,$type] = explode("_",$ids);
        $ini_where = [
            "cm_num" => ["=",$cm_num],
            "type" => ["=",$type]
        ];
        $list = $this->model->where($ini_where)->where("auditor","<>","")->select();
        if(!$list) $this->error("没有考勤情况，无法生成工资单");
        $result = false;
        Db::startTrans();
        try {
            $mainDetail = [
                "cm_num" => $list[0]["cm_num"],
                "sj_day" => $list[0]["sj_day"],
                "type" => $list[0]["type"],
                "writer" => $this->admin['username']
            ];
            $exist = $this->wagesMainModel->where($ini_where)->find();
            //要记得改一下规范
            if($exist) throw new Exception("已生成工资单，无法再次生成");
            $this->wagesMainModel->insert($mainDetail);
            $result = $this->wagesMainModel->getLastInsID();
            $newsData = $this->model->alias("c")
                ->join(["employee"=>"e"],"c.name=e.E_PerNum")
                ->field("c.name,c.cm_day,c.jb_hours,c.sj_day,e.wages,e.base_wages,e.communication_charges,e.post_subsidies,e.car_stickers,e.nutrition_subsidies,e.night_meal_allowance,e.safety_award,e.other_stickers")
                ->where($ini_where)
                ->select();
            $saveData = [];
            foreach($newsData as $k=>$v){
                $saveData[$v["name"]] = [
                    "m_id" => $result,
                    "name" => $v["name"],
                    "cm_day" => $mainDetail["sj_day"],
                    "jb_wages" => $v["jb_hours"],
                    "wages" => round($v["wages"]/$v["cm_day"]*$v["sj_day"],2),
                    "base_wages" => round($v["base_wages"]/$v["cm_day"]*$v["sj_day"],2),
                    "communication_charges" => $v["communication_charges"],
                    "post_subsidies" => $v["post_subsidies"],
                    "car_stickers" => $v["car_stickers"],
                    "nutrition_subsidies" => $v["nutrition_subsidies"],
                    "night_meal_allowance" => $v["night_meal_allowance"],
                    "safety_award" => $v["safety_award"],
                    "other_stickers" => $v["other_stickers"],
                    "meals" => 2*$mainDetail["sj_day"],
                ];
            }
            $this->wagesDetailModel->allowField(true)->saveAll($saveData);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if($result) $this->success();
        else $this->error("添加失败！");

    }

    public function export($ids)
    {
        $ids = $this->request->post("ids");
        [$cm_num,$type] = explode("_",$ids);
        $ini_where = [
            "cm_num" => ["=",$cm_num],
            "type" => ["=",$type]
        ];
        $lists = $this->model->field("*,cast(right(name,5) as unsigned) as name_order")->where($ini_where)->order("name_order asc")->select();
        if(!$lists) {
            $this->error(__('No Results were found'));
        }
        $date = date("Y年m月",strtotime($ids."01"));
        $title = $date."考勤表";
        $day = date("t",strtotime($ids."01"));
        $endCol = 6+$day;
        $endCol = (Coordinate::stringFromColumnIndex($endCol));
        $dateDayList = [];
        for($i=1;$i<=$day;$i++){
            $dateDayList[$i] = date("N",strtotime($ids.(str_pad($i,2,0,STR_PAD_LEFT))));
        }
        $thisList = array_chunk($lists,20);
        $cellList = $mergeList = [];
        $weekList = [
            1 => "一",
            2 => "二",
            3 => "三",
            4 => "四",
            5 => "五",
            6 => "六",
            7 => "七"
        ];
        foreach($thisList as $k=>$v){
            $row = $k*27+1;
            $mergeList[] = "A".$row.":B".($row+1);
            $mergeList[] = "C".$row.":Y".($row+1);
            $mergeList[] = "Z".$row.":Z".($row+1);
            $mergeList[] = "AA".$row.":".$endCol.$row;
            $mergeList[] = "AA".($row+1).":".$endCol.($row+1);
            $cellList[] = ["C".$row,$date."考勤表"];
            $cellList[] = ["Z".$row,"考勤"];
            $cellList[] = ["AA".$row,"出勤 /    病假 ⊕   事   假 ×  产假 ※ 婚嫁 ☆    丧假 ▼   探亲假 ∧    加班 ﹢ "];
            $cellList[] = ["AA".($row+1),"公休 √    补休 ∽    旷   工 ○  迟到 Φ"];
            $row += 2;
            $mergeList[] = "A".$row.":A".($row+4);
            $mergeList[] = "B".$row.":C".($row+2);
            $mergeList[] = "B".($row+3).":C".($row+3);
            $mergeList[] = "B".($row+4).":C".($row+4);
            $cellList[] = ["A".$row,"序号"];
            $cellList[] = ["B".$row,"星期"];
            $cellList[] = ["B".($row+3),"日期"];
            $cellList[] = ["B".($row+4),"姓名"];
            $col=4;
            foreach($dateDayList as $dk=>$dv){
                $mergeList[] = (Coordinate::stringFromColumnIndex($col)).$row.":".(Coordinate::stringFromColumnIndex($col)).($row+3);
                $cellList[] = [(Coordinate::stringFromColumnIndex($col)).$row,$weekList[$dv]];
                $cellList[] = [(Coordinate::stringFromColumnIndex($col)).($row+4),$dk];
                $col++;
            }
            $mergeList[] = (Coordinate::stringFromColumnIndex($col)).$row.":".(Coordinate::stringFromColumnIndex($col+2)).($row+2);
            $cellList[] = [(Coordinate::stringFromColumnIndex($col)).$row,"考勤"];
            $mergeList[] = (Coordinate::stringFromColumnIndex($col)).($row+3).":".(Coordinate::stringFromColumnIndex($col)).($row+4);
            $cellList[] = [(Coordinate::stringFromColumnIndex($col)).($row+3),"出勤"];
            $mergeList[] = (Coordinate::stringFromColumnIndex($col+1)).($row+3).":".(Coordinate::stringFromColumnIndex($col+1)).($row+4);
            $cellList[] = [(Coordinate::stringFromColumnIndex($col+1)).($row+3),"病假"];
            $mergeList[] = (Coordinate::stringFromColumnIndex($col+2)).($row+3).":".(Coordinate::stringFromColumnIndex($col+2)).($row+4);
            $cellList[] = [(Coordinate::stringFromColumnIndex($col+2)).($row+3),"合计"];
            $col = 1;
            $row += 5;
            for($ti=0;$ti<20;$ti++){
                $checkArr = [];
                $cellList[] = [(Coordinate::stringFromColumnIndex($col)).$row,$ti+1];
                $mergeList[] = (Coordinate::stringFromColumnIndex($col+1)).$row.":".(Coordinate::stringFromColumnIndex($col+2)).$row;
                if(isset($v[$ti])){
                    $checkList = json_decode($v[$ti]["check"],true);
                    foreach($checkList as $vv){
                        $checkArr[0+$vv["date"]] = $vv["check"];
                    }
                    ksort($checkArr);
                    $cellList[] = [(Coordinate::stringFromColumnIndex($col+1)).$row,$v[$ti]["name"]];
                    
                    foreach($dateDayList as $dk=>$dv){
                        $cellList[] = [(Coordinate::stringFromColumnIndex($col+$dk+2)).$row,$checkArr[$dk]];
                    }
                    $cellList[] = [(Coordinate::stringFromColumnIndex($col+$dk+3)).$row,$v[$ti]["sj_day"]];
                }
                $row++;
            }
        }
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        foreach($cellList as $k=>$v){
            $sheet->setCellValue($v[0],$v[1]);
        }
        foreach($mergeList as $k=>$v){
            $sheet->mergeCells($v);
        }
        $styleArray = [
            'borders' => [
                    'allBorders' => [
                        'borderStyle' => Border::BORDER_THIN //细边框
                    ]
                ]
        ];
        $sheet->getStyle('A1:'.$endCol.($row-1))->applyFromArray($styleArray);        
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx'); //按照指定格式生成Excel文件
        $this->excelBrowserExport($title, 'Xlsx');
        $writer->save('php://output');
        // $title = $one["DtM_sTypeName"];
        // $list = collection($this->sectModel->alias("bls")
        //     ->join(["baselibrarydetail"=>"bld"],"bls.DtS_ID_PK = bld.DtMD_iSectID_FK")
        //     ->field("case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
        //     SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
        //     when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
        //     when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
        //     when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
        //     when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
        //     else 0 end bjbhn,CAST(bls.DtS_Name AS UNSIGNED) AS number_1,CAST(bld.DtMD_sPartsID AS UNSIGNED) AS number_2,bls.DtS_Name,bld.*,round(DtMD_iUnitCount*DtMD_fUnitWeight,2) as sum_weight")
        //     ->where("bls.DtM_iID_FK",$ids)
        //     ->order("number_1,DtS_Name,bjbhn,number_2,DtMD_sPartsID asc")
        //     ->select())->toArray();
        
        // [$list,$header] = $this->getExportData($list);

        // return Excel::exportData($list, $header, $title. date('Ymd'));
    }

    /**
     * 输出到浏览器(需要设置header头)
     * @param string $fileName 文件名
     * @param string $fileType 文件类型
     */
    public function excelBrowserExport($fileName, $fileType) {

        //文件名称校验
        if(!$fileName) {
            trigger_error('文件名不能为空', E_USER_ERROR);
        }

        //Excel文件类型校验
        $type = ['Excel2007', 'Xlsx', 'Excel5', 'xls'];
        if(!in_array($fileType, $type)) {
            trigger_error('未知文件类型', E_USER_ERROR);
        }

        if($fileType == 'Excel2007' || $fileType == 'Xlsx') {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
            header('Cache-Control: max-age=0');
        } else { //Excel5
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$fileName.'.xls"');
            header('Cache-Control: max-age=0');
        }
    }
}
