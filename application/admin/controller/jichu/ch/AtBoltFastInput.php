<?php

namespace app\admin\controller\jichu\ch;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 常用螺栓规格设置
 *
 * @icon fa fa-circle-o
 */
class AtBoltFastInput extends Backend
{
    
    /**
     * AtBoltFastInput模型对象
     * @var \app\admin\model\jichu\ch\AtBoltFastInput
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\ch\AtBoltFastInput;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order("BD_MaterialName asc,BD_Type asc")
                ->select();
            $result = array("total" => count($list), "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

     public function dealLs()
     {
         $num = $this->request->post("num");
         if($num){
            $vendorArr = [];
            $vendorList = (new \app\admin\model\jichu\ch\VendorJGMsg())->alias('v')
                ->join(["inventorymaterial"=>"i"],"v.IM_Num = i.IM_Num")
                ->field("i.IM_Class as BD_MaterialName,i.IM_Spec as BD_Type,i.IM_BD_Other as BD_Other,i.IM_Length as BD_Lenth")
                ->where("v.V_Num",$num)
                ->select();
            if(!$vendorList) return json(["msg"=>"该供应商无螺栓信息","code"=>0]);
            foreach($vendorList as $v){
                $vendorArr[] = $v->toArray();
            }
            Db::startTrans();
            try {
                
                $this->model->where("BD_sFlag",0)->delete();
                $result = $this->model->allowField(true)->saveAll($vendorArr);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["msg"=>$e->getMessage(),"code"=>0]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["msg"=>$e->getMessage(),"code"=>0]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["msg"=>$e->getMessage(),"code"=>0]);
            }
            if($result) return json(["msg"=>"调拨成功","code"=>1]);
            else return json(["msg"=>"调拨失败","code"=>0]);
         }
     }

}
