<?php

namespace app\admin\controller\jichu\ch;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 供应商螺栓信息
 *
 * @icon fa fa-circle-o
 */
class VendorJGMsg extends Backend
{
    
    /**
     * VendorJGMsg模型对象
     * @var \app\admin\model\jichu\ch\VendorJGMsg
     */
    protected $model = null;
    protected $supplierModel = null;
    protected $supplierList;
    protected $imModel = null;
    protected $imList;
    protected $treeSearch;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\ch\VendorJGMsg;
        $this->supplierModel = new \app\admin\model\jichu\wl\Vendor;
        $this->imModel = new \app\admin\model\jichu\ch\InventoryMaterial;
        $listSupplier = $this->supplierModel->field("V_Num,V_Name")->where("VC_Num","02")->select();
        foreach($listSupplier as $v){
            $this->supplierList[$v["V_Num"]] = [
                "id" => $v["V_Num"],
                "parent" => "#",
                "text" => $v['V_Name']
            ];
        }
        $this->treeSearch = key($this->supplierList);
        // $listIm = $this->imModel->field("IM_Num,IM_Spec")->select();
        // foreach($listIm as $v){
        //     $this->imList[$v["IM_Num"]] = $v["IM_Spec"];
        // }

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $this->treeSearch = $this->request->get("tree", key($this->supplierList));
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $build = $this->model;
            if($this->treeSearch){
                $build = $build -> where("V_Num", $this->treeSearch);
            }
            $list = $build
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = [];
            foreach($list as $v){
                $rows[$v["IM_Num"]] = [
                    "VJG_ID" => $v["VJG_ID"],
                    "IM_Num" => $v["IM_Num"],
                    "IM_Class" => '',
                    "IM_Spec" => '',
                    "VJG_Length" => $v["VJG_Length"],
                    "VJG_PerWeight" => $v["VJG_PerWeight"],
                    "VJG_BlackPerWeight" => $v["VJG_BlackPerWeight"],
                ];
            }
            $imList = $this->imModel->field("IM_Num,IM_Spec,IM_Class")->where("IM_Num",'in',array_keys($rows))->select();
            foreach($imList as $v){
                $rows[$v["IM_Num"]] = array_merge($rows[$v["IM_Num"]],$v->toArray());
            }
            $result = array("total" => $list->total(), "rows" => array_values($rows));

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 读取分类树
     *
     * @internal
     */
    public function companytree()
    {
        $list = array_values($this->supplierList);
        return json($list);
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                foreach($paramsTable as $k=>$v){
                    $paramsTable[$k]["V_Num"] = $params['V_Num'];
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model->allowField(true)->saveAll($paramsTable);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    public function chooseSupplier()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\jichu\wl\Vendor())
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            $classList = $this->vendorList()[0];
            foreach($rows as $k=>$v){
                $rows[$k]["VC_Num"] = $classList[$v["VC_Num"]]??"";
            }
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 读取分类树
     *
     * @internal
     */
    public function vendorTree()
    {
        $list = array_values($this->vendorList(1)[1]);
        return json($list);
    }

    public function isExistVendor()
    {
        $V_Num = $this->request->post("num");
        $one = $this->model->where("V_Num",$V_Num)->find();
        if($one) return json(["code"=>0,'msg'=>"已经有该供应商螺栓信息，请选择修改操作！"]);
        else return json(["code"=>1,"msg"=>"成功"]);
    }

    public function chooseIm()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $IMmodel = new \app\admin\model\jichu\ch\InventoryMaterial();
            $list = $IMmodel
                ->where("Valid",1)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            $treeList = $select = [];
            $treeRows = $IMmodel->where("Valid","1")->order("ParentNum ASC")->select();
            foreach($treeRows as $v){
                $treeList[$v["IM_Num"]] = [
                    "id" => $v["IM_Num"],
                    "parent" => $v["ParentNum"]==""?"#":$v["ParentNum"],
                    "text" => $v['IM_BD_Other']?$v['IM_Spec']." ".$v['IM_BD_Other']:$v['IM_Spec'],
                    "state" => [
                        "opened" => false,
                        "disabled" => false,
                        "selected" => false
                    ]
                ];
                $select[$v["IM_Num"]] = $v["IM_Spec"];
            }
            foreach($rows as $k=>$v){
                $rows[$k]["ParentNum"] = $treeList[$v["ParentNum"]]["text"]??"";
                $rows[$k]["type"] = $select[substr($v['IM_Num'],0,2)];
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $ids = $ids??$this->treeSearch;
        $row = (new \app\admin\model\jichu\wl\Vendor())
            ->field("V_Num,V_Name")
            ->where("V_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                foreach($paramsTable as $k=>$v){
                    $paramsTable[$k]["V_Num"] = $params['V_Num'];
                }
                $result = false;
                Db::startTrans();
                try {
                    $this->model->where("V_Num",$params["V_Num"])->delete();
                    $result = $this->model->allowField(true)->saveAll($paramsTable);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $imField = '';
        $imList = [];
        $imRows = $this->model->alias('v')
            ->join(["inventorymaterial"=>'i'],"v.IM_Num=i.IM_Num")
            ->field("v.V_Num,v.IM_Num,v.VJG_PerWeight,v.VJG_BlackPerWeight,v.VJG_Length,i.IM_Num,i.IM_Spec,i.IM_Class")
            ->where("v.V_Num",$ids)->select();
        foreach($imRows as $k=>$v){
            $imList[] = $v["IM_Num"];
            $imField .= '<tr><td style="width:90px"><input type="button" class="table_del" value="删除" readonly></td>';
            $imField .= '<td>'.$v["IM_Num"].'<input type="hidden" name="table_row[IM_Num][]" value="'.$v["IM_Num"].'"></td>';
            $imField .= '<td>'.$v["IM_Class"].'</td>';
            $imField .= '<td>'.$v["IM_Spec"].'</td>';
            $imField .= '<td><input type="number" name="table_row[VJG_Length][]" value="'.$v["VJG_Length"].'"></td>';
            $imField .= '<td><input type="number" name="table_row[VJG_PerWeight][]" value="'.$v["VJG_PerWeight"].'"></td>';
            $imField .= '<td><input type="number" name="table_row[VJG_BlackPerWeight][]" value="'.$v["VJG_BlackPerWeight"].'"></td></tr>';
        
        }
        $this->view->assign("imList", $imList);
        $this->view->assign("tbdeal", $imField);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
}
