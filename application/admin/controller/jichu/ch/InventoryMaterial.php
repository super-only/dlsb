<?php

namespace app\admin\controller\jichu\ch;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 原材料紧固件档案
 *
 * @icon fa fa-circle-o
 */
class InventoryMaterial extends Backend
{
    
    /**
     * InventoryMaterial模型对象
     * @var \app\admin\model\jichu\ch\InventoryMaterial
     */
    protected $model = null;
    protected $treeList;
    protected $treeSearch;
    protected $selectList;
    protected $mmeasureList;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\ch\InventoryMaterial;
        $this->measureModel = new \app\admin\model\jichu\ch\Measurement;
        $list = $select = $measure = [];
        $ini_where = ["Valid"=>["=",1]];
        $treeRows = $this->model->where($ini_where)->order("ParentNum ASC")->select();
        foreach($treeRows as $v){
            $list[$v["IM_Num"]] = [
                "id" => $v["IM_Num"],
                "parent" => $v["ParentNum"]==""?"#":$v["ParentNum"],
                "text" => $v['IM_BD_Other']?$v['IM_Spec']." ".$v['IM_BD_Other']:$v['IM_Spec'],
                "state" => [
                    "opened" => false,
                    "disabled" => false,
                    "selected" => false
                ]
            ];
            $select[$v["IM_Num"]] = $v["IM_Spec"];
        }

        $measureRows = $this->measureModel->field("M_Name")->order("M_ID ASC")->select();
        foreach($measureRows as $v){
            $measure[$v["M_Name"]] = $v["M_Name"];
        }
        
        $this->treeList = $list;
        $this->selectList = $select;
        $this->mmeasureList = $measure;
        $this->view->assign('treeList',$this->treeList);
        $this->view->assign('selectList',$this->selectList);
        $this->view->assign('measureList',$this->mmeasureList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $this->treeSearch = $this->request->get("tree", '');
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $build = $this->model;
            if($this->treeSearch){
                $build = $build->where(function ($query) {
                    $query->where('IM_Num', 'like', $this->treeSearch.'%');

                });
            }
            $list = $build
                ->where("Valid",1)
                ->where($where)
                // ->order($sort, $order)
                ->order("ParentNum asc,IM_Class asc,IM_Spec asc")
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["ParentNum"] = $this->treeList[$v["ParentNum"]]["text"]??"";
                $rows[$k]["type"] = $this->selectList[substr($v['IM_Num'],0,2)];
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $defaultKey = $this->keyList();
                    foreach($defaultKey as $v){
                        if(isset($params[$v]) && $params[$v]==1) $this->model->where($v,1)->update([$v => 0]);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $tree = $this->request->get('tree','')?$this->request->get('tree',''):"01";
        $addData = $this->getCodeList($tree);
        $this->view->assign('addData',$addData);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $params["ifIMcommision"] = $params["ifIMcommision"]??0;
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $defaultKey = $this->keyList();
                    foreach($defaultKey as $v){
                        if(isset($params[$v]) && $params[$v]==1) $this->model->where($v,1)->update([$v => 0]);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row["type"] = $this->selectList[substr($row["IM_Num"],0,2)];
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function selectTree()
    {
        return $this->view->fetch();
    }

    public function stockTypeCode()
    {
        $tree = $this->request->post("value");
        $addData = $this->getCodeList($tree);
        return json(["code"=>1,"data"=>$addData]);
    }

    /**
     * 读取分类树
     *
     * @internal
     */
    public function companytree($type = null)
    {
        $list = $this->treeList;
        if($type == 2){
            foreach($list as $k=>$v){
                $str = substr($v["id"],0,2);
                if($str != 02) unset($list[$k]);
            }
        }
        return json(array_values($list));
    }

    protected function getCodeList($tree='01')
    {
        $row = [];
        $num = "";
        $row = $this->model
            ->field("IM_Num,cast(IM_Num as unsigned) as number")
            ->where("IM_Num","<>",$tree)
            ->where("ParentNum","=",$tree)
            ->order("number desc,IM_Num DESC")
            ->find();
        $IM_Num = $row?substr($row["IM_Num"],strlen($tree)):0;
        $num = $IM_Num?$tree.str_pad((++$IM_Num),3,0,STR_PAD_LEFT):$tree.'001';
        $im_class = substr($tree,0,5);
        $type = substr($tree,0,2);
        $im_class = intval($im_class)?$this->selectList[$im_class]:$this->selectList[$type];
        $addData = ["IM_Num"=>$num,"ParentNum"=>$tree,"type"=>$this->selectList[$type],"IM_Class"=>$im_class];
        return $addData;
    }

}
