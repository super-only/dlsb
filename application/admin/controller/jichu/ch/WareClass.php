<?php

namespace app\admin\controller\jichu\ch;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 仓库档案
 *
 * @icon fa fa-circle-o
 */
class WareClass extends Backend
{
    
    /**
     * WareClass模型对象
     * @var \app\admin\model\jichu\ch\WareClass
     */
    protected $model = null;
    protected $treeList;
    protected $metatype = ["五金"=>"五金","螺栓"=>"螺栓","产成品"=>"产成品","原材料"=>"原材料","其他"=>"其他"];
    protected $selectList;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\ch\WareClass;
        $list = [];
        $treeRows = $this->model->where("Valid","1")->order("ParentNum ASC")->select();
        foreach($treeRows as $v){
            $list[$v["WC_Num"]] = [
                "id" => $v["WC_Num"],
                "parent" => $v["ParentNum"]==""?"#":$v["ParentNum"],
                "text" => $v['WC_Name']."(".$v['WC_Num'].")",
                "state" => [
                    "opened" => false,
                    "disabled" => false,
                    "selected" => false
                ]
            ];
            $select[$v["WC_Num"]] = $v["WC_Name"];
        }
        $this->treeList = $list;
        $this->selectList = $select;
        $this->view->assign('treeList',$this->treeList);
        $this->view->assign("metatype",$this->metatype);
        $this->view->assign('selectList',$this->selectList);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $this->treeSearch = $this->request->get("tree", '');
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $build = $this->model;
            if($this->treeSearch){
                $build = $build->where(function ($query) {
                    $query->where('WC_Num', 'like', $this->treeSearch.'%');
                });
            }
            $list = $build
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["ParentNum"] = $this->treeList[$v["ParentNum"]]["text"]??"";
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $defaultKey = $this->keyList();
                    foreach($defaultKey as $v){
                        if(isset($params[$v]) && $params[$v]==1) $this->model->where($v,1)->update([$v => 0]);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $tree = $this->request->get('tree','');
        $parentText = $tree?$this->treeList[$tree]["text"]:"";
        $row = [];
        $num = "";
        if($tree){
            $row = $this->model
            ->field("WC_Num")
            ->where("WC_Num","<>",$tree)
            ->where("WC_Num","like",$tree.'%')
            ->order("WC_Num DESC")
            ->find();
            $WC_Num = $row?$row["WC_Num"]:0;
            $num = $WC_Num?"0".(++$WC_Num):$tree.'01';
        }else{
            $row = $this->model->field("WC_Num")->where("ParentNum","")->order("WC_Num DESC")->find();
            $WC_Num = $row?$row["WC_Num"]:0;
            $num = $WC_Num?"0".(++$WC_Num):'01';
        }
        $this->view->assign('WC_Num',$num);
        return $this->view->fetch();
    }

    /**
     * 读取分类树
     *
     * @internal
     */
    public function companytree($id=0,$pid=0)
    {
        $list = array_values($this->treeList);
        return json($list);
    }

    /**
     * 获取编码
     */
    public function stockTypeCode()
    {
        $params = $this->request->post("area");
        $firstNum = array_search($params,$this->selectList)?array_search($params,$this->selectList):key($this->selectList);
        $row = $this->model->field("WC_Num")->where("WC_Num","<>",$firstNum)->where("WC_Num", 'LIKE', $firstNum.'%')->order("WC_Num desc")->find();
        $WC_Num = $row?$row["WC_Num"]:0;
        $WC_Num = $WC_Num?("0".(++$WC_Num)):($firstNum.'01');
        return json(["code"=>1,"data"=>["WC_Num"=>$WC_Num]]);
    }
}
