<?php

namespace app\admin\controller\jichu\ch;

use app\common\controller\Backend;

/**
 * 材质分类
 *
 * @icon fa fa-circle-o
 */
class Limber extends Backend
{
    
    /**
     * Limber模型对象
     * @var \app\admin\model\jichu\ch\Limber
     */
    protected $model = null;
    protected $metatype = ["原材料"=>"原材料","锌锭"=>"锌锭","螺栓"=>"螺栓"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\ch\Limber;
        $this->view->assign("metatype",$this->metatype);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

}
