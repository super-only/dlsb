<?php

namespace app\admin\controller\jichu\yw;

use app\common\controller\Backend;

/**
 * 设备类型
 *
 * @icon fa fa-circle-o
 */
class DeviceModel extends Backend
{
    
    /**
     * DeviceModel模型对象
     * @var \app\admin\model\jichu\yw\DeviceModel
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\yw\DeviceModel;
        $categoryList = [1=>"质检设备",2=>"数控设备"];
        $this->view->assign("categoryList",$categoryList);
        $this->assignconfig("categoryList",$categoryList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            $mimetypeQuery = [];
            $filter = $this->request->request('filter');
            $filterArr = (array)json_decode($filter, true);
            if (isset($filterArr['category']) && $filterArr['category'] == 'unclassed') {
                $filterArr['category'] = ',unclassed';
                $this->request->get(['filter' => json_encode(array_diff_key($filterArr, ['category' => '']))]);
            }
            $this->request->get(['filter' => json_encode($filterArr)]);

            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->field("*,1 as status,1 as work")
                ->where($mimetypeQuery)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function detail($type=null,$ids=null)
    {
        $this->assignconfig("type",$type);
        $this->assignconfig("ids",$ids);
        [$columnList,$sortName] = $this->columnList($type);
        $this->assignconfig("columns",$columnList);
        $this->assignconfig("sortName",$sortName);
        if($type=="镀锌"){
            $this->model = new \app\admin\model\quality\process\DevDuXin();
        }else if($type=="拉力"){
            $this->model = new \app\admin\model\quality\experiment\DevLaLi();
        }else if($type=="光谱"){
            $this->model = new \app\admin\model\quality\experiment\DevGuangPu();
        }else if($type=="冲击"){
            $this->model = new \app\admin\model\quality\experiment\DevChongJi();
        }
        return parent::index();
    }

    protected function columnList($type='镀锌')
    {
        $column = [];
        $sortName = "";
        if($type=="镀锌"){
            $column = [
                ["field" => "id", "title"=> "序号", "operate" => "false"],
                ["field" => "wd", "title"=> "温度", "operate" => "false"],
                ["field" => "sj", "title"=> "时间", "operate" => "false"]
            ];
            $sortName = "sj";
        }else if($type=="拉力"){
            $column = [
                ["field" => "ID", "title"=> "序号", "operate" => "false"],
                ["field" => "Num", "title"=> "编号", "operate" => "false"],
                ["field" => "IBATCH", "title"=> "名称", "operate" => "false"],
                ["field" => "CAIZHI", "title"=> "材质", "operate" => "false"],
                ["field" => "GUIGE", "title"=> "规格", "operate" => "false"],
                ["field" => "IDATE", "title"=> "时间", "operate" => "false"],
                ["field" => "SQFQD", "title"=> "上屈服强度", "operate" => "false"],
                ["field" => "KLQD", "title"=> "抗拉强度", "operate" => "false"],
                ["field" => "LASHENLV", "title"=> "拉伸率", "operate" => "false"]
            ];
            $sortName = "IDATE";
        }else if($type=="光谱"){
            $column = [
                ["field" => "ID", "title"=> "序号", "operate" => "false"],
                ["field" => "FileName", "title"=> "文件名", "operate" => "false"],
                ["field" => "IDATETIME", "title"=> "时间", "operate" => "false"],
                ["field" => "INAME", "title"=> "名称", "operate" => "false"],
                ["field" => "IFIXNAME", "title"=> "校正后的名称", "operate" => "false"],
                ["field" => "C", "title"=> "C", "operate" => "false"],
                ["field" => "Si", "title"=> "Si", "operate" => "false"],
                ["field" => "Mn", "title"=> "Mn", "operate" => "false"],
                ["field" => "P", "title"=> "P", "operate" => "false"],
                ["field" => "S", "title"=> "S", "operate" => "false"],
                ["field" => "V", "title"=> "V", "operate" => "false"],
                ["field" => "Nb", "title"=> "Nb", "operate" => "false"],
                ["field" => "Ti", "title"=> "Ti", "operate" => "false"],
                ["field" => "Cr", "title"=> "Cr", "operate" => "false"]
            ];
            $sortName = "IDATETIME";
        }else if($type=="冲击"){
            $column = [
                ["field" => "ID", "title"=> "序号", "operate" => "false"],
                ["field" => "BATCH", "title"=> "名称", "operate" => "false"],
                ["field" => "NUMBER", "title"=> "编号", "operate" => "false"],
                ["field" => "Energy", "title"=> "冲击能量", "operate" => "false"],
                ["field" => "Temperature", "title"=> "温度", "operate" => "false"]
            ];
            $sortName = "Date";
        }else{

        }

        return [$column,$sortName];
    }

}
