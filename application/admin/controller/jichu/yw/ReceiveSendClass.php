<?php

namespace app\admin\controller\jichu\yw;

use app\common\controller\Backend;

/**
 * 收发类别
 *
 * @icon fa fa-circle-o
 */
class ReceiveSendClass extends Backend
{
    
    /**
     * ReceiveSendClass模型对象
     * @var \app\admin\model\jichu\yw\ReceiveSendClass
     */
    protected $model = null;
    protected $proList,$stockList;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\yw\ReceiveSendClass;
        $this->proList = $this->productType();
        $this->stockList = $this->stockType();
        $typeCode = $this->typeCode()[0][0];
        $row = $this->model->field("RSC_Num")->where("RSC_Num",'LIKE',$typeCode.'%')->order("RSC_Num DESC")->find();
        $defaultCode = $row?'0'.(++$row->RSC_Num):$typeCode.'01';
        $this->view->assign("defaultCode",$defaultCode);
        $this->view->assign("proList",$this->proList);
        $this->view->assign("stockList",$this->stockList); 
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["RSC_RSType"] = $this->stockList[$v["RSC_RSType"]]??"";
                $rows[$k]["RSC_Flag"] = $this->proList[$v["RSC_Flag"]]??"";
            }
            $result = array("total" => $list->total(), "rows" => $rows);
            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 获取收发类型编码 ajax
     */
    public function stockTypeCode()
    {
        $param = $this->request->post();
        list($productType,$stockType) = array_values($param);
        $typeCode = $this->typeCode()[$productType][$stockType];
        $row = $this->model->field("RSC_Num")->where("RSC_Num",'LIKE',$typeCode.'%')->order("RSC_Num DESC")->find();
        $defaultCode = $row?'0'.(++$row->RSC_Num):$typeCode.'01';
        return json(["code"=>1, "data"=>["defaultCode"=>$defaultCode], "msg"=>"success"]);
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

}
