<?php

namespace app\admin\controller\jichu\yw;

use app\common\controller\Backend;

/**
 * 发运方式
 *
 * @icon fa fa-circle-o
 */
class ShippingType extends Backend
{
    
    /**
     * ShippingType模型对象
     * @var \app\admin\model\jichu\yw\ShippingType
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\yw\ShippingType;
        $row = $this->model->field("ST_Num")->order("ST_Num DESC")->limit(1)->find()->toArray();
        $num = str_pad((++$row["ST_Num"]),2,0,STR_PAD_LEFT);
        $this->view->assign('ST_Num',$num);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

}
