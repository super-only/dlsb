<?php

namespace app\admin\controller\jichu\yw;

use app\common\controller\Backend;

/**
 * 合同分类
 *
 * @icon fa fa-circle-o
 */
class CompactSort extends Backend
{
    
    /**
     * CompactSort模型对象
     * @var \app\admin\model\jichu\yw\CompactSort
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\yw\CompactSort;
        $row = $this->model->field("CS_ID")->order("CS_ID DESC")->limit(1)->find()->toArray();
        $num = str_pad((++$row["CS_ID"]),2,0,STR_PAD_LEFT);
        $this->view->assign('CS_ID',$num);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

}
