<?php

namespace app\admin\controller\jichu\qt;

use app\common\controller\Backend;
use jianyan\excel\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

/**
 *
 *
 * @icon fa fa-circle-o
 */
class Change extends Backend
{
    
    /**
     * NotifyEmpSet模型对象
     * @var \app\admin\model\jichu\qt\Change
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();

    }
    public function index(){
        return $this->view->fetch();
    }

    // public function export($file=null){
    //     if(!$file) return false;
    //     $file = base64_decode($file);
    //     $suffix = ucfirst(substr($file,strrpos($file,".")+1));
    //     $data = [];
    //     $file_txt = ROOT_PATH . DS . 'public' . DS . $file;
    //     try {
    //         $data = Excel::import($file_txt, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
    //         if(empty($data)){
    //             $this->error('空数据文件');
    //         }
    //         $data = array_slice($data,1);
    //     }catch (\Exception $e){
    //         $this->error($e->getMessage());
    //     }
    //     $export_data = $end_data = [];
    //     foreach($data as $k=>$v){
    //         if($v[0]=='') continue;
    //         $part_num = 0;
    //         if(substr($v[0],0,2)=='00') $part_num = 0;
    //         else{
    //             if(strpos($v[0],"-") > 0){
    //                 $left_h_part = substr($v[0],0,strpos($v[0],"-"));
    //                 preg_match_all('/(\d+)|([^\d]+)/',$left_h_part,$matches);
    //                 $part_num = $matches[0][0]??0;
    //             }else{
    //                 $part = (int)$v[0];
    //                 if(strlen($part)<=2) $part_num = (int)$part;
    //                 else if(strlen($part)==3) $part_num = substr($part,0,1);
    //                 else $part_num = substr($part,0,2);
    //             }
    //         }
    //         if(strlen($part_num)>1){
    //             $part_num = substr($part_num,0,1)==0?substr($part_num,1):$part_num;
    //         }
    //         $export_data[$part_num][$v[0]] = $v;
    //         $export_data[$part_num][$v[0]][17] = $part_num;

    //         // if($v[0]){
    //         //     if((strlen($v[0])-strlen(str_replace('-','',$v[0])))>1){
    //         //         $part_num = substr($v[0],0,strpos($v[0],'-'))*10000000+substr(substr($v[0],strpos($v[0],'-')),0,strpos('-',substr($v[0],strpos($v[0],'-'))))*10000+intval(substr(substr($v[0],strpos($v[0],'-')),strpos('-',substr($v[0],strpos($v[0],'-')))));
    //         //     }else if((strlen($v[0])-strlen(str_replace('-','',$v[0])))==1 and strlen(intval(substr($v[0],strpos($v[0],'-')+1)))==3){
    //         //         $part_num = substr($v[0],0,strpos($v[0],'-'))*10000000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),0,1)*10000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),1);
    //         //     }else if((strlen($v[0])-strlen(str_replace('-','',$v[0])))==1 and strlen(intval(substr($v[0],strpos($v[0],'-')+1)))==4){
    //         //         $part_num = substr($v[0],0,strpos($v[0],'-'))*10000000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),0,2)*10000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),2);
    //         //     }else if((strlen($v[0])-strlen(str_replace('-','',$v[0])))==1 and strlen(intval(substr($v[0],strpos($v[0],'-')+1)))<3){
    //         //         $part_num = substr($v[0],0,strpos($v[0],'-'))*10000000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),2);
    //         //     }else if(strlen(intval($v[0]))==3){
    //         //         $part_num = substr(intval($v[0]),0,1)*10000000+substr(intval($v[0]),1);
    //         //     }else if(strlen(intval($v[0]))==4){
    //         //         $part_num = substr(intval($v[0]),0,2)*10000000+substr(intval($v[0]),2);
    //         //     }else{
    //         //         $part_num = 0;
    //         //     }
    //         //     $export_data[$part_num][$v[0]] = $v;
    //         //     $export_data[$part_num][$v[0]][17] = $part_num;
    //         // }
    //     }
    //     ksort($export_data,1);
    //     foreach($export_data as $v){
    //         ksort($v);
    //         foreach($v as $vv){
    //             $end_data[$vv[0]] = $vv;
    //         }
    //     }
    //     $header = [
    //         ['零件编号', 0],
    //         ['材质', 1],
    //         ['规格', 2],
    //         ['长度(mm)', 3],
    //         ['宽度(mm)', 4],
    //         ['单基数量', 5],
    //         ['单基重量', 6],
    //         ['孔数', 7],
    //         ['电焊', 8],
    //         ['弯曲', 9],
    //         ['切角', 10],
    //         ['铲背', 11],
    //         ['清根', 12],
    //         ['打扁', 13],
    //         ['开合角', 14],
    //         ['钻孔', 15],
    //         ['备注', 16],
    //         ['排序零件编号', 17]
    //     ];

    //     return Excel::exportData($end_data, $header, '材料表'. date('Ymd'));
    // }

    public function export($file=null)
    {
        if(!$file) return false;
        $file = base64_decode($file);
        $suffix = ucfirst(substr($file,strrpos($file,".")+1));
        $iniData = $data = [];
        $title = '';
        $file_txt = ROOT_PATH . DS . 'public' . DS . $file;
        try {
            $iniData = $data = Excel::import($file_txt, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
            if(empty($data)){
                $this->error('空数据文件');
            }
            preg_match('/\d+/',$data[0][0],$r);
            $title = end($r);
            $data = array_slice($data,1);
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
        foreach($data as $k=>$v){
            $emptyList = array_filter($v);
            if(empty($emptyList)) unset($data[$k]);
        }
        $fabic = "";
        $fabicList = [];
        foreach($data as $k=>$v){
            if(strpos($v[0],"#")!==FALSE){
                $fabic = substr($v[0],0,strlen($v[0])-1);
                continue;
            }else if($v[0]){
            for($i=1;$i<=count($v)-1;$i++){
                if(!$v[$i]) continue;
                    else{
                        $keyField = $i==10?$v[0]*10:(($v[0]-1)==0?"".$i:($v[0]-1).$i);
                        $fabicList[$fabic][$keyField] = $v[$i];
                    }
                }
            }
        }
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells("A1:N1");
        $sheet->setCellValue('A1','SHAOXING QINGDA IMP&EXP CO.,LTD.');
        $sheet->mergeCells("A2:N2");
        $sheet->setCellValue('A2','DETAILED PACKING LIST');
        
        $row=4;$col=1;
        $setCellList = [];
        $mergeCellList = [];
        $fieldList = [
            [0,0,"ORDER"],
            [1,0,"ROLL"],
            [2,0,"COL"],
            [3,0,"YDS"],
            [4,0,"MTRS"],
            [5,0,"N.W."],
            [0,1,"NO"],
            [1,1,"NO"],
            [2,1,"NO"],
            [0,2,$title],
            // [52,0,"TOTAL"],
        ];
        $mergeList = [
            [3,0,1],
            [4,0,1],
            [5,0,1],
            [0,2,50],
            [2,2,50]
        ];
        $count = 0;
        $totalList = [["色号"],["卷数"],["码数"]];
        foreach($fabicList as $k=>$v){
            $totalList[0][] = $k;
            $totalList[1][] = count($v);
            $totalList[2][] = array_sum($v);
            $thisList = array_chunk($v,50,true);
            foreach($thisList as $kk=>$vv){
                $count++;
                foreach($fieldList as $fk=>$fv){
                    $setCellList[] = [(Coordinate::stringFromColumnIndex($col+$fv[0])).($row+$fv[1]),$fv[2]];
                }
                $setCellList[] = [(Coordinate::stringFromColumnIndex($col+2)).($row+2),"COL-".$k];
                foreach($mergeList as $fk=>$fv){
                    $mergeCellList[] = (Coordinate::stringFromColumnIndex($col+$fv[0])).($row+$fv[1]).":".(Coordinate::stringFromColumnIndex($col+$fv[0])).($row+$fv[1]+$fv[2]);
                }
                $row +=2;
                $ini_row = $row;
                foreach($vv as $kkk=>$vvv){
                    $setCellList[] = [(Coordinate::stringFromColumnIndex($col+1)).$ini_row,$kkk];
                    $setCellList[] = [(Coordinate::stringFromColumnIndex($col+3)).$ini_row,$vvv];
                    $setCellList[] = [(Coordinate::stringFromColumnIndex($col+4)).$ini_row,"=".(Coordinate::stringFromColumnIndex($col+3)).$ini_row."*0.9144"];
                    $setCellList[] = [(Coordinate::stringFromColumnIndex($col+5)).$ini_row,"=".(Coordinate::stringFromColumnIndex($col+4)).$ini_row."*0.195"];
                    $ini_row++;
                }
                $row += 50;
                $setCellList[] = [(Coordinate::stringFromColumnIndex($col)).$row,"TOTAL"];
                $setCellList[] = [(Coordinate::stringFromColumnIndex($col+1)).$row,count($vv)];
                $setCellList[] = [(Coordinate::stringFromColumnIndex($col+3)).$row,"=SUM(".(Coordinate::stringFromColumnIndex($col+3)).($row-50).":".(Coordinate::stringFromColumnIndex($col+3)).($row-1).")"];
                $setCellList[] = [(Coordinate::stringFromColumnIndex($col+4)).$row,"=SUM(".(Coordinate::stringFromColumnIndex($col+4)).($row-50).":".(Coordinate::stringFromColumnIndex($col+4)).($row-1).")"];
                $setCellList[] = [(Coordinate::stringFromColumnIndex($col+5)).$row,"=SUM(".(Coordinate::stringFromColumnIndex($col+5)).($row-50).":".(Coordinate::stringFromColumnIndex($col+5)).($row-1).")"];
                $row++;
                if($count==4){
                    $count = 0;
                    $col = 1;
                }else{
                    $row -= 53;
                    $col += 6;
                }
            }
        }
        foreach($setCellList as $k=>$v){
            $sheet->setCellValue($v[0],$v[1]);
        }
        foreach($mergeCellList as $k=>$v){
            $sheet->mergeCells($v);
        }
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->getActiveSheet();
        $row=1;$col=1;
        foreach($totalList as $tk=>$tv){
            foreach($tv as $tvk=>$tvv){
                $sheet->setCellValue((Coordinate::stringFromColumnIndex($col)).$row,$tvv);
                $col++;
            }
            if($tk==0) $sheet->setCellValue((Coordinate::stringFromColumnIndex($col)).$row,"总数");
            else $sheet->setCellValue((Coordinate::stringFromColumnIndex($col)).$row,"=SUM(".(Coordinate::stringFromColumnIndex($col-count($tv))).$row.":".(Coordinate::stringFromColumnIndex($col-1)).$row.")");
            $row++;
            $col=1;
        }
        $fileName = '结果'. date('Ymd');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx'); //按照指定格式生成Excel文件
        $this->excelBrowserExport($fileName, 'Xlsx');
        $writer->save('php://output');
    }

    /**
	 * 输出到浏览器(需要设置header头)
	 * @param string $fileName 文件名
	 * @param string $fileType 文件类型
	 */
	public function excelBrowserExport($fileName, $fileType) {

	    //文件名称校验
	    if(!$fileName) {
	        trigger_error('文件名不能为空', E_USER_ERROR);
	    }

	    //Excel文件类型校验
	    $type = ['Excel2007', 'Xlsx', 'Excel5', 'xls'];
	    if(!in_array($fileType, $type)) {
	        trigger_error('未知文件类型', E_USER_ERROR);
	    }

	    if($fileType == 'Excel2007' || $fileType == 'Xlsx') {
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
	        header('Cache-Control: max-age=0');
	    } else { //Excel5
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment;filename="'.$fileName.'.xls"');
	        header('Cache-Control: max-age=0');
	    }
	}
}
