<?php

namespace app\admin\controller\jichu\wl;

use app\common\controller\Backend;
use think\Validate;
/**
 * 地区分类
 *
 * @icon fa fa-circle-o
 */
class AreaDetail extends Backend
{
    
    /**
     * AreaDetail模型对象
     * @var \app\admin\model\jichu\wl\AreaDetail
     */
    protected $model = null;
    protected $treeList;
    protected $selectList;
    protected $rule =   [
        'AD_Num'  => 'require',
        'AD_Name'   => 'require' 
    ];
    
    protected $message  =   [
        'AD_Num.require' => '地区编码必填',
        'AD_Name.require'     => '地区名称必填'
    ];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\jichu\wl\AreaDetail;
        $list = $select = $adList =  [];
        $treeRows = $this->model->order("ParentNum ASC")->select();
        foreach($treeRows as $v){
            $list[$v["AD_Num"]] = [
                "id" => $v["AD_Num"],
                "parent" => $v["ParentNum"]==""?"#":$v["ParentNum"],
                "text" => $v['AD_Name']."(".$v['AD_Num'].")",
                "state" => [
                    "opened" => false,
                    "disabled" => false,
                    "selected" => false
                ]
            ];
            $select[$v["AD_Num"]] = $v["AD_Name"];
            if($v["ParentNum"]=="") $adList[] = $v["AD_Num"];
        }
        $AD_Num = empty($adList)?0:max($adList);
        $AD_Num = str_pad((++$AD_Num),2,0,STR_PAD_LEFT);
        $this->view->assign("AD_Num",$AD_Num);
        $this->treeList = $list;
        $this->selectList = $select;
        $this->view->assign('treeList',$this->treeList);
        $this->view->assign('selectList',$this->selectList);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $build = $this->model;
            $list = $build
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["ParentNum"] = $this->treeList[$v["ParentNum"]]["text"]??"";
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $result = false;
                    $result = $this->model->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were inserted'));
                    }
                }else{
                    $this->error($validate->getError());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $result = false;
                    $result = $row->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were updated'));
                    }
                }else{
                    $this->error($validate->getError());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row["ParentName"] = $this->selectList[0][$row["ParentNum"]]??"无所属地区";
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 读取公司树
     *
     * @internal
     */
    public function companytree()
    {
        $list = array_values($this->treeList);
        return json($list);
    }

    /**
     * 获取编码
     */
    public function stockTypeCode()
    {
        $num = $this->request->post("num");
        $row = [];
        if($num){
            $where = [
                "AD_Num" => ["<>",$num],
                "AD_Num" => ["LIKE",$num."%"]
            ];
            $row = $this->model->field("AD_Num")->where($where)->order("AD_Num DESC")->find();
        }else{
            $row = $this->model->field("AD_Num")->where("ParentNum","")->order("AD_Num DESC")->find();
        }
        $AD_Num = $row?$row["AD_Num"]:0;
        $value = $AD_Num?(substr($num,0,1)=='0'?"0":"").(++$AD_Num):$num.'01';
        return json(["code"=>1, "data"=>["AD_Num"=>$value], "msg"=>"success"]);
    }
}
