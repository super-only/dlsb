<?php

namespace app\admin\controller\jichu\wl;

use app\common\controller\Backend;
use think\Validate;
/**
 * 客户档案
 *
 * @icon fa fa-circle-o
 */
class Customer extends Backend
{
    
    /**
     * Customer模型对象
     * @var \app\admin\model\jichu\wl\Customer
     */
    protected $model = null;
    protected $classModel = null;
    protected $addressModel = null;
    protected $deptModel = null;
    protected $classList;
    protected $addressList;
    protected $deptList,$admin;
    protected $rule =   [
        'CC_Num'  => 'require',
        'C_Num'   => 'require',
        'C_Name'  => 'require'
    ];
    
    protected $message  =   [
        'CC_Num.require' => '客户类型必填',
        'C_Num.require'  => '客户编码必填',
        'C_Name.require'  => '客户名称必填'
    ];

    public function _initialize()
    {
        parent::_initialize();
        $this->admin = \think\Session::get('admin');
        $this->model = new \app\admin\model\jichu\wl\Customer;
        $this->classModel = new \app\admin\model\jichu\wl\Customerclass;
        $this->addressModel = new \app\admin\model\jichu\wl\AreaDetail;
        $this->deptModel = new \app\admin\model\jichu\jg\Deptdetail;
        $class = $this->classModel->field('CC_Num,CC_Name')->select();
        foreach($class as $v){
            $this->classList[$v["CC_Num"]] = $v["CC_Name"];
        }
        $address = $this->addressModel->field("AD_Name")->select();
        foreach($address as $v){
            $this->addressList[$v["AD_Name"]] = $v["AD_Name"];
        }

        $dept = $this->deptModel->field("DD_Name")->select();
        foreach($dept as $v){
            $this->deptList[$v["DD_Name"]] = $v["DD_Name"];
        }

        $this->view->assign('admin',$this->admin["nickname"]);
        $class_list = [""=>"请选择"];
        $class_list = array_merge($class_list,$this->classList);
        $this->view->assign('classList', $class_list);
        $this->view->assign('addressList',$this->addressList);
        $this->view->assign('deptList',$this->deptList);
        
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["CC_Num"] = $this->classList[$v["CC_Num"]]??"";
            }
            $result = array("total" => $list->total(), "rows" => $rows);
            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 下拉联动修改客户编码
     */
    public function customerType()
    {
        $num = $this->request->post("num");
        $C_Num = "";
        if(isset($this->classList[$num])){
            $firstNum = $this->classList[$num]?$num:key($this->classList);
            $row = $this->model->field("C_Num")->where("C_Num","<>",$firstNum)->where("C_Num", 'LIKE', $firstNum.'%')->order("C_Num desc")->find();
            $C_Num = $row?$row["C_Num"]:0;
            $C_Num = $C_Num?("0".(++$C_Num)):($firstNum.'01');
        }
        return json(["code"=>1,"data"=>["C_Num"=>$C_Num]]);
    }


    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

     /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $result = false;
                    $params['AD_Name'] = $params['AD_Name']?$params['AD_Name']:"[全部]";
                    $params['C_C_Dept'] = $params['C_C_Dept']?$params['C_C_Dept']:"[全部]";
                    $result = $this->model->allowField(true)->save($params);
                    if ($result !== false) {
                        $main = [
                            "ccusabbname" => $params["C_ShortName"]?$params["C_ShortName"]:$params["C_Name"],
                            "ccuscode" => $params["C_Num"],
                            "ccccode" => $params["CC_Num"],
                            "ccusname" => $params["C_Name"]
                        ];
                        $data = [
                            "main" => $main
                        ];
                        $url = "/PostCus/Add";
                        $result = api_post_u($url,$data);
                        $this->success();
                    } else {
                        $this->error(__('No rows were inserted'));
                    }
                }else{
                    $this->error($validate->getError());
                }
                
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $result = false;
                    $params['AD_Name'] = $params['AD_Name']?$params['AD_Name']:"[全部]";
                    $params['C_C_Dept'] = $params['C_C_Dept']?$params['C_C_Dept']:"[全部]";
                    $params["C_Updater"] = $this->admin["nickname"];
                    $result = false;
                    $result = $row->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were updated'));
                    }
                }else{
                    $this->error($validate->getError());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row["CC_Name"] = $this->classList[$row["CC_Num"]];
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
