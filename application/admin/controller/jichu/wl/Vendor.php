<?php

namespace app\admin\controller\jichu\wl;

use app\common\controller\Backend;
use think\Validate;
/**
 * 供应商档案
 *
 * @icon fa fa-circle-o
 */
class Vendor extends Backend
{
    
    /**
     * Vendor模型对象
     * @var \app\admin\model\jichu\wl\Vendor
     */
    protected $model = null;
    protected $classModel = null;
    protected $classList;
    protected $addressModel = null;
    protected $deptModel = null;
    protected $addressList;
    protected $deptList,$admin;
    protected $rule =   [
        'VC_Num'  => 'require',
        'V_Num'   => 'require',
        'V_Name'  => 'require'
    ];
    
    protected $message  =   [
        'VC_Num.require' => '供应商类别必填',
        'V_Num.require'  => '供应商编码必填',
        'V_Name.require'  => '供应商名称必填'
    ];

    public function _initialize()
    {
        parent::_initialize();
        $this->admin = \think\Session::get('admin');
        $this->model = new \app\admin\model\jichu\wl\Vendor;
        $this->classModel = new \app\admin\model\jichu\wl\VendorClass;
        $this->addressModel = new \app\admin\model\jichu\wl\AreaDetail;
        $this->deptModel = new \app\admin\model\jichu\jg\Deptdetail;
        $this->classList = [""=>"请选择"];
        $this->classList = array_merge($this->classList,$this->vendorList(1)[0]);
        // $this->classList = $this->vendorList(1)[0];
        $address = $this->addressModel->field("AD_Name")->select();
        foreach($address as $v){
            $v["AD_Name"] = trim($v["AD_Name"]);
            $this->addressList[$v["AD_Name"]] = $v["AD_Name"];
        }

        $dept = $this->deptModel->field("DD_Name")->select();
        foreach($dept as $v){
            $v["DD_Name"] = trim($v["DD_Name"]);
            $this->deptList[$v["DD_Name"]] = $v["DD_Name"];
        }

        $this->view->assign('admin',$this->admin["nickname"]);
        $this->view->assign('classList', $this->classList);
        $this->view->assign('addressList',$this->addressList);
        $this->view->assign('deptList',$this->deptList);
        
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("v")
                ->join(["vendorclass"=>"vc"],"v.VC_Num = vc.VC_Num")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            // $rows = $list->items();
            // foreach($rows as $k=>$v){
            //     $rows[$k]["VC_Num"] = $this->classList[$v["VC_Num"]]??"";
            // }
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 下拉联动修改编码
     */
    public function customerType()
    {
        $num = trim($this->request->post("num"));
        $V_Num = "";
        if(isset($this->classList[$num])){
            $firstNum = $this->classList[$num]?$num:trim(key($this->classList));
            $row = $this->model->where("V_Num", 'LIKE', $firstNum.'%')->order("V_Num desc")->find();
            $V_Num = $row?$row["V_Num"]:0;
            $V_Num = $V_Num?("0".(++$V_Num)):($firstNum.'001');
            if($num != substr($V_Num,0,2)) return json(["code"=>0,"msg"=>"请增加新的供应商类别后在添加！"]);
        }
        return json(["code"=>1,"data"=>["V_Num"=>$V_Num]]);
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $result = false;
                    $V_Num = $params["V_Num"];
                    $one = $this->model->where("V_Num",$V_Num)->find();
                    if($one) $this->error('已经存在供应商编码，请重试。');
                    $params['AD_Name'] = $params['AD_Name']?$params['AD_Name']:"[全部]";
                    $params["V_Writer"] = $this->admin["nickname"];
                    $params["V_EditDate"] = date("Y-m-d H:i:s");
                    $result = $this->model->allowField(true)->save($params);
                    if ($result !== false) {
                        $main = [
                            "cvenabbname" => $params["V_ShortName"]?$params["V_ShortName"]:$params["V_Name"],
                            "cvencode" => $params["V_Num"],
                            "cvccode" => $params["VC_Num"],
                            "cvenname" => $params["V_Name"]
                        ];
                        $data = [
                            "main" => $main
                        ];
                        $url = "/PostVendor/Add";
                        $result = api_post_u($url,$data);
                        $this->success();
                    } else {
                        $this->error(__('No rows were inserted'));
                    }
                }else{
                    $this->error($validate->getError());
                }
                
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }
    
     /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $validate = new Validate($this->rule, $this->message);
                $msg = $validate->check($params);
                if($msg){
                    $result = false;
                    $params['AD_Name'] = $params['AD_Name']?$params['AD_Name']:"[全部]";
                    $params["V_Updater"] = $this->admin["nickname"];
                    $result = false;
                    $result = $row->allowField(true)->save($params);
                    if ($result !== false) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were updated'));
                    }
                }else{
                    $this->error($validate->getError());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row["VC_Name"] = $this->classList[$row["VC_Num"]];
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
