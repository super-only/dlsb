<?php

namespace app\admin\controller\logisticsbolts\purchase;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class ApplyList extends Backend
{
    
    /**
     * ApplylistXl模型对象
     * @var \app\admin\model\logisticsbolts\purchase\ApplylistJgj
     */
    protected $model = null, $detailModel = null, $admin='';

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\fastening\ApplylistJgj;
        $this->detailModel = new \app\admin\model\chain\fastening\ApplydetailsJgj;
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

     /**
     * 查看
     */
    public function index()
    {
        $this->assignconfig("status",[
            0 => "未采购完成",
            1 => "已采购"
        ]);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("al")
                ->join(["applydetails_jgj"=>"ad"],"al.AL_Num=ad.AL_Num")
                ->field("al.AL_Num,ad.AD_ID,ad.IM_Num,ad.IM_Class,ad.IM_Spec,ad.L_Name,ad.AD_Length,(ad.AD_Count*al.scd_count) as AD_Count,(ad.AD_Weight*al.scd_count) as AD_Weight,al.AL_Writer,al.AL_WriterDate,ad.AD_Memo,ad.ad_cp_check,ad.AD_BuyCount,ad.AD_BuyWeight,((ad.AD_Count*al.scd_count)-AD_BuyCount) as sy_count,((ad.AD_Weight*al.scd_count)-AD_BuyWeight) as sy_weight")
                ->where($where)
                ->where("al.Modifyer","<>","")
                ->where("al.al_reciever",$this->view->admin["id"])
                ->order("ad.ad_cp_check asc,al.AL_WriteDate desc,ad.L_Name asc,AD_Length ASC")
                ->paginate($limit);
            foreach($list as &$v){
                foreach(["AD_Length","AD_Count","AD_Weight","AD_BuyWeight","AD_BuyCount","sy_count","sy_weight"] as $vv){
                    $v[$vv] = round($v[$vv],5);
                }
            }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 完成
     */
    public function auditor($ids = "")
    {
        $this->model = $this->detailModel;
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $result = $this->model->where($pk, 'in', $ids)->update([
                "ad_cp_check" => 1
            ]);
            if($result) $this->success("更新成功");
            else $this->error("更新失败");
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 取消
     */
    public function giveUp($ids = "")
    {
        $this->model = $this->detailModel;
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $result = $this->model->where($pk, 'in', $ids)->update([
                "ad_cp_check" => 0
            ]);
            if($result) $this->success("更新成功");
            else $this->error("更新失败");
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
}