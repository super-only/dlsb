<?php

namespace app\admin\controller\logisticsmatter\purchase;

use app\admin\model\chain\machine\WjStoreIn;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 发票记录
 *
 * @icon fa fa-circle-o
 */
class InvoiceMainWj extends Backend
{
    
    /**
     * InvoiceMainWj模型对象
     * @var \app\admin\model\logisticsmatter\purchase\InvoiceMainWj
     */
    protected $model = null,$admin='',$limberList = [],$wayList = [0=>"数量",1=>"重量"],$bzList = ["人民币"=>"人民币"],$businessType = [0=>"普通采购",1=>"其他"],$invioceList = [0=>"普通发票",1=>"专业发票",2=>"增值税发票"],$procurementType = [0=>"原材料",1=>"机物料",2=>"紧固件",3=>"线缆"];
    // protected $noNeedLogin = ["addMaterial","selectProcurement"];
    protected $relatedModel = null,$detailModel = null,$wjClassList;
    protected $noNeedLogin = ["selectStorein","addMaterial","selectInvoice"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\logisticsmatter\purchase\InvoiceMainWj;
        $this->detailModel = new \app\admin\model\logisticsmatter\purchase\InvoiceDetailWj;
        $this->admin = \think\Session::get('admin');
        [$deptList] = $this->deptType(1);
        $assign = [
            "tableField" => $this->getTableField(),
            "wayList" => $this->wayList,
            "bzList" => $this->bzList,
            "businessType" => $this->businessType,
            "invioceList" => $this->invioceList,
            "procurementType" => $this->procurementType,
            "deptList" => $deptList,
            "nickname" => $this->admin["username"],
            "vendorList" => $this->vendorNumList(),
            "statusList" => [0=>"未支付",1=>"部分支付",2=>"已支付"]
        ];
        $this->view->assign($assign);
        foreach($assign as $k=>$v){
            $this->assignconfig($k,$v);
        }
        $where = [
            "ParentNum" => ["=","01"],
            "WC_Name" => ["<>",""],
            "WC_Sort" => ["=","五金"]
        ];
        $warehouse_list = $this->wareclassList($where,1);
        $this->assignconfig("warehouse_list",$warehouse_list);
        $this->wjClassList = $this->wjInventClassList()[0];
        $this->assignconfig("wjClassList",$this->wjClassList);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            if(!$table) $this->error("发票详情不能为空");
            // $exList = [0=>"Y",1=>"W",2=>"J",3=>"X"];
            $ex = "W";
            //pm_num HT202304-0001
            $year = $ex."FP".date("Ym")."-";
            $lastOne = $this->model->where("invoice_num","LIKE",$year."%")->order("invoice_num desc")->value("invoice_num");
            if($lastOne) $lastOne = str_pad((substr($lastOne,-4)+1),4,0,STR_PAD_LEFT);
            else $lastOne = "0001";
            $params["invoice_num"] = $year.$lastOne;
            if(!$params["supplier"]) $this->error("供应商不能为空");
            $params["fax"] = $params["fax"]?$params["fax"]:0;
            $params["rate"] = $params["rate"]?$params["rate"]:1;
            $params["writer"] = $this->admin["nickname"];
            $params["writer_time"] = date("Y-m-d H:i:s");
            $saveTable = [];
            foreach($table["id"] as $k=>$v){
                // $way = $table["way"][$k];
                $count = ($table["count"][$k]?$table["count"][$k]:0);
                $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                $price = $count?round($amount/$count,5):0;
                $fax_amount = $amount*0.01*$params["fax"];
                $sum_amount = $amount+$fax_amount;
                $fax_price = $count?round($sum_amount/$count,5):0;
                $saveTable[$k] = [
                    "id" => $v,
                    "invoice_num" => $params["invoice_num"],
                    "sid_id" => $table["sid_id"][$k],
                    "im_num" => $table["im_num"][$k],
                    // "l_name" => $table["l_name"][$k],
                    // "length" => $table["length"][$k]?$table["length"][$k]:0,
                    // "width" => $table["width"][$k]?$table["width"][$k]:0,
                    "unit" => $table["unit"][$k],
                    "count" => $table["count"][$k]?$table["count"][$k]:0,
                    // "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                    // "way" => $way,
                    "price" => $price,
                    "fax_price" => $fax_price,
                    "amount" => $amount,
                    "fax_amount" => $fax_amount,
                    "sum_amount" => $sum_amount,
                    "memo" => $table["memo"][$k],
                ];
                if(!$v) unset($saveTable[$k]["id"]);
            }
            $params["amount"] = array_sum(array_column($saveTable,'sum_amount'));
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params);
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->assignconfig("init_data",[]);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $flag = 1;
        if($row["auditor"]=="") $flag = 0;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $detailList = $this->detailModel->alias("d")
            ->join(["wjinventoryrecord"=>"ir"],"d.im_num=ir.IR_Num")
            ->field("d.*,ir.IR_Name,ir.IR_Spec,ir.sortNum")
            ->where("d.invoice_num",$ids)
            ->select();
        $detailList = $detailList?collection($detailList)->toArray():[];
        $detailIdList = [];
        foreach($detailList as $k=>$v){
            $detailIdList[$v["id"]] = $v["id"];
            $detailList[$k]["sortNum"] = $this->wjClassList[$v["sortNum"]];
        }
        [$tbodyField,$tableSidList] = $this->_getTbodyField($detailList,$this->getTableField(),1);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            if(!$table) $this->error("发票详情不能为空");
            $saveTable = [];
            foreach($table["id"] as $k=>$v){
                // $way = $table["way"][$k];
                $count = ($table["count"][$k]?$table["count"][$k]:0);
                $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                $price = $count?round($amount/$count,5):0;
                $fax_amount = $amount*0.01*$params["fax"];
                $sum_amount = $amount+$fax_amount;
                $fax_price = $count?round($sum_amount/$count,5):0;
                $saveTable[$k] = [
                    "id" => $v,
                    "invoice_num" => $ids,
                    "sid_id" => $table["sid_id"][$k],
                    "im_num" => $table["im_num"][$k],
                    // "l_name" => $table["l_name"][$k],
                    // "length" => $table["length"][$k]?$table["length"][$k]:0,
                    // "width" => $table["width"][$k]?$table["width"][$k]:0,
                    "unit" => $table["unit"][$k],
                    "count" => $table["count"][$k]?$table["count"][$k]:0,
                    // "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                    // "way" => $way,
                    "price" => $price,
                    "fax_price" => $fax_price,
                    "amount" => $amount,
                    "fax_amount" => $fax_amount,
                    "sum_amount" => $sum_amount,
                    "memo" => $table["memo"][$k],
                ];
                if(!$v) unset($saveTable[$k]["id"]);
                else if(isset($detailIdList[$k])){
                    unset($detailIdList[$k]);
                }
            }

            $params["amount"] = array_sum(array_column($saveTable,'sum_amount'));
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params,["invoice_num"=>$ids]);
                if(!empty($detailIdList)) $this->detailModel->where("id","in",$detailIdList)->delete();
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->assignconfig("tableSidList",$tableSidList);
        $this->view->assign("tbodyField", $tbodyField);
        $this->view->assign("row", $row);
        // $this->assignconfig("init_data",$init_data);
        $this->assignconfig("ids",$ids);
        $this->view->assign("flag",$flag);
        $this->assignconfig("flag",$flag);
        return $this->view->fetch();
    }

     public function selectStorein($v_num='')
     {
         if(!$v_num) $this->error("请选择供应商后重新点击入库单信息");
         //设置过滤方法
         $this->request->filter(['strip_tags', 'trim']);
         if ($this->request->isAjax()) {
             //如果发送的来源是Selectpage，则转发到Selectpage
             if ($this->request->request('keyField')) {
                 return $this->selectpage();
             }
             list($where, $sort, $order, $offset, $limit) = $this->buildparams();
             //合同管理——到货——入库——发票互相关
             $buildSql = (new WjStoreIn())->alias("wsi")
                ->join(["wjstoreindetail"=>"wsid"],"wsi.RK_ID=wsid.RK_ID")
                ->join(["wjinventoryrecord"=>"ir"],"wsid.IR_Num=ir.IR_Num")
                ->field("wsid.RKDe_ID as sid_id,wsi.RK_Num,wsi.RK_Date,wsi.WC_Num,wsid.IR_Num as im_num,ir.IR_Name,ir.sortNum,ir.IR_Spec,ir.IR_Unit as unit,wsid.RKDe_Count as count,wsid.RKDe_NotaxPrice as price")
                ->where("wsi.V_Num",$v_num)
                ->where("wsi.Assessor","<>","")
                ->buildSql();
             $list = DB::TABLE($buildSql)->alias("a")
                ->where($where)
                ->order($sort,$order)
                ->paginate($limit);
             $result = array("total" => $list->total(), "rows" => $list->items());
 
             return json($result);
         }
         $this->assignconfig("rsclist",["到货入库"=>"到货入库","盘盈入库"=>"盘盈入库"]);
         $this->assignconfig("v_num",$v_num);
         return $this->view->fetch();
     }
    
    public function addMaterial()
    {
        $params = $this->request->post("data");
        $params = json_decode($params,true);
        $tableField = $this->getTableField();
        [$field,$sidList] = $this->_getTbodyField($params,$tableField);
        return json(["code"=>1,"data"=>["field"=>$field,"sidList"=>$sidList]]);
    }

    protected function _getTbodyField($params,$tableField,$type=0)
    {
        $field = "";
        $sidList = [];
        foreach($params as $k=>$v){
            if($type) $sidList[$v["sid_id"]] = $v["sid_id"];
            $field .= '<tr>'
                    . '<td>'
                    . '<a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a>'
                    . '</td>';
            foreach($tableField as $vv){
                if($type) $value = $v[$vv[1]]??'';
                else $value = $v[$vv[3]]??'';
                $field .= '<td '.$vv[6].'><input type="'.$vv[2].'" class="small_input" '.$vv[4].' name="table['.$vv[1].'][]" value="'.($vv[1]=="id"?0:($value??$vv[5])).'"></td>';
            }
            $field .= "</tr>";
        }
        return [$field,array_values($sidList)];
    }
    
    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->select();
            $newIdList = [];
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                    $newIdList[$v[$pk]] = $v[$pk];
                }
                $this->detailModel->where($pk,"IN",$newIdList)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function auditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->update([
                    "auditor" => $this->admin["username"],
                    "auditor_time" => date("Y-m-d H:i:s")
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function giveUp($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","<>","")->where("status","=",0)->update([
                    "auditor" => '',
                    "auditor_time" => "0000-00-00 00:00:00"
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["id","id","text","","readonly",0,"hidden"],
            // ["入库单号","SI_OtherID","text","SI_OtherID","readonly","",""],
            ["入库ID","sid_id","text","sid_id","readonly","",""],
            ["材料编码","im_num","text","im_num","readonly","",""],
            ["材料名称","IR_Name","text","IR_Name","readonly","",""],
            ["材料类别","sortNum","text","sortNum","readonly","",""],
            ["材料规格","IR_Spec","text","IR_Spec","readonly","",""],
            // ["材质","l_name","text","L_Name","readonly","",""],
            // ["长度(mm)","length","number","SID_Length","readonly","",""],
            // ["宽度(mm)","width","number","SID_Width","readonly","",""],
            ["单位","unit","select","unit","readonly","",""],
            ["数量","count","number","count","readonly","",""],
            // ["比重","IM_PerWeight","text","","readonly",0,"hidden"],
            // ["重量","weight","number","SID_FactWeight","readonly","",""],
            // ["单价计算","way","select","way","readonly","",""],
            ["单价","price","number","price","","",""],
            ["含税单价","fax_price","number","","readonly","",""],
            ["金额","amount","number","","readonly","",""],
            ["税额","fax_amount","number","","readonly","",""],
            ["价税合计","sum_amount","number","","readonly","",""],
            ["备注","memo","text","","","",""]
        ];
        return $list;
    }
}
