<?php

namespace app\admin\controller\logisticsmatter\purchase;

use app\admin\model\logisticsmatter\purchase\PayListWjView;
use app\common\controller\Backend;

/**
 * 付款管理
 *
 * @icon fa fa-circle-o
 */
class PayList extends Backend
{
    
    /**
     * PayList模型对象
     * @var \app\admin\model\logisticsmatter\purchase\PayStatusWjView
     */
    protected $model = null, $admin='',$bzList = ["人民币"=>"人民币"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\logisticsmatter\purchase\PayStatusWjView;
        // $this->admin = \think\Session::get('admin');
        $assign = [
            "bzList" => $this->bzList,
            "vendorList" => $this->vendorNumList(),
            // "nickname" => $this->admin["username"]
        ];
        $this->view->assign($assign);
        foreach($assign as $k=>$v){
            $this->assignconfig($k,$v);
        }
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    public function detail()
    {
        $this->model = new PayListWjView();
        return parent::index();
    }
}
