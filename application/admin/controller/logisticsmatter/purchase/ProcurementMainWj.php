<?php

namespace app\admin\controller\logisticsmatter\purchase;

use app\admin\model\chain\machine\ApplydetailsJwl;
use app\admin\model\chain\machine\MaterialGetNoticeJwl;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 合同管理
 *
 * @icon fa fa-circle-o
 */
class ProcurementMainWj extends Backend
{
    
    /**
     * ProcurementMainWj模型对象
     * @var \app\admin\model\logisticsmatter\purchase\ProcurementMainWj
     */
    protected $model = null,$admin='',$limberList = [],$wayList = [0=>"数量",1=>"重量"],$bzList = ["人民币"=>"人民币"],$procurementType = [0=>"原材料",1=>"机物料",2=>"紧固件",3=>"线缆"],$classList=[];
    protected $noNeedLogin = ["addMaterial","selectProcurement","selectProcurementList"];
    protected $relatedModel = null,$detailModel = null,$viewModel=null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\logisticsmatter\purchase\ProcurementMainWj;
        $this->detailModel = new \app\admin\model\logisticsmatter\purchase\ProcurementDetailWj;
        // $this->viewModel = new \app\admin\model\chain\purchase\DhProcurementView;
        $this->admin = \think\Session::get('admin');
        $this->classList = $this->wjInventClassList()[0];
        $this->assignconfig("classList",$this->classList);
        // $this->assignconfig("tableField",$tableField);
        //合同类型 币种 部门
        $purchaseList = $this->purchaseList(1);
        [$deptList] = $this->deptType(1);
        // $this->measurementList = $this->measurementList();
        $assign = [
            "tableField" => $this->getTableField(),
            "purchaseList" => $purchaseList,
            "deptList" => $deptList,
            "bzList" => $this->bzList,
            "wayList" => $this->wayList,
            "nickname" => $this->admin["username"],
            "procurement_type" => $this->procurementType,
            "vendorList" => $this->vendorNumList()
        ];
        $this->view->assign($assign);
        foreach($assign as $k=>$v){
            $this->assignconfig($k,$v);
        }
        [$limberList] = $this->getLimber();
        $this->limberList = $limberList;
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            if(!$table) $this->error("合同详情不能为空");
            // pri($params,$table,1);
            //pm_num HT202304-0001
            if(!$params["pm_num"]){
                $year = "HTY".date("Ym")."-";
                $lastOne = $this->model->where("pm_num","LIKE",$year."%")->order("pm_num desc")->value("pm_num");
                if($lastOne) $lastOne = str_pad((substr($lastOne,-4)+1),4,0,STR_PAD_LEFT);
                else $lastOne = "0001";
                $params["pm_num"] = $year.$lastOne;
            }else{
                $findOne = $this->model->where("pm_num","=",$params["pm_num"])->find();
                if($findOne) $this->error("该编号已存在，请稍后再试");
            }
            $params["writer"] = $this->admin["nickname"];
            $params["writer_time"] = date("Y-m-d H:i:s");
            $saveTable = [];
            foreach($table["id"] as $k=>$v){
                // $way = $table["way"][$k];
                // $count = $way?($table["weight"][$k]?$table["weight"][$k]:0):($table["count"][$k]?$table["count"][$k]:0);
                // $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                // $price = $count?round($amount/$count,5):0;
                $saveTable[$k] = [
                    "id" => $v,
                    "pm_num" => $params["pm_num"],
                    "AD_ID" => $table["AD_ID"][$k],
                    "im_num" => $table["im_num"][$k],
                    // "l_name" => $table["l_name"][$k],
                    // "length" => $table["length"][$k]?$table["length"][$k]*1000:0,
                    // "width" => $table["width"][$k]?$table["width"][$k]*1000:0,
                    "unit" => $table["unit"][$k],
                    "count" => $table["count"][$k]?$table["count"][$k]:0,
                    // "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                    // "way" => $way,
                    "price" => $table["price"][$k]?$table["price"][$k]:0,
                    "amount" => $table["amount"][$k]?$table["amount"][$k]:0,
                    "memo" => $table["memo"][$k],
                ];
                if(!$v) unset($saveTable[$k]["id"]);
            }
            $params["count"] = array_sum(array_column($saveTable,'count'));
            // $params["weight"] = array_sum(array_column($saveTable,'weight'));
            $params["amount"] = array_sum(array_column($saveTable,'amount'));
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params);
                // if(!empty($qgList)) $qgResult = $this->relatedModel->allowField(true)->saveAll($qgList);
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        // $this->assignconfig("init_data",[]);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $flag = 1;
        if($row["auditor"]=="") $flag = 0;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $detailList = $this->detailModel->alias("d")
            ->join(["wjinventoryrecord"=>"im"],"d.IM_Num = im.IR_Num")
            // ->join(["wjinventorysort"=>"wis"],"wis.IS_Num=im.sortNum")
            ->field("d.id,d.im_num,im.IR_Name,im.IR_Spec,im.sortNum,d.unit,d.count,d.amount,d.price,d.memo,d.AD_ID")
            ->where("d.pm_num",$ids)
            ->select();
        $detailList = $detailList?collection($detailList)->toArray():[];
        $detailIdList = array_column($detailList,'id');
        // $detailIdList = $this->detailModel->where("pm_num",$ids)->column("id");
        $tbodyField = $this->_getTbodyField($detailList,1);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            if(!$table) $this->error("合同详情不能为空");
            $saveTable = [];
            foreach($table["id"] as $k=>$v){
                // $way = $table["way"][$k];
                // $count = $way?($table["weight"][$k]?$table["weight"][$k]:0):($table["count"][$k]?$table["count"][$k]:0);
                // $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                // $price = $count?round($amount/$count,5):0;
                $saveTable[$k] = [
                    "id" => $v,
                    "pm_num" => $ids,
                    "AD_ID" => $table["AD_ID"][$k],
                    "im_num" => $table["im_num"][$k],
                    // "l_name" => $table["l_name"][$k],
                    // "length" => $table["length"][$k]?$table["length"][$k]*1000:0,
                    // "width" => $table["width"][$k]?$table["width"][$k]*1000:0,
                    "unit" => $table["unit"][$k],
                    "count" => $table["count"][$k]?$table["count"][$k]:0,
                    // "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                    // "way" => $way,
                    "amount" => $table["amount"][$k]?$table["amount"][$k]:0,
                    "price" => $table["price"][$k]?$table["price"][$k]:0,
                    "memo" => $table["memo"][$k],
                ];
                if(!$v) unset($saveTable[$k]["id"]);
                else if(in_array($v,$detailIdList)){
                    $wz = array_search($v,$detailIdList);
                    unset($detailIdList[$wz]);
                }
            }
            $params["count"] = array_sum(array_column($saveTable,'count'));
            // $params["weight"] = array_sum(array_column($saveTable,'weight'));
            $params["amount"] = array_sum(array_column($saveTable,'amount'));
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params,["pm_num"=>$ids]);
                // $this->relatedModel->where("pm_num",$ids)->delete();
                // if(!empty($qgList)) $qgResult = $this->relatedModel->allowField(true)->saveAll($qgList);
                if(!empty($detailIdList)) $this->detailModel->where("id","in",$detailIdList)->delete();
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->view->assign("tbodyField", $tbodyField);
        $this->view->assign("row", $row);
        // $this->assignconfig("init_data",$init_data);
        $this->assignconfig("ids",$ids);
        $this->view->assign("flag",$flag);
        $this->assignconfig("flag",$flag);
        return $this->view->fetch();
    }

    public function selectProcurementList($v_num='')
    {
        $iniWhere = [];
        if($v_num) $iniWhere["supplier"] = ["=",$v_num];
        // $iniWhere["procurement_type"] = ["=",$type];
        //type后续使用
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $buildSql = $this->model->alias("pm")
                ->join(["procurement_detail_wj"=>"pd"],"pm.pm_num=pd.pm_num")
                ->join(["wjinventoryrecord"=>"im"],"pd.im_num=im.IR_Num")
                ->field("`pm`.`pm_num` AS `pm_num`,
                `pm`.`name` AS `name`,
                `pm`.`type` AS `type`,
                `pm`.`unit` AS `supplier`,
                `pm`.`header` AS `header`,
                `pm`.`sign_date` AS `sign_date`,
                `pm`.`currency` AS `currency`,
                `pm`.`rate` AS `rate`,
                `pm`.`department` AS `department`,
                `pm`.`salesman` AS `salesman`,
                `pm`.`writer` AS `writer`,
                `pm`.`writer_time` AS `writer_time`,
                `pm`.`auditor` AS `auditor`,
                `pm`.`auditor_time` AS `auditor_time`,
                `pm`.`update_time` AS `update_time`,
                `pm`.`procurement_type` AS `procurement_type`,
                `pd`.`id` AS `pm_id`,
                `pd`.`im_num` AS `im_num`,
                `pd`.`AD_ID` AS `AD_ID`,
                `pd`.`l_name` AS `l_name`,
                `pd`.`length` AS `length`,
                `pd`.`width` AS `width`,
                `pd`.`unit` AS `unit`,
                `pd`.`count` AS `count`,
                `pd`.`weight` AS `weight`,
                `pd`.`way` AS `way`,
                `pd`.`amount` AS `amount`,
                `pd`.`price` AS `price`,
                `im`.`IR_Spec` AS `IR_Spec`,
                0 AS `IM_PerWeight`,
                `im`.`IR_Name` AS `IR_Name`,
                `im`.`sortNum` AS `sortNum` ")
                ->where("pm.auditor","<>","")
                ->buildSql();
            $list = DB::TABLE($buildSql)->alias("a")
                ->where($iniWhere)
                ->order($sort,$order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        $this->assignconfig("v_num",$v_num);
        // $this->assignconfig("type",$type);
        return $this->view->fetch();
    }

    public function addMaterial()
    {
        $params = $this->request->post("data");
        $params = json_decode($params,true);
        // $tableField = $this->getTableField();
        $field = $this->_getTbodyField($params);
        return json(["code"=>1,"data"=>$field]);
    }

    protected function _getTbodyField($params=[],$type=0)
    {
        $field = "";
        foreach($params as $k=>$v){
            $field .= '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                    . '<td hidden=""><input type="text" class="small_input" readonly="" name="table[id][]" value="'.($type?$v["id"]:0).'"></td>'
                    . '<td><input type="text" class="small_input" readonly="" name="table[AD_ID][]" value="'.($type?$v["AD_ID"]:$v["AD_ID"]).'"></td>'
                    . '<td><input type="text" class="small_input" readonly="" name="table[im_num][]" value="'.($type?$v["im_num"]:$v["IM_Num"]).'"></td>'
                    . '<td><input type="text" class="small_input" readonly="" name="table[IR_Name][]" value="'.$v["IR_Name"].'"></td>'
                    . '<td><input type="text" class="small_input" readonly="" name="table[IR_Spec][]" value="'.($type?$v["IR_Spec"]:$v["IR_Spec"]).'"></td>'
                    . '<td><input type="text" class="small_input" readonly="" value="'.$this->classList[$v["sortNum"]].'"></td>'
                    . '<td><input type="text" class="small_input" readonly="" name="table[unit][]" value="'.($type?$v["unit"]:$v['IR_Unit']).'"></td>'
                    . '<td><input type="number" class="small_input" name="table[count][]" value="'.round(($type?$v["count"]:$v["sy_count"]),3).'"></td>'
                    . '</td><td><input type="number" class="small_input" name="table[price][]" value="'.round(($type?$v["price"]:''),5).'"></td>'
                    . '<td><input type="number" class="small_input" name="table[amount][]" value="'.round(($type?$v["amount"]:''),5).'"></td>'
                    . '<td><input type="text" class="small_input" name="table[memo][]" value="'.($type?$v["memo"]:'').'"></td></tr>';
        }
        return $field;
    }
     
    //编辑table
    public function getTableField()
    {
        $list = [
            ["id","id","text","","readonly",0,"hidden"],
            ["请购ID","AD_ID","text","","readonly","",""],
            ["材料编号","im_num","text","","readonly","",""],
            ["材料名称","IR_Name","text","","readonly","",""],
            ["规格","IR_Spec","text","","readonly","",""],
            ["类别","sortNum","text","","disabled","",""],
            // ["长度(m)","length","number","","","",""],
            // ["宽度(m)","width","number","","","",""],
            ["单位","unit","text","","readonly","",""],
            ["数量","count","number","","","",""],
            // ["比重","IM_PerWeight","text","","readonly",0,"hidden"],
            // ["重量","weight","number","","","",""],
            // ["单价计算","way","select","","","",""],
            ["单价","price","number","","","",""],
            ["金额","amount","number","","","",""],
            ["备注","memo","text","","","",""]
        ];
        return $list;
    }/**
     * 审核 
     * 核对采购数量是否超过请购数量
     * 核对是否存在无材料编号和无金额的情况
     */
    public function checkAuditor()
    {
        $ids = $this->request->post("ids");
        $row  = $this->model->get($ids);
        if(!$row) $this->error("不存在该合同编号，审核失败");
        $list = $this->detailModel->where("pm_num",$ids)->select();
        $list = $list?collection($list)->toArray():[];
        if(empty($list)) $this->error("不存在合同明细，请先添加合同明细后再进行审核操作");
        foreach($list as $k=>$v){
            if(!$v["im_num"] or round($v["amount"],5)==0) $this->error("存在缺少材料编号或者没有金额的，请检查");
        }
        $applyIdList = array_column($list,'AD_ID');
        $msgList = $this->compact($ids,$applyIdList)[0];
        if(!empty($msgList)) $this->success("请购ID为".implode($msgList)."的采购数量或者重量已超过请购需要量;");
        else $this->success();
    }

    /**
     * 请购和采购比较
     * type 0 超过
     * type 1 大于等于
     */
    protected function compact($ids,$applyIdList,$type=0)
    {
        // 请购数量
        $appNews = (new ApplydetailsJwl())->where("AD_ID","IN",$applyIdList)->column("AD_ID,AD_Count AS count,AD_Weight as weight");
        // 采购数量
        $prodetailNews = $this->model->alias("pm")
            ->join(["procurement_detail_wj"=>"pd"],"pm.pm_num=pd.pm_num")
            ->where("pd.AD_ID","IN",$applyIdList)
            ->where(function ($query) use ($ids) {
                $query->where('pm.auditor',"<>","")->whereor('pm.pm_num', $ids);
            })
            ->group("AD_ID")->column("pd.AD_ID,sum(pd.count) as count,sum(pd.weight) as weight");
        $msgList = [];
        foreach($prodetailNews as $k=>$v){
            $needCount = isset($appNews[$k])?round($appNews[$k]["count"],5):0;
            $needWeight = isset($appNews[$k])?round($appNews[$k]["weight"],5):0;
            $v["count"] = round($v["count"],5);
            $v["weight"] = round($v["weight"],5);
            if($type){
                if($v["count"]>=$needCount or $v["weight"]>=$needWeight) $msgList[$k] = $k;
            }else{
                if($v["count"]>$needCount or $v["weight"]>$needWeight) $msgList[$k] = $k;
            }
            
        }
        return [$msgList,$prodetailNews];
    }

    public function checkGive()
    {
        $ids = $this->request->post("ids");
        $row  = $this->model->get($ids);
        if(!$row) $this->error("不存在该合同编号，审核失败");
        $list = $this->detailModel->where("pm_num",$ids)->select();
        $list = $list?collection($list)->toArray():[];
        if(empty($list)) $this->error("不存在合同明细，请先添加合同明细后再进行审核操作");
        $applyIdList = array_column($list,'id');
        $one = (new MaterialGetNoticeJwl())->where("pm_id","in",$applyIdList)->find();
        if($one) $this->error("已存在部分到货，无法弃审");
        else $this->success();
    }

    /**
     * 删除
     * 需要核对修改 如果已到货 不可删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->select();
            $newIdList = [];
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                    $newIdList[$v[$pk]] = $v[$pk];
                }
                $this->detailModel->where($pk,"IN",$newIdList)->delete();
                // $this->relatedModel->where($pk,"IN",$newIdList)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     * 审核通过同步修改请购已购信息
     * 重量和数量 有大于等于的 即已购入
     */
    public function auditor()
    {
        $ids = $this->request->post("ids");
        $row  = $this->model->get($ids);
        if(!$row) $this->error("不存在该合同编号，审核失败");
        $list = $this->detailModel->where("pm_num",$ids)->select();
        $list = $list?collection($list)->toArray():[];
        $applyIdList = array_column($list,'AD_ID');
        [$msgList,$prodetailNews] = $this->compact($ids,$applyIdList,1);
        $saveDetail = [];
        foreach($prodetailNews as $k=>$v){
            $saveDetail[$k] = [
                "AD_ID" => $k,
                "AD_BuyCount" => $v["count"],
                "AD_BuyWeight" => $v["weight"],
                "AD_BuyDate" => $row["writer_time"],
                "ad_cp_check" => 0
            ];
            if(isset($msgList[$k])) $saveDetail[$k]["ad_cp_check"] = 1;
        }
        Db::startTrans();
        try {
            (new ApplydetailsJwl())->allowField(true)->saveAll($saveDetail);
            $result = $this->model->where("pm_num",$ids)->update([
                "auditor" => $this->admin["username"],
                "auditor_time" => date("Y-m-d H:i:s")
            ]);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        
        if($result) $this->success("审核成功");
        else $this->error("审核失败，请稍后再试");
    }

    /**
     * 弃审
     * 
     */
    public function giveUp($ids = "")
    {
        $ids = $this->request->post("ids");
        $row  = $this->model->get($ids);
        if(!$row) $this->error("不存在该合同编号，审核失败");
        $list = $this->detailModel->where("pm_num",$ids)->select();
        $list = $list?collection($list)->toArray():[];
        $applyIdList = array_column($list,'AD_ID');
        [$msgList,$prodetailNews] = $this->compact($ids,$applyIdList,1);
        $saveDetail = [];
        foreach($prodetailNews as $k=>$v){
            $saveDetail[$k] = [
                "AD_ID" => $k,
                "AD_BuyCount" => $v["count"],
                "AD_BuyWeight" => $v["weight"],
                "AD_BuyDate" => $row["writer_time"],
                "ad_cp_check" => 0
            ];
            if(isset($msgList[$k])) $saveDetail[$k]["ad_cp_check"] = 1;
        }
        Db::startTrans();
        try {
            (new ApplydetailsJwl())->allowField(true)->saveAll($saveDetail);
            $result = $this->model->where("pm_num",$ids)->update([
                "auditor" => '',
                "auditor_time" => "0000-00-00 00:00:00"
            ]);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if($result) $this->success("弃审成功");
        else $this->error("弃审失败，请稍后再试");
    }
}
