<?php

namespace app\admin\controller\quality\process;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 镀锌
 *
 * @icon fa fa-circle-o
 */
class Galvanized extends Backend
{
    
    /**
     * Galvanized模型对象
     * @var \app\admin\model\quality\process\Galvanized
     */
    protected $model = null;
    protected $noNeedLogin = ["*"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\quality\process\GalvanizedTrue;
        // $this->tempModel = new \app\admin\model\quality\process\DevDuXin;
        $this->admin = \think\Session::get('admin');
        $this->view->assign("admin",$this->admin["nickname"]);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index($ids=null)
    {
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->where("ppm_id",$ids)
                ->order($sort, $order)
                ->select();

                $result = array("total" => count($list), "rows" => $list);

            return json($result);
        }
        $this->assignconfig("ppm_id",$ids);
        return $this->view->fetch();
    }

    public function selectTemp()
    {
        $this->model = new \app\admin\model\quality\process\DevDuXin();
        return parent::index();
    }

    /**
     * 添加
     */
    public function add($ppm_id=null)
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("data/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $data = [];
                foreach($params as $k=>$v){
                    $data[$k] = [
                        "id" => $v["id"],
                        "ppm_id" => $ppm_id,
                        "temp" => $v["temp"],
                        "time" => $v["time"]
                    ];
                    if(!$v["id"]) unset($data[$k]["id"]);
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->allowField(true)->saveAll($data);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
    }

}
