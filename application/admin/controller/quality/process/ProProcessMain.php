<?php

namespace app\admin\controller\quality\process;

use app\admin\model\chain\lofting\ProduceTaskDetail;
use app\admin\model\chain\lofting\UnionProduceTaskView;
use app\admin\model\chain\material\Configuresys;
use app\admin\model\quality\process\DevDuXin;
use app\admin\model\quality\process\GalvanizedTrue;
use app\admin\model\quality\process\ProTestDetailBz;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;


/**
 * 生产过程检验
 *
 * @icon fa fa-circle-o
 */
class ProProcessMain extends Backend
{
    
    /**
     * ProProcessMain模型对象
     * @var \app\admin\model\quality\process\ProProcessMain
     */
    protected $model = null;
    protected $noNeedLogin = ["*"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\quality\process\ProProcessMain;
        $this->detailModel = new \app\admin\model\quality\process\ProProcessDetail;
        $this->getTableField = $this->getTableField();
        $this->assignconfig("getTableField",$this->getTableField);
        $this->view->assign("getTableField",$this->getTableField);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    public function add()
    {
        //是否审核判断 不包了
        $findAud = $this->model->where("auditor","=","")->find();
        if($findAud) $this->error("先审核未审核的单子！");
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $PT_Num = $params["PT_Num"];
                $one = $this->model->where("PT_Num",$PT_Num)->value("id");
                if($one) $this->success('成功！',null,$one);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $defaultKey = $this->keyList();
                    foreach($defaultKey as $v){
                        if(isset($params[$v]) && $params[$v]==1) $this->model->where($v,1)->update([$v => 0]);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$this->model->id);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->assignconfig("ids",0);
        $this->assignconfig("dtmdList",[]);
        return $this->view->fetch();
    }

    public function edit($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $list = collection($this->detailModel->field(
            "*,CAST(sect AS UNSIGNED) AS number,case when (length(parts)-length(replace(parts,'-','')))>1 then SUBSTR(parts,1,LOCATE('-',parts)-1)*10000000+SUBSTR(SUBSTR(parts,LOCATE('-',parts)+1),1,LOCATE('-',SUBSTR(parts,LOCATE('-',parts)+1))-1)*10000+cast(SUBSTR(SUBSTR(parts,LOCATE('-',parts)+1),LOCATE('-',SUBSTR(parts,LOCATE('-',parts)+1))+1) as signed)
            when LOCATE('-',parts)>0 and LENGTH(cast(SUBSTR(parts,LOCATE('-',parts)+1) as signed))=3 then SUBSTR(parts,1,LOCATE('-',parts)-1)*10000000+SUBSTR(cast(SUBSTR(parts,LOCATE('-',parts)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(parts,LOCATE('-',parts)+1) as signed),1+1) as signed)
            when LOCATE('-',parts)>0 and LENGTH(cast(SUBSTR(parts,LOCATE('-',parts)+1) as signed))=4 then SUBSTR(parts,1,LOCATE('-',parts)-1)*10000000+SUBSTR(cast(SUBSTR(parts,LOCATE('-',parts)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(parts,LOCATE('-',parts)+1) as signed),2+1) as signed)
            when LENGTH(cast(parts as signed))=3 then SUBSTR(cast(parts as signed),1,1)*10000000+cast(SUBSTR(cast(parts as signed),1+1) as signed)
            when LENGTH(cast(parts as signed))=4 then SUBSTR(cast(parts as signed),1,2)*10000000+cast(SUBSTR(cast(parts as signed),2+1) as signed)
            else 0 end 	bjbhn,count as xl_count,type as xl_type,creat_time as xl_creat_time,CONCAT(hg_count,',',cj_count) AS xl_hg_count,count as zk_count,(case when kong='1' then '钻孔' else '冲孔' end) as zk_kong,sum_hole as zk_sum_hole,CONCAT(hg_count,',',cj_count) AS zk_hg_count,(case when bending<>'0' then count else '' end) as zw_count,(case bending when '0' then '' when 1 then '冷弯' else '热弯' end) as zw_bending,(case when bending<>'0' then CONCAT(hg_count,',',cj_count) else '' end) as zw_hg_count,(case fire when '0' then '' when 1 then '一级' when 2 then '二级' else '三级' end) as hj_fire,(case when fire='0' then '' else CONCAT(processor,',',certificate) end) as hj_processor,(case when fire<>'0' then CONCAT(hg_count,',',cj_count) else '' end) as hj_hg_count,sum_weight as dx_sum_weight,late_weight as dx_late_weight,factory as dx_factory,gal_time as dx_gal_time"
        )->where("m_id",$ids)->order("number,bjbhn,parts")->select())->toArray();
        $dtmdList = [];
        foreach($list as $v){
            $dtmdList[$v["DtMD_ID_PK"]] = $v["DtMD_ID_PK"];
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                
                $field = array_keys($params);
                $partList = $saveDetail = [];
                // $pcSchedulingModel = (new PcScheduling());
                $gal_rate = (new Configuresys())->where("SetName",'gal_rate')->value("SetValue");
                $gal_rate = $gal_rate?$gal_rate:0;
                foreach($params["id"] as $k=>$v){
                    if(isset($partList[$params["parts"][$k]])) $this->error($params["parts"][$k]."部件号重复");
                    $partList[$params["parts"][$k]] = $params["parts"][$k];
                    foreach($field as $vv){
                        $saveDetail[$k][$vv] = $params[$vv][$k];
                    }
                    $saveDetail[$k]["late_weight"] = $saveDetail[$k]["sum_weight"]*(1+$gal_rate);
                    
                    $saveDetail[$k]["m_id"] = $ids;
                    if(!$v) unset($saveDetail[$k]["id"]);
                }
                if(empty($saveDetail)) $this->error("无更新");
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->detailModel->allowField(true)->saveAll($saveDetail);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $flag = true;
        if($row["auditor"]) $flag = false;
        $this->assignconfig("flag",$flag);
        $this->view->assign("row", $row);
        $this->assignconfig("ids",$ids);
        $this->view->assign("list",$list);
        $this->assignconfig("row",$row);
        $this->assignconfig("list",$list);
        $this->assignconfig("dtmdList",array_values($dtmdList));
        return $this->view->fetch();
    }

    public function chooseDetail($pt_num=null)
    {
        if(!$pt_num) $this->error("有误请稍后重试");
        $ex = (new UnionProduceTaskView())->where("PT_Num",$pt_num)->value("sect_field");
        if ($this->request->isPost()) {
            $detailList = $this->model->alias("m")
                ->join(["pro_process_detail"=>"ppd"],"m.id=ppd.m_id")
                ->where("m.PT_Num",$pt_num)
                ->column("ppd.DtMD_ID_PK");

            $where = ["ptd.PT_Num"=>$pt_num];
            [$PT_Sect,$DtMD_sSpecification] = array_values($this->request->post());
            if($PT_Sect) $where["ptd.PT_Sect"] = ["=",$PT_Sect];
            if($DtMD_sSpecification) $where["dmd.DtMD_sSpecification"] = ["=",$DtMD_sSpecification];
            $list = (new ProduceTaskDetail([],$ex))->alias("ptd")
                ->join([$ex."dtmaterialdetial"=>"dmd"],"ptd.DtMD_ID_PK = dmd.DtMD_ID_PK")
                ->field("ptd.DtMD_ID_PK,ptd.PT_Num as pt_num,ptd.PT_Sect as sect,CAST(ptd.PT_Sect AS UNSIGNED) AS number,dmd.DtMD_sPartsID as parts,dmd.DtMD_sStuff as stuff,dmd.DtMD_sMaterial as material,dmd.DtMD_sSpecification as specification,DtMD_iLength as length,DtMD_fWidth as width,type,ptd.PTD_Count as count,dmd.DtMD_fUnitWeight as weight,round(ptd.PTD_Count*dmd.DtMD_fUnitWeight,2) as sum_weight,dmd.DtMD_iUnitHoleCount as hole_number,round(ptd.PTD_Count*dmd.DtMD_iUnitHoleCount,2) as sum_hole,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
                else 0 end 	bjbhn,DtMD_iFireBending as bending,DtMD_ZuanKong as kong,DtMD_iWelding as fire,0 as flag")
                ->where($where)
                ->order("number,bjbhn,DtMD_sPartsID")
                ->select();
            foreach($list as $k=>$v){
                $list[$k] = $v->toArray();
                $list[$k]["parts_Input"] = build_input('parts_Input', 'text', $v["parts"],["readonly"=>"readonly","class"=>'']);
                if(in_array($v["DtMD_ID_PK"],$detailList)) $list[$k]["flag"] = 1;
            }
            return json(["code"=>1,"data"=>$list]);
        }
        $this->assignconfig("pt_num",$pt_num);
        return $this->view->fetch();
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,'msg'=>"删除失败"]);
        $one = $this->model->alias("m")
            ->join(["pro_process_detail"=>"s"],"m.id=s.m_id")
            ->where("s.id",$num)
            ->find();
        if(!$one) return json(["code"=>0,'msg'=>"删除失败"]);
        else{
            if($one["auditor"]) return json(["code"=>0,'msg'=>"已审核，删除失败"]);
            else $this->detailModel->where("id",$num)->delete();
        }
        return json(["code"=>1,'msg'=>"删除成功"]);
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $list = $this->detailModel->where("m_id",$num)->select();
        $rule["nomal"] = [
            ["生产时间","creat_time"],
            ["抽检件数","cj_count"],
            ["合格件数","hg_count"],
            ["镀锌后重量","late_weight"],
            ["镀锌厂家","factory"],
            ["镀锌日期","gal_time"]
        ];
        $rule["fire"] = [
            ["生产时间","creat_time"],
            ["抽检件数","cj_count"],
            ["合格件数","hg_count"],
            ["镀锌后重量","late_weight"],
            ["镀锌厂家","factory"],
            ["镀锌日期","gal_time"],
            ["加工者","processor"],
            ["证书编号","certificate"],
        ];
        foreach($list as $v){
            if($v["fire"]) $ruleList = $rule["fire"];
            else $ruleList = $rule["nomal"];
            foreach($ruleList as $vv){
                if(!$v[$vv[1]]) return json(["code"=>0,"msg"=>"部件号".$v["parts"].'的'.$vv[0].'不能为空']);
            }
        }
        $row = $this->model->get($num);
        $url = ROOT_PATH . 'public' . DS . 'zlfiles' . DS . 'sjzjbg' . DS . $row["PT_Num"] . ".pdf";
        if(!is_file($url)) return json(["code"=>0,"msg"=>"请上传文件后再审核"]);

        // $tempRow = (new GalvanizedTrue())->where("ppm_id",$num)->find();
        // if(!$tempRow) return json(["code"=>0,"msg"=>"请选择镀锌温度点后再审核"]);

        $admin = \think\Session::get('admin');
        $admin = $admin["nickname"];
        $result = $this->model->where("id",$num)->update(['auditor'=>$admin,"auditor_time"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $result = $this->model->where("id",$num)->update(['auditor'=>"","auditor_time"=>"0000-00-00 00:00:00"]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $count = 0;
            Db::startTrans();
            try {
                $count += $this->model->where("id",$ids)->delete();
                $count += $this->detailModel->where("m_id",$ids)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function settingSpec()
    {
        $configModel = new Configuresys();
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $result = $configModel->where("SetName",'gal_rate')->update(["SetValue"=>$params["gal_rate"]]);
            if ($result != false) {
                $this->success();
            } else {
                $this->error(__('No rows were updated'));
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $configModel->field("SetName,SetValue")->where("SetName",'gal_rate')->find();
        $rows = ["gal_rate"=>""];
        if($list) $rows["gal_rate"] = $list["SetValue"];
        $this->view->assign("rows", $rows);
        return $this->view->fetch();
    }

    //编辑table
    public function getTableField()
    {
        //名字 name type 只读和data-id 对应名称 是否隐藏 默认值
        $list = [
            ["id","id","number","readonly","","hidden",""],
            ["DtMD_ID_PK","DtMD_ID_PK","number","readonly","DtMD_ID_PK","hidden",""],
            ["段号","sect","text","readonly","sect","",""],
            ["零件部号","parts","text","readonly","parts","",""],
            ["材料名称","stuff","text","readonly","stuff","",""],
            ["材质","material","text","readonly","material","",""],
            ["规格","specification","text","readonly","specification","",""],
            ["长度","length","number","readonly","length","",""],
            ["宽度","width","number","readonly","width","hidden",""],
            ["类型","type","text","readonly","type","",""],
            ["总数","count","number","readonly","count","",""],
            ["单量","weight","number","readonly","weight","hidden",""],
            ["总重量","sum_weight","number","readonly","sum_weight","",""],
            ["单件孔数","hole_number","number","readonly","hole_number","hidden",""],
            ["总孔数","sum_hole","number","readonly","sum_hole","",""],
            ["焊接","fire","text","readonly","fire","",""],
            ["制弯","bending","text","readonly","bending","",""],
            ["钻孔","kong","text","readonly","kong","",""],
        ];
        $data[0] = $list;
        $type_data[1] = [
            ["生产时间","creat_time","time","data-rule='required'","","",date("Y-m-d",strtotime("-2 days"))],
            ["抽检件数","cj_count","number","data-rule='required'","count","",""],
            ["合格件数","hg_count","number","data-rule='required'","count","",""],
            ["人员","peo","button","","","",""],
            ["加工者","processor","fire","","processor","",""],
            ["证书编号","certificate","fire","","certificate","",""],
            ["镀锌后重量","late_weight","number","","late_weight","",""],
            ["镀锌厂家","factory","text","data-rule='required'","","","绍兴电力设备有限公司"],
            ["镀锌日期","gal_time","time","data-rule='required'","","",date("Y-m-d")]
        ];
        foreach($type_data as $k=>$v){
            $data[$k] = $list;
            foreach($v as $kk=>$vv){
                $data[$k][] = $vv;
            }
        }
        return $data;
    }

    public function allowApi()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"失败"]);
        $params = $this->model->where("id",$num)->where("upload",0)->find();
        if(!$params) return json(["code"=>0,"msg"=>"不存在或者已上传，失败"]);
        $list = $this->detailModel
            ->field("count,type,creat_time,CONCAT(hg_count,',',cj_count) as hg_count,(case when kong=1 then '钻孔' when kong=0 then '冲孔' else '' end) as kong,hole_number,(case when bending=1 then '冷弯' when bending=2 then '热弯' else '' end) as bending,(case when fire=1 then '一级' when fire=2 then '二级' when fire=3 then '三级' else '' end) as fire,concat(processor,',',certificate) as processor,factory,gal_time,parts,material,specification,sum_weight,late_weight")
            ->where("m_id",$num)->select();
        if(!$list) return json(["code"=>0,"msg"=>"失败"]);
        $ini = [
            ["count" , "JGTG45" , "xl"],
            ["type" , "JGTG69" , "xl"],
            ["creat_time" , "JGTG46" , "xl"],
            ["hg_count" , "JGTG47" , "xl"],
            ["count" , "JGTG63" , "zk"],
            ["kong" , "JGTG48" , "zk"],
            ["hole_number" , "JGTG49" , "zk"],
            ["hg_count" , "JGTG50" , "zk"],
            ["count" , "JGTG64" , "zw"],
            ["bending" , "JGTG51" , "zw"],
            ["hg_count" , "JGTG52" , "zw"],
            ["fire" , "JGTG53" , "hj"],
            ["processor" , "JGTG54" , "hj"],
            ["hg_count" , "JGTG55" , "hj"],
            ["sum_weight" , "JGTG57" , "dx"],
            ["late_weight" , "JGTG58" , "dx"],
            ["factory" , "JGTG59" , "dx"],
            ["gal_time" , "JGTG60" , "dx"]
        ];
        $data = [];
        $ex = (new UnionProduceTaskView())->where("PT_Num",$params["PT_Num"])->value("sect_field");
        $sum_count = (new ProduceTaskDetail([],$ex))->where("PT_Num",$params["PT_Num"])->count("PTD_ID");
        $xl_count = $zk_count = $zw_count = $hj_count = $dx_count = 0;
        foreach($list as $v){
            $xl_count++;
            $zk_count++;
            $dx_count++;
            if($v["fire"]) $hj_count++;
            if($v["bending"]) $zw_count++;
            foreach($ini as $iv){
                if(!$v["fire"] and $iv[2]=="hj") continue;
                if(!$v["bending"] and $iv[2]=="zw") continue;
                $data[] = [
                    "PT_Num" => $params["PT_Num"],
                    "FIELD" => $iv[0],
                    "RULE_CODE" => $iv[1],
                    "UPLOAD" => 0,
                    "type" => 0,
                    "workmanship" => $iv[2],
                    "value" => $v[$iv[0]],
                    "rate" => 0,
                    "time" => $params["writer_time"],
                    "PART_CODE" => $v["parts"],
                    "MATMATERIAL" => $v["material"],
                    "MATSPEC" => $v["specification"]
                ];
            }
        }
        $xl = $zk = $dx = round($xl_count/$sum_count*100);
        $zw = round($zw_count/$sum_count*100);
        $hj = round($hj_count/$sum_count*100);
        $rateList = ["xl"=>$xl,"zk"=>$zk,"hj"=>$hj,"dx"=>$dx,"zw"=>$zw];
        foreach($data as $k=>$v){
            $data[$k]["rate"] = $rateList[$v["workmanship"]];
        }
        $result = false;
        Db::startTrans();
        try {
            $this->model->where("id",$num)->update(["upload"=>1]);
            $result = (new ProTestDetailBz())->saveAll($data);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $this->success("允许上传成功");
        } else {
            $this->error("允许上传失败");
        }
        
    }

    public function updateTemp()
    {
        $list = $this->model->alias("ppm")
            ->join(["product_work_view"=>"pwv"],"ppm.PT_Num=pwv.PT_Num")
            ->field("ppm.id,ppm.PT_Num,ppm.writer_time")
            ->select();
        $list = $list?collection($list)->toArray():[];
        $data = [];
        $model = new DevDuXin();
        $trueModel = new GalvanizedTrue();
        foreach($list as $k=>$v){
            $tempList = $model->where("sj","<=",$v["writer_time"])->order("sj desc")->limit(6)->select();
            foreach($tempList as $vv){
                $data[] = [
                    "ppm_id" => $v["id"],
                    "temp_id" => $vv["id"]
                ];
            }
        }
        pri($data,1);
        $trueResult = $trueModel->allowField(true)->saveAll($data);
        pri(count($trueResult),1);
    }

    public function partImport($ids=null)
    {
        $row = $this->model->get($ids);
        $old_url = ROOT_PATH . 'public' . DS . 'zlfiles' . DS . 'sjzjbg' . DS . $row["PT_Num"] . ".pdf";
        if(!is_file($old_url)) $html_url = '';
        else $html_url = DS . 'zlfiles' . DS . 'sjzjbg' . DS . $row["PT_Num"] . ".pdf";
        $this->view->assign("html_url",$html_url);
        if ($this->request->isPost()) {
            $filename = $this->request->post("filename");
            if(is_file($old_url)) unlink($old_url);
            if($filename){
                $new_url = ROOT_PATH . 'public' .$filename;
                rename($new_url,$old_url);
            }
            $this->success();
        }
        return $this->view->fetch();
    }

}
