<?php

namespace app\admin\controller\quality\experiment;

use app\admin\model\chain\material\CommisionTest;
use app\admin\model\chain\material\CommisionTestDetail;
use app\admin\model\quality\experiment\Recheck;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;

/**
 * 钢材试验
 *
 * @icon fa fa-circle-o
 */
class MaterialTest extends Backend
{
    
    /**
     * MaterialTest模型对象
     * @var \app\admin\model\quality\experiment\MaterialTest
     */
    protected $model = null;
    protected $noNeedLogin = ["allowApi"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\quality\experiment\MaterialTest;
        $this->detailModel = new \app\admin\model\quality\experiment\MaterialTestDetail;
        // $this->bzModel = new \app\admin\model\quality\experiment\MaterialTestDetailBz;
        $this->admin = \think\Session::get('admin');
        $this->assignconfig("time",date("Y-m-d H:i:s"));
        $deptModel = (new \app\admin\model\jichu\jg\Deptdetail())
            ->field("DD_Name")
            ->where(["Valid"=>1])
            ->order(["ParentNum"=>"ASC","DD_Num"=>"ASC"])
            ->select();
        $deptList = [];
        $commisiontestArr = [0=>[],1=>[]];
        foreach($deptModel as $v){
            $deptList[$v["DD_Name"]] = $v["DD_Name"];
        }
        $commisiontestList = db()->query("select CT_Standard,ifCommision,ifWm from commisionteststandard where valid=1 order by ct_default ASC,id asc");
        foreach($commisiontestList as $v){
            if($v["ifCommision"]) $commisiontestArr[0][$v["CT_Standard"]] = $v["CT_Standard"];
            if($v["ifWm"]) $commisiontestArr[1][$v["CT_Standard"]] = $v["CT_Standard"];
        }
        $this->view->assign("deptList",$deptList);
        $this->view->assign("commisiontestArr",$commisiontestArr);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $ini_where = [];
            if(isset($filter["is_check"])){
                if($filter["is_check"]=="1") $ini_where["MT_Auditor"] = ["=",""];
                if($filter["is_check"]=="2") $ini_where["MT_Auditor"] = ["<>",""];
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->field("*,(case when MT_Auditor='' then '未审核' else '已审核' end ) as is_check")
                ->where($where)
                ->where($ini_where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function requisitionDetail()
    {
        $ids = $this->request->post("ids");
        if(!$ids) return json(["code"=>0,"msg"=>"有误"]);
        $list = $this->detailModel->alias("mtd")
            ->join(["inventorymaterial"=>"im"],"mtd.IM_Num = im.IM_Num")
            ->field("mtd.MTD_TestNo,im.IM_Class,mtd.CT_Num,mtd.CTD_Pi,im.IM_Spec,mtd.L_Name,mtd.MGN_Length,mtd.LuPiHao,mtd.PiHao,MTD_C,MTD_Si,MTD_Mn,MTD_P,MTD_S,MTD_V,MTD_Nb,MTD_Ti,MTD_Cr,MTD_Czsl,MTD_Rel,MTD_Rm,MTD_Percent,MTD_Temperature,MTD_Cjf,MTD_Cjs,MTD_Cjt,round((MTD_Cjf+MTD_Cjs+MTD_Cjt)/3,3) as MTD_Cj_ave,MTD_Wq,MTD_Wg,MTD_ChDate,MTD_Conclusion,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("mtd.MT_Num",$ids)
            ->order("clsort,mtd.L_Name,bh,mtd.MGN_Length ASC")
            ->select();
        foreach($list as $k=>$v){
            $arr = $v->toArray();
            foreach($arr as $kk=>$vv){
                // if(is_numeric($vv)) $arr[$kk] = round($vv,3);
                if(is_numeric($vv)){
                    if(!checkZeroes($vv)) $arr[$kk] = 0;
                }
            }
            $list[$k] = $arr;
        }
        return json(["code"=>1,"data"=>$list]);
    }
    
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $params["MT_Standard"] = empty($params["MT_Standard"])?"":implode(",",$params["MT_Standard"]);
                $params["QualityNum"] = empty($params["QualityNum"])?"":implode(",",$params["QualityNum"]);
                $MT_Num = $params["MT_Num"];
                if($MT_Num){
                    $one = $this->model->where("MT_Num",$MT_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("Ymd");
                    $one = $this->model->field("MT_Num")->where("MT_Num","LIKE","GC".$month.'-%')->order("MT_Num DESC")->find();
                    if($one){
                        $num = substr($one["MT_Num"],11);
                        $MT_Num = 'GC'.$month.'-'.str_pad(++$num,2,0,STR_PAD_LEFT);
                    }else $MT_Num = 'GC'.$month.'-01';
                }
                $params["MT_Num"] = $MT_Num;
                $sectSaveList = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk]["MT_Num"] = $params["MT_Num"];
                        if($k=="MTD_ID" and $vv==0) continue;
                        else if($vv=="") continue;
                        else if(in_array($k,["MTD_Nb","MTD_Ti","MTD_Cr"])) $sectSaveList[$kk][$k] = $vv?$vv:0;
                        else if($k=="MTD_V"){
                            if(!$vv){
                                preg_match_all('/\∠\d+\*/', $sectSaveList[$kk]['IM_Spec'], $matches);
                                if(isset($matches[0][0])){
                                    $string = substr($matches[0][0],1,strlen($matches[0][0])-2);
                                    $sectSaveList[$kk][$k] = ($string<=200)?0:$sectSaveList[$kk][$k];
                                }
                            }else $sectSaveList[$kk][$k] = $vv;
                        }else $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $result = false;
                // $commision_model = new CommisionTestDetail();
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model::create($params);
                    $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$result["MT_ID"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];

        

        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $params["MT_Standard"] = empty($params["MT_Standard"])?"":implode(",",$params["MT_Standard"]);
                $params["QualityNum"] = empty($params["QualityNum"])?"":implode(",",$params["QualityNum"]);
                $MT_Num = $row["MT_Num"];
                $sectSaveList = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk]["MT_Num"] = $MT_Num;
                        if($k=="MTD_ID" and $vv=="") continue;
                        else if($vv=="") continue;
                        else if(in_array($k,["MTD_Nb","MTD_Ti","MTD_Cr"])) $sectSaveList[$kk][$k] = $vv?$vv:0;
                        else if($k=="MTD_V"){
                            if(!$vv){
                                preg_match_all('/\∠\d+\*/', $sectSaveList[$kk]['IM_Spec'], $matches);
                                if(isset($matches[0][0])){
                                    $string = substr($matches[0][0],1,strlen($matches[0][0])-2);
                                    $sectSaveList[$kk][$k] = ($string<=200)?0:$sectSaveList[$kk][$k];
                                }
                            }else $sectSaveList[$kk][$k] = $vv;
                        }else $sectSaveList[$kk][$k] = $vv;
                        
                    }
                }
                $result = false;
				// $commision_model = new CommisionTestDetail();
				Db::startTrans();
                try {
                    $result = $this->model->where("MT_ID",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $list = $this->detailModel->alias("mtd")
            ->join(["inventorymaterial"=>"im"],"mtd.IM_Num = im.IM_Num")
            ->field("MTD_ID,mtd.IM_Num,CT_Num,im.IM_Spec,MGN_Length,L_Name,CTD_Pi,MTD_TestNo,MTD_Mader,QualityNum,QualityStandard,LuPiHao,PiHao,MTD_C,MTD_Si,MTD_Mn,MTD_P,MTD_S,MTD_V,MTD_Nb,MTD_Ti,MTD_Cr,MTD_Czsl,MTD_Rel,MTD_Rm,MTD_Percent,MTD_Temperature,MTD_Cjf,MTD_Cjs,MTD_Cjt,round((MTD_Cjf+MTD_Cjs+MTD_Cjt)/3,2) as MTD_Cj_ave,MTD_Wq,MTD_Wg,MTD_ChDate,MTD_Conclusion,CTD_Weight,MN_Date,CT_Date,Memo,MTD_YaBian,0 as 'area',case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("MT_Num",$row["MT_Num"])
            ->order("mtd.L_Name,LuPiHao ASC")
            ->select();
        foreach($list as $k=>$v){
            $arr = $v->toArray();
            foreach($arr as $kk=>$vv){
                // if(is_numeric($vv) and $kk!="IM_Num") $arr[$kk] = round($vv,3);
                if(is_numeric($vv)){
                    if(!checkZeroes($vv)) $arr[$kk] = 0;
                }
            }
            $list[$k] = $arr;
        }
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $flag = true;
        if($row["MT_Auditor"]) $flag = false;
        $this->assignconfig("flag",$flag);
        $this->view->assign("list",$list);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }
    public function export($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $title = $row["MT_Num"];

        $list = $this->detailModel->alias("mtd")
            ->join(["inventorymaterial"=>"im"],"mtd.IM_Num = im.IM_Num")
            ->field("MTD_ID,mtd.IM_Num,CT_Num,im.IM_Spec,MGN_Length,L_Name,CTD_Pi,MTD_TestNo,MTD_Mader,QualityNum,QualityStandard,LuPiHao,PiHao,MTD_PHYNo,MTD_ChmNo,MTD_C,MTD_Si,MTD_Mn,MTD_P,MTD_S,MTD_V,MTD_Nb,MTD_Ti,MTD_Cr,MTD_Czsl,MTD_Rel,MTD_Rm,MTD_Percent,MTD_Temperature,MTD_Cjf,MTD_Cjs,MTD_Cjt,round((MTD_Cjf+MTD_Cjs+MTD_Cjt)/3,3) as MTD_Cj_ave,MTD_Wq,MTD_Wg,MTD_ChDate,MTD_Conclusion,CTD_Weight,MN_Date,CT_Date,Memo,MTD_YaBian,0 as 'area',case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("MT_Num",$row["MT_Num"])
            ->order("clsort,L_Name,bh,MGN_Length ASC")
            ->select();
        foreach($list as $k=>$v){
            $list[$k]["ID"] = $k+1;
            $list[$k]["MGN_Length"] = round($v["MGN_Length"],2);
        }
        $tableField = $this->getTableField();
        $header = [['ID', 'ID']];
        foreach($tableField as $k=>$v){
            if($v[5]==""){
                $header[] = [$v[0],$v[1]];
            }
        }
        return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
    
    }

    public function chooseSupplier($v_name="")
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            $params = $this->request->post();
            $where = ["ct.Auditor"=>["<>",""]];
            if(isset($params["V_Name"]) and $params["V_Name"]) $where["v.V_Name"] = ["=",$params["V_Name"]];
            if(isset($params["CT_Num"]) and $params["CT_Num"]) $where["ct.CT_Num"] = ["=",$params["CT_Num"]];
            if(isset($params["LuPiHao"]) and $params["LuPiHao"]) $where["ss.LuPiHao"] = ["=",$params["LuPiHao"]];
            if(isset($params["PiHao"]) and $params["PiHao"]) $where["ss.PiHao"] = ["=",$params["PiHao"]];
            if(isset($params["CTD_testnum"]) and $params["CTD_testnum"]) $where["ss.CTD_testnum"] = ["=",$params["CTD_testnum"]];
            if(isset($params["IM_Class"]) and $params["IM_Class"]) $where["im.IM_Class"] = ["=",$params["IM_Class"]];
            if(isset($params["CTD_Spec"]) and $params["CTD_Spec"]) $where["ss.CTD_Spec"] = ["=",$params["CTD_Spec"]];
            if(isset($params["L_Name"]) and $params["L_Name"]) $where["ss.L_Name"] = ["=",$params["L_Name"]];
            if(isset($params["CTD_Project"]) and $params["CTD_Project"]) $where["ss.CTD_Project"] = ["=",$params["CTD_Project"]];
            $subsql = (new CommisionTestDetail())->alias("ctd")
                ->join(["commisiontest"=>"ct"],"ctd.CT_Num = ct.CT_Num")
                ->join(["materialgetnotice"=>"mgn"],"ctd.MGN_ID = mgn.MGN_ID")
                ->field("ct.CT_Standard,ctd.MGN_ID,ctd.CT_Num,mgn.IM_Num,ctd.CTD_ID,ctd.LuPiHao,ctd.PiHao,(case when ctd.CTD_testnum='' then CONCAT('FJ-',ctd.LuPiHao) else ctd.CTD_testnum end) as CTD_testnum,(case when ctd.CTD_testnum='' then CONCAT('FJ-',ctd.LuPiHao) else ctd.CTD_testnum end) as 'MTD_TestNo',ctd.CTD_Spec,ctd.L_Name,ctd.CTD_Project,mgn.MGN_Maker as 'MTD_Mader',ctd.CTD_Pi,mgn.MGN_Length,ctd.CTD_Count,ctd.CTD_QualityNum as 'QualityNum',ctd.CTD_QualityJS as 'QualityStandard',ctd.CTD_Weight,ctd.CTD_UnqualityWeight,mgn.MN_Num,mgn.MGN_Destination,ctd.CTD_Memo,ct.CT_Date")
                ->where("ctd.CTD_Result","")
                // ->where("not exists ( select 'x' from materialtestdetail mtd where mtd.CT_Num=ctd.CT_Num and mgn.IM_Num=mtd.IM_Num and ctd.LuPiHao=mtd.LuPiHao and ctd.PiHao=mtd.PiHao and ctd.L_Name=mtd.PiHao and mtd.MGN_Length=mgn.MGN_Length)")
                ->order("ct.CT_Date DESC,ctd.L_Name ASC,ctd.CTD_Spec ASC,mgn.MGN_Length ASC,ctd.LuPiHao ASC")
                // ->limit(50)
                ->buildSql();
            $list = (new CommisionTest())->alias("ct")
                ->join([$subsql=>"ss"],"ss.CT_Num = ct.CT_Num")
                ->join(["vendor"=>"v"],"v.V_Num = ct.V_Num")
                ->join(["inventorymaterial"=>"im"],"ss.IM_Num = im.IM_Num")
                ->join(["materialnote"=>"mn"],"mn.MN_Num = ss.MN_Num")
                ->field("*,ct.CT_Memo as Memo,mn.MN_Date,case IM_Class when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
                when IM_Class='角钢' then 
                SUBSTR(REPLACE(CTD_Spec,'∠',''),1,locate('*',REPLACE(CTD_Spec,'∠',''))-1)*1000+
                SUBSTR(REPLACE(CTD_Spec,'∠',''),locate('*',REPLACE(CTD_Spec,'∠',''))+1,2)*1
                when (IM_Class='钢板' or IM_Class='钢管') then 
                REPLACE(CTD_Spec,'-','')
                else 0
                end) as UNSIGNED) bh")
                ->where($where)
                ->where("mn.WriteTime",">=",date("Y-m-d", strtotime(' -2 month')))
                ->order("ss.CT_Date DESC,ss.L_Name ASC,clsort ASC,bh ASC,ss.MGN_Length ASC,ss.LuPiHao ASC")
                ->select();
         
            return json(["code"=>1,"data"=>objectToArray($list)]);
        }
        $this->assignconfig("v_name",$v_name);
        $this->view->assign("v_name",$v_name);
        return $this->view->fetch();
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            $ct_one = $this->model->alias("m")->join(["materialtestdetail"=>"mtd"],"mtd.MT_Num = m.MT_Num")->where("mtd.MTD_ID",$num)->find();
            if($ct_one["MT_Auditor"]) return json(["code"=>0,'msg'=>"已审核，不能进行删除！"]);
            Db::startTrans();
            try {
                $this->detailModel->where("MTD_ID",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function del($ids=null)
    {
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $count = false;
            $list = $this->model->where("MT_ID",$ids)->find();
            if($list){
                if($list["MT_Auditor"]) $this->error("已审核，不能进行删除！");

                Db::startTrans();
                try {
                    // $commision_model->where("CTD_ID","IN",$find_arr)->update(["CTD_Result"=>""]);
                    $count += $this->model->where("MT_ID",$ids)->delete();
                    $count += $this->detailModel->where("MT_Num",$list["MT_Num"])->delete();
                    Db::commit();
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function allowApi()
    {
        $num = $this->request->post("num");
        $one = $this->model->get($num);
        if(!$one) $this->error("无数据，请刷新后重试");
        $result = $this->detailModel->where([
            "MT_Num" => ["=", $one["MT_Num"]],
            "allow" => ["=",0]
        ])->update(["allow"=>1]);
        if ($result) {
            $this->success("允许上传成功");
        } else {
            $this->error("允许上传失败");
        }
    }

    // public function allowApi()
    // {
    //     $num = $this->request->post("num");
    //     $one = $this->model->get($num);
    //     if(!$one) $this->error("无数据，请刷新后重试");
    //     $ini = [
    //         ["MTD_C" , "JGTG01" , "C"],
    //         ["MTD_Si" , "JGTG02" , "Si"],
    //         ["MTD_Mn" , "JGTG03" , "Mn"],
    //         ["MTD_P" , "JGTG04" , "P"],
    //         ["MTD_S" , "JGTG05" , "S"],
    //         ["MTD_V" , "JGTG06" , "V"],
    //         ["MTD_Nb" , "JGTG07" , "Nb"],
    //         ["MTD_Ti" , "JGTG08" , "Ti"],
    //         ["MTD_Cr" , "JGTG68" , "Cr"],
    //         ["MTD_Czsl" , "JGTG32" , "层状撕裂检验"],
    //         ["MTD_Rm" , "JGTG09" , "抗拉强度"],
    //         ["MTD_Rel" , "JGTG10" , "屈服强度"],
    //         ["MTD_Percent" , "JGTG11" , "断后伸长率"],
    //         ["MTD_Temperature" , "JGTG12" , "冲击检测"],
    //         ["MTD_Wq" , "JGTG13" , "弯曲试验"],
    //         ["MTD_Wg" , "JGTG14" , "尺寸外观"],
    //         ["MTD_Mader" , "JGTG65" , "生产厂家"],
    //         ["MTD_ChDate" , "JGTG66" , "原材料出厂日期"]
    //     ];
    //     $list = $this->detailModel->where("MT_Num",$one["MT_Num"])->column("MTD_ID");
    //     $data = [];
    //     foreach($list as $v){
    //         foreach($ini as $iv){
    //             $data[] = [
    //                 "MTD_ID" => $v,
    //                 "FIELD" => $iv[0],
    //                 "RULE_CODE" => $iv[1],
    //                 "NAME" => $iv[2],
    //                 "UPLOAD" => 0
    //             ];
    //         }
    //     }
    //     $result = false;
    //     Db::startTrans();
    //     try {
    //         $del = $this->bzModel->where("MTD_ID","IN",$list)->delete();
    //         $result = $this->bzModel->saveAll($data);
    //         Db::commit();
    //     } catch (PDOException $e) {
    //         Db::rollback();
    //         $this->error($e->getMessage());
    //     } catch (Exception $e) {
    //         Db::rollback();
    //         $this->error($e->getMessage());
    //     }
    //     if ($result !== false) {
    //         $this->success("允许上传成功");
    //     } else {
    //         $this->error("允许上传失败");
    //     }
        
    // }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $commision_model = new CommisionTestDetail();
        $one = $this->model->where("MT_ID",$num)->find();
        if($one["MT_Auditor"]) $this->error("已审核");
        $list = $this->model->alias("m")->join(["materialtestdetail"=>"mtd"],"m.MT_Num = mtd.MT_Num")->where("MT_ID",$num)->select();
        $result = false;
        Db::startTrans();
        try {
            $result = $this->model->where("MT_ID",$num)->update(['MT_ID' => $num,'MT_Auditor'=>$admin,"MT_AuditDate"=>date("Y-m-d H:i:s")]);
            foreach($list as $k=>$v){
                $con_result = $v["MTD_Conclusion"]=="合格"?"合格":"不合格";
                $material_result = [
                    "cd.CT_Num" => ["=",$v["CT_Num"]??""],
                    "cd.L_Name" => ["=",$v["L_Name"]??""],
                    "cd.LuPiHao" => ["=",$v["LuPiHao"]??""],
                    "cd.PiHao" => ["=",$v["PiHao"]??""],
                    "mgn.MGN_Length" => ["=",$v["MGN_Length"]??""]
                ];
                $com_detail_result = $commision_model->alias("cd")
                    ->join(["materialgetnotice"=>"mgn"],"cd.MGN_ID=mgn.MGN_ID")
                    ->where($material_result)->update(["cd.CTD_Result"=>$con_result]);
            }
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
        
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $commision_model = new CommisionTestDetail();
        $one = $this->model->where("MT_ID",$num)->find();
        if(!$one["MT_Auditor"]) $this->error("已弃审");
        $list = $this->model->alias("m")->join(["materialtestdetail"=>"mtd"],"m.MT_Num = mtd.MT_Num")->where("MT_ID",$num)->select();
        $result = false;
        Db::startTrans();
        try {
            $result = $this->model->where("MT_ID",$num)->update(['MT_ID' => $num,'MT_Auditor'=>"","MT_AuditDate"=>"0000-00-00 00:00:00"]);
            foreach($list as $k=>$v){
                $material_result = [
                    "cd.CT_Num" => ["=",$v["CT_Num"]??""],
                    "cd.L_Name" => ["=",$v["L_Name"]??""],
                    "cd.LuPiHao" => ["=",$v["LuPiHao"]??""],
                    "cd.PiHao" => ["=",$v["PiHao"]??""],
                    "mgn.MGN_Length" => ["=",$v["MGN_Length"]??""]
                ];
                $com_detail_result = $commision_model->alias("cd")
                    ->join(["materialgetnotice"=>"mgn"],"cd.MGN_ID=mgn.MGN_ID")
                    ->where($material_result)->update(["cd.CTD_Result"=>""]);
            }
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function copyField()
    {
        $return_data = ["code"=>0,"msg"=>"或信息缺失，同步失败","data"=>[]];
        [$luhao,$pihao] = array_values($this->request->post());
        $where = [];
        if($luhao) $where["LuPiHao"] = ["=",$luhao];
        else if($pihao) $where["PiHao"] = ["=",$pihao];
        else return json($return_data);
        $list = (new Recheck())->field("*,round((MTD_Cjf+MTD_Cjs+MTD_Cjt)/3,3) as MTD_Cj_ave")->where($where)->find();
        if(!$list) return json($return_data);
        else{
            $return_data["code"]=1;
            $return_data["msg"]="同步成功";
            $return_data["data"] = $list;
        }
        return json($return_data);
    }

    public function chooseWarranty($luhao=null)
    {
        $this->assignconfig("luhao",$luhao);
        $this->model = (new Recheck());
        return parent::index();
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["MTD_ID","MTD_ID","text","readonly","","hidden"],
            ["IM_Num","IM_Num","text","readonly","","hidden"],
            ["委托单编号","CT_Num","text","readonly","",""],
            ["规格","IM_Spec","text","readonly","",""],
            ["长度(mm)","MGN_Length","text","readonly",0,""],
            ["材质","L_Name","text","readonly","",""],
            ["进货批次","CTD_Pi","text","",0,""],
            ["试验编号","MTD_TestNo","text","",0,""],
            ["生产厂家","MTD_Mader","text","",0,""],
            // ["质量保证书号","QualityNum","text","",0,"hidden"],
            // ["质保标准","QualityStandard","text","",0,""],
            ["炉号","LuPiHao","text","",0,""],
            ["批号","PiHao","text","","",""],
            ["C(%)","MTD_C","text","","",""],
            ["Si(%)","MTD_Si","text","","",""],
            ["Mn(%)","MTD_Mn","text","","",""],
            ["P(%)","MTD_P","text","","",""],
            ["S(%)","MTD_S","text","","",""],
            ["V(%)","MTD_V","text","","",""],
            ["Nb(%)","MTD_Nb","text","","",""],
            ["Ti(%)","MTD_Ti","text","","",""],
            ["Cr(%)","MTD_Cr","text","","",""],
            ["层状撕裂","MTD_Czsl","select","","",""],
            ["抗拉强度(MPa)","MTD_Rm","text","","",""],
            ["屈服强度(MPa)","MTD_Rel","text","","",""],
            // ["屈服荷载(kN)","TestMin","text","","",""],
            ["伸长率(%)","MTD_Percent","text","","",""],
            ["冲击温度(℃)","MTD_Temperature","text","","",""],
            ["冲击1","MTD_Cjf","text","","",""],
            ["冲击2","MTD_Cjs","text","","",""],
            ["冲击3","MTD_Cjt","text","","",""],
            ["冲击均值","MTD_Cj_ave","text","readonly","",""],
            ["弯曲试验","MTD_Wq","select","","",""],
            ["尺寸外观","MTD_Wg","select","","",""],
            ["尺寸合格重量","CTD_Weight","text","","",""],
            ["原材料出厂日期","MTD_ChDate","datetimerange","","",""],
            ["结论","MTD_Conclusion","select","data-rule='required'","",""],
            ["到货时间","MN_Date","datetimerange","","",""],
            ["申请时间","CT_Date","datetimerange","","",""],
            ["备注","Memo","text","","",""],
            // ["物理样品编号","MTD_PHYNo","text","","",""],
            // ["化学样品编号","MTD_ChmNo","text","","",""],
            // ["Ni(%)","MTD_Ni","text","","",""],
            // ["Mo(%)","MTD_Mo","text","","",""],
            // ["化学化验结论","MTD_ChemistryResult","select","",1,""],
            // ["试件厚(mm)/直径(mm)","TestLength","text","",1,""],
            // ["试件宽(mm)","TestWidth","text","","",""],
            // ["横截面积","area","text","","",""],
            // ["抗拉荷载(kN)","TestMax","text","","",""],
            // ["原始标距(mm)","TestLo","text","","",""],
            // ["屈服强度(MPa)","MTD_Rel","text","","",""],
            // ["V型冲击(KV2/J)","MTD_Vtype","text","",1,""],
            // ["冷弯(180°)","MTD_Cold","text","","",""],
            // ["物理试验结论","MTD_PhysicalResult","select","","",""],
        ];
        return $list;
    }

}
