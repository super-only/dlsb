<?php

namespace app\admin\controller\quality\experiment;

use app\common\controller\Backend;

/**
 * 复检数据
 *
 * @icon fa fa-circle-o
 */
class Recheck extends Backend
{
    
    /**
     * Recheck模型对象
     * @var \app\admin\model\quality\experiment\Recheck
     */
    protected $model = null;
    protected $noNeedLogin = ["chooseGp","chooseQd","chooseCj","chooseMain"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\quality\experiment\Recheck;
        $this->admin = \think\Session::get('admin');

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params["LuPiHao"] and !$params["PiHao"]) $this->error("炉批号必填");
        }
        return parent::add();
    }

    public function edit($ids=null)
    {
        $this->assignconfig("ids",$ids);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params["LuPiHao"] and !$params["PiHao"]) $this->error("炉批号必填");
        }
        return parent::edit($ids);
    }

    public function chooseGp()
    {
        $this->model = new \app\admin\model\quality\experiment\DevGuangPu;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $total = $list->total();
            $data = $list?collection($list->items())->toArray():[];
            foreach($data as $k=>$v){
                foreach($v as $kk=>$vv){
                    $v[$kk] = ltrim($vv,"<");
                    if($kk=="INAME"){
                        $explode_result = explode("/",$vv);
                        $v["cz"] = $explode_result[0]??"";
                        $v["gg"] = $explode_result[1]??"";
                        $v["lph"] = $explode_result[2]??"";
                        $v["sccj"] = $explode_result[3]??"";
                    }
                }
                $data[$k] = $v;
            }
            $result = array("total" => $total, "rows" => $data);

            return json($result);
        }
        return $this->view->fetch();
    }

    public function chooseQd()
    {
        $this->model = new \app\admin\model\quality\experiment\DevLaLi;
        return parent::index();
    }

    public function chooseCj()
    {
         //设置过滤方法
         $this->request->filter(['strip_tags', 'trim']);
         if ($this->request->isAjax()) {
             //如果发送的来源是Selectpage，则转发到Selectpage
             if ($this->request->request('keyField')) {
                return $this->selectpage();
             }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $this->model = new \app\admin\model\quality\experiment\DevChongJi();
            $list = $this->model
                ->field("BATCH,Temperature,Date")
                ->where($where)
                ->group("BATCH")
                ->order($sort, $order)
                ->paginate($limit);
            $data = [];
            foreach($list as $v){
                $data[$v["BATCH"]] = $v->toArray();
            }
            $energyList = $this->model->field("NUMBER,Energy,BATCH")->where("BATCH","IN",array_keys($data))->order("id")->select();
            foreach($energyList as $k=>$v){
                if(isset($data[$v["BATCH"]]["Cjt"])) $ch_field="Cjt";
                else if(isset($data[$v["BATCH"]]["Cjs"])) $ch_field="Cjt";
                else if(isset($data[$v["BATCH"]]["Cjf"])) $ch_field="Cjs";
                else $ch_field = "Cjf";
                $data[$v["BATCH"]][$ch_field] = $v["Energy"];
            }
 
            $result = array("total" => $list->total(), "rows" => array_values($data));
 
            return json($result);
         }
         return $this->view->fetch();
    }

    public function chooseMain()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\material\CommisionTestDetail())
                ->field("LuPiHao")
                ->where($where)
                ->where("LuPiHao","<>","")
                ->group("LuPiHao")
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["username"];
        $result = $this->model->update(["id"=>$num,'auditor'=>$admin,"auditor_time"=>date("Y-m-d H:i:s")]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $result = $this->model->update(["id"=>$num,'auditor'=>"","auditor_time"=>"0000-00-00 00:00:00"]);
        if ($result) {
            return json(["code"=>1,"msg"=>"弃审成功"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

}
