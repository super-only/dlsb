<?php

namespace app\admin\controller\logistics\invoice;

use app\admin\model\chain\material\ProcurementStoreIn;
use app\admin\model\chain\material\StoreIn;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 发票记录
 *
 * @icon fa fa-circle-o
 */
class InvoiceMain extends Backend
{
    
    /**
     * InvoiceMain模型对象
     * @var \app\admin\model\logistics\invoice\InvoiceMain
     */
    protected $model = null,$admin='',$limberList = [],$wayList = [0=>"数量",1=>"重量"],$bzList = ["人民币"=>"人民币"],$businessType = [0=>"普通采购",1=>"其他"],$invioceList = [0=>"普通发票",1=>"专业发票",2=>"增值税发票"],$procurementType = [0=>"原材料",1=>"机物料",2=>"紧固件",3=>"线缆"];
    // protected $noNeedLogin = ["addMaterial","selectProcurement"];
    protected $relatedModel = null,$detailModel = null;
    // protected $noNeedLogin = ["selectStorein","addMaterial","selectInvoice"];
    protected $noNeedLogin = ["*"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\logistics\invoice\InvoiceMain;
        // $this->relatedModel = new \app\admin\model\logistics\invoice\InvoiceStoreInRelated;
        $this->detailModel = new \app\admin\model\logistics\invoice\InvoiceDetail;
        $this->admin = \think\Session::get('admin');
        [$deptList] = $this->deptType(1);
        $assign = [
            "tableField" => $this->getTableField(),
            "wayList" => $this->wayList,
            "bzList" => $this->bzList,
            "businessType" => $this->businessType,
            "invioceList" => $this->invioceList,
            "procurementType" => $this->procurementType,
            "deptList" => $deptList,
            "nickname" => $this->admin["username"],
            "vendorList" => $this->vendorNumList(),
            // "statusList" => [0=>"未支付",1=>"部分支付",2=>"已支付"]
        ];
        $this->view->assign($assign);
        foreach($assign as $k=>$v){
            $this->assignconfig($k,$v);
        }
    }

    public function import()
    {
        parent::import();
    }

    public function selectInvoice($v_num='',$type=0)
    {
        $iniWhere = [];
        if($v_num) $iniWhere["supplier"] = ["=",$v_num];
        //type后续使用
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("i")
                ->join(["invoice_detail"=>"ind"],"i.invoice_num=ind.invoice_num")
                ->field("i.*,sum(ind.sum_amount) as amount")
                ->where($where)
                ->where($iniWhere)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->assignconfig("v_num",$v_num);
        $this->assignconfig("type",$type);
        return $this->view->fetch();
    }
    
    public function selectStorein($v_num='')
    {
        if(!$v_num) $this->error("请选择供应商后重新点击入库单信息");
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            //合同管理——到货——入库——发票互相关
            $list = (new StoreIn())->alias("si")
                ->join(["storeindetail"=>"sid"],"si.SI_OtherID = sid.SI_OtherID")
                // ->join(["inventorymaterial"=>"im"],"im.IM_Num = sid.IM_Num","LEFT")
                // ->join(["materialgetnotice"=>"mgn"],"mgn.MGN_ID = sid.MGN_ID")
                // ->join(["vendor"=>"v"],"v.V_Num = sid.V_Num","LEFT")
                // ->join(["projectcatalog"=>"pcl"],"pcl.PC_Num = si.PC_Num","LEFT")
                // ->field("si.offer,si.SI_OtherID,sid.SID_ID,si.SI_InDate,si.SI_WareHouse,sid.IM_Num,im.IM_Class,sid.L_Name,sid.L_Name as 'sid.L_Name',im.IM_Spec,sid.SID_Weight,round(sid.SID_Length/1000,3) as SID_Length,round(sid.SID_Width/1000,3) as SID_Width,sid.SID_Count,sid.SID_FactWeight,sid.SID_RestCount,sid.SID_RestWeight,sid.SID_TestResult,sid.LuPiHao,sid.PiHao,v.V_Name,si.RSC_ID,pcl.PC_ProjectName,(case when si.SI_AudioPepo='' then '未审核' else '已审核' end ) as is_check,im.IM_Measurement")
                ->where($where)
                ->where("NOT EXISTS (SELECT 'x' FROM invoice_detail id WHERE id.sid_id=sid.SID_ID)")
                ->WHERE("si.SI_AudioPepo","<>","")
                // ->where("v.V_Num",$v_num)
                ->order($sort,$order)
                ->group("si.SI_OtherID")
                ->paginate($limit);
            // $data = [];
            // foreach($list as $k=>$v){
            //     $data[$v["SID_ID"]] = $v->toArray();
            // }
            // $priceList = (new ProcurementStoreIn())->where("SID_ID","IN",array_keys($data))->column("SID_ID,way,price");
            // foreach($priceList as $k=>$v){
            //     $data[$k]["price"] = $v["price"];
            //     $data[$k]["way"] = $v["way"];
            // }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->assignconfig("rsclist",["到货入库"=>"到货入库","盘盈入库"=>"盘盈入库"]);
        $this->assignconfig("v_num",$v_num);
        return $this->view->fetch();
    }

    public function getDetail()
    {
        $tableField = $this->request->post("tableField");
        $tableList = $tableField?json_decode($tableField,true):'';
        if(empty($tableList)) return json(["code"=>0,"msg"=>"有误，请稍后重试","data"=>[]]);
        $list = (new StoreIn())->alias("si")
            ->join(["storeindetail"=>"sid"],"si.SI_OtherID = sid.SI_OtherID")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = sid.IM_Num","LEFT")
            ->join(["materialgetnotice"=>"mgn"],"mgn.MGN_ID = sid.MGN_ID")
            ->join(["vendor"=>"v"],"v.V_Num = sid.V_Num","LEFT")
            ->join(["projectcatalog"=>"pcl"],"pcl.PC_Num = si.PC_Num","LEFT")
            ->field("si.offer,si.SI_OtherID,sid.SID_ID,si.SI_InDate,si.SI_WareHouse,sid.IM_Num,im.IM_Class,sid.L_Name,sid.L_Name as 'sid.L_Name',im.IM_Spec,sid.SID_Weight,round(sid.SID_Length/1000,3) as SID_Length,round(sid.SID_Width/1000,3) as SID_Width,sid.SID_Count,sid.SID_FactWeight,sid.SID_RestCount,sid.SID_RestWeight,sid.SID_TestResult,sid.LuPiHao,sid.PiHao,v.V_Name,si.RSC_ID,pcl.PC_ProjectName,(case when si.SI_AudioPepo='' then '未审核' else '已审核' end ) as is_check,im.IM_Measurement")
            // ->where("NOT EXISTS (SELECT 'x' FROM invoice_detail id WHERE id.sid_id=sid.SID_ID)")
            ->where("si.SI_OtherID","IN",$tableList)
            ->select();
        $data = [];
        foreach($list as $k=>$v){
            $data[$v["SID_ID"]] = $v->toArray();
        }
        $priceList = (new ProcurementStoreIn())->where("SID_ID","IN",array_keys($data))->column("SID_ID,way,price");
        foreach($priceList as $k=>$v){
            $data[$k]["price"] = $v["price"];
            $data[$k]["way"] = $v["way"];
        }
        return json(["code"=>1,"msg"=>"成功","data"=>array_values($data)]);
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            // if(!$table) $this->error("发票详情不能为空");
            // $exList = [0=>"Y",1=>"W",2=>"J",3=>"X"];
            $ex = "Y";
            //pm_num HT202304-0001
            $year = $ex."FP".date("Ym")."-";
            $lastOne = $this->model->where("invoice_num","LIKE",$year."%")->order("invoice_num desc")->value("invoice_num");
            if($lastOne) $lastOne = str_pad((substr($lastOne,-4)+1),4,0,STR_PAD_LEFT);
            else $lastOne = "0001";
            $params["invoice_num"] = $year.$lastOne;
            if(!$params["supplier"]) $this->error("供应商不能为空");
            $params["fax"] = $params["fax"]?$params["fax"]:0;
            $params["rate"] = $params["rate"]?$params["rate"]:1;
            $params["writer"] = $this->admin["nickname"];
            $params["writer_time"] = date("Y-m-d H:i:s");
            $saveTable = [];
            $sidList = $table["sid_id"];
            $sidExistList = $this->detailModel->where("sid_id","in",$sidList)->column("sid_id as id,sid_id");
            foreach($table["id"] as $k=>$v){
                if(isset($sidExistList[$sidList[$k]])) continue;
                $way = $table["way"][$k];
                $count = $way?($table["weight"][$k]?$table["weight"][$k]:0):($table["count"][$k]?$table["count"][$k]:0);
                $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                $price = $count?round($amount/$count,5):0;
                $fax_amount = $amount*0.01*$params["fax"];
                $sum_amount = $amount+$fax_amount;
                $fax_price = $count?round($sum_amount/$count,5):0;
                $saveTable[$k] = [
                    "id" => $v,
                    "invoice_num" => $params["invoice_num"],
                    "sid_id" => $table["sid_id"][$k],
                    "im_num" => $table["im_num"][$k],
                    "l_name" => $table["l_name"][$k],
                    "length" => $table["length"][$k]?$table["length"][$k]:0,
                    "width" => $table["width"][$k]?$table["width"][$k]:0,
                    "unit" => $table["unit"][$k],
                    "count" => $table["count"][$k]?$table["count"][$k]:0,
                    "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                    "way" => $way,
                    "price" => $price,
                    "fax_price" => $fax_price,
                    "amount" => $amount,
                    "fax_amount" => $fax_amount,
                    "sum_amount" => $sum_amount,
                    "memo" => $table["memo"][$k],
                ];
                if(!$v) unset($saveTable[$k]["id"]);
            }
            $params["amount"] = array_sum(array_column($saveTable,'sum_amount'));
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params);
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->assignconfig("init_data",[]);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $flag = 1;
        if($row["auditor"]=="") $flag = 0;
        $row["amount"] = round($row["amount"],2);
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $detailList = $this->detailModel->alias("d")
            ->join(["inventorymaterial"=>"im"],"d.im_num=im.IM_Num")
            ->field("d.*,im.IM_Class,im.IM_Spec")
            ->where("d.invoice_num",$ids)
            ->select();
        $detailList = $detailList?collection($detailList)->toArray():[];
        $detailIdList = [];
        foreach($detailList as $k=>$v){
            $detailIdList[$v["id"]] = $v["id"];

        }
        [$tbodyField,$tableSidList] = $this->_getTbodyField($detailList,$this->getTableField(),1);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            if(!$table) $this->error("发票详情不能为空");
            $saveTable = [];
            $sidList = $table["sid_id"];
            $sidExistList = $this->detailModel->where("sid_id","in",$sidList)->where("invoice_num","<>",$ids)->column("sid_id as id,sid_id");
            foreach($table["id"] as $k=>$v){
                if(isset($sidExistList[$sidList[$k]])) continue;
                $way = $table["way"][$k];
                $count = $way?($table["weight"][$k]?$table["weight"][$k]:0):($table["count"][$k]?$table["count"][$k]:0);
                $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                $price = $count?round($amount/$count,5):0;
                $fax_amount = $amount*0.01*$params["fax"];
                $sum_amount = $amount+$fax_amount;
                $fax_price = $count?round($sum_amount/$count,5):0;
                $saveTable[$k] = [
                    "id" => $v,
                    "invoice_num" => $ids,
                    "sid_id" => $table["sid_id"][$k],
                    "im_num" => $table["im_num"][$k],
                    "l_name" => $table["l_name"][$k],
                    "length" => $table["length"][$k]?$table["length"][$k]:0,
                    "width" => $table["width"][$k]?$table["width"][$k]:0,
                    "unit" => $table["unit"][$k],
                    "count" => $table["count"][$k]?$table["count"][$k]:0,
                    "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                    "way" => $way,
                    "price" => $price,
                    "fax_price" => $fax_price,
                    "amount" => $amount,
                    "fax_amount" => $fax_amount,
                    "sum_amount" => $sum_amount,
                    "memo" => $table["memo"][$k],
                ];
                if(!$v) unset($saveTable[$k]["id"]);
                else if(isset($detailIdList[$k])){
                    unset($detailIdList[$k]);
                }
            }

            $params["amount"] = array_sum(array_column($saveTable,'sum_amount'));
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params,["invoice_num"=>$ids]);
                if(!empty($detailIdList)) $this->detailModel->where("id","in",$detailIdList)->delete();
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->assignconfig("tableSidList",$tableSidList);
        $this->view->assign("tbodyField", $tbodyField);
        $this->view->assign("row", $row);
        // $this->assignconfig("init_data",$init_data);
        $this->assignconfig("ids",$ids);
        $this->view->assign("flag",$flag);
        $this->assignconfig("flag",$flag);
        return $this->view->fetch();
    }

    public function addMaterial()
    {
        $params = $this->request->post("data");
        $params = json_decode($params,true);
        $tableField = $this->getTableField();
        [$field,$sidList] = $this->_getTbodyField($params,$tableField);
        return json(["code"=>1,"data"=>["field"=>$field,"sidList"=>$sidList]]);
    }

    protected function _getTbodyField($params,$tableField,$type=0)
    {
        $field = "";
        $sidList = [];
        foreach($params as $k=>$v){
            if($type) $sidList[$v["sid_id"]] = $v["sid_id"];
            $field .= '<tr>'
                    . '<td>'
                    . '<a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a>'
                    . '</td>';
            foreach($tableField as $vv){
                if($type) $value = $v[$vv[1]]??'';
                else $value = $v[$vv[3]]??'';
                if(in_array($vv[1],["price","fax_price","amount","fax_amount","sum_amount"])) $value = round($value,2);
                if(in_array($vv[1],["length","width"])){
                    if(!$type) $value = $value*1000;
                    $field .= '<td '.$vv[6].'><input type="'.$vv[2].'" class="small_input" '.$vv[4].' name="table['.$vv[1].'][]" value="'.($vv[1]=="id"?0:$value).'"></td>';
                }else if($vv[1]=="l_name"){
                    $field .= "<td ".$vv[6].">";
                    $field .= '<input type="text" class="small_input" list="typelist" name="table['.$vv[1].'][]" value="'.$value.'" placeholder="请选择"><datalist id="typelist">';
                    foreach($this->limberList as $vvv){
                        $field .= '<option value="'.$vvv.'">'.$vvv.'</option>';
                    }
                    $field .= "</datalist></td>";
                }else if($vv[1]=="way"){
                    $field .= "<td ".$vv[6].">";
                    $field .= build_select('table['.$vv[1].'][]',$this->wayList,$value?$value:1,['class'=>'form-control selectpicker',"id"=>"c-".$vv[1],"data-rule"=>"required"]);
                    $field .= "</td>";
                }else if($vv[1]=="unit"){
                    $field .= "<td ".$vv[6].">";
                    $field .= build_select('table['.$vv[1].'][]',$this->measurementList(),($value??$v["IM_Measurement"]),['class'=>'form-control selectpicker',"id"=>"c-".$vv[1],"data-rule"=>"required"]);
                    $field .= "</td>";
                }else{
                    $field .= '<td '.$vv[6].'><input type="'.$vv[2].'" class="small_input" '.$vv[4].' name="table['.$vv[1].'][]" value="'.($vv[1]=="id"?0:($value??$vv[5])).'"></td>';
                }
            }
            $field .= "</tr>";
        }
        return [$field,array_values($sidList)];
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->select();
            $newIdList = [];
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                    $newIdList[$v[$pk]] = $v[$pk];
                }
                $this->detailModel->where($pk,"IN",$newIdList)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
    
    /**
     * 审核
     */
    public function auditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->update([
                    "auditor" => $this->admin["username"],
                    "auditor_time" => date("Y-m-d H:i:s")
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function giveUp($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","<>","")->where("status","=",0)->update([
                    "auditor" => '',
                    "auditor_time" => "0000-00-00 00:00:00"
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["id","id","text","","readonly",0,"hidden"],
            // ["入库单号","SI_OtherID","text","SI_OtherID","readonly","",""],
            ["入库ID","sid_id","text","SID_ID","readonly","",""],
            ["材料编码","im_num","text","IM_Num","readonly","",""],
            ["材料名称","IM_Class","text","IM_Class","readonly","",""],
            ["规格","IM_Spec","text","IM_Spec","readonly","",""],
            ["材质","l_name","text","L_Name","readonly","",""],
            ["长度(mm)","length","number","SID_Length","readonly","",""],
            ["宽度(mm)","width","number","SID_Width","readonly","",""],
            ["单位","unit","select","IM_Measurement","readonly","",""],
            ["数量","count","number","SID_Count","readonly","",""],
            // ["比重","IM_PerWeight","text","","readonly",0,"hidden"],
            ["重量","weight","number","SID_FactWeight","readonly","",""],
            ["单价计算","way","select","way","readonly","",""],
            ["单价","price","number","price","","",""],
            ["含税单价","fax_price","number","","readonly","",""],
            ["金额","amount","number","","readonly","",""],
            ["税额","fax_amount","number","","readonly","",""],
            ["价税合计","sum_amount","number","","readonly","",""],
            ["备注","memo","text","","","",""]
        ];
        return $list;
    }
}
