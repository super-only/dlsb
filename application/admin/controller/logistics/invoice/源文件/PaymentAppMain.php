<?php

namespace app\admin\controller\logistics\invoice;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 付款申请单
 *
 * @icon fa fa-circle-o
 */
class PaymentAppMain extends Backend
{
    
    /**
     * PaymentAppMain模型对象
     * @var \app\admin\model\logistics\invoice\PaymentAppMain
     */
    protected $model = null,$detailModel = null, $payModel = null, $viewModel = null, $admin='',$limberList = [],$bzList = ["人民币"=>"人民币"],$businessType = [0=>"普通采购",1=>"其他"];
    protected $noNeedLogin = ["selectStorein","addMaterial"];

    public function _initialize()
    {
        parent::_initialize();
        $this->viewModel = new \app\admin\model\logistics\invoice\PaymentAppMain;
        $this->detailModel = new \app\admin\model\logistics\invoice\PaymentAppDetail;
        $this->payModel = new \app\admin\model\logistics\invoice\PayList;
        $this->model = new \app\admin\model\logistics\invoice\PayStatusView;
        $this->admin = \think\Session::get('admin');
        $statusList = [
            "未支付" => "未支付",
            "部分支付" => "部分支付",
            "已支付" => "已支付"
        ];
        [$deptList] = $this->deptType(1);
        $assign = [
            "tableField" => $this->getTableField(),
            "bzList" => $this->bzList,
            "businessType" => $this->businessType,
            "deptList" => $deptList,
            "nickname" => $this->admin["username"],
            "vendorList" => $this->vendorNumList(),
            "balanceMode" => $this->_getBalanceModel(),
            "statusList" => $statusList
        ];
        $this->view->assign($assign);
        foreach($assign as $k=>$v){
            $this->assignconfig($k,$v);
        }
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            if(!$table) $this->error("发票详情不能为空");
            // $exList = [0=>"Y",1=>"W",2=>"J",3=>"X"];
            // $ex = $exList[$params["type"]];
            //pm_num HT202304-0001
            $year = "FK".date("Ym")."-";
            $lastOne = $this->viewModel->where("pay_app_num","LIKE",$year."%")->order("pay_app_num desc")->value("pay_app_num");
            if($lastOne) $lastOne = str_pad((substr($lastOne,-4)+1),4,0,STR_PAD_LEFT);
            else $lastOne = "0001";
            $params["pay_app_num"] = $year.$lastOne;
            if(!$params["supplier"]) $this->error("供应商不能为空");
            $params["rate"] = $params["rate"]?$params["rate"]:1;
            $params["writer"] = $this->admin["username"];
            $params["writer_time"] = date("Y-m-d H:i:s");
            $saveTable = [];
            foreach($table["id"] as $k=>$v){
                if(!$table["amount"][$k]) $this->error("金额不能为0");
                $saveTable[$k] = [
                    "id" => $v,
                    "pay_app_num" => $params["pay_app_num"],
                    "department" => $params["department"],
                    "salesman" => $params["salesman"],
                    "amount" => $table["amount"][$k],
                    "pm_num" => $table["pm_num"][$k],
                    "invoice_num" => $table["invoice_num"][$k],
                    "memo" => $table["memo"][$k]
                ];
                if(!$v) unset($saveTable[$k]["id"]);
            }
            $result = false;
            Db::startTrans();
            try {
                $result = $this->viewModel->allowField(true)->save($params);
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->assignconfig("init_data",[]);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->viewModel->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $flag = 1;
        if($row["auditor"]=="") $flag = 0;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $detailList = $this->detailModel
            ->where("pay_app_num",$ids)
            ->select();
        $detailList = $detailList?collection($detailList)->toArray():[];
        $detailIdList = [];
        foreach($detailList as $k=>$v){
            $detailIdList[$v["id"]] = $v["id"];
        }
        [$tbodyField,$tableSidList] = $this->_getTbodyField($detailList,$this->getTableField());
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            if(!$table) $this->error("付款申请详情不能为空");
            $saveTable = [];
            foreach($table["id"] as $k=>$v){
                if(!$table["amount"][$k]) $this->error("金额不能为0");
                $saveTable[$k] = [
                    "id" => $v,
                    "pay_app_num" => $ids,
                    "department" => $params["department"],
                    "salesman" => $params["salesman"],
                    "amount" => $table["amount"][$k],
                    "pm_num" => $table["pm_num"][$k],
                    "invoice_num" => $table["invoice_num"][$k],
                    "memo" => $table["memo"][$k]
                ];
                if(!$v) unset($saveTable[$k]["id"]);
                else if(in_array($v,$detailIdList)!=-1){
                    $wz = in_array($v,$detailIdList);
                    unset($detailIdList[$wz]);
                }
            }

            $result = false;
            Db::startTrans();
            try {
                $result = $this->viewModel->allowField(true)->save($params,["pay_app_num"=>$ids]);
                if(!empty($detailIdList)) $this->detailModel->where("id","in",$detailIdList)->delete();
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->assignconfig("tableSidList",$tableSidList);
        $this->view->assign("tbodyField", $tbodyField);
        $this->view->assign("row", $row);
        // $this->assignconfig("init_data",$init_data);
        $this->assignconfig("ids",$ids);
        $this->view->assign("flag",$flag);
        $this->assignconfig("flag",$flag);
        return $this->view->fetch();
    }

    public function addMaterial()
    {
        $params = $this->request->post("data");
        $params = json_decode($params,true);
        $tableField = $this->getTableField();
        [$field,$sidList] = $this->_getTbodyField($params,$tableField);
        return json(["code"=>1,"data"=>["field"=>$field,"sidList"=>$sidList]]);
    }

    protected function _getTbodyField($params,$tableField)
    {
        $field = "";
        $sidList = [];
        foreach($params as $k=>$v){
            $field .= '<tr>'
                    . '<td>'
                    . '<a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a>'
                    . '</td>';
            foreach($tableField as $vv){
                $value = $v[$vv[1]]??'';
                $field .= '<td '.$vv[6].'><input type="'.$vv[2].'" class="small_input" '.$vv[4].' name="table['.$vv[1].'][]" value="'.($vv[1]=="id"?0:($value??$vv[5])).'"></td>';
            }
            $field .= "</tr>";
        }
        return [$field,array_values($sidList)];
    }
    
    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->viewModel->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->viewModel->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->viewModel->where($pk, 'in', $ids)->where("auditor","=","")->select();
            $newIdList = [];
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                    $newIdList[$v[$pk]] = $v[$pk];
                }
                $this->detailModel->where($pk,"IN",$newIdList)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
    
    /**
     * 审核
     */
    public function auditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->viewModel->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->viewModel->where($this->dataLimitField, 'in', $adminIds);
            }
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->viewModel->where($pk, 'in', $ids)->where("auditor","=","")->where("url","<>","")->update([
                    "auditor" => $this->admin["username"],
                    "auditor_time" => date("Y-m-d H:i:s")
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行,查看是否未更新附件");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function giveUp($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->viewModel->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->viewModel->where($this->dataLimitField, 'in', $adminIds);
            }

            $count = 0;
            Db::startTrans();
            try {
                $count = $this->viewModel->where($pk, 'in', $ids)->where("auditor","<>","")->update([
                    "auditor" => '',
                    "auditor_time" => "0000-00-00 00:00:00"
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["id","id","text","","readonly",0,"hidden"],
            // ["入库单号","SI_OtherID","text","SI_OtherID","readonly","",""],
            ["合同号","pm_num","text","","readonly","",""],
            ["发票号","invoice_num","text","","readonly","",""],
            ["金额","amount","number","","data-rule='required'","",""],
            ["备注","memo","text","","","",""]
        ];
        return $list;
    }
}
