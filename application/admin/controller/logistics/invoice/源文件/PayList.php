<?php

namespace app\admin\controller\logistics\invoice;

use app\admin\model\logistics\invoice\PaymentAppMain;
use app\common\controller\Backend;

/**
 * 付款管理
 *
 * @icon fa fa-circle-o
 */
class PayList extends Backend
{
    
    /**
     * PayList模型对象
     * @var \app\admin\model\logistics\invoice\PayList
     */
    protected $model = null, $admin='',$bzList = ["人民币"=>"人民币"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\logistics\invoice\PayList;
        $this->admin = \think\Session::get('admin');
        $assign = [
            "bzList" => $this->bzList,
            "nickname" => $this->admin["username"]
        ];
        $this->view->assign($assign);
        foreach($assign as $k=>$v){
            $this->assignconfig($k,$v);
        }
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    public function selectPayList($pay_app_num='')
    {
        if(!$pay_app_num) $this->error("没有关联申请单，请稍后再试");
        $iniWhere = [];
        $iniWhere["pay_app_num"] = ["=",$pay_app_num];
        //type后续使用
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->where($iniWhere)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->assignconfig("pay_app_num",$pay_app_num);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add($pay_app_num='')
    {
        $row = (new PaymentAppMain())->alias("pam")
            ->join(["payment_app_detail"=>"pad"],"pam.pay_app_num=pad.pay_app_num")
            ->field("pam.*,sum(pad.amount) as amount")
            ->where("pam.pay_app_num",$pay_app_num)
            ->find();
        if(!$row) $this->error("没有该申请付款单，请稍后再试");
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                if(round($params["amount"],3)==0) $this->error("金额不能为零");
                $amount = $this->model->where("pay_app_num",$pay_app_num)->value("sum(amount) as amount");
                if($row["amount"]<($amount+$params["amount"])) $this->error("实际付款金额不能超过申请金额");

                $params["pay_app_num"] = $pay_app_num;
                $year = "SFK".date("Ym")."-";
                $lastOne = $this->model->where("pay_num","LIKE",$year."%")->order("pay_num desc")->value("pay_num");
                if($lastOne) $lastOne = str_pad((substr($lastOne,-4)+1),4,0,STR_PAD_LEFT);
                else $lastOne = "0001";
                $params["pay_num"] = $year.$lastOne;
                $params["writer"] = $this->admin["username"];
                $params["writer_time"] = date("Y-m-d H:i:s");
                $result = $this->model->allowField(true)->save($params);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row",$row);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $base = (new PaymentAppMain())->alias("pam")
                    ->join(["payment_app_detail"=>"pad"],"pam.pay_app_num=pad.pay_app_num")
                    ->field("pam.*,sum(pad.amount) as amount")
                    ->where("pam.pay_app_num",$row["pay_app_num"])
                    ->find();
                if(round($params["amount"],3)==0) $this->error("金额不能为零");
                $amount = $this->model->where("pay_app_num",$row["pay_app_num"])->where("id","<>",$ids)->value("sum(amount) as amount");
                if($row["amount"]<($amount+$params["amount"])) $this->error("实际付款金额不能超过申请金额");
                $params = $this->preExcludeFields($params);
                $result = $row->allowField(true)->save($params);
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
}
