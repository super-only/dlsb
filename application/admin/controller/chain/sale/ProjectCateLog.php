<?php

namespace app\admin\controller\chain\sale;

use app\admin\model\chain\sale\Compact;
use app\admin\model\chain\sale\Task;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class ProjectCateLog extends Backend
{
    
    /**
     * ProjectCateLog模型对象
     * @var \app\admin\model\chain\sale\ProjectCateLog
     */
    protected $model = null;
    protected $admin;
    protected $noNeedLogin = ["selectCcNum","selectAdName","selectType","selectCustomer","selectSaleMan"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\sale\ProjectCateLog;
        $this->admin = \think\Session::get('admin');
        $this->view->assign('admin',$this->admin["nickname"]);
        $this->view->assign('productType',$this->_productTypeList());
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {   
        $saleTypeList = $this->saleType()[0];
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["ST_Num"] = $saleTypeList[$v["ST_Num"]]??"";
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        $this->view->assign("saleTypeList",$saleTypeList);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                if($params["PC_type"]!=3 and !$params["PC_gross"]) $this->error("铁塔必填工程总重量");
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $defaultKey = $this->keyList();
                    foreach($defaultKey as $v){
                        if(isset($params[$v]) && $params[$v]==1) $this->model->where($v,1)->update([$v => 0]);
                    }
                    
                    $year = date("Y",time());
                    $findOne = $this->model->where("PC_Num",'LIKE',"GC".$year.'%')->order("PC_Num DESC")->find();

                    if(isset($findOne["PC_Num"])){
                        $num = substr($findOne["PC_Num"],6);
                        $num = "GC".$year.str_pad((++$num),4,0,STR_PAD_LEFT);
                    }else $num = "GC".$year.'0001';
                    $params["PC_Num"] = $num;
                    $params["PC_ShortName"] = $params["PC_ShortName"]?$params["PC_ShortName"]:$params["PC_ProjectName"];

                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$num);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign('saleList',$this->saleType(1)[0]);
        $this->view->assign('areaList',$this->areaList(1));
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if($params["PC_type"]!=3 and !$params["PC_gross"]) $this->error("铁塔必填工程总重量");
                $project_list = $this->model->alias("pc")
                    ->join(["compact"=>"c"],"pc.PC_Num = c.PC_Num","left")
                    ->join(["task"=>"t"],"c.C_Num = t.C_Num","left")
                    ->field("c.C_Num,t.T_Num,c.C_Project,c.C_SortProject,t.t_project,t.t_shortproject")
                    ->where("pc.PC_Num",$ids)
                    ->select();
                $c_num_list = $t_num_list = [];
                foreach($project_list as $k=>$v){
                    $c_num_list[$v["C_Num"]] = $v["C_Num"];
                    $t_num_list[$v["T_Num"]] = $v["T_Num"];
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $defaultKey = $this->keyList();
                    foreach($defaultKey as $v){
                        if(isset($params[$v]) && $params[$v]==1) $this->model->where($v,1)->update([$v => 0]);
                    }
                    $params["PC_ShortName"] = $params["PC_ShortName"]?$params["PC_ShortName"]:$params["PC_ProjectName"];
                    $params["PC_EditDate"] = date("Y-m-d H:i:s",time());
                    $result = $row->allowField(true)->save($params);

                    if(!empty($c_num_list)) (new Compact())->where("C_Num","IN",$c_num_list)->update(["C_Project"=>$params["PC_ProjectName"],"C_SortProject"=>$params["PC_ShortName"]]);
                    if(!empty($t_num_list)) (new Task())->where("T_Num","IN",$t_num_list)->update(["t_project"=>$params["PC_ProjectName"],"t_shortproject"=>$params["PC_ShortName"]]);
                    

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        $this->view->assign('saleList',$this->saleType(1)[0]);
        $this->view->assign('areaList',$this->areaList(1));
        return $this->view->fetch();
    }
    
    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, $ids)->select();
            $compactModel = new \app\admin\model\chain\sale\Compact();
            $count = 0;
            Db::startTrans();
            try {
                if(empty($compactModel->where("PC_Num",$list[0]["PC_Num"])->find())) $count += $list[0]->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 点击打开新页面 客户
     */
    public function selectCustomer()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $this->treeSearch = $this->request->get("tree", '');
            $list = (new \app\admin\model\jichu\wl\Customer())
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["CC_Num"] = $this->customerList()[0][$v["CC_Num"]];
            }
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        $deptList = $this->deptType(1)[0];
        $classList = $this->customerList(1)[0];
        $addressList = $this->areaList(1);
        $this->view->assign('classList', $classList);
        $this->view->assign('addressList',$addressList);
        $this->view->assign('deptList',$deptList);
        return $this->view->fetch();
    }

    /**
     * 点击打开新页面 销售人员的选择
     */
    public function selectSaleMan($type=0)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\jichu\jg\Employee())
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
                
            $rows = $list->items();
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        $this->assignconfig("type",$type);
        return $this->view->fetch();
    }

    /**
     * 客户树型的json
     */
    public function selectType()
    {
        $list = $this->customerList()[1];
        return json(array_values($list));
    }

    /**
     * 客户下拉框的json
     */
    public function selectCcNum()
    {
        $list = $this->customerList()[0];
        $data = [];
        foreach($list as $k=>$v){
            $data[] = ["id"=>$k,"title"=>$v];
        }
        return json(["list"=>$data,"total"=>count($data)]);
    }

    /**
     * 地区下拉框的json
     */
    public function selectAdName()
    {
        $list = $this->areaList();
        $data = [];
        foreach($list as $k=>$v){
            $data[] = ["id"=>$k,"title"=>$v];
        }
        return json(["list"=>$data,"total"=>count($data)]);
    }

    /**
     * 审核
     */
    public function auditor()
    {
        // echo 1;
        $PC_Num = $this->request->post("PC_Num");

        $project_one = $this->model->field("PC_ProjectName,PC_ShortName,ST_Num,C_Name,PC_Contactor,PC_ContactTel,PC_SalesMan")->where("PC_Num",$PC_Num)->find();
        $compact_one = (new Compact())->where("PC_Num",$PC_Num)->find();
        $sale_type_list = $this->saleType(1)[0];
        if($compact_one){
            $compact_one->update(["C_Project"=>$project_one["PC_ProjectName"],"C_SortProject"=>$project_one["PC_ShortName"],"C_SaleType"=>$sale_type_list[$project_one["ST_Num"]],"Customer_Name"=>$project_one["C_Name"],"C_Contractor"=>$project_one["PC_Contactor"],"C_Phone"=>$project_one["PC_ContactTel"],"C_SaleMan"=>$project_one["PC_SalesMan"]],["PC_Num"=>$PC_Num]);
            $task_list = (new Task())->where("C_Num",$compact_one["C_Num"])->select();
            foreach($task_list as $k=>$v){
                $v->update(["t_project"=>$project_one["PC_ProjectName"],"t_shortproject"=>$project_one["PC_ShortName"]],["C_Num"=>$compact_one["C_Num"]]);
            }
        }

        $admin = $this->admin["nickname"];
        $result = $this->model->update(['PC_Num' => $PC_Num,'Auditor'=>$admin]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    /**
     * 弃审
     */
    public function giveUp()
    {
        // echo 1;
        $PC_Num = $this->request->post("PC_Num");
        $result = $this->model->update(['PC_Num' => $PC_Num,'Auditor'=>""]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }
}
