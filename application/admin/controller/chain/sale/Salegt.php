<?php

namespace app\admin\controller\chain\sale;

use app\admin\controller\api\PurchaseApi;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 销售杆塔信息管理
 *
 * @icon fa fa-circle-o
 */
class Salegt extends Backend
{
    
    /**
     * Salegt模型对象
     * @var \app\admin\model\chain\sale\Salegt
     */
    protected $noNeedLogin = ["eipUpload","contrast","selectCompact"];
    protected $model = null;
    protected $listModel = null;
    //名称 字段 类型 规则 默认值
    protected $tableArr = [
        ["SCD_ID","SCD_ID","text","",0,"60"],
        ["杆塔号","SCD_TPNum","text","","","80"],
        ["标段","BiaoDuan","text","","","60"],
        ["线路","SCD_lineName","text","","","60"],
        ["*电压等级","TD_Pressure","text","","","60"],
        ["*塔型/型号","TD_TypeName","text","data-rule='required'","","90"],
        ["*呼高","TH_Height","text","","","60"],
        ["*产品名称","SCD_ProductName","text","","铁塔","60"],
        ["*单位","SCD_Unit","text","data-rule='required'","基","60"],
        ["*数量","SCD_Count","text","data-rule='required'","1","60"],
        ["公共塔身","SCD_Part","text","","","120"],
        ["专用段","SCD_SpPart","text","","","120"],
        ["A段位","SCD_APart","text","","","60"],
        ["A腿长(米)","SCD_ALength","text","","","60"],
        ["B段位","SCD_BPart","text","","","60"],
        ["B腿长(米)","SCD_BLength","text","","","60"],
        ["C段位","SCD_CPart","text","","","60"],
        ["C腿长(米)","SCD_CLength","text","","","60"],
        ["D段位","SCD_DPart","text","","","60"],
        ["D腿长(米)","SCD_DLength","text","","","60"],
        ["单重(kg)","SCD_Weight","text","","","80"],
        ["单价","SCD_UnitPrice","text","","","60"],
        ["总重量(kg)","Sum_Weight","text","","","80"],
        ["转角度数","SCD_Corner","text","","","60"],
        ["基础形式","SCD_BStyle","text","","","60"],
        ["挂点","SCD_MountPoint","text","","","60"],
        ["交货日期","SCD_SendDate","text","","","100"],
        ["基础半根开","SCD_BGK","text","","","60"],
        ["地脚螺栓直径","SCD_StoneBoltZJ","text","","","60"],
        ["备注","SCD_Memo","text","","","100"],
        ["计算总价方式","price_way","text","data-rule='required'","1","60"],
    ];
    protected $admin;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\sale\Salegt;
        $this->listModel = new \app\admin\model\chain\sale\Sectconfigdetail;
        $this->view->assign("nowtime",date("Y-m-d H:i:s",time()));
        $this->admin = \think\Session::get('admin');
        $this->view->assign('produce_type',$this->_productTypeList());
        $this->assignconfig('produce_type',$this->_productTypeList());

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model->alias('s')
                    ->join([ 'compact'=>'c' ],'s.C_Num=c.C_Num','LEFT')
                    ->field('s.*,c.Customer_Name,c.Customer_Name as "c.Customer_Name",c.C_Project,c.C_Project as "c.C_Project",c.C_SortProject,c.C_SortProject as "c.C_SortProject",c.PC_Num,c.PC_Num as "c.PC_Num",c.produce_type')
                    ->where($where)
                    ->order($sort, $order)
                    ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function contrast()
    {
        $ids = $this->request->post("ids");
        if(!$ids) return json(["code"=>0,"data"=>[]]);
        $leftList = $this->listModel->where("SGT_ID",$ids)->order("SCD_ID ASC")->select();
        $list = [];
        foreach($leftList as $v){
            if($v["TH_ID"]==0){
                $list[$v["SCD_ID"]] = $v->toArray();
                $list[$v["SCD_ID"]]["Sum_Weight"] = round($v['SCD_Count']*$v['SCD_Weight'],3);
            }
        }
        return json(["code"=>1,"data"=>array_values($list)]);
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $params_table = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                if(!$params["C_Num"]) $this->error("请选择工程后再保存");
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $params["Writer"] = $this->admin["nickname"];
                    $params["WriteDate"] = date("Y-m-d H:i:s");
                    $result = $this->model->allowField(true)->insertGetId($params);
                    // pri($one);die;
                    // $one = $this->model->order("WriteDate DESC")->find();
                    $tableRow = [];
                    if(!empty($params_table)){
                        unset($params_table["Sum_Weight"],$params_table["SCD_ID"]);
                        foreach($params_table as $k=>$v){
                            foreach($v as $kk=>$vv){
                                $tableRow[$kk]['SGT_ID'] = $result;
                                $tableRow[$kk]['Writer'] = $params["Writer"];
                                $tableRow[$kk]['WriteDate'] = $params["WriteDate"];
                                $tableRow[$kk][$k] = $vv;
                            }
                        }
                        $this->listModel->allowField(true)->saveAll($tableRow);
                    }
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$result);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        
        $this->view->assign("tableField",$this->tableArr);
        $this->assignconfig("fieldArr",$this->tableArr);
        return $this->view->fetch();
    }

    /**
     * 查看
     */
    public function selectCompact()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\sale\Compact())
                ->where("Auditor",'<>',"")
                ->where($where)
                ->where("NOT EXISTS ( SELECT C_Num FROM salegt s WHERE s.C_Num=compact.C_Num )")
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function detail()
    {
        $C_Num = $this->request->post("C_Num");
        $list = (new \app\admin\model\chain\sale\OrderDetail())
            ->field("OD_LineName as SCD_lineName,OD_LineNum as SCD_TPNum,OD_Pressure as TD_Pressure,OD_TypeName as TD_TypeName,OD_Height as TH_Height,OD_Count as SCD_Count,OD_Unit as SCD_Unit,OD_Weight as SCD_Weight,OD_SumWeight as Sum_Weight,C_Price as SCD_UnitPrice,OD_Memo as SCD_Memo,OD_ProductName as SCD_ProductName,price_way")
            ->where("C_Num",$C_Num)
            ->select();
        $arr = [];
        foreach($list as $v){
            $arr[] = $v->toArray();
        }
        return json(["code"=>1,"data"=>$arr]);
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        // $row = $this->model->get($ids);
        $row = $this->model->alias('s')
            ->join([ 'compact'=>'c' ],'s.C_Num=c.C_Num','LEFT')
            ->field('s.*,c.Customer_Name,c.C_Project,c.C_SortProject,c.PC_Num,c.produce_type')
            ->where("SGT_ID",$ids)
            ->find();
        $tableField = [];
        $list = $this->listModel->where("SGT_ID",$ids)->select();
        $number = $weight = 0;
        foreach($list as $k=>$v){
            $number += $v["SCD_Count"];
            $tableField[$v["SCD_ID"]] = $v->toArray();
            $tableField[$v["SCD_ID"]]["Sum_Weight"] = round($v["SCD_Weight"]*$v["SCD_Count"],3);
            $weight += $tableField[$v["SCD_ID"]]["Sum_Weight"];
        }
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $params_table = $this->request->post("table_row/a");
            $update = $add = $tableRow = [];
            if(!empty($params_table)){
                unset($params_table["Sum_Weight"]);
                foreach($params_table as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $tableRow[$kk][$k] = $vv;
                        $tableRow[$kk]['Writer'] = $this->admin["nickname"];
                        $tableRow[$kk]['WriteDate'] = date("Y-m-d H:i:s");
                    }
                }
                
                $keys = array_keys($tableField);
                foreach($tableRow as $k=>$v){
                    if($v["SCD_ID"]){
                        if (in_array($v["SCD_ID"], $keys)){
                            $update[] = $v;
                            unset($tableField[$v["SCD_ID"]]);
                        }
                    }else{
                        unset($v["SCD_ID"]);
                        $add[] = array_merge($v,["SGT_ID"=>$ids]);
                    }
                }
            }
            foreach($tableField as $v){
                if($v["TH_ID"]!=0) $this->error("已生成生产销售联系单，不能进行删除！");
            }
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->get($ids)->allowField(true)->save($params);
                    
                    if(!empty($add)){
                        $this->listModel->allowField(true)->saveAll($add);
                    }
                    if(!empty($update)){
                        $this->listModel->allowField(true)->saveAll($update);
                    }
                    
                    if(!empty($tableField)){
                        foreach($tableField as $k=>$v){
                            $this->listModel->where("SCD_ID",$k)->delete();
                        }
                        
                    }
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("number", $number);
        $this->view->assign("weight", $weight);
        $this->view->assign("row", $row);
        $this->view->assign("list",$tableField);
        $fieldArr = $this->tableArr;
        $fieldArr[7][4] = $this->_productTypeList()[$row["produce_type"]];
        $this->view->assign("tableField",$this->tableArr);
        $this->assignconfig("fieldArr",$fieldArr);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, $ids)->find();
            $taskModel = new \app\admin\model\chain\sale\Task();
            if($taskModel->where("C_Num",$list["C_Num"])->select()) $this->error("已经生成销售联系单，不能删除");
            $count = 0;
            Db::startTrans();
            try {
                $count += $list->delete();
                $this->listModel->where('SGT_ID',$list["SGT_ID"])->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function auditor()
    {
        $SGT_ID = $this->request->post("SGT_ID");
        $admin = $this->admin["nickname"];
        $result = $this->model->update(['SGT_ID' => $SGT_ID,'Auditor'=>$admin,"AuditDate"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function giveUp()
    {
        $SGT_ID = $this->request->post("SGT_ID");
        $result = $this->model->update(['SGT_ID' => $SGT_ID,'Auditor'=>""]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }


    // public function eipUpload()
    // {
    //     $SGT_ID = $this->request->post("SGT_ID");
    //     $json_result = ["code"=>0,"msg"=>"失败"];
    //     // $SGT_ID = "2434";
    //     $params = $this->model->alias("si")
    //         ->join(["sectconfigdetail"=>"scd"],"si.SGT_ID = scd.SGT_ID")
    //         ->join(["compact"=>"c"],"si.C_Num=c.C_Num")
    //         ->field("*")
    //         ->where("si.SGT_ID",$SGT_ID)
    //         ->where("scd.SCD_ProductName","铁塔")
    //         ->order("scd.SCD_ID ASC")
    //         ->select();
    //     if(!$params) return json($json_result);
    //     $purchaseApiControl = new PurchaseApi();
    //     $saveData = [];
    //     $param_url = "supplier-so";
    //     foreach($params as $k=>$v){
    //         $operatetype = "ADD";
    //         $api_data = [
    //             "purchaserHqCode" => "SGCC",
    //             "soNo" => $v["C_Num"].'-'.$v["SCD_ID"],
    //             "supplierCode" => "1000014615",
    //             "buyerName" => $v["Customer_Name"],
    //             "categoryCode" => "60",
    //             "subclassCode" => "60001",
    //             "soItemNo" => $v["C_Num"].'-'.$v["SCD_ID"],
    //             "poItemId" => $v["poItemId"],
    //             "productCode" => $v["TD_TypeName"],
    //             "productName" => $v["TD_TypeName"],
    //             "productUnit" => $v["SCD_Unit"],
    //             "productAmount" => $v["SCD_Count"],
    //             "dataSource" => 0,
    //             "dataSourceCreateTime"=>date("Y-m-d H:i:s")
    //         ];
    //         if($v["eip_upload"]){
    //             $operatetype = 'UPDATE';
    //             unset($api_data["dataSourceCreateTime"]);
    //         }
    //         $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$api_data);
    //         if($api_result["code"]==1) $this->detailModel->where("SCD_ID",$v["SCD_ID"])->update(["eip_upload"=>1]);
    //         else $saveData[$v["TD_TypeName"].$v["SCD_TPNum"]] = $api_result["msg"];
    //     }
    //     $msg = "上传成功";
    //     if(!empty($saveData)) {
    //         $msg = "";
    //         foreach($saveData as $k=>$v){
    //             $msg .= $k.$v.";";
    //         }
    //         $json_result["msg"] = $msg;
    //     }else $json_result = ["code"=>1,"msg"=>$msg];
    //     return json($json_result);
    // }

    public function eipUpload()
    {
        $SGT_ID = $this->request->post("SGT_ID");
        $json_result = ["code"=>0,"msg"=>"失败"];
        // $SGT_ID = "2434";
        $mainParams = $this->model->alias("si")->join(["compact"=>"c"],"si.C_Num=c.C_Num")->where("si.SGT_ID",$SGT_ID)->where("c.produce_type","1")->find();
        if(!$mainParams) return json($json_result);
        $poItemId = $mainParams["poItemId"];
        $api_data = [
            "purchaserHqCode" => "SGCC",
            "soNo" => $mainParams["C_Num"],
            "supplierCode" => "1000014615",
            "buyerName" => $mainParams["Customer_Name"],
            "categoryCode" => "60",
            "dataSource" => "0",
            "dataSourceCreateTime" => date("Y-m-d H:i:s"),
            "itemList" => []
        ];
        $params = $this->listModel
            ->where("SGT_ID",$SGT_ID)
            ->order("SCD_ID ASC")
            ->select();
        if(!$params) return json($json_result);
        $purchaseApiControl = new PurchaseApi();
        $saveData = [];
        $param_url = "supplier-so";
        $operatetype = "ADD";
        foreach($params as $k=>$v){
            $api_data["itemList"][] = [
                "categoryCode" => "60",
                "subclassCode" => "60001",
                "soItemNo" => $v["SCD_ID"],
                "poItemId" => $poItemId,
                "productCode" => $v["TD_TypeName"],
                "productName" => $v["TD_TypeName"],
                "productUnit" => $v["SCD_Unit"],
                "productAmount" => "".$v["SCD_Count"],
            ];
        }
        if($mainParams["eip_upload"]) $operatetype = 'UPDATE';
        $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$api_data);
        if($api_result["code"]==1) $this->model->where("SGT_ID",$SGT_ID)->update(["eip_upload"=>1]);
        else $saveData[$mainParams["C_Num"]] = $api_result["msg"];
        $msg = "上传成功";
        if(!empty($saveData)) {
            $msg = "";
            foreach($saveData as $k=>$v){
                $msg .= $k.$v.";";
            }
            $json_result["msg"] = $msg;
        }else $json_result = ["code"=>1,"msg"=>$msg];
        return json($json_result);
    }
}
