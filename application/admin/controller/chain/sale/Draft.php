<?php

namespace app\admin\controller\chain\sale;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
error_reporting(0);

/**
 * 打草案
 *
 * @icon fa fa-circle-o
 */
class Draft extends Backend
{
    
    /**
     * Draft模型对象
     * @var \app\admin\model\chain\sale\Draft
     */
    protected $model = null,$detailModel=null,$admin=null;
    protected $noNeedLogin = ["theadField","selectDraft"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\sale\Draft;
        $this->detailModel = new \app\admin\model\chain\sale\DraftDetail;
        $this->admin = \think\Session::get('admin');
        $type_list = [1=>"铁塔（复杂模板）", 2=>"铁塔（简单模板）", 3=>"铁附件"];
        $this->view->assign("type_list",$type_list);
        $this->assignconfig("type_list",$type_list);
        list($dept_list,$dept_tree) = $this->deptType();
        foreach($dept_list as $k=>$v){
            $dept_list[$v] = $v;
            unset($dept_list[$k]);
        }
        $this->view->assign("dept_list",$dept_list);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    public function importType()
    {
        if ($this->request->isPost()) {
            $file = $this->request->post("filename");
            $type = $this->request->post("type");
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $data = [];
            $file = ROOT_PATH . DS . 'public' . DS . $file;
            try {
                $data = Excel::import($file, $startRow = 0, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($data)){
                    $this->error('空数据文件');
                }
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            foreach($data as $k=>$v){
                if($v[0]=="") unset($data[$k]);
            }
            $year = date("Ym");
            $one = $this->model->where("ID","LIKE",$year."%")->order("ID DESC")->find();
            $num = $one?substr($one["ID"],6):0;
            $code = $num?$year.str_pad((++$num),4,0,STR_PAD_LEFT):$year.'0001';
            $result = false;
            Db::startTrans();
            try {
                $count=count($data);
                $writer_news = ["writer"=>$this->admin["nickname"],"writer_time"=>date("Y-m-d H:i:s")];
                foreach($data[$count] as $v){
                    $v = str_replace("：",":",$v);
                    if($v!='' and strpos($v,"制表")!==false) $writer_news["writer"] = explode(":",$v)[1];
                    if($v!='' and strpos($v,"日期")!==false) $writer_news["writer_time"] = explode(":",$v)[1];
                }
                $arr = date_parse_from_format('Y.n.j',$writer_news["writer_time"]);
                // pri(date("Y-m-d",strtotime($writer_news["writer_time"])),$arr,1);
                $time = mktime(0,0,0,$arr['month'],$arr['day'],$arr['year']);
                $draft_one = [
                    "type" => $type,
                    "ID" => $code,
                    "project_name" => trim($data[0][0]),
                    "writer" => $writer_news["writer"],
                    "writer_time" => date("Y-m-d",$time),
                    "remark" => $data[$count-1][0]?$data[$count-1][0]:""
                ];
                $this->model->save($draft_one);
                if($type==1) $data = array_slice($data,3,$count-6);
                else $data = array_slice($data,2,$count-5);
                $save_list = $field_list = [];
                $fieldContent = $this->fieldHideContent($type);
                foreach($fieldContent as $k=>$v){
                    if(empty($v['son'])){
                        unset($v["son"]);
                        $field_list[] = $v;
                    }else{
                        foreach($v["son"] as $kk=>$vv){
                            unset($vv["son"]);
                            $field_list[] = $vv;
                        }
                    }
                }
                foreach($data as $k=>$v){
                    $save_list[$k]["draft_id"] = $code;
                    foreach($field_list as $kk=>$vv){
                        if(isset($v[$kk])) $save_list[$k][$vv[1]] = $v[$kk];
                        else $save_list[$k][$vv[1]] = "";
                        if($save_list[$k][$vv[1]]=="") $save_list[$k][$vv[1]] =  $vv[2]=="s"?"":0;
                    }
                    if($type==1) $save_list[$k]["segment_form"] = trim($save_list[$k]["segment_body"].','.$save_list[$k]["segment_leg"],",");
                    $save_list[$k]["weight"] = round((int)$save_list[$k]["count"]*(float)$save_list[$k]["sum_weight"],2);
                }
                $result = $this->detailModel->allowField(true)->saveAll($save_list);
                Db::commit();
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success("成功导入".count($result)."条记录！");
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        $list = [1=>"铁塔复杂模板",2=>"铁塔简单模板"];
        $this->view->assign("list",$list);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        $fieldHideContent = $this->fieldHideContent(1);
        $secondField = [];
        foreach($fieldHideContent as $k=>$v){
            foreach($v["son"] as $kk=>$vv){
                $secondField[$kk] = $vv;
                $secondField[$kk]["parent"] = $k;
            }
        }
        $total_list = ["tower_type"=>"合计"];
        foreach($fieldHideContent as $vv){
            if(count($vv["son"])==0){
                if($vv[3]=="sum") $total_list[$vv[1]] = 0;
            }else{
                foreach($vv["son"] as $vvv){
                    if($vvv[3]=="sum") $total_list[$vvv[1]] = 0;
                }
            }
        }
        $list[] = $total_list;
        $this->view->assign("list",$list);
        // $tableField = $this->fieldContent(1);
        // $theadField = $this->fieldContentHead(1);
        // $htmlField = array_merge([["ID","ID","f","hidden"]],$tableField);
        // $this->view->assign("theadField",$theadField);
        // $this->assignConfig("htmlField",$htmlField);
        $this->view->assign("fieldHideContent",$fieldHideContent);
        $this->view->assign("secondField",$secondField);
        $this->assignConfig("fieldHideContent",$fieldHideContent);
        $this->assignConfig("secondField",$secondField);
        $this->assignConfig("type",1);
        $this->view->assign("row",$row);
        $this->assignConfig("ids",0);
        return $this->view->fetch();
    }

        

    /**
     * 添加
     */
    public function edit($ids=null)
    {
        
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $list = $this->detailModel->field("*,count*sum_weight as weight")->where(["draft_id"=>["=",$row["ID"]]])->select();
        $fieldHideContent = $this->fieldHideContent($row["type"]);
        $total_list = ["tower_type"=>"合计"];
        foreach($fieldHideContent as $vv){
            if(count($vv["son"])==0){
                if($vv[3]=="sum") $total_list[$vv[1]] = 0;
            }else{
                foreach($vv["son"] as $vvv){
                    if($vvv[3]=="sum") $total_list[$vvv[1]] = 0;
                }
            }
        }
        foreach($list as $k=>$v){
            $list[$k] = $v->toArray();
            foreach($fieldHideContent as $vv){
                if(count($vv["son"])==0){
                    if($vv[3]=="sum"){
                        isset($total_list[$vv[1]])?"":$total_list[$vv[1]] = 0;
                        $total_list[$vv[1]] += $v[$vv[1]];
                    }
                    if($vv[2]!="s") isset($v[$vv[1]])?$list[$k][$vv[1]] = round($v[$vv[1]],2):0;
                }else{
                    foreach($vv["son"] as $vvv){
                        if($vvv[3]=="sum"){
                            isset($total_list[$vvv[1]])?"":$total_list[$vvv[1]] = 0;
                            $total_list[$vvv[1]] += $v[$vvv[1]];
                        }
                        if($vvv[2]!="s") isset($v[$vvv[1]])?$list[$k][$vvv[1]] = round($v[$vvv[1]],2):0;
                    }
                }
            }
        }
        // if($row["auditor"]){
            $list[] = $total_list;
        // }
        $secondField = [];
        $first = $start = $end = 5;$second = 1;
        foreach($fieldHideContent as $k=>$v){
            foreach($v["son"] as $kk=>$vv){
                $secondField[$kk] = $vv;
                $secondField[$kk]["parent"] = $k;
            }
            $fieldHideContent[$k]["first"] = $first;
            $fieldHideContent[$k]["start"] = $start;
            $count = count($v["son"]);
            if($count!=0){
                foreach($v["son"] as $kk=>$vv){
                    $fieldHideContent[$k]["son"][$kk]["second"] = $second;
                    $second++;
                    $end++;
                }
                $start = $end;
                $fieldHideContent[$k]["end"] = $end-1;
            }else{
                $fieldHideContent[$k]["end"] = $end;
                $start++;
                $end++;
            }
            $first++;
        }
        $this->view->assign("fieldHideContent",$fieldHideContent);
        $this->view->assign("secondField",$secondField);
        $this->assignConfig("fieldHideContent",$fieldHideContent);
        $this->assignConfig("secondField",$secondField);
        $this->assignConfig("type",$row["type"]);
        $this->view->assign("row",$row);
        $this->view->assign("list",$list);
        $this->assignConfig("ids",$ids);
        return $this->view->fetch();
    }
    public function save($ids=0)
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("data");
            $params = $params?json_decode($params,true):[];
            $row = [];
            $table_params = [];
            foreach($params as $k=>$v){
                if(substr($v["name"],0,4) == "row["){
                    $key = substr($v["name"],4,strlen($v["name"])-5);
                    $row[$key] = $v["value"];
                }else{
                    $table_params[$v["name"]][] = $v["value"];
                }
            }
            if(empty($row)) return json(["code"=>0,"msg"=>"请填写主信息"]);
            if($row["project_name"]=="" or $row["type"]=="") return json(["code"=>0,"msg"=>"工程目录和类型必填"]);
            if(empty($table_params)) return json(["code"=>0,"msg"=>"请填写子信息"]);
            $tableField = $this->fieldHideContent($row["type"]);
            $row["ID"] = $ids;
            if($ids==0){
                $year = date("Ym");
                $one = $this->model->where("ID","LIKE",$year."%")->order("ID DESC")->find();
                $num = $one?substr($one["ID"],6):0;
                $code = $num?$year.str_pad((++$num),4,0,STR_PAD_LEFT):$year.'0001';
                $row["ID"] = $code;
            }
            $result = false;
            Db::startTrans();
            try {
                if($ids==0) $a = $this->model->save($row);
                else $a = $this->model->update($row,["ID"=>$ids]);
                $sectSaveList = [];
                foreach($table_params["tower_type"] as $k=>$v){
                    if(!$v) continue;
                    $sectSaveList[$k]["ID"] = $table_params["ID"][$k];
                    foreach($tableField as $vv){
                        if(count($vv["son"])==0){
                            if(!$table_params[$vv[1]][$k]) $value = $vv[2]=="s"?"":0;
                            else $value = $table_params[$vv[1]][$k];
                            $sectSaveList[$k][$vv[1]] = $value;
                        }else{
                            foreach($vv["son"] as $vvv){
                                if(!$table_params[$vvv[1]][$k]) $value = $vvv[2]=="s"?"":0;
                                else $value = $table_params[$vvv[1]][$k];
                                $sectSaveList[$k][$vvv[1]] = $value;
                            }
                        }
                    }
                    $sectSaveList[$k]["draft_id"] = $row["ID"];
                    $sectSaveList[$k]["weight"] = round($sectSaveList[$k]["count"]*$sectSaveList[$k]["sum_weight"],2);
                    if(!$sectSaveList[$k]["ID"]) unset($sectSaveList[$k]["ID"]);
                }
                $result = $this->detailModel->allowField(true)->saveAll(array_values($sectSaveList));
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
        
            if ($result !== false) {
                return json(["code"=>1,"msg"=>"保存成功","data"=>$row["ID"]]);
            } else {
                return json(["code"=>1,"msg"=>"没有数据更新","data"=>$row["ID"]]);
            }
        }
        return json(["code"=>0,"msg"=>"保存失败"]);
    }

    public function selectDraft($type=1)
    {
        $ini_where = ["auditor"=>["<>",""]];
        if($type==1) $ini_where["type"] = ["IN",[1,2]];
        else $ini_where["type"] = ["=",$type];
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("d")
                ->join(["draftdetail"=>"dd"],"d.ID=dd.draft_id")
                ->field("dd.ID,draft_id,tower_type,pole_number,unit,height,count,weight,sum_weight,project_name,(case when d.type=3 then dd.remark else '' end) as OD_Memo")
                ->where("NOT EXISTS ( SELECT draft_detail_id FROM orderdetail od WHERE dd.ID=od.draft_detail_id )")
                ->where($ini_where)
                ->where($where)
                ->order("d.ID desc,dd.ID asc")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->assignconfig("type",$type);
        return $this->view->fetch();
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            $one = $this->model->alias("d")
                ->join(["draftdetail"=>"dd"],"d.ID=dd.draft_id")
                ->where("dd.ID",$num)
                ->find();
            if(!$one) return json(["code"=>0,'msg'=>"删除失败,请刷新后重试"]);
            elseif($one["auditor"]) return json(["code"=>0,'msg'=>"删除失败,已审核"]);
            Db::startTrans();
            try {
                $this->detailModel->where("ID",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function del($ids = null)
    {
        if(!$ids) $this->error("删除失败");
        $this_one = $this->model->where("ID",$ids)->find();
        if($this_one["auditor"]) $this->error("已审核，不能进行删除！");
        $count = 0;
        Db::startTrans();
        try {
            $count += $this->model->where("ID",$ids)->delete();
            $count += $this->detailModel->where("draft_id",$ids)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if($count) $this->success("删除成功");
        else $this->error("删除失败");
    }

    public function auditor()
    {
        $ID = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $result = $this->model->update(['auditor'=>$admin,"auditor_time"=>date("Y-m-d H:i:s")],['ID' => $ID]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"审核成功"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function giveUp()
    {
        $ID = $this->request->post("num");
        $result = $this->model->update(['auditor'=>"","auditor_time"=>"0000:00:00 00:00:00"],['ID' => $ID]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"弃审成功"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

    public function theadField()
    {
        $type = $this->request->post("type");
        if($type){
            $fieldHideContent = $this->fieldHideContent($type);
            $secondField = [];
            $html = '<thead id="thshow"><tr>'.'<th width="40" rowspan="2">序号</th>'.'<th width="40" rowspan="2">删</th>'.'<th width="40" rowspan="2"><input type="checkbox" id="allchoose"></th>';
            $html .= '<th hidden width="60" rowspan="2">ID</th>';
            foreach($fieldHideContent as $k=>$v){
                $html .= '<th width="'.($v[5]?(60*$v[5]):60).'" '.($v[4]?'rowspan="'.$v[4].'"':'').($v[5]?' colspan="'.$v[5].'" style="text-align: center;"':'').'>'.($v[0]).'</th>';
                foreach($v["son"] as $kk=>$vv){
                    $secondField[$kk] = $vv;
                    $secondField[$kk]["parent"] = $k;
                }
            }
            $html .=  '</tr><tr>';
            foreach($secondField as $k=>$v){
                $html .= '<th style="top:20px" width="'.($v[5]?(60*$v[5]):60).'" '.($v[4]?'rowspan="'.$v[4].'"':'').($v[5]?' colspan="'.$v[5].'" style="text-align: center;"':'').'>'.($v[0]).'</th>';
            }
            $html .=  '</tr></thead><tbody id="tbshow"><tr><td>1</td><td></td><td></td><td hidden><input class="small_input" type="text" value="0"></td>';
            foreach($fieldHideContent as $vv){
                if($vv["1"]=="tower_type") $html .= '<td><input class="small_input" type="text" readonly value="合计"></td>';
                else if(count($vv["son"])!=0){
                    foreach($vv["son"] as $vvv){
                        $html .= '<td><input class="small_input" type="text" readonly value=""></td>';
                    }
                }else{
                    $html .= '<td><input class="small_input" type="text" readonly value=""></td>';
                }
            }
            $html .=  '</tr></tbody>';
            return json(["code"=>1,"data"=>["fieldHideContent"=>$fieldHideContent,"html"=>$html]]);
        }
        return json(["code"=>0,"msg"=>"有误，请稍后重试"]);
    }

    public function export($ids=null,$field=''){
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $list = $this->detailModel->field("*,count*sum_weight as weight")->where(["draft_id"=>["=",$row["ID"]]])->select();
        $tableField = $this->fieldHideContent($row["type"]);
        $table_list = $list_count = [];
        $field_list = $field?explode(",",$field):[];
        foreach($field_list as $k=>$v){
            unset($tableField[$v]);
        }
        $count=0;
        foreach($tableField as $k=>$v){
            if(empty($v["son"])){
                unset($v["son"]);
                $table_list[$k] = $v;
                $count++;
                $table_list[$k][] = $count;
            }else{
                foreach($v["son"] as $kk=>$vv){
                    unset($vv["son"]);
                    $table_list[$kk] = $vv;
                    $count++;
                    $table_list[$kk][] = $count;
                }
            }
        }
        // $tableExcelField = $this->fieldExcelContent($row["type"]);
        // 初始化
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER, //水平居中
                'vertical' => Alignment::VERTICAL_CENTER, //垂直居中
            ],
        ];
        $the_end_index = Coordinate::stringFromColumnIndex(count($table_list));
        //写入头部
        $sheet->mergeCells('A1:'.$the_end_index.'1');
        $sheet->setCellValue("A1",$row["project_name"]);
        if($row["type"]==1){
            $sheet->mergeCells('A2:'.$the_end_index.'2');
            $sheet->setCellValue("A2","铁塔加工重量核算明细表   单位:KG");
            $column = 5;
        }else if($row["type"]==2) $column = 4;
        else $column = 3;
        $thead_span = 1;
        foreach ($tableField as $v) {
            $start_index = Coordinate::stringFromColumnIndex($thead_span).($column-2);
            if(empty($v["son"])){
                $end_index = Coordinate::stringFromColumnIndex($thead_span).($column-1);
                $sheet->mergeCells($start_index.':'.$end_index);
                $sheet->setCellValue($start_index,$v[0]);
                $thead_span++;
            }else{
                $end_index = Coordinate::stringFromColumnIndex($thead_span+count($v["son"])-1).($column-2);
                $sheet->mergeCells($start_index.':'.$end_index);
                $sheet->setCellValue($start_index,$v[0]);
                foreach($v["son"] as $vv){
                    $start_index = Coordinate::stringFromColumnIndex($thead_span).($column-1);
                    $sheet->setCellValue($start_index,$vv[0]);
                    $thead_span++;
                }
            }

        }

        $list_count = [
            1=>["count"=>[0,"J"], "body_weight"=>[0,"Q"], "leg_weight"=>[0,"T"], "update_add_weight"=>[0,"U"], "hang_point_weight"=>[0,"V"], "doublt_bolt_weight"=>[0,"W"], "bolt_add_weight"=>[0,"X"], "wall_add_weight"=>[0,"Y"], "other_weight"=>[0,"Z"], "total_material"=>[0,"AA"], "total_bolt"=>[0,"AB"], "sum_weight"=>[0,"AC"]],
            2=>["count"=>[0,"T"],"weight"=>[0,"U"]],
            3=>["count"=>[0,"D"],"weight"=>[0,"E"]]
        ];
        foreach($list_count[$row["type"]] as $k=>$v){
            if(!isset($table_list[$k])) unset($list_count[$row["type"]][$k]);
            else $list_count[$row["type"]][$k][1] = Coordinate::stringFromColumnIndex($table_list[$k][6]);
        }
        // 开始写入内容
        $size = ceil(count($list) / 500);
        for ($i = 0; $i < $size; $i++) {
            $buffer = array_slice($list, $i * 500, 500);
            // pri($buffer,$table_list,1);
            foreach ($buffer as $k => $v) {
                $span = 1;
                foreach ($table_list as $value) {
                    // 写入excel
                    $rowR = Coordinate::stringFromColumnIndex($span);
                    $sheet->getColumnDimension($rowR)->setWidth(20);
                    // $save_value = ;
                    $sheet->setCellValue(Coordinate::stringFromColumnIndex($span) . $column,$v[$value[1]]);
                    $span++;
                    isset($list_count[$row["type"]][$value[1]])?$list_count[$row["type"]][$value[1]][0]+=$v[$value[1]]:"";
                }
                $column++;
                unset($buffer[$k]);
            }
            $sheet->setCellValue('A'.$column,"小计");
            foreach($list_count[$row["type"]] AS $tv){
                $sheet->setCellValue($tv[1] . $column,$tv[0]);
            }
            $column++;
            $sheet->mergeCells('A'.$column.':'.$the_end_index.$column);
            $sheet->setCellValue('A'.$column,$row["remark"]);
            $column++;
            $sheet->setCellValue('A'.$column,"制表:".$row["writer"]);
            $sheet->setCellValue($the_end_index.$column,"日期:".date("Y.m.d",strtotime($row["writer_time"])));
            $column++;
        }
        $sheet->getstyle("A1:".$the_end_index.$column)->applyFromArray($styleArray);
        $filename=$row["project_name"];
        $writer = new Xls($spreadsheet);
        if (!empty($path)) {
            $writer->save($path);
        } else {
            header("Content-Type:application/vnd.ms-excel;charset=utf-8;");
            header("Content-Disposition:inline;filename=\"{$filename}.xls\"");
            header('Cache-Control: max-age=0');
            $writer->save('php://output');
        }
        exit();
        return true;
    }
    
    protected function fieldHideContent($type=1)
    {
        $tableField = [];
        //名称 field 字符 求和情况 rowspan colspan
        if($type==1){
            $tableField = [
                "pole_number"=>["杆号","pole_number","s","",2,0,"son"=>[]],
                "tower_number"=>["桩号","tower_number","s","",2,0,"son"=>[]],
                "tower_type"=>["塔型","tower_type","s","",2,0,"son"=>[]],
                "height"=>["呼高(m)","height","f","",2,0,"son"=>[]],
                "body_height"=>["本体高(m)","body_height","f","",2,0,"son"=>[]],
                "leg_extension"=>["接腿","","","",0,4,"son"=>[
                    "leg_extension_a"=>["A","leg_extension_a","fs","",0,0],
                    "leg_extension_b"=>["B","leg_extension_b","fs","",0,0],
                    "leg_extension_c"=>["C","leg_extension_c","fs","",0,0],
                    "leg_extension_d"=>["D","leg_extension_d","fs","",0,0],
                ]],
                "count"=>["基数","count","f","sum",2,0,"son"=>[]],
                "connection_type"=>["基础连接型式","connection_type","s","",2,0,"son"=>[]],
                "leaping_dance"=>["跨越舞动","leaping_dance","s","",2,0,"son"=>[]],
                "segment"=>["段号","","","",0,2,"son"=>[
                    "segment_body"=>["本体","segment_body","s","",0,0],
                    "segment_leg"=>["接腿","segment_leg","s","",0,0]
                ]],
                "body"=>["本体(单基)","","","",0,3,"son"=>[
                    "body_material"=>["塔材(Kg)","body_material","f","",0,0],
                    "body_bolt"=>["螺栓(Kg)","body_bolt","f","",0,0],
                    "body_weight"=>["小计(Kg)","body_weight","f","sum",0,0]
                ]],
                "leg"=>["接腿(单基)","","","",0,3,"son"=>[
                    "leg_material"=>["塔材(Kg)","leg_material","f","",0,0],
                    "leg_bolt"=>["螺栓(Kg)","leg_bolt","f","",0,0],
                    "leg_weight"=>["小计(Kg)","leg_weight","f","sum",0,0]
                ]],
                "update_add_weight"=>["修改增重(Kg)","update_add_weight","f","sum",2,0,"son"=>[]],
                "hang_point_weight"=>["挂点增重(Kg)","hang_point_weight","f","sum",2,0,"son"=>[]],
                "doublt_bolt_weight"=>["全塔防盗/双帽螺栓增重(Kg)","doublt_bolt_weight","f","sum",2,0,"son"=>[]],
                "bolt_add_weight"=>["螺栓损耗增重(Kg)","bolt_add_weight","f","sum",2,0,"son"=>[]],
                "wall_add_weight"=>["堵孔螺栓增重(Kg)","wall_add_weight","f","sum",2,0,"son"=>[]],
                "foot_add_weight"=>["防坠落脚钉加强增重(Kg)","foot_add_weight","f","sum",2,0,"son"=>[]],
                "other_weight"=>["其他增重(Kg)","other_weight","f","sum",2,0,"son"=>[]],
                "total"=>["接腿(单基)","","","",0,3,"son"=>[
                    "total_material"=>["塔材(Kg)","total_material","f","sum",0,0],
                    "total_bolt"=>["螺栓(Kg)","total_bolt","f","sum",0,0],
                    "sum_weight"=>["小计(Kg)","sum_weight","f","sum",0,0]
                ]],
                "remark"=>["备注","remark","s","",2,0,"son"=>[]],
                "construction_section"=>["施工标段","construction_section","s","",2,0,"son"=>[]]
            ];
        }else if($type==2){
            $tableField = [
                "tower_type"=>["塔型","tower_type","s","",2,0,"son"=>[]],
                "pole_number"=>["杆号","pole_number","s","",2,0,"son"=>[]],
                "tower_number"=>["桩号","tower_number","s","",2,0,"son"=>[]],
                "height"=>["呼高(m)","height","f","",2,0,"son"=>[]],
                "body_height"=>["本体高(m)","body_height","f","",2,0,"son"=>[]],
                "body_weight"=>["本体重量(kg)","body_weight","f","",2,0,"son"=>[]],
                "leg_extension"=>["接腿","","","",0,8,"son"=>[
                    "leg_extension_a_length"=>["A长度","leg_extension_a_length","fs","",0,0],
                    "leg_extension_a_weight"=>["A重量","leg_extension_a_weight","f","",0,0],
                    "leg_extension_b_length"=>["B长度","leg_extension_b_length","fs","",0,0],
                    "leg_extension_b_weight"=>["B重量","leg_extension_b_weight","f","",0,0],
                    "leg_extension_c_length"=>["C长度","leg_extension_c_length","fs","",0,0],
                    "leg_extension_c_weight"=>["C重量","leg_extension_c_weight","f","",0,0],
                    "leg_extension_d_length"=>["D长度","leg_extension_d_length","fs","",0,0],
                    "leg_extension_d_weight"=>["D重量","leg_extension_d_weight","f","",0,0],
                ]],
                "tower_leg_weight"=>["塔脚重量(Kg)","tower_leg_weight","f","",2,0,"son"=>[]],
                "update_add_weight"=>["修改增量(Kg)","update_add_weight","f","",2,0,"son"=>[]],
                "bolt_add_weight"=>["螺栓增重(Kg)","bolt_add_weight","f","",2,0,"son"=>[]],
                "other_weight"=>["支架/附件增重(Kg)","other_weight","f","",2,0,"son"=>[]],
                "sum_weight"=>["単基重量(Kg)","sum_weight","f","",2,0,"son"=>[]],
                "count"=>["基数","count","f","sum",2,0,"son"=>[]],
                "weight"=>["总重(kg)","weight","f","sum",2,0,"son"=>[]],
                "remark"=>["备注","remark","s","",2,0,"son"=>[]],
                // "segment_form"=>["段位组成","segment_form","s","",2,0,"son"=>[]]
            ];
        }else{
            $tableField = [
                "tower_type"=>["型号及规格","tower_type","s","",2,0,"son"=>[]],
                "unit"=>["单位","unit","s","",2,0,"son"=>[]],
                "count"=>["数量","count","f","",2,0,"son"=>[]],
                "weight_total"=>["重量","","","",0,2,"son"=>[
                    "sum_weight"=>["単基重量(kg)","sum_weight","f","sum",0,0],
                    "weight"=>["总重(kg)","weight","f","sum",0,0]
                ]],
                "remark"=>["备注","remark","s","",2,0,"son"=>[]]
            ];
        }
        return $tableField;
    }

    public function print($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $list = $this->detailModel->field("*,count*sum_weight as weight")->where(["draft_id"=>["=",$row["ID"]]])->select();
        $fieldHideContent = $this->fieldHideContent($row["type"]);
        $total_list = ["tower_type"=>"合计"];
        foreach($list as $k=>$v){
            $list[$k] = $v->toArray();
            foreach($fieldHideContent as $vv){
                if(count($vv["son"])==0){
                    if($vv[3]=="sum"){
                        isset($total_list[$vv[1]])?"":$total_list[$vv[1]] = 0;
                        $total_list[$vv[1]] += $v[$vv[1]];
                    }
                    if($vv[2]!="s") isset($v[$vv[1]])?$list[$k][$vv[1]] = round($v[$vv[1]],2):0;
                }else{
                    foreach($vv["son"] as $vvv){
                        if($vvv[3]=="sum"){
                            isset($total_list[$vvv[1]])?"":$total_list[$vvv[1]] = 0;
                            $total_list[$vvv[1]] += $v[$vvv[1]];
                        }
                        if($vvv[2]!="s") isset($v[$vvv[1]])?$list[$k][$vvv[1]] = round($v[$vvv[1]],2):0;
                    }
                }
            }
        }
        if($row["auditor"]){
            $list[] = $total_list;
        }
        $this->assignConfig("row",$row);
        $this->assignConfig("list",$list);
        $this->assignConfig("ids",$ids);
        return $this->view->fetch();
    }
}
