<?php

namespace app\admin\controller\chain\sale;

use app\admin\model\chain\sale\ProjectCateLog;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 销售合同
 *
 * @icon fa fa-circle-o
 */
class Compact extends Backend
{
    
    /**
     * Compact模型对象
     * @var \app\admin\model\chain\sale\Compact
     */
    protected $model = null;
    protected $listModel = null,$saleTypeList = null,$saleTypeTree = null;
    protected $noNeedLogin = ["contrast","selectProject"];
    protected $fieldList = [
        1 => "",
        3 => "T",
        4 => "G",
        5 => "H"
    ];
    
    // protected $saleTypeList;
    // protected $saleTypeTree;
    
    
    protected $admin;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\sale\Compact;
        $this->listModel = new \app\admin\model\chain\sale\OrderDetail;
        list($this->saleTypeList,$this->saleTypeTree) = $this->saleType(1);
        $saleList = [];
        foreach($this->saleTypeList as $k=>$v){
            $saleList[$v] = $v;
        }
        $this->admin = \think\Session::get('admin');
        $this->view->assign('produce_type',$this->_productTypeList());
        $this->assignconfig('produce_type',$this->_productTypeList());
        $this->view->assign("saleType",$saleList);
        $this->view->assign("sortList",$this->sortList());
        $this->view->assign("belongList",$this->belongList());
        $this->view->assign("moneyList",$this->moneyList());
        $this->view->assign("wayList",$this->wayList());
        $this->view->assign("fuwu",$this->fuwu());

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias('a')
                ->join(["cg_purchase"=>"c"],"a.poItemId=c.poItemId","left")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $params_table = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                if(strtotime($params["C_DeliverDate"])<=time()) return $this->error("计划交货时间不得小于等于制单时间");
                $tableRow = [];
                if(!empty($params_table)){
                    unset($params_table["OD_ID"]);
                    foreach($params_table as $k=>$v){
                        foreach($v as $kk=>$vv){
                            $tableRow[$kk][$k] = $vv;
                        }
                    }
                    $params_table_count = count($tableRow);
                }
                $pc_type = (new ProjectCateLog())->where("PC_Num",$params["PC_Num"])->value("PC_type");
                $params["produce_type"] = $pc_type?$pc_type:1;
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $year = date("Y",time());
                    $findOne = $this->model->where("C_Num",'LIKE',$year.'%')->where("produce_type","=",$params["produce_type"])->order("C_Num DESC")->find();
                    if($params["produce_type"]==3){
                        $params["C_Sumprice"] = 0;
                        $ex_field = $year.'-T';
                    }else{
                        $params["C_Sumprice"] = $params["C_Sumprice"];
                        $fieldList = $this->fieldList;
                        $ex_field = $year;
                        $fieldList[$params["produce_type"]]?$ex_field.="-".$fieldList[$params["produce_type"]]:$ex_field;
                    }
                    if(isset($findOne["C_Num"])){
                        $num = substr($findOne["C_Num"],-4);
                        $num = $ex_field.str_pad((++$num),4,0,STR_PAD_LEFT);
                    }else $num = $ex_field.'0001';
                    $params["C_Num"] = $num;
                    $params["C_Writer"] = $this->admin["nickname"];
                    if(!empty($tableRow)){
                        for($i=0;$i<$params_table_count;$i++){
                            $tableRow[$i]['C_Num'] = $num;
                            if($params['produce_type']==3)
                            $tableRow[$i]['OD_LineNum'] = $tableRow[$i]["OD_LineNum"]?$tableRow[$i]["OD_LineNum"]:$tableRow[$i]["OD_TypeName"];
                        }
                    }
                    $result = $this->model->allowField(true)->save($params);
                    if($result) $this->listModel->allowField(true)->saveAll($tableRow);

                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$num);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        
        $this->view->assign("tableField",$this->tableField());
        $this->assignconfig("tableField",$this->tableField());
        $this->assignconfig("type_flag",1);
        return $this->view->fetch();
    }

    public function selectProject()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\sale\ProjectCateLog())
                ->where("Auditor","<>","")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            // pri($list,1);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["ST_Name"] = $this->saleTypeList[$v["ST_Num"]];
            }
            // pri($rows,1);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        $tableField = [];
        $list = $this->listModel->where("C_Num",$ids)->order("OD_ID ASC")->select();
        foreach($list as $k=>$v){
            foreach($this->tableField() as $kk=>$vv){
                $tableField[$v["OD_ID"]][] = [
                    "name" => $vv[0],
                    "rule" => $vv[1],
                    "type" => $vv[2],
                    "value" => $list[$k][$vv[0]]
                ];
            }
        }
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $params_table = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if(strtotime($params["C_DeliverDate"])<=time()) return $this->error("计划交货时间不得小于等于制单时间");
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    $update = $tableRow = [];
                    $this->listModel->where("C_Num",$ids)->delete();
                    if(!empty($params_table)){
                        foreach($params_table as $k=>$v){
                            foreach($v as $kk=>$vv){
                                $tableRow[$kk][$k] = $vv;
                            }
                        }
                        foreach($tableRow as $k=>$v){
                            if($row['produce_type']==3){
                            (isset($tableField[$v["OD_ID"]]) and $tableField[$v["OD_ID"]][3]["value"]==$v["OD_LineNum"])? ($v["OD_LineNum"] = $v["OD_TypeName"]):"";
                            $v["OD_LineNum"] = $v["OD_LineNum"]?$v["OD_LineNum"]:$v["OD_TypeName"];
                            }
                            unset($v["OD_ID"]);
                            $update[] = array_merge($v,["C_Num"=>$ids]);
                        }
                        $this->listModel->allowField(true)->saveAll($update);
                    }
                    
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        $tableHeadField = $this->tableField();
        $tableHeadField[4][3] = $this->_productTypeList()[$row["produce_type"]];
        $tableHeadField[6][4] = $row["produce_type"]==3?"*型号":"*塔型";
        $this->view->assign("tableHeadField", $tableHeadField);
        $this->view->assign("tableField", $tableField);
        $this->assignconfig("tableField", $tableHeadField);
        $this->assignconfig("type_flag",$row["produce_type"]);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, $ids)->select();
            $salegtModel = new \app\admin\model\chain\sale\Salegt();
            $count = 0;
            Db::startTrans();
            try {
                if(empty($salegtModel->where("C_Num",$list[0]["C_Num"])->find())){
                    $count += $list[0]->delete();
                    $this->listModel->where('C_Num',$list[0]["C_Num"])->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function auditor()
    {
        // echo 1;
        $C_Num = $this->request->post("C_Num");
        $admin = $this->admin["nickname"];
        $result = $this->model->update(['C_Num' => $C_Num,'Auditor'=>$admin,"AuditorDate"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function giveUp()
    {
        // echo 1;
        $C_Num = $this->request->post("C_Num");
        $result = $this->model->update(['C_Num' => $C_Num,'Auditor'=>""]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function contrast()
    {
        
        $ids = $this->request->post("ids");
        if(!$ids) return json(["code"=>0,"leftList"=>[],"rightList"=>[]]);
        $leftList = $this->listModel->field("OD_LineNum,OD_LineName,OD_Pressure,OD_TypeName,OD_Height,OD_Count,OD_Weight,(OD_Count*OD_Weight) as OD_SumWeight,DeliverPlace,OD_Memo")->where("C_Num",$ids)->order("OD_ID ASC")->select();
        
        $leftList = objectToArray($leftList);
        $rightList = (new \app\admin\model\chain\sale\Sectconfigdetail())->alias("sd")
            ->join(["salegt"=>"s"],"sd.SGT_ID=s.SGT_ID","left")
            ->field("sd.SCD_TPNum,sd.TD_Pressure,sd.TD_TypeName,sd.TH_Height,sd.SCD_Count,sd.SCD_Weight,(sd.SCD_Count*sd.SCD_Weight) as SCD_SumWeight")
            ->where("s.C_Num",$ids)
            ->select();
        $rightList = objectToArray($rightList);
        return json(["code"=>1,"leftList"=>$leftList,"rightList"=>$rightList]);
    }

    public function sortList(){
        $sortList = [1=>"正式合同",2=>"电话订单",3=>"口头订单",4=>"其他",5=>"非正式合同",6=>"网内合同"];
        return $sortList;
    }
    public function belongList(){
        $belongList = ["公司"=>"公司","铁塔"=>"铁塔"];
        return $belongList;
    }
    public function moneyList(){
        $moneyList = ["RMB人民币"=>"RMB人民币","USD美元"=>"USD美元","EUR欧元"=>"EUR欧元","JPY日元"=>"JPY日元","GBP英镑"=>"GBP英镑","CAD加拿大元"=>"CAD加拿大元","AUD澳大利亚元"=>"AUD澳大利亚元"];
        return $moneyList;
    }
    public function wayList(){
        $wayList = ["保函"=>"保函","汇票"=>"汇票","无"=>"无","预付款"=>"预付款"];
        return $wayList;
    }
    public function fuwu(){
        $fuwu = ["无"=>"无","安装"=>"安装","服务"=>"服务"];
        return $fuwu;
    }
    public function tableField()
    {
        $tableField = [
            ["OD_ID","","text",0,"OD_ID","OD_ID"],
            ["draft_detail_id","","text",0,"draft_detail_id","ID"],
            ["OD_LineName","","text","","线路名称","OD_LineName"],
            ["OD_LineNum","","text","","杆塔号","pole_number"],
            ["OD_ProductName","","text","角钢塔","产品名称","OD_ProductName"],
            ["OD_Pressure","","text","","电压等级","OD_Pressure"],
            ["OD_TypeName","data-rule='required'","text","","*塔型","tower_type"],
            ["OD_Height","","text","","呼高","height"],
            ["OD_Count","data-rule='required'","text",1,"数量","count"],
            ["OD_Unit","data-rule='required'","text","基","单位","unit"],
            ["OD_Weight","","text","","单重","sum_weight"],
            ["OD_SumWeight","readonly","text","","总重","weight"],
            ["C_Price","","text","0","单价","C_Price"],
            ["price_way","","select","1","总价计算方式","price_way"],
            ["C_Sumprice","readonly","text","","总价","C_Sumprice"],
            ["DeliverPlace","","text","","送货地点","DeliverPlace"],
            ["OD_Memo","","text","","备注","OD_Memo"]
        ];
        return $tableField;
    }

}
