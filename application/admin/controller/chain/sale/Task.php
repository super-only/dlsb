<?php

namespace app\admin\controller\chain\sale;

use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\model\chain\pack\ProduceUnitIn;
use app\admin\model\chain\pack\SaleInvoiceSingle;
use app\admin\model\chain\sale\Compact;
use app\admin\model\chain\sale\Sectconfigdetail;
use app\admin\model\chain\sale\TaskHeight;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use jianyan\excel\Excel;

/**
 * 生产销售联系单
 *
 * @icon fa fa-circle-o
 */
class Task extends Backend
{
    
    /**
     * Task模型对象
     * @var \app\admin\model\chain\sale\Task
     */
    protected $noNeedLogin = ["eipUpload","confirmChange","derive","print","contrast","selectCompact","addShowTable","showTable"];
    protected $model = null,$detailModel = null, $heightModel = null,$sectconfigModel=null,$oldModel=null;
    protected $companyList = ["公司"=> "公司", "外协"=> "外协"];
    protected $showField = [
        ["SCD_ID","SCD_ID",'hidden'],
        ["杆塔号","SCD_TPNum",""],
        ["线路名","SCD_lineName",""],
        ["产品名称","SCD_ProductName",""],
        ["塔型/型号","TD_TypeName",""],
        ["电压等级","TD_Pressure",""],
        ["可下数","SCD_Count",""]
    ];
    protected $fieldList = [
        1 => "",
        3 => "T",
        4 => "G",
        5 => "H"
    ];
    protected function getTableField()
    {
        $dealshowField[1] = [
            ["SCD_ID","SCD_ID","text","data-rule='required' readonly","hidden"],
            ["TD_ID","TD_ID","text"," readonly","hidden"],
            ["TH_ID","TH_ID","text"," readonly","hidden"],
            ["线路名称","P_Num","text"," readonly",""],
            ["产品名称","P_Name","text"," readonly",""],
            ["电压等级","TD_Pressure","text"," readonly",""],
            ["塔型","TD_TypeName","text","data-rule='required' readonly",""],
            ["呼高","TH_Height","text","readonly",""],
            ["数量","SCD_Count","text","data-rule='required' disabled",""],
            ["最新数","task_count","text","data-rule='required'",""],
            ["初始单重","SCD_Weight","text","",""],
            ["同步","SCD_ChangeWeight","button","",""],
            ["修改单重","SCD_ChangeWeight","text","",""],
            ["杆塔号","SCD_TPNum","text"," readonly",""],
            ["单位","TPN_ID","text"," readonly",""],
            ["交货时间","TH_DeliverTime","text","",""],
            ["备注","SCD_Memo","text","",""],
        ];
        $dealshowField[3] = [
            ["SCD_ID","SCD_ID","text","data-rule='required' readonly","hidden"],
            ["TD_ID","TD_ID","text"," readonly","hidden"],
            ["TH_ID","TH_ID","text"," readonly","hidden"],
            ["线路名称","P_Num","text"," readonly",""],
            ["产品名称","P_Name","text"," readonly",""],
            ["电压等级","TD_Pressure","text"," readonly","hidden"],
            ["型号","TD_TypeName","text","data-rule='required' readonly",""],
            ["呼高","TH_Height","text","readonly","hidden"],
            ["数量","SCD_Count","text","data-rule='required' disabled",""],
            ["最新数","task_count","text","data-rule='required'",""],
            ["初始单重","SCD_Weight","text","",""],
            ["同步","SCD_ChangeWeight","button","",""],
            ["修改单重","SCD_ChangeWeight","text","",""],
            ["杆塔号","SCD_TPNum","text"," readonly",""],
            ["单位","TPN_ID","text"," readonly",""],
            ["交货时间","TH_DeliverTime","text","",""],
            ["备注","SCD_Memo","text","",""],
        ];
        $dealshowField[4] = $dealshowField[5] = $dealshowField[1];
        return $dealshowField;
    }
    // protected $dealshowField = [
    //     ["SCD_ID","SCD_ID","text","data-rule='required' readonly","hidden"],
    //     ["TD_ID","TD_ID","text"," readonly","hidden"],
    //     ["TH_ID","TH_ID","text"," readonly","hidden"],
    //     ["线路名称","P_Num","text"," readonly",""],
    //     ["产品名称","P_Name","text"," readonly",""],
    //     ["电压等级","TD_Pressure","text"," readonly",""],
    //     ["塔型","TD_TypeName","text","data-rule='required' readonly",""],
    //     ["呼高","TH_Height","text","readonly",""],
    //     ["数量","SCD_Count","text","data-rule='required' readonly",""],
    //     // ["最新数","task_count","text","data-rule='required'",""],
    //     ["初始单重","SCD_Weight","text","",""],
    //     ["同步","SCD_ChangeWeight","button","",""],
    //     ["修改单重","SCD_ChangeWeight","text","",""],
    //     ["杆塔号","SCD_TPNum","text"," readonly",""],
    //     ["单位","TPN_ID","text"," readonly",""],
    //     ["交货时间","TH_DeliverTime","text","",""],
    //     ["备注","TH_Memo","text","",""],
    // ];
    protected $admin;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\sale\Task;
        $this->detailModel = new \app\admin\model\chain\sale\TaskDetail;
        $this->heightModel = new \app\admin\model\chain\sale\TaskHeight;
        $this->sectconfigModel = new \app\admin\model\chain\sale\Sectconfigdetail;
        $this->oldModel = new \app\admin\model\chain\lofting\NewOldTdTypeName;
        $this->admin = \think\Session::get('admin');
        $this->view->assign("sortList",$this->_productTypeList(1));
        $this->assignconfig("sortList",$this->_productTypeList(1));
        $this->view->assign("companyList",$this->companyList);
        $this->view->assign("showField",$this->showField);
        $this->view->assign("dealshowField",$this->getTableField());
        $this->assignconfig("dealshowField",$this->getTableField());
        $this->view->assign("nowtime",date("Y-m-d H:i:s",time()));
        $unitList = $this->measurementList();
        $this->view->assign("unitList",$unitList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model->alias('t')
                    ->join([ 'compact'=>'c' ],'t.C_Num=c.C_Num','LEFT')
                    ->join(["taskdetail"=>"td"],"t.T_Num = td.T_Num","left")
                    ->field('td.TD_TypeName as "td.TD_TypeName",t.*,c.PC_Num,c.PC_Num as "c.PC_Num",c.Customer_Name,c.Customer_Name as "c.Customer_Name",c.C_SaleType,c.C_SaleType as "c.C_SaleType",c.C_SaleMan,c.C_SaleMan as "c.C_SaleMan"')
                    ->where($where)
                    ->group("t.T_Num")
                    ->order($sort, $order)
                    ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function contrast()
    {
        $ids = $this->request->post("ids");
        if(!$ids) return json(["code"=>0,"data"=>[]]);
        $list = (new Sectconfigdetail())->alias("scd")
            ->join(["taskheight"=>"th"],"scd.TH_ID=th.TH_ID","left")
            ->join(["taskdetail"=>"td"],"th.TD_ID=td.TD_ID","left")
            ->join(["task"=>"t"],"td.T_Num=t.T_Num","left")
            ->field("t.T_Date,td.P_Num,td.P_Name,td.TD_Pressure,td.TD_TypeName,td.TD_TypeName as TD_OtherTypeName,th.TH_Height,scd.SCD_Count,scd.SCD_Weight,(scd.SCD_Count*scd.SCD_Weight) as Sum_Weight,scd.SCD_Unit,scd.SCD_Memo,td.TD_ID,th.TH_ID")
            ->where("t.T_Num",$ids)
            ->select();
        $lists = $dtmaterialList = $producetask = $tdIdList = [];
        foreach($list as $v){
            $tdIdList[] = $v["TD_ID"];
            $lists[] = $v->toArray();
        }
        $rows = (new Dtmaterial())->field("TD_ID")->where("TD_ID","in",$tdIdList)->group("TD_ID")->select();
        foreach($rows as $k=>$v){
            $dtmaterialList[$v["TD_ID"]] = $v["TD_ID"];
        }
        $rows = (new \app\admin\model\chain\lofting\ProduceTask())->field("TD_ID")->where("TD_ID","in",$tdIdList)->group("TD_ID")->select();
        foreach($rows as $v){
            $producetask[$v["TD_ID"]] = $v["TD_ID"];
        }
        $rows = (new \app\admin\model\chain\lofting\MergTypeName())->field("TD_ID,mTD_ID")->where("TD_ID","in",$tdIdList)->select();
        foreach($rows as $v){
            $producetask[$v["TD_ID"]] = $v["TD_ID"];
            isset($dtmaterialList[$v["mTD_ID"]])?$dtmaterialList[$v["TD_ID"]] = $v["TD_ID"]:"";
        }
        foreach($lists as $k=>$v){
            if(isset($producetask[$v["TD_ID"]])) $lists[$k]["state"] = "已下达";
            elseif(isset($dtmaterialList[$v["TD_ID"]])) $lists[$k]["state"] = "已放样";
            else  $lists[$k]["state"] = "未放样";
        }


        return json(["code"=>1,"data"=>array_values($lists)]);
    }

    public function delSectTask()
    {
        $td = $this->request->post("num");
        $rows = (new Dtmaterial())->field("TD_ID")->where("TD_ID",$td)->find();
        if($rows) return json(["code"=>0,"msg"=>"已放样，不可删除"]);
        else return json(["code"=>1,"msg"=>"删除成功"]);
    }

    public function delPretend()
    {
        $scd = $this->request->post("num");
        $sectModel = new Sectconfigdetail();
        $one = $sectModel->get($scd);
        $update = $sectModel->where("SCD_ID",$scd)->update(["SCD_Count"=>0,"task_count"=>0]);
        $sum = $sectModel->field("sum(SCD_Count) as TD_Count")->where("TH_ID",$one["TH_ID"])->GROUP("TH_ID")->SELECT();
        $th_update = (new TaskHeight())->where("TH_ID",$one["TH_ID"])->update(["TD_Count"=>$sum[0]["TD_Count"]]);
        return json(["code"=>1,"msg"=>"已删除"]);
    }

    public function showTable()
    {
        $taskList = $arr = [];
        $C_Num = $this->request->post("C_Num");
        if(!$C_Num) return json(["code"=>0,"data"=>$arr, "msg"=>"请补全合同"]);
        $taskList = $this->model->where("C_Num",$C_Num)->find();
        if(!empty($taskList))  return json(["code"=>2,"data"=>$taskList["T_Num"]]);
        $list = (new \app\admin\model\chain\sale\Salegt())
            ->field("SGT_ID")
            ->where("C_Num",$C_Num)
            ->find();
        if(empty($list)) return json(["code"=>0,"data"=>$arr, "msg"=>"请补全合同"]);
        $detailList = $this->sectconfigModel
            ->field("SCD_ID,SCD_TPNum,SCD_lineName,SCD_ProductName,TD_TypeName,TD_Pressure,SCD_Count")
            ->where("SGT_ID",$list["SGT_ID"])
            ->where("TH_ID","0")
            ->select();
        foreach($detailList as $k=>$v){
            $arr[$v["SCD_ID"]] = $v->toArray();
        }
        return json(["code"=>1,"data"=>array_values($arr),"showList"=>array_keys($arr),'msg'=>"success"]);
    }

    public function addShowTable()
    {
        $arr = [];
        $num = $this->request->post("num");
        $detailList = $this->sectconfigModel
            ->field("SCD_ID,SCD_TPNum,SCD_lineName,SCD_ProductName,TD_TypeName,TD_Pressure,SCD_Count")
            ->where("SCD_ID",$num)
            ->select();
        foreach($detailList as $k=>$v){
            $arr[$v["SCD_ID"]] = $v->toArray();
        }
        return json(["code"=>1,"data"=>array_values($arr),"showList"=>array_keys($arr),'msg'=>"success"]);
    }

    public function dealTable()
    {
        $scdIdArr = $arr = [];
        $tableShow = $this->request->post("tableShow/a");
        if(!isset($tableShow["check"]) or empty($tableShow["check"])) return json(["code"=>0,"data"=>$scdIdArr, "msg"=>"请勾选"]);
        $scdIdArr = $tableShow["check"];
        $detailList = $this->sectconfigModel
            ->field("SCD_ID,
                    SCD_lineName as P_Num,
                    SCD_ProductName as P_Name,
                    TD_TypeName,
                    TD_Pressure,
                    TH_Height,
                    SCD_Count,
                    SCD_Count as task_count,
                    SCD_Weight,
                    SCD_TPNum,
                    SCD_Unit as TPN_ID,
                    SCD_SendDate as TH_DeliverTime,
                    SCD_Memo")
            // ->where("SCD_ID","IN",implode(",",$scdIdArr))
            ->whereIn("SCD_ID",$scdIdArr)
            // ->where("TH_ID","0")
            ->select();
        $weight = 0;
        foreach($detailList as $k=>$v){
            $arr[$v["SCD_ID"]] = $v->toArray();
            // $weight += $v["SCD_Count"]*$v["SCD_Weight"];
        }
        return json(["code"=>1,"data"=>array_values($arr),"dealList"=>array_keys($arr),"weight"=>$weight,'msg'=>"success"]);
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $params_table = $this->request->post("table_row/a");
            
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $compact = (new Compact())->get($params["C_Num"]);
                $params["T_Sort"] = $this->_productTypeList()[$compact["produce_type"]];
                //if(strtotime($params["T_WriterDate"])>=strtotime($params["T_Date"])) return $this->error("计划开始时间不能大于要求交货时间");
                $result = false;
                Db::startTrans();
                try {
                    foreach($params as $k=>$v){
                        if($v==='') unset($params[$k]);
                    }
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $year = date("Y",time());
                    $findOne = $this->model->where("T_Num","LIKE","NO.".$year."%")->where("T_Sort","=",$params["T_Sort"])->order("T_Num DESC")->value("T_Num");
                    $fieldList = $this->fieldList;
                    $ex_field = $year;
                    $fieldList[$compact["produce_type"]]?$ex_field.="-".$fieldList[$compact["produce_type"]]:$ex_field;
                    if($findOne){
                        $num = substr($findOne,-4);
                        $T_Num = $ex_field.str_pad((++$num),4,0,STR_PAD_LEFT);
                    }else $T_Num = $ex_field.'0001';
                    $T_Num = "NO.".$T_Num;
                    $params["T_Num"] = $T_Num;

                    $params["Writer"] = $this->admin["nickname"];
                    //$params["T_WriterDate"] = $params["T_WriterDate"]?$params["T_WriterDate"]:date("Y-m-d H:i:s");
                    $params["T_Num"] = $T_Num;
                    $detail_list_add = $height_list_add = $sect_list = $old_add = [];
                    if(!empty($params_table)){
                        list($detail_list_add,$detail_list_edit,$height_list_edit,$height_list_add,$sect_list,$old_add,$old_edit) = $this->addEditSectTask($params_table,$T_Num,[]);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    if(!empty($detail_list_add)) $this->detailModel->saveAll($detail_list_add, false);
                    if(!empty($height_list_add)) $this->heightModel->saveAll($height_list_add, false);
                    if(!empty($sect_list)) $this->sectconfigModel->saveAll($sect_list);
                    if(!empty($old_add)) $this->oldModel->saveAll($old_add,false);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$T_Num);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->assignconfig("js_way",1);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $arr = [];
        $row = $this->model->alias('t')
            ->join([ 'compact'=>'s' ],'t.C_Num=s.C_Num','LEFT')
            ->join([ 'salegt' => 'c'], 't.C_Num=c.C_Num','LEFT')
            ->field("t.Writer,t.T_WriterDate,s.produce_type,c.SGT_ID,t.T_Num,t.C_Num,s.C_Name,s.Customer_Name,t.t_project,t.t_shortproject,t.T_Sort,t.T_SumWeight,t.T_Date,t.T_Company,t.T_LogTime,t.T_BaseTime,t.T_Memo,t.Auditor")
            ->where("T_Num",$ids)
            ->find();
        $sectconfigList = $this->sectconfigModel
            ->field("SCD_ID,SCD_TPNum,SCD_lineName,SCD_ProductName,TD_TypeName,TD_Pressure,SCD_Count,TH_ID")
            ->where("SGT_ID",$row["SGT_ID"])
            ->where("TH_ID","0")
            ->select();
        foreach($sectconfigList as $k=>$v){
            $arr[$v["SCD_ID"]] = $v->toArray();
        }
        $detailList = $this->detailModel->alias('t')
            ->join(["taskheight" => "h"], "t.TD_ID = h.TD_ID")
            ->join(["sectconfigdetail" => "s"], "h.TH_ID = s.TH_ID")
            ->field("s.SCD_ID,
                    t.TD_ID,
                    h.TH_ID,
                    t.P_Num,
                    t.P_Name,
                    t.TD_TypeName,
                    t.TD_Pressure,
                    h.TH_Height,
                    s.task_count as SCD_Count,
                    s.task_count,
                    h.TD_Count,
                    s.SCD_Weight,
                    s.SCD_ChangeWeight,
                    s.SCD_TPNum,
                    h.TPN_ID,
                    h.TH_DeliverTime,
                    s.SCD_Memo")
            ->where("T_Num",$ids)
            ->select();
        $detailArr = $dealList = $taskheighttd = $id_task_detail_list = $id_task_height_list = $task_list = [];
        foreach($detailList as $v){
            isset($task_list[$v["TD_TypeName"]])?$task_list[$v["TD_TypeName"]] += $v["task_count"]:$task_list[$v["TD_TypeName"]] = $v["task_count"];
            $detailArr[] = $v->toArray();
            $dealList[] = $v["SCD_ID"];
            $taskheighttd[$v["TH_Height"].'-'.$v["TD_ID"]] = $v["TH_ID"];
            $id_task_detail_list[$v["TD_ID"]] = $v["TD_ID"];
            $id_task_height_list[$v["TH_ID"]] = $v["TH_ID"];
        }
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $params_table = $this->request->post("table_row/a");
            if ($params) {
                //if(strtotime($params["T_WriterDate"])>=strtotime($params["T_Date"])) return $this->error("计划开始时间不能大于要求交货时间");
                foreach($params as $k=>$v){
                    if($v==='') unset($params[$k]);
                }
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                $detail_list_add = $detail_list_edit = $height_list_edit = $height_list_add = $sect_list = $old_add = $old_edit = [];
                try {
                    $result = $this->model->get($ids)->allowField(true)->save($params);
                    if(!empty($params_table)){
                        list($detail_list_add,$detail_list_edit,$height_list_edit,$height_list_add,$sect_list,$old_add,$old_edit) = $this->addEditSectTask($params_table,$ids,$taskheighttd);
                    }
                    // pri($old_add,$old_edit);die;
                    $diff_arr = array_diff($id_task_detail_list,array_keys($detail_list_edit));
                    if(!empty($diff_arr)){
                        $this->detailModel->where("TD_ID","IN",$diff_arr)->delete();
                        $this->oldModel->where("TD_ID","IN",$diff_arr)->delete();
                    }
                    if(!empty($detail_list_edit)) $this->detailModel->saveAll($detail_list_edit);
                    
                    
                    $diff_arr = array_diff($id_task_height_list,array_keys($height_list_edit));
                    if(!empty($diff_arr)) $this->heightModel->where("TH_ID","IN",$diff_arr)->delete();
                    if(!empty($height_list_edit)) $this->heightModel->saveAll($height_list_edit);
                    
                    $diff_arr = array_diff($dealList,array_keys($sect_list));
                    foreach($diff_arr as $k=>$v){
                        $sect_list[$v]['SCD_ID'] = $v;
                        $sect_list[$v]['TH_ID'] = 0;
                        $sect_list[$v]['task_count'] = 0;
                    }
                    if(!empty($sect_list)) $this->sectconfigModel->saveAll($sect_list);


                    if(!empty($detail_list_add)) $this->detailModel->saveAll($detail_list_add, false);
                    if(!empty($height_list_add)) $this->heightModel->saveAll($height_list_add, false);
                    if(!empty($old_add)) $this->oldModel->saveAll($old_add,false);
                    if(!empty($old_edit)) $this->oldModel->saveAll($old_edit);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("type_number", count($task_list));
        $this->view->assign("number", array_sum($task_list));
        $this->view->assign("row", $row);
        $this->view->assign("showArr",array_values($arr));
        $this->view->assign("showList",array_keys($arr));
        $this->view->assign("detailArr",array_values($detailArr));
        $this->view->assign("dealList",$dealList);
        $this->assignconfig("ids",$ids);
        $this->assignconfig("js_way", $row["produce_type"]);
        return $this->view->fetch();
    }

    public function selectCompact()
    {
        $produce_type = $this->_productTypeList();
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = (new \app\admin\model\chain\sale\Salegt())->alias('s')
                    ->join([ 'compact'=>'c' ],'s.C_Num=c.C_Num','LEFT')
                    ->join(["task"=>"t"],"t.C_Num = c.C_Num",'LEFT')
                    ->field('s.*,c.Customer_Name,c.C_Project,c.C_SortProject,c.PC_Num,group_concat(t.T_Num) as T_Num,c.produce_type')
                    ->where($where)
                    ->where("s.Auditor","<>","")
                    ->group("c.C_Num")
                    ->order($sort, $order)
                    ->paginate($limit);
            foreach($list as $k=>$v){
                $list[$k]["type"] = $produce_type[$v["produce_type"]];
            }

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->assignconfig("produce_type",$produce_type);
        return $this->view->fetch();
    }

    // public function delDetail()
    // {
    //     $arr = [];
    //     $TD_ID = $this->request->post("TD_ID");
    //     if($TD_ID){
    //         Db::startTrans();
    //         try {
    //             $TH_one = $this->heightModel->field("TH_ID")->where("TD_ID",$TD_ID)->find();
    //             $this->detailModel->where("TD_ID",$TD_ID)->delete();
    //             $this->heightModel->where("TD_ID",$TD_ID)->delete();
    //             $this->sectconfigModel->where("TH_ID",$TH_one["TH_ID"])->update(["TH_ID"=>0]);
                
    //             Db::commit();
    //         } catch (ValidateException $e) {
    //             Db::rollback();
    //             $this->error($e->getMessage());
    //         } catch (PDOException $e) {
    //             Db::rollback();
    //             $this->error($e->getMessage());
    //         } catch (Exception $e) {
    //             Db::rollback();
    //             $this->error($e->getMessage());
    //         }
    //     }
    //     return json(["code"=>1,'msg'=>"success"]);
    // }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();
            $taskDetail = $this->detailModel->field("TD_ID")->where($pk,$ids)->select();
            $task_detail_id = $task_height_id = [];
            foreach($taskDetail as $v){
                $task_detail_id[] = $v["TD_ID"];
            }
            if(!empty($task_detail_id)){
				$rows = (new Dtmaterial())->field("TD_ID")->where("TD_ID","IN",$task_detail_id)->find();
				if($rows) $this->error("已有放样，不能删除！");
			}
            $count = 0;
            Db::startTrans();
            try {
                foreach($taskDetail as $v){
                    $v->delete();
                }
                $taskHeight = $this->heightModel->field("TH_ID")->where("TD_ID","IN",$task_detail_id)->select();
                foreach($taskHeight as $v){
                    $task_height_id[] = $v["TH_ID"];
                    $v->delete();
                }
                $this->sectconfigModel->where("TH_ID","IN",$task_height_id)->update(['TH_ID' => 0,"task_count"=>0]);
                foreach ($list as $v) {
                    $count += $v->delete();
                }
                $this->oldModel->where("TD_ID","IN",$task_detail_id)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function auditor()
    {
        // echo 1;
        $C_Num = $this->request->post("T_Num");
        $admin = $this->admin["nickname"];
        $result = $this->model->update(['T_Num' => $C_Num,'Auditor'=>$admin,"T_AuditorDate"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function giveUp()
    {
        // echo 1;
        $C_Num = $this->request->post("T_Num");
        $result = $this->model->update(['T_Num' => $C_Num,'Auditor'=>""]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function addEditSectTask($params_table=[],$ids=0,$taskheighttd=[])
    {
        $table_list = $tableDetailCate = $detail_list_add = $height_list_add = $sect_list = $old_add = $old_edit = [];
        $detail_list_edit = $height_list_edit = [];
        foreach($params_table as $k=>$v){
            foreach($v as $kk=>$vv){
                $table_list[$params_table['SCD_ID'][$kk]][$k] = $vv;
            }
        }
        foreach($table_list as $k=>$v){
            $tableDetailCate[$v["P_Num"].'(-)'.$v["P_Name"].'(-)'.$v["TD_Pressure"].'(-)'.$v["TD_TypeName"]]["self"] = $v;
            $tableDetailCate[$v["P_Num"].'(-)'.$v["P_Name"].'(-)'.$v["TD_Pressure"].'(-)'.$v["TD_TypeName"]]["height"][$v["TH_Height"]][] = $v;
        }
        $task_detail_one = $this->detailModel
            ->field("TD_ID")
            ->order("TD_ID DESC")
            ->find();
        $task_detail_id = $task_detail_one?$task_detail_one["TD_ID"]+1:1;
        $task_height_one = $this->heightModel
            ->field("TH_ID")
            ->order("TH_ID DESC")
            ->find();
        $task_height_id = $task_height_one?$task_height_one["TH_ID"]+1:1;
        foreach($tableDetailCate as $k=>$v){
            $task_id = $v["self"]["TD_ID"]==0?$task_detail_id:$v["self"]["TD_ID"];
            $detail_list = [
                "P_Num" => $v["self"]["P_Num"],
                "P_Name" => $v["self"]["P_Name"],
                "TD_Pressure" => $v["self"]["TD_Pressure"],
                "TD_TypeName" => $v["self"]["TD_TypeName"],
                "TD_Memo" => $v["self"]["SCD_Memo"],
                "T_Num" => $ids,
                "TD_ID" => $task_id,
            ];
            foreach($v["height"] as $kk=>$vv){
                $height_id = $taskheighttd[$kk.'-'.$v["self"]["TD_ID"]]??$task_height_id;
                foreach($vv as $vvv){
                    isset($height_list[$height_id])?"":$height_list[$height_id] = [
                        "TH_ID" => $height_id,
                        "TD_ID" => $task_id,
                        "TH_Height" => $kk,
                        "TH_Weight" => $vvv["SCD_Weight"],
                        "TPN_ID" => $vvv["TPN_ID"],
                        "TH_DeliverTime" => $vvv["TH_DeliverTime"],
                        "TH_Memo" => $vvv["SCD_Memo"],
                        "Valid" => 1,
                        "TD_Count" => 0,
                        "SCD_list" => []
                    ];
                    $height_list[$height_id]["SCD_list"][$vvv["SCD_ID"]] = $vvv["SCD_ID"];
                    $height_list[$height_id]["TD_Count"] = count($height_list[$height_id]["SCD_list"]);
                    $sect_list[$vvv["SCD_ID"]] = ['SCD_ID'=>$vvv["SCD_ID"],"task_count"=>$vvv["task_count"],"TH_ID"=>$height_id,"SCD_Memo"=>$vvv["SCD_Memo"]];
                }
                unset($height_list[$height_id]["SCD_list"]);
                if(isset($taskheighttd[$kk.'-'.$v["self"]["TD_ID"]])) $height_list_edit[$height_id] = $height_list[$height_id];
                else {
                    $task_height_id++;
                    $height_list_add[] = $height_list[$height_id];
                }
            }
            if($v["self"]["TD_ID"]==0){
                $task_detail_id++;
                $detail_list_add[] = $detail_list;
                $old_add[$detail_list["TD_ID"]] = ["TD_ID"=>$detail_list["TD_ID"],"TD_TypeName"=>$detail_list["TD_TypeName"]];
            }else{
                $detail_list_edit[$task_id] = $detail_list;
                $old_edit[$detail_list["TD_ID"]] = ["TD_ID"=>$detail_list["TD_ID"],"TD_TypeName"=>$detail_list["TD_TypeName"]];
            }
        }
        return [$detail_list_add,$detail_list_edit,$height_list_edit,$height_list_add,$sect_list,$old_add,$old_edit];
    }

    public function confirmChange()
    {
        list($scd_id,$change_weight) = array_values($this->request->post());
        $result = ["code"=>1,"msg"=>"成功"];
        $data = (new SaleInvoiceSingle())->alias("sis")->join(["saleinvoice"=>"si"],"sis.SI_Num=si.SI_Num")->where("SIS_ScdId",$scd_id)->value("SI_Date");
        if($data) $result = ["code"=>0,"msg"=>"该杆塔号在".$data."已经出库，是否仍然同步修改单重？"];
        return json($result);
    } 

    public function changeWeight()
    {
        list($scd_id,$change_weight) = array_values($this->request->post());
        if(!$scd_id or !$change_weight) return json(["code"=>0,"msg"=>"同步失败，稍后重试"]);
        $scd_one = $this->sectconfigModel->get($scd_id);
        if(!$scd_one) return json(["code"=>0,"msg"=>"同步失败，稍后重试"]);
        else if(!$scd_one["TH_ID"]) return json(["code"=>0,"msg"=>"同步失败，无需修改同步"]);
        $update_result = false;
        Db::startTrans();
        try {
            $update_result = $this->sectconfigModel->where("SCD_ID",$scd_id)->update(["SCD_ChangeWeight"=>$change_weight]);
            $produce_unit_result = (new ProduceUnitIn())->where("SCD_ID",$scd_id)->update(["PU_Weight"=>$change_weight]);
            (new SaleInvoiceSingle())->where("SIS_ScdId",$scd_id)->update(["SIS_Weight"=>$change_weight]);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($update_result !== false) {
            return json(["code"=>1,"msg"=>"同步修改成功"]);
        } else {
            return json(["code"=>0,"msg"=>"同步失败"]);
        }
    }

    public function print($t_num = null)
    {
        $ids = $t_num;
        $arr = [];
        $row = $this->model->alias('t')
            ->join([ 'compact'=>'s' ],'t.C_Num=s.C_Num','LEFT')
            ->join([ 'salegt' => 'c'], 't.C_Num=c.C_Num','LEFT')
            ->field("t.Writer,s.produce_type,c.SGT_ID,t.T_Num,t.C_Num,s.C_Name,s.Customer_Name,t.t_project,t.t_shortproject,t.T_Sort,t.T_SumWeight,t.T_Date,t.T_Company,t.T_LogTime,t.T_BaseTime,t.T_Memo,t.Auditor,t.T_WriterDate,s.C_SinglePrice,s.C_SaleMan")
            ->where("T_Num",$ids)
            ->find();
        $sectconfigList = $this->sectconfigModel
            ->field("SCD_ID,SCD_TPNum,SCD_lineName,SCD_ProductName,TD_TypeName,TD_Pressure,SCD_Count,TH_ID")
            ->where("SGT_ID",$row["SGT_ID"])
            ->where("TH_ID","0")
            ->select();
        foreach($sectconfigList as $k=>$v){
            $arr[$v["SCD_ID"]] = $v->toArray();
        }
        $detailList = $this->detailModel->alias('t')
            ->join(["taskheight" => "h"], "t.TD_ID = h.TD_ID")
            ->join(["sectconfigdetail" => "s"], "h.TH_ID = s.TH_ID")
            ->field("s.SCD_ID,
                    t.TD_ID,
                    h.TH_ID,
                    t.P_Num,
                    t.P_Name,
                    t.TD_TypeName,
                    t.TD_Pressure,
                    h.TH_Height,
                    s.task_count as SCD_Count,
                    s.task_count,
                    h.TD_Count,
                    (case when ifnull(s.SCD_ChangeWeight,0)=0 then s.SCD_Weight else s.SCD_ChangeWeight end) AS TH_Weight,
                    s.SCD_TPNum,
                    h.TPN_ID,
                    h.TH_DeliverTime,
                    h.TH_Memo,
                    s.SCD_Memo,
                    s.SCD_UnitPrice,
                    s.price_way,
                    case when s.price_way=1 then round(SCD_UnitPrice*(case when ifnull(s.SCD_ChangeWeight,0)=0 then s.SCD_Weight else s.SCD_ChangeWeight end)*task_count,2) else round(SCD_UnitPrice*task_count,2) end as sum_price")
            ->where("T_Num",$ids)
            ->where("s.task_count","<>",0)
            ->select();
        $detailArr = $dealList = $taskheighttd = $id_task_detail_list = $id_task_height_list = $task_list = [];
        foreach($detailList as $k=>$v){
            isset($task_list[$v["TD_TypeName"]])?$task_list[$v["TD_TypeName"]] += $v["task_count"]:$task_list[$v["TD_TypeName"]] = $v["task_count"];
            $detailArr[$k] = $v->toArray();
            $detailArr[$k]["price"] = $v["SCD_UnitPrice"];
            $detailArr[$k]["TH_Height"] = $v["TH_Height"]==0?"":$v["TH_Height"];
            $detailArr[$k]["SCD_TPNum"] = $v["SCD_TPNum"]==$v["TD_TypeName"]?"":$v["SCD_TPNum"];
            $dealList[] = $v["SCD_ID"];
            $taskheighttd[$v["TH_Height"].'-'.$v["TD_ID"]] = $v["TH_ID"];
            $id_task_detail_list[$v["TD_ID"]] = $v["TD_ID"];
            $id_task_height_list[$v["TH_ID"]] = $v["TH_ID"];
        }
        $row["T_Date"] = strtotime($row["T_Date"])>=strtotime($row["T_WriterDate"])?date("Y年n月d日", strtotime($row["T_Date"])):"";
        $row["T_WriterDate"] = date("Y年n月d日", strtotime($row["T_WriterDate"]));
        $rows = (new Dtmaterial())->field("TD_ID")->where("TD_ID","in",$id_task_detail_list)->group("TD_ID")->select();
        foreach($rows as $k=>$v){
            $dtmaterialList[$v["TD_ID"]] = $v["TD_ID"];
        }
        $rows = (new \app\admin\model\chain\lofting\ProduceTask())->field("TD_ID")->where("TD_ID","in",$id_task_detail_list)->group("TD_ID")->select();
        foreach($rows as $v){
            $producetask[$v["TD_ID"]] = $v["TD_ID"];
        }
        foreach($detailArr as $k=>$v){
            $v["Sum_Weight"] = $v["TH_Weight"]*$v["SCD_Count"];
            $detailArr[$k]["Sum_Weight"] = $v["Sum_Weight"];
            $detailArr[$k]["price"] = round($v["price"],5);
            $detailArr[$k]["sum_price"] = round($v["sum_price"],2);
            if(isset($producetask[$v["TD_ID"]])) $detailArr[$k]["state"] = "已下达";
            elseif(isset($dtmaterialList[$v["TD_ID"]])) $detailArr[$k]["state"] = "已放样";
            else $detailArr[$k]["state"] = "未放样";
        }
        $this->assignconfig("row",$row);
        $this->assignconfig("list",$detailArr);
        return $this->view->fetch();
    }

    public function derive($ids=null)
    {
        $task_one = $this->model->get($ids);
        if($task_one["T_Sort"]=="铁附件"){
            $C_SaleMan = (new Compact())->where("C_Num",$task_one["C_Num"])->value("C_SaleMan");
            $customer = (new Compact())->where("C_Num",$task_one["C_Num"])->value("Customer_name");
            $sectList = $this->sectconfigModel->alias("sect")
                ->join(["taskheight"=>"th"],"sect.TH_ID = th.TH_ID")
                ->JOIN(["taskdetail"=>"td"],"td.TD_ID = th.TD_ID")
                ->field("sect.TD_TypeName,sect.SCD_Unit,sect.SCD_Count,sect.SCD_Weight as unit_weight,round(sect.SCD_Weight*sect.SCD_Count,3) as sum_weight,sect.SCD_UnitPrice as unit_price,(case when sect.price_way=1 then round(sect.SCD_Weight*sect.SCD_Count*sect.SCD_UnitPrice,2) else round(sect.SCD_Count*sect.SCD_UnitPrice,2) end) as sum_price,sect.SCD_Memo")
                ->where("td.T_Num",$ids)
                ->where("sect.task_count","<>",0)
                ->where("sect.SCD_Count","<>",0)
                ->select();
            if($sectList) $sectList = array_chunk(collection($sectList)->toArray(),16);
            else $sectList = [];
            $headList = ["TD_TypeName","SCD_Unit","SCD_Count","unit_weight","sum_weight","unit_price","sum_price","SCD_Memo"];
            $spreadsheet = new Spreadsheet();
            foreach($sectList as $k=>$v){
                $spreadsheet->createSheet();
                $spreadsheet->setActiveSheetIndex($k);
                $sheet = $spreadsheet->getActiveSheet($k);
                $sheet->setTitle("第".($k+1)."页");
                $styleArray = [
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER, //水平居中
                        'vertical' => Alignment::VERTICAL_CENTER, //垂直居中
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => [
                                'rgb' => '000000'
                            ]
                        ]
                    ]
                ];
                $mergeList = [
                    "A1:J1","A2:J2","A3:J3","A4:J4","A5:J5","A6:J6","A7:J7","A8:B8","C8:F8","H8:J8","A9:B9","C9:F9","G9:J9","B10:C11","E10:E11","F10:G11","H10:I11","A28:C28","A29:J30","A31:J31"
                ];
                $setValueList = [
                    "A2" => "铁附件                                                            Q/SDS 1012-05",
                    "A3" => "附录E",
                    "A4" => "（规范性附录）",
                    "A5" => "绍兴电力设备有限公司",
                    "A6" => "生产销售联系单",
                    "A7" => date("Y年m月d日",strtotime($task_one["T_WriterDate"]))."                       序号：".$task_one["T_Num"]."                  第".($k+1)."页 共".count($sectList)."页",
                    "A8" => "工程单位",
                    "C8" => $customer,
                    "G8" => "工程名称",
                    "H8" => $task_one["t_project"],
                    "A9" => "抄送单位",
                    "C9" => "公司办、仓库、质保部、铁塔车间、镀锌车间",
                    "G9" => "附件：",
                    "A10" => "编",
                    "A11" => "号",
                    "B10" => "型号规格",
                    "D10" => "单",
                    "D11" => "位",
                    "E10" => "数量",
                    "F10" => "重量",
                    "H10" => "金额(元)",
                    "F11" => "一件",
                    "G11" => "总重(kg)",
                    "H11" => "单价(元/公斤)",
                    "I11" => "小计(元)",
                    "J10" => "备",
                    "J11" => "注",
                    "A28" => "合计",
                    "A29" => $task_one["T_Memo"],
                    "A31" => "制单： ".$task_one["Writer"]."                                                    业务员：".$C_SaleMan
                ];
                $count = $weight = $price = 0;
                $row = 12;
                foreach($v as $kk=>$vv){
                    $sheet->setCellValue("A".$row,$kk+1);
                    foreach($headList as $hkk=>$hvv){
                        if($hvv == "TD_TypeName"){
                            $sheet->mergeCells("B".$row.":C".$row);
                            $sheet->setCellValue("B".$row,$vv[$hvv]);
                        }else $sheet->setCellValue((Coordinate::stringFromColumnIndex($hkk+3)).$row,$vv[$hvv]);
                        
                    }
                    $count += $vv["SCD_Count"];
                    $weight += $vv["sum_weight"];
                    $price += $vv["sum_price"];
                    $row++;
                }
                for($i=$row;$i<=27;$i++){
                    $sheet->setCellValue("A".$i,($i-11));
                    $mergeList[] = "B".$i.":C".$i;
                }
                $setValueList["E28"] = $count;
                $setValueList["G28"] = $weight;
                $setValueList["I28"] = $price;
                $sheet->mergeCells(implode(",",$mergeList));
                foreach($setValueList as $sk=>$sv){
                    $sheet->setCellValue($sk,$sv);
                }
                $sheet->getstyle("A1:J31")->applyFromArray($styleArray);
            }
            $fileName = $task_one["t_project"].'生产销售联系单';

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx'); //按照指定格式生成Excel文件
            $this->excelBrowserExport($fileName, 'Xlsx');
            $writer->save('php://output');
        }else{
            $title = $task_one["t_project"];
            $towerList = $this->detailModel->alias("td")
                ->join(["taskheight"=>"th"],"td.TD_ID=th.TD_ID")
                ->JOIN(["sectconfigdetail"=>"scd"],"scd.TH_ID=th.TH_ID")
                ->field("scd.TD_TypeName,scd.TH_Height,scd.SCD_TPNum,SCD_Memo,'基' as unit,SCD_Count,(case when ifnull(SCD_ChangeWeight,0)=0 then SCD_Weight else SCD_ChangeWeight end) as weight")
                ->WHERE("td.T_Num",$ids)
                ->SELECT();
            if($towerList) $towerList = collection($towerList)->toArray();
            else $towerList = [];
            $header = [
                ['塔型', 'TD_TypeName'],
                ['呼高', 'TH_Height'],
                ['桩位号', 'SCD_TPNum'],
                ['备注', 'SCD_Memo'],
                ['单位', 'unit'],
                ['数量', 'SCD_Count'],
                ['重量（kg）', 'weight']
            ];
    
            return Excel::exportData($towerList, $header, $title . date('Ymd'));
        }
    }

    /**
	 * 输出到浏览器(需要设置header头)
	 * @param string $fileName 文件名
	 * @param string $fileType 文件类型
	 */
	public function excelBrowserExport($fileName, $fileType) {

	    //文件名称校验
	    if(!$fileName) {
	        trigger_error('文件名不能为空', E_USER_ERROR);
	    }

	    //Excel文件类型校验
	    $type = ['Excel2007', 'Xlsx', 'Excel5', 'xls'];
	    if(!in_array($fileType, $type)) {
	        trigger_error('未知文件类型', E_USER_ERROR);
	    }

	    if($fileType == 'Excel2007' || $fileType == 'Xlsx') {
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
	        header('Cache-Control: max-age=0');
	    } else { //Excel5
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment;filename="'.$fileName.'.xls"');
	        header('Cache-Control: max-age=0');
	    }
	}


    // public function eipUpload()
    // {
    //     $T_Num = $this->request->post("T_Num");
    //     $json_result = ["code"=>0,"msg"=>"失败"];
    //     $where = [
    //         "T_Sort" => ["=","铁塔"],
    //         "T_Num" => ["=",$T_Num]
    //     ];
    //     $params = (new \app\admin\model\view\TowerDetialView())->field("T_Num,poItemId,sum(SCD_Count) as SCD_Count,T_WriterDate,T_Date,group_concat(TD_TypeName) as concat_TD_TypeName,towerEipUpload")->where($where)->group("T_Num")->find();
    //     if(!$params) return json($json_result);
    //     $purchaseApiControl = new PurchaseApi();
    //     $saveData = [];
    //     $param_url = "supplier-ipo";
    //     // foreach($params as $k=>$v){
    //         $operatetype = "ADD";
    //         $api_data = [
    //             "purchaserHqCode"=>"SGCC",
    //             "ipoType"=>0,
    //             "supplierCode"=>"1000014615",
    //             "supplierName"=>"绍兴电力设备有限公司",
    //             "ipoNo"=>$params["T_Num"],
    //             "categoryCode"=>"60",
    //             "subclassCode"=>"60001",
    //             "poItemId"=>$params["poItemId"],
    //             "dataType"=>1,
    //             "materialsCode"=>"铁塔",
    //             "materialsName"=>"铁塔",
    //             "materialsUnit"=>"基",
    //             "materialsDesc"=>$params["concat_TD_TypeName"],
    //             "amount"=>$params["SCD_Count"],
    //             "unit"=>"基",
    //             "planStartDate"=>date("Y-m-d",strtotime($params["T_WriterDate"])),
    //             "planFinishDate"=>date("Y-m-d",strtotime($params["T_Date"])),
    //             "dataSource"=>0,
    //             "dataSourceCreateTime"=>date("Y-m-d H:i:s")
    //         ];
    //         if($params["towerEipUpload"]) $operatetype = 'UPDATE';
    //         $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$api_data);
    //         if($api_result["code"]==1) $this->model->where("T_Num",$T_Num)->update(["eip_upload"=>1]);
    //         else $saveData[$T_Num] = $api_result["msg"];
    //     // }
    //     $msg = "上传成功";
    //     if(!empty($saveData)) {
    //         $msg = "";
    //         foreach($saveData as $k=>$v){
    //             $msg .= $k.$v.";";
    //         }
    //         $json_result["msg"] = $msg;
    //     }else $json_result = ["code"=>1,"msg"=>$msg];
    //     return json($json_result);
    // }
}
