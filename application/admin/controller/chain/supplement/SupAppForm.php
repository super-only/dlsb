<?php

namespace app\admin\controller\chain\supplement;

use app\admin\controller\Technology;
use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\model\chain\sale\TaskDetail;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 补件申请单
 *
 * @icon fa fa-circle-o
 */
class SupAppForm extends Technology
{
    
    /**
     * SupAppForm模型对象
     * @var \app\admin\model\chain\supplement\SupAppForm
     */
    protected $model = null,$detailModel=null,$bjtype=null,$admin=null;
    protected $noNeedLogin = ["detailMaterial","chooseDetail","selectProject","searchNo","editContent"];

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_field = \think\Session::get('technology_field');
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_type = \think\Session::get('technology_type');
        $this->technology_xd = \think\Session::get('technology_xd');
        // $this->model = DB($this->technology_ex."taskrenewmain");
        // $this->ttt =3;
        $this->model = new \app\admin\model\chain\supplement\TaskReNewMain([],$this->technology_ex);
        // $this->model->setTablediy();
        $this->detailModel = new \app\admin\model\chain\supplement\TaskReNew([],$this->technology_ex);
        $this->bjtype = ["工地补件"=>"工地补件","厂内补件"=>"厂内补件"];
        $this->view->assign("bjtype",$this->bjtype);
        $this->assignconfig("bjtype",$this->bjtype);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            // pri($this->model->alias("trnm")->join(["taskdetail"=>"td"],"td.TD_ID = trnm.TD_ID"),1);
            $list = $this->model->alias("trnm")
                ->join(["taskrenew"=>"trn"],"trnm.TRNM_Num = trn.TRNM_Num")
                ->join(["taskdetail"=>"td"],"td.TD_ID = trnm.TD_ID")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num")
                ->join(["newoldtdtypename"=>"nottn"],"nottn.TD_ID = td.TD_ID","left")
                ->field("trnm.*,t.C_Num,t.t_project,td.T_Num,td.TD_TypeName,nottn.old_TypeName")
                ->where($where)
                ->group("trnm.newBnum,trnm.T_Num")
                ->order($sort, $order)
                // ->select();
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        return $this->view->fetch();
    }

    public function selectProject()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\sale\ProjectCateLog())->alias("pc")
                ->join(["compact"=>"c"],"c.PC_Num = pc.PC_Num")
                ->join(["task"=>"t"],"c.C_Num = t.C_Num")
                ->join(["taskdetail"=>"td"],"t.T_Num = td.T_Num")
                ->field("pc.PC_Num,c.C_Name,pc.PC_ProjectName,pc.PC_ShortName,pc.PC_Employer,pc.PC_Pressure,pc.PC_Place,pc.PC_StartPlace,pc.PC_StartDate,pc.PC_gross,pc.PC_EditDate,pc.PC_SalesMan,group_concat(td.TD_TypeName) as TD_TypeName")
                ->where("pc.Auditor","<>","")
                ->where($where)
                ->where("c.produce_type",$this->technology_type)
                ->order($sort, $order)
                ->group("pc.PC_Num")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }
    
    public function searchNo()
    {
        $num = $this->request->post("num");
        $num = json_decode(str_replace('&quot;','"',$num), true);
        if(!$num) return json(["code"=>0,"msg"=>"有误，请稍后重试"]);
        $bjtype = $this->request->post("bjtype");
        // $bnum = $this->request->post("bnum");
        $task_detail_model = new TaskDetail();
        $year = date("Y");
        $one = $this->model->order("newBnum DESC")->limit(1)->value("newBnum");
        $bnum_xh = $one?$one+1:1;
        $bnum_xh = str_pad(($bnum_xh),11,0,STR_PAD_LEFT);

        $num_one = $this->model->where("TRNM_Num","LIKE","BJ".$year."%")->order("TRNM_Num DESC")->limit(1)->value("right(TRNM_Num,4)");
        $num_xh = $num_one?$num_one+1:1;
        // if($one){
        //     $xh = (int)substr($one["TRNM_Num"],-4);
        //     $bnum_xh = $one["newBnum"]?str_pad(($one["newBnum"]+1),8,0,STR_PAD_LEFT):$bnum_xh;
        // }
        $list = $task_detail_model->alias("td")
            ->join(["task"=>"t"],"td.T_Num = t.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->join([$this->technology_ex."producetask"=>"pt"],"td.TD_ID=pt.TD_ID")
            // ->join(["taskrenewmain"=>"trnm"],"trnm.TD_ID = td.TD_ID","LEFT")
            ->field("td.T_Num,td.TD_ID,pt.PT_Num,ifnull(PT_DHmemo,'') as PT_DHmemo")
            ->where("c.PC_Num","IN",$num)
            ->group("pt.PT_Num")
            ->order("td.TD_ID ASC")
            ->select();
        $add_list = [];
        foreach($list as $k=>$v){
            // if(!$v["TRNM_Num"]){
            $add_list[$k] = $v->toArray();
            $add_list[$k]["ty_tower"] = $v["PT_DHmemo"];
            $add_list[$k]["TRNM_Sort"] = $bjtype;
            $add_list[$k]["newBnum"] = str_pad(($bnum_xh),8,0,STR_PAD_LEFT);
            $add_list[$k]["TRNM_Num"] = "BJ".$year.str_pad(($num_xh),4,0,STR_PAD_LEFT);
                
            // $add_list[$k]["TRNM_Sort"] = $trnm_sort;
            $add_list[$k]["TRNM_Time"] = date("Y-m-d H:i:s");
            $add_list[$k]["TRNM_WriteDate"] = date("Y-m-d H:i:s");
            $add_list[$k]["TRNM_Writer"] = $this->admin["nickname"];
            $num_xh++;
            // }else{
                // $save_list[$k] = $v->toArray();
            // }
            // if($v["TRNM_Sort"]) $trnm_sort = $v["TRNM_Sort"];
        }
        // $trnm_sort = $trnm_sort?$trnm_sort:"工地补件";
        // foreach($add_list as $k=>$v){
        // }
        // $save_result = $this->model->allowField(true)->saveAll($save_list);
        $add_result = $this->model->allowField(true)->saveAll($add_list,false);
        if ($add_result != false) {
            return json(["code"=>1,"data"=>$add_result[0]["TRNM_Num"]]);
        } else {
            return json(["code"=>0,"msg"=>"有误，请稍后重试"]);
        }
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        // $one = $this->model->get($ids);

        $row = $this->model->alias("trnm")
            ->join(["taskdetail"=>"td"],"trnm.TD_ID = td.TD_ID")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->where("trnm.TRNM_Num",$ids)
            ->field("trnm.*,td.TD_TypeName,td.T_Num,t.t_project,trnm.TD_ID,c.PC_Num")
            ->order("TRNM_Num")
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $list = $this->model->alias("trnm")
            ->join(["taskdetail"=>"td"],["trnm.TD_ID=td.TD_ID","trnm.T_Num = td.T_Num"],"left")
            ->join(["task"=>"t"],"td.T_Num=t.T_Num","left")
            ->join(["compact"=>"c"],"t.C_Num = c.C_Num","left")
            ->join([$this->technology_ex."producetask"=>"pt"],"pt.PT_Num=trnm.PT_Num","left")
            ->field("trnm.TRNM_Num,td.TD_TypeName,td.TD_ID,td.T_Num,trnm.TRNM_Memo,trnm.PT_Num,pt.PT_DHmemo")
            ->where(["trnm.newBnum"=>["=",$row["newBnum"]]])
            ->order("TD_ID ASC")
            ->select();
        $TRNM_Num_list = $dt_detail_list = [];
        foreach($list as $k=>$v){
            $TRNM_Num_list[$v["TRNM_Num"]] = $v->toArray();
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_right_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                foreach($TRNM_Num_list as $k=>$v){
                    if($v["TRNM_Num"] == $params["TRNM_Num"]) $TRNM_Num_list[$k] = array_merge($TRNM_Num_list[$k],$params);
                    else{
                        $TRNM_Num_list[$k]["TRNM_Sort"] = $params["TRNM_Sort"];
                        // $TRNM_Num_list[$k]["TRNM_Bnum"] = $params["TRNM_Bnum"];
                        $TRNM_Num_list[$k]["TRNM_Time"] = $params["TRNM_Time"];
                    }
                }
                $sectSaveList = [];
                foreach($paramsTable["TRN_ID"] as $k=>$v){
                    $sectSaveList[$k] = [
                        "TD_ID" => $paramsTable["TD_ID"][$k],
                        "TRN_ID" => $v,
                        "TRN_Project" => $paramsTable["TRN_Project"][$k],
                        "TRN_TPNum" => $paramsTable["TRN_TPNum"][$k],
                        "TRN_TPNumber" => $paramsTable["TRN_TPNumber"][$k],
                        "TRN_TPWeight" => $paramsTable["TRN_TPWeight"][$k],
                        "TRNM_Num" => $paramsTable["TRNM_Num"][$k],
                        "DtS_Name" => $paramsTable["DtS_Name"][$k]
                    ];
                    if(!$v) unset($sectSaveList[$k]["TRN_ID"]);
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->allowField(true)->saveAll($TRNM_Num_list);
                    if(!empty($sectSaveList)) $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        
        
        $dt_list = $this->detailModel->alias("trn")
            ->join([$this->technology_ex."dtmaterial"=>"dtm"],"trn.TD_ID=dtm.TD_ID")
            ->join([$this->technology_ex."dtsect"=>"dts"],["dtm.DtM_iID_PK = dts.DtM_iID_FK","trn.DtS_Name = dts.DtS_Name"])
            ->join([$this->technology_ex."dtmaterialdetial"=>"dtmd"],["dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK","dtmd.DtMD_sPartsID = trn.TRN_TPNum"])
            ->field("trn.TD_ID,trn.TRNM_Num,trn.TRN_ID,trn.DtS_Name,CAST(trn.DtS_Name AS UNSIGNED) AS number_1,trn.TRN_Project,trn.TRN_TPNum,CAST(trn.TRN_TPNum AS UNSIGNED) AS number_2,trn.TRN_TPNumber,dtmd.DtMD_sStuff,dtmd.DtMD_sMaterial,dtmd.DtMD_sSpecification,dtmd.DtMD_iUnitCount,dtmd.DtMD_iLength,dtmd.DtMD_fWidth,dtmd.DtMD_iTorch,dtmd.DtMD_iUnitHoleCount,dtmd.DtMD_fUnitWeight as TRN_TPWeight,round(trn.TRN_TPNumber*dtmd.DtMD_fUnitWeight,2) as sum_weight,dtmd.DtMD_sRemark,dtmd.type")
            ->where("trn.TRNM_Num","in",array_keys($TRNM_Num_list))
            ->order("trn.TD_ID,trn.TRNM_Num,number_1,number_2")
            ->select();
        foreach($dt_list as $k=>$v){
            $dt_detail_list[$k] = $v->toArray();
            $dt_detail_list[$k]["TD_TypeName"] = @$TRNM_Num_list[$v["TRNM_Num"]]["TD_TypeName"];
        }
        $tableLeftField = $this->getLeftTableField();
        $tableRightField = $this->getRightTableField();
        $this->view->assign("tableLeftField",$tableLeftField);
        $this->view->assign("tableRightField",$tableRightField);
        $this->assignconfig('tableRightField',$tableRightField);
        $this->view->assign("detail_list",$dt_detail_list);
        $this->view->assign("row",$row);
        $this->view->assign("list",$TRNM_Num_list);
        $this->view->assign("ids",$ids);
        $this->assignconfig("ids",$ids);
        
        return $this->view->fetch();
    }

    public function editContent()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,'msg'=>"有误请重试"]);
        $one = $this->model->alias("trnm")
            ->join(["taskdetail"=>"td"],"trnm.TD_ID = td.TD_ID")
            ->field("trnm.TD_ID,trnm.T_Num,trnm.TRNM_Num,td.TD_TypeName,trnm.TRNM_Time,trnm.TRNM_Memo,trnm.ty_tower")
            ->where("trnm.TRNM_Num",$num)
            ->find();
        if(!$one) return json(["code"=>0,'msg'=>"有误请重试"]);
        else return json(["code"=>1,'data'=>$one]);

    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,'msg'=>"删除失败"]);
        $count = $this->detailModel->where("TRN_ID",$num)->delete();
        if($count) return json(["code"=>1,'msg'=>"删除成功"]);
        else return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function chooseDetail($ids=null)
    {
        if(!$ids) $this->error(__('No Results were found'));

        $tableDetailField = $this->getDetailField();
        $this->view->assign("tableDetailField",$tableDetailField);
        $this->assignconfig("tableDetailField",$tableDetailField);
        $this->assignconfig("TRNM_Num",$ids);
        return $this->view->fetch();
    }

    public function detailMaterial()
    {
        list($TRNM_Num,$DtS_Name,$DtMD_iWelding) = array_values($this->request->post());
        if(!$TRNM_Num) return json(["code"=>0,"msg"=>"有误，失败"]);
        $dtm_model = new Dtmaterial([],$this->technology_ex);
        $pt_num = $this->model->where("TRNM_Num",$TRNM_Num)->find();
        // $dtm_one = $dtm_model->alias("dt")->join(["taskrenewmain"=>"trnm"],"trnm.TD_ID = dt.TD_ID")->where("trnm.TRNM_Num",$TRNM_Num)->find();
        if(!$pt_num) return json(["code"=>0,"msg"=>"有误，失败"]);
        $where = [
            "ptd.PT_Num" => ["=",$pt_num["PT_Num"]],
        ];
        if($DtS_Name) $where["dts.DtS_Name"] = ["=",$DtS_Name];
        if($DtMD_iWelding==1) $where["dtmd.DtMD_iWelding"] = ["<>",0];
        else if($DtMD_iWelding==0) $where["dtmd.DtMD_iWelding"] = ["=",1];
        $list = $dtm_model->alias("d")
            ->join([$this->technology_ex."dtsect"=>"dts"],"d.DtM_iID_PK = dts.DtM_iID_FK")
            ->join([$this->technology_ex."dtmaterialdetial"=>"dtmd"],"dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK")
            ->join([$this->technology_ex."producetaskdetail"=>"ptd"],"ptd.DtMD_ID_PK=dtmd.DtMD_ID_PK")
            ->field("'".$pt_num["TRNM_Num"]."' AS TRNM_Num,'".$pt_num["TD_ID"]."' AS TD_ID,dts.DtS_Name,dtmd.DtMD_sPartsID AS TRN_TPNum,dtmd.DtMD_sRemark,dtmd.DtMD_sStuff,dtmd.DtMD_sMaterial,dtmd.DtMD_sSpecification,dtmd.DtMD_iLength,dtmd.DtMD_fWidth,dtmd.DtMD_iTorch,(CASE WHEN dtmd.DtMD_iWelding=0 THEN '否' ELSE '是' END) AS DtMD_iWelding,ptd.PTD_Count as DtMD_iUnitCount,ptd.PTD_Count as TRN_TPNumber,ptd.PTD_SWeight as sum_weight,ptd.PTD_SWeight as TRN_TPWeight,dtmd.DtMD_iUnitHoleCount,'".$pt_num["T_Num"]."' AS T_Num,d.DtM_sTypeName as TD_TypeName,d.DtM_sPressure,dtmd.DtMD_iFireBending,dtmd.DtMD_KaiHeJiao,dtmd.DtMD_DaBian,dtmd.DtMD_iCuttingAngle,dtmd.DtMD_fBackOff,dtmd.DtMD_iBackGouging,dtmd.DtMD_GeHuo,dtmd.type,
            case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn")
            ->where($where)
            ->order("DtS_Name,bjbhn")
            ->select();
        foreach($list as $k=>$v){
            $list[$k]["TRN_TPNum_Input"] = build_input('TRN_TPNum_Input', 'text', $v["TRN_TPNum"],["readonly"=>"readonly","class"=>"small_input"]);
        }
        return json(["code"=>1,"data"=>$list]);
    }

    public function allDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>__('No Results were found')]);
        $row = $this->model->alias("trnm")
            ->join(["taskdetail"=>"td"],"trnm.TD_ID = td.TD_ID")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->where("trnm.TRNM_Num",$num)
            ->field("trnm.*,td.TD_TypeName,td.T_Num,t.t_project,trnm.TD_ID,c.PC_Num")
            ->order("TRNM_Num")
            ->find();
        if (!$row) {
            return json(["code"=>0,"msg"=>__('No Results were found')]);
        }
        $list = $this->model->alias("trnm")
            ->join(["taskdetail"=>"td"],["trnm.TD_ID=td.TD_ID","trnm.T_Num = td.T_Num"])
            ->join(["task"=>"t"],"td.T_Num=t.T_Num")
            ->join(["compact"=>"c"],"t.C_Num = c.C_Num")
            ->field("trnm.TRNM_Num,td.TD_TypeName,td.TD_ID,td.T_Num,trnm.TRNM_Memo")
            ->where(["c.PC_Num"=>["=",$row["PC_Num"]],"trnm.TRNM_Bnum"=>["=",$row["TRNM_Bnum"]]])
            ->order("TD_ID ASC")
            ->select();
        $trnm_num_list = [];
        foreach($list as $v){
            $trnm_num_list[$v["TRNM_Num"]] = $v["TRNM_Num"];
        }
       
        if(!empty($trnm_num_list)){
            $result = 0;
            Db::startTrans();
            try {
                $result += $this->model->where("TRNM_Num","in",$trnm_num_list)->delete();
                $result += $this->detailModel->where("TRNM_Num","in",$trnm_num_list)->delete();
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
            if ($result!=0) {
                return json(["code"=>1,"msg"=>"删除成功！"]);
            } else {
                return json(["code"=>0,"msg"=>"请稍后重试！"]);
            }
        }else return json(["code"=>0,"msg"=>"请稍后重试！"]);
        
        
    }

    //编辑table
    public function getLeftTableField()
    {
        $list = [
            ["单据号","TRNM_Num","hidden"],
            ["塔型","TD_TypeName",""],
            ["任务单","T_Num",""],
            ["TD_ID","TD_ID","hidden"],
            ["下达单号","PT_Num",""],
            ["套用塔型","PT_DHmemo",""],
            ["备注","TRNM_Memo","hidden"]
        ];
        return $list;
    }
    //编辑table
    public function getRightTableField()
    {
        $list = [
            ["TRNM_Num","TRNM_Num","hidden","readonly"],
            ["TD_ID","TD_ID","hidden","readonly"],
            ["TRN_ID","TRN_ID","hidden","readonly"],
            ["塔型","TD_TypeName","","readonly"],
            ["段号","DtS_Name","","readonly"],
            ["杆塔号","TRN_Project","",""],
            ["部件号","TRN_TPNum","","readonly"],
            ["数量","TRN_TPNumber","",""],
            ["材料","DtMD_sStuff","","readonly"],
            ["材质","DtMD_sMaterial","","readonly"],
            ["规格","DtMD_sSpecification","","readonly"],
            // ["单基件数","DtMD_iUnitCount","","readonly"],
            ["长度","DtMD_iLength","","readonly"],
            ["宽度","DtMD_fWidth","","readonly"],
            ["厚度","DtMD_iTorch","","readonly"],
            ["单件孔数","DtMD_iUnitHoleCount","","readonly"],
            ["单重","TRN_TPWeight","","readonly"],
            ["总重","sum_weight","","readonly"],
            ["类型","type","","readonly"],
            ["工艺备注","DtMD_sRemark","","readonly"]
        ];
        return $list;
    }

    //编辑table
    public function getDetailField()
    {
        $list = [
            ["单据号","TRNM_Num","hidden","readonly"],
            ["TD_ID","TD_ID","hidden","readonly"],
            ["段名","DtS_Name","","readonly"],
            ["部件号","TRN_TPNum","","readonly"],
            ["备注","DtMD_sRemark","","readonly"],
            ["材料","DtMD_sStuff","",""],
            ["材质","DtMD_sMaterial","","readonly"],
            ["规格","DtMD_sSpecification","",""],
            ["长度(mm)","DtMD_iLength","","readonly"],
            ["宽度(mm)","DtMD_fWidth","","readonly"],
            ["厚度(mm)","DtMD_iTorch","","readonly"],
            ["是否电焊","DtMD_iWelding","","readonly"],
            ["类型","type","","readonly"],
            ["数量","TRN_TPNumber","",""],
            ["单基数量","DtMD_iUnitCount","","readonly"],
            ["单件重量(kg)","TRN_TPWeight","","readonly"],
            ["总重","sum_weight","hidden","readonly"],
            ["单件孔数","DtMD_iUnitHoleCount","","readonly"],
            ["任务单","T_Num","","readonly"],
            ["塔型","TD_TypeName","","readonly"],
            ["电压等级","DtM_sPressure","","readonly"],
            ["制弯","DtMD_iFireBending","","readonly"],
            ["开合角","DtMD_KaiHeJiao","","readonly"],
            ["压扁","DtMD_DaBian","","readonly"],
            ["切角","DtMD_iCuttingAngle","","readonly"],
            ["铲背","DtMD_fBackOff","","readonly"],
            ["清根","DtMD_iBackGouging","","readonly"],
            ["割豁","DtMD_GeHuo","","readonly"],
        ];
        return $list;
    }

    /**
     * 打印
     */
    public function print($ids = null)
    {
        $thisOne = $this->model->where("TRNM_Num",$ids)->find();
        if (!$thisOne) {
            $this->error(__('No Results were found'));
        }
        $row = [
            "TRNM_WriteDate" => $thisOne["TRNM_WriteDate"],
            "TRNM_Memo" => $thisOne["TRNM_Memo"],
            "ty_tower" => $thisOne["ty_tower"],
            "TRNM_Sort" => $thisOne["TRNM_Sort"],
            "TRNM_Writer" => $thisOne["TRNM_Writer"],
            "Approver" => $thisOne["Approver"]
        ];
        $list = $this->model->alias("trnm")
            ->join(["taskdetail"=>"td"],"trnm.TD_ID = td.TD_ID")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->where("trnm.newBnum",$thisOne["newBnum"])
            // ->where("trnm.T_Num",$thisOne["T_Num"])
            ->field("trnm.*,td.TD_TypeName,td.T_Num,t.t_project,trnm.TD_ID,c.PC_Num,'' as old_TypeName, c.Customer_Name,trnm.PT_Num,ty_tower")
            ->order("TRNM_Num")
            ->select();
        $TRNM_Num_list = $dt_detail_list = [];
        foreach($list as $k=>$v){
            $TRNM_Num_list[$v["TRNM_Num"]] = $v->toArray();
        }
        $dt_list = $this->model->alias("trnm")
            ->join([$this->technology_ex."taskrenew"=>"trn"],"trnm.TRNM_Num=trn.TRNM_Num")
            ->join([$this->technology_ex."dtmaterial"=>"dtm"],"trn.TD_ID=dtm.TD_ID")
            ->join([$this->technology_ex."dtsect"=>"dts"],["dtm.DtM_iID_PK = dts.DtM_iID_FK","trn.DtS_Name = dts.DtS_Name"])
            ->join([$this->technology_ex."dtmaterialdetial"=>"dtmd"],["dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK","dtmd.DtMD_sPartsID = trn.TRN_TPNum"])
            ->field("trn.TD_ID,trn.TRNM_Num,trn.TRN_ID,trn.DtS_Name,CAST(trn.DtS_Name AS UNSIGNED) AS number_1,trn.TRN_Project,trn.TRN_TPNum,CAST(trn.TRN_TPNum AS UNSIGNED) AS number_2,trn.TRN_TPNumber,dtmd.DtMD_sStuff,dtmd.DtMD_sMaterial,dtmd.DtMD_sSpecification,dtmd.DtMD_iUnitCount,dtmd.DtMD_iLength,dtmd.DtMD_fWidth,dtmd.DtMD_iUnitHoleCount,dtmd.DtMD_fUnitWeight as TRN_TPWeight,round(trn.TRN_TPNumber*dtmd.DtMD_fUnitWeight,2) as sum_weight,dtmd.DtMD_sRemark,
            dtmd.DtMD_iWelding,dtmd.DtMD_iFireBending, dtmd.DtMD_GeHuo,dtmd.DtMD_iCuttingAngle,dtmd.DtMD_fBackOff,dtmd.DtMD_iBackGouging,dtmd.DtMD_DaBian,dtmd.DtMD_KaiHeJiao,dtmd.DtMD_ZuanKong")
            ->where("trn.TRNM_Num","in",array_keys($TRNM_Num_list))
            ->order("trn.TRN_Project,trn.TRNM_Num,number_1,number_2")
            ->select();
        foreach($dt_list as $k=>$v){
            $dt_detail_list[$k] = $v->toArray();
            $dt_detail_list[$k]["Customer_Name"] = $TRNM_Num_list[$v["TRNM_Num"]]["Customer_Name"];
            $dt_detail_list[$k]["PC_Num"] = $TRNM_Num_list[$v["TRNM_Num"]]["PC_Num"];
            $dt_detail_list[$k]["t_project"] = $TRNM_Num_list[$v["TRNM_Num"]]["t_project"];
            $dt_detail_list[$k]["TRNM_Memo"] = $TRNM_Num_list[$v["TRNM_Num"]]["TRNM_Memo"];
            $dt_detail_list[$k]["TRNM_WriteDate"] = date("Y-m-d",strtotime($TRNM_Num_list[$v["TRNM_Num"]]["TRNM_WriteDate"]));
            $dt_detail_list[$k]["TD_TypeName"] = $TRNM_Num_list[$v["TRNM_Num"]]["TD_TypeName"].";下达单号:".$TRNM_Num_list[$v["TRNM_Num"]]["PT_Num"].";套用塔型:".$TRNM_Num_list[$v["TRNM_Num"]]["ty_tower"];
        }
        $row['TRNM_WriteDate'] = date("Y-m-d", strtotime($row["TRNM_WriteDate"]));
        $this->assignconfig("detail_list",$dt_detail_list);
        $this->assignconfig("row",$row);
        
        return $this->view->fetch();
    }

    /**
     * a5打印
     */
    public function a5print($ids = null)
    {
        $thisOne = $this->model->where("TRNM_Num",$ids)->find();
        if (!$thisOne) {
            $this->error(__('No Results were found'));
        }
        $list = $this->model->alias("trnm")
            ->join(["taskdetail"=>"td"],"trnm.TD_ID = td.TD_ID")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->where("trnm.newBnum",$thisOne["newBnum"])
            ->field("trnm.*,td.TD_TypeName,td.T_Num,t.t_project,trnm.TD_ID,c.PC_Num,'' as old_TypeName, c.Customer_Name,trnm.PT_Num,ty_tower")
            ->order("TRNM_Num")
            ->select();
        $TRNM_Num_list = $dt_detail_list = [];
        foreach($list as $k=>$v){
            $TRNM_Num_list[$v["TRNM_Num"]] = $v->toArray();
            if($v["TRNM_Num"]==$ids){
                $row = [
                    "TRNM_Num" => $v["TRNM_Num"],
                    "Customer_Name" => $v["Customer_Name"],
                    "t_project" => $v["t_project"],
                    "PT_Num" => $v["PT_Num"],
                    "T_Num" => $v["T_Num"],
                    "TRNM_WriteDate" => $v["TRNM_WriteDate"],
                    "TRNM_Memo" => $v["TRNM_Memo"],
                    "TRNM_Sort" => $v["TRNM_Sort"],
                    "TRNM_Writer" => $v["TRNM_Writer"],
                    "Approver" => $v["Approver"]
                ];
            }
        }
        $dt_list = $this->model->alias("trnm")
            ->join([$this->technology_ex."taskrenew"=>"trn"],"trnm.TRNM_Num=trn.TRNM_Num")
            ->join([$this->technology_ex."dtmaterial"=>"dtm"],"trn.TD_ID=dtm.TD_ID")
            ->join([$this->technology_ex."dtsect"=>"dts"],["dtm.DtM_iID_PK = dts.DtM_iID_FK","trn.DtS_Name = dts.DtS_Name"])
            ->join([$this->technology_ex."dtmaterialdetial"=>"dtmd"],["dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK","dtmd.DtMD_sPartsID = trn.TRN_TPNum"])
            ->field("trn.TD_ID,trn.TRNM_Num,trn.TRN_ID,trn.DtS_Name,
            CAST(trn.DtS_Name AS UNSIGNED) AS number_1,trn.TRN_Project,trn.TRN_TPNum,
            CAST(trn.TRN_TPNum AS UNSIGNED) AS number_2,trn.TRN_TPNumber,
            dtmd.DtMD_sStuff,dtmd.DtMD_sMaterial,dtmd.DtMD_sSpecification,dtmd.DtMD_iUnitCount,dtmd.DtMD_iLength,dtmd.DtMD_fWidth,dtmd.DtMD_iUnitHoleCount,dtmd.DtMD_fUnitWeight as TRN_TPWeight,round(dtmd.DtMD_iUnitCount*dtmd.DtMD_fUnitWeight,2) as sum_weight,dtmd.DtMD_sRemark,
            dtmd.DtMD_GeHuo,dtmd.DtMD_ZuanKong,dtmd.DtMD_iWelding,dtmd.DtMD_iFireBending,dtmd.DtMD_KaiHeJiao,dtmd.DtMD_DaBian,dtmd.DtMD_iCuttingAngle,dtmd.DtMD_fBackOff,dtmd.DtMD_iBackGouging")
            ->where("trn.TRNM_Num","in",array_keys($TRNM_Num_list))
            ->order("trn.TRN_Project,trn.TRNM_Num,number_1,number_2")
            ->select();
        foreach($dt_list as $k=>$v){
            $dt_detail_list[$k] = $v->toArray();
            $dt_detail_list[$k]["ty_tower"] = @$TRNM_Num_list[$v["TRNM_Num"]]["ty_tower"];
            $dt_detail_list[$k]["PT_Num"] = @$TRNM_Num_list[$v["TRNM_Num"]]["PT_Num"];
            $dt_detail_list[$k]["t_project"] = @$TRNM_Num_list[$v["TRNM_Num"]]["t_project"];
            $dt_detail_list[$k]["TD_TypeName"] = @$TRNM_Num_list[$v["TRNM_Num"]]["TD_TypeName"];
        }
        
        $row['TRNM_WriteDate'] = date("Y-m-d", strtotime($row["TRNM_WriteDate"]));
        $this->assignconfig("detail_list",$dt_detail_list);
        $this->assignconfig("row",$row);
        
        return $this->view->fetch();
    }
}
