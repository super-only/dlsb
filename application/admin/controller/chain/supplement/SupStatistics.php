<?php

namespace app\admin\controller\chain\supplement;

use app\admin\controller\Technology;
/**
 * 补件申请单
 *
 * @icon fa fa-circle-o
 */
class SupStatistics extends Technology
{
    
    /**
     * SupAppForm模型对象
     * @var \app\admin\model\chain\supplement\SupAppForm
     */
    protected $model = null,$detailModel=null,$bjtype=null,$admin=null;

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->model = new \app\admin\model\chain\supplement\TaskReNewMain([],$this->technology_ex);
        $this->detailModel = new \app\admin\model\chain\supplement\TaskReNew([],$this->technology_ex);
        $this->bjtype = ["工地补件"=>"工地补件","厂内补件"=>"厂内补件"];
        $this->assignconfig("bjtype",$this->bjtype);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $where = [];
            foreach($filter as $k=>$v){
                if($k=="TRNM_WriteDate"){
                    list($start_time,$end_time) = explode(" - ",$v);
                    $where[$k] = ["between time",[$start_time,$end_time]];
                }else $where[$k] = ["LIKE","%".$v."%"];
            }
            $count = $weight = 0;
            $row = [];
            $list = $this->model->alias("trnm")
                ->join([$this->technology_ex."taskrenew"=>"trn"],"trnm.TRNM_Num = trn.TRNM_Num")
                ->join(["taskdetail"=>"td"],"trnm.TD_ID = td.TD_ID","LEFT")
                ->join(["task"=>"t"],"td.T_Num = t.T_Num","LEFT")
                ->join(["compact"=>"c"],"t.C_Num = c.C_Num","LEFT")
                ->field("trnm.TRNM_WriteDate,t.C_Num,c.Customer_Name,t.t_project,td.TD_TypeName,trnm.TRNM_Sort,sum(trn.TRN_TPNumber) as count,sum(trn.TRN_TPNumber*trn.TRN_TPWeight) as weight,trnm.TRNM_Num")
                ->where($where)
                ->order("TRNM_WriteDate DESC")
                ->group("trnm.TRNM_Num")
                ->select();
            foreach($list as $k=>$v){
                $row[] = $v->toArray();
                $count += $v["count"];
                $weight += $v["weight"];
            }
            $row[] = ["t_project"=>"合计","count"=>$count,"weight"=>round($weight,2)];
            
            $result = array("total" => count($list), "rows" => $row);

            return json($result);
        }
        $defaultTime = date("Y-m-1 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        return $this->view->fetch();
    }
    
    public function detail($num = null)
    {
        $thisOne = $this->model->alias("trnm")
        ->join([$this->technology_ex."taskrenew"=>"trn"],"trnm.TRNM_Num = trn.TRNM_Num")
        ->join(["taskdetail"=>"td"],"trnm.TD_ID = td.TD_ID","LEFT")
        ->join(["task"=>"t"],"td.T_Num = t.T_Num","LEFT")
        ->join(["compact"=>"c"],"t.C_Num = c.C_Num","LEFT")
        ->field("trnm.TRNM_WriteDate,t.C_Num,c.Customer_Name,t.t_project,td.TD_TypeName,trnm.TRNM_Sort,sum(trn.TRN_TPNumber) as count,sum(trn.TRN_TPNumber*trn.TRN_TPWeight) as weight,trnm.TRNM_Num")
        ->where("trnm.TRNM_Num",$num)
        ->order("TRNM_WriteDate DESC")
        ->group("trnm.TRNM_Num")
        ->find();
        if (!$thisOne) {
            $this->error(__('No Results were found'));
        }
        $dt_list = $this->detailModel->alias("trn")
            ->join([$this->technology_ex."dtmaterial"=>"dtm"],"trn.TD_ID=dtm.TD_ID")
            ->join([$this->technology_ex."dtsect"=>"dts"],["dtm.DtM_iID_PK = dts.DtM_iID_FK","trn.DtS_Name = dts.DtS_Name"])
            ->join([$this->technology_ex."dtmaterialdetial"=>"dtmd"],["dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK","dtmd.DtMD_sPartsID = trn.TRN_TPNum"])
            ->field("trn.TD_ID,trn.TRNM_Num,trn.TRN_ID,trn.DtS_Name,CAST(trn.DtS_Name AS UNSIGNED) AS number_1,trn.TRN_Project,trn.TRN_TPNum,CAST(trn.TRN_TPNum AS UNSIGNED) AS number_2,trn.TRN_TPNumber,dtmd.DtMD_sStuff,dtmd.DtMD_sMaterial,dtmd.DtMD_sSpecification,dtmd.DtMD_iUnitCount,dtmd.DtMD_iLength,dtmd.DtMD_fWidth,dtmd.DtMD_iTorch,dtmd.DtMD_iUnitHoleCount,dtmd.DtMD_fUnitWeight as TRN_TPWeight,round(trn.TRN_TPNumber*dtmd.DtMD_fUnitWeight,2) as sum_weight,dtmd.DtMD_sRemark,dtmd.type,'".$thisOne["TD_TypeName"]."' as TD_TypeName")
            ->where("trn.TRNM_Num",$num)
            ->order("trn.TD_ID,trn.TRNM_Num,number_1,number_2")
            ->select();
        $dt_list = $dt_list?collection($dt_list)->toArray():[];
        $this->view->assign("row",$thisOne);
        $this->assignconfig("list",$dt_list);
        return $this->view->fetch();
    }
}
