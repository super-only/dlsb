<?php

namespace app\admin\controller\chain\material;

use app\admin\model\chain\material\CommisionWarranty;
use app\admin\model\chain\material\MaterialGetNotice;
use app\admin\model\chain\material\MaterialNote;
use app\admin\model\chain\material\StoreInDetail;
use app\admin\model\jichu\wl\Vendor;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;
/**
 * 材料理化申请
 *
 * @icon fa fa-circle-o
 */
class CommisionTest extends Backend
{
    
    /**
     * CommisionTest模型对象
     * @var \app\admin\model\chain\material\CommisionTest
     */
    protected $model = null;
    // protected $noNeedLogin = ["selectCopy",""];
    protected $noNeedLogin = ["detailImport"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\CommisionTest;
        $this->detailModel = new \app\admin\model\chain\material\CommisionTestDetail;
        $this->warrantyModel = new \app\admin\model\chain\material\CommisionWarranty;
        $this->admin = \think\Session::get('admin');
        $this->assignconfig("time",date("Y-m-d H:i:s"));
        $inventoryArr = $commisiontestArr = [""=>"[全部]"];
        $inventorList = db()->query("select IM_Class from inventorymaterial where IM_Class=IM_Spec group by IM_Class order by IM_Num asc");
        foreach($inventorList as $v){
            $inventoryArr[$v["IM_Class"]] = $v["IM_Class"];
        }
        $commisiontestList = db()->query("select CT_Standard from commisionteststandard where valid=1 and ifCommision=1 order by ct_default ASC,id asc");
        foreach($commisiontestList as $v){
            $commisiontestArr[$v["CT_Standard"]] = $v["CT_Standard"];
        }
        $this->inventoryArr = $inventoryArr;
        $this->commisiontestArr = $commisiontestArr;
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $ini_where = [];
            if(isset($filter["is_check"])){
                if($filter["is_check"]=="1") $ini_where["c.Auditor"] = ["=",""];
                if($filter["is_check"]=="2") $ini_where["c.Auditor"] = ["<>",""];
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("c")
                ->join(["vendor"=>"v"],"c.V_Num = v.V_Num")
                ->field("c.*,v.V_Name,(case when c.Auditor='' then '未审核' else '已审核' end ) as is_check")
                ->where($where)
                ->where($ini_where)
                ->order($sort, $order)
                ->order("c.CT_Num desc")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function requisitionDetail()
    {
        $ids = $this->request->post("ids");
        if(!$ids) return json(["code"=>0,"msg"=>"有误"]);
        $list = $this->detailModel->alias("ctd")
            ->join(["materialgetnotice"=>"mgn"],"ctd.MGN_ID = mgn.MGN_ID")
            ->join(["inventorymaterial"=>"im"],"mgn.IM_Num = im.IM_Num")
            ->field("ctd.CTD_Pi,ctd.CTD_testnum,mgn.MN_Num,im.IM_Class,ctd.CTD_Spec,ctd.L_Name,round(mgn.MGN_Length/1000,3) as MGN_Length,round(mgn.MGN_Width/1000,3) as MGN_Width,ctd.CTD_FactCount,ctd.CTD_Weight,ctd.CTD_ShapeSizeRes,ctd.CTD_ShowQualityRes,mgn.MGN_Maker,ctd.LuPiHao,ctd.PiHao,ctd.JiaoGangNum,ctd.CTD_Project,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("ctd.CT_Num",$ids)
            ->order("clsort,ctd.L_Name,bh,MGN_Length ASC")
            ->select();
        $sum_weight = $count = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["MGN_Length"] = round($v['MGN_Length'],3);
            $rows[$k]["MGN_Width"] = round($v['MGN_Width'],3);
            $rows[$k]["CTD_FactCount"] = round($v['CTD_FactCount'],2);
            $rows[$k]["CTD_Weight"] = round($v['CTD_Weight'],2);
            $sum_weight += $v["CTD_Weight"];
            $count += $v["CTD_FactCount"];
        }
        $rows[] = ["CTD_Pi"=>"合计","CTD_Weight"=>round($sum_weight,2),"CTD_FactCount"=>$count];
        return json(["code"=>1,"data"=>$rows]);
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $CT_Num = $params["CT_Num"];
                if($CT_Num){
                    $one = $this->model->where("CT_Num",$CT_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("ym");
                    $one = $this->model->field("CT_Num")->where("CT_Num","LIKE","SJ".$month.'-%')->order("CT_Num DESC")->find();
                    if($one){
                        $num = substr($one["CT_Num"],7);
                        $CT_Num = 'SJ'.$month.'-'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $CT_Num = 'SJ'.$month.'-001';
                }
                $params["CT_Num"] = $CT_Num;
                $sectSaveList = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk]["CT_Num"] = $params["CT_Num"];
                        if($k=="CTD_ID" and $vv==0) continue;
                        else if($vv=="") continue;
                        else if($k=="MGN_Length" or $k=="MGN_Width") $sectSaveList[$kk][$k] = $vv*1000;
                        else $sectSaveList[$kk][$k] = $vv;
                        
                    }
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model::create($params);
                    $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$result["CT_Num"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];

        

        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("inventoryArr",$this->inventoryArr);
        $this->view->assign("commisiontestArr",$this->commisiontestArr);
        $this->assignconfig('tableField',$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        $vendor_list = (new Vendor())->field("V_Name")->where("V_Num",$row["V_Num"])->find();
        $row["V_Name"] = @$vendor_list["V_Name"];
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            
            if ($params and $paramsTable) {
                $si_one = $this->detailModel->alias("d")->join(["storeindetail"=>"sid"],"sid.MGN_ID = d.CTD_ID")->where("d.CT_Num",$ids)->find();
                if($si_one) $this->error("已存在入库，不能进行修改！");

                $params = $this->preExcludeFields($params);
                $CT_Num = $row["CT_Num"];
                $sectSaveList = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk]["CT_Num"] = $CT_Num;
                        if($k=="CTD_ID" and $vv==0) continue;
                        else if($vv=="") continue;
                        else if($k=="MGN_Length" or $k=="MGN_Width") $sectSaveList[$kk][$k] = $vv*1000;
                        // else if($vv=="CTD_Weight") $sectSaveList[$kk]["CTD_GuoBanWeight"] = $vv;
                        else $sectSaveList[$kk][$k] = $vv;
                        
                    }
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("CT_Num",$ids)->update($params);
                    if($row["Auditor"]=="") $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $this->detailModel->alias("ctd")
            ->join(["materialgetnotice"=>"mgn"],"mgn.MGN_ID = ctd.MGN_ID")
            ->join(["materialnote"=>"mn"],"mn.MN_Num = mgn.MN_Num")
            ->join(["inventorymaterial"=>"im"],"mgn.IM_Num = im.IM_Num")
            ->field("ctd.CTD_ID,ctd.MGN_ID,ctd.CTD_testnum,im.IM_Class,ctd.CTD_Spec,ctd.L_Name,round(mgn.MGN_Length/1000,3) as MGN_Length,round(mgn.MGN_Width/1000,3) as MGN_Width,ctd.CTD_FactCount,im.IM_PerWeight,round(mgn.MGN_Weight/mgn.MGN_Count,2) as dz,ctd.CTD_Weight,ctd.CTD_GuoBanWeight,mgn.MGN_Maker,ctd.LuPiHao,ctd.PiHao,ctd.JiaoGangNum,ctd.CTD_QualityNo,ctd.CTD_QualityNum,ctd.QualityRequestion,ctd.CTD_QualityJS,ctd.CTD_Pi,ctd.CTD_UnqualityCount,ctd.CTD_UnqualityWeight,ctd.CTD_ShapeSize,ctd.CTD_ShapeSizeRes,ctd.CTD_ShowQuality,ctd.CTD_ShowQualityRes,ctd.CTD_Count,ctd.CTD_Project,ctd.CTD_Result,ctd.CTD_Memo,ctd.CTD_Dept,mn.MN_Date,ctd.CTD_QYRen,ctd.CTD_QYDate,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(ctd.CTD_Spec,'∠',''),1,locate('*',REPLACE(ctd.CTD_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(ctd.CTD_Spec,'∠',''),locate('*',REPLACE(ctd.CTD_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(ctd.CTD_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("ctd.CT_Num",$row["CT_Num"])
            ->order("clsort,ctd.L_Name,bh,MGN_Length ASC")
            ->select();
        $sum_weight = $count = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["MGN_Length"] = round($v['MGN_Length'],3);
            $rows[$k]["MGN_Width"] = round($v['MGN_Width'],3);
            $rows[$k]["CTD_FactCount"] = round($v['CTD_FactCount'],2);
            $rows[$k]["CTD_Weight"] = round($v['CTD_Weight'],2);
            $sum_weight += $v["CTD_Weight"];
            $count += $v["CTD_FactCount"];
        }
        $row["sum_weight"] = $sum_weight;
        $row["count"] = $count;
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("list",$rows);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("inventoryArr",$this->inventoryArr);
        $this->view->assign("commisiontestArr",$this->commisiontestArr);
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function warranty($ids=null)
    {
        $row = $this->detailModel->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if(!$row['CTD_QualityNo']) $this->error("请填写质保书号并保存当前状态");
        if(!$row["LuPiHao"] and !$row["PiHao"]) $this->error("请务必填写炉号或者批号并保存当前状态");
        $ini_row = $this->warrantyModel->get($ids);
        $warranty_row = [];
        if(!$ini_row){
            $column_list = DB::query("select COLUMN_NAME as name from information_schema.COLUMNS where table_name = 'commision_warranty'");
            foreach($column_list as $k=>$v){
                $warranty_row[$v["name"]] = "";
            }
            $warranty_row["CTD_ID"] = $ids;
            $warranty_row["writer"] = $this->admin["nickname"];
            $warranty_row["writer_time"] = date("Y-m-d");
            $warranty_row["auditor_time"] = "";
        }else $warranty_row = $ini_row;
        $warranty_row["CTD_QualityNo"] = $row["CTD_QualityNo"];
        $warranty_row["LuPiHao"] = $row["LuPiHao"];
        $warranty_row["PiHao"] = $row["PiHao"];
        if ($this->request->isPost()) {
            if($warranty_row["auditor"]) $this->error("已审核，无法修改内容");
            $params = $this->request->post("row/a");
            if (empty($params)) {
                $this->error(__('Parameter %s can not be empty', ''));
            }
            $params = $this->preExcludeFields($params);
            $params["CTD_ID"] = $ids;
            if($ini_row) $result = $this->warrantyModel->allowField(true)->update($params,["CTD_ID",$ids]);
            else $result = $this->warrantyModel->allowField(true)->save($params);
            if($result) $this->success("保存成功");
            else $this->error("失败，请重试");
        }
        $this->assignconfig("field",$this->_getField());
        $this->view->assign("row",$warranty_row);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function export($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $title = $row["CT_Num"];

        $list = $this->detailModel->alias("ctd")
            ->join(["materialgetnotice"=>"mgn"],"mgn.MGN_ID = ctd.MGN_ID")
            ->join(["materialnote"=>"mn"],"mn.MN_Num = mgn.MN_Num")
            ->join(["inventorymaterial"=>"im"],"mgn.IM_Num = im.IM_Num")
            ->field("ctd.CTD_ID,ctd.MGN_ID,ctd.CTD_testnum,im.IM_Class,ctd.CTD_Spec,ctd.L_Name,round(mgn.MGN_Length/1000,3) as MGN_Length,round(mgn.MGN_Width/1000,3) as MGN_Width,ctd.CTD_FactCount,im.IM_PerWeight,round(mgn.MGN_Weight/mgn.MGN_Count,2) as dz,ctd.CTD_Weight,ctd.CTD_GuoBanWeight,mgn.MGN_Maker,ctd.LuPiHao,ctd.PiHao,ctd.JiaoGangNum,ctd.CTD_QualityNo,ctd.CTD_QualityNum,ctd.QualityRequestion,ctd.CTD_QualityJS,ctd.CTD_Pi,ctd.CTD_UnqualityCount,ctd.CTD_UnqualityWeight,ctd.CTD_ShapeSize,ctd.CTD_ShapeSizeRes,ctd.CTD_ShowQuality,ctd.CTD_ShowQualityRes,ctd.CTD_Count,ctd.CTD_Project,ctd.CTD_Result,ctd.CTD_Memo,ctd.CTD_Dept,mn.MN_Date,ctd.CTD_QYRen,ctd.CTD_QYDate,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(ctd.CTD_Spec,'∠',''),1,locate('*',REPLACE(ctd.CTD_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(ctd.CTD_Spec,'∠',''),locate('*',REPLACE(ctd.CTD_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(ctd.CTD_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("ctd.CT_Num",$row["CT_Num"])
            ->order("clsort,ctd.L_Name,bh,MGN_Length ASC")
            ->select();
        foreach($list as $k=>$v){
            $list[$k]["ID"] = $k+1;
            $list[$k]["MGN_Length"] = round($v["MGN_Length"],3);
            $list[$k]["MGN_Width"] = round($v["MGN_Width"],3);
            $list[$k]["CTD_FactCount"] = round($v['CTD_FactCount'],2);
            $list[$k]["CTD_Weight"] = round($v['CTD_Weight'],2);
        }
        $tableField = $this->getTableField();
        $header = [['ID', 'ID']];
        foreach($tableField as $k=>$v){
            if($v[5]==""){
                $header[] = [$v[0],$v[1]];
            }
        }
        return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
    
    }

    public function chooseDetail($supplier = "")
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $subQuery = (new MaterialGetNotice())->alias("mgn")
                ->field("mgn.mgn_testnum,mgn.IM_Num,mgn.MGN_ID,mgn.MN_Num,mgn.MGN_Destination,mgn.L_Name,mgn.MGN_Length,mgn.MGN_Width,mgn.MGN_Count,mgn.MGN_Weight,mgn.MGN_Weight,mgn.MGN_Maker,mgn.mgn_pi as CTD_Pi")
                ->where("not exists ( select 'x' from commisiontestdetail ctd where ctd.MGN_ID = mgn.MGN_ID)")
                ->buildSql();
            $list = (new MaterialNote())->alias("mn")
                ->join([$subQuery=>"mgn"],"mgn.MN_Num = mn.MN_Num")
                ->join(["vendor"=>"v"],"v.V_Num = mn.V_Num")
                ->join(["inventorymaterial"=>"im"],"im.IM_Num = mgn.IM_Num")
                ->field("mgn.mgn_testnum as CTD_testnum,mgn.IM_Num,mgn.MGN_ID,mgn.MN_Num,v.V_Num,v.V_Name,mn.MN_Date,mgn.MGN_Destination,mgn.L_Name,mgn.MGN_Length,mgn.MGN_Width,mgn.MGN_Count as CTD_FactCount,round(mgn.MGN_Weight,2) as CTD_Weight,mgn.MGN_Weight as CTD_GuoBanWeight,mn.MN_Date,mgn.MGN_Maker,im.IM_Class,im.IM_Spec as CTD_Spec,im.IM_PerWeight,mgn.CTD_Pi,round(mgn.MGN_Weight/mgn.MGN_Count,2) as dz")
                ->where($where)->where("mn.Auditor","<>","")->where("ifIMcommision",0)->where("mn.WriteTime",">=",date("Y-m-d", strtotime(' -2 month')));
            if($supplier!='') $list = $list->where("mn.V_Num",$supplier);
            $list = $list->order($sort, $order)
                ->order("im.IM_Spec asc")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->assignconfig("supplier",$supplier);
        return $this->view->fetch();

    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            $ct_one = $this->model->alias("m")->join(["commisiontestdetail"=>"ctd"],"ctd.CT_Num = m.CT_Num")->where("ctd.CTD_ID",$num)->find();
            if($ct_one["Auditor"]) return json(["code"=>0,'msg'=>"已审核，不能进行删除！"]);
            $one = (new StoreInDetail())->where("MGN_ID",$num)->find();
            if($one) return json(["code"=>0,'msg'=>"已存在入库，不能进行删除！"]);
            Db::startTrans();
            try {
                $this->detailModel->where("CTD_ID",$num)->delete();
                (new CommisionWarranty())->where("CTD_ID",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $one = $this->model->where("CT_Num",$ids)->find();
            if($one["Auditor"]) $this->error("已审核，不能进行删除！");
            $si_one = $this->detailModel->alias("d")->join(["storeindetail"=>"sid"],"sid.MGN_ID = d.CTD_ID")->where("d.CT_Num",$ids)->find();
            if($si_one) $this->error("已存在入库，不能进行删除！");

            // $list = $this->model->where($pk, 'in', $ids)->select();
            $count = false;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->delete();
                $com_list = $this->detailModel->where($pk,'in',$ids)->column("CTD_ID");
                $count += (new CommisionWarranty())
                    ->where("CTD_ID","IN",$com_list)
                    ->delete();
                $count += $this->detailModel->where($pk,"IN",$ids)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function detailImport($qn='')
    {
        $file = "";
        $file_url = ROOT_PATH . DS . 'public' . DS . 'zlfiles' . DS . 'cczlzms' . DS . $qn .".pdf";
        if(is_file($file_url)) $file = DS . 'zlfiles' . DS . 'cczlzms' . DS . $qn .".pdf";
        $row = [
            "qn" => $qn,
            "file" => $file
        ];
        $this->view->assign("row",$row);
        if ($this->request->isPost()) {
            $postRow = $this->request->post();
            $filename = $postRow["row"]["file"];
            $newfieldname = ROOT_PATH . 'public' . DS . 'zlfiles' . DS . 'cczlzms' . DS . $qn .".pdf";
            if(file_exists($newfieldname)) unlink($newfieldname);
            $result = true;
            if($filename){
                $oldfieldname = ROOT_PATH . 'public' . $filename;
                if($oldfieldname == $file_url) $this->success();
                $result = rename($oldfieldname,$newfieldname);
            }
            // $ncData[$DtMD_sPartsID] = [
            //     "TD_ID" => $td_id,
            //     "PartName" => $DtMD_sPartsID,
            //     "PartNCPath" => $td_id.'-'.$DtMD_sPartsID,
            //     "datStream" => DS.'ncfiles' .DS.$td_id.DS.$DtMD_sPartsID.$wz
            // ];
            // if($PcNcFile) $ncData[$DtMD_sPartsID]["ID"] = $PcNcFile["ID"];
            // $result = $PcNcFile->allowField(true)->saveAll($ncData);
            if ($result) {
                // unlink($oldfieldname);
                $this->success();
            } else {
                $this->error("失败");
            }
        }
        return $this->view->fetch();
    }
    

    //编辑table
    public function getTableField()
    {
        $list = [
            ["CTD_ID","CTD_ID","text","readonly","","hidden"],
            ["MGN_ID","MGN_ID","text","readonly","","hidden"],
            ["试验编号","CTD_testnum","text","","",""],
            ["材料名称","IM_Class","text","readonly","",""],
            ["规格","CTD_Spec","text","readonly","",""],
            ["材质","L_Name","text","readonly","",""],
            ["长度(m)","MGN_Length","text","readonly",0,""],
            ["宽度(m)","MGN_Width","text","readonly",0,""],
            ["到货数量","CTD_FactCount","text","",0,""],
            // ["比重","IM_PerWeight","text","readonly",0,"hidden"],
            ["单重","dz","text","readonly",0,"hidden"],
            ["重量","CTD_Weight","text","",0,""],
            ["重量","CTD_GuoBanWeight","text","",0,"hidden"],
            ["生产厂家","MGN_Maker","text","readonly","",""],
            ["炉号","LuPiHao","text","","",""],
            ["批号","PiHao","text","","",""],
            ["角钢编号","JiaoGangNum","text","","",""],
            ["质保书号","CTD_QualityNo","text","","",""],
            ["上传","CTD_QualityNum","text","","",""],
            ["质保书缺陷原因","QualityRequestion","text","","",""],
            ["产品执行标准","CTD_QualityJS","text","","",""],
            ["进货批次","CTD_Pi","text","","",""],
            ["不合格数","CTD_UnqualityCount","text","","",""],
            ["不合格重量","CTD_UnqualityWeight","text","","",""],
            ["外形尺寸","CTD_ShapeSize","text","","",""],
            ["外形尺寸结论","CTD_ShapeSizeRes","select","","",""],
            ["外观质量","CTD_ShowQuality","text","","",""],
            ["外观质量结论","CTD_ShowQualityRes","select","","",""],
            ["试验数量","CTD_Count","text","",1,""],
            ["试验项目","CTD_Project","text","","",""],
            ["试验结果","CTD_Result","text","readonly","",""],
            ["说明","CTD_Memo","text","","",""],
            ["取样车间","CTD_Dept","text","","",""],
            ["到货日期","MN_Date","datetimerange","","",""],
            ["取样人","CTD_QYRen","text","","",""],
            ["确认时间","CTD_QYDate","datetimerange","","",""]
        ];
        return $list;
    }
    
    /**
     * 打印
     */
    public function printDetail($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $row = $row->toArray();
        $vendor_list = (new Vendor())->field("V_Name")->where("V_Num",$row["V_Num"])->find();
        $row["V_Name"] = @$vendor_list["V_Name"];
        
        $list = $this->detailModel->alias("ctd")
            ->join(["materialgetnotice"=>"mgn"],"mgn.MGN_ID = ctd.MGN_ID")
            ->join(["materialnote"=>"mn"],"mn.MN_Num = mgn.MN_Num")
            ->join(["inventorymaterial"=>"im"],"mgn.IM_Num = im.IM_Num")
            ->field("ctd.CTD_ID,ctd.MGN_ID,ctd.CTD_testnum,im.IM_Class,ctd.CTD_Spec,ctd.L_Name,round(mgn.MGN_Length/1000,2) as MGN_Length,round(mgn.MGN_Width/1000,2) as MGN_Width,ctd.CTD_FactCount,im.IM_PerWeight,round(mgn.MGN_Weight/mgn.MGN_Count,2) as dz,ctd.CTD_Weight,ctd.CTD_GuoBanWeight,mgn.MGN_Maker,ctd.LuPiHao,ctd.PiHao,ctd.JiaoGangNum,ctd.CTD_QualityNo,ctd.CTD_QualityNum,ctd.QualityRequestion,ctd.CTD_QualityJS,ctd.CTD_Pi,ctd.CTD_UnqualityCount,ctd.CTD_UnqualityWeight,ctd.CTD_ShapeSize,ctd.CTD_ShapeSizeRes,ctd.CTD_ShowQuality,ctd.CTD_ShowQualityRes,ctd.CTD_Count,ctd.CTD_Project,ctd.CTD_Result,ctd.CTD_Memo,ctd.CTD_Dept,mn.MN_Date,ctd.CTD_QYRen,ctd.CTD_QYDate")
            ->where("ctd.CT_Num",$row["CT_Num"])
            ->order("im.IM_Class,ctd.CTD_Spec ASC")
            ->select();

        foreach($list as $v){
            $v["MN_Date"] = date_format(date_create($v["MN_Date"]), "Y-m-d");
            $v["CT_Date"] = date_format(date_create($row["CT_Date"]), "Y-m-d");
        }
        
        $mainInfos = [];
        $mainInfos['CT_Num'] = $row['CT_Num'];
        $mainInfos['V_Name'] = $row['V_Name'];
        $mainInfos['CT_RequestPepo'] = $row['CT_RequestPepo'];
        // $mainInfos['CT_RequestPepo'] = $row['CT_RequestPepo'];
        // $mainInfos['CT_RequestPepo'] = $row['CT_RequestPepo'];

        $this->assignconfig('list',$list);
        $this->assignconfig('mainInfos',$mainInfos);
        return $this->view->fetch();
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $result = $this->model->where("CT_Num",$num)->update(['CT_Num' => $num,'Auditor'=>$admin,"AuditTime"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"审核成功","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $result = $this->model->where("CT_Num",$num)->update(['CT_Num' => $num,'Auditor'=>"","AuditTime"=>"0000-00-00 00:00:00"]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"弃审成功","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

    public function warrantyAud()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $result = $this->warrantyModel->where("CTD_ID",$num)->update(['auditor'=>$admin,"auditor_time"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"审核成功","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function warrantyGiv()
    {
        $num = $this->request->post("num");
        $result = $this->warrantyModel->where("CTD_ID",$num)->update(['auditor'=>"","auditor_time"=>"0000-00-00 00:00:00"]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"弃审成功","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

    public function selectCopy($ids = null)
    {
        $row = $this->detailModel->get($ids);
        $field = $this->_getField();
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->detailModel->alias("d")
                ->join(["commision_warranty"=>"cw"],"d.CTD_ID=cw.CTD_ID")
                ->join(["materialgetnotice"=>"mgn"],"d.MGN_ID = mgn.MGN_ID")
                ->field("CTD_QualityNo,LuPiHao,PiHao,CTD_testnum,CTD_Spec,d.L_Name,MGN_Length,MGN_Width,MGN_Maker,JiaoGangNum,cw.*,(CTD_attack_first+CTD_attack_second+CTD_attack_third)/3 as CTD_attack_average")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            foreach($list as &$v){
                foreach($field as $fv){
                    $v[$fv] = round($v[$fv],3);
                }
            }
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        $this->assignconfig("ids",$ids);
        $this->assignconfig("row",$row);
        return $this->view->fetch();
        
    }

    protected function _getField()
    {
        return ["CTD_C","CTD_Si","CTD_Mn","CTD_P","CTD_S","CTD_V","CTD_Nb","CTD_Ti","CTD_Rel","CTD_Rm","CTD_Percent","CTD_attack_temp","CTD_attack_first","CTD_attack_second","CTD_attack_third","CTD_attack_average"];
    }
}
