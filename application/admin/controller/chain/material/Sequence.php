<?php

namespace app\admin\controller\chain\material;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Sequence extends Backend
{
    
    /**
     * Sequence模型对象
     * @var \app\admin\model\chain\material\Sequence
     */
    protected $model = null;
    protected $noNeedLogin = ["*"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\Sequence;
        $this->machineArr = [
            "BL2020" => "BL2020",
            "ADM3635" => "ADM3635",
            "BL2532" => "BL2532",
            "APM0708" => "APM0708",
            "APM2020" => "APM2020",
            "BL1412C" => "BL1412C",
            "ADM3635" => "ADM3635",
            "APM1412" => "APM1412",
            "PP123" => "PP123",
            "PPR103" => "PPR103",
            "PD16C" => "PD16C"

        ];
        $this->assignconfig("machineArr",$this->machineArr);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

}
