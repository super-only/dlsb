<?php

namespace app\admin\controller\chain\material;

use app\admin\model\chain\lofting\ProduceTask;
use app\admin\model\chain\lofting\UnionProduceTaskView;
use app\admin\model\chain\material\StoreInDetail;
use app\admin\model\chain\material\StoreState;
use app\admin\model\chain\material\WorkReceive;
use app\admin\model\chain\material\WorkReceiveDetail;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 原材料出库清单
 *
 * @icon fa fa-circle-o
 */
class StoreOutDetail extends Backend
{
    
    /**
     * StoreOutDetail模型对象
     * @var \app\admin\model\chain\material\StoreOutDetail
     */
    protected $model = null;
    protected $noNeedLogin = ["upload","eipUpload","offerU","detailWorkIn","detailWork","chooseWork"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\StoreOutDetail;
        $this->firstModel = new \app\admin\model\chain\material\StoreOut;
        $this->admin = \think\Session::get('admin');
        $this->assignconfig("time",date("Y-m-d H:i:s"));
        $this->ck_type = [0=>"正常出库",1=>"退货出库",2=>"外售出库",3=>"基建领料",4=>"外委领料",5=>"五车间领料",6=>"车间错情",7=>"放样错情",8=>"内外补料",9=>"工装",10=>"外委镀锌",11=>"外加工出库"];
        $this->view->assign("ck_type",$this->ck_type);
        $this->assignconfig("ck_type",$this->ck_type);
        $this->wareArr = $this->wareclassList([],2);
        $this->view->assign("wareclassList",$this->wareclassList());
        $deptList = [""=>"请选择"];
        $deptArr = [];
        $deptModel = (new \app\admin\model\jichu\jg\Deptdetail())
            ->field("DD_Name,DD_Num")
            ->where(["Valid"=>1])
            ->order(["ParentNum"=>"ASC","DD_Num"=>"ASC"])
            ->select();
        foreach($deptModel as $v){
            $deptList[$v["DD_Name"]] = $v["DD_Name"];
            $deptArr[$v["DD_Name"]] = $v["DD_Num"];
        }
        $this->deptArr = $deptArr;
        $this->view->assign("deptList",$deptList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $ini_where = [];
            $ck_type = $this->ck_type;
            if(isset($filter["is_check"])){
                if($filter["is_check"]=="1") $ini_where["so.SO_Auditor"] = ["=",""];
                if($filter["is_check"]=="2") $ini_where["so.SO_Auditor"] = ["<>",""];
            }
            if(isset($filter["SO_Flag_use"])) $ini_where["so.SO_Flag"] = ["=",array_search($filter["SO_Flag_use"],$ck_type)];
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            //有问题
            $list = $this->firstModel->alias("so")->force("SO_ID")
                ->join(["storeoutdetail"=>"sod"],"so.SO_Num = sod.SO_Num","left")
                ->join(["storeindetail"=>"sid"],"sod.SID_ID = sid.SID_ID","LEFT")
                ->join(["inventorymaterial"=>"im"],"im.IM_Num = sod.IM_Num","left")
                ->join(["vendor"=>"v"],"v.V_Num = sid.V_Num","left")
                ->field("so.offer,im.IM_Spec,im.IM_Class,so.SO_ID,so.SO_Num,sod.SOD_ID,so.SO_TakeDate,sod.IM_Num,sod.L_Name,round(sod.SOD_Length/1000,3) as SOD_Length,so.SO_Writer,so.SO_WriteDate,so.SO_Auditor,so.SO_AudiDate,round(sod.SOD_Width/1000,3) as SOD_Width,sod.SOD_Weight,sod.SOD_Count,so.SO_Num,so.SO_ProjectName,so.SO_C_Name,so.SO_TowerType,so.PC_Num,so.SO_EType,so.T_Num,sod.PT_Num,sod.LuPiHao,sod.PiHao,sid.SID_CTD_Pi,so.SO_Memo,so.SO_Take,so.SO_WareHouse,so.SO_Flag,sod.SOD_Memo,so.Hand_OutNum,v.V_Name,so.C_Num,so.E_Name,sod.purpose,so.E_NameBao,sid.SID_testnum,(case when so.SO_Auditor='' then '未审核' else '已审核' end ) as is_check,'' as SO_Flag_use,sid.SID_TestResult")
                ->where($where)
                ->where($ini_where)
                ->order($sort,$order)
                ->order("SO_WriteDate desc")
                ->select();
                // ->paginate($limit);
            $row = [];
            $count = $weight = 0;
            foreach($list as $k=>$v){
                $v["SOD_Length"] = round($v["SOD_Length"],3);
                $v["SOD_Width"] = round($v["SOD_Width"],3);
                $v["SOD_Count"] = round($v["SOD_Count"],2);
                $v["SOD_Weight"] = round($v["SOD_Weight"],2);
                $row[$k] = $v->toArray();
                $row[$k]["SO_Flag"] = $this->ck_type[$v["SO_Flag"]];
                $count += $v["SOD_Count"];
                $weight += $v["SOD_Weight"];
            }
            $row[] = ["IM_Class"=>"合计","SOD_Count"=>round($count,2),"SOD_Weight"=>round($weight,2)];
            // $result = array("total" => $list->total(), "rows" => $list->items());
            $result = array("total" => count($list), "rows" => $row);
            return json($result);
        }
        $defaultTime = date("Y-m-d 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        return $this->view->fetch();
    }

    public function xddh()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new UnionProduceTaskView())->alias("p")
                ->join(["taskdetail"=>"d"],"p.TD_ID = D.TD_ID")
                ->join(["task"=>"t"],"t.T_Num=d.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->join(["workreceive"=>"wr"],"wr.PT_Num = p.PT_Num","left")
                ->field("p.*,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num',c.C_Num,c.C_Num as 'c.C_Num',c.PC_Num,c.PC_Num as 'c.PC_Num',d.TD_Pressure,ifnull(wr.WR_Num,0) as WR_Num")
                ->where($where)
                ->order("p.WriterDate desc,p.PT_Num desc")
                // ->select(false);pri($list,1);
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $SO_Num = $params["SO_Num"];
                if($SO_Num){
                    $one = $this->firstModel->where("SO_Num",$SO_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("Ymd");
                    $one = $this->firstModel->field("SO_Num")->where("SO_Num","LIKE","CK".$month.'-%')->order("SO_Num DESC")->find();
                    if($one){
                        $num = substr($one["SO_Num"],11);
                        $SO_Num = 'CK'.$month.'-'.str_pad(++$num,4,0,STR_PAD_LEFT);
                    }else $SO_Num = 'CK'.$month.'-0001';
                }
                $params["SO_Num"] = $SO_Num;
                // $sid_list = $sid_save_list = [];
                $sectSaveList = $store_in_list = [];
                $msg = "";
                // pri($paramsTable,1);
                list($sectSaveList,$store_in_list,$msg) = $this->storeOutGetState($params,$paramsTable);
                if($msg != "") $this->error($msg);
                unset($params["PT_Num"]);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->firstModel));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->firstModel->validateFailException(true)->validate($validate);
                    }
                    $this->firstModel::create($params);
                    $result = $this->model->allowField(true)->saveAll(array_values($sectSaveList));
                    $store_in_result = (new StoreInDetail())->allowField(true)->saveAll($store_in_list);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$SO_Num);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        $tableField = $this->getDetailField();
        // $limberList = $this->limberList();
        $this->view->assign("row",$row);
        $this->view->assign("tableField", $tableField);
        // $this->view->assign("limberList", $limberList);
        $this->assignconfig("tableField", $tableField);
        // $this->assignconfig("limberList", $limberList);
        return $this->view->fetch();
    }

    //ids 为sod_id
    public function edit($ids=null)
    {
        $row = $this->firstModel->alias("so")
            ->join(["storeoutdetail"=>"sod"],"so.SO_Num = sod.SO_Num")
            ->where("so.SO_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $SO_Num = $row["SO_Num"];
                $params["SO_Num"] = $SO_Num;
                $sectSaveList = $store_in_list = [];
                $msg = "";
                list($sectSaveList,$store_in_list,$msg) = $this->storeOutGetState($params,$paramsTable);
                if($msg != "") $this->error($msg);
                unset($params["PT_Num"]);
                Db::startTrans();
                try {
                    $this->firstModel->where("SO_Num",$SO_Num)->update($params);
                    if($row["SO_Auditor"]==""){
                        $result = $this->model->allowField(true)->saveAll(array_values($sectSaveList));
                        $store_in_result = (new StoreInDetail())->allowField(true)->saveAll($store_in_list);
                    }
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $list = $this->model->alias("sod")
            ->join(["storeindetail"=>"sid"],"sid.SID_ID = sod.SID_ID","left")
            ->join(["storein"=>"si"],"si.SI_OtherID = sid.SI_OtherID","left")
            // ->join(["storestate"=>"ss"],$ss_join_list,"left")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = sod.IM_Num","left")
            ->join(["workreceivedetail"=>"wrd"],"wrd.WRD_ID = sod.WRD_ID","left")
            ->field("sod.WRD_ID,im.IM_PerWeight,sod.SOD_ID,sod.SID_ID,im.IM_Num,'".$row["SO_WareHouse"]."' as SO_WareHouse,im.IM_Class,sod.L_Name,im.IM_Spec,round(sod.SOD_Length/1000,3) as SOD_Length,round(sod.SOD_Width/1000,3) as SOD_Width,sod.SOD_Count,sod.SOD_Count as 'ago_SOD_Count',sod.SOD_Weight,sod.SOD_Weight as 'ago_SOD_Weight',sid.SID_RestCount,sid.SID_RestWeight,wrd.WRD_PlanCount,wrd.WRD_PlanWeight,sid.SID_CTD_Pi,sod.LuPiHao,sod.PiHao,sid.CT_Num,left(si.SI_InDate,10) as SI_InDate,sod.SOD_Memo,sod.purpose,sod.SOD_BWeight,sid.SID_testnum,sid.SID_TestResult,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("sod.SO_Num",$ids)
            ->where("si.SI_WareHouse",$row["SO_WareHouse"])
            ->order("clsort,sod.L_Name,bh,im.IM_Class,SOD_Length ASC")
            ->select();
        $sum_weight = $count = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $v['SOD_Length'] = round($v['SOD_Length'],3);
            $v['SOD_Width'] = round($v['SOD_Width'],3);
            $v['SOD_Count'] = round($v['SOD_Count'],2);
            $v['ago_SOD_Count'] = round($v['ago_SOD_Count'],2);
            $v['SOD_Weight'] = round($v['SOD_Weight'],2);
            $v['ago_SOD_Weight'] = round($v['ago_SOD_Weight'],2);
            $v['SID_RestCount'] = round($v['SID_RestCount'],2);
            $v['SID_RestWeight'] = round($v['SID_RestWeight'],2);
            $v['WRD_PlanCount'] = round($v['WRD_PlanCount'],2);
            $v['WRD_PlanWeight'] = round($v['WRD_PlanWeight'],2);
            $v['SOD_BWeight'] = round($v['SOD_BWeight'],2);
            if($row["SO_Auditor"]==""){
                $v["SID_RestCount"] += $v["SOD_Count"];
                $v["SID_RestWeight"] += $v["SOD_Weight"];
            }
            $rows[$k] = $v->toArray();
            $sum_weight += $v["SOD_Weight"];
            $count += $v["SOD_Count"];
        }
        $row["sum_weight"] = $sum_weight;
        $row["count"] = $count;
        $tableField = $this->getDetailField();
        $this->view->assign("row", $row);
        $this->view->assign("detailList", $rows);
        $this->view->assign("tableField", $tableField);
        $this->assignconfig("tableField", $tableField);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function allDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);

        $row = $this->firstModel->where("SO_Num",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"该条信息已删除，或者请稍后重试"]);
        else if($row["SO_Auditor"]) return json(["code"=>0,"msg"=>"该条信息已审核，或者请稍后重试"]);

        $store_in_model = new StoreInDetail();
        $list = $this->model->where("SO_Num",$num)->select();
        $state_in_list = [];
        foreach($list as $v){
            $store_in_one = $store_in_model->where("SID_ID",$v["SID_ID"])->find();
            if(!$store_in_one) return json(["code"=>0,"msg"=>"删除失败，或者请稍后重试"]);
            isset($state_in_list[$store_in_one["SID_ID"]])?"":$state_in_list[$store_in_one["SID_ID"]] = ["SID_ID"=>$store_in_one["SID_ID"],"SID_RestCount"=>$store_in_one["SID_RestCount"],"SID_RestWeight"=>$store_in_one["SID_RestWeight"]];
            $state_in_list[$store_in_one["SID_ID"]]["SID_RestCount"] += $v["SOD_Count"];
            $state_in_list[$store_in_one["SID_ID"]]["SID_RestWeight"] += $v["SOD_Weight"];
        }

        $result=0;
        Db::startTrans();
        try {
            $result += $this->firstModel->where("SO_Num",$num)->delete();
            $result += $this->model->where("SO_Num",$num)->delete();
            if(!empty($state_in_list)) $store_in_model->allowField(true)->saveAll($state_in_list);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result != false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        $one = $this->model->alias("sod")
            ->join(["storeout"=>"so"],"sod.SO_Num = so.SO_Num")
            ->field("so.SO_WareHouse,sod.IM_Num,sod.SID_ID,sod.L_Name,sod.SOD_Count,sod.SOD_Length,sod.SOD_Width,sod.SOD_Weight,sod.SOD_Money,so.SO_Auditor")
            ->where("sod.SOD_ID",$num)
            ->find();
        if(!$one) return json(["code"=>0,'msg'=>'刪除失敗']);
        else if($one["SO_Auditor"]) return json(["code"=>0,'msg'=>'已审核，删除失败']);
        $sid_model = new StoreInDetail();
        $sid_update = [];
        $sid_one = $sid_model->where("SID_ID",$one["SID_ID"])->find();
        if($sid_one){
            $sid_update = [
                "SID_RestCount" => $one["SOD_Count"]+$sid_one["SID_RestCount"],
                "SID_RestWeight" => $one["SOD_Weight"]+$sid_one["SID_RestWeight"],
            ];
        }
        if($num){
            Db::startTrans();
            try {
                $this->model->where("SOD_ID",$num)->delete();
                if(!empty($sid_update)) $sid_model->where("SID_ID",$one["SID_ID"])->update($sid_update);
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function chooseWork($warehouse='')
    {
        if(!$warehouse) $this->error("没有选择仓库！");
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $workreceive_model = new WorkReceive();
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $workreceive_model->alias("workreceive")
                ->join(["taskdetail"=>"taskdetail"],"workreceive.TD_ID = taskdetail.TD_ID","left")
                ->join(["task"=>"task"],"task.T_Num = taskdetail.T_Num","left")
                ->join(["compact"=>"compact"],"task.C_Num = compact.C_Num","left")
                ->field("workreceive.WR_Send,workreceive.WR_Date,workreceive.WR_Num,workreceive.WR_WareHouse,taskdetail.T_Num,task.t_project,task.t_shortproject,workreceive.TD_TypeName,taskdetail.TD_Pressure,workreceive.WR_Person,workreceive.WR_Dept,workreceive.Writer,workreceive.WriteDate,workreceive.PT_Num,workreceive.WR_Memo,workreceive.TD_ID,CASE WHEN WR_Send=0 THEN '未发料' ELSE '已发料' END AS WR_Send,compact.PC_Num,compact.Customer_Name,compact.C_Num")
                ->where($where)
                ->where("workreceive.WR_WareHouse",$warehouse)
                ->where("workreceive.WR_Send","")
                ->order($sort, $order)
                ->order("workreceive.WR_Num desc")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->assignconfig("warehouse",$warehouse);
        $defaultTime = "2022-05-28 00:00:00 - ".date("Y-m-d H:i:s");
        $this->assignconfig("defaultTime",$defaultTime);
        return $this->view->fetch();
    }

    public function detailWork()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $wrd_out = $this->model->alias("sod")
            ->join(["workreceivedetail"=>"wrd"],"sod.WRD_ID = wrd.WRD_ID")
            ->field("wrd.WRD_ID,sum(sod.SOD_Count) as SOD_Count,sum(sod.SOD_Weight) as SOD_Weight")
            ->where("wrd.WR_Num",$num)
            ->group("wrd.WRD_ID")
            ->select();
        $work_list = $wrd_list = [];
        foreach($wrd_out as $k=>$v){
            $wrd_list[$v["WRD_ID"]] = ["SOD_Count"=>round($v["SOD_Count"],2),"SOD_Weight"=>round($v["SOD_Weight"],2)];
        }
        $list = (new WorkReceiveDetail())->field("WRD_ID,WRD_WareHouse,WRD_Place,WR_Num,IM_Class,IM_Spec,D_IM_Spec,L_Name,D_L_Name,WRD_Length,WRD_Width,WRD_Unit,WRD_RealCount,WRD_RealWeight,WRD_Memo,IM_Num,(CASE WHEN D_IM_Num='' THEN IM_Num ELSE D_IM_Num END) AS D_IM_Num,(CASE WHEN D_IM_Spec='' THEN IM_Spec ELSE D_IM_Spec END) AS D_IM_Spec,(CASE WHEN D_L_Name='' THEN L_Name ELSE D_L_Name END) AS D_L_Name,case IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
        when IM_Class='角钢' then 
        SUBSTR(REPLACE(IM_Spec,'∠',''),1,locate('*',REPLACE(IM_Spec,'∠',''))-1)*1000+
        SUBSTR(REPLACE(IM_Spec,'∠',''),locate('*',REPLACE(IM_Spec,'∠',''))+1,2)*1
        when (IM_Class='钢板' or IM_Class='法兰') then 
        REPLACE(IM_Spec,'-','')
        else 0
        end) as UNSIGNED) bh")->where("WR_Num",$num)->order("clsort,IM_Class,bh,WRD_Length")->select();
        foreach($list as $k=>$v){
            $v["WRD_RealCount"] = round($v["WRD_RealCount"],2);
            $v["WRD_RealWeight"] = round($v["WRD_RealWeight"],2);
            $v["WRD_Length"] = round($v["WRD_Length"],3);
            $v["WRD_Width"] = round($v["WRD_Width"],3);
            $work_list[$k] = $v->toArray();
            $work_list[$k]["WRD_ResetCount"] = $v["WRD_RealCount"]-($wrd_list[$v["WRD_ID"]]["SOD_Count"]??0);
            $work_list[$k]["WRD_ResetWeight"] = $v["WRD_RealWeight"]-($wrd_list[$v["WRD_ID"]]["SOD_Weight"]??0);
            $work_list[$k]["WRD_ResetCount"] = round($work_list[$k]["WRD_ResetCount"],2);
            $work_list[$k]["WRD_ResetWeight"] = round($work_list[$k]["WRD_ResetWeight"],2);
            if($work_list[$k]["WRD_ResetWeight"]<=0){
                unset($work_list[$k]);
            }
        }
        // pri($work_list,1);
        return json(["code"=>1,"data"=>array_values($work_list)]);
    }

    public function detailWorkIn()
    {
        $data = $this->request->post("data");
        $formdata = json_decode($data,true);
        $store_in_model = new StoreInDetail();
        $out_list = $reset_list = [];
        foreach($formdata as $k=>$v){
            $where = [
                "sid.IM_Num" => ["=",$v["D_IM_Num"]],
                "sid.L_Name" => ["=",$v["D_L_Name"]],
                "sid.SID_Length" => ["=",$v["WRD_Length"]],
                "sid.SID_Width" => ["=",$v["WRD_Width"]],
                "si.SI_WareHouse" => ["=",$v["WRD_WareHouse"]]
            ];
            $ck_count = $v["WRD_ResetCount"];
            $ck_weight = $v["WRD_ResetWeight"];
            if($v["IM_Class"]=="角钢"){
                if($ck_count>0){
                    $in_list = $store_in_model->alias("sid")->join(["storein"=>"si"],"si.SI_OtherID = sid.SI_OtherID")->join(["inventorymaterial"=>"im"],"im.IM_Num=sid.IM_Num")->field("sid.*,im.IM_PerWeight")->where($where)->where("SID_RestCount",">",0)->where("si.SI_AudioPepo","<>","")->order("order asc,SID_ID DESC")->select();
                    foreach($in_list as $kk=>$vv){
                        isset($reset_list[$vv["SID_ID"]])?"":$reset_list[$vv["SID_ID"]] = ["SID_ID"=>$vv["SID_ID"],"SID_RestCount"=>$vv["SID_RestCount"],"SID_RestWeight"=>$vv["SID_RestWeight"]];
                        if($reset_list[$vv["SID_ID"]]["SID_RestCount"]<=0) continue;
                        $dz = $reset_list[$vv["SID_ID"]]["SID_RestCount"]?round($reset_list[$vv["SID_ID"]]["SID_RestWeight"]/$reset_list[$vv["SID_ID"]]["SID_RestCount"],2):0;
                        $bc_count = ($ck_count-$reset_list[$vv["SID_ID"]]["SID_RestCount"])<0?$ck_count:$reset_list[$vv["SID_ID"]]["SID_RestCount"];
                        $bc_weight = ($ck_count-$reset_list[$vv["SID_ID"]]["SID_RestCount"])<0?$ck_weight:$reset_list[$vv["SID_ID"]]["SID_RestWeight"];
                        $out_list[] = [
                            "IM_PerWeight" => $vv["IM_PerWeight"],
                            "WRD_ID" => $v["WRD_ID"],
                            "SID_ID" => $vv["SID_ID"],
                            "IM_Num" => $vv["IM_Num"],
                            "SO_WareHouse" => $v["WRD_WareHouse"],
                            "IM_Class" => $v["IM_Class"],
                            "L_Name" => $v["D_L_Name"],
                            "IM_Spec" => $v["D_IM_Spec"],
                            "SOD_Length" => $vv["SID_Length"],
                            "SOD_Width" => $vv["SID_Width"],
                            "SOD_Count" => $bc_count,
                            "SOD_Weight" => $bc_weight,
                            "SID_RestCount" => $reset_list[$vv["SID_ID"]]["SID_RestCount"],
                            "SID_RestWeight" => $reset_list[$vv["SID_ID"]]["SID_RestWeight"],
                            "SID_CTD_Pi" => $vv["SID_CTD_Pi"],
                            "LuPiHao" => $vv["LuPiHao"],
                            "PiHao" => $vv["PiHao"],
                            "SOD_BWeight" => $bc_weight
                        ];
                        $reset_list[$vv["SID_ID"]]["SID_RestCount"] -= $bc_count;
                        $reset_list[$vv["SID_ID"]]["SID_RestWeight"] -= $bc_weight;
                        $ck_count -= $bc_count;
                        $ck_weight -= $bc_weight;
                        if($ck_count<=0) break;
                    }
                }
            }else{
                if($ck_weight>0){
                    $in_list = $store_in_model->alias("sid")->join(["storein"=>"si"],"si.SI_OtherID = sid.SI_OtherID")->join(["inventorymaterial"=>"im"],"im.IM_Num=sid.IM_Num")->field("sid.*,im.IM_PerWeight")->where($where)->where("si.SI_AudioPepo","<>","")->where("SID_RestWeight",">",0)->order("order asc,SID_ID DESC")->select();
                    foreach($in_list as $kk=>$vv){
                        // pri(1,$reset_list,'');
                        isset($reset_list[$vv["SID_ID"]])?"":$reset_list[$vv["SID_ID"]] = ["SID_ID"=>$vv["SID_ID"],"SID_RestCount"=>$vv["SID_RestCount"],"SID_RestWeight"=>$vv["SID_RestWeight"]];
                        if($reset_list[$vv["SID_ID"]]["SID_RestWeight"]<=0) continue;

                        $bc_count = ($ck_count-$reset_list[$vv["SID_ID"]]["SID_RestCount"])<0?$ck_count:$reset_list[$vv["SID_ID"]]["SID_RestCount"];
                        $bc_weight = ($ck_weight-$reset_list[$vv["SID_ID"]]["SID_RestWeight"])<0?$ck_weight:$reset_list[$vv["SID_ID"]]["SID_RestWeight"];
                        $out_list[] = [
                            "IM_PerWeight" => $vv["IM_PerWeight"],
                            "WRD_ID" => $v["WRD_ID"],
                            "SID_ID" => $vv["SID_ID"],
                            "IM_Num" => $vv["IM_Num"],
                            "SO_WareHouse" => $v["WRD_WareHouse"],
                            "IM_Class" => $v["IM_Class"],
                            "L_Name" => $v["D_L_Name"],
                            "IM_Spec" => $v["D_IM_Spec"],
                            "SOD_Length" => $vv["SID_Length"],
                            "SOD_Width" => $vv["SID_Width"],
                            "SOD_Count" => $bc_count,
                            "SOD_Weight" => $bc_weight,
                            "SID_RestCount" => $reset_list[$vv["SID_ID"]]["SID_RestCount"],
                            "SID_RestWeight" => $reset_list[$vv["SID_ID"]]["SID_RestWeight"],
                            "SID_CTD_Pi" => $vv["SID_CTD_Pi"],
                            "LuPiHao" => $vv["LuPiHao"],
                            "PiHao" => $vv["PiHao"],
                            "SOD_BWeight" => $bc_weight
                        ];
                        $reset_list[$vv["SID_ID"]]["SID_RestCount"] -= $bc_count;
                        $reset_list[$vv["SID_ID"]]["SID_RestWeight"] -= $bc_weight;
                        $ck_count -= $bc_count;
                        $ck_weight -= $bc_weight;
                        if($ck_weight<=0) break;
                        // pri(2,$reset_list,'');
                    }
                }
            }
            
            // pri($reset_list,1);
        }
        // die;
        return json(["code"=>1,"data"=>$out_list]);
    }

    public function getDetailField(){
        $tableField = [
            ["比重","IM_PerWeight","readonly","hidden","40","","IM_PerWeight",0],
            ["WRD_ID","WRD_ID","readonly","hidden","40","","WRD_ID",0],
            ["出库ID","SOD_ID","readonly","","50","save","SOD_ID",0],
            ["入库ID","SID_ID","readonly","","50","save","SID_ID",""],
            ["存货编码","IM_Num","readonly","","70","save","IM_Num",""],
            ["所在仓库","SO_WareHouse","readonly","","90","save","SI_WareHouse",""],
            ["材料名称","IM_Class","readonly","","60","","IM_Class",""],
            ["材质","L_Name","readonly","","70","","L_Name",""],
            ["规格","IM_Spec","readonly","","75","","IM_Spec",""],
            ["长度(m)","SOD_Length","readonly","","40","save","SSCD_Length",0],
            ["宽度(m)","SOD_Width","readonly","","40","save","SSCD_Width",0],
            ["出库数量","ago_SOD_Count","","hidden","40","save","ago_SOD_Count",0],
            ["出库数量","SOD_Count","","","40","save","SOD_Count",0],
            ["出库重量(kg)","ago_SOD_Weight","","hidden","40","save","ago_SOD_Weight",0],
            ["出库重量(kg)","SOD_Weight","","","65","save","SOD_Weight",0],
            //剩余
            ["剩余数量","SID_RestCount","readonly","","40","","SID_RestCount",0],
            ["剩余重量(kg)","SID_RestWeight","readonly","","65","","SID_RestWeight",0],
            
            // ["含税单价","SID_PlanPrice","readonly","","60","save","SSCD_Price",0],
            // ["含税金额","ago_SOD_Money","readonly","hidden","85","save","ago_SOD_Money",0],
            // ["含税金额","SOD_Money","readonly","","85","save","SOD_Money",0],
            // ["不含税单价","SID_NoTaxPrice","readonly","","85","save","SSCD_NaxPrice",0],
            // ["不含税金额","SOD_NoTaxMoney","readonly","","85","save","SOD_NoTaxMoney",0],
            //计划
            // ["计划单价","WRD_PlanCount","readonly","","60","","WRD_PlanCount",0],
            // ["计划金额","WRD_PlanWeight","readonly","","85","","WRD_PlanWeight",0],
            
            ["进货批次","SID_CTD_Pi","readonly","","60","","SID_CTD_Pi",""],
            ["炉号","LuPiHao","readonly","","110","","LuPiHao",""],
            ["批号","PiHao","readonly","","110","","PiHao",""],
            //理化申请 暂时不用
            // ["质保书","WRD_Unit","readonly","","30","save"],
            ["委试号","CT_Num","readonly","","90","","CT_Num",""],
            ["入库时间","SI_InDate","readonly","","80","","SI_InDate",""],
            ["备注","SOD_Memo","","","80","save","SOD_Memo",""],
            ["用途","purpose","","","80","save","purpose",""],
            ["过磅重量","SOD_BWeight","","","65","save","SOD_BWeight",""],
            //过磅金额 = 含税金额 
            // ["过磅金额","SOD_BMoney","","","85","","SOD_BMoney",""],
            ["试验编号","SID_testnum","","","80","","SID_testnum",""],
            //到货单 记得补上
            // ["到货日期","WRD_Unit","","","30",""],
            ["生产厂家","SID_TestResult","readonly","","80","","SID_TestResult",""],
            //到货和理化申请都有 记得补上
            // ["供应商","WRD_Unit","","","30","save"]
        ];
        return $tableField;
    }

    protected function storeOutGetState($params,$paramsTable)
    {
        $sectSaveList = $store_in_list = [];
        $msg = "";
        // $store_in_list = [];
        $store_in_model = new StoreInDetail();
        foreach($paramsTable["SOD_ID"] as $k=>$v){
            if(round($paramsTable["SOD_Count"][$k],2)==false and round($paramsTable["SOD_Weight"][$k],2)==false) $msg .= "第".($k+1)."行出库数量和出库重量不能都为0;<br>";
            $in_detail_one = $store_in_model->where("SID_ID",$paramsTable["SID_ID"][$k])->find();
            if($in_detail_one){
                isset($store_in_list[$in_detail_one["SID_ID"]])?"":$store_in_list[$in_detail_one["SID_ID"]]=["SID_ID"=>$paramsTable["SID_ID"][$k],"SID_RestCount"=>$in_detail_one["SID_RestCount"],"SID_RestWeight"=>$in_detail_one["SID_RestWeight"]];
            }else{
                $msg .= "第".($k+1)."入库单出错，请删除;<br>";
                break;
            }
            $sectSaveList[$k] = [
                "SOD_ID" => $paramsTable["SOD_ID"][$k],
                "IM_Num" => $paramsTable["IM_Num"][$k],
                "L_Name" => $paramsTable["L_Name"][$k],
                "SOD_Count" => $paramsTable["SOD_Count"][$k]==false?0:$paramsTable["SOD_Count"][$k],
                "SOD_Length" => ($paramsTable["SOD_Length"][$k]==false?0:$paramsTable["SOD_Length"][$k])*1000,
                "SOD_Width" => ($paramsTable["SOD_Width"][$k]==false?0:$paramsTable["SOD_Width"][$k])*1000,
                "SOD_Weight" => $paramsTable["SOD_Weight"][$k]==false?0:$paramsTable["SOD_Weight"][$k],
                "SOD_Memo" => $paramsTable["SOD_Memo"][$k],
                "SOD_Sended" => $paramsTable["WRD_ID"][$k]==false?0:1,
                "SID_ID" => $paramsTable["SID_ID"][$k],
                "LuPiHao" => $paramsTable["LuPiHao"][$k],
                "PiHao" => $paramsTable["PiHao"][$k],
                "SO_ProjectName" => $params["SO_ProjectName"],
                "SO_TowerType" => $params["SO_TowerType"],
                "SO_EType" => $params["SO_EType"],
                "PC_Num" => $params["PC_Num"],
                "T_Num" => $params["T_Num"],
                "PT_Num" => $params["PT_Num"],
                "purpose" => $paramsTable["purpose"][$k],
                "WRD_ID" => $paramsTable["WRD_ID"][$k],
                "SOD_BWeight" => $paramsTable["SOD_BWeight"][$k],
                "SO_Num" => $params["SO_Num"]
            ];

            $store_in_list[$in_detail_one["SID_ID"]]["SID_RestCount"] += ($paramsTable["ago_SOD_Count"][$k]-($paramsTable["SOD_Count"][$k]==false?0:$paramsTable["SOD_Count"][$k]));
            $store_in_list[$in_detail_one["SID_ID"]]["SID_RestWeight"] += ($paramsTable["ago_SOD_Weight"][$k]-($paramsTable["SOD_Weight"][$k]==false?0:$paramsTable["SOD_Weight"][$k]));
            if($store_in_list[$in_detail_one["SID_ID"]]["SID_RestCount"]<0) $msg .= "第".($k+1)."行出库数量大于剩余数量;<br>";
            if($store_in_list[$in_detail_one["SID_ID"]]["SID_RestWeight"]<0) $msg .= "第".($k+1)."行出库重量大于剩余重量;<br>";


            if(!$paramsTable["SOD_ID"][$k]) unset($sectSaveList[$k]["SOD_ID"]);
            
            
        }
        return [$sectSaveList,$store_in_list,$msg];
    }
    
    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $state_model = new StoreState();
        $list = $this->firstModel->alias("so")->join(["storeoutdetail"=>"sod"],"so.SO_Num = sod.SO_Num")->join(["inventorymaterial"=>"im"],"im.IM_Num = sod.IM_Num")->where("so.SO_Num",$num)->select();
        $sectSaveList = $state = [];
        foreach($list as $k=>$v){
            $key = $v["IM_Num"].'-'.$v["L_Name"].'-'.round($v["SOD_Length"],2).'-'.round($v["SOD_Width"],2);
            if(!isset($sectSaveList[$key])){
                $sectSaveList[$key] = [
                    "IM_Num" => $v["IM_Num"],
                    "IM_Spec" => $v["IM_Spec"],
                    "L_Name" => $v["L_Name"],
                    "SOD_Length" => $v["SOD_Length"],
                    "SOD_Width" => $v["SOD_Width"],
                    "SS_WareHouse" => $v["SO_WareHouse"],
                    "SOD_Count" => $v["SOD_Count"],
                    "SOD_Weight" => $v["SOD_Weight"],
                ];
            }else{
                $sectSaveList[$key]["SOD_Count"] += $v["SOD_Count"];
                $sectSaveList[$key]["SOD_Weight"] += $v["SOD_Weight"];
            }
        }
        foreach($sectSaveList as $k=>$v){
            $where = [
                "IM_Num" => ["=",$v["IM_Num"]],
                "L_Name" => ["=",$v["L_Name"]],
                "SS_Length" => ["=",$v["SOD_Length"]],
                "SS_Width" => ["=",$v["SOD_Width"]],
                "SS_WareHouse" => ["=",$v["SS_WareHouse"]]
            ];
            $state_one = $state_model->where($where)->find();
            $edit_one = [];
            if($state_one){
                $edit_one = $state_one->toArray();
                $edit_one["SS_Count"] = $edit_one["SS_Count"]-$v["SOD_Count"];
                $edit_one["SS_Weight"] = $edit_one["SS_Weight"]-$v["SOD_Weight"];
                // $edit_one["ASF_Count"] = $edit_one["ASF_Count"]-$v["SOD_Count"];
                // $edit_one["ASF_Weight"] = $edit_one["ASF_Weight"]-$v["SOD_Weight"];
            }else return json(["code"=>0,"msg"=>"库存有误！"]);
            $state[] = $edit_one;
        }
        $result = false;
        Db::startTrans();
        try {
            $result = $this->firstModel->where("SO_Num",$num)->update(['SO_Num' => $num,'SO_Auditor'=>$admin,"SO_AudiDate"=>date("Y-m-d H:i:s")]);
            $state_model->allowField(true)->saveAll($state);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
        
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $state_model = new StoreState();
        $list = $this->firstModel->alias("so")->join(["storeoutdetail"=>"sod"],"so.SO_Num = sod.SO_Num")->join(["inventorymaterial"=>"im"],"im.IM_Num = sod.IM_Num")->where("so.SO_Num",$num)->select();
        $sectSaveList = $state = [];
        foreach($list as $k=>$v){
            $key = $v["IM_Num"].'-'.$v["L_Name"].'-'.round($v["SOD_Length"],2).'-'.round($v["SOD_Width"],2);
            if(!isset($sectSaveList[$key])){
                $sectSaveList[$key] = [
                    "IM_Num" => $v["IM_Num"],
                    "IM_Spec" => $v["IM_Spec"],
                    "L_Name" => $v["L_Name"],
                    "SOD_Length" => $v["SOD_Length"],
                    "SOD_Width" => $v["SOD_Width"],
                    "SS_WareHouse" => $v["SO_WareHouse"],
                    "SOD_Count" => $v["SOD_Count"],
                    "SOD_Weight" => $v["SOD_Weight"]
                ];
            }else{
                $sectSaveList[$key]["SOD_Count"] += $v["SOD_Count"];
                $sectSaveList[$key]["SOD_Weight"] += $v["SOD_Weight"];
            }
        }
        foreach($sectSaveList as $k=>$v){
            $where = [
                "IM_Num" => ["=",$v["IM_Num"]],
                "L_Name" => ["=",$v["L_Name"]],
                "SS_Length" => ["=",$v["SOD_Length"]],
                "SS_Width" => ["=",$v["SOD_Width"]],
                "SS_WareHouse" => ["=",$v["SS_WareHouse"]]
            ];
            $state_one = $state_model->where($where)->find();
            $edit_one = [];
            if($state_one){
                $edit_one = $state_one->toArray();
                $edit_one["SS_Count"] = $edit_one["SS_Count"]+$v["SOD_Count"];
                $edit_one["SS_Weight"] = $edit_one["SS_Weight"]+$v["SOD_Weight"];
                // $edit_one["ASF_Count"] = $edit_one["ASF_Count"]+$v["SOD_Count"];
                // $edit_one["ASF_Weight"] = $edit_one["ASF_Weight"]+$v["SOD_Weight"];
            }else{
                $edit_one = [
                    "IM_Num" => $v["IM_Num"],
                    "IM_Spec" => $v["IM_Spec"],
                    "L_Name" => $v["L_Name"],
                    "SS_Length" => $v["SOD_Length"],
                    "SS_Width" => $v["SOD_Width"],
                    "SS_Count" => $v["SOD_Count"],
                    "SS_Weight" => $v["SOD_Weight"],
                    "SS_WareHouse" => $v["SS_WareHouse"]
                ];
            }
            $state[] = $edit_one;
        }
        $result = false;
        Db::startTrans();
        try {
            $result = $this->firstModel->where("SO_Num",$num)->update(['SO_Num' => $num,'SO_Auditor'=>"","SO_AudiDate"=>"0000-00-00 00:00:00"]);
            $state_model->allowField(true)->saveAll($state);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function offerU()
    {
        $id = $this->request->post("id");
        $row = $this->firstModel->where("SO_Num",$id)->find();
        $res = [
            "code"=>0,
            "msg" => "有误",
            "data" => []
        ];
        if(!$row) return json($res);
        else if($row["SO_Auditor"]==''){
            $res["msg"] = "请先审核";
            return json($res);
        }
        $cdepcode = $row["SO_Take"];
        if(!$cdepcode){
            $res["msg"] = "不存在领用部门编号";
            return json($res);
        }
        $cwhcode = $row["SO_WareHouse"];
        if(!$cwhcode){
            $res["msg"] = "不存在仓库编号";
            return json($res);
        }
        $rscList = [0=>"201",11=>"206"];
        $crdcode = $rscList[$row["SO_Flag"]]??'';
        if(!$crdcode){
            $res["msg"] = "不存在该收发类别";
            return json($res);
        }
        $main = [
            "ccode" => $id,
            //采购类型编码
            "cdepname" => $cdepcode,
            //收发类别
            "crdcode" => $crdcode,
            "cwhname" => $cwhcode,
            "ddate" => date("Y-m-d",strtotime($row["SO_TakeDate"])),
            "cmaker" => $row['SO_Writer'],
            "cmemo" => $row['SO_Memo'],
            "chandler" => $row['SO_Auditor'],
            "dveridate" => date("Y-m-d",strtotime($row["SO_AudiDate"])),
        ];
        $list = $this->model
            ->where("SO_Num",$id)
            ->select();
        $details = [];
        foreach($list as $k=>$v){
            $details[$k] = [
                "cinvcode" => $v["IM_Num"],
                "iquantity" => $v["SOD_Weight"],
                "cdefine22" => '',
                "cfree1" => $v["L_Name"],
                "cfree2" => '',
                "cfree3" => $v["SOD_Length"]>0?round($v["SOD_Length"]/1000,3):'',
                "inum" => $v["SOD_Count"]
            ];
        }
        $data = [
            "main" => $main,
            "details" => $details
        ];
        $url = "/PostRdrecord11/Add";
        $result = api_post_u($url,$data);
        if($result["code"]==0){
            $this->firstModel->where("SO_Num",$id)->update(["offer"=>1]);
            $res["code"] = 1;
            $res["msg"] = "成功";
        }else $res["msg"] = $result["ErrorMsg"];
        return json($res);
    }

    // public function eipUpload()
    // {
    //     $id = $this->request->post("id");
    //     $json_result = ["code"=>0,"msg"=>"失败"];
    //     if(!$id) return json($json_result);
    //     $bzList = $this->model->alias("sid")->join(["inventorymaterial"=>"im"],"sid.IM_Num=im.IM_Num")->where("sid.SO_Num",$id)->column("concat(im.IM_Class,sid.SOD_Length,sid.L_Name,im.IM_Spec) AS bz");
    //     $params = (new \app\admin\model\view\StateStoreView())->where("bz","IN",$bzList)->select();
    //     if(!$params) return json($json_result);
    //     $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
    //     $stateBzModel = new \app\admin\model\chain\material\StateBz();
    //     $errorData = $saveData = $eipUploadList = [];
    //     $param_url = "eipmatinventory";
    //     // $materialList= [
    //     //     "角钢" => "01001",
    //     //     "钢板" => "01002",
    //     //     "圆管" => "01003",
    //     //     "钢管" => "01004",
    //     //     "辅助材料" => "01007",
    //     // ];
    //     foreach($params as $k=>$v){
    //         $eipUploadList[$v["bz"]] = ["eip_upload"=>$v["eip_upload"],"bz"=>$v["bz"]];
    //         $v["id"]?$eipUploadList[$v["bz"]]["id"] = $v["id"]:"";
    //         isset($api_data[$v["bz"]])?"":$api_data[$v["bz"]] = [
    //             "purchaserHqCode" => "SGCC",
    //             "supplierCode" => "1000014615",
    //             "categoryCode" => "60",
    //             "subclassCode" => "60001",
    //             "matName" => $v["IM_Class"],
    //             "matCode" => $v["IM_Class"]=="角钢"?"MAT_4526358899591262803":"MAT_4526358899591262804",
    //             "matNum" => "".$v["SID_RestWeight"],
    //             "matUnit" => "kg",
    //             "matVoltageLevel" => "1000KV",
    //             "matProdAddr" => "中国",
    //             "speModels" => $v["bz"],
    //             "putStorageTime" => date("Y-m-d",strtotime($v["SI_WriteDate"]))
    //         ];
    //     }
    //     foreach($api_data as $k=>$v){
    //         $operatetype = "ADD";
    //         if($eipUploadList[$k]["eip_upload"]){
    //             $operatetype = "UPDATE";
    //         }
    //         $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$v);
    //         if($api_result["code"]==1){
    //             $saveData[$k] = $eipUploadList[$k];
    //             $saveData[$k]["eip_upload"] = 1;
    //         }else $errorData[$k] = $api_result["msg"];
    //     }
    //     $msg = "上传成功";
    //     if(!empty($errorData)) {
    //         $msg = "";
    //         foreach($errorData as $k=>$v){
    //             $msg .= $k.$v.";";
    //         }
    //         $json_result["msg"] = $msg;
    //     }else $json_result = ["code"=>1,"msg"=>$msg];
    //     if(!empty($saveData)) $stateBzModel->allowField(true)->saveAll($saveData);
    //     return json($json_result);
    // }

    //上传
    // public function upload()
    // {
    //     // ID
    //     $num = $this->request->post("num");
    //     if(!$num) return json(["code"=>0,"msg"=>"失败"]);
    //     $list = $this->model->where("SO_Num",$num)->column("SOD_ID");
    //     $bzModel = new \app\admin\model\chain\material\ProMaterialTestDetailBz;
    //     $find_one = $bzModel->where("SOD_ID","IN",$list)->find();
    //     if($find_one) return json(["code"=>0,"msg"=>"已上传无法更改"]);
    //     $ini = [
    //         ["MTD_C" , "JGTG01" , "C", 0],
    //         ["MTD_Si" , "JGTG02" , "Si", 0],
    //         ["MTD_Mn" , "JGTG03" , "Mn", 0],
    //         ["MTD_P" , "JGTG04" , "P", 0],
    //         ["MTD_S" , "JGTG05" , "S", 0],
    //         ["MTD_V" , "JGTG06" , "V", 0],
    //         ["MTD_Nb" , "JGTG07" , "Nb", 0],
    //         ["MTD_Ti" , "JGTG08" , "Ti", 0],
    //         ["MTD_Cr" , "JGTG68" , "Cr", 0],
    //         ["MTD_Czsl" , "JGTG32" , "层状撕裂检验", 0],
    //         ["MTD_Rm" , "JGTG09" , "抗拉强度", 0],
    //         ["MTD_Rel" , "JGTG10" , "屈服强度", 0],
    //         ["MTD_Percent" , "JGTG11" , "断后伸长率", 0],
    //         ["MTD_Temperature" , "JGTG12" , "冲击检测", 0],
    //         ["MTD_Wq" , "JGTG13" , "弯曲试验", 0],
    //         ["MTD_Wg" , "JGTG14" , "尺寸外观", 0],
    //         ["MTD_Mader" , "JGTG65" , "生产厂家", 0],
    //         ["MTD_ChDate" , "JGTG66" , "原材料出厂日期", 0],
    //         ["hx_bg" , "JGTS01" , "化学检验报告", 1],
    //         ["fj_bg" , "JGTS05" , "入厂复检报告", 1],
    //         ["ch_bg" , "JGTS49" , "出厂质量证明书", 1]
    //     ];
    //     $data = [];
    //     foreach($list as $v){
    //         foreach($ini as $vv){
    //             $data[] = [
    //                 "SOD_ID" => $v,
    //                 "NAME" => $vv[2],
    //                 "FIELD" => $vv[0],
    //                 "RULE_CODE" => $vv[1],
    //                 "TYPE" => $vv[3],
    //                 "UPLOAD" => 0
    //             ];
    //         }
    //     }
    //     $result = $bzModel->allowField(true)->saveAll($data);
    //     if($result) return json(["code"=>1,"msg"=>"已允许上传"]);
    //     else return json(["code"=>0,"msg"=>"失败"]);
    // }

    //

}
