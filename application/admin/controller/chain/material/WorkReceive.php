<?php

namespace app\admin\controller\chain\material;

use app\admin\model\chain\lofting\MaterialReplace;
use app\admin\model\chain\lofting\MaterialReplaceDetail;
use app\admin\model\chain\lofting\ProduceTask;
use app\admin\model\chain\lofting\ProduceTaskDetail;
use app\admin\model\chain\lofting\UnionProduceTaskView;
use app\admin\model\chain\material\Configuresys;
use app\admin\model\chain\material\StoreOutDetail;
use app\admin\model\jichu\ch\InventoryMaterial;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;

/**
 * 车间领料单
 *
 * @icon fa fa-circle-o
 */
class WorkReceive extends Backend
{
    
    /**
     * WorkReceive模型对象
     * @var \app\admin\model\chain\material\WorkReceive
     */
    protected $model = null,$detailModel,$admin;
    protected $noNeedLogin = ["settingSpec","setting","xddh","organizeMaterials","getRowsSh","getMinSpec"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\WorkReceive;
        $this->detailModel = new \app\admin\model\chain\material\WorkReceiveDetail;
        $this->admin = \think\Session::get('admin');
        $this->assignconfig("time",date("Y-m-d H:i:s"));
        $deptList = [""=>"请选择"];
        $deptModel = (new \app\admin\model\jichu\jg\Deptdetail())
            ->field("DD_Name")
            ->where(["Valid"=>1])
            ->order(["ParentNum"=>"ASC","DD_Num"=>"ASC"])
            ->select();
        foreach($deptModel as $v){
            $deptList[$v["DD_Name"]] = $v["DD_Name"];
        }
        $this->view->assign("deptList",$deptList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $ini_where = [];
            if(isset($filter["is_check"])){
                if($filter["is_check"]=="1") $ini_where["wr.Auditor"] = ["=",""];
                if($filter["is_check"]=="2") $ini_where["wr.Auditor"] = ["<>",""];
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("wr")
                ->join(["taskdetail"=>"td"],"td.TD_ID = wr.TD_ID","LEFT")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num","LEFT")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num","LEFT")
                ->field("wr.WR_Num,wr.WR_Date,wr.WR_Num,wr.WR_WareHouse,wr.TD_ID,c.PC_Num,t.t_project,wr.TD_TypeName,wr.WR_Person,wr.WR_Dept,wr.WR_Memo,wr.Writer,wr.WriteDate,wr.Auditor,wr.AudiDate,t.T_Num,wr.PT_Num,(case when wr.Auditor='' then '未审核' else '已审核' end ) as is_check")
                ->where($where)
                ->where($ini_where)
                ->order($sort, $order)
                ->order("wr.WR_Num desc")
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params["WR_WareHouse"]){
                $this->error("仓库必选！");
            }
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $WR_Num = $params["WR_Num"];
                if($WR_Num){
                    $one = $this->model->where("WR_Num",$WR_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("ym");
                    $one = $this->model->field("WR_Num")->where("WR_Num","LIKE","LLD".$month.'-%')->order("WR_Num DESC")->find();
                    if($one){
                        $num = substr($one["WR_Num"],8);
                        $WR_Num = 'LLD'.$month.'-'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $WR_Num = 'LLD'.$month.'-001';
                }
                $params["WR_Num"] = $WR_Num;
                $sectSaveList = [];
                $inventoryNumList = $this->inventoryNumList();
                $msg = "";
                foreach($paramsTable["WRD_ID"] as $k=>$v){
                    $paramsTable["D_IM_Spec"][$k] = $paramsTable["D_IM_Spec"][$k]?$paramsTable["D_IM_Spec"][$k]:$paramsTable["IM_Spec"][$k];
                    $paramsTable["D_L_Name"][$k] = $paramsTable["D_L_Name"][$k]?$paramsTable["D_L_Name"][$k]:$paramsTable["L_Name"][$k];
                    if($paramsTable["IM_Spec"][$k]!=$paramsTable["D_IM_Spec"][$k]){
                        $searchInventoryKey = $paramsTable["IM_Class"][$k].'-'.$paramsTable["D_IM_Spec"][$k];
                        if(!isset($inventoryNumList[$searchInventoryKey])) $this->error("第".($k+1)."行未匹配到代领材料");
                        $paramsTable["D_IM_Num"][$k] = $inventoryNumList[$searchInventoryKey];
                    }else $paramsTable["D_IM_Num"][$k] = $paramsTable["IM_Num"][$k];
                    if($paramsTable["IM_Class"][$k]=="角钢" and round($paramsTable["WRD_Length"][$k],2)==false) $msg .= "第".($k+1)."行角钢长度不能为0;<br>";
                    $key = $paramsTable["IM_Num"][$k].'-'.$paramsTable["D_IM_Num"][$k].'-'.$paramsTable["D_L_Name"][$k].'-'.$paramsTable["L_Name"][$k].'-'.round($paramsTable["WRD_Length"][$k],2).'-'.round($paramsTable["WRD_Width"][$k],2);
                    if(isset($sectSaveList[$key])) $msg .= "第".($k+1)."行信息重复;<br>";
                    if(round($paramsTable["WRD_RealWeight"][$k],2)==false) $msg .= "第".($k+1)."行实领重量不能为0;<br>";
                    $sectSaveList[$key] = [
                        "WR_Num" => $WR_Num,
                        "IM_Num" => $paramsTable["IM_Num"][$k],
                        "D_IM_Num" => $paramsTable["D_IM_Num"][$k],
                        "IM_Class" => $paramsTable["IM_Class"][$k],
                        "L_Name" => $paramsTable["L_Name"][$k],
                        "D_L_Name" => $paramsTable["D_L_Name"][$k],
                        "IM_Spec" => $paramsTable["IM_Spec"][$k],
                        "D_IM_Spec" => $paramsTable["D_IM_Spec"][$k],
                        "WRD_Length" => ($paramsTable["WRD_Length"][$k]==false?0:$paramsTable["WRD_Length"][$k])*1000,
                        "WRD_Width" => ($paramsTable["WRD_Width"][$k]==false?0:$paramsTable["WRD_Width"][$k])*1000,
                        "WRD_Unit" => $paramsTable["WRD_Unit"][$k],
                        "WRD_PlanCount" => $paramsTable["WRD_PlanCount"][$k]==false?0:$paramsTable["WRD_PlanCount"][$k],
                        "WRD_PlanWeight" => $paramsTable["WRD_PlanWeight"][$k]==false?0:$paramsTable["WRD_PlanWeight"][$k],
                        "WRD_RealCount" => $paramsTable["WRD_RealCount"][$k]==false?0:$paramsTable["WRD_RealCount"][$k],
                        "WRD_RealWeight" => $paramsTable["WRD_RealWeight"][$k]==false?0:$paramsTable["WRD_RealWeight"][$k],
                        "WRD_PlanTime" => $paramsTable["WRD_PlanTime"][$k]==false?"0000-00-00 00:00:00":$paramsTable["WRD_PlanTime"][$k],
                        "WRD_Memo" => $paramsTable["WRD_Memo"][$k],
                        "WRD_Place" => $paramsTable["WRD_Place"][$k],
                        "LuPiHao" => $params["WR_Lu"],
                        "PiHao" => $params["WR_Pi"],
                        "WRD_WareHouse" => $params["WR_WareHouse"]
                    ];
                }
                if($msg != "") $this->error($msg);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model::create($params);
                    $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$WR_Num);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        $tableField = $this->getDetailField();
        list($limberList,$kj_limberList) = $this->getLimber();
        $this->view->assign("row",$row);
        $this->view->assign("wareclassList",$this->wareclassList());
        $this->view->assign("tableField", $tableField);
        $this->view->assign("limberList", $limberList);
        $this->assignconfig("tableField", $tableField);
        $this->assignconfig("limberList", $limberList);
        $this->assignconfig("kj_limberList", $kj_limberList);
        return $this->view->fetch();
    }

    public function edit($ids=null)
    {
        $row = $this->model->alias("wr")
            ->join(["taskdetail"=>"td"],"td.TD_ID = wr.TD_ID","left")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num","left")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num","left")
            ->field("wr.WR_Num,wr.WR_WareHouse,wr.TD_ID,t.t_project,c.PC_Num,t.T_Num,wr.TD_TypeName,wr.WR_Date,wr.WR_Person,wr.WR_Dept,wr.PT_Num,wr.WR_BuyDate,wr.WR_Lu,wr.WR_Pi,wr.WR_BuyPi,wr.WR_ZB,wr.WR_HY,wr.V_Name,wr.WR_Memo,wr.WR_Send,wr.Writer,wr.WriteDate,wr.Auditor,wr.AudiDate,WR_SY")
            ->where("wr.WR_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            $params["WR_Send"] = $params["WR_Send"]??0;
            // $one = $this->model->alias("wr")->join(["workreceivedetail"=>"wrd"],"wr.WR_Num = wrd.WR_Num")->join(["storeoutdetail"=>"sod"],"wrd.WRD_ID=sod.WRD_ID")->where("wr.WR_Num",$ids)->find();
            // if($one){
            //     $this->model->where("WR_Num",$ids)->update($params);
            //     $this->success("保存成功");
            // }
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $WR_Num = $row["WR_Num"];
                $sectSaveList = $compareList = [];
                $inventoryNumList = $this->inventoryNumList();
                $msg = "";
                foreach($paramsTable["WRD_ID"] as $k=>$v){
                    $paramsTable["D_IM_Spec"][$k] = $paramsTable["D_IM_Spec"][$k]?$paramsTable["D_IM_Spec"][$k]:$paramsTable["IM_Spec"][$k];
                    $paramsTable["D_L_Name"][$k] = $paramsTable["D_L_Name"][$k]?$paramsTable["D_L_Name"][$k]:$paramsTable["L_Name"][$k];
                    if($paramsTable["IM_Spec"][$k]!=$paramsTable["D_IM_Spec"][$k]){
                        $searchInventoryKey = $paramsTable["IM_Class"][$k].'-'.$paramsTable["D_IM_Spec"][$k];
                        if(!isset($inventoryNumList[$searchInventoryKey])) $this->error("第".($k+1)."行未匹配到代领材料");
                        $paramsTable["D_IM_Num"][$k] = $inventoryNumList[$searchInventoryKey];
                    }else $paramsTable["D_IM_Num"][$k] = $paramsTable["IM_Num"][$k];

                    if($paramsTable["IM_Class"][$k]=="角钢" and round($paramsTable["WRD_Length"][$k],2)==false) $msg .= "第".($k+1)."行角钢长度不能为0;<br>";
                    $key = $paramsTable["IM_Num"][$k].'-'.$paramsTable["D_IM_Num"][$k].'-'.$paramsTable["D_L_Name"][$k].'-'.$paramsTable["L_Name"][$k].'-'.round($paramsTable["WRD_Length"][$k],2).'-'.round($paramsTable["WRD_Width"][$k],2);
                    if(isset($sectSaveList[$key])) $msg .= "第".($k+1)."行信息重复;<br>";
                    if(round($paramsTable["WRD_RealWeight"][$k],2)==false) $msg .= "第".($k+1)."行实领重量不能为0;<br>";
                    $sectSaveList[$key] = [
                        "row" => ($k+1),
                        "WRD_ID" => $v,
                        "WR_Num" => $WR_Num,
                        "IM_Num" => $paramsTable["IM_Num"][$k],
                        "D_IM_Num" => $paramsTable["D_IM_Num"][$k],
                        "IM_Class" => $paramsTable["IM_Class"][$k],
                        "L_Name" => $paramsTable["L_Name"][$k],
                        "D_L_Name" => $paramsTable["D_L_Name"][$k],
                        "IM_Spec" => $paramsTable["IM_Spec"][$k],
                        "D_IM_Spec" => $paramsTable["D_IM_Spec"][$k],
                        "WRD_Length" => ($paramsTable["WRD_Length"][$k]==false?0:$paramsTable["WRD_Length"][$k])*1000,
                        "WRD_Width" => ($paramsTable["WRD_Width"][$k]==false?0:$paramsTable["WRD_Width"][$k])*1000,
                        "WRD_Unit" => $paramsTable["WRD_Unit"][$k],
                        "WRD_PlanCount" => $paramsTable["WRD_PlanCount"][$k]==false?0:$paramsTable["WRD_PlanCount"][$k],
                        "WRD_PlanWeight" => $paramsTable["WRD_PlanWeight"][$k]==false?0:$paramsTable["WRD_PlanWeight"][$k],
                        "WRD_RealCount" => $paramsTable["WRD_RealCount"][$k]==false?0:$paramsTable["WRD_RealCount"][$k],
                        "WRD_RealWeight" => $paramsTable["WRD_RealWeight"][$k]==false?0:$paramsTable["WRD_RealWeight"][$k],
                        "WRD_PlanTime" => $paramsTable["WRD_PlanTime"][$k]==false?"0000-00-00 00:00:00":$paramsTable["WRD_PlanTime"][$k],
                        "WRD_Memo" => $paramsTable["WRD_Memo"][$k],
                        "WRD_Place" => $paramsTable["WRD_Place"][$k],
                        "LuPiHao" => $params["WR_Lu"],
                        "PiHao" => $params["WR_Pi"],
                        "WRD_WareHouse" => $params["WR_WareHouse"]
                    ];
                    if($v==false) unset($sectSaveList[$key]["WRD_ID"]);
                    else{
                        $compareList[$v] = [
                            "row" => $sectSaveList[$key]["row"],
                            "WRD_RealCount" => $sectSaveList[$key]["WRD_RealCount"],
                            "WRD_RealWeight" => $sectSaveList[$key]["WRD_RealWeight"]
                        ];
                    }
                }
                if($msg != "") $this->error($msg);
                // $outList = (new StoreOutDetail())->field("WRD_ID,SUM(SOD_Count) AS count,SUM(SOD_Weight) AS weight")->where("WRD_ID","IN",array_keys($compareList))->group("WRD_ID")->select();
                // foreach($outList as $v){
                //     if($compareList[$v["WRD_ID"]]["WRD_RealCount"]<$v["count"]) $msg .= $compareList[$v["WRD_ID"]]["row"]."已出库数量大于修改的实际数量<br>";
                //     if($compareList[$v["WRD_ID"]]["WRD_RealWeight"]<$v["weight"]) $msg .= $compareList[$v["WRD_ID"]]["row"]."已出库重量大于修改的实际重量<br>";
                // }
                // if($msg != "") $this->error($msg);
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("WR_Num",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        
        

        $rows_sh = $this->getRowsSh();

        $join_con = [
            "d.D_IM_Num = ss.IM_Num",
            "d.D_L_Name = ss.L_Name",
            "d.D_IM_Spec = ss.IM_Spec",
            "d.WRD_Length = ss.SS_Length",
            "d.WRD_Width = ss.SS_Width",
            "ss.SS_WareHouse = '".$row["WR_WareHouse"]."'"
        ];
        $detailList = $this->detailModel->alias("d")
            ->join(["inventorymaterial"=>"im"],"d.D_IM_Num = im.IM_Num","left")
            ->join(["storestate"=>"ss"],$join_con,"left")
            ->field("d.WRD_ID,d.IM_Num,im.IM_PerWeight,d.IM_Class,d.IM_Spec,D_IM_Spec,(CASE WHEN d.D_IM_Spec=d.IM_Spec THEN '' ELSE d.D_IM_Spec END) AS D_IM_Spec,d.L_Name,(CASE WHEN d.D_L_Name=d.L_Name THEN '' ELSE d.D_L_Name END) AS D_L_Name,round(d.WRD_Length/1000,3) as WRD_Length,round(d.WRD_Width/1000,3) as WRD_Width,d.WRD_Unit,d.WRD_PlanCount,d.WRD_PlanWeight,d.WRD_RealCount,d.WRD_RealWeight,d.WRD_PlanTime,d.WRD_Place,d.WRD_Price,d.WRD_Memo,ss.SS_Count,ss.SS_Weight,case d.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when d.IM_Class='角钢' then 
            SUBSTR(REPLACE(d.IM_Spec,'∠',''),1,locate('*',REPLACE(d.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(d.IM_Spec,'∠',''),locate('*',REPLACE(d.IM_Spec,'∠',''))+1,2)*1
            when (d.IM_Class='钢板' or d.IM_Class='法兰') then 
            REPLACE(d.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("d.WR_Num",$ids)
            ->group("d.WRD_ID")
            ->order("clsort,d.L_Name,bh,d.WRD_Length")
            ->select();
        $detail_arr = [];
        $plan_count = $plan_weight = $real_count = $real_weight = 0;
        foreach($detailList as $k=>$v){
            $detail_arr[$k] = $v->toArray();
            // $detail_arr[$k] = $k+1;
            $detail_arr[$k]["sh_rate"] = $rows_sh[$v["IM_Class"]]??1;
            $detail_arr[$k]["WRD_Length"] = round($v["WRD_Length"],3);
            $detail_arr[$k]["WRD_Width"] = round($v["WRD_Width"],3);
            $detail_arr[$k]["WRD_PlanCount"] = round($v["WRD_PlanCount"],2);
            $detail_arr[$k]["WRD_PlanWeight"] = round($v["WRD_PlanWeight"],2);
            $detail_arr[$k]["WRD_RealCount"] = round($v["WRD_RealCount"],2);
            $detail_arr[$k]["WRD_RealWeight"] = round($v["WRD_RealWeight"],2);
            $detail_arr[$k]["SS_Count"] = round($v["SS_Count"],2);
            $detail_arr[$k]["SS_Weight"] = round($v["SS_Weight"],2);
            $plan_count += $v["WRD_PlanCount"];
            $plan_weight += $v["WRD_PlanWeight"];
            $real_count += $v["WRD_RealCount"];
            $real_weight += $v["WRD_RealWeight"];
        }
        // pri($detail_arr,1);
        $row["plan_count"] = $plan_count;
        $row["plan_weight"] = $plan_weight;
        $row["real_count"] = $real_count;
        $row["real_weight"] = $real_weight;
        $tableField = $this->getDetailField();
        list($limberList,$kj_limberList) = $this->getLimber();
        $this->view->assign("row", $row);
        $this->view->assign("detailList", $detail_arr);
        $this->view->assign("wareclassList",$this->wareclassList());
        $this->view->assign("tableField", $tableField);
        $this->view->assign("limberList", $limberList);
        $this->assignconfig("tableField", $tableField);
        $this->assignconfig("limberList", $limberList);
        $this->assignconfig("kj_limberList", $kj_limberList);
        $this->assignconfig("ids", $ids);
        return $this->view->fetch();
    }

    public function export($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $title = $row["WR_Num"];

        $rows_sh = $this->getRowsSh();

        $join_con = [
            "d.D_IM_Num = ss.IM_Num",
            "d.D_L_Name = ss.L_Name",
            "d.D_IM_Spec = ss.IM_Spec",
            "d.WRD_Length = ss.SS_Length",
            "d.WRD_Width = ss.SS_Width",
            "ss.SS_WareHouse = '".$row["WR_WareHouse"]."'"
        ];
        $detailList = $this->detailModel->alias("d")
            ->join(["inventorymaterial"=>"im"],"d.D_IM_Num = im.IM_Num","left")
            ->join(["storestate"=>"ss"],$join_con,"left")
            ->field("d.WRD_ID,d.IM_Num,im.IM_PerWeight,d.IM_Class,d.IM_Spec,D_IM_Spec,(CASE WHEN d.D_IM_Spec=d.IM_Spec THEN '' ELSE d.D_IM_Spec END) AS D_IM_Spec,d.L_Name,(CASE WHEN d.D_L_Name=d.L_Name THEN '' ELSE d.D_L_Name END) AS D_L_Name,round(d.WRD_Length/1000,3) as WRD_Length,round(d.WRD_Width/1000,3) as WRD_Width,d.WRD_Unit,d.WRD_PlanCount,d.WRD_PlanWeight,d.WRD_RealCount,d.WRD_RealWeight,d.WRD_PlanTime,d.WRD_Place,d.WRD_Price,d.WRD_Memo,ss.SS_Count,ss.SS_Weight,case d.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when d.IM_Class='角钢' then 
            SUBSTR(REPLACE(d.IM_Spec,'∠',''),1,locate('*',REPLACE(d.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(d.IM_Spec,'∠',''),locate('*',REPLACE(d.IM_Spec,'∠',''))+1,2)*1
            when (d.IM_Class='钢板' or d.IM_Class='法兰') then 
            REPLACE(d.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("d.WR_Num",$ids)
            ->group("d.WRD_ID")
            ->order("clsort,d.L_Name,bh,d.WRD_Length")
            ->select();
        $detail_arr = [];
        foreach($detailList as $k=>$v){
            $detail_arr[$k] = $v->toArray();
            $detail_arr[$k]["ID"] = $k+1;
            $detail_arr[$k]["sh_rate"] = $rows_sh[$v["IM_Class"]]??1;
            $detail_arr[$k]["WRD_Length"] = round($v["WRD_Length"],3);
            $detail_arr[$k]["WRD_Width"] = round($v["WRD_Width"],3);
            $detail_arr[$k]["WRD_PlanWeight"] = round($v["WRD_PlanWeight"],2);
            $detail_arr[$k]["WRD_RealWeight"] = round($v["WRD_RealWeight"],2);
        }
        $tableField = $this->getDetailField();
        $header = [['ID', 'ID']];
        foreach($tableField as $k=>$v){
            if($v[5]==""){
                $header[] = [$v[0],$v[1]];
            }
        }
        return Excel::exportData($detail_arr, $header, $title .'-清单-'. date('Ymd'));
    
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            $one = $this->model->alias("m")->join(["workreceivedetail"=>"wrd"],"wrd.WR_Num = m.WR_Num")->where("wrd.WRD_ID",$num)->find();
            if($one["Auditor"]) return json(["code"=>0,'msg'=>"已审核，不能进行删除！"]);
            Db::startTrans();
            try {
                $this->detailModel->where("WRD_ID",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function setting()
    {
        $configModel = new Configuresys();
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $result = false;
            foreach($params as $k=>$v){
                $result += $configModel->save(["SetValue"=>$v],["SetName"=>$k]);
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were updated'));
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $where_list = ["loss_rate_JG"=>"角钢","loss_rate_GB"=>"钢板","loss_rate_yg"=>"圆钢","loss_rate_lwg"=>"螺纹钢","loss_rate_wfg"=>"无缝管","loss_rate_qt"=>"其他"];
        $list = $configModel->field("SetName,SetValue")->where("SetName","IN",array_keys($where_list))->select();
        $rows = [];
        foreach($list as $v){
            $rows[] = [
                "name" => $where_list[$v["SetName"]],
                "key" => $v["SetName"],
                "value" => $v["SetValue"]
            ];
        }
        // pri($rows,1);
        $this->view->assign("rows", $rows);
        return $this->view->fetch();
    }

    public function settingSpec()
    {
        $configModel = new Configuresys();
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $result = $configModel->where("SetName",'work_min_spec')->update(["SetValue"=>$params["work_min_spec"]]);
            if ($result != false) {
                $this->success();
            } else {
                $this->error(__('No rows were updated'));
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $configModel->field("SetName,SetValue")->where("SetName",'work_min_spec')->find();
        $rows = ["work_min_spec"=>""];
        if($list) $rows["work_min_spec"] = $list["SetValue"];
        $this->view->assign("rows", $rows);
        return $this->view->fetch();
    }

    public function xddh()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new UnionProduceTaskView())->alias("p")
                ->join(["taskdetail"=>"d"],"p.TD_ID = D.TD_ID")
                ->join(["task"=>"t"],"t.T_Num=d.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->join(["workreceive"=>"wr"],"wr.PT_Num = p.PT_Num","left")
                ->field("p.*,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num',c.C_Num,c.C_Num as 'c.C_Num',c.PC_Num,c.PC_Num as 'c.PC_Num',d.TD_Pressure,ifnull(wr.WR_Num,0) as WR_Num")
                ->where($where)
                ->where("not exists ( select 'x' from workreceive wr where wr.PT_Num=p.PT_Num )")
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $defaultTime = "2022-05-28 00:00:00 - ".date("Y-m-d H:i:s");
        $this->assignconfig("defaultTime",$defaultTime);
        return $this->view->fetch();
    }

    public function del($ids = null)
    {
        if(!$ids) $this->error("删除失败");
        $this_one = $this->model->where("WR_Num",$ids)->find();
        if($this_one["Auditor"]) $this->error("已审核，不能进行删除！");
        $one = $this->model->alias("wr")->join(["workreceivedetail"=>"wrd"],"wr.WR_Num = wrd.WR_Num")->join(["storeoutdetail"=>"sod"],"wrd.WRD_ID=sod.WRD_ID")->where("wr.WR_Num",$ids)->find();
        if($one) $this->error("已有部分出库，不能进行删除！");
        $count = 0;
        Db::startTrans();
        try {
            $count += $this->model->where("WR_Num",$ids)->delete();
            $count += $this->detailModel->where("WR_Num",$ids)->delete();
            // pri($count,1);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if($count) $this->success("删除成功");
        else $this->error("删除失败");
    }

    public function organizeMaterials()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"有误，重试"]);
        $col = 0;

        $rows_sh = $this->getRowsSh();
        $spec_str = $this->getMinSpec();
        $join_list = [
            "ptd.PTD_Specification = im.IM_Spec",
            "dd.DtMD_sStuff = im.IM_Class"
        ];
        $ex = (new UnionProduceTaskView())->where("PT_Num",$num)->value("sect_field");
        $params = (new ProduceTaskDetail([],$ex))->alias("ptd")
            ->join([$ex."dtmaterialdetial"=>"dd"],"ptd.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->join(["inventorymaterial"=>"im"],$join_list,"left")
            ->field("im.IM_Num,im.IM_PerWeight,dd.DtMD_sStuff as IM_Class,ptd.PTD_Specification as IM_Spec,ptd.PTD_Material as L_Name,'kg' as WRD_Unit,0 as WRD_PlanCount,sum(ptd.PTD_sumWeight) as WRD_PlanWeight,0 as WRD_RealCount,sum(ptd.PTD_sumWeight) as WRD_RealWeight,
            case dd.DtMD_sStuff when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when dd.DtMD_sStuff='角钢' then 
            SUBSTR(REPLACE(ptd.PTD_Specification,'∠',''),1,locate('*',REPLACE(ptd.PTD_Specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(ptd.PTD_Specification,'∠',''),locate('*',REPLACE(ptd.PTD_Specification,'∠',''))+1,2)*1
            when (dd.DtMD_sStuff='钢板' or dd.DtMD_sStuff='法兰') then 
            REPLACE(ptd.PTD_Specification,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("ptd.PT_Num",$num)
            ->order("clsort ASC,L_Name ASC,bh ASC")
            ->group("dd.DtMD_sStuff,ptd.PTD_Material,ptd.PTD_Specification")
            ->select();
        $field = "";
        $tableField = $this->getDetailField();
        list($limberList,$kj_limberList) = $this->getLimber();
        foreach($params as $v){
            if($v["IM_Class"]=="角钢"){
                preg_match_all("/\d+/",$v["IM_Spec"],$matches);
                $matches = $matches[0];
                if($matches[0]>$spec_str[0]) continue;
                else if($matches[0]==$spec_str[0] and $matches[1]>$spec_str[1]) continue;
            }
            $col += 1;
            $field .= '<tr><td>'.$col.'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
            foreach($tableField as $vv){
                if($vv[1]=="D_L_Name"){
                    $field .= "<td ".$vv[3].">";
                    $field .= '<input type="text" class="small_input" list="typelist" name="table_row['.$vv[1].'][]" value="" placeholder="请选择"><datalist id="typelist">';
                    foreach($limberList as $vvv){
                        $field .= '<option value="'.$vvv.'">'.$vvv.'</option>';
                    }
                    $field .= "</datalist></td>";
                }else{
                    if($vv[1]=="WRD_PlanWeight" or $vv[1]=="WRD_RealWeight") $value = round($v[$vv[1]]*($rows_sh[$v["IM_Class"]]??1));
                    else if($vv[1]=="WRD_PlanTime") $value = date("Y-m-d H:i:s");
                    else if($vv[1]=="sh_rate") $value = $rows_sh[$v["IM_Class"]]??1;
                    else $value = $v[$vv[1]]??"";
                    $field .= '<td '.$vv[3].'><input type="text" class="small_input" '.$vv[2].' name="table_row['.$vv[1].'][]" value="'.$value.'"></td>';
                }
            }
            $field .= "</tr>";
        }
        return json(["code"=>1,"data"=>$field]);

    }

    public function getDetailField(){
        $tableField = [
            ["WRD_ID","WRD_ID","readonly","hidden","40","save"],
            ["编号","IM_Num","readonly","hidden","50",""],
            ["比重","IM_PerWeight","readonly","hidden","40",""],
            ["损耗","sh_rate","readonly","hidden","40",""],
            ["名称","IM_Class","readonly","","40",""],
            ["规格","IM_Spec","readonly","","80",""],
            ["代领规格","D_IM_Spec","","","80",""],
            ["代领编号","D_IM_Num","","hidden","80",""],
            ["材质","L_Name","readonly","","55",""],
            ["代领材质","D_L_Name","","","55",""],
            ["长度(m)","WRD_Length","","","50","save"],
            ["宽度(m)","WRD_Width","","","50","save"],
            ["单位","WRD_Unit","","","30","save"],
            ["计划数量","WRD_PlanCount","readonly","","60",""],
            ["计划重量(kg)","WRD_PlanWeight","readonly","","88",""],
            ["实领数量","WRD_RealCount","","","60","save"],
            ["实领重量(kg)","WRD_RealWeight","data-rule='required range(0~)'","","88","save"],
            ["库存数量","SS_Count","readonly","","60",""],
            ["库存重量(kg)","SS_Weight","readonly","","88",""],
            ["计划用料时间","WRD_PlanTime","","","90","save"],
            // ["机台","WRD_JiTai","text","between","","40","checked"],
            ["区域","WRD_Place","","","40","save"],
            // ["进货价(元/吨)","WRD_Price","","","90","save"],
            // ["金额(元)","WRD_Money","readonly","","60","save"],
            ["备注","WRD_Memo","","","60","save"]
        ];
        return $tableField;
    }

    public function getRowsSh(){
        $where_list = ["loss_rate_JG"=>"角钢","loss_rate_GB"=>"钢板","loss_rate_yg"=>"圆钢","loss_rate_lwg"=>"螺纹钢","loss_rate_wfg"=>"无缝管","loss_rate_qt"=>"其他"];
        $sh_list = (new Configuresys())->field("SetName,SetValue")->where("SetName","IN",array_keys($where_list))->select();
        $rows_sh = [];
        foreach($sh_list as $v){
            $rows_sh[$where_list[$v["SetName"]]] = $v["SetValue"];
        }
        return $rows_sh;
    }

    public function getMinSpec()
    {
        $arr = [[0,0]];
        $sh_list = (new Configuresys())->field("SetName,SetValue")->where("SetName","work_min_spec")->find();
        if($sh_list){
            preg_match_all("/\d+/",$sh_list["SetValue"],$arr);
        }
        return $arr[0];
    }

    public function a4Print($ids=null,$type=0)
    {
        $row = $this->model->alias("wr")
            ->join(["taskdetail"=>"td"],"td.TD_ID = wr.TD_ID","LEFT")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num","LEFT")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num","LEFT")
            ->field("wr.WR_Num,wr.WR_WareHouse,wr.TD_ID,t.t_project,c.PC_Num,t.T_Num,wr.TD_TypeName,wr.WR_Date,wr.WR_Person,wr.WR_Dept,wr.PT_Num,wr.WR_BuyDate,wr.WR_Lu,wr.WR_Pi,wr.WR_BuyPi,wr.WR_ZB,wr.WR_HY,wr.V_Name,wr.WR_Memo,wr.WR_Send,wr.Writer,wr.WriteDate,wr.Auditor,wr.AudiDate, wr.WR_SY")
            ->where("wr.WR_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $row = $row->toArray();
        $row["WR_Date"] = date_format(date_create($row["WR_Date"]), "Y-m-d");
        $detailWhere = [
            "d.IM_Num = ss.IM_Num",
            "d.L_Name = ss.L_Name",
            "d.IM_Spec = ss.IM_Spec",
            "d.WRD_Length = ss.SS_Length",
            "d.WRD_Width = ss.SS_Width",
            "ss.SS_WareHouse = '".$row["WR_WareHouse"]."'"
        ];
        $produceWhere = [];
        if($type) $produceWhere = [
            "PT_Num" => ["=",$row["PT_Num"]]
        ];
        $detailList = $this->detailModel->getDetailData($ids,$produceWhere,$detailWhere);
        $this->assignconfig("detailList", $detailList);
        $this->assignconfig("mainInfos", $row);
        return $this->view->fetch();
    }

    public function a4PrintSecond($ids=null)
    {
        return $this->a4Print($ids,1);
    }

    public function a5Print($ids=null)
    {
        $row = $this->model->alias("wr")
            ->join(["taskdetail"=>"td"],"td.TD_ID = wr.TD_ID","LEFT")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num","LEFT")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num","LEFT")
            ->field("wr.Approver,wr.AppDate,wr.WR_Num,wr.WR_WareHouse,wr.TD_ID,t.t_project,c.PC_Num,t.T_Num,wr.TD_TypeName,wr.WR_Date,wr.WR_Person,wr.WR_Dept,
            wr.PT_Num,wr.WR_BuyDate,wr.WR_Lu,wr.WR_Pi,wr.WR_BuyPi,wr.WR_ZB,wr.WR_HY,wr.V_Name,wr.WR_Memo,wr.WR_Send,wr.Writer,wr.WriteDate,wr.Auditor,wr.AudiDate")
            ->where("wr.WR_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        $rows_sh = $this->getRowsSh();

        $join_con = [
            "d.IM_Num = ss.IM_Num",
            "d.L_Name = ss.L_Name",
            "d.IM_Spec = ss.IM_Spec",
            "d.WRD_Length = ss.SS_Length",
            "d.WRD_Width = ss.SS_Width",
            "ss.SS_WareHouse = '".$row["WR_WareHouse"]."'"
        ];
        $detailList = $this->detailModel->alias("d")
            ->join(["inventorymaterial"=>"im"],"d.IM_Num = im.IM_Num","left")
            ->join(["storestate"=>"ss"],$join_con,"left")
            ->field("d.WRD_ID,d.IM_Num,im.IM_PerWeight,d.IM_Class,d.IM_Spec,D_IM_Spec,d.L_Name,
            (CASE WHEN d.D_L_Name='' THEN d.L_Name ELSE d.D_L_Name END) AS D_L_Name,
            REPLACE(d.L_Name,'Q','') as dlnum,
            case d.IM_Class when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when d.IM_Class='角钢' then 
            SUBSTR(REPLACE(d.IM_Spec,'∠',''),1,locate('*',REPLACE(d.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(d.IM_Spec,'∠',''),locate('*',REPLACE(d.IM_Spec,'∠',''))+1,2)*1
            when (d.IM_Class='钢板' or d.IM_Class='法兰') then 
            REPLACE(d.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh, 
            round(d.WRD_Length/1000,3) as WRD_Length,
            round(d.WRD_Width/1000,3) as WRD_Width,
            d.WRD_Unit,d.WRD_PlanCount,d.WRD_PlanWeight,d.WRD_RealCount,d.WRD_RealWeight,d.WRD_PlanTime,d.WRD_Place,d.WRD_Price,d.WRD_Memo,ss.SS_Count,ss.SS_Weight")
            ->where("d.WR_Num",$ids)
            ->group("d.WRD_ID")
            ->order("clsort, bh, dlnum, IM_Num ASC")
            ->select();
        $detail_arr = [];
        foreach($detailList as $k=>$v){
            $v["WRD_Length"] = round($v["WRD_Length"],3);
            $v["WRD_Width"] = round($v["WRD_Width"],2);
            $v["WRD_PlanCount"] = round($v["WRD_PlanCount"],2);
            $v["WRD_PlanWeight"] = round($v["WRD_PlanWeight"],2);
            $v["WRD_RealCount"] = round($v["WRD_RealCount"],2);
            $v["WRD_RealWeight"] = round($v["WRD_RealWeight"],2);
            $detail_arr[$k] = $v->toArray();
            $detail_arr[$k]["sh_rate"] = $rows_sh[$v["IM_Class"]]??1;
        }

        $limberList = $this->limberList();
        $this->view->assign("row", $row);
        $this->view->assign("wareclassList",$this->wareclassList());
        
        $mainInfos = [];
        $mainInfos["t_project"] = $row["t_project"];
        $mainInfos["TD_TypeName"] = $row["TD_TypeName"];
        $mainInfos["PT_Num"] = $row["PT_Num"];
        $mainInfos["WR_Num"] = $row["WR_Num"];
        $mainInfos["WR_WareHouse"] = $row["WR_WareHouse"];
        $mainInfos["WR_Dept"] = $row["WR_Dept"];
        $mainInfos["T_Num"] = $row["T_Num"];
        $mainInfos["WR_Memo"] = $row["WR_Memo"];
        $mainInfos["Writer"] = $row["Writer"];
        $mainInfos["WriteDate"] = date_format(date_create($row["WriteDate"]), "Y-m-d");
        $mainInfos["Auditor"] = $row["Auditor"];
        $mainInfos["AudiDate"] = date_format(date_create($row["AudiDate"]), "Y-m-d");
        $mainInfos["Approver"] = $row["Approver"];
        $mainInfos["AppDate"] = date_format(date_create($row["AppDate"]), "Y-m-d");
        $mainInfos["WR_Person"] = $row["WR_Person"];
        
        $date=date_create($row["WR_Date"]);
        $mainInfos["WR_Date"] = date_format($date, "Y-m-d");
        $this->assignconfig("limberList", $limberList);
        $this->assignconfig("detailList", $detailList);
        $this->assignconfig("mainInfos", $mainInfos);
        return $this->view->fetch();
    }

    public function materialSub()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"生成失败！请稍后重试！"]);
        $one = $this->model->get($num);
        if(!$one) return json(["code"=>0,"msg"=>"生成失败！请稍后重试！"]);
        $PT_Num = $one["PT_Num"];

        $material_replace_detail_model = new MaterialReplaceDetail();
        $material_replace_model = new MaterialReplace();
        $mrd = $material_replace_detail_model->where("PT_Num",$PT_Num)->find();
        if($mrd) $MR_Num = $mrd["MR_Num"];
        else{
            $year = date("Y");
            $new_one = $material_replace_model->field("MR_Num")->where("MR_Num","LIKE","MR".$year."%")->order("MR_Num desc")->find();
            if($new_one){
                $xh_num = substr($new_one["MR_Num"],-4);
                $MR_Num = "MR".$year.(str_pad((++$xh_num),4,0,STR_PAD_LEFT));
            }else $MR_Num = "MR".$year."0001";
        }

        $list = $this->detailModel->where("WR_Num",$num)->select();
        $saveList = [];
        foreach($list as $k=>$v){
            if(($v["IM_Num"]!=$v["D_IM_Num"]) or ($v["L_Name"]!=$v["D_L_Name"]) or ($v["IM_Spec"]!=$v["D_IM_Spec"])){
                $saveList[] = [
                    "MR_Num" => $MR_Num,
                    "OldLimber" => $v["L_Name"],
                    "OldType" => $v["IM_Spec"],
                    "NewLimber" => $v["D_L_Name"],
                    "NewType" => $v["D_IM_Spec"],
                    "OldWeight" => $v["WRD_PlanWeight"],
                    "NewWeight" => $v["WRD_RealWeight"],
                    "IM_Class" => $v["IM_Class"],
                    "OldLength" => $v["WRD_Length"],
                    "OldWidth" => $v["WRD_Width"],
                    "OldCount" => $v["WRD_PlanCount"],
                    "NewLength" => $v["WRD_Length"],
                    "NewWidth" => $v["WRD_Width"],
                    "NewCount" => $v["WRD_RealCount"],
                    "MRD_TD_ID" => $one["TD_ID"],
                    "NewClass" => $v["IM_Class"],
                    "PT_Num" => $PT_Num
                ];
            }
        }
        $material_replace_one = [
            "MR_Num" => $MR_Num,
            "TD_ID" => $one["TD_ID"],
            "Writer" => $this->admin["nickname"]
        ];
        if(!empty($saveList)){
            $result = false;
            Db::startTrans();
            try {
                if($mrd) $result = $material_replace_model->save($material_replace_one,["MR_Num"=>$MR_Num]);
                else $result = $material_replace_model->save($material_replace_one);
                $material_replace_detail_model->where("PT_Num",$PT_Num)->delete();
                $result = $material_replace_detail_model->allowField(true)->saveAll($saveList,false);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
            if($result) return json(["code"=>1,"data"=>$MR_Num]);
            else return json(["code"=>0,"msg"=>"没有需要生成的代料"]);
        }else{
            return json(["code"=>0,"msg"=>"没有需要生成的代料"]);
        }
        
    }

    public function getWeight()
    {
        $IM_Spec = $this->request->post("IM_Spec");
        $IM_Class = $this->request->post("IM_Class");
        if(!$IM_Class or !$IM_Spec) return json(["code"=>0,"msg"=>"代领材料无法匹配"]);
        $where = [
            "IM_Spec"=>["=",$IM_Spec],
            "IM_Class"=>["=",$IM_Class]
        ];
        $one = (new InventoryMaterial())->field("IM_PerWeight")->where($where)->find();
        if($one) return json(["code"=>1,"data"=>$one["IM_PerWeight"]]);
        else return json(["code"=>0,"msg"=>"代领材料无法匹配"]);
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $result = $this->model->where("WR_Num",$num)->update(['WR_Num' => $num,'Auditor'=>$admin,"AudiDate"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $result = $this->model->where("WR_Num",$num)->update(['WR_Num' => $num,'Auditor'=>"","AudiDate"=>"0000-00-00 00:00:00"]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }
}
