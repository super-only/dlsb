<?php

namespace app\admin\controller\chain\material;

use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\model\chain\lofting\ProduceTask;
use app\admin\model\chain\lofting\ProduceTaskDetail;
use app\admin\model\chain\material\PcScheduling;
use app\admin\model\chain\material\PcSchedulingSect;
use app\admin\model\chain\sale\TaskDetail;
use app\admin\model\chain\supplement\TaskReNew;
use app\admin\model\chain\supplement\TaskReNewMain;
use app\admin\model\jichu\ch\InventoryMaterial;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 套料
 *
 * @icon fa fa-circle-o
 */
class Nesting extends Backend
{
    
    /**
     * Nesting模型对象
     * @var \app\admin\model\chain\material\Nesting
     */
    protected $model = null;
    protected $noNeedLogin = ["xddh"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\Nesting;
        $this->detailModel = new \app\admin\model\chain\material\NestingSub;
        $wareList = [];
        $wareModel = (new \app\admin\model\jichu\ch\WareClass())->field("WC_Name")->where("WC_Sort","原材料")->order("WC_Num")->select();
        foreach($wareModel as $v){
            $wareList[$v["WC_Name"]] = $v["WC_Name"];
        }
        $this->view->assign("wareList",$wareList);
        $this->assignconfig("wareList",$wareList);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("n")
                ->join(["taskdetail"=>"td"],"td.TD_ID=n.TD_ID")
                ->field("n.*,td.TD_TypeName")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                if($params["trnm_num"]) $params["sup"] = 1;
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model::create($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$result["id"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $td_one = (new TaskDetail())->get($row["td_id"]);
        $row["tower_name"] = $td_one?$td_one['TD_TypeName']:"";
        if($row["auditor"]!="" and $row["pc_flag"]==0) $row['qs_flag']=1;
        else $row["qs_flag"]=0;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $tableList = $this->detailModel->where("n_id",$ids)->order("id")->select();
        if($tableList) $tableList = collection($tableList)->toArray();
        else $tableList = [];
        $field = $this->getTableField();
        if ($this->request->isPost()) {
            if($row["auditor"]) $this->error("已审核，修改失败");
            $post_data = $this->request->post("data");
            $post_data = $post_data?json_decode($post_data,true):[];
            $params = [];
            $paramsTable = [];
            $replace_list = ["table_row[","][]"];
            foreach($post_data as $k=>$v){
                if(substr($v["name"],0,4) == "row["){
                    $key = rtrim(ltrim(($v["name"]),"row["),"]");
                    $params[$key] = trim($v["value"]);
                }else{
                    foreach($replace_list as $vv){
                        $v["name"] = str_replace($vv,'',$v["name"]);
                    }
                    $key = $v["name"];
                    $paramsTable[$key][] = trim($v["value"]);
                }
            }
            if ($params) {
                $params = $this->preExcludeFields($params);
                if(!empty($paramsTable)){
                    $saveDetail = [];
                    $biZhong = (new InventoryMaterial())->getIMPerWeight();
                    foreach($paramsTable["id"] as $k=>$v){
                        if(!$paramsTable["im_num"][$k] or !$paramsTable["count"][$k]) continue;
                        $DtMD_fUnitWeight = 0;
                        $DtMD_sStuff=$paramsTable["stuff"][$k];

                        $length = $paramsTable["length"][$k]?$paramsTable["length"][$k]*0.001:0;
                        $width = $paramsTable["width"][$k]?$paramsTable["width"][$k]*0.001:0;
                        $area = $DtMD_sStuff!='钢板'?$length:$length*$width*abs($paramsTable["stuff"][$k]);
                        if($DtMD_sStuff=="圆钢"){
                            preg_match_all("/\d+\.?\d*/",$paramsTable["specification"][$k],$matches);
                            $L = isset($matches[0][0])?$matches[0][0]:0;
                            $DtMD_fUnitWeight = round($L*$L*$length*0.00617,2);
                        }else if($DtMD_sStuff=="钢管"){
                            preg_match_all("/\d+\.?\d*/",$paramsTable["specification"][$k],$matches);
                            $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                            $r = isset($matches[0][1])?$matches[0][1]/2*0.001:0;
                            if(strpos($paramsTable["specification"][$k],"*")) $r = $R-(2*$r);
                            $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
                        }else if($DtMD_sStuff=="格栅板"){
                            $rou = ["G255/40/100W"=>32.1,"G253/40/100W"=>21.3];
                            $rou_one = isset($rou[$paramsTable["specification"][$k]])?$rou[$paramsTable["specification"][$k]]:0;
                            $DtMD_fUnitWeight = round($rou_one * $length*$width,4);
                        }else{
                            $DtMD_fUnitWeight = (isset($biZhong[$DtMD_sStuff][$paramsTable["specification"][$k]]))?round($biZhong[$DtMD_sStuff][$paramsTable["specification"][$k]] * $area,4):0;
                        }
                        $saveDetail[$k] = [
                            "id" => $v,
                            "n_id" => $ids,
                            "im_num" => $paramsTable["im_num"][$k],
                            "stuff" => $DtMD_sStuff,
                            "material" => $paramsTable["material"][$k],
                            "specification" => $paramsTable["specification"][$k],
                            "length" => $paramsTable["length"][$k]??0,
                            "width" => $paramsTable["width"][$k]??0,
                            "count" => $paramsTable["count"][$k]??0,
                            "weight" => $DtMD_fUnitWeight,
                            "mate" => $paramsTable["mate"][$k],
                            "remark" => $paramsTable["remark"][$k],
                        ];
                        if(!$v) unset($saveDetail[$k]["id"]);
                    }
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    if(!empty($saveDetail)) $this->detailModel->allowField(true)->saveAll($saveDetail);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        list($limberList,$kj_limberList) = $this->getLimber();
        $this->assignconfig("limber_list",$limberList);
        $this->view->assign("field",$field);
        $this->view->assign("list",$tableList);
        $this->assignconfig("field",$field);
        $this->view->assign("row", $row);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function xddh()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new ProduceTask())->alias("p")
                ->join(["taskdetail"=>"d"],"p.TD_ID = d.TD_ID")
                ->join(["task"=>"t"],"t.T_Num=d.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->field("p.*,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num',c.C_Num,c.C_Num as 'c.C_Num',c.PC_Num,c.PC_Num as 'c.PC_Num',d.TD_Pressure")
                ->where($where)
                ->order("p.WriterDate desc,p.PT_Num desc")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function bjdh($td_id = null)
    {
        $this->assignconfig("td_id",$td_id);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $dbSql = (new TaskReNewMain())->alias("trm")
                ->join(["mergtypename"=>"mtn"],"trm.TD_ID=mtn.mTD_ID","left")
                ->field("trm.*")
                ->where("trm.TD_ID|mtn.mTD_ID","=",$td_id)
                ->group("trm.TRNM_Num")
                ->buildSql();
            $list = (new TaskDetail())->alias("d")
                ->join(["task"=>"t"],"t.T_Num=d.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->join([$dbSql=>"trm"],"d.TD_ID = trm.TD_ID")
                ->field("trm.*,c.C_Project,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num',c.C_Num,c.C_Num as 'c.C_Num',c.PC_Num,c.PC_Num as 'c.PC_Num',d.TD_Pressure")
                ->where($where)
                ->order("trm.TRNM_WriteDate desc,trm.TRNM_Num desc")
                ->paginate($limit);

            // $list = DB::query($dbSql)->alias("trm")
            //     // ->join(["mergtypename"=>"mtn"],"trm.TD_ID=mtn.mTD_ID","left")
            //     ->join(["taskdetail"=>"d"],"d.TD_ID = trm.TD_ID")
            //     ->join(["task"=>"t"],"t.T_Num=trm.T_Num")
            //     ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            //     ->field("trm.*,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num',c.C_Num,c.C_Num as 'c.C_Num',c.PC_Num,c.PC_Num as 'c.PC_Num',d.TD_Pressure")
            //     ->where($where)
            //     ->order("trm.TRNM_WriteDate desc,trm.TRNM_Num desc")
            //     ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function generMaterial($ids = null)
    {
        $this->assignconfig("ids",$ids);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            $result = array("total" => 0, "rows" => []);
            $row = $this->model->get($ids);
            if(!$row) return json($result);
            $join_list = [
                "dtmd.DtMD_sSpecification = im.IM_Spec",
                "dtmd.DtMD_sStuff = im.IM_Class"
            ];
            // if($row["sup"]){
            //     $partList = (new TaskReNew())->where("TRNM_Num","=",$row["trnm_num"])->column("TRN_TPNum");
            //     $dbSql = (new Dtmaterial())->alias("dtm")
            //         ->join(["dtsect"=>"dts"],["dtm.DtM_iID_PK = dts.DtM_iID_FK"])
            //         ->join(["dtmaterialdetial"=>"dtmd"],["dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK"])
            //         ->where("dtmd.DtMD_sPartsID","in",$partList)
            //         ->where("dtm.TD_ID","=",$row["td_id"])
            //         ->group("DtMD_sStuff,DtMD_sSpecification")
            //         ->buildSql();
            //     $list = (new InventoryMaterial())->alias("im")
            //         ->join([$dbSql=>"dtmd"],$join_list)
            //         ->field("im.*")
            //         ->group("im.IM_Num")
            //         ->select();
                // $list = (new Dtmaterial())->alias("dtm")
                // ->join(["dtsect"=>"dts"],["dtm.DtM_iID_PK = dts.DtM_iID_FK"])
                // ->join(["dtmaterialdetial"=>"dtmd"],["dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK"])
                // ->join(["inventorymaterial"=>"im"],$join_list)
                // ->field("im.*")
                // ->where("dtmd.DtMD_sPartsID","in",$partList)
                // ->where("dtm.TD_ID","=",$row["td_id"])
                // ->group("im.IM_Num")
                // ->select(false);
                // pri($list,1);

                // $list = (new TaskReNew())->alias("trn")
                // ->join(["dtmaterial"=>"dtm"],"trn.TD_ID=dtm.TD_ID")
                // ->join(["dtsect"=>"dts"],["dtm.DtM_iID_PK = dts.DtM_iID_FK","trn.DtS_Name = dts.DtS_Name"])
                // ->join(["dtmaterialdetial"=>"dtmd"],["dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK","dtmd.DtMD_sPartsID = trn.TRN_TPNum"])
                // ->join(["inventorymaterial"=>"im"],$join_list,"left")
                // ->field("im.IM_Num,trn.TD_ID,trn.TRNM_Num,trn.TRN_ID,trn.DtS_Name,CAST(trn.DtS_Name AS UNSIGNED) AS number_1,trn.TRN_Project,trn.TRN_TPNum,CAST(trn.TRN_TPNum AS UNSIGNED) AS number_2,dtmd.DtMD_sStuff,DtMD_sMaterial,DtMD_sSpecification")
                // ->where("trn.TRNM_Num","=",$row["trnm_num"])
                // ->order("trn.TD_ID,trn.TRNM_Num,number_1,number_2")
                // ->group("DtMD_sMaterial,DtMD_sSpecification")
                // ->select(false);
                // pri($list,1);
            // }else{
                $list = (new ProduceTaskDetail())->alias("ptd")
                    ->join(["dtmaterialdetial"=>"dtmd"],["ptd.DtMD_ID_PK = dtmd.DtMD_ID_PK"])
                    ->join(["inventorymaterial"=>"im"],$join_list)
                    ->field("im.*")
                    ->where("ptd.PT_Num","=",$row["pt_num"])
                    // ->order("trn.TD_ID,trn.TRNM_Num,number_1,number_2")
                    ->group("im.IM_Num")
                    ->select();
            // }
            $result["total"] = count($list);
            $result["rows"] = $list?collection($list)->toArray():[];
            return json($result);
        }
        return $this->view->fetch();
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        $return_news = ["code"=>0,'msg'=>"删除失败"];
        if($num){
            $mn_one = $this->model->alias("m")->join(["nesting_sub"=>"ns"],"m.id = ns.n_id")->where("ns.id",$num)->find();
            if($mn_one["auditor"]) return json(["code"=>0,'msg'=>"已审核，不能进行删除！"]);
            $result = $this->detailModel->where("id",$num)->delete();
            if($result){
                $return_news["code"] = 1;
                $return_news["msg"] = "删除成功";
            }
        }
        return $return_news;
    }

    public function schedulingRelease()
    {
        $ids = $this->request->post("ids");
        $nesting_one = $this->model->where("id",$ids)->find();
        $pt_num = $nesting_one["pt_num"];
        $td_id = $nesting_one["td_id"];
        $list = $this->detailModel
            ->field("id as P_ID,'".$pt_num."' AS PtNum,stuff as IMName,specification as IMStd,material as IMMaterial,length as IMLength,count as Num,mate")
            ->where("n_id",$ids)
            ->order("id")
            ->select();
        if($list) $list = collection($list)->toArray();
        else $list = [];
        $scheduling_list = [];
        $main_list = [];
        $sect_list = [];
        foreach($list as $k=>$v){
            $v["pc_flag"] = 1;
            $main_list[$v["P_ID"]] = $v;
            $sect_list[$v["P_ID"]] = $this->_getMateData($v["mate"]);
        }
        $data = ["code"=>0,"msg"=>"生成排产失败"];
        if(!empty($main_list) and !empty($sect_list)){
            $scheduling_result = false;
            Db::startTrans();
            try {
                $result = $this->model->where("id",$ids)->update(["pc_flag"=>1]);
                $detail_result = $this->detailModel->where("n_id",$ids)->update(["pc_flag"=>1]);
                $main_result = (new PcScheduling())->allowField(true)->saveAll($main_list);
                foreach($sect_list as $sk=>$sv){
                    foreach($sv as $v){
                        $scheduling_list[] = array_merge($v,[
                            "MID" => $main_result[$sk]["ID"],
                            "PartNCPath"=> $td_id.'-'.$v["PartName"],
                            "QTY" => $v["SingleQTY"]*$main_result[$sk]["Num"]
                        ]);
                    }
                }
                $scheduling_result = (new PcSchedulingSect())->allowField(true)->saveAll($scheduling_list);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($scheduling_result !== false) {
                $data["code"]=1;
                $data["msg"] = "生成排产成功";
            }
        }
        return json($data);
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $result = $this->model->save(['auditor'=>$admin,"auditor_time"=>date("Y-m-d H:i:s")],["id"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if($this->model->where("id",$num)->value("pc_flag")) return json(["code"=>0,"msg"=>"弃审失败"]);
        $result = $this->model->update(['auditor'=>"","auditor_time"=>"0000-00-00 00:00:00"],["id"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"弃审成功"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();

            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    if($v["auditor"]==""){
                        $count += $v->delete();
                        $this->detailModel->where("n_id",$v[$pk])->delete();
                    }
                }
                
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["ID","id","text","readonly","",40],
            ["存货编号","im_num","text","readonly data-rule='required' ","",80],
            ["材料名称","stuff","text","readonly","",80],
            ["材质","material","select"," data-rule='required' ","",80],
            ["规格","specification","text","readonly","",80],
            ["长度(mm)","length","text","",0,40],
            ["宽度(mm)","width","text","",0,40],
            ["数量","count","text"," data-rule='required' ",0,40],
            ["重量(kg)","weight","text","readonly","",40],
            ["匹配结果(部件号/长度*数量)","mate","text","","",150],
            ["备注","remark","text","","",80],
        ];
        return $list;
    }

    protected function _getMateData($string='')
    {
        if(!$string) return [];
        $arr = explode("+",$string);
        foreach($arr as $k=>$v){
            if(strstr($v,"*")===false) $v .= "*1";
            $arr[$k] = $v;
        }
        $string = implode("+",$arr);
        preg_match_all("/\d+/",$string,$matches);
        $sect_list = array_chunk($matches[0],3);
        $son_list = [];
        foreach($sect_list as $v){
            $son_list[] = ["PartName"=>$v[0],"PartLength"=>$v[1],"SingleQTY"=>$v[2]];
        }
        return $son_list;
    }
}
