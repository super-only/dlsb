<?php

namespace app\admin\controller\chain\material;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 计划排产
 *
 * @icon fa fa-circle-o
 */
class GwPcScheduling extends Backend
{
    
    /**
     * GwPcScheduling模型对象
     * @var \app\admin\model\chain\material\GwPcScheduling
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\GwPcScheduling;
        $this->detailModel = new \app\admin\model\chain\material\GwPcSchedulingDetail;
        $this->admin = \think\Session::get('admin');
        $this->view->assign("admin",$this->admin["username"]);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            [$params,$xl,$zg,$zz,$hj,$dx,$zw] = array_values($this->request->post());
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                //g_pc_num GPC20221205001
                $g_pc_num = "";
                $one = $this->model->where("g_pc_num","LIKE","GPC".date("Ymd")."%")->order("g_pc_num desc")->find();
                if($one){
                    $num = substr($one["g_pc_num"],-3);
                    $num++;
                    $g_pc_num = "GPC".date("Ymd").(str_pad(($num),3,0,STR_PAD_LEFT));
                }else $g_pc_num = "GPC".date("Ymd")."001";
                $params["g_pc_num"] = $g_pc_num;
                $detailList = [];
                foreach([$xl,$zg,$zz,$hj,$dx,$zw] as $v){
                    if($v["choose"]==1){
                        unset($v["d_id"],$v["choose"]);
                        $v["g_pc_num"] = $g_pc_num;
                        $detailList[] = $v;
                    }
                }
                if(empty($detailList)) $this->error("没有工序");
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    $this->detailModel->allowField(true)->saveAll($detailList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        // $xl TTGX01,$zg TTGX03,$zz TTGX04,$hj TTGX05,$dx TTGX06,$zw TTGX07
        $xl = $zg = $zz = $hj = $dx = $zw = [];
        $list = $this->detailModel->where("g_pc_num",$ids)->order("processCode")->select();
        foreach($list as $k=>$v){
            $v = $v->toArray();
            $v["choose"] = 1;
            if($v["processCode"]=="TTGX01") $xl = $v;
            if($v["processCode"]=="TTGX03") $zg = $v;
            if($v["processCode"]=="TTGX04") $zz = $v;
            if($v["processCode"]=="TTGX05") $hj = $v;
            if($v["processCode"]=="TTGX06") $dx = $v;
            if($v["processCode"]=="TTGX07") $zw = $v;
        }
        if ($this->request->isPost()) {
            [$params,$xl,$zg,$zz,$hj,$dx,$zw] = array_values($this->request->post());
            if ($params) {
                $params = $this->preExcludeFields($params);
                $detailList = $delList = [];
                foreach([$xl,$zg,$zz,$hj,$dx,$zw] as $v){
                    if($v["choose"]==1){
                        unset($v["choose"]);
                        if(!$v["d_id"]) unset($v["d_id"]);
                        $v["g_pc_num"] = $ids;
                        $detailList[] = $v;
                    }else if($v["d_id"]){
                        $delList[$v["d_id"]] = $v["d_id"];
                    }
                }
                if(empty($detailList)) $this->error("没有工序");
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    $this->detailModel->where("d_id","IN",array_values($delList))->delete();
                    $this->detailModel->allowField(true)->saveAll($detailList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $assignList = [
            "row" => $row,
            "xl" => $xl,
            "zg" => $zg,
            "zz" => $zz,
            "hj" => $hj,
            "dx" => $dx,
            "zw" => $zw,
        ];
        $this->view->assign($assignList);
        return $this->view->fetch();
    }

}
