<?php

namespace app\admin\controller\chain\material;

use app\admin\controller\TouchNew;
use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\model\chain\lofting\ProduceTask;
use app\admin\model\chain\lofting\ProduceTaskDetail;
use app\admin\model\chain\lofting\UnionProduceTaskView;
use app\admin\model\chain\lofting\UnionTaskReNewMainView;
use app\admin\model\chain\material\PcNcfile;
use app\admin\model\chain\material\PcScheduling;
use app\admin\model\chain\material\PcSchedulingSect;
use app\admin\model\chain\material\PcSchedulingSectView;
use app\admin\model\chain\material\StoreState;
use app\admin\model\chain\sale\TaskDetail;
use app\admin\model\chain\sequence\Sequence;
use app\admin\model\chain\supplement\TaskReNew;
use app\admin\model\chain\supplement\TaskReNewMain;
use app\admin\model\jichu\ch\InventoryMaterial;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 套料
 *
 * @icon fa fa-circle-o
 */
class Nesting extends Backend
{
    
    /**
     * Nesting模型对象
     * @var \app\admin\model\chain\material\Nesting
     */
    protected $model = null,$detailModel,$orderList,$admin;
    // protected $noNeedLogin = ["xddh","print","zlprint"];
    protected $noNeedLogin = ['*'];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\Nesting;
        $this->detailModel = new \app\admin\model\chain\material\NestingSub;
        $wareList = [];
        $wareModel = (new \app\admin\model\jichu\ch\WareClass())->field("WC_Name")->where("WC_Sort","原材料")->order("WC_Num")->select();
        foreach($wareModel as $v){
            $wareList[$v["WC_Name"]] = $v["WC_Name"];
        }
        $this->orderList = $this->_orderList();
        $this->view->assign("orderList",$this->orderList);
        $this->view->assign("wareList",$wareList);
        $this->assignconfig("wareList",$wareList);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("n")
                ->join(["taskdetail"=>"td"],"td.TD_ID=n.TD_ID")
                ->field("n.*,td.TD_TypeName")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                if($params["trnm_num"]) $params["sup"] = 1;
                else $params["sup"] = 0;
                $getOne = $this->model->where([
                    "pt_num" => ["=",$params["pt_num"]],
                    "sup" => ["=",$params["sup"]],
                    "trnm_num" => ["=",$params["trnm_num"]],
                    "orderId" => ["=",$params["orderId"]]
                ])->find();
                if($getOne) $this->error("该生产下达单号的对应工序已经生成过排产！");
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model::create($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$result["id"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $ex = (new UnionProduceTaskView())->where("PT_Num",$row["pt_num"])->value("sect_field");
        $td_one = (new TaskDetail([],$ex))->get($row["td_id"]);
        $row["tower_name"] = $td_one?$td_one['TD_TypeName']:"";
        if($row["auditor"]!="" and $row["pc_flag"]==0) $row['qs_flag']=1;
        else $row["qs_flag"]=0;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        // $partList = [];
        // $select_result = (new ProduceTaskDetail())->alias("ptd")
        //     ->join(["dtmaterialdetial"=>"dtmd"],["ptd.DtMD_ID_PK = dtmd.DtMD_ID_PK"])
        //     ->where("ptd.PT_Num","=",$row["pt_num"])
        //     ->column("dtmd.DtMD_sPartsID,dtmd.DtMD_sStuff as IM_Class,ptd.PTD_Material as material,ptd.PTD_Specification as specification,dtmd.DtMD_iLength as length");
        // foreach($select_result as $lv){
        //     $partList[$lv["DtMD_sPartsID"]] = [
        //         "length" => $lv["length"],
        //         "IM_Class" => $lv["IM_Class"],
        //         "material" => $lv["material"],
        //         "specification" => $lv["specification"],
        //     ];
        // }
        $field = $this->getTableField();
        if ($this->request->isPost()) {
            if($row["auditor"]) $this->error("已审核，修改失败");
            $post_data = $this->request->post("data");
            $post_data = $post_data?json_decode($post_data,true):[];
            $params = [];
            $paramsTable = [];
            foreach($post_data as $k=>$v){
                if(substr($v["name"],0,3) == "row"){
                    $key = substr($v["name"],4,strlen($v["name"])-5);
                    $params[$key] = trim($v["value"]);
                }else{
                    $key = substr($v["name"],10,strlen($v["name"])-13);
                    $paramsTable[$key][] = trim($v["value"]);
                }
            }
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $tableList = $this->detailModel->where("n_id",$ids)->order("id")->select();
        if($tableList) $tableList = collection($tableList)->toArray();
        else $tableList = [];
        $partList = [];
        $part_result = (new Dtmaterial([],$ex))->alias("dtm")
                ->join([$ex."dtsect"=>"dts"],"dtm.DtM_iID_PK = dts.DtM_iID_FK")
                ->join([$ex."dtmaterialdetial"=>"dtmd"],"dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK")
                ->where("dtm.TD_ID","=",$row["td_id"])
                ->field("DtMD_sStuff as IM_Class,DtMD_sMaterial as material,DtMD_sSpecification as specification,dtmd.DtMD_iLength as length,'' as mate,CAST(DtS_Name AS SIGNED) as name_number,DtS_Name,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
                SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
                else 0 end bjbhn,CAST(DtMD_sPartsID AS SIGNED) as number,DtMD_sPartsID")
                ->order("name_number,DtS_Name,bjbhn,number,DtMD_sPartsID")
                ->select();
        foreach($part_result as $lk=>$lv){
            $partList[$lv["DtMD_sPartsID"]] = [
                "length" => $lv["length"],
                "IM_Class" => $lv["IM_Class"],
                "material" => $lv["material"],
                "specification" => $lv["specification"],
            ];
        }
        list($limberList,$kj_limberList) = $this->getLimber();
        $this->assignconfig("limber_list",$limberList);
        $this->view->assign("field",$field);
        $this->view->assign("list",$tableList);
        $this->assignconfig("field",$field);
        $this->view->assign("row", $row);
        $this->assignconfig("ids",$ids);
        $this->assignconfig("partList",$partList);
        return $this->view->fetch();
    }

    public function editUpdate()
    {
        [$nesting_ids,$tableField] = array_values($this->request->post());
        if(!$nesting_ids or !$tableField) return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
        $table = json_decode($tableField,true);
        $row = $this->detailModel->get($nesting_ids);
        if($row["pc_flag"]) return json(["data"=>[],"code"=>0,"msg"=>"已经排产 无法修改"]);
        else{
            $table["auto"] = $table["dssh"] = 0;
            $explodeList = explode("+",$table["mate"]);
            $dsList = [];
            foreach($explodeList as $vv){
                $dsList[] = substr($vv,strrpos($vv,'*',0)+1);
            }
            if($table["stuff"]=="角钢"){
                preg_match_all("/\d+\.?\d*/",$table["specification"],$matches);
                $x = $matches[0][0]?((float)$matches[0][0]):0;
                $y = $matches[0][1]?((float)$matches[0][1]):0;
                $ds = 1+array_sum($dsList);
                $dssh = shAndWbData($x,$y,$table["material"])[0]*$ds;
                $table["dssh"] = $dssh;
            }
            $result = $this->detailModel->where("id",$nesting_ids)->update($table);
            if($result) return json(["data"=>[],"code"=>1,"msg"=>"修改成功"]);
            else return json(["data"=>[],"code"=>0,"msg"=>"未修改"]);
        }
    }

    public function editDel()
    {
        [$tableField] = array_values($this->request->post());
        if(!$tableField) return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
        $table = json_decode($tableField,true);
        $delList = [];
        foreach($table as $v){
            $delList[$v["id"]] = $v["id"];
        }
        $exitList = $this->detailModel->where([
            "id" => ["in",array_values($delList)],
            "pc_flag" => ["=",0]
        ])->column("id");
        $diff = array_diff($delList,$exitList);
        $this->detailModel->where("id","IN",$exitList)->delete();
        if(!empty($diff)) $str = "ID为".(implode(",",$diff))."的套料信息已经排产，请稍后再试";
        if(isset($str)) return json(["data"=>[],"code"=>1,"msg"=>$str]);
        else return json(["data"=>[],"code"=>1,"msg"=>"删除成功"]);
    }

    public function editPcDel()
    {
        [$tableField] = array_values($this->request->post());
        if(!$tableField) $this->error("失败，稍后重试");
        $table = json_decode($tableField,true);
        if($table) $table = $table[0];
        $schedulingSectViewModel = new PcSchedulingSectView();
        $selectView = $schedulingSectViewModel->where("P_ID",$table["id"])->column("ID,nrecord");
        foreach($selectView as $k=>$v){
            if($v>0) $this->error("机器一开始工作，无法删除");
        }
        $result = false;
        Db::startTrans();
        try {
            $result = $this->detailModel->where("id",$table["id"])->delete();
            (new PcScheduling())->where("P_ID",$table["id"])->delete();
            (new PcSchedulingSect())->where("ID","IN",array_keys($selectView))->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $this->success("删除成功");
        } else {
            $this->error(__('No rows were updated'));
        }
    }

    public function detailNoTable()
    {
        $ids = $this->request->post("ids");
        $noWhere = [];
        $l_name = $this->request->post("l_name");
        if($l_name) $noWhere["DtMD_sMaterial"] = ["=",$l_name];
        $spec = $this->request->post("spec");
        if($spec) $noWhere["DtMD_sSpecification"] = ["=",$spec];
        $row = $this->model->get($ids);
        if (!$row) {
            return json(["code"=>0,"data"=>[],"msg"=>__('No Results were found')]);
        }
        $partList = [];
        $allTableList = $this->model->alias("m")
            ->join(["nesting_sub"=>"ns"],"m.id=ns.n_id")
            ->field("ns.*,
            case stuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,
            cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where([
                "pt_num" => ["=",$row["pt_num"]],
                "sup" => ["=",$row["sup"]],
                "trnm_num" => ["=",$row["trnm_num"]]
            ])->order("clsort asc,material asc,bh asc,specification asc,length desc,pc_flag asc,id asc")
            ->select();
        $pTableList = [];
        foreach($allTableList as $k=>$v){
            if($v["n_id"]==$ids){
                $pTableList[$k] = $v->toArray();
                $ds = $dssh = $sycd = $yl = $sh = $shv = $dsNumList =  0;
                $lyv = 100;
                
                $explodeList = explode("+",$v["mate"]);
                $endActList = $dsList = [];
                foreach($explodeList as $vv){
                    $endActList[] = substr($vv,strrpos($vv,'/',0)+1,strrpos($vv,'*',0)-strrpos($vv,'/',0)-1).'*'.substr($vv,strrpos($vv,'*',0)+1);
                    $dsList[] = substr($vv,strrpos($vv,'*',0)+1);
                    $dsNumList += round((float)(substr($vv,strrpos($vv,'/',0)+1,strrpos($vv,'*',0)-strrpos($vv,'/',0)-1))*(float)(substr($vv,strrpos($vv,'*',0)+1)),2);
                }
                $pTableList[$k]["act_mate"] = implode("+",$endActList);
                $ds = 1+array_sum($dsList);
                $dssh = $v["dssh"];
                $sycd = $dsNumList;
                $yl = $v["length"]-$dssh-$sycd;
                $sh = $v["length"]-$sycd;
                $shv = round($sh/$v["length"]*100,2);
                $lyv = round(100-$shv,2);
                // if($v["stuff"]=="角钢"){
                //     preg_match_all("/\d+\.?\d*/",$v["specification"],$matches);
                //     $x = $matches[0][0]?((float)$matches[0][0]):0;
                //     $y = $matches[0][1]?((float)$matches[0][1]):0;
                //     $ds = 1+array_sum($dsList);
                //     $dssh = shAndWbData($x,$y)[0]*$ds;
                //     $sycd = $dsNumList;
                //     $yl = $v["length"]-$dssh-$sycd;
                //     $sh = $v["length"]-$sycd;
                //     $shv = round($sh/$v["length"]*100,2);
                //     $lyv = round(100-$shv,2);
                // }
                $pTableList[$k]["ds"] = $ds;
                $pTableList[$k]["dssh"] = $dssh;
                $pTableList[$k]["sycd"] = $sycd;
                $pTableList[$k]["yl"] = $yl;
                $pTableList[$k]["sh"] = $sh;
                $pTableList[$k]["shv"] = $shv;
                $pTableList[$k]["lyv"] = $lyv;
            }
            if($v["mate"]){
                $mateList = $this->_getMateData($v["mate"]);
                foreach($mateList as $kk=>$vv){
                    isset($partList[$vv["PartName"]])?($partList[$vv["PartName"]] += $v["count"]*$vv["SingleQTY"]):($partList[$vv["PartName"]] = $v["count"]*$vv["SingleQTY"]);
                }
            }
        }
        $ex = (new UnionProduceTaskView())->where("PT_Num",$row["pt_num"])->value("sect_field");
        if($row["sup"]){
            // $ex = (new UnionTaskReNewMainView())->where("TRNM_Num","=",$row["trnm_num"])->value("sect_field");
            $select_result = (new TaskReNew([],$ex))->where("TRNM_Num","=",$row["trnm_num"])->column("TRN_TPNum,TRN_TPNumber");
        }else{
            $select_result = (new ProduceTaskDetail([],$ex))->alias("ptd")
                ->join([$ex."dtmaterialdetial"=>"dtmd"],["ptd.DtMD_ID_PK = dtmd.DtMD_ID_PK"])
                ->where("ptd.PT_Num","=",$row["pt_num"])
                ->column("dtmd.DtMD_sPartsID,ptd.PTD_Count");
        }
        $diff = array_diff_assoc($select_result,$partList);
        foreach($diff as $k=>$v){
            if(isset($partList[$k])) $diff[$k] -= $partList[$k];
        }
        // $diff = array_diff_assoc($select_result,$partList);
        $part_result = [];
        if(!empty($diff)){
            $part_result = (new Dtmaterial([],$ex))->alias("dtm")
                ->join([$ex."dtsect"=>"dts"],"dtm.DtM_iID_PK = dts.DtM_iID_FK")
                ->join([$ex."dtmaterialdetial"=>"dtmd"],"dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK")
                ->where("dtmd.DtMD_sPartsID","in",array_keys($diff))
                ->where("dtm.TD_ID","=",$row["td_id"])
                ->where($noWhere)
                ->field("DtMD_sPartsID as id,DtMD_sPartsID,DtMD_sStuff,DtMD_sMaterial,DtMD_sSpecification,DtMD_iLength,DtS_Name,CAST(DtS_Name AS SIGNED) as name_number,
                case DtMD_sStuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,
                cast((case 
                when DtMD_sStuff='角钢' then 
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
                when (DtMD_sStuff='钢板' or DtMD_sStuff='钢管') then 
                REPLACE(DtMD_sSpecification,'-','')
                else 0
                end) as UNSIGNED) bh,CAST(DtMD_sPartsID AS SIGNED) as number")
                ->order("clsort asc,DtMD_sMaterial asc,bh asc,DtMD_sSpecification asc,DtMD_iLength desc")
                ->select();
            if($part_result) $part_result = collection($part_result)->toArray();
            else $part_result = [];
            foreach($part_result as $k=>$v){
                $part_result[$k]["count"] = $diff[$v["DtMD_sPartsID"]]??0;
                if($part_result[$k]["count"]==0) unset($part_result[$k]);
            }
        }
        return json(["code"=>1,"data"=>["ptable"=>array_values($pTableList),"notable"=>array_values($part_result)],"msg"=>"成功"]);
    }

    public function saveMaterial()
    {
        [$ids,$tableField] = array_values($this->request->post());
        if(!$ids or !$tableField) return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
        $table = json_decode($tableField,true);
        $saveDetail = [];
        foreach($table as $k=>$v){
            $dssh = 0;
            $explodeList = explode("+",$v["mate"]);
            $dsList = [];
            foreach($explodeList as $vv){
                $dsList[] = substr($vv,strrpos($vv,'*',0)+1);
            }
            if($v["stuff"]=="角钢"){
                preg_match_all("/\d+\.?\d*/",$v["specification"],$matches);
                $x = $matches[0][0]?((float)$matches[0][0]):0;
                $y = $matches[0][1]?((float)$matches[0][1]):0;
                $ds = 1+array_sum($dsList);
                $dssh = shAndWbData($x,$y,$v["material"])[0]*$ds;
            }
            $saveDetail[] = [
                "n_id" => $ids,
                "im_num" => $v["IM_Num"],
                "stuff" => $v["IM_Class"],
                "material" => $v["material"],
                "specification" => $v["IM_Spec"],
                "length" => $v["length"],
                "count" => $v["count"],
                "mate" => $v["mate"],
                "dssh" => $dssh
            ];
        }
        $result = $this->detailModel->allowField(true)->saveAll($saveDetail);
        if($result) return json(["data"=>[],"code"=>1,"msg"=>"成功"]);
        else return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
    }

    public function autoNesting()
    {
        [$ids,$tableField] = array_values($this->request->post());
        if(!$ids or !$tableField) return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
        $row = $this->model->get($ids);
        $table = json_decode($tableField,true);
        $cateList = $imList = $kcList = $res = [];
        $saveDetail = [];
        foreach($table as $v){
            if($v["DtMD_sStuff"]!="角钢") $saveDetail[] = [
                "n_id" => $ids,
                "stuff" => $v["DtMD_sStuff"],
                "material" => $v["DtMD_sMaterial"],
                "specification" => $v["DtMD_sSpecification"],
                "length" => $v["DtMD_iLength"],
                "count" => $v["count"],
                "mate" => $v["DtMD_sPartsID"]."/".$v["DtMD_iLength"]."*1",
                "auto" => 1
            ];
            else{
                $key = $v["DtMD_sStuff"].','.$v["DtMD_sMaterial"].','.$v["DtMD_sSpecification"];
                $cateList[$key][] = [
                    $v["DtMD_sPartsID"],
                    $v["DtMD_iLength"],
                    $v["count"],
                    $v["DtMD_sMaterial"],
                    $v["DtMD_sSpecification"]
                ];
                $imList[$key] = [
                    "IM_Spec" => ["=",$v["DtMD_sSpecification"]],
                    "L_Name" => ["=",$v["DtMD_sMaterial"]],
                    "SS_WareHouse" => ["=",$row["warehouse"]]
                ];
            }
        }
        $storeStateModel = new StoreState();

        foreach($imList as $k=>$v){
            $kcArr = [];
            $kcState = $storeStateModel->field("SS_Count as count,SS_Length as length")->where("SS_Count",">",0)->where($imList[$k])->select();
            foreach($kcState as $vv){
                $length = round($vv["length"],2);
                $kcArr[$length] = $vv["count"];
            }
            empty($kcArr)?'':$kcList[$k] = $kcArr;
        };
        $touchClass = new TouchNew();
        foreach($cateList as $k=>$v){
            if(empty($kcList[$k])) continue;
            $res[$k] = $touchClass->main($v,$kcList[$k]);
        }
        foreach($res as $k=>$vv){
            foreach($vv as $v){
                if(!isset($saveDetail[$v["mate"]])){
                    $saveDetail[$v["mate"]] = [
                        "n_id" => $ids,
                        // "im_num" => $v["IM_Num"],stripos($k,",");
                        "stuff" => substr($k,0,stripos($k,",")),
                        "material" => $v["material"],
                        "specification" => $v["spec"],
                        "length" => $v["nextClosest"],
                        "count" => 1,
                        "mate" => $v["mate"],
                        "dssh" => $v["dssh"],
                        "auto" => 1
                    ];
                }else $saveDetail[$v["mate"]]["count"]++;
            }
        }
        $result = $this->detailModel->allowField(true)->saveAll($saveDetail);
        if($result) return json(["data"=>[],"code"=>1,"msg"=>"成功"]);
        else return json(["data"=>[],"code"=>0,"msg"=>"失败，可能存在缺料或者其他情况，请检查后重试"]);
        // return $this->view->fetch();
    }

    public function manualMaterial($ids=null)
    {
        $this->assignconfig("ids",$ids);
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $ex = (new UnionProduceTaskView())->where("PT_Num",$row["pt_num"])->value("sect_field");
        $materialSelectModel = new \app\admin\model\chain\material\MaterialSelect();
        $selectRow = $materialSelectModel->order("id desc")->find();
        $selectRow = $selectRow?$selectRow->toArray():[];
        $this->view->assign("selectRow",$selectRow);
        $diff = $this->getDiff($row);
        // $partList = [];
        $kcArr = $imList = [];
        if(!empty($diff)){
            $part_result = (new Dtmaterial([],$ex))->alias("dtm")
                ->join([$ex."dtsect"=>"dts"],"dtm.DtM_iID_PK = dts.DtM_iID_FK")
                ->join([$ex."dtmaterialdetial"=>"dtmd"],"dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK")
                ->where("dtmd.DtMD_sPartsID","in",array_keys($diff))
                ->where("dtm.TD_ID","=",$row["td_id"])
                ->field("DtMD_sPartsID as id,DtMD_sPartsID,DtMD_sStuff AS IM_Class,DtMD_sMaterial AS material,DtMD_sSpecification AS specification,DtMD_iLength AS length,DtS_Name,CAST(DtS_Name AS SIGNED) as name_number,case DtMD_sStuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,(SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1) bh,CAST(DtMD_sPartsID AS SIGNED) as number")
                ->order("clsort asc,DtMD_sMaterial asc,bh asc,DtMD_sSpecification asc,length desc")
                ->select();
            $part_result = $part_result?collection($part_result)->toArray():[];
            foreach($part_result as $lv){
                $key = $lv["IM_Class"].','.$lv["material"].','.$lv["specification"];
                $imList[$key] = [
                    "m.IM_Spec" => ["=",$lv["specification"]],
                    "m.L_Name" => ["=",$lv["material"]]
                ];
                $partList[$lv["DtMD_sPartsID"]] = [
                    "length" => $lv["length"],
                    "IM_Class" => $lv["IM_Class"],
                    "material" => $lv["material"],
                    "specification" => $lv["specification"],
                ];
            }
            $storeStateModel = new StoreState();
            $summary_query = "(select WR_WareHouse,IM_Num,L_Name,IM_Spec,WRD_Length,WRD_Width,sum(sy_count) as sy_count,sum(sy_weight) as sy_weight from (select wr.WR_WareHouse,wrd.D_IM_Num as IM_Num,wrd.D_L_Name as L_Name,wrd.D_IM_Spec as IM_Spec,wrd.WRD_Length,wrd.WRD_Width,wrd.WRD_RealCount-ifnull(sum(SOD_Count),0) as sy_count,wrd.WRD_RealWeight-ifnull(sum(SOD_Weight),0) as sy_weight
                from workreceive wr
                inner join workreceivedetail wrd on wr.WR_Num=wrd.WR_Num
                LEFT join storeoutdetail sod on wrd.WRD_ID=sod.WRD_ID 
                where wr.WriteDate>='2022-05-28' and wr.WR_Send=0 and wr.WR_WareHouse='".$row["warehouse"]."'
                GROUP BY wrd.WRD_ID) aw group by WR_WareHouse,IM_Num,L_Name,IM_Spec,WRD_Length,WRD_Width)";
            $kcState = $storeStateModel->alias("m")
                ->join([$summary_query=>"w"],["w.WR_WareHouse=m.SS_WareHouse","w.IM_Num=m.IM_Num","w.L_Name=m.L_Name","w.IM_Spec=m.IM_Spec","w.WRD_Length=m.SS_Length","w.WRD_Width=m.SS_Width"],"left")
                ->field("SS_Length as length,(SS_Count-ifnull(w.sy_count,0)) as count,m.IM_Spec,m.L_Name")
                ->where(function ($query) use ($imList){
                    foreach($imList as $k=>$v){
                        $query->whereor($v);
                    }
                })
                ->where("m.SS_WareHouse",$row["warehouse"])
                ->where("(SS_Count-ifnull(w.sy_count,0))>0")
                ->group("m.IM_Spec,m.L_Name,m.SS_WareHouse,SS_Length")
                ->order("SS_Length asc")
                ->select();
            foreach($kcState as $v){
                $kcArr[$v["L_Name"].','.$v["IM_Spec"]][] = [
                    round($v["length"],2),
                    round($v["count"],2)
                ];
            }
        }
        // $this->assignconfig("part_result",$part_result);
        $this->assignconfig("kcArr",$kcArr);
        $this->assignconfig("partList",$partList);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function manualNoTable()
    {
        $ids = $this->request->post("ids");
        $noWhere = $saveData = [];
        $l_name = $this->request->post("l_name");
        if($l_name){
            $saveData["material"] = $l_name;
            $noWhere["DtMD_sMaterial"] = ["=",$l_name];
        }
        $spec = $this->request->post("spec");
        if($spec){
            $saveData["specification"] = $spec;
            $noWhere["DtMD_sSpecification"] = ["=",$spec];
        }
        if(!empty($saveData)) (new \app\admin\model\chain\material\MaterialSelect())->save($saveData);
        $row = $this->model->get($ids);
        if (!$row) {
            return json(["code"=>0,"data"=>[],"msg"=>__('No Results were found')]);
        }
        $ex = (new UnionProduceTaskView())->where("PT_Num",$row["pt_num"])->value("sect_field");
        $diff = $this->getDiff($row);
        // $partList = [];
        if(!empty($diff)){
            $part_result = (new Dtmaterial([],$ex))->alias("dtm")
                ->join([$ex."dtsect"=>"dts"],"dtm.DtM_iID_PK = dts.DtM_iID_FK")
                ->join([$ex."dtmaterialdetial"=>"dtmd"],"dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK")
                ->where("dtmd.DtMD_sPartsID","in",array_keys($diff))
                ->where("dtm.TD_ID","=",$row["td_id"])
                ->where($noWhere)
                ->field("DtMD_sPartsID as id,DtMD_sPartsID,DtMD_sStuff AS IM_Class,DtMD_sMaterial AS material,DtMD_sSpecification AS specification,DtMD_iLength AS length,DtS_Name,CAST(DtS_Name AS SIGNED) as name_number,case DtMD_sStuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,(SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1) bh,CAST(DtMD_sPartsID AS SIGNED) as number")
                ->order("clsort asc,DtMD_sMaterial asc,bh asc,DtMD_sSpecification asc,length desc")
                ->select();
            $part_result = $part_result?collection($part_result)->toArray():[];
            foreach($part_result as $lk=>$lv){
                $part_result[$lk]["count"] = $diff[$lv["DtMD_sPartsID"]]??0;
                if($part_result[$lk]["count"]==0) unset($part_result[$lk]);
                // $partList[$lv["DtMD_sPartsID"]] = [
                //     "length" => $lv["length"],
                //     "IM_Class" => $lv["IM_Class"],
                //     "material" => $lv["material"],
                //     "specification" => $lv["specification"],
                // ];
            }
        }
        return json(["code"=>1,"data"=>["part_result"=>array_values($part_result)],"msg"=>"成功"]);
    }

    public function manualNesting()
    {
        [$ids,$tableField] = array_values($this->request->post());
        if(!$ids or !$tableField) return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
        $table = json_decode($tableField,true);
        $saveDetail = [];
        foreach($table as $v){
            $dssh = 0;
            $explodeList = explode("+",$v["mate"]);
            $dsList = [];
            foreach($explodeList as $vv){
                $dsList[] = substr($vv,strrpos($vv,'*',0)+1);
            }
            if($v["stuff"]=="角钢"){
                preg_match_all("/\d+\.?\d*/",$v["specification"],$matches);
                $x = $matches[0][0]?((float)$matches[0][0]):0;
                $y = $matches[0][1]?((float)$matches[0][1]):0;
                $ds = 1+array_sum($dsList);
                $dssh = shAndWbData($x,$y,$v["material"])[0]*$ds;
            }
            $saveDetail[] = [
                "n_id" => $ids,
                "stuff" => $v["stuff"],
                "material" => $v["material"],
                "specification" => $v["specification"],
                "length" => $v["length"],
                "count" => $v["count"],
                "mate" => $v["mate"],
                "dssh" => $dssh
            ];
        }
        $result = $this->detailModel->allowField(true)->saveAll($saveDetail);
        if($result) return json(["data"=>[],"code"=>1,"msg"=>"成功"]);
        else return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
    }

    public function generMaterial($ids = null)
    {
        $this->assignconfig("ids",$ids);
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $ex = (new UnionProduceTaskView())->where("PT_Num",$row["pt_num"])->value("sect_field");
        $this->request->filter(['strip_tags', 'trim']);
        $diff = $this->getDiff($row);
        if ($this->request->isAjax()) {
            $part_result = [];
            if(!empty($diff)){
                // $join_list = [
                //     "dtmd.DtMD_sSpecification = im.IM_Spec",
                //     "dtmd.DtMD_sStuff = im.IM_Class"
                // ];
                $filter = $this->request->get("filter", '');
                $filter = (array)json_decode($filter, true);
                $filter = $filter ? $filter : [];
                $where = [];
                if(isset($filter['IM_Class'])) $where["DtMD_sStuff"] = ["=",$filter['IM_Class']];
                if(isset($filter["material"])) $where["DtMD_sMaterial"] = ["=",$filter['material']];
                if(isset($filter["specification"])) $where["DtMD_sSpecification"] = ["=",$filter['specification']];
                $part_result = (new Dtmaterial([],$ex))->alias("dtm")
                    ->join([$ex."dtsect"=>"dts"],"dtm.DtM_iID_PK = dts.DtM_iID_FK")
                    ->join([$ex."dtmaterialdetial"=>"dtmd"],"dts.DtS_ID_PK = dtmd.DtMD_iSectID_FK")
                    ->where("dtmd.DtMD_sPartsID","in",array_keys($diff))
                    ->where("dtm.TD_ID","=",$row["td_id"])
                    ->where($where)
                    ->field("DtMD_sStuff as IM_Class,DtMD_sMaterial as material,DtMD_sSpecification as specification,dtmd.DtMD_iLength as length,'' as mate,CAST(DtS_Name AS SIGNED) as name_number,DtS_Name,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
                    SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
                    when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
                    when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
                    when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
                    when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
                    else 0 end bjbhn,CAST(DtMD_sPartsID AS SIGNED) as number,DtMD_sPartsID")
                    ->order("name_number,DtS_Name,bjbhn,number,DtMD_sPartsID")
                    ->select();
                $part_result = $part_result?collection($part_result)->toArray():[];
                foreach($part_result as $lk=>$lv){
                    $part_result[$lk]["count"] = $diff[$lv["DtMD_sPartsID"]];
                    if($lv["IM_Class"]!="角钢"){
                        $part_result[$lk]["mate"] = $lv["DtMD_sPartsID"]."/".$lv["length"]."*1";
                    }
                }
                $result["total"] = count($part_result);
                $result["rows"] = $part_result?collection($part_result)->toArray():[];
            }else{
                $result["total"] = 0;
                $result["rows"] = [];
            }
            return json($result);
        }
        return $this->view->fetch();
    }

    public function xddh()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new UnionProduceTaskView())->alias("p")
                ->join(["taskdetail"=>"d"],"p.TD_ID = d.TD_ID")
                ->join(["task"=>"t"],"t.T_Num=d.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->field("p.*,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num',c.C_Num,c.C_Num as 'c.C_Num',c.PC_Num,c.PC_Num as 'c.PC_Num',d.TD_Pressure")
                ->where($where)
                ->order("p.WriterDate desc,p.PT_Num desc")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function schedulingRelease()
    {
        [$ids,$tableField] = array_values($this->request->post());
        if(!$ids or !$tableField) return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
        $table = json_decode($tableField,true);
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $sequenceModel = new Sequence();
        $sNum = $sequenceModel->where("PT_Num",$row["pt_num"])->value("sNum");
        $data = ["code"=>0,"msg"=>"发送机台失败","data"=>[]];
        //找到排产情况
        $pcList = $sequenceModel->alias("s")
            ->join(["pc_sequence_detail"=>"sd"],"s.sNum = sd.sNum")
            ->join(["pc_sequence_part"=>"sp"],"sd.sdNum=sp.sdNum")
            ->where([
                "s.PT_Num" => ["=",$row["pt_num"]],
                "sd.orderId" => ["=",$row["orderId"]]
            ])->column("concat(sStuff,',',sMaterial,',',sSpecification) as key_word,machine");
        $sNum = "PC".date("Ymd");
        $getOne = (new PcScheduling())->where("PC_Num","LIKE",'PC'.date('Ymd').'%')->order("PC_Num desc")->value("right(PC_Num,3)");
        if($getOne) $num = str_pad(($getOne+1),3,0,STR_PAD_LEFT);
        else $num = "001";
        $sNum .= $num;
        // pri($table,$pcList,1);
        $saveDetail = $saveDetailList = $noPc = $fileList = [];
        foreach($table as $k=>$v){
            $key_word = $v["stuff"].','.$v["material"].','.$v["specification"];
            if(isset($pcList[$key_word])){
                $saveDetail[$v["id"]] = [
                    "P_ID" => $v["id"],
                    "sNum" => $sNum,
                    "PC_Num" => $sNum,
                    "PtNum" => $row["pt_num"],
                    "IMName" => $v["stuff"],
                    "IMStd" => $v["specification"],
                    "IMMaterial" => $v["material"],
                    "IMLength" => $v["length"],
                    "Num" => $v["count"],
                    "MachineName" => $pcList[$key_word],
                    "writer" => $this->admin["id"],
                    "orderId" => $row["orderId"],
                    "auditor" => $this->admin["id"],
                ];
                $explodeList = explode("+",$v["mate"]);
                foreach($explodeList as $kk=>$vv){
                    $PartNCPath = $row["td_id"].'-'.substr($vv,0,strrpos($vv,'/',0));
                    $saveDetailList[$v["id"]][] = [
                        "PartName" => substr($vv,0,strrpos($vv,'/',0)),
                        "PartNCPath" => $PartNCPath,
                        "PartLength" => substr($vv,strrpos($vv,'/',0)+1,strrpos($vv,'*',0)-strrpos($vv,'/',0)-1),
                        "SingleQTY" => substr($vv,strrpos($vv,'*',0)+1),
                        "QTY" => $v["count"]*substr($vv,strrpos($vv,'*',0)+1)
                    ];
                    $fileList[$PartNCPath] = $PartNCPath;
                }
            }else $noPc[$key_word] = $key_word;
        }
        //未存在图纸文件提醒
        if(!empty($fileList)){
            $existFile = (new PcNcfile())->where("PartNCPath","IN",$fileList)->column("PartNCPath");
            $diffFile = array_diff($fileList,$existFile);
            if(!empty($diffFile)){
                $data["msg"] = "部件号为".implode(",",$diffFile)."没有文件，请补全";
                return json($data);
            }
        }
        

        //已有存在的看看nrecord有值 无法更新
        //nrecord=0的 主表和子表都删除 重新添加
        //不存在 直接添加
        $schedulingModel = (new PcScheduling());
        $schedulingSectModel = (new PcSchedulingSect());
        $result = false;
        if(!empty($saveDetail)){
            $scheduling_result = false;
            Db::startTrans();
            try {
                $existScheduling = $schedulingModel->where("P_ID","IN",array_keys($saveDetail))->column("ID");
                if(!empty($existScheduling)){
                    $schedulingSectModel->where("MID","IN",$existScheduling)->delete();
                    $schedulingModel->where("ID","IN",$existScheduling)->delete();
                }
                $result = $schedulingModel->allowField(true)->saveAll($saveDetail);
                $detailList = [];
                foreach($saveDetailList as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $detailList[] = $vv+["MID"=>$result[$k]["ID"]];
                    }
                }
                $schedulingSectModel->allowField(true)->saveAll($detailList);
                $this->detailModel->where("id","IN",array_keys($result))->update(["pc_flag"=>1]);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $data["code"]=1;
                $data["msg"] = "发送机台成功";
            }
        }
        if(!empty($noPc)) $data["msg"] .= ",\"".implode("/",$noPc)."\"未进行排产";
        return json($data);
    }

    public function print($ids)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $ex = (new UnionProduceTaskView())->where("PT_Num",$row["pt_num"])->value("sect_field");
        $tx = (new TaskDetail())->where("TD_ID",$row["td_id"])->value("TD_TypeName");
        $gcmc = (new ProduceTask([],$ex))->where("PT_Num",$row["pt_num"])->field("C_ProjectName,PT_Number")->find();
        $rows = [
            "title" => $row["pt_num"]."套料结果打印单",
            "gcmc" => $gcmc?$gcmc["C_ProjectName"]:'',
            "sl" => $gcmc?$gcmc["PT_Number"].'基':'',
            "xddh" => $row["pt_num"],
            "tx" => $tx??''
        ];
        $this->assignconfig("rows",$rows);
        $allTableList = $this->model->alias("m")
            ->join(["nesting_sub"=>"ns"],"m.id=ns.n_id")
            ->field("ns.*,
            case stuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,
            cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("m.id",$ids)
            ->where("stuff","角钢")
            // ->where("ns.pc_flag",1)
            ->order("clsort asc,material asc,bh asc,specification asc,length desc")
            ->select();
        if($allTableList) $allTableList = collection($allTableList)->toArray();
        else $allTableList = [];
        foreach($allTableList as $k=>&$v){
            $v["id"] = $k+1;
            $ds = $dssh = $sycd = $yl = $sh = $shv = $dsNumList =  0;
            $lyv = 100;
            $explodeList = explode("+",$v["mate"]);
            $dsList = [];
            foreach($explodeList as $vv){
                $dsList[] = substr($vv,strrpos($vv,'*',0)+1);
                $dsNumList += round((float)(substr($vv,strrpos($vv,'/',0)+1,strrpos($vv,'*',0)-strrpos($vv,'/',0)-1))*(float)(substr($vv,strrpos($vv,'*',0)+1)),2);
            }
            $dssh = $v["dssh"];
            $sycd = $dsNumList;
            $yl = $v["length"]-$dssh-$sycd;
            $sh = $v["length"]-$sycd;
            $shv = round($sh/$v["length"]*100,2);
            $lyv = round(100-$shv,2);
            // if($v["stuff"]=="角钢"){
            //     preg_match_all("/\d+\.?\d*/",$v["specification"],$matches);
            //     $x = $matches[0][0]?((float)$matches[0][0]):0;
            //     $y = $matches[0][1]?((float)$matches[0][1]):0;
            //     $ds = 1+array_sum($dsList);
            //     $dssh = shAndWbData($x,$y)[0]*$ds;
            //     $sycd = $dsNumList;
            //     $yl = $v["length"]-$dssh-$sycd;
            //     $sh = $v["length"]-$sycd;
            //     $shv = round($sh/$v["length"]*100,2);
            //     $lyv = round(100-$shv,2);
            // }
            $v["yl"] = $yl;
            $v["lyv"] = $lyv;
        }
        // $title = date('Y年').$month.'月 成品出入库月统计';
        // $list = json_decode(Cache::get('month/monthlystatistics')); 
        // $this->assignconfig('list',$list);
        // $this->assignconfig('zb', $this->admin['username']);
        $this->assignconfig('list', $allTableList);
        return $this->view->fetch();
    }

    public function total($ids=null)
    {
        $list = $this->detailModel->field("stuff,material,specification,length,width,sum(count) as count,
        (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
        SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1) bh")->where("n_id","=",$ids)->where("stuff","角钢")->order("material asc,bh asc,specification asc,length asc")->group("stuff,material,specification,length,width")->select();
        $this->assignconfig("list",$list);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function zlprint($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $ex = (new UnionProduceTaskView())->where("PT_Num",$row["pt_num"])->value("sect_field");
        $tx = (new TaskDetail())->where("TD_ID",$row["td_id"])->value("TD_TypeName");
        $gcmc = (new ProduceTask([],$ex))->where("PT_Num",$row["pt_num"])->field("C_ProjectName,PT_Number")->find();
        $rows = [
            "title" => $row["pt_num"]."套料结果打印单",
            "gcmc" => $gcmc?$gcmc["C_ProjectName"]:'',
            "sl" => $gcmc?$gcmc["PT_Number"].'基':'',
            "xddh" => $row["pt_num"],
            "tx" => $tx??''
        ];
        $this->assignconfig("rows",$rows);
        $list = $this->detailModel->field("stuff,material,specification,length,width,sum(count) as count,case stuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,
        (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
        SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1) bh")->where("n_id","=",$ids)->where("stuff","角钢")->order("material asc,bh asc,specification asc,length asc")->group("stuff,material,specification,length,width")->select();
        $list = $list?collection($list)->toArray():[];
        $this->assignconfig("list",$list);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }


    ///stop
    public function bjdh($td_id = null)
    {
        $this->assignconfig("td_id",$td_id);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $taskDetailModel = new TaskDetail();
            $tdList = $taskDetailModel->get($td_id);
            $ex = $this->_technologyEx()[$tdList["P_Name"]];
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $dbSql = (new TaskReNewMain([],$ex))->alias("trm")
                ->join(["mergtypename"=>"mtn"],"trm.TD_ID=mtn.mTD_ID","left")
                ->field("trm.*")
                ->where("trm.TD_ID|mtn.mTD_ID","=",$td_id)
                ->group("trm.TRNM_Num")
                ->buildSql();
            $list = $taskDetailModel->alias("d")
                ->join(["task"=>"t"],"t.T_Num=d.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->join([$dbSql=>"trm"],"d.TD_ID = trm.TD_ID")
                ->field("trm.*,c.C_Project,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num',c.C_Num,c.C_Num as 'c.C_Num',c.PC_Num,c.PC_Num as 'c.PC_Num',d.TD_Pressure")
                ->where($where)
                ->order("trm.TRNM_WriteDate desc,trm.TRNM_Num desc")
                ->paginate($limit);

            // $list = DB::query($dbSql)->alias("trm")
            //     // ->join(["mergtypename"=>"mtn"],"trm.TD_ID=mtn.mTD_ID","left")
            //     ->join(["taskdetail"=>"d"],"d.TD_ID = trm.TD_ID")
            //     ->join(["task"=>"t"],"t.T_Num=trm.T_Num")
            //     ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            //     ->field("trm.*,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num',c.C_Num,c.C_Num as 'c.C_Num',c.PC_Num,c.PC_Num as 'c.PC_Num',d.TD_Pressure")
            //     ->where($where)
            //     ->order("trm.TRNM_WriteDate desc,trm.TRNM_Num desc")
            //     ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }
    
    

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        $return_news = ["code"=>0,'msg'=>"删除失败"];
        if($num){
            $mn_one = $this->model->alias("m")->join(["nesting_sub"=>"ns"],"m.id = ns.n_id")->where("ns.id",$num)->find();
            if($mn_one["auditor"]) return json(["code"=>0,'msg'=>"已审核，不能进行删除！"]);
            $result = $this->detailModel->where("id",$num)->delete();
            if($result){
                $return_news["code"] = 1;
                $return_news["msg"] = "删除成功";
            }
        }
        return $return_news;
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $result = $this->model->save(['auditor'=>$admin,"auditor_time"=>date("Y-m-d H:i:s")],["id"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if($this->model->where("id",$num)->value("pc_flag")) return json(["code"=>0,"msg"=>"弃审失败"]);
        $result = $this->model->update(['auditor'=>"","auditor_time"=>"0000-00-00 00:00:00"],["id"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"弃审成功"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
            {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $pcPcList = $this->model->alias("n")
                ->join(["nesting_sub"=>"ns"],"n.id=ns.n_id")
                ->where("n.".$pk,"in",$ids)
                ->where("ns.pc_flag","=",1)
                ->group("n.".$pk)
                ->column("n.".$pk);
            $delList = explode(",",$ids);
            $list = array_diff($delList,$pcPcList);
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where("id","IN",$list)->delete();
                $this->detailModel->where("n_id","IN",$list)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["ID","id","text","readonly","",40],
            ["存货编号","im_num","text","readonly data-rule='required' ","",80],
            ["材料名称","stuff","text","readonly","",80],
            ["材质","material","select"," data-rule='required' ","",80],
            ["规格","specification","text","readonly","",80],
            ["长度(mm)","length","text","",0,40],
            ["宽度(mm)","width","text","",0,40],
            ["数量","count","text"," data-rule='required' ",0,40],
            // ["重量(kg)","weight","text","readonly","",40],
            ["匹配结果(部件号/长度*数量)","mate","text","","",150],
            ["备注","remark","text","","",80],
        ];
        return $list;
    }

    protected function _getMateData($string='')
    {
        if(!$string) return [];
        $arr = explode("+",$string);
        $son_list = [];
        foreach($arr as $k=>$v){
            if(strstr($v,"*")===false) $v .= "*1";
            preg_match_all("/(.*)\/(\d+)\*(\d+)/",$v,$matches);
            if($matches[1][0]) $son_list[] = ["PartName"=>$matches[1][0],"PartLength"=>$matches[2][0],"SingleQTY"=>$matches[3][0]??1];
        }
        
        // pri($son_list,1);
        // // $string = implode("+",$arr);
        // preg_match_all("/(.*)\/(\d+)\*(\d+)/",$string,$matches);
        // // preg_match_all("/\d+/",$string,$matches);
        // // $sect_list = array_chunk($matches[0],3);
        // $son_list = [];
        // foreach($matches as $v){
        //     if($matches[1][0]) $son_list[] = ["PartName"=>$matches[1][0],"PartLength"=>$matches[2][0],"SingleQTY"=>$matches[3][0]??1];
        // }
        return $son_list;
    }

    protected function getDiff($row)
    {
        $partList = [];
        $allTableList = $this->model->alias("m")
            ->join(["nesting_sub"=>"ns"],"m.id=ns.n_id")
            ->field("ns.*")
            ->where([
                "pt_num" => ["=",$row["pt_num"]],
                "sup" => ["=",$row["sup"]],
                "trnm_num" => ["=",$row["trnm_num"]]
            ])->order("id")
            ->select();
        foreach($allTableList as $k=>$v){
            if($v["mate"]){
                $mateList = $this->_getMateData($v["mate"]);
                foreach($mateList as $kk=>$vv){
                    isset($partList[$vv["PartName"]])?($partList[$vv["PartName"]] += $v["count"]*$vv["SingleQTY"]):($partList[$vv["PartName"]] = $v["count"]*$vv["SingleQTY"]);
                }
            }
        }
        $ex = (new UnionProduceTaskView())->where("PT_Num",$row["pt_num"])->value("sect_field");
        if($row["sup"]){
            $select_result = (new TaskReNew([],$ex))->where("TRNM_Num","=",$row["trnm_num"])->column("TRN_TPNum,TRN_TPNumber");
        }else{
            $select_result = (new ProduceTaskDetail([],$ex))->alias("ptd")
                ->join([$ex."dtmaterialdetial"=>"dtmd"],["ptd.DtMD_ID_PK = dtmd.DtMD_ID_PK"])
                ->where("ptd.PT_Num","=",$row["pt_num"])
                ->column("dtmd.DtMD_sPartsID,ptd.PTD_Count");
        }
        $diff = array_diff_assoc($select_result,$partList);
        foreach($diff as $k=>$v){
            if(isset($partList[$k])) $diff[$k] -= $partList[$k];
        }
        return $diff;
    }

    public function updateNestingDssh()
    {
        die;
        $list = $this->detailModel->select();
        $list = $list?collection($list)->toArray():[];
        foreach($list as $k=>$v){
            $dssh = 0;
            $explodeList = explode("+",$v["mate"]);
            $dsList = [];
            foreach($explodeList as $vv){
                $dsList[] = substr($vv,strrpos($vv,'*',0)+1);
            }
            if($v["stuff"]=="角钢"){
                preg_match_all("/\d+\.?\d*/",$v["specification"],$matches);
                $x = $matches[0][0]?((float)$matches[0][0]):0;
                $y = $matches[0][1]?((float)$matches[0][1]):0;
                $ds = 1+array_sum($dsList);
                $dssh = shAndWbData($x,$y,$v["material"])[0]*$ds;
            }
            $list[$k]["dssh"] = $dssh;
        }
        $result = $this->detailModel->allowField(true)->saveAll($list);
        pri(count($result),1);
    }
}
