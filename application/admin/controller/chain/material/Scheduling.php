<?php

namespace app\admin\controller\chain\material;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 套料
 *
 * @icon fa fa-circle-o
 */
class Scheduling extends Backend
{
    
    /**
     * Nesting模型对象
     * @var \app\admin\model\chain\material\Nesting
     */
    protected $model = null;
    protected $noNeedLogin = ['*'];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\PcScheduling;
        $this->detailModel = new \app\admin\model\chain\material\PcSchedulingSect;
        $this->detailVewModel = new \app\admin\model\chain\material\PcSchedulingSectView;
        $wareList = [];
        $wareModel = (new \app\admin\model\jichu\ch\WareClass())->field("WC_Name")->where("WC_Sort","原材料")->order("WC_Num")->select();
        foreach($wareModel as $v){
            $wareList[$v["WC_Name"]] = $v["WC_Name"];
        }
        $this->view->assign("wareList",$wareList);
        $this->assignconfig("wareList",$wareList);
        // $this->machineArr = $this->machineList(["type"=>["=",2]])[0];
        $this->machineArr = [
            // "BL2020" => "BL2020",
            // "ADM3635" => "ADM3635",
            // "BL2532" => "BL2532",
            // "APM0708" => "APM0708",
            // "APM2020" => "APM2020",
            // "BL1412C" => "BL1412C",
            // "ADM3635" => "ADM3635",
            // "APM1412" => "APM1412",
            // "PP123" => "PP123",
            // "PPR103" => "PPR103",
            // "PD16C" => "PD16C"

            "TPP103" => "TPP103",
            "PP123M-L" => "PP123M-L",
            "PP123M-H" => "PP123M-H",
            "APM0708" => "APM0708",
            "BL1412C" => "BL1412C",
            "APM1412" => "APM1412",
            "BL2020" => "BL2020",
            "ADM3635-1" => "ADM3635-1",
            "APH2020" => "APH2020",
            "BL2532" => "BL2532",
            "ADM3635-2" => "ADM3635-2"

        ];
        $this->assignconfig("machineArr",$this->machineArr);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->where($where)->order($sort, $order)->group("PtNum,orderId")->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $this->assignconfig("ids",$ids);
        $row = $this->model->alias("m")
            ->join(["nesting_sub"=>"ns"],"m.P_ID=ns.id")
            ->join(["nesting"=>"n"],"n.id=ns.n_id")
            ->field("n.orderId,n.pt_num,n.warehouse")
            ->where("m.ID",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $pt_num = $row->pt_num;
            $orderId = $row->orderId;
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->field("*,case IMName when '角钢' then 0 when '钢板' then 1 else 2 end clsort,
            cast((case 
            when IMName='角钢' then 
            SUBSTR(REPLACE(IMStd,'∠',''),1,locate('*',REPLACE(IMStd,'∠',''))-1)*1000+
            SUBSTR(REPLACE(IMStd,'∠',''),locate('*',REPLACE(IMStd,'∠',''))+1,2)*1
            when (IMName='钢板' or IMName='钢管') then 
            REPLACE(IMStd,'-','')
            else 0
            end) as UNSIGNED) bh
            ")->where([
                "PtNum" => ["=",$pt_num],
                "orderId" => ["=",$orderId]
            ])->where($where)->order("bh asc,IMStd asc")
            // ->select(false);pri($list,1);
            ->paginate($limit);
            $data = $endData = [];
            foreach($list as $v){
                $data[$v["ID"]] = $v->toArray();
            }
            $detailList = $this->detailModel->where("MID","IN",array_keys($data))->select();
            foreach($detailList as $k=>$v){
                $endData[] = [
                    "p_id" => $v["MID"],
                    "IMName" => $data[$v["MID"]]["IMName"],
                    "IMStd" => $data[$v["MID"]]["IMStd"],
                    "IMMaterial" => $data[$v["MID"]]["IMMaterial"],
                    "IMLength" => $data[$v["MID"]]["IMLength"],
                    "Num" => $data[$v["MID"]]["Num"],
                    "nrecord" => $data[$v["MID"]]["nrecord"],
                    "MachineName" => $data[$v["MID"]]["MachineName"],
                    "PartName" => $v["PartName"],
                    "PartNCPath" => $v["PartNCPath"],
                    "PartLength" => $v["PartLength"],
                    "SingleQTY" => $v["SingleQTY"],
                    "QTY" => $v["QTY"],
                ];
            }
            $result = array("total" => $list->total(), "rows" => $endData);

            return json($result);
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function del($ids=null)
    {
        $this->error("暂无该功能");
    }
    // public function delDetailContent()
    // {
    //     $num = $this->request->post("num");
    //     $return_news = ["code"=>0,'msg'=>"删除失败"];
    //     if($num){
    //         $mn_one = $this->model->alias("m")->join(["nesting_sub"=>"ns"],"m.id = ns.n_id")->where("ns.id",$num)->find();
    //         if($mn_one["auditor"]) return json(["code"=>0,'msg'=>"已审核，不能进行删除！"]);
    //         $result = $this->detailModel->where("id",$num)->delete();
    //         if($result){
    //             $return_news["code"] = 1;
    //             $return_news["msg"] = "删除成功";
    //         }
    //     }
    //     return $return_news;
    // }


    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $find = $this->model->alias("ps")
            ->join(["nesting_sub"=>"ns"],"ns.id=ps.P_ID")
            ->where("ns.n_id",$num)
            ->where("ps.MachineName","")
            ->find();
        if($find) return json(["code"=>0,"msg"=>"有未选择设备的情况"]);
        $result = $this->model->alias("ps")
            ->join(["nesting_sub"=>"ns"],"ns.id=ps.P_ID")
            ->where("ns.n_id",$num)
            ->update(["ps.auditor"=>$admin,"auditor_time"=>date("Y-m-d H:i:s")]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $find = $this->model->alias("ps")
            ->join(["pc_scheduling_sect"=>"pss"],"ps.ID=pss.MID")
            ->join(["nesting_sub"=>"ns"],"ns.id=ps.P_ID")
            ->field("pss.nrecord")
            ->where("ns.n_id",$num)
            ->where("pss.nrecord","<>",0)
            ->select();
        if($find) return json(["code"=>0,"msg"=>"已经进行制作，无法弃审"]);
        $result = $this->model->alias("ps")
            ->join(["nesting_sub"=>"ns"],"ns.id=ps.P_ID")
            ->where("ns.n_id",$num)
            ->update(["ps.auditor"=>"","auditor_time"=>"0000-00-00 00:00:00"]);
        if ($result) {
            return json(["code"=>1,"msg"=>"弃审成功"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

    // public function eipUpload()
    // {
    //     $PC_Num = $this->request->post("PC_Num");
    //     $json_result = ["code"=>0,"msg"=>"失败"];
    //     $where = [
    //         "PC_Num" => ["=",$PC_Num]
    //     ];
    //     $params = (new \app\admin\model\view\PcDetailView())->where($where)->select();
    //     if(!$params) return json($json_result);
    //     $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
    //     $saveData = [];
    //     $param_url = "supplier-production-schedule";
    //     foreach($params as $k=>$v){
    //         $operatetype = "ADD";
    //         $api_data = [
    //             "purchaserHqCode" => "SGCC",
    //             "supplierCode" => "1000014615",
    //             "supplierName" => "绍兴电力设备有限公司",
    //             "poItemId" => $v["poItemId"],
    //             "scheduleCode" => $v["PC_Num"],
    //             "planPeriod" => $v["planPeriod"],
    //             "dueDate" => date("Y-m-d",strtotime($v["T_Date"])),
    //             "planStartDate" => date("Y-m-d",strtotime($v["writer_time"])),
    //             "planFinishDate" => date("Y-m-d",strtotime("+".$v["planPeriod"]." days",strtotime($v["writer_time"]))),
    //             "categoryCode" => "60",
    //             "subclassCode" => "60001",
    //             "dataSource" => 0,
    //             "dataSourceCreateTime"=>date("Y-m-d H:i:s"),
    //             "woList" => [
    //                 [
    //                     "batchCode" => 1,
    //                     "scheduleWo" => $v["PC_Num"],
    //                     "matCode" => $v["TD_TypeName"],
    //                     "matDesc" => $v["TD_TypeName"],
    //                     "scheduleAmount" => $v["PT_Number"],
    //                     "scheduleUnit" => $v["SCD_Unit"],
    //                     "processList" => [
    //                         [
    //                             "processCode" => "TTGX01",
    //                             "processName" => "下料",
    //                             "planStartDate" => date("Y-m-d",strtotime($v["writer_time"])),
    //                             "planFinishDate" => date("Y-m-d",strtotime("+".$v["planPeriod"]." days",strtotime($v["writer_time"]))),
    //                         ],
    //                         [
    //                             "processCode" => "TTGX02",
    //                             "processName" => "制孔",
    //                             "planStartDate" => date("Y-m-d",strtotime($v["writer_time"])),
    //                             "planFinishDate" => date("Y-m-d",strtotime("+".$v["planPeriod"]." days",strtotime($v["writer_time"]))),
    //                         ],
    //                         [
    //                             "processCode" => "TTGX04",
    //                             "processName" => "组装",
    //                             "planStartDate" => date("Y-m-d",strtotime($v["writer_time"])),
    //                             "planFinishDate" => date("Y-m-d",strtotime("+".$v["planPeriod"]." days",strtotime($v["writer_time"]))),
    //                         ],
    //                         [
    //                             "processCode" => "TTGX05",
    //                             "processName" => "焊接",
    //                             "planStartDate" => date("Y-m-d",strtotime($v["writer_time"])),
    //                             "planFinishDate" => date("Y-m-d",strtotime("+".$v["planPeriod"]." days",strtotime($v["writer_time"]))),
    //                         ],
    //                         [
    //                             "processCode" => "TTGX06",
    //                             "processName" => "镀锌",
    //                             "planStartDate" => date("Y-m-d",strtotime($v["writer_time"])),
    //                             "planFinishDate" => date("Y-m-d",strtotime("+".$v["planPeriod"]." days",strtotime($v["writer_time"]))),
    //                         ],
    //                         [
    //                             "processCode" => "TTGX07",
    //                             "processName" => "制弯",
    //                             "planStartDate" => date("Y-m-d",strtotime($v["writer_time"])),
    //                             "planFinishDate" => date("Y-m-d",strtotime("+".$v["planPeriod"]." days",strtotime($v["writer_time"]))),
    //                         ]
    //                     ]
    //                 ],
    //             ]
    //         ];
    //         if($v["eip_upload"]) $operatetype = 'UPDATE';
    //         $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$api_data);
    //         if($api_result["code"]==1) $this->model->where("PC_Num",$PC_Num)->update(["eip_upload"=>1]);
    //         else $saveData[$v["PC_Num"]] = $api_result["msg"];
    //     }
    //     $msg = "上传成功";
    //     if(!empty($saveData)) {
    //         $msg = "";
    //         foreach($saveData as $k=>$v){
    //             $msg .= $k.$v.";";
    //         }
    //         $json_result["msg"] = $msg;
    //     }else $json_result = ["code"=>1,"msg"=>$msg];
    //     return json($json_result);
    // }
}
