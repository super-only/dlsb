<?php

namespace app\admin\controller\chain\material;

use app\admin\model\chain\material\CommisionTestDetail;
use app\admin\model\chain\material\MaterialGetNotice;
use app\admin\model\chain\material\StoreOutDetail;
use app\admin\model\chain\material\StoreState;
use app\admin\model\logistics\invoice\InvoiceDetail;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
// use jianyan\excel\Excel;

/**
 * 原材料入库
 *
 * @icon fa fa-circle-o
 */
class StoreInDetail extends Backend
{
    
    /**
     * StoreInDetail模型对象
     * @var \app\admin\model\chain\material\StoreInDetail
     */
    protected $model = null,$firstModel,$wareArr,$admin;
    protected $noNeedLogin = ["eipUpload","offerU"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\StoreInDetail;
        $this->firstModel = new \app\admin\model\chain\material\StoreIn;
        $wareList = $deptList = [""=>"请选择"];
        $deptModel = (new \app\admin\model\jichu\jg\Deptdetail())
            ->field("DD_Name")
            ->where(["Valid"=>1])
            ->order(["ParentNum"=>"ASC","DD_Num"=>"ASC"])
            ->select();
        foreach($deptModel as $v){
            $deptList[$v["DD_Name"]] = $v["DD_Name"];
        }
        $wareArr = [];
        $wareModel = (new \app\admin\model\jichu\ch\WareClass())->field("WC_Name,WC_Num")->where("WC_Sort","原材料")->order("WC_Num")->select();
        foreach($wareModel as $v){
            $wareList[$v["WC_Name"]] = $v["WC_Name"];
            $wareArr[$v["WC_Name"]] = $v["WC_Num"];
        }
        $this->wareArr = $wareArr;
        $this->view->assign("deptList",$deptList);
        $this->view->assign("wareList",$wareList);
        $this->admin = \think\Session::get('admin');
        $this->assignconfig("time",date("Y-m-d H:i:s"));
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $ini_where = [];
            if(isset($filter["is_check"])){
                if($filter["is_check"]=="1") $ini_where["si.SI_AudioPepo"] = ["=",""];
                if($filter["is_check"]=="2") $ini_where["si.SI_AudioPepo"] = ["<>",""];
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->firstModel->alias("si")
                ->join(["storeindetail"=>"sid"],"si.SI_OtherID = sid.SI_OtherID")
                ->join(["inventorymaterial"=>"im"],"im.IM_Num = sid.IM_Num","left")
                // ->join(["materialgetnotice"=>"mgn"],"mgn.MGN_ID = sid.MGN_ID")
                ->join(["vendor"=>"v"],"v.V_Num = sid.V_Num","left")
                ->join(["projectcatalog"=>"pcl"],"pcl.PC_Num = si.PC_Num","left")
                ->field("si.offer,si.SI_OtherID,sid.SID_ID,si.SI_InDate,si.SI_WareHouse,sid.IM_Num,im.IM_Class,sid.L_Name,sid.L_Name as 'sid.L_Name',im.IM_Spec,sid.SID_Weight,round(sid.SID_Length/1000,3) as SID_Length,round(sid.SID_Width/1000,3) as SID_Width,sid.SID_Count,sid.SID_FactWeight,sid.SID_RestCount,sid.SID_RestWeight,sid.SID_TestResult,sid.LuPiHao,sid.PiHao,v.V_Name,si.RSC_ID,pcl.PC_ProjectName,(case when si.SI_AudioPepo='' then '未审核' else '已审核' end ) as is_check")
                ->where($where)
                ->where($ini_where)
                ->order($sort,$order)
                ->select();
            $data = [];
            $count = $weight = 0;
            foreach($list as $k=>$v){
                $v["SID_Count"] = round($v["SID_Count"],2);
                $v["SID_FactWeight"] = round($v["SID_FactWeight"],2);
                $v["SID_RestCount"] = round($v["SID_RestCount"],2);
                $v["SID_RestWeight"] = round($v["SID_RestWeight"],2);
                $data[$k] = $v->toArray();
                $data[$k]["SID_Length"] = round($v["SID_Length"],3);
                $data[$k]["SID_Width"] = round($v["SID_Width"],3);
                $count += $v["SID_Count"];
                $weight += $v["SID_FactWeight"];
            }
            $data[] = ["SID_ID"=>"合计","SID_Count"=>round($count,2),"SID_FactWeight"=>round($weight,2)];
            $result = array("total" => count($list), "rows" => array_values($data));

            return json($result);
        }
        $defaultTime = date("Y-m-d 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        $this->assignconfig("rsclist",["到货入库"=>"到货入库","盘盈入库"=>"盘盈入库"]);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $SI_OtherID = $params["SI_OtherID"];
                if($SI_OtherID){
                    $one = $this->firstModel->where("SI_OtherID",$SI_OtherID)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("Ymd");
                    $one = $this->firstModel->field("SI_OtherID")->where("SI_OtherID","LIKE","RK".$month.'-%')->order("SI_OtherID DESC")->find();
                    if($one){
                        $num = substr($one["SI_OtherID"],11);
                        $SI_OtherID = 'RK'.$month.'-'.str_pad(++$num,4,0,STR_PAD_LEFT);
                    }else $SI_OtherID = 'RK'.$month.'-0001';
                }
                $params["SI_OtherID"] = $SI_OtherID;

                $msg = "";
                list($sectSaveList,$msg) = $this->getState($params,$paramsTable);
                if($msg) $this->error($msg);
                $result = false;
                Db::startTrans();
                try {
                    if(!empty($sectSaveList)){
                        $this->firstModel::create($params);
                        $result = $this->model->allowField(true)->saveAll(array_values($sectSaveList));
                    }
                    // if(!empty($state)) (new StoreState())->allowField(true)->saveAll($state);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$SI_OtherID);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->firstModel->alias("f")
            ->join(["projectcatalog"=>"p"],"f.PC_Num = p.PC_Num","LEFT")
            ->field("f.*,p.PC_ProjectName,p.PC_Num")
            ->where("SI_OtherID",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if($row["SI_Flag"]!=1) $this->error("该入库不是到货入库，不能进行修改！");

            $ck_list = $this->model->alias("rk")
                ->join(["storeoutdetail"=>"ck"],"rk.SID_ID = ck.SID_ID")
                ->where("rk.SI_OtherID",$ids)
                ->find();
            if($ck_list) $this->error("已存在出库，不能进行修改！");

            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $SI_OtherID = $ids;
                $msg = "";
                list($sectSaveList,$msg) = $this->getState($row,$paramsTable);
                if($msg) $this->error($msg);
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->firstModel->where("SI_OtherID",$SI_OtherID)->update($params);
                    if(!empty($sectSaveList) and $row["SI_AudioPepo"]=="") $this->model->allowField(true)->saveAll($sectSaveList);
                    // if(!empty($state)) (new StoreState())->allowField(true)->saveAll($state);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        // $list = [];
        $joinList = $this->model->alias("sid")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = sid.IM_Num")
            ->join(["commisiontestdetail"=>"ctd"],"ctd.CTD_ID = sid.MGN_ID","left")
            ->join(["commisiontest"=>"ct"],"ct.CT_Num = ctd.CT_Num","left")
            // ->join(["vendor"=>"v"],"ct.V_Num = v.V_Num","left")
            ->join(["vendor"=>"v"],"v.V_Num = sid.V_Num","left")
            ->join(["materialgetnotice"=>"mgn"],"mgn.MGN_ID = sid.MGN_ID2","left")
            ->join(["materialnote"=>"mn"],"mn.MN_Num = mgn.MN_Num","left")
            ->field("sid.SID_ID,v.V_Name,v.V_Num,im.IM_Num,im.IM_Class,sid.L_Name,im.IM_Spec,round(sid.SID_Length/1000,3) as 'SID_Length',round(sid.SID_Width/1000,3) as 'SID_Width',sid.SID_Count as 'ago_SID_Count',sid.SID_Count,im.IM_PerWeight,sid.SID_Weight as 'ago_SID_Weight',sid.SID_Weight,sid.SID_PlanPrice,0 as 'sumPrice',0 as 'ago_sumPrice',sid.SID_NoTaxPrice,0 as 'sumNoTaxPrice',sid.sid_inside_price,0 as 'planPrice',sid.SID_FRate,sid.SID_TestResult,mgn_DuiFangDian,sid.LuPiHao,sid.PiHao,ctd.CT_Num,sid.SID_Conclusion,sid.SID_testnum,mn.mn_delivernum,mn.mn_license,ctd.CTD_ShowQuality,sid.SID_CTD_Pi,sid.MGN_ID,sid.MGN_ID2,(CASE WHEN sid.SID_Count=0 THEN 0 ELSE round(sid.SID_Weight/sid.SID_Count,2) END) as dz,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("SI_OtherID",$ids)
            ->order("clsort,sid.L_Name,bh,SID_Length ASC")
            ->select();
        // foreach($joinList as $k=>$v){
            // $list[$k] = $v->toArray();
            // $list[$k]["dz"] = $v["SID_Count"]?round($v["SID_Weight"]/$v["SID_Count"],2):0;
            // $list[$k]["ago_sumPrice"] = $list[$k]["sumPrice"] = round($v["SID_Weight"]*$v["SID_PlanPrice"]/1000,2);
            // $list[$k]["sumNoTaxPrice"] = round($v["SID_Weight"]*$v["SID_NoTaxPrice"]/1000,2);
        // }
        $sum_weight = $count = 0;
        $rows = [];
        foreach($joinList as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["SID_Length"] = round($v['SID_Length'],3);
            $rows[$k]["SID_Width"] = round($v['SID_Width'],3);
            $rows[$k]["SID_Count"] = round($v['SID_Count'],2);
            $rows[$k]["ago_SID_Count"] = round($v['ago_SID_Count'],2);
            $rows[$k]["SID_Weight"] = round($v['SID_Weight'],2);
            $rows[$k]["ago_SID_Weight"] = round($v['ago_SID_Weight'],2);
            $sum_weight += $v["SID_Weight"];
            $count += $v["SID_Count"];
        }
        $row["sum_weight"] = $sum_weight;
        $row["count"] = $count;
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("list",$rows);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        // $this->assignconfig("si_other_id",$one["SI_OtherID"]);
        $this->assignconfig('ids',$ids);
        
        return $this->view->fetch();
    }

    public function selectPhysic()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $join_list = [
                "sid.MGN_ID = ctd.CTD_ID",
                "sid.MGN_ID2 = ctd.MGN_ID"
            ];
            $sql_where = ["ctd.CTD_Result"=>["=","合格"]];
            $subsql = (new CommisionTestDetail())->alias("ctd")
                ->join("(select MGN_ID,MGN_ID2,SUM(SID_Count) AS SID_Count,SUM(SID_Weight) AS SID_Weight from storeindetail group by MGN_ID,MGN_ID2) sid",$join_list,"LEFT")
                ->field("ctd.CTD_FactCount,case when isnull(sid.SID_Count) then 0 else SID_Count end as SID_Count,case when isnull(sid.SID_Count) then ctd.CTD_FactCount else (ctd.CTD_FactCount-sid.SID_Count) end as sy_count,(case when isnull(sid.SID_Weight) then CTD_Weight else (ctd.CTD_Weight-sid.SID_Weight) end) as sy_weight,ctd.CTD_ID,ctd.MGN_ID,ctd.CTD_testnum,ctd.LuPiHao,ctd.PiHao,ctd.CTD_Pi,ctd.CTD_Spec,ctd.L_Name,ctd.CTD_Weight,ctd.CTD_UnqualityCount,ctd.CTD_Result,ctd.CT_Num")
                ->having("sy_count > 0 or sy_weight > 0")
                ->where($sql_where)
                ->group("ctd.CTD_ID")
                // ->select(false);pri($subsql,1);
                ->buildSql();
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new MaterialGetNotice())->alias("mgn")
                ->join(["materialnote"=>"mn"],"mn.MN_Num = mgn.MN_Num")
                ->join([$subsql=>"ss"],"mgn.MGN_ID = ss.MGN_ID")
                ->join(["inventorymaterial"=>"im"],"mgn.IM_Num = im.IM_Num","left")
                ->join(["vendor"=>"v"],"v.V_Num = mn.V_Num","left")
                ->field("ss.CTD_ID as MGN_ID,mgn.MGN_ID AS MGN_ID2,mgn.MGN_Destination,mn.MN_Date,ss.CTD_testnum,v.V_Num,v.V_Name,ss.LuPiHao,ss.PiHao,ss.CTD_Pi,im.IM_Class,ss.CTD_Spec,ss.L_Name,mgn.MGN_Length,mgn.MGN_Width,ss.CTD_FactCount,ss.CTD_Weight,round(ss.sy_count,2) as sy_count,round(ss.sy_weight,2) as sy_weight,ss.CTD_UnqualityCount,ss.CTD_Result,mn.MN_Num,ss.CT_Num,mgn.MGN_Maker,mgn.mgn_DuiFangDian,mgn.IM_Num,ss.CTD_Spec as IM_Spec,mgn.MGN_Length as SID_Length,mgn.MGN_Width as SID_Width,ss.sy_count as SID_Count,ss.sy_weight as SID_Weight,mgn.MGN_Maker as SID_TestResult,ss.CTD_testnum as SID_testnum,ss.CTD_Pi as SID_CTD_Pi,mn_license,mn_delivernum,round(mgn.MGN_Weight/mgn.MGN_Count,2) as dz")
                ->where($where)
                ->where("mn.WriteTime",">=",date("Y-m-d", strtotime(' -2 month')))
                ->where("im.ifIMcommision",0)
                ->order($sort, $order)
                ->paginate($limit);
            foreach($list as &$v){
                $v["MGN_Length"] = round($v["MGN_Length"],3);
                $v["MGN_Width"] = round($v["MGN_Width"],3);
                $v["CTD_FactCount"] = round($v["CTD_FactCount"],2);
                $v["CTD_Weight"] = round($v["CTD_Weight"],2);
                $v["sy_count"] = round($v["sy_count"],2);
                $v["sy_weight"] = round($v["sy_weight"],2);
                $v["CTD_UnqualityCount"] = round($v["CTD_UnqualityCount"],2);
                $v["SID_Length"] = round($v["SID_Length"],3);
                $v["SID_Width"] = round($v["SID_Width"],3);
                $v["SID_Count"] = round($v["SID_Count"],2);
                $v["SID_Weight"] = round($v["SID_Weight"],2);
            }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function selectArrival()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $subsql = (new MaterialGetNotice())->alias("mgn")
                ->join(["storeindetail"=>"sid"],"mgn.MGN_ID=sid.MGN_ID2")
                ->field("mgn.MGN_ID,sum(sid.SID_Count) as sy_count,sum(sid.SID_Weight) as sy_weight")
                ->group("mgn.MGN_ID")
                ->buildSql();
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new MaterialGetNotice())->alias("mgn")
                ->join(["materialnote"=>"mn"],"mgn.MN_Num = mn.MN_Num")
                ->join([$subsql=>"ss"],"mgn.MGN_ID = ss.MGN_ID","left")
                ->join(["inventorymaterial"=>"im"],"mgn.IM_Num = im.IM_Num","left")
                ->join(["vendor"=>"v"],"v.V_Num = mn.V_Num","left")
                ->field("'原材料仓库' as MGN_Destination,0 as MGN_ID,mgn.MGN_ID AS MGN_ID2,v.V_Num,v.V_Name,mgn.IM_Num,im.IM_Class,mgn.L_Name,im.IM_Spec,mgn.MGN_Length,mgn.MGN_Length as SID_Length,mgn.MGN_Width,mgn.MGN_Width as SID_Width,mgn.MGN_Count,(CASE WHEN IFNULL(ss.sy_count,0)=0 THEN mgn.MGN_Count ELSE (mgn.MGN_Count-ss.sy_count) END) as SID_Count,mgn.MGN_Weight,(CASE WHEN IFNULL(ss.sy_weight,0)=0 THEN mgn.MGN_Weight ELSE (mgn.MGN_Weight-ss.sy_weight) END) as SID_Weight,round(mgn.MGN_Weight/mgn.MGN_Count,2) as dz,mgn.MGN_Maker as SID_TestResult,mgn.mgn_DuiFangDian,mgn.mgn_testnum as SID_testnum,mn.mn_license,mn.mn_delivernum,mgn.mgn_pi as SID_CTD_Pi,mn.MN_Date,mn.MN_Num")
                ->where($where)
                ->where("im.ifIMcommision","<>",0)
                ->where("mn.WriteTime",">=",date("Y-m-d", strtotime(' -2 month')))
                ->order($sort, $order)
                ->having("SID_Count>0 or SID_Weight>0")
                ->select();
                // ->paginate($limit);
            foreach($list as &$v){
                $v["MGN_Length"] = round($v["MGN_Length"],3);
                $v["MGN_Width"] = round($v["MGN_Width"],3);
                $v["MGN_Count"] = round($v["MGN_Count"],2);
                $v["MGN_Weight"] = round($v["MGN_Weight"],2);
                // $v["sy_count"] = round($v["sy_count"],2);
                // $v["sy_weight"] = round($v["sy_weight"],2);
                // $v["CTD_UnqualityCount"] = round($v["CTD_UnqualityCount"],2);
                $v["SID_Length"] = round($v["SID_Length"],3);
                $v["SID_Width"] = round($v["SID_Width"],3);
                $v["SID_Count"] = round($v["SID_Count"],2);
                $v["SID_Weight"] = round($v["SID_Weight"],2);
            }
            $result = array("total" => count($list), "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    public function allDel()
    {

        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);

        $row = $this->firstModel->where("SI_OtherID",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"该条信息已删除，或者请稍后重试"]);
        else if($row["SI_AudioPepo"]) return json(["code"=>0,"msg"=>"该条信息已审核，或者请稍后重试"]);
        $sod_list = $this->model->alias("sid")
            ->join(["storeoutdetail"=>"sod"],"sid.SID_ID = sod.SID_ID")
            ->where("sid.SI_OtherID",$num)
            ->find();
        if($sod_list) return json(["code"=>0,"msg"=>"该入库单已出库，删除失败，或者请稍后重试"]);
        $result=0;
        Db::startTrans();
        try {
            $result += $this->firstModel->where("SI_OtherID",$num)->delete();
            $result += $this->model->where("SI_OtherID",$num)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result != false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            
            $one = (new StoreOutDetail())->where("SID_ID",$num)->find();
            if($one) return json(["code"=>0,"msg"=>"已经出库，无法进行删除"]);
            $in_detail = $this->model->alias("sid")->join(["storein"=>"si"],"sid.SI_OtherID = si.SI_OtherID")->where("si.SI_Flag",1)->where("sid.SID_ID",$num)->find();
            if(!$in_detail) return json(["code"=>0,"msg"=>"删除失败"]);
            else if($in_detail["SI_AudioPepo"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);
            $result = $this->model->where("SID_ID",$num)->delete();
            if($result) return json(["code"=>1,"msg"=>"删除成功"]);
            else json(["code"=>0,'msg'=>"删除失败"]);
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["ID","SID_ID","text","readonly","","","save"],
            ["MGN_ID","MGN_ID","text","readonly","","hidden","save"],
            ["MGN_ID2","MGN_ID2","text","readonly","","hidden","save"],
            ["供应商","V_Num","text","readonly","","hidden",""],
            ["供应商","V_Name","text","readonly","","",""],
            ["存货编码","IM_Num","text","readonly","","",""],
            ["材料名称","IM_Class","text","readonly","","",""],
            ["材质","L_Name","text","readonly","","",""],
            ["规格","IM_Spec","text","readonly","","",""],
            ["长度(m)","SID_Length","text","readonly",0,"",""],
            ["宽度(m)","SID_Width","text","readonly",0,"",""],
            ["入库数","SID_Count","text","",0,"","save"],
            ["入库数","ago_SID_Count","text","",0,"hidden","save"],
            ["单重","dz","text","readonly",0,"hidden",""],
            // ["比重","IM_PerWeight","text","readonly",0,"hidden",""],
            ["入库重量(kg)","SID_Weight","text","",0,"","save"],
            ["入库重量(kg)","ago_SID_Weight","text","",0,"hidden","save"],
            // ["单价(元/吨)","SID_PlanPrice","text","",0,"",""],
            // ["金额","ago_sumPrice","text","readonly",0,"hidden",""],
            // ["金额","sumPrice","text","readonly",0,"",""],
            // ["不含税单价","SID_NoTaxPrice","text","readonly",0,"",""],
            // ["不含税金额","sumNoTaxPrice","text","readonly",0,"",""],
            // ["计划单价","sid_inside_price","text","",0,"","save"],
            // ["计划金额","planPrice","text","readonly",0,"",""],
            // ["税率","SID_FRate","text","","1.17","","save"],
            ["生产厂家","SID_TestResult","text","readonly","","",""],
            ["堆放点","mgn_DuiFangDian","text","","","","save"],
            ["炉号","LuPiHao","text","readonly","","",""],
            ["批号","PiHao","text","readonly","","",""],
            ["委试号","CT_Num","text","readonly","","",""],
            ["备注","SID_Conclusion","text","","","","save"],
            ["试验编号","SID_testnum","text","readonly","","",""],
            ["送货单号","mn_delivernum","text","readonly","","",""],
            ["车牌号","mn_license","text","readonly","","",""],
            ["进货批次","SID_CTD_Pi","text","","","","save"]
        ];
        return $list;
    }

    protected function getState($one,$paramsTable){
        // $invent_list = $this->imClassNumList();
        $sectSaveList = [];
        $state = [];
        $msg = "";
        // $state_model = new StoreState();
        foreach($paramsTable["SID_ID"] as $k=>$v){
            // $key = $paramsTable["IM_Num"][$k].'-'.$paramsTable["L_Name"][$k].'-'.round($paramsTable["SID_Length"][$k],2).'-'.round($paramsTable["SID_Width"][$k],2);
            // if(isset($sectSaveList[$key])) $msg .= "第".($k+1)."行信息重复;<br>";
            // $material_type = $invent_list[$paramsTable["IM_Class"][$k]];
            $material_name = $paramsTable["IM_Class"][$k];
            $cz_name = $paramsTable["L_Name"][$k];
            preg_match_all("/\d+/",$paramsTable["IM_Spec"][$k],$matches);
            $gg_name = '';
            if(count($matches[0])){
                for($i=0;$i<count($matches[0]);$i++){
                    $gg_name .= str_pad($matches[0][$i],3,0,STR_PAD_LEFT);
                }
            }else $gg_name = "000";
            $length = $paramsTable["SID_Length"][$k]?round($paramsTable["SID_Length"][$k],2):0;
            $combination_id = $length?($length.'米'.$material_name):$material_name;
            $combination_id .= $cz_name.$material_name.$gg_name;
            $sectSaveList[$k] = [
                "SID_ID" => $paramsTable["SID_ID"][$k],
                "SI_OtherID" => $one["SI_OtherID"],
                "MGN_ID" => $paramsTable["MGN_ID"][$k]?$paramsTable["MGN_ID"][$k]:0,
                "CT_Num" => $paramsTable["CT_Num"][$k]?$paramsTable["CT_Num"][$k]:0,
                "IM_Num" => $paramsTable["IM_Num"][$k],
                "L_Name" => $paramsTable["L_Name"][$k],
                "SID_Length" => $paramsTable["SID_Length"][$k]?$paramsTable["SID_Length"][$k]*1000:0,
                "SID_Width" => $paramsTable["SID_Width"][$k]?$paramsTable["SID_Width"][$k]*1000:0,
                "SID_Count" => $paramsTable["SID_Count"][$k]?$paramsTable["SID_Count"][$k]:0,
                "SID_Weight" => $paramsTable["SID_Weight"][$k]?$paramsTable["SID_Weight"][$k]:0,
                "SID_FactWeight" => $paramsTable["SID_Weight"][$k]?$paramsTable["SID_Weight"][$k]:0,
                "SID_RestCount" => $paramsTable["SID_Count"][$k]?$paramsTable["SID_Count"][$k]:0,
                "SID_RestWeight" => $paramsTable["SID_Weight"][$k]?$paramsTable["SID_Weight"][$k]:0,
                "SID_TestResult" => $paramsTable["SID_TestResult"][$k],
                "LuPiHao" => $paramsTable["LuPiHao"][$k],
                "PiHao" => $paramsTable["PiHao"][$k],
                "SID_Conclusion" => $paramsTable["SID_Conclusion"][$k],
                "SID_testnum" => $paramsTable["SID_testnum"][$k],
                "mn_delivernum" => $paramsTable["mn_delivernum"][$k],
                "mn_license" => $paramsTable["mn_license"][$k],
                "SID_CTD_Pi" => $paramsTable["SID_CTD_Pi"][$k],
                "V_Num" => $paramsTable["V_Num"][$k],
                "MGN_ID2" => $paramsTable["MGN_ID2"][$k],
                "combination_id" => $combination_id
            ];
            if(!$v){
                $iniCount = 0;
                $sectSaveList[$k]["order"] = $iniCount;
                unset($sectSaveList[$k]["SID_ID"]);
                $inList = $this->model->field("order,SID_ID")->where([
                    "IM_Num" => ["=",$paramsTable["IM_Num"][$k]],
                    "L_Name" => ["=",$paramsTable["L_Name"][$k]],
                    "SID_Length" => ["=",$paramsTable["SID_Length"][$k]*1000],
                    "SID_Width" => ["=",$paramsTable["SID_Width"][$k]*1000],
                    "SID_RestWeight" => [">",0]
                ])->order("order asc")->select();
                foreach($inList as $k=>$v){
                    $iniCount++;
                    $state[$v["SID_ID"]] = ["SID_ID"=>$v["SID_ID"],"order"=>$iniCount];
                }
            }
            // if($v){
            //     $self_one = $this->model->field("order")->where("SID_ID",$v)->find();
            //     if($self_one) $sectSaveList[$k]["order"] = $self_one["order"];
            //     else{
            //         $in_one = $this->model->field("order")->where([
            //             "IM_Num" => ["=",$paramsTable["IM_Num"][$k]],
            //             "L_Name" => ["=",$paramsTable["L_Name"][$k]],
            //             "SID_Length" => ["=",$paramsTable["SID_Length"][$k]*1000],
            //             "SID_Width" => ["=",$paramsTable["SID_Width"][$k]*1000],
            //         ])->order("order desc")->find();
            //         if($in_one) $sectSaveList[$k]["order"] = $in_one["order"]+1;
            //         else $sectSaveList[$k]["order"] = 0;
            //     }
            // }else{
            //     $in_one = $this->model->field("order")->where([
            //         "IM_Num" => ["=",$paramsTable["IM_Num"][$k]],
            //         "L_Name" => ["=",$paramsTable["L_Name"][$k]],
            //         "SID_Length" => ["=",$paramsTable["SID_Length"][$k]*1000],
            //         "SID_Width" => ["=",$paramsTable["SID_Width"][$k]*1000],
            //     ])->order("order desc")->find();
            //     if($in_one) $sectSaveList[$k]["order"] = $in_one["order"]+1;
            //     else $sectSaveList[$k]["order"] = 0;
            //     unset($sectSaveList[$k]["SID_ID"]);
            // }
        }
        foreach($state as $k=>$v){
            $sectSaveList[] = $v;
        }
        return [$sectSaveList,$msg];
    }
    /**
     * 打印入库单
     */
    public function rkdPrint($ids = null)
    {
        $row = $this->firstModel->alias("f")
            ->join(["projectcatalog"=>"p"],"f.PC_Num = p.PC_Num","LEFT")
            ->field("f.*,p.PC_ProjectName,p.PC_Num")
            ->where("SI_OtherID",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        // $list = [];
        $joinList = $this->model->alias("sid")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = sid.IM_Num")
            ->join(["commisiontestdetail"=>"ctd"],"ctd.CTD_ID = sid.MGN_ID","left")
            ->join(["commisiontest"=>"ct"],"ct.CT_Num = ctd.CT_Num","left")
            // ->join(["vendor"=>"v"],"ct.V_Num = v.V_Num","left")
            ->join(["vendor"=>"v"],"v.V_Num = sid.V_Num","left")
            ->join(["materialgetnotice"=>"mgn"],"mgn.MGN_ID = sid.MGN_ID2","left")
            ->join(["materialnote"=>"mn"],"mn.MN_Num = mgn.MN_Num","left")
            ->field("sid.SID_ID,v.V_Name,v.V_Num,im.IM_Num,im.IM_Class,sid.L_Name,im.IM_Spec,round(sid.SID_Length/1000,2) as 'SID_Length',round(sid.SID_Width/1000,2) as 'SID_Width',sid.SID_Count as 'ago_SID_Count',sid.SID_Count,im.IM_PerWeight,sid.SID_Weight as 'ago_SID_Weight',sid.SID_Weight,sid.SID_PlanPrice,0 as 'sumPrice',0 as 'ago_sumPrice',sid.SID_NoTaxPrice,0 as 'sumNoTaxPrice',sid.sid_inside_price,0 as 'planPrice',sid.SID_FRate,sid.SID_TestResult,mgn_DuiFangDian,sid.LuPiHao,sid.PiHao,ctd.CT_Num,sid.SID_Conclusion,sid.SID_testnum,mn.mn_delivernum,mn.mn_license,ctd.CTD_ShowQuality,sid.SID_CTD_Pi,sid.MGN_ID,sid.MGN_ID2,(CASE WHEN sid.SID_Count=0 THEN 0 ELSE round(sid.SID_Weight/sid.SID_Count,2) END) as dz,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("SI_OtherID",$ids)
            ->order("clsort,sid.L_Name,bh,SID_Length ASC")
            ->select();
        $sum_weight = $count = 0;
        $rows = [];
        foreach($joinList as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["SID_Length"] = round($v['SID_Length'],3);
            $rows[$k]["SID_Width"] = round($v['SID_Width'],3);
            $rows[$k]["SID_Count"] = round($v['SID_Count'],2);
            $rows[$k]["ago_SID_Count"] = round($v['ago_SID_Count'],2);
            $rows[$k]["SID_Weight"] = round($v['SID_Weight'],2);
            $rows[$k]["ago_SID_Weight"] = round($v['ago_SID_Weight'],2);
            $sum_weight += $v["SID_Weight"];
            $count += $v["SID_Count"];
        }
        $row["sum_weight"] = $sum_weight;
        $row["count"] = $count;
        
        $mainInfos = [];
        $mainInfos["SI_OtherID"] = $row["SI_OtherID"];
        $mainInfos["SI_WareHouse"] = $row["SI_WareHouse"];
        $mainInfos["RSC_ID"] = $row["RSC_ID"];
        $date=date_create($row["SI_InDate"]);
        $mainInfos["SI_InDate"] = date_format($date, "Y-m-d");
        $mainInfos["SID_TestResult"] = count($rows)>0?$rows[0]["SID_TestResult"]:"";
        $mainInfos["SI_Memo"] = $row["SI_Memo"];
        $mainInfos["SI_Writer"] = $row["SI_Writer"];
        $mainInfos["PC_ProjectName"] = $row["PC_ProjectName"];
        // $mainInfos["SI_AudioPepo"] = $row["SI_AudioPepo"];

        $this->assignconfig("mainInfos",$mainInfos);
        $this->assignconfig("list",$rows);
        return $this->view->fetch();
    }

    
    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $one = $this->firstModel->where("SI_OtherID",$num)->find();
        if($one["SI_AudioPepo"]) return json(["code"=>0,"msg"=>"已审核"]);
        $state_model = new StoreState();
        $list = $this->firstModel->alias("si")->join(["storeindetail"=>"sid"],"si.SI_OtherID = sid.SI_OtherID")->join(["inventorymaterial"=>"im"],"im.IM_Num = sid.IM_Num")->where("si.SI_OtherID",$num)->select();
        $state = $this->getStateList($list);
        $result = false;
        Db::startTrans();
        try {
            $result = $this->firstModel->where("SI_OtherID",$num)->update(['SI_OtherID' => $num,'SI_AudioPepo'=>$admin,"SI_AudioDate"=>date("Y-m-d H:i:s")]);
            $state_model->allowField(true)->saveAll($state);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
        
    }


    public function giveUp()
    {
        $num = $this->request->post("num");
        $one = $this->firstModel->where("SI_OtherID",$num)->find();
        if(!$one["SI_AudioPepo"]) return json(["code"=>0,"msg"=>"已弃核"]);
        if($one["SI_Flag"]==0) return json(["code"=>0,"msg"=>"无法弃审"]);
        $state_model = new StoreState();
        $list = $this->firstModel->alias("si")->join(["storeindetail"=>"sid"],"si.SI_OtherID = sid.SI_OtherID")->join(["inventorymaterial"=>"im"],"im.IM_Num = sid.IM_Num")->where("si.SI_OtherID",$num)->select();
        $sidList = array_column($list,'SID_ID');
        $findOne = (new InvoiceDetail())->where("sid_id","in",$sidList)->find();
        if($findOne) return json(["code"=>0,"msg"=>"已存在开票，无法弃审，请稍后再试"]);
        list($state,$msg) = $this->getStateUpList($list);
        if($msg) return json(["code"=>0,"msg"=>"库存有误，无法弃审"]);
        $result = false;
        Db::startTrans();
        try {
            $result = $this->firstModel->where("SI_OtherID",$num)->update(['SI_OtherID' => $num,'SI_AudioPepo'=>"","SI_AudioDate"=>"0000-00-00 00:00:00"]);
            $state_model->allowField(true)->saveAll($state);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function storeInDetailNum()
    {
        $list = $this->model->alias("sid")
            ->join(["inventorymaterial"=>"im"],"sid.IM_Num = im.IM_Num")
            ->field("sid.IM_Num,sid.SID_ID,sid.L_Name,sid.SID_Length,im.IM_Spec,im.IM_Class")
            ->select();
        $save_list = [];
        foreach($list as $v){
            $material_name = $v["IM_Class"];
            $cz_name = $v["L_Name"];
            preg_match_all("/\d+/",$v["IM_Spec"],$matches);
            $gg_name = '';
            if(count($matches[0])){
                for($i=0;$i<count($matches[0]);$i++){
                    $gg_name .= str_pad($matches[0][$i],3,0,STR_PAD_LEFT);
                }
            }else $gg_name = "000";
            $length = $v["SID_Length"]?round($v["SID_Length"]/1000,2):0;
            $combination_id = $length?($length.'米'.$material_name):$material_name;
            $combination_id .= $cz_name.$material_name.$gg_name;
            $save_list[] = [
                "SID_ID" => $v["SID_ID"],
                "combination_id" => $combination_id
            ];
        }
        $result = $this->model->allowField(true)->saveAll($save_list);
        pri(count($result));
    }

    public function offerU()
    {
        $id = $this->request->post("id");
        $row = $this->firstModel->where("SI_OtherID",$id)->find();
        $res = [
            "code"=>0,
            "msg" => "有误",
            "data" => []
        ];
        if(!$row) return json($res);
        else if($row["SI_AudioPepo"]==''){
            $res["msg"] = "请先审核";
            return json($res);
        }
        $cvencode = $this->model->alias("a")
            ->join(["materialgetnotice"=>"ctd"],"a.MGN_ID2=ctd.MGN_ID")
            ->join(["materialnote"=>"ct"],"ctd.MN_Num=ct.MN_Num")
            ->join(["vendor"=>"v"],"v.V_Num=ct.V_Num")
            ->where("a.SI_OtherID",$id)
            ->where("ct.V_Num","<>",'')
            // ->select(false);
            ->value("v.V_Name");
        if(!$cvencode){
            $res["msg"] = "不存在供应商编号";
            return json($res);
        }
        $cwhcode = $row["SI_WareHouse"];
        if(!$cwhcode){
            $res["msg"] = "不存在仓库编号";
            return json($res);
        }
        $rscList = [
            "到货入库" => '101',
            "外加工入库" => "106"
        ];
        $crdcode = $rscList[$row["RSC_ID"]]??'';
        if(!$crdcode){
            $res["msg"] = "不存在该收发类别";
            return json($res);
        }
        $main = [
            "ccode" => $id,
            //采购类型编码
            "cptcode" => "1",
            //收发类别
            "crdcode" => $crdcode,
            "cvenname" => $cvencode,
            "cwhname" => $cwhcode,
            "ddate" => date("Y-m-d",strtotime($row["SI_ArriveDate"])),
            "cmaker" => $row['SI_Writer'],
            "cmemo" => $row['SI_Memo'],
            "chandler" => $row['SI_AudioPepo'],
            "dveridate" => date("Y-m-d",strtotime($row["SI_AudioDate"])),
        ];
        $list = $this->model
            ->where("SI_OtherID",$id)
            ->select();
        $details = [];
        foreach($list as $k=>$v){
            $details[$k] = [
                "cinvcode" => $v["IM_Num"],
                "iquantity" => $v["SID_FactWeight"],
                "iunitcost" => 0,
                "cfree1" => $v["L_Name"],
                "cfree2" => '',
                "cfree3" => $v["SID_Length"]>0?round($v["SID_Length"]/1000,3):'',
                "inum" => $v["SID_Count"]
            ];
        }
        $data = [
            "main" => $main,
            "details" => $details
        ];
        $url = "/PostRdrecord01/Add";
        $result = api_post_u($url,$data);
        if($result["code"]==0){
            $this->firstModel->where("SI_OtherID",$id)->update(["offer"=>1]);
            $res["code"] = 1;
            $res["msg"] = "成功";
        }else $res["msg"] = $result["ErrorMsg"];
        return json($res);
    }

    // public function eipUpload()
    // {
    //     $id = $this->request->post("id");
    //     $json_result = ["code"=>0,"msg"=>"失败"];
    //     if(!$id) return json($json_result);
    //     $bzList = $this->model->alias("sid")->join(["inventorymaterial"=>"im"],"sid.IM_Num=im.IM_Num")->where("sid.SI_OtherID",$id)->column("concat(im.IM_Class,sid.SID_Length,sid.L_Name,im.IM_Spec) AS bz");
    //     $params = (new \app\admin\model\view\StateStoreView())->where("bz","IN",$bzList)->select();
    //     if(!$params) return json($json_result);
    //     $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
    //     $stateBzModel = new \app\admin\model\chain\material\StateBz();
    //     $errorData = $saveData = $eipUploadList = [];
    //     $param_url = "eipmatinventory";
    //     // $materialList= [
    //     //     "角钢" => "01001",
    //     //     "钢板" => "01002",
    //     //     "圆管" => "01003",
    //     //     "钢管" => "01004",
    //     //     "辅助材料" => "01007",
    //     // ];
    //     foreach($params as $k=>$v){
    //         $eipUploadList[$v["bz"]] = ["eip_upload"=>$v["eip_upload"],"bz"=>$v["bz"]];
    //         $v["id"]?$eipUploadList[$v["bz"]]["id"] = $v["id"]:"";
    //         isset($api_data[$v["bz"]])?"":$api_data[$v["bz"]] = [
    //             "purchaserHqCode" => "SGCC",
    //             "supplierCode" => "1000014615",
    //             "categoryCode" => "60",
    //             "subclassCode" => "60001",
    //             "matName" => $v["IM_Class"],
    //             "matCode" => $v["IM_Class"]=="角钢"?"MAT_4526358899591262803":"MAT_4526358899591262804",
    //             "matNum" => "".$v["SID_RestWeight"],
    //             "matUnit" => "kg",
    //             "matVoltageLevel" => "1000KV",
    //             "matProdAddr" => "中国",
    //             "speModels" => $v["bz"],
    //             "putStorageTime" => date("Y-m-d",strtotime($v["SI_WriteDate"]))
    //         ];
    //     }
    //     foreach($api_data as $k=>$v){
    //         $operatetype = "ADD";
    //         if($eipUploadList[$k]["eip_upload"]){
    //             $operatetype = "UPDATE";
    //         }
    //         $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$v);
    //         if($api_result["code"]==1){
    //             $saveData[$k] = $eipUploadList[$k];
    //             $saveData[$k]["eip_upload"] = 1;
    //         }else $errorData[$k] = $api_result["msg"];
    //     }
    //     $msg = "上传成功";
    //     if(!empty($errorData)) {
    //         $msg = "";
    //         foreach($errorData as $k=>$v){
    //             $msg .= $k.$v.";";
    //         }
    //         $json_result["msg"] = $msg;
    //     }else $json_result = ["code"=>1,"msg"=>$msg];
    //     if(!empty($saveData)) $stateBzModel->allowField(true)->saveAll($saveData);
    //     return json($json_result);
    // }

}
