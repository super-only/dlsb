<?php

namespace app\admin\controller\chain\material;

use app\admin\model\chain\material\StoreIn;
use app\admin\model\chain\material\StoreInDetail;
use app\admin\model\chain\material\StoreState;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 原材料盘点
 *
 * @icon fa fa-circle-o
 */
class storestatecheck extends Backend
{
    
    /**
     * storestatecheck模型对象
     * @var \app\admin\model\chain\material\storestatecheck
     */
    protected $model = null;
    protected $noNeedLogin = ["chooseMaterialWare"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\StoreStateCheck;
        $this->detailModel = new \app\admin\model\chain\material\StoreStateCheckDetail;
        $this->ssc_list = [0=>"盘盈入库",1=>"盘亏出库"];
        $this->admin = \think\Session::get('admin');
        $this->assignconfig("time",date("Y-m-d H:i:s"));
        $this->view->assign("ssc_list", $this->ssc_list);
        $this->assignconfig('ssc_list',$this->ssc_list);
        $this->assignconfig("limberList",$this->limberList());
        $this->assignconfig("inventoryList",$this->inventoryList());
        $this->assignconfig('wareclassList',$this->wareclassList());
        $this->assignconfig("wareclassList",$this->wareclassList());
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $ini_where = [];
            if(isset($filter["is_check"])){
                if($filter["is_check"]=="1") $ini_where["ssc.Auditor"] = ["=",""];
                if($filter["is_check"]=="2") $ini_where["ssc.Auditor"] = ["<>",""];
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("ssc")
                ->join(["storestatecheckdetail"=>"sscd"],"ssc.SSC_Num = sscd.SSC_Num")
                ->join(["inventorymaterial"=>"im"],"im.IM_Num = sscd.IM_Num")
                ->field("ssc.SSC_Num as 'ssc.SSC_Num',ssc.SSC_Num,CASE WHEN ssc.SSC_Flag = 0 THEN '盘盈入库' WHEN ssc.SSC_Flag = 1 THEN '盘亏出库' WHEN ssc.SSC_Flag = 2 THEN '盘点冲账' ELSE '盘点库存' END AS SSC_Flag,ssc.SSC_Date,ssc.SSC_WareHouse,im.IM_Class,im.IM_Rax,sscd.L_Name,im.IM_Spec,sscd.SSCD_Length,sscd.SSCD_Width,sscd.SSCD_Count,sscd.SSCD_Weight,sscd.LuPiHao,sscd.PiHao,sscd.testnum,sscd.SSCD_Memo,SSCD_ID,(case when ssc.Auditor='' then '未审核' else '已审核' end ) as is_check")
                ->where($where)
                ->where($ini_where)
                ->order($sort,$order)
                ->order("WriteDate DESC")
                ->paginate($limit);
            foreach($list as &$v){
                $v["SSCD_Length"] = round($v["SSCD_Length"],3);
                $v["SSCD_Width"] = round($v["SSCD_Width"],3);
                $v["SSCD_Count"] = round($v["SSCD_Count"],2);
                $v["SSCD_Weight"] = round($v["SSCD_Weight"],2);
            }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $SSC_Num = $params["SSC_Num"];
                if($SSC_Num){
                    $one = $this->model->where("SSC_Num",$SSC_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("ym");
                    $one = $this->model->field("SSC_Num")->where("SSC_Num","LIKE","PD".$month.'-%')->order("SSC_Num DESC")->find();
                    if($one){
                        $num = substr($one["SSC_Num"],7);
                        $SSC_Num = 'PD'.$month.'-'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $SSC_Num = 'PD'.$month.'-001';
                }
                $params["SSC_Num"] = $SSC_Num;
                $sectSaveList = [];
                $msg = "";
                list($sectSaveList,$msg,$store_in_list) = $this->getState($params,$paramsTable);
                // pri($paramsTable,1);
                
                
                if($msg != "") $this->error($msg);
                
                $result = false;
                Db::startTrans();
                try {
                    $this->model::create($params);
                    $result = $this->detailModel->allowField(true)->saveAll(array_values($sectSaveList));
                    if(!empty($store_in_list)) (new StoreInDetail())->allowField(true)->saveAll(array_values($store_in_list));
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$SSC_Num);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d")
        ];
        // $tableField = $this->getDetailField();
        $tableField = $this->getDetailField(0);
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        list($limberList,$kj_limberList) = $this->getLimber();
        $this->view->assign("limberList", $limberList);
        $this->assignconfig("limberList", $limberList);
        $this->assignconfig("kj_limberList", $kj_limberList);
        $this->view->assign("wareclassList",$this->wareclassList());
        return $this->view->fetch();
    }

    //ids 为sod_id
    public function edit($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row["SSC_Flag"]>1) $this->error('无法查看');
        $adminIds = $this->getDataLimitAdminIds();

        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $SSC_Num = $row["SSC_Num"];
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $params["SSC_Flag"] = $row["SSC_Flag"];
                $params["SSC_Num"] = $row["SSC_Num"];
                $sectSaveList = [];
                $msg = "";
                list($sectSaveList,$msg,$store_in_list) = $this->getState($params,$paramsTable);
                if($msg != "") $this->error($msg);
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("SSC_Num",$ids)->update($params);
                    $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    if(!empty($store_in_list)) (new StoreInDetail())->allowField(true)->saveAll(array_values($store_in_list));
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $list = DB::query("SELECT storestatecheck.SSC_Num, storestatecheck.SI_OtherID, storestatecheck.SSC_Flag,  CASE WHEN storestatecheck.SSC_Flag = 0 THEN '盘盈入库' WHEN storestatecheck.SSC_Flag = 1 THEN '盘亏出库' WHEN storestatecheck.SSC_Flag = 2 THEN '盘点冲账' ELSE '盘点库存' END AS Flag, storestatecheck.SSC_Date, storestatecheck.SSC_WareHouse, storestatecheck.Writer, storestatecheck.WriteDate, storestatecheck.Auditor, storestatecheck.AudiDate, storestatecheck.Approver, storestatecheck.AppDate, storestatecheck.SSC_Memo, storestatecheckDetail.SSCD_ID, storestatecheckDetail.SID_TestResult, storestatecheckDetail.SID_ID, storestatecheckDetail.IM_Num, storestatecheckDetail.L_Name, storestatecheckDetail.SSCD_Length, storein.SI_InDate, storestatecheckDetail.SSCD_Width, storestatecheckDetail.SSCD_Count, storestatecheckDetail.SSCD_Weight, storestatecheckDetail.SSCD_Price, storestatecheckDetail.SSCD_Money,storestatecheckDetail.SSCD_Count as ago_SSCD_Count,storestatecheckDetail.SSCD_Weight as ago_SSCD_Weight, storestatecheckDetail.LuPiHao, storestatecheckDetail.PiHao, storestatecheckDetail.SSCD_Memo, inventorymaterial.IM_PerWeight, inventorymaterial.IM_Name, inventorymaterial.IM_Spec, inventorymaterial.IM_Class, storeindetail.SID_RestCount, storeindetail.SID_RestWeight, storestatecheckDetail.CurrentSSCount, storestatecheckDetail.CurrentSSWeight, storestatecheckDetail.CurrentPDCount, storestatecheckDetail.CurrentPDWeight, storestate.SS_Count, storestate.SS_Weight, storeindetail.SID_testnum, storeindetail.SID_CTD_Pi, storestatecheckDetail.testnum, CASE inventorymaterial.IM_Rax WHEN 0 THEN storestatecheckDetail.SSCD_Price ELSE round(storestatecheckDetail.SSCD_Price /IFNULL(inventorymaterial.IM_Rax,1.17),2) END AS SSCD_NaxPrice, CAST(CASE inventorymaterial.IM_Rax WHEN 0 THEN	 storestatecheckDetail.SSCD_Money ELSE storestatecheckDetail.SSCD_Money /IFNULL(inventorymaterial.IM_Rax,1.17) END as decimal(18,3)) AS SSCD_NaxMoney, storestatecheckDetail.SSCD_CTD_ID, materialgetnotice.MGN_Count, materialnote.MN_Date,IFNULL(inventorymaterial.IM_Rax,1.17) AS IM_Rax ,case inventorymaterial.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
        when inventorymaterial.IM_Class='角钢' then 
        SUBSTR(REPLACE(inventorymaterial.IM_Spec,'∠',''),1,locate('*',REPLACE(inventorymaterial.IM_Spec,'∠',''))-1)*1000+
        SUBSTR(REPLACE(inventorymaterial.IM_Spec,'∠',''),locate('*',REPLACE(inventorymaterial.IM_Spec,'∠',''))+1,2)*1
        when (inventorymaterial.IM_Class='钢板' or inventorymaterial.IM_Class='法兰') then 
        REPLACE(inventorymaterial.IM_Spec,'-','')
        else 0
        end) as UNSIGNED) bh FROM materialgetnotice INNER JOIN materialnote ON materialgetnotice.MN_Num = materialnote.MN_Num RIGHT OUTER JOIN storeindetail INNER JOIN storein ON storeindetail.SI_OtherID = storein.SI_OtherID ON materialgetnotice.MGN_ID = storeindetail.MGN_ID2 RIGHT OUTER JOIN storestate RIGHT OUTER JOIN storestatecheck INNER JOIN storestatecheckDetail ON storestatecheck.SSC_Num = storestatecheckDetail.SSC_Num INNER JOIN inventorymaterial ON storestatecheckDetail.IM_Num = inventorymaterial.IM_Num ON storestate.SS_Length = storestatecheckDetail.SSCD_Length AND storestate.IM_Num = storestatecheckDetail.IM_Num AND storestate.SS_WareHouse = storestatecheck.SSC_WareHouse AND storestate.L_Name = storestatecheckDetail.L_Name AND storestate.SS_Width = storestatecheckDetail.SSCD_Width ON storeindetail.SID_ID = storestatecheckDetail.SID_ID WHERE storestatecheck.SSC_Num = '".$SSC_Num."' group by SSCD_ID ORDER BY clsort,storestatecheckDetail.L_Name,bh,storestatecheckDetail.SSCD_Length ASC");

        $sum_weight = $count = 0;
        foreach($list as $k=>$v){
            $list[$k]["SSCD_Length"] = round($v['SSCD_Length'],2);
            $list[$k]["SSCD_Width"] = round($v['SSCD_Width'],2);
            $list[$k]["SSCD_Count"] = round($v['SSCD_Count'],2);
            $list[$k]["SSCD_Weight"] = round($v['SSCD_Weight'],2);
            if($row["Auditor"]==""){
                $list[$k]["SID_RestCount"] += $v["SSCD_Count"];
                $list[$k]["SID_RestWeight"] += $v["SSCD_Weight"];
            }
            $sum_weight += $v["SSCD_Weight"];
            $count += $v["SSCD_Count"];
        }
        $row["sum_weight"] = round($sum_weight,2);
        $row["count"] = round($count,2);
        $tableField = $this->getDetailField($row["SSC_Flag"]);
        $this->view->assign("row", $row);
        $this->view->assign("list", $list);
        $this->view->assign("wareclassList",$this->wareclassList());
        $this->view->assign("tableField", $tableField);
        $this->assignconfig("tableField", $tableField);
        $this->assignconfig("SSC_Num", $row["SSC_Num"]);
        list($limberList,$kj_limberList) = $this->getLimber();
        $this->view->assign("limberList", $limberList);
        $this->assignconfig("limberList", $limberList);
        $this->assignconfig("kj_limberList", $kj_limberList);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function allDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);

        $row = $this->model->where("SSC_Num",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"该条信息已删除，或者请稍后重试"]);
        else if($row["Auditor"]) return json(["code"=>0,"msg"=>"该条信息已审核，或者请稍后重试"]);
        $result=0;
        Db::startTrans();
        try {
            $result += $this->model->where("SSC_Num",$num)->delete();
            $result += $this->detailModel->where("SSC_Num",$num)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result != false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

    public function examine()
    {
        $SSC_Num = $this->request->post("num");
        if(!$SSC_Num) return json(["code"=>0,"msg"=>"审核失败！"]);
        $check_one = $this->model->where("SSC_Num",$SSC_Num)->find();
        if(!$check_one) return json(["code"=>0,"msg"=>"审核失败！"]);
        //审核入库
        $store_model = new StoreIn();
        $store_in_model = new StoreInDetail();
        $state_model = new StoreState();
        if($check_one["SSC_Flag"]==0){
            $check_list = $this->detailModel->alias("d")->join(["inventorymaterial"=>"im"],"d.IM_Num=im.IM_Num")->field("d.*,im.IM_Spec")->where("d.SSC_Num",$SSC_Num)->select();
            if(!$check_list) return json(["code"=>0,"msg"=>"审核失败！"]);
            $month = date("Ymd");
            $one = $this->model->field("SI_OtherID")->where("SI_OtherID","LIKE","PD".$month.'-%')->order("SI_OtherID DESC")->find();
            if($one){
                $num = substr($one["SI_OtherID"],11);
                $SI_OtherID = 'PD'.$month.'-'.str_pad(++$num,4,0,STR_PAD_LEFT);
            }else $SI_OtherID = 'PD'.$month.'-0001';
            $params = [
                "SI_OtherID"=>$SI_OtherID,
                "SI_ArriveDate"=>$check_one["SSC_Date"],
                "SI_Writer"=>$this->admin["nickname"],
                "SI_Flag"=>0,
                "SI_WareHouse"=>$check_one["SSC_WareHouse"],
                "RSC_ID"=>"盘盈入库",
                "SI_InDate"=>$check_one["SSC_Date"],
                "SI_Memo"=>"盘盈入库,单号:".$check_one["SSC_Num"],
                "SI_AudioPepo" => $this->admin["nickname"],
                "SI_AudioDate" => date("Y-m-d H:i:s")
            ];
            $sectSaveList = [];
            foreach($check_list as $v){
                $sectSaveList[$v["SSCD_ID"]] = [
                    "SI_OtherID" => $SI_OtherID,
                    "IM_Num" => $v["IM_Num"],
                    "L_Name" => $v["L_Name"],
                    "SID_Length" => $v["SSCD_Length"],
                    "SID_Width" => $v["SSCD_Width"],
                    "SID_Count" => $v["SSCD_Count"],
                    "SID_Weight" => $v["SSCD_Weight"],
                    "SID_FactWeight" => $v["SSCD_Weight"],
                    "SID_RestCount" => $v["SSCD_Count"],
                    "SID_RestWeight" => $v["SSCD_Weight"],
                    "SID_TestResult" => $v["SID_TestResult"],
                    "LuPiHao" => $v["LuPiHao"],
                    "PiHao" => $v["PiHao"],
                    "SID_testnum" => $v["testnum"]
                ];
            }
            if(empty($sectSaveList)) return json(["code"=>0,"msg"=>"审核失败！"]);
            $result = false;
            Db::startTrans();
            try {
                $this->model->where("SSC_Num",$check_one["SSC_Num"])->update(["SI_OtherID"=>$SI_OtherID,"Auditor"=>$this->admin["nickname"],"AudiDate"=>date("Y-m-d H:i:s")]);
                $store_model->allowField(true)->save($params);
                $result = $store_in_model->allowField(true)->saveAll($sectSaveList);
                $list = $store_model->alias("si")->join(["storeindetail"=>"sid"],"si.SI_OtherID = sid.SI_OtherID")->join(["inventorymaterial"=>"im"],"im.IM_Num = sid.IM_Num")->where("si.SI_OtherID",$SI_OtherID)->select();
                $state = $this->getStateList($list);
                $state_model->allowField(true)->saveAll($state);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }else{
            $list = $this->model->alias("so")->join(["storestatecheckdetail"=>"sod"],"so.SSC_Num = sod.SSC_Num")->join(["inventorymaterial"=>"im"],"im.IM_Num = sod.IM_Num")->where("so.SSC_Num",$SSC_Num)->select();
            $sectSaveList = $state = [];
            foreach($list as $k=>$v){
                $key = $v["IM_Num"].'-'.$v["L_Name"].'-'.round($v["SSCD_Length"],2).'-'.round($v["SSCD_Width"],2);
                if(!isset($sectSaveList[$key])){
                    $sectSaveList[$key] = [
                        "IM_Num" => $v["IM_Num"],
                        "IM_Spec" => $v["IM_Spec"],
                        "L_Name" => $v["L_Name"],
                        "SSCD_Length" => $v["SSCD_Length"],
                        "SSCD_Width" => $v["SSCD_Width"],
                        "SS_WareHouse" => $v["SSC_WareHouse"],
                        "SSCD_Count" => $v["SSCD_Count"],
                        "SSCD_Weight" => $v["SSCD_Weight"]
                    ];
                }else{
                    $sectSaveList[$key]["SSCD_Count"] += $v["SSCD_Count"];
                    $sectSaveList[$key]["SSCD_Weight"] += $v["SSCD_Weight"];
                }
            }
            foreach($sectSaveList as $k=>$v){
                $where = [
                    "IM_Num" => ["=",$v["IM_Num"]],
                    "L_Name" => ["=",$v["L_Name"]],
                    "SS_Length" => ["=",$v["SSCD_Length"]],
                    "SS_Width" => ["=",$v["SSCD_Width"]],
                    "SS_WareHouse" => ["=",$v["SS_WareHouse"]]
                ];
                $state_one = $state_model->where($where)->find();
                $edit_one = [];
                if($state_one){
                    $edit_one = $state_one->toArray();
                    $edit_one["SS_Count"] = $edit_one["SS_Count"]-$v["SSCD_Count"];
                    $edit_one["SS_Weight"] = $edit_one["SS_Weight"]-$v["SSCD_Weight"];
                }else return json(["code"=>0,"msg"=>"库存有误！"]);
                $state[] = $edit_one;
            }
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->where("SSC_Num",$check_one["SSC_Num"])->update(["Auditor"=>$this->admin["nickname"],"AudiDate"=>date("Y-m-d H:i:s")]);
                $state_model->allowField(true)->saveAll($state);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"成功"]);
        } else {
            return json(["code"=>1,"msg"=>"成功，但无数据更新"]);
        }
    }

    protected function storeOutGetState($params,$paramsTable)
    {
        $sectSaveList = $store_in_list = [];
        $msg = "";
        // $store_in_list = [];
        $store_in_model = new StoreInDetail();
        foreach($paramsTable["SOD_ID"] as $k=>$v){
            if(round($paramsTable["SOD_Count"][$k],2)==false and round($paramsTable["SOD_Weight"][$k],2)==false) $msg .= "第".($k+1)."行出库数量和出库重量不能都为0;<br>";
            $in_detail_one = $store_in_model->where("SID_ID",$paramsTable["SID_ID"][$k])->find();
            if($in_detail_one){
                isset($store_in_list[$in_detail_one["SID_ID"]])?"":$store_in_list[$in_detail_one["SID_ID"]]=["SID_ID"=>$paramsTable["SID_ID"][$k],"SID_RestCount"=>$in_detail_one["SID_RestCount"],"SID_RestWeight"=>$in_detail_one["SID_RestWeight"]];
            }else{
                $msg .= "第".($k+1)."入库单出错，请删除;<br>";
                break;
            }
            $sectSaveList[$k] = [
                "SOD_ID" => $paramsTable["SOD_ID"][$k],
                "IM_Num" => $paramsTable["IM_Num"][$k],
                "L_Name" => $paramsTable["L_Name"][$k],
                "SOD_Count" => $paramsTable["SOD_Count"][$k]==false?0:$paramsTable["SOD_Count"][$k],
                "SOD_Length" => ($paramsTable["SOD_Length"][$k]==false?0:$paramsTable["SOD_Length"][$k])*1000,
                "SOD_Width" => ($paramsTable["SOD_Width"][$k]==false?0:$paramsTable["SOD_Width"][$k])*1000,
                "SOD_Weight" => $paramsTable["SOD_Weight"][$k]==false?0:$paramsTable["SOD_Weight"][$k],
                "SOD_Memo" => $paramsTable["SOD_Memo"][$k],
                "SOD_Sended" => $paramsTable["WRD_ID"][$k]==false?0:1,
                "SID_ID" => $paramsTable["SID_ID"][$k],
                "LuPiHao" => $paramsTable["LuPiHao"][$k],
                "PiHao" => $paramsTable["PiHao"][$k],
                "SO_ProjectName" => $params["SO_ProjectName"],
                "SO_TowerType" => $params["SO_TowerType"],
                "SO_EType" => $params["SO_EType"],
                "PC_Num" => $params["PC_Num"],
                "T_Num" => $params["T_Num"],
                "PT_Num" => $params["PT_Num"],
                "purpose" => $paramsTable["purpose"][$k],
                "WRD_ID" => $paramsTable["WRD_ID"][$k],
                "SOD_BWeight" => $paramsTable["SOD_BWeight"][$k],
                "SO_Num" => $params["SO_Num"]
            ];

            $store_in_list[$in_detail_one["SID_ID"]]["SID_RestCount"] += ($paramsTable["ago_SOD_Count"][$k]-($paramsTable["SOD_Count"][$k]==false?0:$paramsTable["SOD_Count"][$k]));
            $store_in_list[$in_detail_one["SID_ID"]]["SID_RestWeight"] += ($paramsTable["ago_SOD_Weight"][$k]-($paramsTable["SOD_Weight"][$k]==false?0:$paramsTable["SOD_Weight"][$k]));
            if($store_in_list[$in_detail_one["SID_ID"]]["SID_RestCount"]<0) $msg .= "第".($k+1)."行出库数量大于剩余数量;<br>";
            if($store_in_list[$in_detail_one["SID_ID"]]["SID_RestWeight"]<0) $msg .= "第".($k+1)."行出库重量大于剩余重量;<br>";


            if(!$paramsTable["SOD_ID"][$k]) unset($sectSaveList[$k]["SOD_ID"]);

            
            
        }
        return [$sectSaveList,$store_in_list,$msg];
    }

    public function abandonment()
    {
        $SSC_Num = $this->request->post("num");
        if(!$SSC_Num) return json(["code"=>0,"msg"=>"弃审失败！"]);
        $check_one = $this->model->where("SSC_Num",$SSC_Num)->find();
        
        if(!$check_one) return json(["code"=>0,"msg"=>"弃审失败！"]);
        $store_model = new StoreIn();
        $state_model = new StoreState();
        $store_in_model = new StoreInDetail();
        //弃审入库
        if($check_one["SSC_Flag"]==0){
            $list = $store_model->alias("si")->join(["storeindetail"=>"sid"],"si.SI_OtherID = sid.SI_OtherID")->join(["inventorymaterial"=>"im"],"im.IM_Num = sid.IM_Num")->where("si.SI_OtherID",$check_one["SI_OtherID"])->select();
            list($state,$msg) = $this->getStateUpList($list);
            if($msg) return json(["code"=>0,"msg"=>"库存有误，无法弃审"]);
            $result = false;
            Db::startTrans();
            try {
                $this->model->where("SSC_Num",$check_one["SSC_Num"])->update(["SI_OtherID"=>"","Auditor"=>"","AudiDate"=>"0000-00-00 00:00:00"]);
                $store_model->where("SI_OtherID",$check_one["SI_OtherID"])->delete();
                $store_in_model->where("SI_OtherID",$check_one["SI_OtherID"])->delete();
                $state_model->allowField(true)->saveAll($state);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }else{
            $list = $this->model->alias("so")->join(["storestatecheckdetail"=>"sod"],"so.SSC_Num = sod.SSC_Num")->join(["inventorymaterial"=>"im"],"im.IM_Num = sod.IM_Num")->where("so.SSC_Num",$SSC_Num)->select();
            $sectSaveList = $state = [];
            foreach($list as $k=>$v){
                $key = $v["IM_Num"].'-'.$v["L_Name"].'-'.round($v["SSCD_Length"],2).'-'.round($v["SSCD_Width"],2);
                if(!isset($sectSaveList[$key])){
                    $sectSaveList[$key] = [
                        "IM_Num" => $v["IM_Num"],
                        "IM_Spec" => $v["IM_Spec"],
                        "L_Name" => $v["L_Name"],
                        "SSCD_Length" => $v["SSCD_Length"],
                        "SSCD_Width" => $v["SSCD_Width"],
                        "SS_WareHouse" => $v["SSC_WareHouse"],
                        "SSCD_Count" => $v["SSCD_Count"],
                        "SSCD_Weight" => $v["SSCD_Weight"]
                    ];
                }else{
                    $sectSaveList[$key]["SSCD_Count"] += $v["SSCD_Count"];
                    $sectSaveList[$key]["SSCD_Weight"] += $v["SSCD_Weight"];
                }
            }
            foreach($sectSaveList as $k=>$v){
                $where = [
                    "IM_Num" => ["=",$v["IM_Num"]],
                    "L_Name" => ["=",$v["L_Name"]],
                    "SS_Length" => ["=",$v["SSCD_Length"]],
                    "SS_Width" => ["=",$v["SSCD_Width"]],
                    "SS_WareHouse" => ["=",$v["SS_WareHouse"]]
                ];
                $state_one = $state_model->where($where)->find();
                $edit_one = [];
                if($state_one){
                    $edit_one = $state_one->toArray();
                    $edit_one["SS_Count"] = $edit_one["SS_Count"]+$v["SSCD_Count"];
                    $edit_one["SS_Weight"] = $edit_one["SS_Weight"]+$v["SSCD_Weight"];
                }else{
                    $edit_one = [
                        "IM_Num" => $v["IM_Num"],
                        "IM_Spec" => $v["IM_Spec"],
                        "L_Name" => $v["L_Name"],
                        "SS_Length" => $v["SSCD_Length"],
                        "SS_Width" => $v["SSCD_Width"],
                        "SS_Count" => $v["SSCD_Count"],
                        "SS_Weight" => $v["SSCD_Weight"],
                        "SS_WareHouse" => $v["SS_WareHouse"]
                    ];
                }
                $state[] = $edit_one;
            }
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->where("SSC_Num",$check_one["SSC_Num"])->update(["Auditor"=>"","AudiDate"=>"0000:00:00 00:00:00"]);
                $state_model->allowField(true)->saveAll($state);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"成功"]);
        } else {
            return json(["code"=>1,"msg"=>"成功，但无数据更新"]);
        }
    }

    public function tableHead()
    {
        $num = $this->request->post("num");
        $tableField = $this->getDetailField($num);
        $tableFieldContent = '<tr><th width="40">删</th>';
        foreach($tableField as $k=>$v){
            $tableFieldContent .= "<th ".$v[3]." width=".$v[4].">".$v[0]."</th>";
        }
        $tableFieldContent .= "</tr>";
        $data = [
            "tableField" => $tableField,
            "tableFieldContent" => $tableFieldContent
        ];
        return json(["code"=>1,"data"=>$data]);
    }

    public function chooseMaterialWare($warehouse = "")
    {
        if($warehouse == "") $this->error("请先选择仓库");
        if ($this->request->isAjax()) {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new StoreIn())->alias("si")
                ->join(["storeindetail"=>"sid"],"si.SI_OtherID = sid.SI_OtherID","left")
                ->join(["inventorymaterial"=>"im"],"sid.IM_Num = im.IM_Num","left")
                ->field("sid.SID_ID,im.IM_Class,im.IM_Spec,sid.L_Name,sid.SID_Length as SSCD_Length,sid.SID_Width as SSCD_Width,sid.SID_RestCount,sid.SID_RestWeight,sid.LuPiHao,sid.PiHao,sid.sid_testnum as testnum,im.IM_PerWeight,sid.IM_Num,sid.SID_Count as SSCD_Count,sid.SID_Weight as SSCD_Weight,si.SI_WareHouse,si.SI_AudioPepo,sid.SID_Valid,sid.SID_Length,sid.SID_Width,si.SI_ArriveDate,si.SI_WareHouse,si.SI_InDate")
                ->where("si.SI_WareHouse",$warehouse)
                ->where($where)
                ->where("si.SI_AudioPepo","<>","")
                ->having("(SID_RestWeight>0 or SID_RestCount>0 )")
                ->order("SI_InDate desc,SI_WareHouse,IM_Class,IM_Num,L_Name,IM_Spec,SID_Length")
                ->select();
            $result = array("total" => count($list), "rows" => $list);
            return json($result);
        }
        $defaultTime = date("Y-m-d 00:00:00",strtotime("-6 day")).' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        $this->assignconfig("warehouse",$warehouse);
        return $this->view->fetch();
    }

    // public function chooseMaterialKc($warehouse = "")
    // {
    //     if($warehouse == "") $this->error("请先选择仓库");
    //     //设置过滤方法
    //     $this->request->filter(['strip_tags', 'trim']);
    //     if ($this->request->isAjax()) {
    //         //如果发送的来源是Selectpage，则转发到Selectpage
    //         if ($this->request->request('keyField')) {
    //             return $this->selectpage();
    //         }

    //         list($where, $sort, $order, $offset, $limit) = $this->buildparams();
    //         $list = (new InventoryMaterial())->alias("im")
    //             ->join(["storestate"=>"ss"],"im.IM_Num = ss.IM_Num")
    //             ->field("im.IM_Class, ss.SS_ID, ss.IM_Num, im.IM_Name,im.IM_Measurement, ss.L_Name, ss.SS_Length as SSCD_Length, ss.SS_Width as SSCD_Width, ss.SS_Count, ss.SS_Weight,ss.SS_Memo, IFNULL(ss.SS_MinStore,0) as SS_MinStore, ss.SS_MaxStore,ss.SS_WareHouse, IFNULL(ss.SF_Count,0) AS SF_Count, IFNULL(ss.SF_Weight,0) AS SF_Weight, ss.ASF_Count, ss.ASF_Weight, im.IM_Spec, im.IM_Max, im.IM_Min,0 as avweight,IFNULL(im.ifSuanByCount,0) AS ifSuanByCount,im.IM_PerWeight")
    //             ->where("ss.SS_WareHouse",$warehouse)
    //             ->where($where)
    //             ->order("IM_Class,IM_Class,IM_Num,L_Name,IM_Spec,SSCD_Length")
    //             ->select();
    //         $result = array("total" => count($list), "rows" => $list);
    //         return json($result);
    //     }
    //     $this->assignconfig("warehouse",$warehouse);
    //     return $this->view->fetch();
    // }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        $one = $this->model->alias("m")
            ->join(["storestatecheckdetail"=>"sscd"],"m.SSC_Num = sscd.SSC_Num")
            ->where("sscd.SSCD_ID",$num)
            ->find();
        if(!$one or $one["Auditor"]) return json(["code"=>0,'msg'=>"已审核或者该单子不存在，删除失败"]);
        if($num){
            Db::startTrans();
            try {
                $this->detailModel->where("SSCD_ID",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function getDetailField($type=0){
        $tableField = [];
        if($type == 0){
            $tableField = [
                ["ID","SSCD_ID","readonly","hidden","50"],
                ["比重","IM_PerWeight","readonly","hidden","40"],
                ["材料名称","IM_Class","readonly","","60"],
                ["存货编码","IM_Num","readonly","","70"],
                // ["税率","IM_Rax","readonly","","70"],
                ["材质","L_Name","readonly","","70"],
                ["规格","IM_Spec","readonly","","75"],
                ["长度(mm)","SSCD_Length","","","40"],
                ["宽度(mm)","SSCD_Width","","","40"],
                ["库存数量","SS_Count","readonly","","40"],
                ["库存重量(kg)","SS_Weight","readonly","","65"],
                ["数量","SSCD_Count","","","40"],
                ["重量(kg)","SSCD_Weight","data-rule='required'","","65"],
                ["生产厂家","SID_TestResult","","","40"],
                ["炉号","LuPiHao","","","40"],
                ["批号","PiHao","","","65"],
                ["试验编号","testnum","","","65"],
                // ["含税单价","SSCD_Price","","","60"],
                // ["含税金额","SSCD_Money","","","85"],
                // ["不含税单价","SSCD_NaxPrice","","","85"],
                // ["不含税金额","SSCD_NaxMoney","","","85"],
                ["备注","SSCD_Memo","","","80"],
                ["到货数量","MGN_Count","","","80"],
                ["到货日期","MN_Date","","","65"]
            ];
        }else if($type == 1){
            $tableField = [
                ["ID","SSCD_ID","readonly","hidden","50"],
                ["SID","SID_ID","readonly","hidden","50"],
                ["比重","IM_PerWeight","readonly","hidden","40"],
                ["材料名称","IM_Class","readonly","","60"],
                ["存货编码","IM_Num","readonly","","70"],
                // ["税率","IM_Rax","readonly","","70"],
                ["材质","L_Name","readonly","","70"],
                ["规格","IM_Spec","readonly","","75"],
                ["长度(mm)","SSCD_Length","","","40"],
                ["宽度(mm)","SSCD_Width","","","40"],
                ["剩余数量","SID_RestCount","readonly","","40"],
                ["剩余重量(kg)","SID_RestWeight","readonly","","65"],
                ["库存数量","SS_Count","readonly","","40"],
                ["库存重量(kg)","SS_Weight","readonly","","65"],
                ["数量","ago_SSCD_Count","","hidden","40"],
                ["数量","SSCD_Count","data-rule='required'","","40"],
                ["重量(kg)","ago_SSCD_Weight","","hidden","65"],
                ["重量(kg)","SSCD_Weight","data-rule='required'","","65"],
                ["炉号","LuPiHao","","","40"],
                ["批号","PiHao","","","65"],
                ["试验编号","testnum","","","65"],
                // ["含税单价","SSCD_Price","","","60"],
                // ["含税金额","SSCD_Money","","","85"],
                // ["不含税单价","SSCD_NaxPrice","","","85"],
                // ["不含税金额","SSCD_NaxMoney","","","85"],
                ["备注","SSCD_Memo","","","80"],
                ["到货数量","MGN_Count","","","80"],
                ["到货日期","MN_Date","","","65"]
            ];
        }else if($type == 3){
            $tableField = [
                ["ID","SSCD_ID","readonly","","50"],
                ["比重","IM_PerWeight","readonly","hidden","40"],
                ["材料名称","IM_Class","readonly","","60"],
                ["存货编码","IM_Num","readonly","","70"],
                // ["税率","IM_Rax","readonly","","70"],
                ["材质","L_Name","readonly","","70"],
                ["规格","IM_Spec","readonly","","75"],
                ["长度(mm)","SSCD_Length","","","40"],
                ["宽度(mm)","SSCD_Width","","","40"],
                ["库存数量","SS_Count","","","40"],
                ["库存重量(kg)","SS_Weight","","","65"],
                ["当日库存数量","CurrentSSCount","readonly","","40"],
                ["当日库存重量(kg)","CurrentSSWeight","readonly","","65"],
                ["盘点总数","CurrentPDCount","readonly","","40"],
                ["盘点总重量(kg)","CurrentPDWeight","readonly","","65"],
                ["数量","SSCD_Count","","","40"],
                ["重量(kg)","SSCD_Weight","","","65"],
                ["炉号","LuPiHao","","","40"],
                ["批号","PiHao","","","65"],
                ["试验编号","testnum","","","65"],
                // ["含税单价","SSCD_Price","","","60"],
                // ["含税金额","SSCD_Money","","","85"],
                // ["不含税单价","SSCD_NaxPrice","","","85"],
                // ["不含税金额","SSCD_NaxMoney","","","85"],
                ["备注","SSCD_Memo","","","80"],
                ["到货数量","MGN_Count","","","80"],
                ["到货日期","MN_Date","","","65"]
            ];
        }
        return $tableField;
    }

    protected function getState($params,$paramsTable)
    {
        $store_in_list = $sectSaveList = [];
        $msg = "";
        if($params["SSC_Flag"]==0){
            foreach($paramsTable["SSCD_ID"] as $k=>$v){
                if($paramsTable["IM_Class"][$k]=="角钢" and round($paramsTable["SSCD_Length"][$k],2)==false) $msg .= "第".($k+1)."行角钢长度不能为0;<br>";
                $sectSaveList[$k] = [
                    "SSCD_ID" => $v,
                    "SSC_Num" => $params["SSC_Num"],
                    "IM_Num" => $paramsTable["IM_Num"][$k],
                    "L_Name" => $paramsTable["L_Name"][$k],
                    "SSCD_Length" => $paramsTable["SSCD_Length"][$k]==false?0:$paramsTable["SSCD_Length"][$k],
                    "SSCD_Width" => $paramsTable["SSCD_Width"][$k]==false?0:$paramsTable["SSCD_Width"][$k],
                    "SSCD_Count" => $paramsTable["SSCD_Count"][$k]==false?0:$paramsTable["SSCD_Count"][$k],
                    "SSCD_Weight" => $paramsTable["SSCD_Weight"][$k]==false?0:$paramsTable["SSCD_Weight"][$k],
                    "SID_TestResult" => $paramsTable["SID_TestResult"][$k],
                    "LuPiHao" => $paramsTable["LuPiHao"][$k],
                    "PiHao" => $paramsTable["PiHao"][$k],
                    "SSCD_Memo" => $paramsTable["SSCD_Memo"][$k],
                    "testnum" => $paramsTable["testnum"][$k]
                ];
                if($v==false) unset($sectSaveList[$k]["SSCD_ID"]);
            }
        }elseif($params["SSC_Flag"]==1){
            $store_in_model = new StoreInDetail();
            foreach($paramsTable["SSCD_ID"] as $k=>$v){
                if($paramsTable["IM_Class"][$k]=="角钢" and round($paramsTable["SSCD_Length"][$k],2)==false) $msg .= "第".($k+1)."行角钢长度不能为0;<br>";
                if($paramsTable["SSCD_Count"][$k]>$paramsTable["SID_RestCount"][$k]) $msg .= "第".($k+1)."行出库数量大于剩余数量;<br>";
                if($paramsTable["SSCD_Weight"][$k]>$paramsTable["SID_RestWeight"][$k]) $msg .= "第".($k+1)."行出库重量大于剩余重量;<br>";
                $sectSaveList[$k] = [
                    "SSCD_ID" => $v,
                    "SSC_Num" => $params["SSC_Num"],
                    "SID_ID" => $paramsTable["SID_ID"][$k],
                    "IM_Num" => $paramsTable["IM_Num"][$k],
                    "L_Name" => $paramsTable["L_Name"][$k],
                    "SSCD_Length" => $paramsTable["SSCD_Length"][$k]==false?0:$paramsTable["SSCD_Length"][$k],
                    "SSCD_Width" => $paramsTable["SSCD_Width"][$k]==false?0:$paramsTable["SSCD_Width"][$k],
                    "SSCD_Count" => $paramsTable["SSCD_Count"][$k]==false?0:$paramsTable["SSCD_Count"][$k],
                    "SSCD_Weight" => $paramsTable["SSCD_Weight"][$k]==false?0:$paramsTable["SSCD_Weight"][$k],
                    "SSCD_Memo" => $paramsTable["SSCD_Memo"][$k],
                    "LuPiHao" => $paramsTable["LuPiHao"][$k],
                    "PiHao" => $paramsTable["PiHao"][$k],
                    "testnum" => $paramsTable["testnum"][$k]
                ];
                if($v==false) unset($sectSaveList[$k]["SSCD_ID"]);
                if(isset($store_in_list[$paramsTable["SID_ID"][$k]])){
                    $store_in_list[$paramsTable["SID_ID"][$k]]["use_count"] += ($paramsTable["SSCD_Count"][$k]-(int)$paramsTable["ago_SSCD_Count"][$k]);
                    $store_in_list[$paramsTable["SID_ID"][$k]]["use_weight"] += ($paramsTable["SSCD_Weight"][$k]-(int)$paramsTable["ago_SSCD_Weight"][$k]);
                }else{
                    $in_detail_one = $store_in_model->where("SID_ID",$paramsTable["SID_ID"][$k])->find();
                    if($in_detail_one){
                        isset($store_in_list[$in_detail_one["SID_ID"]])?"":$store_in_list[$in_detail_one["SID_ID"]]=["SID_ID"=>$in_detail_one["SID_ID"],"count"=>$in_detail_one["SID_RestCount"],"weight"=>$in_detail_one["SID_RestWeight"],"use_count"=>($paramsTable["SSCD_Count"][$k]-(int)$paramsTable["ago_SSCD_Count"][$k]),"use_weight"=>($paramsTable["SSCD_Weight"][$k]-(int)$paramsTable["ago_SSCD_Weight"][$k])];
                    }else{
                        $msg .= "第".($k+1)."入库单出错，请删除;<br>";
                        return [[],$msg];
                    }
                }
                if($store_in_list[$paramsTable["SID_ID"][$k]]["use_count"]>$store_in_list[$paramsTable["SID_ID"][$k]]["count"]) $msg .= "第".($k+1)."行出库数量大于剩余数量;<br>";
                if($store_in_list[$paramsTable["SID_ID"][$k]]["use_weight"]>$store_in_list[$paramsTable["SID_ID"][$k]]["weight"]) $msg .= "第".($k+1)."行出库重量大于剩余重量;<br>";
            }
        }
        if(!$msg){
            foreach($store_in_list as $k=>$v){
                $store_in_list[$k]["SID_RestCount"] = $v["count"]-$v["use_count"];
                $store_in_list[$k]["SID_RestWeight"] = $v["weight"]-$v["use_weight"];
                unset($store_in_list[$k]["count"],$store_in_list[$k]["weight"],$store_in_list[$k]["use_count"],$store_in_list[$k]["use_weight"]);
            }
        }
        return [$sectSaveList,$msg,$store_in_list];
    }

}
