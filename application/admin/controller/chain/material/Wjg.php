<?php

namespace app\admin\controller\chain\material;

use app\admin\model\chain\lofting\ProduceTask;
use app\admin\validate\chain\material\Wjg as MaterialWjg;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;

/**
 * 原材料入库
 *
 * @icon fa fa-circle-o
 */
class Wjg extends Backend
{
    
    /**
     * StoreInDetail模型对象
     * @var \app\admin\model\chain\material\Wjg
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 导入
     */
    public function import()
    {
        // if ($this->request->isPost()) {
            $file = $this->request->request('file');
            if (!$file) {
                $this->error(__('Parameter %s can not be empty', 'file'));
            }
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $filePath = ROOT_PATH . DS . 'public' . DS . $file;
            if (!is_file($filePath)) {
                $this->error(__('No results were found'));
            }
            try {
                $importData = Excel::import($filePath, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($importData)){
                    $this->error('空数据文件');
                }
                $importData = array_slice($importData,1);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $ptList = $saveVList = $data = [];
            $imList = $this->inventoryNumList();
            $vList = $this->vendorNumList();
            $vendorModel = (new \app\admin\model\jichu\wl\Vendor());
            $vNum = $vendorModel->where("VC_Num","01")->order("V_Num desc")->value("right(V_Num,3)");
            $vNum = $vNum?($vNum+1):1;
            $validate = new MaterialWjg();
            $thisField = ["PT_Num/s","material/s","IM_Spec/s","L_Name/s","length/f","width/f","quantity/f","weight/f","cc_date/t","fh_date/t","dh_date/t","lh_date/t","rk_date/t","ck_date/t","v_num/s","CTD_QualityNo/s",'MT_Standard/s',"LuPiHao/s","MTD_C/f","MTD_Si/f","MTD_Mn/f","MTD_P/f","MTD_S/f","MTD_V/f","MTD_Nb/f","MTD_Ti/f","MTD_Cr/f","MTD_Rm/f","MTD_Rel/f","MTD_Percent/f","MTD_Temperature/f","MTD_Cjf/f","MTD_Cjs/f","MTD_Cjt/f","MTD_Mader/s","mn_deliverdate/t","MT_AuditDate/t","MT_Auditor/s"];
            foreach($importData as $k=>$v){
                if(!$v[0]) continue;
                $thisData = [];
                foreach($thisField as $kk=>$vv){
                    $field = explode("/",$vv);
                    if($field[1]=="s") $v[$kk] = $v[$kk]?$v[$kk]:"";
                    else if($field[1]=="t") $v[$kk] = $v[$kk]?date("Y-m-d H:i:s",strtotime($v[$kk])):"";
                    else $v[$kk] = $v[$kk]?round($v[$kk],3):0;
                    $thisData[$field[0]] = trim($v[$kk]);
                }

                foreach([["L","∠"],["∟","∠"],["X","*"],["x","*"]] as $kk=>$vv){
                    $thisData["IM_Spec"] = str_replace($vv[0],$vv[1],$thisData["IM_Spec"]);
                }
                if(!isset($imList[$thisData["material"]."-".$thisData["IM_Spec"]])) continue;
                else $thisData["IM_Num"] = $imList[$thisData["material"]."-".$thisData["IM_Spec"]];
                if(!$validate->check($thisData)) $this->error("第".($k+1)."行".$validate->getError());
                if(array_search($thisData["v_num"],$vList)) $thisData["v_number"] = array_search($thisData["v_num"],$vList);
                else{
                    $saveVList[] = [
                        "V_Num" => "01".str_pad($vNum,3,0,STR_PAD_LEFT),
                        "V_Name" => $thisData["v_num"],
                        "VC_Num" => "01",
                        "V_C_Dept" => "公司",
                        "V_Writer" => "吴昌驰",
                        "V_EditDate" => date("Y-m-d H:i:s")
                    ];
                    $thisData["v_number"] = "01".str_pad($vNum,3,0,STR_PAD_LEFT);
                    $vNum++;
                }
                $data[$k] = $thisData;
                $data[$k]["result"] = "合格";
                $data[$k]["MTD_Czsl"] = "合格";
                $data[$k]["MTD_Wq"] = "合格";
                $data[$k]["MTD_Wg"] = "合格";
                $ptList[$thisData["PT_Num"]] = $thisData["PT_Num"];
            }
            if(empty($data)) $this->error("没有材料库，导入失败");
            $productViewList = (new ProduceTask())->alias("pt")
                ->join(["taskdetail"=>"td"],"pt.TD_ID=td.TD_ID")
                ->join(["task"=>"t"],"td.T_Num=t.T_Num")
                ->join(["compact"=>"c"],"c.C_Num=t.C_Num")
                ->field("t.t_project,td.TD_TypeName,td.TD_Pressure,c.PC_Num,t.T_Num,c.C_Num,c.Customer_Name,pt.PT_Num")
                ->where('pt.PT_Num',"IN",$ptList)
                ->select();
            $productViewData = [];
            foreach($productViewList as $k=>$v){
                $productViewData[$v["PT_Num"]] = $v->toArray();
            }
            $diff = array_diff($ptList,array_keys($productViewData));
            if(!empty($diff)) $this->error("下达单号为".implode(",",$diff)."的不存在！请先添加下达单号！");

            $materialNoteModel = new \app\admin\model\chain\material\MaterialNote();
            $materialGetNoticeModel = new \app\admin\model\chain\material\MaterialGetNotice();
            $commisionTestModel = new \app\admin\model\chain\material\CommisionTest();
            $commisionTestDetailModel = new \app\admin\model\chain\material\CommisionTestDetail();
            $materialTestModel = new \app\admin\model\quality\experiment\MaterialTest();
            $materialTestDetailModel = new \app\admin\model\quality\experiment\MaterialTestDetail();
            $storeInModel = new \app\admin\model\chain\material\StoreIn();
            $storeInDetailModel = new \app\admin\model\chain\material\StoreInDetail();
            $storeOutModel = new \app\admin\model\chain\material\StoreOut();
            $storeOutDetailModel = new \app\admin\model\chain\material\StoreOutDetail();
            $mnNum = $materialNoteModel->where("MN_Num","LIKE","WDH".date("Ymd")."-%")->order("MN_Num desc")->value("right(MN_Num,5)");
            $mnNum = $mnNum?($mnNum+1):1;
            $ctNum = $commisionTestModel->where("CT_Num","LIKE","WSJ".date("Ymd")."-%")->order("CT_Num desc")->value("right(CT_Num,5)");
            $ctNum = $ctNum?($ctNum+1):1;
            $mtNum = $materialTestModel->where("MT_Num","LIKE","WGC".date("Ymd")."-%")->order("MT_Num desc")->value("right(MT_Num,5)");
            $mtNum = $mtNum?($mtNum+1):1;
            $siNum = $storeInModel->where("SI_OtherID","LIKE","WRK".date("Ymd")."-%")->order("SI_OtherID desc")->value("right(SI_OtherID,5)");
            $siNum = $siNum?($siNum+1):1;
            $soNum = $storeOutModel->where("SO_Num","LIKE","WCK".date("Ymd")."-%")->order("SO_Num desc")->value("right(SO_Num,5)");
            $soNum = $soNum?($soNum+1):1;


            $storeOutDetailResult = false;
            Db::startTrans();
            try {
                $materialNoteList = $materialGetNoticeList = $commisionTestList = $commisionTestDetailList = [];
                $materialTestList = $materialTestDetailList = $storeInList = $storeInDetailList = $storeOutList = $storeOutDetailList = [];
                foreach($data as $k=>$v){
                    $materialNoteList[$k] = [
                        "MN_Num" => "WDH".date("Ymd")."-".str_pad($mnNum,5,0,STR_PAD_LEFT),
                        "V_Num" => $v["v_number"],
                        "Writer" => "吴昌驰",
                        "WriteTime" => $v["dh_date"],
                        "Auditor" => "吴昌驰",
                        "AuditTime" => $v["dh_date"],
                        "ST_Num" => "01",
                        "MN_Date" => $v["fh_date"],
                        "mn_deliverdate" => $v["dh_date"],
                        "MAT_STANDARD" => 1
                    ];
                    $materialGetNoticeList[$k] = [
                        "MN_Num" => "WDH".date("Ymd")."-".str_pad($mnNum,5,0,STR_PAD_LEFT),
                        "IM_Num" => $v["IM_Num"],
                        "L_Name" => $v["L_Name"],
                        "MGN_Length" => $v["length"],
                        "MGN_Width" => $v["width"],
                        "MGN_Weight" => $v["weight"],
                        "MGN_Maker" => $v["MTD_Mader"],
                        "MGN_Count" => $v["quantity"],
                        "MGN_Destination" => "原材料仓库",
                        "mgn_formuleweight" => round($v["weight"]/1000,6),
                        "mgn_yf_rate" => 7,
                        "iflh" => 1
                    ];
                    $mnNum++;
                }
                $materialNoteResult = $materialNoteModel->allowField(true)->saveAll($materialNoteList);
                $materialGetNoticeResult = $materialGetNoticeModel->allowField(true)->saveAll($materialGetNoticeList);

                foreach($data as $k=>$v){
                    $commisionTestList[$k] = [
                        "CT_Num" => "WSJ".date("Ymd")."-".str_pad($ctNum,5,0,STR_PAD_LEFT),
                        "CT_Date" => $v["lh_date"],
                        "CT_TakeDate" => $v["rk_date"],
                        "CT_Standard" => $v["MT_Standard"],
                        "V_Num" => $v["v_number"],
                        "Writer" => "吴昌驰",
                        "WriteTime" => $v["rk_date"],
                        "Auditor" => "吴昌驰",
                        "AuditTime" => $v["rk_date"],
                        "CT_Type" => $v["material"]
                    ];
                    $commisionTestDetailList[$k] = [
                        "CT_Num" => "WSJ".date("Ymd")."-".str_pad($ctNum,5,0,STR_PAD_LEFT),
                        "CTD_Spec" => $v["IM_Spec"],
                        "L_Name" => $v["L_Name"],
                        "CTD_Count" => 1,
                        "LuPiHao" => $v["LuPiHao"],
                        "CTD_Weight" => $v["weight"],
                        "MGN_ID" => $materialGetNoticeResult[$k]["MGN_ID"],
                        "CTD_FactCount" => $v["quantity"],
                        "CTD_ShapeSizeRes" => "合格",
                        "CTD_ShowQualityRes" => "合格",
                        "CTD_QualityNo" => $v["CTD_QualityNo"],
                        "CTD_QYDate" => $v["lh_date"],
                        "CTD_GuoBanWeight" => $v["weight"],
                        "ifNoMaterialTest" => 1
                    ];
                    $ctNum++;
                }
                $commisionTestResult = $commisionTestModel->allowField(true)->saveAll($commisionTestList,false);
                $commisionTestDetailResult = $commisionTestDetailModel->allowField(true)->saveAll($commisionTestDetailList);

                foreach($data as $k=>$v){
                    $materialTestList[$k] = [
                        "MT_Num" => "WGC".date("Ymd")."-".str_pad($mtNum,5,0,STR_PAD_LEFT),
                        "DD_Name" => "物流部",
                        "MT_Mader" => $v["MTD_Mader"],
                        "MT_Standard" => $v["MT_Standard"],
                        "MT_Writer" => $v["MT_Auditor"],
                        "MT_WriteDate" => $v["MT_AuditDate"],
                        "MT_Auditor" => $v["MT_Auditor"],
                        "MT_AuditDate" => $v["MT_AuditDate"],
                        "MT_Test" => $v["MT_Auditor"],
                        "MT_TestDate" => $v["MT_AuditDate"]
                    ];
                    $materialTestDetailList[$k] = [
                        "CT_Num" => $commisionTestResult[$k]["CT_Num"],
                        "MT_Num" => "WGC".date("Ymd")."-".str_pad($mtNum,5,0,STR_PAD_LEFT),
                        "MGN_Length" => $v["length"],
                        "IM_Num" => $v["IM_Num"],
                        "L_Name" => $v["L_Name"],
                        "MTD_TestNo" => "FJ-".$v["LuPiHao"],
                        "MTD_Mader" => $v["MTD_Mader"],
                        "LuPiHao" => $v["LuPiHao"],
                        "MTD_C" => $v["MTD_C"],
                        "MTD_Si" => $v["MTD_Si"],
                        "MTD_Mn" => $v["MTD_Mn"],
                        "MTD_P" => $v["MTD_P"],
                        "MTD_S" => $v["MTD_S"],
                        "MTD_V" => $v["MTD_V"],
                        "MTD_Nb" => $v["MTD_Nb"],
                        "MTD_Ti" => $v["MTD_Ti"],
                        "MTD_Cr" => $v["MTD_Cr"],
                        "MTD_Czsl" => $v["MTD_Czsl"],
                        "MTD_Rm" => $v["MTD_Rm"],
                        "MTD_Rel" => $v["MTD_Rel"],
                        "MTD_Percent" => $v["MTD_Percent"],
                        "MTD_Temperature" => $v["MTD_Temperature"],
                        "MTD_Cjf" => $v["MTD_Cjf"],
                        "MTD_Cjs" => $v["MTD_Cjs"],
                        "MTD_Cjt" => $v["MTD_Cjt"],
                        "MTD_ChDate" => $v["mn_deliverdate"],
                        "MTD_Wq" => $v["MTD_Wq"],
                        "MTD_Wg" => $v["MTD_Wg"],
                        "MTD_Conclusion" => $v["result"],
                        "MN_Date" => $v["dh_date"],
                        "CT_Date" => $v["lh_date"],
                        "CTD_Weight" => $v["weight"]
                    ];
                    $storeInList[$k] = [
                        "SI_OtherID" => "WRK".date("Ymd")."-".str_pad($siNum,5,0,STR_PAD_LEFT),
                        "PC_Num" => $productViewData[$v["PT_Num"]]["PC_Num"],
                        "SI_ArriveDate" => $v["rk_date"],
                        "SI_WriteDate" => $v["rk_date"],
                        "SI_Writer" => "吴昌驰",
                        "SI_AudioPepo" => "吴昌驰",
                        "SI_AudioDate" => $v["rk_date"],
                        "SI_WareHouse" => "铁塔国网外加工库",
                        "SI_InDate" => $v["rk_date"]
                    ];
                    $storeInDetailList[$k] = [
                        "SI_OtherID" => "WRK".date("Ymd")."-".str_pad($siNum,5,0,STR_PAD_LEFT),
                        "MGN_ID" => $commisionTestDetailResult[$k]["CTD_ID"],
                        "CT_Num" => $commisionTestResult[$k]["CT_Num"],
                        "IM_Num" => $v["IM_Num"],
                        "L_Name" => $v["L_Name"],
                        "SID_Length" => $v["length"],
                        "SID_Width" => $v["width"],
                        "SID_Count" => $v["quantity"],
                        "SID_Weight" => $v["weight"],
                        "SID_FactWeight" => $v["weight"],
                        "SID_RestCount" => 0,
                        "SID_RestWeight" => 0,
                        "SID_TestResult" => $v["MTD_Mader"],
                        "LuPiHao" => $v["LuPiHao"],
                        "V_Num" => $v["v_number"],
                        "MGN_ID2" => $materialGetNoticeResult[$k]["MGN_ID"],
                    ];
                    $mtNum++;
                    $siNum++;
                }
                $materialTestResult = $materialTestModel->allowField(true)->saveAll($materialTestList);
                $materialTestDetailResult = $materialTestDetailModel->allowField(true)->saveAll($materialTestDetailList);
                $storeInResult = $storeInModel->allowField(true)->saveAll($storeInList);
                $storeInDetailResult = $storeInDetailModel->allowField(true)->saveAll($storeInDetailList);

                foreach($data as $k=>$v){
                    $storeOutList[$k] = [
                        "SO_Num" => "WCK".date("Ymd")."-".str_pad($soNum,5,0,STR_PAD_LEFT),
                        "SO_ProjectName" => $productViewData[$v["PT_Num"]]["t_project"],
                        "SO_TowerType" => $productViewData[$v["PT_Num"]]["TD_TypeName"],
                        "SO_EType" => $productViewData[$v["PT_Num"]]["TD_Pressure"],
                        "SO_TakeDate" => $v["ck_date"],
                        "SO_Writer" => "吴昌驰",
                        "SO_WriteDate" => $v["ck_date"],
                        "SO_AudiDate" => $v["ck_date"],
                        "SO_Auditor" => "吴昌驰",
                        "SO_WareHouse" => "铁塔国网外加工库",
                        "SO_Take" => "铁塔车间",
                        "PC_Num" => $productViewData[$v["PT_Num"]]["PC_Num"],
                        "T_Num" => $productViewData[$v["PT_Num"]]["T_Num"],
                        "C_Num" => $productViewData[$v["PT_Num"]]["C_Num"],
                        "SO_C_Name" => $productViewData[$v["PT_Num"]]["Customer_Name"]
                    ];
                    $storeOutDetailList[$k] = [
                        "SO_Num" => "WCK".date("Ymd")."-".str_pad($soNum,5,0,STR_PAD_LEFT),
                        "IM_Num" => $v["IM_Num"],
                        "L_Name" => $v["L_Name"],
                        "SOD_Count" => $v["quantity"],
                        "SOD_Length" => $v["length"],
                        "SOD_Width" => $v["width"],
                        "SOD_Weight" => $v["weight"],
                        "SOD_Sended" => 0,
                        "SID_ID" => $storeInDetailResult[$k]["SID_ID"],
                        "LuPiHao" => $v["LuPiHao"],
                        "SO_ProjectName" => $productViewData[$v["PT_Num"]]["t_project"],
                        "SO_TowerType" => $productViewData[$v["PT_Num"]]["TD_TypeName"],
                        "SO_EType" => $productViewData[$v["PT_Num"]]["TD_Pressure"],
                        "PC_Num" => $productViewData[$v["PT_Num"]]["PC_Num"],
                        "T_Num" => $productViewData[$v["PT_Num"]]["T_Num"],
                        "PT_Num" => $productViewData[$v["PT_Num"]]["PT_Num"],
                        "SOD_BWeight" => $v["weight"],
                    ];
                    $soNum++;
                }
                $storeOutResult = $storeOutModel->allowField(true)->saveAll($storeOutList);
                $storeOutDetailResult = $storeOutDetailModel->allowField(true)->saveAll($storeOutDetailList);
                $vendorResult = $vendorModel->allowField(true)->saveAll($saveVList,false);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($storeOutDetailResult !== false) {
                $this->success("成功导入");
            } else {
                $this->error(__('No rows were updated'));
            }
        // }
        // return $this->view->fetch();
    }
}
