<?php

namespace app\admin\controller\chain\material;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 原材料出库清单
 *
 * @icon fa fa-circle-o
 */
class MonthlyStatistics extends Backend
{
    
    /**
     * MonthlyStatistics
     * @var \app\admin\model\chain\material\MonthlyStatistics
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->inModel = new \app\admin\model\chain\material\StoreIn;
        $this->inDetailModel = new \app\admin\model\chain\material\StoreInDetail;
        $this->outModel = new \app\admin\model\chain\material\StoreOut;
        $this->outDetailModel = new \app\admin\model\chain\material\StoreOutDetail;
        $this->stateModel = new \app\admin\model\chain\material\StoreState;
        $this->admin = \think\Session::get('admin');
        $this->assignconfig("time",date("Y-m-d H:i:s"));
        $this->assignconfig("limberList",$this->limberList());
        $this->assignconfig("inventoryList",$this->inventoryList());
        $this->assignconfig('wareclassList',$this->wareclassList());
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            //
            // [SS_WareHouse] => 仓库1国网
            // [IM_Class] => 角钢
            // [IM_Spec] => ∠25*3
            // [L_Name] => Q235B
            // [SS_Length] => 25000
            // [SS_Width] => 1000
            // [dj_time] => 2022-03-01 00:00:00 - 2022-03-31 23:59:59
            if(!isset($filter["SS_WareHouse"])) $this->error("请选择仓库！");
            if(!isset($filter["dj_time"])) $this->error("请选择时间段！");
            list($from,$end) = explode(" - ",$filter["dj_time"]);
            $where = ["1=1"];
            if(isset($filter["IM_Class"])) $where[] = "im.IM_Class = '".$filter["IM_Class"]."'";
            if(isset($filter["IM_Spec"])) $where[] = "im.IM_Spec = '".$filter["IM_Spec"]."'";
            if(isset($filter["L_Name"])) $where[] = "zl.L_Name = '".$filter["L_Name"]."'";
            if(isset($filter["SS_Length"])) $where[] = "length = '".$filter["SS_Length"]."'";
            if(isset($filter["SS_Width"])) $where[] = "width = '".$filter["SS_Width"]."'";
            $where_str = implode(" AND ",$where);
            

            // FULL JOIN
            $sql = "select length,width,zl.IM_Num,zl.L_Name,ifnull(qm_count,0) as qm_count,round(ifnull(qm_weight,0),2) as qm_weight,round(ck_count,2) as ck_count,round(ck_weight,2) as ck_weight,round(rk_count,2) as rk_count,round(rk_weight,2) as rk_weight,round(qc_count,2) as qc_count,round(qc_weight,2) as qc_weight,im.IM_Spec,im.IM_Class,'".$filter["SS_WareHouse"]."' as 'SS_WareHouse', case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh
            from (
                select qm.length,qm.width,qm.IM_Num,qm.L_Name,qm.qm_count,qm.qm_weight,(ifnull(ck.count,0)+ifnull(pk.count,0)) as ck_count,(ifnull(ck.weight,0)+ifnull(pk.weight,0)) as ck_weight,ifnull(rk.count,0) as rk_count,ifnull(rk.weight,0) as rk_weight,(ifnull(qm_count,0)+ifnull(pk.count,0)+ifnull(ck.count,0)-ifnull(rk.count,0)) as qc_count,(ifnull(qm_weight,0)+ifnull(pk.weight,0)+ifnull(ck.weight,0)-ifnull(rk.weight,0)) as qc_weight
                from (
                    select rk.length,rk.width,rk.IM_Num,rk.L_Name,(ifnull(rk.count,0)-ifnull(ck.count,0)-ifnull(pk.count,0)) as qm_count,(ifnull(rk.weight,0)-ifnull(ck.weight,0)-ifnull(pk.weight,0)) as qm_weight 
                    from (
                        select sid.SID_Length as length,sid.SID_Width as width,sid.IM_Num,sid.L_Name,sum(sid.SID_Count) as count,sum(sid.SID_Weight) as weight from storein si inner join storeindetail sid on si.SI_OtherID=sid.SI_OtherID 
                        WHERE si.SI_WareHouse='".$filter["SS_WareHouse"]."' and si.SI_InDate<='".$end."' and si.SI_AudioPepo<>'' 
                        GROUP BY sid.IM_Num,sid.L_Name,sid.SID_Length,sid.SID_Width 
                        HAVING count>0 or weight>0
                    ) rk 
                    LEFT JOIN (
                        select sod.SOD_Length as length,sod.SOD_Width as width,sod.IM_Num,sod.L_Name,sum(sod.SOD_Count) as count,sum(sod.SOD_Weight) as weight 
                        from storeout so inner join storeoutdetail sod on so.SO_Num=sod.SO_Num 
                        WHERE so.SO_WareHouse='".$filter["SS_WareHouse"]."' and so.SO_TakeDate<='".$end."' and so.SO_Auditor<>'' 
                        GROUP BY sod.IM_Num,sod.L_Name,sod.SOD_Length,sod.SOD_Width 
                        HAVING count>0 or weight>0
                    ) ck on rk.length=ck.length and rk.width=ck.width and rk.IM_Num=ck.IM_Num and rk.L_Name=ck.L_Name
                    LEFT JOIN (
                        select sscd.SSCD_Length as length,sscd.SSCD_Width as width,sscd.IM_Num,sscd.L_Name,sum(sscd.SSCD_Count) as count,sum(sscd.SSCD_Weight) as weight 
                    from storestatecheck ssc inner join storestatecheckdetail sscd on ssc.SSC_Num=sscd.SSC_Num 
                        WHERE ssc.SSC_WareHouse='".$filter["SS_WareHouse"]."' and ssc.SSC_Date<='".$end."' and ssc.SSC_Flag=1 and ssc.Auditor<>'' 
                        GROUP BY sscd.IM_Num,sscd.L_Name,sscd.SSCD_Length,sscd.SSCD_Width 
                        HAVING count>0 or weight>0
                    ) pk on rk.length=pk.length and rk.width=pk.width and rk.IM_Num=pk.IM_Num and rk.L_Name=pk.L_Name
                ) qm 
                left join (
                    select sid.SID_Length as length,sid.SID_Width as width,sid.IM_Num,sid.L_Name,sum(sid.SID_Count) as count,sum(sid.SID_Weight) as weight 
                    from storein si 
                    inner join storeindetail sid 
                    on si.SI_OtherID=sid.SI_OtherID 
                    WHERE si.SI_WareHouse='".$filter["SS_WareHouse"]."' and si.SI_InDate>='".$from."' and si.SI_InDate<='".$end."' and si.SI_AudioPepo<>'' 
                    GROUP BY sid.IM_Num,sid.L_Name,sid.SID_Length,sid.SID_Width 
                    HAVING count>0 or weight>0
                ) rk on rk.length=qm.length and rk.width=qm.width and rk.IM_Num=qm.IM_Num and rk.L_Name=qm.L_Name
                left join (
                    select sod.SOD_Length as length,sod.SOD_Width as width,sod.IM_Num,sod.L_Name,sum(sod.SOD_Count) as count,sum(sod.SOD_Weight) as weight 
                    from storeout so 
                    inner join storeoutdetail sod on so.SO_Num=sod.SO_Num 
                    WHERE so.SO_WareHouse='".$filter["SS_WareHouse"]."' and so.SO_TakeDate>='".$from."' and  so.SO_TakeDate<='".$end."' and so.SO_Auditor<>'' 
                    GROUP BY sod.IM_Num,sod.L_Name,sod.SOD_Length,sod.SOD_Width 
                    HAVING count>0 or weight>0
                ) ck on ck.length=qm.length and ck.width=qm.width and ck.IM_Num=qm.IM_Num and ck.L_Name=qm.L_Name
                left join (
                    select sscd.SSCD_Length as length,sscd.SSCD_Width as width,sscd.IM_Num,sscd.L_Name,sum(sscd.SSCD_Count) as count,sum(sscd.SSCD_Weight) as weight 
                    from storestatecheck ssc inner join storestatecheckdetail sscd on ssc.SSC_Num=sscd.SSC_Num 
                    WHERE ssc.SSC_WareHouse='".$filter["SS_WareHouse"]."' and ssc.SSC_Date>='".$from."' and ssc.SSC_Date<='".$end."' and ssc.SSC_Flag=1 and ssc.Auditor<>'' 
                    GROUP BY sscd.IM_Num,sscd.L_Name,sscd.SSCD_Length,sscd.SSCD_Width 
                    HAVING count>0 or weight>0
                ) pk on pk.length=qm.length and pk.width=qm.width and pk.IM_Num=qm.IM_Num and pk.L_Name=qm.L_Name
                having qm_count>0 or qm_weight>0 or ck_count>0 or ck_weight>0 or rk_count>0 or rk_weight>0 or qc_count>0 or qc_weight>0
            ) zl 
            left join `inventorymaterial` im
            on im.IM_Num=zl.IM_Num
            where ".$where_str."
            order by clsort,zl.L_Name,bh,length ASC";
            // pri($sql,1);
            $list = DB::query($sql);
            $qc_count = $qc_weight = $rk_count = $rk_weight = $ck_count = $ck_weight = $qm_count = $qm_weight = 0;
            $key_name = ["length","width","qc_count","qc_weight","rk_count","rk_weight","ck_count","ck_weight","qm_count","qm_weight"];
            foreach($list as $k=>$v){
                foreach($key_name as $vv){
                    $list[$k][$vv] = round($v[$vv],2);
                }
                $qc_count += $v["qc_count"];
                $qc_weight += $v["qc_weight"];
                $rk_count += $v["rk_count"];
                $rk_weight += $v["rk_weight"];
                $ck_count += $v["ck_count"];
                $ck_weight += $v["ck_weight"];
                $qm_count += $v["qm_count"];
                $qm_weight += $v["qm_weight"];
            }
            $list[] = ["IM_Spec"=>"合计","qc_count"=>$qc_count,"qc_weight"=>round($qc_weight,2),"rk_count"=>$rk_count,"rk_weight"=>round($rk_weight,2),"ck_count"=>$ck_count,"ck_weight"=>round($ck_weight,2),"qm_count"=>$qm_count,"qm_weight"=>round($qm_weight,2)];
            $result = array("total" => count($list), "rows" => $list);
            return json($result);
        }
        return $this->view->fetch();
    }

    //ids 为sod_id
    public function edit($ids=null)
    {
        $row = $this->firstModel->alias("so")
            ->join(["storeoutdetail"=>"sod"],"so.SO_Num = sod.SO_Num")
            ->where("sod.SOD_ID",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();

        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $so_id = $row["SO_ID"];
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $WR_Num = $row["WR_Num"];
                $sectSaveList = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk]["WR_Num"] = $WR_Num;
                        $sectSaveList[$kk]["LuPiHao"] = $row["WR_Lu"];
                        $sectSaveList[$kk]["PiHao"] = $row["WR_Pi"];
                        if($k=="WRD_ID" and $vv==false) continue;
                        else if($vv=="") continue;
                        else if($vv=="WRD_Length" or $vv=="WRD_Width") $sectSaveList[$kk][$k] = $vv*1000;
                        else $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("WR_Num",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $ss_join_list = [
            "ss.IM_Num = sod.IM_Num",
            "ss.L_Name = sod.L_Name",
            "ss.SS_Length = sod.SOD_Length",
            "ss.SS_Width = sod.SOD_Width",
            "ss.SS_WareHouse = '".$row["SO_WareHouse"]."'"
        ];
        $list = $this->model->alias("sod")
            ->join(["storeindetail"=>"sid"],"sid.SID_ID = sod.SID_ID","left")
            ->join(["storein"=>"si"],"si.SI_OtherID = sid.SI_OtherID","left")
            ->join(["storestate"=>"ss"],$ss_join_list,"left")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = sod.IM_Num","left")
            ->join(["workreceivedetail"=>"wrd"],"wrd.WRD_ID = sod.WRD_ID","left")
            ->field("im.IM_PerWeight,sod.SOD_ID,sod.SID_ID,im.IM_Num,'".$row["SO_WareHouse"]."' as SO_WareHouse,im.IM_Class,sod.L_Name,im.IM_Spec,round(sod.SOD_Length/1000,2) as SOD_Length,round(sod.SOD_Width/1000,2) as SOD_Width,sod.SOD_Count,sod.SOD_Weight,(ss.SS_Count-sod.SOD_Count) as SS_Count,(ss.SS_Weight-sod.SOD_Weight) as SS_Weight,sid.SID_PlanPrice,sod.SOD_Money,sid.SID_NoTaxPrice,sod.SOD_NoTaxMoney,wrd.WRD_PlanCount,wrd.WRD_PlanWeight,sid.SID_CTD_Pi,sid.LuPiHao,sid.PiHao,sid.CT_Num,left(si.SI_InDate,10) as SI_InDate,sod.SOD_Memo,sod.purpose,sod.SOD_BWeight,sid.SID_testnum,sid.SID_TestResult,(SOD_BWeight*SOD_Money/1000) as SOD_BMoney")
            ->where("sod.SO_Num",$row["SO_Num"])
            ->where("si.SI_WareHouse",$row["SO_WareHouse"])
            ->order("im.IM_Class,sod.L_Name,im.IM_Spec asc")
            ->select();

        $tableField = $this->getDetailField();
        $this->view->assign("row", $row);
        $this->view->assign("detailList", $list);
        $this->view->assign("wareclassList",$this->wareclassList());
        $this->view->assign("tableField", $tableField);
        $this->assignconfig("tableField", $tableField);
        return $this->view->fetch();
    }

    public function getDetailField(){
        $tableField = [
            ["比重","IM_PerWeight","readonly","hidden","40",""],
            ["出库ID","SOD_ID","readonly","","50","save"],
            ["入库ID","SID_ID","readonly","","50","save"],
            ["存货编码","IM_Num","readonly","","70","save"],
            ["所在仓库","SO_WareHouse","readonly","","90","save"],
            ["材料名称","IM_Class","readonly","","60",""],
            ["材质","L_Name","readonly","","70",""],
            ["规格","IM_Spec","readonly","","75",""],
            ["长度(m)","SOD_Length","readonly","","40","save"],
            ["宽度(m)","SOD_Width","readonly","","40","save"],
            ["出库数量","SOD_Count","","","40","save"],
            ["出库重量(kg)","SOD_Weight","readonly","","65","save"],
            //剩余
            ["剩余数量","SS_Count","readonly","","40",""],
            ["剩余重量(kg)","SS_Weight","readonly","","65",""],
            
            ["含税单价","SID_PlanPrice","readonly","","60","save"],
            ["含税金额","SOD_Money","readonly","","85","save"],
            ["不含税单价","SID_NoTaxPrice","readonly","","85","save"],
            ["不含税金额","SOD_NoTaxMoney","readonly","","85","save"],
            //计划
            ["计划单价","WRD_PlanCount","readonly","","60",""],
            ["计划金额","WRD_PlanWeight","readonly","","85",""],

            ["进货批次","SID_CTD_Pi","readonly","","60",""],
            ["炉号","LuPiHao","readonly","","110",""],
            ["批号","PiHao","readonly","","110",""],
            //理化申请 暂时不用
            // ["质保书","WRD_Unit","readonly","","30","save"],
            ["委试号","CT_Num","readonly","","90",""],
            ["入库时间","SI_InDate","readonly","","80",""],
            ["备注","SOD_Memo","","","80","save"],
            ["用途","purpose","","","80","save"],
            ["过磅重量","SOD_BWeight","","","65","save"],
            //过磅金额 = 含税金额 
            ["过磅金额","SOD_BMoney","readonly","","85",""],
            ["试验编号","SID_testnum","readonly","","80",""],
            //到货单 记得补上
            // ["到货日期","WRD_Unit","","","30",""],
            ["生产厂家","SID_TestResult","readonly","","80",""],
            //到货和理化申请都有 记得补上
            // ["供应商","WRD_Unit","","","30","save"]
        ];
        return $tableField;
    }
}
