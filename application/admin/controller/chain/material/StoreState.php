<?php

namespace app\admin\controller\chain\material;

use app\admin\model\chain\material\StoreIn;
use app\admin\model\chain\material\StoreOut;
use app\admin\model\chain\material\StoreInDetail;
use app\admin\model\chain\material\StoreOutDetail;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 原材料库存
 *
 * @icon fa fa-circle-o
 */
class StoreState extends Backend
{
    
    /**
     * StoreState模型对象
     * @var \app\admin\model\chain\material\StoreState
     */
    protected $model = null;
    protected $noNeedLogin = ["right","eipUpload"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\material\StoreState;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index($type=0)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $having_filter = [];
            isset($filter["ASF_Count"])?($having_filter["ASF_Count"] = $filter["ASF_Count"]):"";
            isset($filter["ASF_Weight"])?($having_filter["ASF_Weight"] = $filter["ASF_Weight"]):"";
            $having = [];
            foreach($having_filter as $k=>$v){
                $arr = array_slice(explode(',', $v), 0, 2);
                $sym = "between";
                if (stripos($v, ',') === false || !array_filter($arr, function($v){
                    return $v != '' && $v !== false && $v !== null;
                })) {
                    continue;
                }
                //当出现一边为空时改变操作符
                if ($arr[0] === '') {
                    $sym = '<=';
                    $arr = $arr[1];
                } elseif ($arr[1] === '') {
                    $sym = '>=';
                    $arr = $arr[0];
                }
                if($sym=="between") $having[$k] = $k.">=".$arr[0]." and ".$k."<=".$arr[1];
                else $having[$k] = $k.$sym.$arr;
            }
            $having_field = (empty($having))?"":implode(" and ",$having);


            $where_ini = [
                "m.SS_Weight" => [">",0],
                // "m.SS_WareHouse" => ["not like",'%不合格%']
            ];
            $summary_query = "(select WR_WareHouse,IM_Num,L_Name,IM_Spec,WRD_Length,WRD_Width,sum(sy_count) as sy_count,sum(sy_weight) as sy_weight from (select wr.WR_WareHouse,wrd.D_IM_Num as IM_Num,wrd.D_L_Name as L_Name,wrd.D_IM_Spec as IM_Spec,wrd.WRD_Length,wrd.WRD_Width,wrd.WRD_RealCount-ifnull(sum(SOD_Count),0) as sy_count,wrd.WRD_RealWeight-ifnull(sum(SOD_Weight),0) as sy_weight
			from workreceive wr
			inner join workreceivedetail wrd on wr.WR_Num=wrd.WR_Num
			LEFT join storeoutdetail sod on wrd.WRD_ID=sod.WRD_ID 
			where wr.WriteDate>='2022-05-28' and wr.WR_Send=0
			GROUP BY wrd.WRD_ID) aw group by WR_WareHouse,IM_Num,L_Name,IM_Spec,WRD_Length,WRD_Width)";
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("m")
                ->join(["inventorymaterial"=>"im"],"m.IM_Num = im.IM_Num")
                ->join([$summary_query=>"w"],["w.WR_WareHouse=m.SS_WareHouse","w.IM_Num=m.IM_Num","w.L_Name=m.L_Name","w.IM_Spec=m.IM_Spec","w.WRD_Length=m.SS_Length","w.WRD_Width=m.SS_Width"],"left")
                ->field("m.SS_ID,m.IM_Num,im.IM_Class,m.L_Name,m.IM_Spec,m.IM_Spec as 'm.IM_Spec',round(SS_Length/1000,3) as SS_Length,round(SS_Width/1000,3) as SS_Width,SS_Count,SS_Weight,(SS_Count-ifnull(w.sy_count,0)) as ASF_Count,(SS_Weight-ifnull(w.sy_weight,0)) as ASF_Weight,m.SS_WareHouse,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
                when im.IM_Class='角钢' then 
                SUBSTR(REPLACE(m.IM_Spec,'∠',''),1,locate('*',REPLACE(m.IM_Spec,'∠',''))-1)*1000+
                SUBSTR(REPLACE(m.IM_Spec,'∠',''),locate('*',REPLACE(m.IM_Spec,'∠',''))+1,2)*1
                when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
                REPLACE(m.IM_Spec,'-','')
                else 0
                end) as UNSIGNED) bh")
                ->where($where_ini)
                ->where($where)
                // ->order("clsort,ad.L_Name,bh,AD_Length ASC")
                // ->order("SS_WareHouse desc,m.IM_Num ASC,im.IM_Class ASC,m.L_Name ASC,m.SS_Length ASC")
                ->order("SS_WareHouse desc,clsort ASC,m.IM_Num ASC,m.L_Name ASC,bh ASC,m.SS_Length ASC")
                ->having($having_field)
                ->select();
            // pri($list,1);
            $total = ["SS_Count"=>0,"SS_Weight"=>0,"ASF_Count"=>0,"ASF_Weight"=>0];
            $key_total = array_keys($total);
            $rows = [];
            if($type == 0){
                foreach($list as $k=>$v){
                    $v["SS_Length"] = round($v["SS_Length"],3);
                    $v["SS_Width"] = round($v["SS_Width"],3);
                    foreach($key_total as $val){
                        $total[$val] += $v[$val];
                        $v[$val] = round($v[$val],2);
                    }
                    $rows[] = $v->toArray();
                }
            }else{
                $flag = "";
                $total_list = ["SS_Count"=>0,"SS_Weight"=>0,"ASF_Count"=>0,"ASF_Weight"=>0];
                foreach($list as $k=>$v){
                    if($v["SS_WareHouse"]=="铁塔镀锌仓库" or $v["SS_WareHouse"]=="不合格品库"){
                        $flag = $flag==""?$v["SS_WareHouse"]:$flag;
                        if($flag != $v["SS_WareHouse"]){
                            $total_list["L_Name"] = "小计";
                            $rows[] = $total_list;
                            $total_list = ["SS_Count"=>0,"SS_Weight"=>0,"ASF_Count"=>0,"ASF_Weight"=>0];
                        }
                        $v["SS_Length"] = round($v["SS_Length"],3);
                        $v["SS_Width"] = round($v["SS_Width"],3);
                        foreach($key_total as $val){
                            $total_list[$val] += $v[$val];
                            $total[$val] += $v[$val];
                            $v[$val] = round($v[$val],2);
                        }
                        $rows[] = $v->toArray();
                        $flag = $v["SS_WareHouse"];
                    }else{
                        $flag = $flag==""?($v["L_Name"].'-'.$v["IM_Num"]):$flag;
                        if($flag != ($v["L_Name"].'-'.$v["IM_Num"])){
                            $total_list["L_Name"] = "小计";
                            $rows[] = $total_list;
                            $total_list = ["SS_Count"=>0,"SS_Weight"=>0,"ASF_Count"=>0,"ASF_Weight"=>0];
                        }
                        $v["SS_Length"] = round($v["SS_Length"],3);
                        $v["SS_Width"] = round($v["SS_Width"],3);
                        foreach($key_total as $val){
                            $total_list[$val] += $v[$val];
                            $total[$val] += $v[$val];
                            $v[$val] = round($v[$val],2);
                        }
                        $rows[] = $v->toArray();
                        $flag = ($v["L_Name"].'-'.$v["IM_Num"]);
                    }
                    if($k==(count($list)-1)){
                        $total_list["L_Name"] = "小计";
                        $rows[] = $total_list;
                    }
                }
            }
            foreach($total as $k=>$v){
                $total[$k] = round($v,3);
            }
            $total["L_Name"] = "合计";
            $rows[] = $total;
            

            $result = array("total" => count($list), "rows" => $rows);

            return json($result);
        }

        $this->assignconfig("inventoryList",$this->inventoryList());
        $this->assignconfig("wareclassList",$this->wareclassList());
        $this->assignconfig("limberList",$this->limberList());
        $this->assignconfig("type",$type);
        return $this->view->fetch();
    }

    public function integration()
    {
        $save_list = [];
        $list =  $this->model->where("IR_Num","01013019")->select();
        foreach($list as $row){
            $in_where = [
                "sid.IM_Num" => ["=",$row["IM_Num"]],
                "sid.L_Name" => ["=",$row["L_Name"]],
                "sid.SID_Length" => ["=",$row["SS_Length"]],
                "sid.SID_Width" => ["=",$row["SS_Width"]],
                "si.SI_WareHouse" => ["=",$row["SS_WareHouse"]],
            ];
            $this_rk = (new StoreInDetail())->alias("sid")
            ->join(["storein"=>"si"],"sid.SI_OtherID = si.SI_OtherID")
            ->field("sum(sid.SID_RestCount) as SS_Count,sum(sid.SID_RestWeight) as SS_Weight")
            ->where($in_where)
            ->where("sid.SID_RestWeight",">",0)
            ->group("IM_Num")
            ->find();
            if($this_rk) $save_list[] = [
                "SS_ID" => $row["SS_ID"],
                "SS_Count" => $this_rk["SS_Count"],
                "SS_Weight" => $this_rk["SS_Weight"]
            ];
        }
        $result = $this->model->allowField(true)->saveAll($save_list);
    }

    public function detail($ids=null)
    {
        if(!$ids or $ids=='undefined') $this->error(__('No Results were found'));
        $row = $this->model->alias("m")
            ->join(["inventorymaterial"=>"im"],"m.IM_Num = im.IM_Num")
            ->field("m.SS_ID,m.IM_Num,im.IM_Class,m.L_Name,m.IM_Spec,SS_Length as 'Length',round(SS_Length/1000,3) as SS_Length,SS_Width as 'Width',round(SS_Width/1000,3) as SS_Width,SS_Count,SS_Weight,SF_Count,SF_Weight,im.ifSuanByCount AS ifSuanByCount,m.SS_WareHouse")
            ->where("m.SS_ID",$ids)
            ->find();
        $row["SS_Length"] = round($row['SS_Length'],3);
        $row["SS_Width"] = round($row['SS_Width'],3);
        $row["SS_Count"] = round($row['SS_Count'],2);
        $row["SS_Weight"] = round($row['SS_Weight'],2);
        // pri($row,1);
        $zl_row = [
            "last_count" =>0,
            "last_weight" =>0,
            "this_rk_count" =>0,
            "this_rk_weight" =>0,
            "this_ck_count" =>0,
            "this_ck_weight" =>0
        ];
        //本月入库
        $in_where = [
            "sid.IM_Num" => ["=",$row["IM_Num"]],
            // "sid.IM_Class" => ["=",$row["IM_Class"]],
            "sid.L_Name" => ["=",$row["L_Name"]],
            // "sid.IM_Spec" => ["=",$row["IM_Spec"]],
            "sid.SID_Length" => ["=",$row["Length"]],
            "sid.SID_Width" => ["=",$row["Width"]],
            "si.SI_WareHouse" => ["=",$row["SS_WareHouse"]],
            // "si.SI_InDate" => ["between time",]
        ];
        $this_rk_list = [];
        $this_rk = (new StoreInDetail())->alias("sid")
            ->join(["storein"=>"si"],"sid.SI_OtherID = si.SI_OtherID")
            ->join(["materialgetnotice"=>"mgn"],"mgn.MGN_ID = sid.MGN_ID2","LEFT")
            ->join(["materialnote"=>"mn"],"mgn.MN_Num = mn.MN_Num","LEFT")
            ->join(["vendor"=>"v"],"v.V_Num = sid.V_Num","left")
            ->field("sid.SID_Count,sid.SID_Weight,sid.SI_OtherID,si.SI_InDate,mn.MN_Date,sid.SID_testnum,sid.SID_RestCount,sid.SID_RestWeight,sid.SID_CTD_Pi,sid.LuPiHao,sid.PiHao,v.V_Name,sid.IM_Num,sid.L_Name,sid.SID_Length,sid.SID_Width,si.SI_WareHouse,si.SI_InDate,sid.order,sid.SID_ID")
            ->where($in_where)
            ->where("sid.SID_RestWeight",">",0)
            // ->whereTime('si.SI_InDate', 'month')
            // ->order("si.SI_InDate ASC")
            ->order("sid.order asc")
            ->select();
        foreach($this_rk as $k=>$v){
            $this_rk_list[] = $v->toArray();
            if(strtotime($v["SI_InDate"]) >= strtotime(date("Y-m")."-01 00:00:00")){
                $zl_row["this_rk_count"] += $v["SID_Count"];
                $zl_row["this_rk_weight"] += $v["SID_Weight"];
            }
        }
        //本月出库
        $out_where = [
            "sid.IM_Num" => ["=",$row["IM_Num"]],
            // "sid.IM_Class" => ["=",$row["IM_Class"]],
            "sid.L_Name" => ["=",$row["L_Name"]],
            // "sid.IM_Spec" => ["=",$row["IM_Spec"]],
            "sid.SOD_Length" => ["=",$row["Length"]],
            "sid.SOD_Width" => ["=",$row["Width"]],
            "si.SO_WareHouse" => ["=",$row["SS_WareHouse"]],
            // "si.SO_TakeDate" => ["between time",[date("Y-m")."-01 00:00:00",date('Y-m-d', mktime(0, 0, 0,date('m')+1,1)-1)." 59:59:59"]]
        ];
        $this_rk = (new StoreOutDetail())->alias("sid")
            ->join(["storeout"=>"si"],"sid.SO_Num = si.SO_Num")
            ->field("sum(SOD_Count) as this_ck_count,sum(SOD_Weight) as this_ck_weight,sid.IM_Num,sid.L_Name,sid.SOD_Length,sid.SOD_Width,si.SO_WareHouse,si.SO_TakeDate")
            ->where($out_where)
            ->whereTime('si.SO_TakeDate', 'month')
            ->find();
        $zl_row["this_ck_count"] = $this_rk["this_ck_count"]??0;
        $zl_row["this_ck_weight"] = $this_rk["this_ck_weight"]??0;
        //上月结存
        $zl_row["last_count"] = $row["SS_Count"] + $zl_row["this_ck_count"] - $zl_row["this_rk_count"];
        $zl_row["last_weight"] = $row["SS_Weight"] + $zl_row["this_ck_weight"] - $zl_row["this_rk_weight"];
        // pri($row,1);
        $this->view->assign("row", $row);
        $this->view->assign("zl_row", $zl_row);
        $this->assignconfig("this_rk",$this_rk_list);
        return $this->view->fetch();
    }

    public function topping($sid_id = null)
    {
        if(!$sid_id) $this->error("置顶失败");
        $store_in_model = new StoreIn();
        $store_in_detail_model = new StoreInDetail();
        $one = $store_in_model->alias("si")
            ->join(["storeindetail"=>"sid"],"si.SI_OtherID=sid.SI_OtherID")
            ->where("sid.SID_ID",$sid_id)
            ->find();
        if(!$one) $this->error("置顶失败");
        $where = [
            "si.SI_WareHouse"=>["=",$one["SI_WareHouse"]],
            "sid.IM_Num"=>["=",$one["IM_Num"]],
            "sid.L_Name"=>["=",$one["L_Name"]],
            "sid.SID_Length"=>["=",$one["SID_Length"]],
            "sid.SID_Width"=>["=",$one["SID_Width"]],
            "sid.SID_ID"=>["<>",$one["SID_ID"]]
        ];
        $list = $store_in_model->alias("si")
            ->join(["storeindetail"=>"sid"],"si.SI_OtherID=sid.SI_OtherID")
            ->where($where)
            ->where("sid.SID_RestCount|sid.SID_RestWeight",">",0)
            ->order("sid.order asc,si.SI_InDate,sid.SID_ID desc")
            ->select();
        $update = [$sid_id=>["SID_ID"=>$sid_id,"order"=>0]];
        $order = 0;
        foreach($list as $k=>$v){
            $order++;
            $update[$v["SID_ID"]] = ["SID_ID"=>$v["SID_ID"],"order"=>$order];
        }
        $result = false;
        Db::startTrans();
        try {
            $result = $store_in_detail_model->allowField(true)->saveAll($update);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $this->success();
        } else {
            $this->error(__('No rows were updated'));
        }

    }

    

    public function zh()
    {
        die;
        $list = DB::QUERY("SELECT * FROM `storestate` WHERE `L_Name` = 'Q355B'  ORDER BY `IM_Spec` DESC, `SS_Length` DESC, `SS_WareHouse` DESC,SS_ID DESC");
        $save_list = [];
        foreach($list as $k=>$v){
            $key = $v["IM_Num"].'-'.$v["IM_Spec"].'-'.$v["L_Name"].'-'.$v["SS_Length"].'-'.$v["SS_Width"].'-'.$v["SS_WareHouse"];
            if(isset($save_list[$key])){
                $save_list[$key]["SS_Count"] += $v["SS_Count"];
                $save_list[$key]["SS_Weight"] += $v["SS_Weight"];
            }else{
                $save_list[$key] = $v;
            }
        }
        $count = $this->model->where("L_Name","Q355B")->delete();
        $result = $this->model->allowField(true)->saveAll($save_list,false);
        pri($count,count($result));
    }

    public function right()
    {
        $storeInModel = new StoreIn();
        $storeOutModel = new StoreOut();
        $msg = "";
        $iniStoreIn = $storeInModel->where("SI_AudioPepo","=","")->where("SI_WriteDate",">=","2022-10-01 00:00:00")->select();
        $iniStoreIn?($msg .= "2022-10-01之后仍存在未审核入库单，请优先审核!"):"";
        $iniStoreOut = $storeOutModel->where("SO_Auditor","=","")->where("SO_WriteDate",">=","2022-10-01 00:00:00")->select();
        $iniStoreOut?($msg .= "2022-10-01之后仍存在未审核出库单，请优先审核!"):"";

        if($msg) $this->error($msg);

        $warelist = $this->model->group("SS_WareHouse")->column("SS_WareHouse");
        Db::startTrans();
        try {
            foreach($warelist as $v){
                DB::Query("update storestate ss
                inner join (select IM_Num,L_Name,SID_Length,SID_Width,sum(SID_RestCount) as SID_RestCount,sum(SID_RestWeight) as SID_RestWeight from storein si inner join storeindetail sid on sid.SI_OtherID=si.SI_OtherID where si.SI_WareHouse='".$v."' GROUP BY IM_Num,L_Name,SID_Length,SID_Width) a on a.IM_Num=ss.IM_Num and a.L_Name=ss.L_Name and a.SID_Length=ss.SS_Length and a.SID_Width=ss.SS_Width and ss.SS_WareHouse='".$v."'
                set ss.SS_Count=a.SID_RestCount");
                DB::Query("update storestate ss
                inner join (select IM_Num,L_Name,SID_Length,SID_Width,sum(SID_RestCount) as SID_RestCount,sum(SID_RestWeight) as SID_RestWeight from storein si inner join storeindetail sid on sid.SI_OtherID=si.SI_OtherID where si.SI_WareHouse='".$v."' GROUP BY IM_Num,L_Name,SID_Length,SID_Width) a on a.IM_Num=ss.IM_Num and a.L_Name=ss.L_Name and a.SID_Length=ss.SS_Length and a.SID_Width=ss.SS_Width and ss.SS_WareHouse='".$v."'
                set ss.SS_Weight=a.SID_RestWeight");
            }
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        $this->success();

    }

    // public function eipUpload()
    // {
    //     $json_result = ["code"=>0,"msg"=>"失败"];
    //     $params = (new \app\admin\model\view\StateStoreView())->select();
    //     if(!$params) return json($json_result);
    //     $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
    //     $stateBzModel = new \app\admin\model\chain\material\StateBz();
    //     $errorData = $saveData = $eipUploadList = [];
    //     $param_url = "eipmatinventory";
    //     foreach($params as $k=>$v){
    //         $eipUploadList[$v["bz"]] = ["eip_upload"=>$v["eip_upload"],"bz"=>$v["bz"]];
    //         $v["id"]?$eipUploadList[$v["bz"]]["id"] = $v["id"]:"";
    //         isset($api_data[$v["bz"]])?"":$api_data[$v["bz"]] = [
    //             "purchaserHqCode" => "SGCC",
    //             "supplierCode" => "1000014615",
    //             "categoryCode" => "60",
    //             "subclassCode" => "60001",
    //             "matName" => $v["bz"],
    //             "matCode" => $v["IM_Class"]=="角钢"?"MAT_4526358899591262803":"MAT_4526358899591262804",
    //             "matNum" => $v["SS_Weight"],
    //             "matUnit" => "kg",
    //             "matVoltageLevel" => "1000KV",
    //             "matProdAddr" => "",
    //             "putStorageTime" => ""
    //         ];
    //     }
    //     foreach($api_data as $k=>$v){
    //         $operatetype = "ADD";
    //         if($eipUploadList[$k]["eip_upload"]){
    //             $operatetype = "UPDATE";
    //         }
    //         $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$v);
    //         if($api_result["code"]==1){
    //             $saveData[$k] = $eipUploadList[$k];
    //             $saveData[$k]["eip_upload"] = 1;
    //         }else $errorData[$k] = $api_result["msg"];
    //     }
    //     $msg = "上传成功";
    //     if(!empty($errorData)) {
    //         $msg = "";
    //         foreach($errorData as $k=>$v){
    //             $msg .= $k.$v.";";
    //         }
    //         $json_result["msg"] = $msg;
    //     }else $json_result = ["code"=>1,"msg"=>$msg];
    //     if(!empty($saveData)) $stateBzModel->allowField(true)->saveAll($saveData);
    //     return json($json_result);
    // }

}
