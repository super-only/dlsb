<?php

namespace app\admin\controller\chain\fastening;

use app\admin\model\chain\fastening\BoltStoreIn;
use app\admin\model\chain\lofting\HeightBolt;
use app\admin\model\chain\sale\Task;
use app\admin\model\chain\sale\TaskDetail;
use app\admin\model\jichu\yw\ReceiveSendClass;
use app\admin\Model\jichu\ch\WareClass;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class BoltStoreOutSingle extends Backend
{
    
    /**
     * BoltStoreOutSingle模型对象
     * @var \app\admin\model\chain\fastening\BoltStoreOutSingle
     */
    protected $model = null;
    protected $noNeedLogin = ["offerU"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\fastening\BoltStoreOutSingle;
        $this->firstModel = new \app\admin\model\chain\fastening\BoltStoreOut;
        $this->stateModel = new \app\admin\model\chain\fastening\BoltStoreStock;
        $this->viewModel = new \app\admin\model\chain\fastening\BoltStoreOutSingleView();
        $this->admin = \think\Session::get('admin');
        $defaultTime = date("Y-m-1 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        $receive_row = (new ReceiveSendClass())->field("RSC_Num,RSC_Name")->where("RSC_Num",'LIKE','05%')->order("RSC_Num ASC")->select();
        $receive_list = [];
        foreach($receive_row as $k=>$v){
            $receive_list[$v["RSC_Name"]] = $v["RSC_Name"];
        }
        $this->receive_list = $receive_list;
        $this->view->assign("receive_list",$receive_list);
        $this->assignconfig("receive_list",$receive_list);
        $wareclass = (new WareClass())->field("WC_Name,WC_Num")->where("WC_Sort","螺栓")->select();
        $ware_list = [];
        $deptArr = [];
        $wareArr = [];
        foreach($wareclass as $k=>$v){
            $ware_list[$v["WC_Name"]] = $v["WC_Name"];
            $wareArr[$v["WC_Name"]] = $v["WC_Num"];
        }
        $this->wareArr = $wareArr;
        $this->ware_list = $ware_list;
        $this->view->assign("ware_list",$ware_list);
        $deptList = [""=>"请选择"];
        $deptModel = (new \app\admin\model\jichu\jg\Deptdetail())
            ->field("DD_Name,DD_Num")
            ->where(["Valid"=>1])
            ->order(["ParentNum"=>"ASC","DD_Num"=>"ASC"])
            ->select();
        foreach($deptModel as $v){
            $deptList[$v["DD_Name"]] = $v["DD_Name"];
            $deptArr[$v["DD_Name"]] = $v["DD_Num"];
        }
        $this->deptArr = $deptArr;
        $this->view->assign("deptList",$deptList);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->viewModel
                ->where($where)
                ->order($sort,$order)
                ->order("IM_LName,IM_Num,IM_Spec")
                ->paginate($limit);
           
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        // $this->assignconfig("rsclist",["到货出库"=>"到货出库","盘盈出库"=>"盘盈出库"]);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $content = $this->request->post("data");
            $content = $content?json_decode($content,true):[];
            $params = [];
            $paramsTable = [];
            foreach($content as $k=>$v){
                if(substr($v["name"],0,4) == "row["){
                    $key = rtrim(ltrim(($v["name"]),"row["),"]");
                    $params[$key] = $v["value"];
                }else{
                    $key = rtrim(ltrim(($v["name"]),"table_row["),"][]");
                    $paramsTable[$key][] = $v["value"];
                }
            }
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $Out_Num = $params["Out_Num"];
                if($Out_Num){
                    $one = $this->firstModel->where("Out_Num",$Out_Num)->find();
                    if($one) return json(["code"=>0,"msg"=>"单号不能重复，请重新填写或者自动生成"]);
                }else{
                    $month = date("ym");
                    $one = $this->firstModel->field("Out_Num")->where("Out_Num","LIKE","JCK".$month.'-%')->order("Out_Num DESC")->find();
                    if($one){
                        $num = substr($one["Out_Num"],8);
                        $Out_Num = 'JCK'.$month.'-'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $Out_Num = 'JCK'.$month.'-001';
                }
                $params["Out_Num"] = $Out_Num;
                $msg = "";
                $sectSaveList = [];
                foreach($paramsTable["BsOut_ID"] as $k=>$v){
                    if(!$paramsTable["V_Num"][$k]) $msg .="第".($k+1)."行缺少供应商<br>";
                    if(!$paramsTable["OutCount"][$k]) $msg .="第".($k+1)."行缺少数量<br>";
                    if(!$paramsTable["Out_Bsid"][$k]) $msg .="第".($k+1)."行选择有误<br>";
                    $sectSaveList[$k] = [
                        "Out_Num" => $Out_Num,
                        "BsOut_ID" => $v,
                        "Out_Bsid" => $paramsTable["Out_Bsid"][$k],
                        "V_Num" => $paramsTable["V_Num"][$k],
                        "IM_Num" => $paramsTable["IM_Num"][$k],
                        "IM_LName" => $paramsTable["IM_LName"][$k],
                        "IM_Length" => $paramsTable["IM_Length"][$k],
                        "OutCount" => $paramsTable["OutCount"][$k],
                        "OutWeight" => $paramsTable["OutWeight"][$k],
                        "OutNaxPrice" => $paramsTable["OutCount"][$k]?round($paramsTable["OutNaxSumPrice"][$k]/$paramsTable["OutCount"][$k],4):0,
                        "OutNaxSumPrice" => $paramsTable["OutNaxSumPrice"][$k],
                        "OutSort" => $paramsTable["OutSort"][$k],
                        "OutSingleMemo" => $paramsTable["OutSingleMemo"][$k]
                    ];
                    if(!$v) unset($sectSaveList[$k]["BsOut_ID"]);
                }
                if($msg) return json(["code"=>0,"msg"=>$msg]);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        $this->firstModel::create($params);
                        $result = $this->model->allowField(true)->saveAll(array_values($sectSaveList));
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    } catch (PDOException $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    } catch (Exception $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    }
                }
                
                if ($result !== false) {
                    return json(["code"=>1,"msg"=>"成功！","data"=>$Out_Num]);
                } else {
                    return json(["code"=>0,"msg"=>__('No rows were updated')]);
                }
            }
            return json(["code"=>0,"msg"=>__('Parameter %s can not be empty', '')]);
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        return $this->view->fetch();
    }

    public function chooseMaterial($warehouse = "")
    {
        if($warehouse == "") $this->error("请先选择仓库");
        if ($this->request->isAjax()) {
            $subsql = $this->model->field("Out_Bsid,sum(OutCount) as OutCount,sum(OutWeight) as OutWeight,sum(OutNaxSumPrice) as OutNaxSumPrice")
                ->group("Out_Bsid")->select();
            $out_detail = [];
            foreach($subsql as $k=>$v){
                $out_detail[$v["Out_Bsid"]] = $v->toArray();
            }

            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new BoltStoreIn())->alias("si")
                ->join(["boltstoreinsingle"=>"sid"],"si.In_Num = sid.In_Num","left")
                ->join(["inventorymaterial"=>"im"],"sid.IM_Num = im.IM_Num","left")
                ->join(["vendor"=>"v"],"v.V_Num = sid.V_Num","left")
                ->field("sid.Bsid,si.In_Num,si.InDate,v.V_ShortName,sid.IM_Num,im.IM_Class,sid.IM_LName,im.IM_Spec,sid.IM_Length,im.IM_Measurement,sid.InCount,sid.InWeight,sid.InSort,sid.InNaxPrice,sid.InNaxSumPrice,si.Memo,si.Writer,si.Auditor,sid.BsMemo,sid.InCount as restcount,sid.InWeight as restweight,sid.InNaxSumPrice as restsumprice,v.V_Num,(CASE WHEN sid.InCount=0 THEN 0 ELSE round(sid.InWeight/sid.InCount,4) END) as dz")
                ->where("si.Auditor","<>","")
                ->where("si.InPlace",$warehouse)
                // ->where("si.In_Num","<>","JRK2212-003")
                // ->where("si.In_Num","<>","JRK2304-014")
                // ->where("si.In_Num","<>","JRK2304-012")
                ->where($where)
                ->order("si.InDate desc")
                ->select();
            $in_list = [];
            foreach($list as $k=>$v){
                $in_list[$k] = $v->toArray();
                if(isset($out_detail[$v["Bsid"]])){
                    $in_list[$k]["restcount"] -= $out_detail[$v["Bsid"]]["OutCount"];
                    $in_list[$k]["restweight"] -= $out_detail[$v["Bsid"]]["OutWeight"];
                    $in_list[$k]["restsumprice"] -= $out_detail[$v["Bsid"]]["OutNaxSumPrice"];
                }
                if($in_list[$k]["restcount"]<=0 and $in_list[$k]["restweight"]<=0) unset($in_list[$k]);
            }
            $result = array("total" => count($in_list), "rows" => array_values($in_list));
            return json($result);
        }
        $this->assignconfig("warehouse",$warehouse);
        return $this->view->fetch();
    }

    public function selectProject()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new Task())->alias("t")
                ->join(["compact"=>"c"],"c.C_Num=t.C_Num","left")
                // ->field("t.P_Num,d.TD_ID,t.C_Num,t.C_Num AS 't.C_Num',c.PC_Num,t.T_Num,t.T_Num AS 't.T_Num',t.T_Sort,t.T_Company,t.T_WriterDate,t.t_project,t.t_shortproject,c.Customer_Name,d.TD_TypeName,d.TD_TypeName AS 'd.TD_TypeName',d.TD_Pressure,nn.old_TypeName")
                ->where($where)
                ->order($sort, $order)
                ->group("t.T_Num")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function checkProject($ids="")
    {
        if(!$ids) $this->error(__('No Results were found'));
        $sort = (new Task())->where("T_Num",$ids)->value("T_Sort");
        if(!$sort) $this->error(__('No Results were found'));
        $ex = $this->_technologyEx()[$sort];
        $sect_list = (new TaskDetail())->alias("td")
            ->join(["taskheight"=>"th"],"td.TD_ID = th.TD_ID")
            ->join(["sectconfigdetail"=>"scd"],"scd.TH_ID = th.TH_ID")
            ->where([
                "td.T_Num" => ["=",$ids]
            ])
            ->column("scd.SCD_ID");
        $list = (new HeightBolt([],$ex))->alias("hb")
            ->join(["boltdetail"=>"bd"],"hb.HB_ID = bd.HB_ID")
            ->field("BD_MaterialName,BD_Type,BD_Limber,BD_Lenth,sum(BD_SumCount) as BD_SumCount")
            ->group("BD_MaterialName,BD_Type,BD_Limber,BD_Lenth")
            ->where([
                "hb.SCD_ID" => ["IN",$sect_list]
            ])
            ->select();
        $this->assignconfig("list",$list);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->firstModel->alias("f")->join(["task"=>"t"],"f.T_Num = t.T_Num","left")->field("f.*,t.t_project")->where("Out_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        if ($this->request->isPost()) {
            $content = $this->request->post("data");
            $content = $content?json_decode($content,true):[];
            $params = [];
            $paramsTable = [];
            foreach($content as $k=>$v){
                if(substr($v["name"],0,4) == "row["){
                    $key = rtrim(ltrim(($v["name"]),"row["),"]");
                    $params[$key] = $v["value"];
                }else{
                    $key = rtrim(ltrim(($v["name"]),"table_row["),"][]");
                    $paramsTable[$key][] = $v["value"];
                }
            }
            if($row["Auditor"]) return json(["code"=>0,"msg"=>"该出库单已审核，无法修改"]);

            //月结的概念
            // $ck_list = $this->model->alias("rk")
            //     ->join(["boltstoreoutsingle"=>"ck"],"ck.Out_Bsid = rk.Bsid")
            //     ->where("rk.In_Num",$row["In_Num"])
            //     ->find();
            // if($ck_list) $this->error("已存在出库，不能进行修改！");

            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $Out_Num = $row["Out_Num"];
                $msg = "";
                $sectSaveList = [];
                foreach($paramsTable["BsOut_ID"] as $k=>$v){
                    if(!$paramsTable["V_Num"][$k]) $msg .="第".($k+1)."行缺少供应商<br>";
                    if(!$paramsTable["OutCount"][$k]) $msg .="第".($k+1)."行缺少数量<br>";
                    if(!$paramsTable["Out_Bsid"][$k]) $msg .="第".($k+1)."行选择有误<br>";
                    $sectSaveList[$k] = [
                        "Out_Num" => $Out_Num,
                        "BsOut_ID" => $v,
                        "Out_Bsid" => $paramsTable["Out_Bsid"][$k],
                        "V_Num" => $paramsTable["V_Num"][$k],
                        "IM_Num" => $paramsTable["IM_Num"][$k],
                        "IM_LName" => $paramsTable["IM_LName"][$k],
                        "IM_Length" => $paramsTable["IM_Length"][$k],
                        "OutCount" => $paramsTable["OutCount"][$k],
                        "OutWeight" => $paramsTable["OutWeight"][$k],
                        "OutNaxPrice" => $paramsTable["OutCount"][$k]?round($paramsTable["OutNaxSumPrice"][$k]/$paramsTable["OutCount"][$k],4):0,
                        "OutNaxSumPrice" => $paramsTable["OutNaxSumPrice"][$k],
                        "OutSort" => $paramsTable["OutSort"][$k],
                        "OutSingleMemo" => $paramsTable["OutSingleMemo"][$k]
                    ];
                    if(!$v) unset($sectSaveList[$k]["BsOut_ID"]);
                }
                if($msg) return json(["code"=>0,"msg"=>$msg]);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        $this->firstModel->where("Out_Num",$Out_Num)->update($params);
                        $result = $this->model->allowField(true)->saveAll(array_values($sectSaveList));
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    } catch (PDOException $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    } catch (Exception $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    }
                }
                if ($result !== false) {
                    return json(["code"=>1,"msg"=>"保存成功！"]);
                } else {
                    return json(["code"=>0,"msg"=>__('No rows were updated')]);
                }
            }
            return json(["code"=>0,"msg"=>__('Parameter %s can not be empty', '')]);
        }

        $list = $this->model->alias("m")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = m.IM_Num","left")
            ->join(["vendor"=>"v"],"v.V_Num = m.V_Num","left")
            ->field("m.BsOut_ID,m.Out_Bsid,(CASE WHEN m.OutCount=0 THEN 0 ELSE round(OutWeight/OutCount,4) END) as dz,m.V_Num,v.V_ShortName,m.IM_Num,im.IM_Class,m.IM_LName,im.IM_Spec,m.IM_Length,im.IM_Measurement,m.OutCount,m.OutWeight,m.OutNaxPrice,m.OutNaxSumPrice,m.OutSort,m.OutSingleMemo")
            ->where("Out_Num",$row["Out_Num"])
            ->order("Out_Num,IM_LName,IM_Spec,IM_Length")
            ->select();
        $sum_weight = $count = $price = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["OutWeight"] = round($v["OutWeight"],2);
            $rows[$k]["OutNaxPrice"] = round($v["OutNaxPrice"],4);
            $rows[$k]["OutNaxSumPrice"] = round($v["OutNaxSumPrice"],2);
            $sum_weight += $v["OutWeight"];
            $count += $v["OutCount"];
            $price += $v["OutNaxSumPrice"];
        }
        $row["sum_weight"] = $sum_weight;
        $row["count"] = $count;
        $row["price"] = $price;
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("list",$rows);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->firstModel->where("Out_Num",$num)->find();
        if($row["Auditor"]) return json(["code"=>0,"msg"=>"已审核"]);

        $list = $this->model->where("Out_Num",$num)->select();
        $state_list = [];
        $msg = "";
        foreach($list as $k=>$v){
            $key = $v["IM_Num"].'-'.$v["IM_LName"].'-'.$v["IM_Length"].'-'.$row["OutPlace"].'-'.$v["OutSort"].'-'.$v["V_Num"];
            isset($state_list[$key])?"":$state_list[$key] = [
                "IM_Num" => $v["IM_Num"],
                "IM_LName" => $v["IM_LName"],
                "IM_Length" => $v["IM_Length"],
                "BSS_Count" => 0,
                "BSS_Weight" => 0,
                "BSS_Place" => $row["OutPlace"],
                "BSS_Sort" => $v["OutSort"],
                "V_Num" => $v["V_Num"],
                "BSS_NaxPrice" => 0,
                "BSS_NaxSumMoney" => 0
            ];
            $state_list[$key]["BSS_Count"] += $v["OutCount"];
            $state_list[$key]["BSS_Weight"] += $v["OutWeight"];
            $state_list[$key]["BSS_NaxSumMoney"] += $v["OutNaxSumPrice"];
        }
        $state_update_list = $qingkuang = [];
        foreach($state_list as $k=>$v){
            $search_where = [
                "IM_Num" => ["=",$v["IM_Num"]],
                "IM_LName" => ["=",$v["IM_LName"]],
                "IM_Length" => ["=",$v["IM_Length"]],
                "BSS_Place" => ["=",$v["BSS_Place"]],
                "BSS_Sort" => ["=",$v["BSS_Sort"]],
                "V_Num" => ["=",$v["V_Num"]],
                // "BSS_NaxPrice" => ["=",$v["BSS_NaxPrice"]]
            ];
            $one = $this->stateModel->field("BSS_ID,BSS_Count,BSS_Weight,BSS_NaxSumMoney")->where($search_where)->find();
            if($one){
                $resetCount = $one["BSS_Count"]-$v["BSS_Count"];
                $resetWeight = $one["BSS_Weight"]-$v["BSS_Weight"];
                $resetWeight = $resetWeight<0?0:$resetWeight;
                $resetPrice = $one["BSS_NaxSumMoney"]-$v["BSS_NaxSumMoney"];
                $resetPrice = $resetPrice<0?0:$resetPrice;
                if($resetCount<0) $msg .= "存货编号为".$v["IM_Num"]."的材料存在大于库存数量的情况，请核查<br>";

                // if($resetCount<0){
                //     $resetCount = $one["BSS_Count"];
                //     $resetWeight = $one["BSS_Weight"];
                // }
                // if($resetWeight<0){
                //     $resetWeight = $one["BSS_Weight"];
                // }
                $state_update_list[] = [
                    "BSS_ID" => $one["BSS_ID"],
                    "BSS_Count" => $resetCount,
                    "BSS_Weight" => $resetWeight,
                    "BSS_NaxSumMoney" => $resetPrice,
                    "BSS_NaxPrice" => $resetCount?round($resetPrice/$resetCount,2):0
                ];
            }else $msg .= "存货编号为".$v["IM_Num"]."的材料存在没有库存的情况，请核查<br>";
        }
        if($msg) return json(["code"=>0,"msg"=>$msg]);
        $update = [
            "Auditor" => $this->admin["nickname"],
            "AuditorDate" => date("Y-m-d H:i:s")
        ];
        $result = FALSE;
        if(!empty($state_update_list)){
            Db::startTrans();
            try {
                $this->firstModel->update($update,["Out_Num"=>$num]);
                $result = $this->stateModel->allowField(true)->saveAll(array_values($state_update_list));
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"弃审失败，请稍后重试"]);
        $row = $this->firstModel->where("Out_Num",$num)->find();
        if(!$row["Auditor"]) return json(["code"=>0,"msg"=>"已弃审"]);

        $list = $this->model->where("Out_Num",$num)->select();
        $state_list = [];
        $msg = "";
        foreach($list as $k=>$v){
            $key = $v["IM_Num"].'-'.$v["IM_LName"].'-'.$v["IM_Length"].'-'.$row["OutPlace"].'-'.$v["OutSort"].'-'.$v["V_Num"];
            isset($state_list[$key])?"":$state_list[$key] = [
                "IM_Num" => $v["IM_Num"],
                "IM_LName" => $v["IM_LName"],
                "IM_Length" => $v["IM_Length"],
                "BSS_Count" => 0,
                "BSS_Weight" => 0,
                "BSS_Place" => $row["OutPlace"],
                "BSS_Sort" => $v["OutSort"],
                "V_Num" => $v["V_Num"],
                "BSS_NaxPrice" => 0,
                "BSS_NaxSumMoney" => 0
            ];
            $state_list[$key]["BSS_Count"] += $v["OutCount"];
            $state_list[$key]["BSS_Weight"] += $v["OutWeight"];
            $state_list[$key]["BSS_NaxSumMoney"] += $v["OutNaxSumPrice"];
        }
        $state_update_list = [];
        foreach($state_list as $k=>$v){
            $search_where = [
                "IM_Num" => ["=",$v["IM_Num"]],
                "IM_LName" => ["=",$v["IM_LName"]],
                "IM_Length" => ["=",$v["IM_Length"]],
                "BSS_Place" => ["=",$v["BSS_Place"]],
                "BSS_Sort" => ["=",$v["BSS_Sort"]],
                "V_Num" => ["=",$v["V_Num"]],
                // "BSS_NaxPrice" => ["=",$v["BSS_NaxPrice"]]
            ];
            $one = $this->stateModel->field("BSS_ID,BSS_Count,BSS_Weight,BSS_NaxSumMoney")->where($search_where)->find();
            if($one){
                $resetCount = $one["BSS_Count"]+$v["BSS_Count"];
                $resetWeight = $one["BSS_Weight"]+$v["BSS_Weight"];
                $resetPrice = $one["BSS_NaxSumMoney"]+$v["BSS_NaxSumMoney"];
                $state_update_list[] = [
                    "BSS_ID" => $one["BSS_ID"],
                    "BSS_Count" => $resetCount,
                    "BSS_Weight" => $resetWeight,
                    "BSS_NaxSumMoney" => $resetPrice,
                    "BSS_NaxPrice" => $resetCount?round($resetPrice/$resetCount,2):0
                ];
            }else $msg .= "存货编号为".$v["IM_Num"]."的材料存在没有库存的情况，请核查<br>";
        }
        if($msg) return json(["code"=>0,"msg"=>$msg]);
        $update = [
            "Auditor" => '',
            "AuditorDate" => "0000-00-00 00:00:00"
        ];
        $result = FALSE;
        if(!empty($state_update_list)){
            Db::startTrans();
            try {
                $this->firstModel->update($update,["Out_Num"=>$num]);
                $result = $this->stateModel->allowField(true)->saveAll(array_values($state_update_list));
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result) {
            $this->success("弃审成功！");
        } else {
            $this->error("弃审失败！");
        }
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){

            $in_detail = $this->model->alias("sid")->join(["boltstoreout"=>"si"],"sid.Out_Num = si.Out_Num")->where("sid.BsOut_ID",$num)->find();
            if(!$in_detail) return json(["code"=>0,"msg"=>"删除失败"]);
            if($in_detail["Auditor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);
            $count = $this->model->where("BsOut_ID",$num)->delete();
            if($count) return json(["code"=>1,'msg'=>"删除成功"]);
            else return json(["code"=>0,"msg"=>"删除失败"]);
            
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function allDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);

        $row = $this->firstModel->where("Out_Num",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"该条信息已不存在，或者请稍后重试"]);
        if($row["Auditor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);

        $result=0;
        Db::startTrans();
        try {
            $result += $this->firstModel->where("Out_Num",$num)->delete();
            $result += $this->model->where("Out_Num",$num)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result != false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["ID","BsOut_ID","readonly class='small_input'","","hidden","save","ID"],
            ["Out_Bsid","Out_Bsid","readonly class='small_input'","","hidden","save","Bsid"],
            ["dz","dz","readonly class='small_input'","","hidden","save","dz"],
            ["V_Num","V_Num","readonly class='small_input'","","hidden","save","V_Num"],
            ["供应商简称","V_ShortName","readonly class='small_input gysxz'","","","save","V_ShortName"],
            ["存货编码","IM_Num","readonly class='small_input'","","","save","IM_Num"],
            ["存货名称","IM_Class","readonly class='small_input'","","","","IM_Class"],
            ["级别","IM_LName","readonly class='small_input'","","","save","IM_LName"],
            ["规格","IM_Spec","readonly class='small_input'","","","","IM_Spec"],
            ["无扣长","IM_Length","readonly class='small_input'","","","save","IM_Length"],
            ["单位","IM_Measurement","readonly class='small_input'",0,"","","IM_Measurement"],
            ["出库数量","OutCount","class='small_input'",0,"","save","restcount"],
            ["出库重量","OutWeight","class='small_input'",0,"","save","restweight"],
            ["不含税单价","OutNaxPrice","readonly class='small_input'",0,"","save","InNaxPrice"],
            ["不含税金额","OutNaxSumPrice","class='small_input'",0,"","save","restsumprice"],
            ["类别","OutSort","readonly class='small_input'","外购","","save","InSort"],
            ["说明","OutSingleMemo","class='small_input'","","","save","OutSingleMemo"]
        ];
        return $list;
    }

    public function offerU()
    {
        $id = $this->request->post("id");
        $row = $this->firstModel->where("Out_Num",$id)->find();
        $res = [
            "code"=>0,
            "msg" => "有误",
            "data" => []
        ];
        if(!$row) return json($res);
        else if($row["Auditor"]==''){
            $res["msg"] = "请先审核";
            return json($res);
        }
        $cdepcode = $row["Dept"];
        if(!$cdepcode){
            $res["msg"] = "不存在领用部门编号";
            return json($res);
        }
        $cwhcode = $row["OutPlace"];
        if(!$cwhcode){
            $res["msg"] = "不存在仓库编号";
            return json($res);
        }
        $main = [
            "ccode" => $id,
            //采购类型编码
            "cdepname" => $cdepcode,
            //收发类别
            "crdcode" => "201",
            "cwhname" => $cwhcode,
            "ddate" => date("Y-m-d",strtotime($row["OutDate"])),
            "cmaker" => $row['Writer'],
            "cmemo" => $row['OutMemo'],
            "chandler" => $row['Auditor'],
            "dveridate" => date("Y-m-d",strtotime($row["AuditorDate"]))
        ];
        $list = $this->model
            ->where("Out_Num",$id)
            ->select();
        $details = [];
        foreach($list as $k=>$v){
            $details[] = [
                "cinvcode" => $v["IM_Num"],
                "iquantity" => $v["OutWeight"],
                "cdefine22" => '',
                "cfree1" => '',
                "cfree2" => $v["IM_LName"],
                "cfree3" => '',
                "inum" => $v["OutCount"],
            ];
        }
        $data = [
            "main" => $main,
            "details" => $details
        ];
        $url = "/PostRdrecord11/Add";
        $result = api_post_u($url,$data);
        if($result["code"]==0){
            $this->firstModel->where("Out_Num",$id)->update(["offer"=>1]);
            $res["code"] = 1;
            $res["msg"] = "成功";
        }else $res["msg"] = $result["ErrorMsg"];
        return json($res);
    }
}
