<?php

namespace app\admin\controller\chain\fastening;

// use app\admin\model\chain\fastening\BoltStoreIn;
// use app\admin\model\chain\fastening\BoltStoreOut;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;

/**
 * 紧固件月结
 *
 * @icon fa fa-circle-o
 */
class BoltMonthEnd extends Backend
{
    
    /**
     * BoltMonthEnd模型对象
     * @var \app\admin\model\chain\fastening\BoltMonthEnd
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\fastening\BoltMonthEnd;
        $this->detailModel = new \app\admin\model\chain\fastening\BoltMonthEndSingle;
        $this->inModel = new \app\admin\model\chain\fastening\BoltStoreIn;
        $this->outModel = new \app\admin\model\chain\fastening\BoltStoreOut;
        $this->stateModel = new \app\admin\model\chain\fastening\BoltStoreStock;
        $this->admin = \think\Session::get('admin');
        $month_list = ["1"=>"1月","2"=>"2月","3"=>"3月","4"=>"4月","5"=>"5月","6"=>"6月","7"=>"7月","8"=>"8月","9"=>"9月","10"=>"10月","11"=>"11月","12"=>"12月"];
        $year = date("Y");
        $time_range = [];
        foreach($month_list as $k=>$v){
            $time_range[$k] = $this->get_monthinfo_by_date($year."-".$k."-01");
        }
        $this->month_list = $month_list;
        $this->time_range = $time_range;
        $this->view->assign("month_list",$month_list);
        $this->assignconfig("time_range",$time_range);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     /**
     * 添加
     */
    public function add()
    {
        
        $time_range = $this->time_range;
        $year = date("Y");
        $month = date("m");
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $store_in = $this->inModel->where("Auditor","=","")->whereTime("InDate","between",[$params["YME_DateFrom"], $params["YME_DateTo"]])->find();
                if($store_in) $this->error("本月存在未审核的入库单，请先审核");
                $store_out = $this->outModel->where("Auditor","=","")->whereTime("OutDate","between",[$params["YME_DateFrom"], $params["YME_DateTo"]])->find();
                if($store_out) $this->error("本月存在未审核的出库单，请先审核");

                $last_ym_num = "";
                $last_one = $this->model->where("YME_DateFrom","<=",$params["YME_DateFrom"])->order("YME_Num desc")->find();
                if($last_one){
                    if($last_one["YME_DateFrom"]==$params["YME_DateFrom"]) $this->success('',null,$last_one["YME_Num"]);
                    else if($last_one["YME_DateFrom"]!=date("Y-m-01 00:00:00",strtotime("last month",strtotime($params["YME_DateFrom"])))) $this->error("上个月尚未月结，请先月结上个月！");
                    else $last_ym_num = $last_one["YME_Num"];
                }

                $YME_Num = $params["YME_Num"];
                if($YME_Num){
                    $one = $this->model->where("YME_Num",$YME_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $one = $this->model->field("YME_Num")->where("YME_Num","LIKE","YJ".$year.'%')->order("YME_Num DESC")->find();
                    if($one){
                        $num = substr($one["YME_Num"],6);
                        $YME_Num = 'YJ'.$year.str_pad(++$num,4,0,STR_PAD_LEFT);
                    }else $YME_Num = 'YJ'.$year.'0001';
                }
                $params["YME_Num"] = $YME_Num;
                $params["Writer"] = $this->admin['nickname'];
                $params["WriterDate"] = date("Y-m-d H:i:s");

                $stock_list = $this->getBoltStateList($params,$last_ym_num);
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->allowField(true)->save($params);
                    $result = $this->detailModel->allowField(true)->saveAll($stock_list);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$YME_Num);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "year" => $year,
            "month" => $month,
            "now_time" => date("Y-m-d H:i:s"),
            "start_time" => $time_range[(int)$month]["month_start_day"],
            "end_time" => $time_range[(int)$month]["month_end_day"],
            "admin" => $this->admin["nickname"]
        ];
        $this->view->assign("row",$row);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                $store_in = $this->inModel->where("Auditor","=","")->whereTime("InDate","between",[$row["YME_DateFrom"], $row["YME_DateTo"]])->find();
                if($store_in) $this->error("本月存在未审核的入库单，请先审核");
                $store_out = $this->outModel->where("Auditor","=","")->whereTime("OutDate","between",[$row["YME_DateFrom"], $row["YME_DateTo"]])->find();
                if($store_out) $this->error("本月存在未审核的出库单，请先审核");

                $last_ym_num = "";
                $last_one = $this->model->where("YME_DateFrom","<",$row["YME_DateFrom"])->order("YME_Num desc")->find();
                if($last_one){
                    if($last_one["YME_DateFrom"]!=date("Y-m-01 00:00:00",strtotime("last month",strtotime($row["YME_DateFrom"])))) $this->error("上个月尚未月结，请先月结上个月！");
                    else $last_ym_num = $last_one["YME_Num"];
                }
                $stock_list = $this->getBoltStateList($row,$last_ym_num);
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("YME_Num",$ids)->update($params);
                    $this->detailModel->where("YME_Num",$ids)->delete();
                    $result = $this->detailModel->allowField(true)->saveAll($stock_list);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $this->detailModel->alias("bmes")
            ->join(["boltstorestock"=>"bss"],"bss.BSS_ID = bmes.SS_ID","left")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = bss.IM_Num","left")
            ->join(["vendor"=>"v"],"v.V_Num = bss.V_Num","left")
            ->field("v.V_ShortName,im.IM_Class,bss.IM_LName,im.IM_Spec,bss.IM_Length,im.IM_Measurement,bss.BSS_Sort,bmes.LastCount,bmes.LastMoney,bmes.InCount,bmes.InMoney,bmes.OutCount,bmes.OutMoney,bmes.FinalCount,bmes.AvgMoney,bss.BSS_Place,bmes.SS_ID")
            ->order("bss.V_Num,bss.IM_Num,bss.IM_LName,bss.IM_Length")
            ->where("bmes.YME_Num",$ids)
            ->select();
        $total = $rows = [];
        $total_field = ["LastCount","LastMoney","InCount","InMoney","OutCount","OutMoney","FinalCount","AvgMoney"];
        foreach($list as $k=>$v){
            if($v["SS_ID"]){
                $flag = 0;
                foreach($total_field as $vv){
                    $v[$vv] = round($v[$vv],2);
                    if($v[$vv]!=0) $flag = 1;
                }
                if($flag) $rows[] = $v->toArray();
            }else $total = [
                "IM_LName" => "合计",
                "LastCount" => $v["LastCount"],
                "LastMoney" => $v["LastMoney"],
                "InCount" => $v["InCount"],
                "InMoney" => $v["InMoney"],
                "OutCount" => $v["OutCount"],
                "OutMoney" => $v["OutMoney"],
                "FinalCount" => $v["FinalCount"],
                "AvgMoney" => $v["AvgMoney"]
            ];
        }
        $rows[] = $total;
        $this->assignconfig("ids",$ids);
        $this->assignconfig("list",$rows);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function summary()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row");
            $params = $params?json_decode($params,true):[];
            if ($params) {
                $store_in = $this->inModel->where("Auditor","=","")->whereTime("InDate","between",[$params["YME_DateFrom"], $params["YME_DateTo"]])->find();
                if($store_in) $this->error("本月存在未审核的入库单，请先审核");
                $store_out = $this->outModel->where("Auditor","=","")->whereTime("OutDate","between",[$params["YME_DateFrom"], $params["YME_DateTo"]])->find();
                if($store_out) $this->error("本月存在未审核的出库单，请先审核");

                $last_ym_num = "";
                $last_one = $this->model->where("YME_DateFrom","<=",$params["YME_DateFrom"])->order("YME_Num desc")->find();
                if($last_one){
                    if($last_one["YME_DateFrom"]==$params["YME_DateFrom"]) $this->success('',null,$last_one["YME_Num"]);
                    else if($last_one["YME_DateFrom"]!=date("Y-m-01 00:00:00",strtotime("last month",strtotime($params["YME_DateFrom"])))) $this->error("上个月尚未月结，请先月结上个月！");
                    else $last_ym_num = $last_one["YME_Num"];
                }

                $boltStoreNewsList = $this->boltStoreNews();
                $params["YME_Num"] = "";
                $stock_list = $this->getBoltStateList($params,$last_ym_num);
                foreach($stock_list as $k=>$v){
                    $stock_list[$k] = isset($boltStoreNewsList[$v["SS_ID"]])?array_merge($v,$boltStoreNewsList[$v["SS_ID"]]):$v;
                }
                return json(["code"=>1,"data"=>array_values($stock_list)]);
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return json(["code"=>0,"msg"=>"有误"]);
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->model->where("YME_Num",$num)->find();
        if($row["Auditor"]) return json(["code"=>0,"msg"=>"已审核"]);
        $update = [
            "Auditor" => $this->admin["nickname"],
            "AuditorDate" => date("Y-m-d H:i:s")
        ];
        $result = $this->model->update($update,["YME_Num"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"弃审失败，请稍后重试"]);
        $row = $this->model->where("YME_Num",$num)->find();
        if(!$row["Auditor"]) return json(["code"=>0,"msg"=>"已弃审"]);

        $update = [
            "Auditor" => '',
            "AuditorDate" => "0000-00-00 00:00:00"
        ];
        $result = $this->model->update($update,["YME_Num"=>$num]);
        if ($result) {
            $this->success("弃审成功！");
        } else {
            $this->error("弃审失败！");
        }
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $count = 0;
            Db::startTrans();
            try {
                $count += $this->model->where("YME_Num",$ids)->delete();
                $count += $this->detailModel->where("YME_Num",$ids)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    protected function getBoltStateList($params,$last_ym_num)
    {
        $stock_list = [];
        if($last_ym_num){
            $last_where = [
                "YME_Num" => ["=",$last_ym_num],
                "SS_ID" => ["<>",0]
            ];
            $last_list = $this->detailModel->alias("bmes")
                ->join(["boltstorestock"=>"bss"],"bmes.SS_ID = bss.BSS_ID")
                ->where($last_where)->select();
            foreach($last_list as $k=>$v){
                $key = $v["IM_Num"].'-'.$v["IM_LName"].'-'.$v["IM_Length"].'-'.$v["BSS_Place"].'-'.$v["BSS_Sort"].'-'.$v["V_Num"];
                $stock_list[$key] = [
                    "YME_Num" => $params["YME_Num"],
                    "SS_ID" => $v["SS_ID"],
                    "LastCount" => $v["FinalCount"],
                    "LastWeight" => $v["FinalWeight"],
                    "LastMoney" => $v["AvgMoney"],
                    "InCount" => 0,
                    "InWeight" => 0,
                    "InMoney" => 0,
                    "OutCount" => 0,
                    "OutWeight" => 0,
                    "OutMoney" => 0,
                    "FinalCount" => $v["FinalCount"],
                    "FinalWeight" => $v["FinalWeight"],
                    "AvgMoney" => $v["AvgMoney"],
                    "AvgPrice" => 0
                ];
            }
        }

        $in_list = $this->inModel->alias("bsi")->join(["boltstoreinsingle"=>"bsis"],"bsi.In_Num = bsis.In_Num")->field("IM_Num,IM_LName,IM_Length,InPlace,InSort,V_Num,sum(InCount) as incount,sum(InWeight) as inweight,sum(InNaxSumPrice) as price")->whereTime("InDate","between",[$params["YME_DateFrom"], $params["YME_DateTo"]])->group("IM_Num,IM_LName,IM_Length,InPlace,InSort,V_Num")->select();

        foreach($in_list as $k=>$v){
            
            $key = $v["IM_Num"].'-'.$v["IM_LName"].'-'.$v["IM_Length"].'-'.$v["InPlace"].'-'.$v["InSort"].'-'.$v["V_Num"];
            if(!isset($stock_list[$key])){
                $search_where = [
                    "IM_Num" => ["=",$v["IM_Num"]],
                    "IM_LName" => ["=",$v["IM_LName"]],
                    "IM_Length" => ["=",$v["IM_Length"]],
                    "BSS_Place" => ["=",$v["InPlace"]],
                    "BSS_Sort" => ["=",$v["InSort"]],
                    "V_Num" => ["=",$v["V_Num"]]
                ];
                $state_ss = $this->stateModel->field("BSS_ID")->where($search_where)->find();
                if($state_ss){
                    $stock_list[$key]=[
                        "YME_Num" => $params["YME_Num"],
                        "SS_ID" => $state_ss["BSS_ID"],
                        "LastCount" => 0,
                        "LastWeight" => 0,
                        "LastMoney" => 0,
                        "InCount" => $v["incount"],
                        "InWeight" => $v["inweight"],
                        "InMoney" => $v["price"],
                        "OutCount" => 0,
                        "OutWeight" => 0,
                        "OutMoney" => 0,
                        "FinalCount" => $v["incount"],
                        "FinalWeight" => $v["inweight"],
                        "AvgMoney" => $v["price"],
                        "AvgPrice" => ($v["incount"]?round($v["price"]/$v["incount"],2):0)
                    ];
                }
            }else{
                $stock_list[$key]["InCount"] = $v["incount"];
                $stock_list[$key]["InWeight"] = $v["inweight"];
                $stock_list[$key]["InMoney"] = $v["price"];
                $stock_list[$key]["FinalCount"] += $v["incount"];
                $stock_list[$key]["FinalWeight"] += $v["inweight"];
                $stock_list[$key]["AvgMoney"] += $v["price"];
                $stock_list[$key]["AvgPrice"] = ($v["incount"]?round($v["price"]/$v["incount"],2):0);
                
            }
            
        }

        $out_list = $this->outModel->alias("bsi")->join(["boltstoreoutsingle"=>"bsis"],"bsi.Out_Num = bsis.Out_Num")->field("IM_Num,IM_LName,IM_Length,OutPlace,OutSort,V_Num,sum(OutCount) as incount,sum(OutWeight) as inweight,sum(OutNaxSumPrice) as price")->whereTime("OutDate","between",[$params["YME_DateFrom"], $params["YME_DateTo"]])->group("IM_Num,IM_LName,IM_Length,OutPlace,OutSort,V_Num")->select();

        foreach($out_list as $k=>$v){
            $key = $v["IM_Num"].'-'.$v["IM_LName"].'-'.$v["IM_Length"].'-'.$v["OutPlace"].'-'.$v["OutSort"].'-'.$v["V_Num"];
            $stock_list[$key]["OutCount"] = $v["incount"];
            $stock_list[$key]["OutWeight"] = $v["inweight"];
            $stock_list[$key]["OutMoney"] = $v["price"];
            $stock_list[$key]["FinalCount"] -= $v["incount"];
            $stock_list[$key]["FinalWeight"] -= $v["inweight"];
            $stock_list[$key]["AvgMoney"] -= $v["price"];
        }
        $total = ["LastCount"=>0,"LastWeight"=>2,"LastMoney"=>2,"InCount"=>0,"InWeight"=>2,"InMoney"=>2,"OutCount"=>0,"OutWeight"=>2,"OutMoney"=>2,"FinalCount"=>0,"FinalWeight" =>2, "AvgMoney"=>2];
        foreach($stock_list as $k=>&$v){
            if($v["LastCount"]!=$v["FinalCount"] and $v["FinalCount"]<0){
                $v["OutCount"] += $v["FinalCount"];
                $v["FinalCount"] = 0;
            }
            if($v["LastWeight"]!=$v["FinalWeight"] and $v["FinalWeight"]<0){
                $v["OutWeight"] += $v["FinalWeight"];
                $v["FinalWeight"] = 0;
            }
            $flag = 0;
            foreach($total as $kk=>$vv){
                $v[$kk] = round($v[$kk],$vv);
                $total[$kk] += $v[$kk];
                if($v[$kk]!=0) $flag = 1;
            }
            if(!$flag) unset($stock_list[$kk]);
        }
        $stock_list[] = array_merge($total,["SS_ID"=>0,"YME_Num"=>$params["YME_Num"]]);
        return $stock_list;
    }

    /**
     * 打印
     */
    public function print($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        
        $list = $this->detailModel->alias("bmes")
            ->join(["boltstorestock"=>"bss"],"bss.BSS_ID = bmes.SS_ID","left")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = bss.IM_Num","left")
            ->join(["vendor"=>"v"],"v.V_Num = bss.V_Num","left")
            ->field("v.V_ShortName,im.IM_Class,bss.IM_LName,im.IM_Spec,bss.IM_Length,im.IM_Measurement,bss.BSS_Sort,bmes.LastCount,bmes.LastMoney,bmes.InCount,bmes.InMoney,bmes.OutCount,bmes.OutMoney,bmes.FinalCount,bmes.AvgMoney,bss.BSS_Place,bmes.SS_ID")
            ->order("bss.V_Num,bss.IM_Num,bss.IM_LName,bss.IM_Length")
            ->where("bmes.YME_Num",$ids)
            ->select();
        $total = $rows = [];
        $total_field = ["LastCount","LastMoney","InCount","InMoney","OutCount","OutMoney","FinalCount","AvgMoney"];
        foreach($list as $k=>$v){
            if($v["SS_ID"]){
                $flag = 0;
                foreach($total_field as $vv){
                    $v[$vv] = round($v[$vv],2);
                    if($v[$vv]!=0) $flag = 1;
                }
                if($flag) $rows[] = $v->toArray();
            }else $total = [
                "IM_LName" => "合计",
                "LastCount" => $v["LastCount"],
                "LastMoney" => $v["LastMoney"],
                "InCount" => $v["InCount"],
                "InMoney" => $v["InMoney"],
                "OutCount" => $v["OutCount"],
                "OutMoney" => $v["OutMoney"],
                "FinalCount" => $v["FinalCount"],
                "AvgMoney" => $v["AvgMoney"]
            ];
        }
        $rows[] = $total;
        $row['WriterDate'] = date("Y-m-d",strtotime($row["WriterDate"]));
        $row['AuditorDate'] = date("Y-m-d",strtotime($row["AuditorDate"]));
        $row['YME_DateTo'] = date("Y-m-d",strtotime($row["YME_DateTo"]));
        $row['PrintDate'] = date("Y-m-d");
        $this->assignconfig("ids",$ids);
        $this->assignconfig("list",$rows);
        $this->assignconfig("row", $row);
        return $this->view->fetch();
    }

    public function export($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $list = $this->detailModel->alias("bmes")
            ->join(["boltstorestock"=>"bss"],"bss.BSS_ID = bmes.SS_ID","left")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = bss.IM_Num","left")
            ->join(["vendor"=>"v"],"v.V_Num = bss.V_Num","left")
            ->field("v.V_ShortName,im.IM_Class,bss.IM_LName,im.IM_Spec,bss.IM_Length,im.IM_Measurement,bss.BSS_Sort,bmes.LastCount,bmes.LastMoney,bmes.InCount,bmes.InMoney,bmes.OutCount,bmes.OutMoney,bmes.FinalCount,bmes.AvgMoney,bss.BSS_Place,bmes.SS_ID,bss.IM_Num")
            ->order("bss.V_Num,bss.IM_Num,bss.IM_LName,bss.IM_Length")
            ->where("bmes.YME_Num",$ids)
            ->select();
        $title = $ids;
        $header = [
            ['仓库', 'BSS_Place'],
            ['供应商', 'V_ShortName'],
            ['材料编号', 'IM_Num'],
            ['存货名称', 'IM_Class'],
            ['级别', 'IM_LName'],
            ['规格', 'IM_Spec'],
            ['无扣长', 'IM_Length'],
            ['单位', 'IM_Measurement'],
            ['类别', 'BSS_Sort'],
            ['期初数量', 'LastCount'],
            ['期初金额', 'LastMoney'],
            ['本月收进数量', 'InCount'],
            ['本月收进金额', 'InMoney'],
            ['本月付出数量', 'OutCount'],
            ['本月付出金额', 'OutMoney'],
            ['期末数量', 'FinalCount'],
            ['期末金额', 'AvgMoney']
        ];
        return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
    }
}
