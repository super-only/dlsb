<?php

namespace app\admin\controller\chain\fastening;

use app\admin\model\jichu\ch\WareClass;
use app\common\controller\Backend;

/**
 * 库存
 *
 * @icon fa fa-circle-o
 */
class BoltStoreStock extends Backend
{
    
    /**
     * BoltStoreStock模型对象
     * @var \app\admin\model\chain\fastening\BoltStoreStock
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\fastening\BoltStoreStock;
        $wareclass = (new WareClass())->field("WC_Name")->where("WC_Sort","螺栓")->select();
        $ware_list = [];
        foreach($wareclass as $k=>$v){
            $ware_list[$v["WC_Name"]] = $v["WC_Name"];
        }
        $this->ware_list = $ware_list;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("m")
                ->join(["inventorymaterial"=>"im"],"m.IM_Num = im.IM_Num","left")
                ->join(["vendor"=>"v"],"v.V_Num = m.V_Num","left")
                ->where($where)
                ->where("m.BSS_Count>0 or m.BSS_Weight>0")
                ->order("m.V_Num,m.IM_Num,m.IM_LName,m.IM_Length")
                ->select();
            $count = $weight = $price = 0;
            $index_list = [];
            foreach($list as $v){
                $index_list[] = $v->toArray();
                $count += $v["BSS_Count"];
                $weight += $v["BSS_Weight"];
                $price += $v["BSS_NaxSumMoney"];
            }
            $index_list[] = ["IM_Class"=>"合计","BSS_Count"=>$count,"BSS_Weight"=>round($weight,2),"BSS_NaxSumMoney"=>round($price,2)];
            $result = array("total" => count($list), "rows" => $index_list);

            return json($result);
        }
        return $this->view->fetch();
    }

}
