<?php

namespace app\admin\controller\chain\fastening;

use app\admin\model\chain\fastening\BoltStoreOutSingle;
use app\admin\model\chain\fastening\MaterialGetNoticeJgj;
use app\admin\Model\jichu\ch\WareClass;
use app\admin\model\logisticsbolts\purchase\InvoiceDetailBolt;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 紧固件入库
 *
 * @icon fa fa-circle-o
 */
class BoltStoreInSingle extends Backend
{
    
    /**
     * BoltStoreInSingle模型对象
     * @var \app\admin\model\chain\fastening\BoltStoreInSingle
     */
    protected $model = null,$firstModel,$stateModel,$admin,$wareArr,$ware_list,$viewModel;
    protected $noNeedLogin = ["offerU"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\fastening\BoltStoreInSingle;
        $this->firstModel = new \app\admin\model\chain\fastening\BoltStoreIn;
        $this->stateModel = new \app\admin\model\chain\fastening\BoltStoreStock;
        $this->viewModel = new \app\admin\model\chain\fastening\BoltStoreInSingleView();
        $this->admin = \think\Session::get('admin');
        $defaultTime = date("Y-m-1 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        $wareclass = (new WareClass())->field("WC_Name,WC_Num")->where("WC_Sort","螺栓")->select();
        $ware_list = [];
        $wareArr = [];
        foreach($wareclass as $k=>$v){
            $ware_list[$v["WC_Name"]] = $v["WC_Name"];
            $wareArr[$v["WC_Name"]] = $v["WC_Num"];
        }
        $this->wareArr = $wareArr;
        $this->ware_list = $ware_list;
        $this->view->assign("ware_list",$ware_list);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->viewModel
                ->where($where)
                ->order($sort,$order)
                ->order("IM_LName,IM_Num,IM_Spec")
                ->paginate($limit);
           
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        // $this->assignconfig("rsclist",["到货入库"=>"到货入库","盘盈入库"=>"盘盈入库"]);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $content = $this->request->post("data");
            $content = $content?json_decode($content,true):[];
            $params = [];
            $paramsTable = [];
            foreach($content as $k=>$v){
                if(substr($v["name"],0,4) == "row["){
                    $key = rtrim(ltrim(($v["name"]),"row["),"]");
                    $params[$key] = $v["value"];
                }else{
                    $key = rtrim(ltrim(($v["name"]),"table_row["),"][]");
                    $paramsTable[$key][] = $v["value"];
                }
            }
            if(!$params["V_Num"]) return json(["code"=>0,"msg"=>"供应商必填"]);
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $In_Num = $params["In_Num"];
                if($In_Num){
                    $one = $this->firstModel->where("In_Num",$In_Num)->find();
                    if($one) return json(["code"=>0,"msg"=>"单号不能重复，请重新填写或者自动生成"]);
                }else{
                    $month = date("ym");
                    $one = $this->firstModel->field("In_Num")->where("In_Num","LIKE","JRK".$month.'-%')->order("In_Num DESC")->find();
                    if($one){
                        $num = substr($one["In_Num"],8);
                        $In_Num = 'JRK'.$month.'-'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $In_Num = 'JRK'.$month.'-001';
                }
                $params["In_Num"] = $In_Num;
                $msg = "";
                $sectSaveList = [];
                foreach($paramsTable["Bsid"] as $k=>$v){
                    if(!$paramsTable["InCount"][$k]) $msg .="第".($k+1)."行缺少数量<br>";
                    $sectSaveList[$k] = [
                        "In_Num" => $In_Num,
                        "Bsid" => $v,
                        "V_Num" => $params["V_Num"],
                        "IM_Num" => $paramsTable["IM_Num"][$k],
                        "IM_LName" => $paramsTable["IM_LName"][$k],
                        "IM_Length" => $paramsTable["IM_Length"][$k],
                        "InCount" => $paramsTable["InCount"][$k],
                        "InWeight" => $paramsTable["InWeight"][$k],
                        "InNaxPrice" => $paramsTable["InCount"][$k]?round($paramsTable["InNaxSumPrice"][$k]/$paramsTable["InCount"][$k],4):0,
                        "InNaxSumPrice" => $paramsTable["InNaxSumPrice"][$k],
                        "InSort" => $paramsTable["InSort"][$k],
                        "BsMemo" => $paramsTable["BsMemo"][$k]
                    ];
                    if(!$v) unset($sectSaveList[$k]["Bsid"]);
                }
                if($msg) return json(["code"=>0,"msg"=>$msg]);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        unset($params["V_Num"]);
                        $this->firstModel::create($params);
                        $result = $this->model->allowField(true)->saveAll(array_values($sectSaveList));
                        
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    } catch (PDOException $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    } catch (Exception $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    }
                }
                
                if ($result !== false) {
                    return json(["code"=>1,"msg"=>"成功！","data"=>$In_Num]);
                } else {
                    return json(["code"=>0,"msg"=>__('No rows were updated')]);
                }
            }
            return json(["code"=>0,"msg"=>__('Parameter %s can not be empty', '')]);
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->firstModel->where("In_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        if ($this->request->isPost()) {
            $content = $this->request->post("data");
            $content = $content?json_decode($content,true):[];
            $params = [];
            $paramsTable = [];
            foreach($content as $k=>$v){
                if(substr($v["name"],0,4) == "row["){
                    $key = rtrim(ltrim(($v["name"]),"row["),"]");
                    $params[$key] = $v["value"];
                }else{
                    $key = rtrim(ltrim(($v["name"]),"table_row["),"][]");
                    $paramsTable[$key][] = $v["value"];
                }
            }
            if(!$params["V_Num"]) return json(["code"=>0,"msg"=>"供应商必填"]);

            if($row["Auditor"]) return json(["code"=>0,"msg"=>"该入库单已审核，无法修改"]);

            $ck_list = $this->model->alias("rk")
                ->join(["boltstoreoutsingle"=>"ck"],"ck.Out_Bsid = rk.Bsid")
                ->where("rk.In_Num",$row["In_Num"])
                ->find();
            if($ck_list) return json(["code"=>0,"msg"=>"已存在出库，不能进行修改！"]);

            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $In_Num = $row["In_Num"];
                $msg = "";
                $sectSaveList = [];
                foreach($paramsTable["Bsid"] as $k=>$v){
                    // if(!$paramsTable["V_Num"][$k]) $msg .="第".($k+1)."行缺少供应商<br>";
                    if(!$paramsTable["InCount"][$k]) $msg .="第".($k+1)."行缺少数量<br>";
                    $sectSaveList[$k] = [
                        "In_Num" => $In_Num,
                        "Bsid" => $v,
                        "V_Num" => $params["V_Num"],
                        "IM_Num" => $paramsTable["IM_Num"][$k],
                        "IM_LName" => $paramsTable["IM_LName"][$k],
                        "IM_Length" => $paramsTable["IM_Length"][$k],
                        "InCount" => $paramsTable["InCount"][$k],
                        "InWeight" => $paramsTable["InWeight"][$k],
                        "InNaxPrice" => $paramsTable["InCount"][$k]?round($paramsTable["InNaxSumPrice"][$k]/$paramsTable["InCount"][$k],4):0,
                        "InNaxSumPrice" => $paramsTable["InNaxSumPrice"][$k],
                        "InSort" => $paramsTable["InSort"][$k],
                        "BsMemo" => $paramsTable["BsMemo"][$k]
                    ];
                    if(!$v) unset($sectSaveList[$k]["Bsid"]);
                }
                if($msg) return json(["code"=>0,"msg"=>$msg]);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        unset($params["V_Num"]);
                        $this->firstModel->where("In_Num",$In_Num)->update($params);
                        $result = $this->model->allowField(true)->saveAll(array_values($sectSaveList));
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    } catch (PDOException $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    } catch (Exception $e) {
                        Db::rollback();
                        return json(["code"=>0,"msg"=>$e->getMessage()]);
                    }
                }
                if ($result !== false) {
                    return json(["code"=>1,"msg"=>"保存成功！"]);
                } else {
                    return json(["code"=>0,"msg"=>__('No rows were updated')]);
                }
            }
            return json(["code"=>0,"msg"=>__('Parameter %s can not be empty', '')]);
        }

        $list = $this->model->alias("m")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = m.IM_Num","left")
            ->join(["vendor"=>"v"],"v.V_Num = m.V_Num","left")
            ->field("Bsid,m.V_Num,v.V_ShortName,m.IM_Num,im.IM_Class,m.IM_LName,im.IM_Spec,m.IM_Length,im.IM_Measurement,m.InCount,m.InWeight,m.InNaxPrice,m.InNaxSumPrice,m.InSort,m.BsMemo")
            ->where("In_Num",$row["In_Num"])
            ->order("Bsid,IM_Num,IM_LName,IM_Spec,IM_Length")
            ->select();
        $sum_weight = $count = $price = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["InWeight"] = round($v["InWeight"],2);
            $rows[$k]["InNaxPrice"] = round($v["InNaxPrice"],4);
            $rows[$k]["InNaxSumPrice"] = round($v["InNaxSumPrice"],2);
            $way = $this->_getWay($v["IM_Class"]);
            $rows[$k]["way"] = $way;
            $sum_weight += $v["InWeight"];
            $count += $v["InCount"];
            $price += $v["InNaxSumPrice"];
        }
        $row["sum_weight"] = $sum_weight;
        $row["count"] = $count;
        $row["price"] = $price;
        if($list){
            $row["V_Num"] = $rows[0]["V_Num"];
            $row["V_ShortName"] = $rows[0]["V_ShortName"];
        }
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("list",$rows);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->firstModel->where("In_Num",$num)->find();
        if($row["Auditor"]) return json(["code"=>0,"msg"=>"已审核"]);

        $list = $this->model->where("In_Num",$num)->select();
        $state_list = [];
        foreach($list as $k=>$v){
            $key = $v["IM_Num"].'-'.$v["IM_LName"].'-'.$v["IM_Length"].'-'.$row["InPlace"].'-'.$v["InSort"].'-'.$v["V_Num"];
            isset($state_list[$key])?"":$state_list[$key] = [
                "IM_Num" => $v["IM_Num"],
                "IM_LName" => $v["IM_LName"],
                "IM_Length" => $v["IM_Length"],
                "BSS_Count" => 0,
                "BSS_Weight" => 0,
                "BSS_Place" => $row["InPlace"],
                "BSS_Sort" => $v["InSort"],
                "V_Num" => $v["V_Num"],
                "BSS_NaxPrice" => 0,
                "BSS_NaxSumMoney" => 0
            ];
            $state_list[$key]["BSS_Count"] += $v["InCount"];
            $state_list[$key]["BSS_Weight"] += $v["InWeight"];
            // $state_list[$key]["BSS_NaxPrice"] += $v["InNaxPrice"];
            $state_list[$key]["BSS_NaxSumMoney"] += $v["InNaxSumPrice"];
        }
        foreach($state_list as $k=>$v){
            $search_where = [
                "IM_Num" => ["=",$v["IM_Num"]],
                "IM_LName" => ["=",$v["IM_LName"]],
                "IM_Length" => ["=",$v["IM_Length"]],
                "BSS_Place" => ["=",$v["BSS_Place"]],
                "BSS_Sort" => ["=",$v["BSS_Sort"]],
                "V_Num" => ["=",$v["V_Num"]]
            ];
            $one = $this->stateModel->field("BSS_ID,BSS_Count,BSS_Weight,BSS_NaxSumMoney")->where($search_where)->find();
            if($one){
                $state_list[$k]["BSS_ID"] = $one["BSS_ID"];
                $state_list[$k]["BSS_Count"] += $one["BSS_Count"];
                $state_list[$k]["BSS_Weight"] += $one["BSS_Weight"];
                $state_list[$k]["BSS_NaxSumMoney"] += $one["BSS_NaxSumMoney"];
                $state_list[$k]["BSS_NaxPrice"] = $state_list[$k]["BSS_Count"]?round($state_list[$k]["BSS_NaxSumMoney"]/$state_list[$k]["BSS_Count"],2):0;
            }
        }
        

        $update = [
            "Auditor" => $this->admin["nickname"],
            "AuditorDate" => date("Y-m-d H:i:s")
        ];
        $result = FALSE;
        if(!empty($state_list)){
            Db::startTrans();
            try {
                $this->firstModel->update($update,["In_Num"=>$num]);
                $result = $this->stateModel->allowField(true)->saveAll(array_values($state_list));
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"弃审失败，请稍后重试"]);
        $row = $this->firstModel->where("In_Num",$num)->find();
        if(!$row["Auditor"]) return json(["code"=>0,"msg"=>"已弃审"]);

        $list = $this->model->where("In_Num",$num)->select();
        $sidList = array_column($list,'Bsid');
        $findOne = (new InvoiceDetailBolt())->where("sid_id","in",$sidList)->find();
        if($findOne) return json(["code"=>0,"msg"=>"已存在开票，无法弃审，请稍后再试"]);
        $state_list = [];
        foreach($list as $k=>$v){
            $key = $v["IM_Num"].'-'.$v["IM_LName"].'-'.$v["IM_Length"].'-'.$row["InPlace"].'-'.$v["InSort"].'-'.$v["V_Num"];
            isset($state_list[$key])?"":$state_list[$key] = [
                "IM_Num" => $v["IM_Num"],
                "IM_LName" => $v["IM_LName"],
                "IM_Length" => $v["IM_Length"],
                "BSS_Count" => 0,
                "BSS_Weight" => 0,
                "BSS_Place" => $row["InPlace"],
                "BSS_Sort" => $v["InSort"],
                "V_Num" => $v["V_Num"],
                "BSS_NaxPrice" => 0,
                "BSS_NaxSumMoney" => 0
            ];
            $state_list[$key]["BSS_Count"] += $v["InCount"];
            $state_list[$key]["BSS_Weight"] += $v["InWeight"];
            // $state_list[$key]["BSS_NaxPrice"] += $v["InNaxPrice"];
            $state_list[$key]["BSS_NaxSumMoney"] += $v["InNaxSumPrice"];
        }
        $state_update_list = [];
        foreach($state_list as $k=>$v){
            $search_where = [
                "IM_Num" => ["=",$v["IM_Num"]],
                "IM_LName" => ["=",$v["IM_LName"]],
                "IM_Length" => ["=",$v["IM_Length"]],
                "BSS_Place" => ["=",$v["BSS_Place"]],
                "BSS_Sort" => ["=",$v["BSS_Sort"]],
                "V_Num" => ["=",$v["V_Num"]],
                // "BSS_NaxPrice" => ["=",$v["BSS_NaxPrice"]]
            ];
            $one = $this->stateModel->field("BSS_ID,BSS_Count,BSS_Weight,BSS_NaxSumMoney")->where($search_where)->find();
            if($one){
                $resetCount = $one["BSS_Count"]-$v["BSS_Count"];
                $resetWeight = $one["BSS_Weight"]-$v["BSS_Weight"];
                $resetPrice = $one["BSS_NaxSumMoney"]-$v["BSS_NaxSumMoney"];
                $state_update_list[] = [
                    "BSS_ID" => $one["BSS_ID"],
                    "BSS_Count" => $resetCount,
                    "BSS_Weight" => $resetWeight,
                    "BSS_NaxSumMoney" => $resetPrice,
                    "BSS_NaxPrice" => $resetCount?round($resetPrice/$resetCount,2):0
                ];
            }
        }
        $update = [
            "Auditor" => '',
            "AuditorDate" => "0000-00-00 00:00:00"
        ];
        $result = FALSE;
        if(!empty($state_update_list)){
            Db::startTrans();
            try {
                $this->firstModel->update($update,["In_Num"=>$num]);
                $result = $this->stateModel->allowField(true)->saveAll(array_values($state_update_list));
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result) {
            $this->success("弃审成功！");
        } else {
            $this->error("弃审失败！");
        }
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            
            $one = (new BoltStoreOutSingle())->where("Out_Bsid",$num)->find();
            if($one) return json(["code"=>0,"msg"=>"已经出库，无法进行删除"]);
            $in_detail = $this->model->alias("sid")->join(["boltstorein"=>"si"],"sid.In_Num = si.In_Num")->where("sid.Bsid",$num)->find();
            if(!$in_detail) return json(["code"=>0,"msg"=>"删除失败"]);
            if($in_detail["Auditor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);
            $count = $this->model->where("Bsid",$num)->delete();
            if($count) return json(["code"=>1,'msg'=>"删除成功"]);
            else return json(["code"=>0,"msg"=>"删除失败"]);
            
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function allDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);

        $row = $this->firstModel->where("In_Num",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"该条信息已不存在，或者请稍后重试"]);
        if($row["Auditor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);

        $result=0;
        Db::startTrans();
        try {
            $result += $this->firstModel->where("In_Num",$num)->delete();
            $result += $this->model->where("In_Num",$num)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result != false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }


    //编辑table
    public function getTableField()
    {
        $list = [
            ["ID","Bsid","readonly class='small_input'","","hidden","save"],
            // ["V_Num","V_Num","readonly class='small_input'","","hidden","save"],
            // ["供应商简称","V_ShortName","readonly class='small_input gysxz'","","","save"],
            ["存货编码","IM_Num","readonly class='small_input'","","","save"],
            ["存货名称","IM_Class","readonly class='small_input'","","",""],
            ["级别","IM_LName","class='small_input'","6.8","","save"],
            ["规格","IM_Spec","readonly class='small_input'","","",""],
            ["无扣长","IM_Length","class='small_input'","","","save"],
            ["单位","IM_Measurement","readonly class='small_input'",0,"",""],
            ["入库数量","InCount","class='small_input'",0,"","save"],
            ["入库重量","InWeight","class='small_input'",0,"","save"],
            ["不含税单价","InNaxPrice","class='small_input'",0,"","save"],
            ["不含税金额","InNaxSumPrice","class='small_input'",0,"","save"],
            ["计算金额方式","way","class='small_input'",0,"hidden","save"],
            ["类别","InSort","class='small_input'","外购","","save"],
            ["说明","BsMemo","class='small_input'","","","save"]
        ];
        return $list;
    }

    public function print($ids = null)
    {
        $row = $this->firstModel->where("In_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();

        $list = $this->model->alias("m")
            ->join(["inventorymaterial"=>"im"],"im.IM_Num = m.IM_Num","left")
            ->join(["vendor"=>"v"],"v.V_Num = m.V_Num","left")
            ->field("Bsid,m.V_Num,v.V_ShortName, v.V_Name,m.IM_Num,im.IM_Class,m.IM_LName,im.IM_Spec,m.IM_Length,im.IM_Measurement,m.InCount,m.InWeight,m.InNaxPrice,m.InNaxSumPrice,m.InSort,m.BsMemo")
            ->where("In_Num",$row["In_Num"])
            ->order("Bsid, IM_Num,IM_LName,IM_Spec,IM_Length")
            ->select();
        $sum_weight = $count = $price = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["InWeight"] = round($v["InWeight"],2);
            $rows[$k]["InNaxPrice"] = round($v["InNaxPrice"],4);
            $rows[$k]["InNaxSumPrice"] = round($v["InNaxSumPrice"],2);
            $sum_weight += $v["InWeight"];
            $count += $v["InCount"];
            $price += $v["InNaxSumPrice"];
        }
        $row["sum_weight"] = $sum_weight;
        $row["count"] = $count;
        $row["price"] = $price;
        $row["InDate"] = date("Y-m-d", strtotime($row["InDate"]));
        $row["V_Name"] = count($list)>0?$list[0]['V_Name']:"";
        
        $this->assignconfig("row",$row);
        $this->assignconfig("list",$rows);
        
        return $this->view->fetch();
    }

    public function offerU()
    {
        $id = $this->request->post("id");
        $row = $this->firstModel->where("In_Num",$id)->find();
        $res = [
            "code"=>0,
            "msg" => "有误",
            "data" => []
        ];
        if(!$row) return json($res);
        else if($row["Auditor"]==''){
            $res["msg"] = "请先审核";
            return json($res);
        }
        $cvencode = $this->model->alias("ct")
            ->join(["vendor"=>"v"],"v.V_Num=ct.V_Num")
            ->where("ct.In_Num",$id)
            ->where("ct.V_Num","<>",'')
            // ->select(false);
            ->value("v.V_Name");
        if(!$cvencode){
            $res["msg"] = "不存在供应商编号";
            return json($res);
        }
        $cwhcode = $row["InPlace"];
        if(!$cwhcode){
            $res["msg"] = "不存在仓库编号";
            return json($res);
        }
        $main = [
            "ccode" => $id,
            //采购类型编码
            "cptcode" => "1",
            //收发类别
            "crdcode" => '101',
            "cvenname" => $cvencode,
            "cwhname" => $cwhcode,
            "ddate" => date("Y-m-d",strtotime($row["InDate"])),
            "cmaker" => $row['Writer'],
            "cmemo" => $row['Writer'],
            "chandler" => $row['Auditor'],
            "dveridate" => date("Y-m-d",strtotime($row["AuditorDate"])),
        ];
        $list = $this->model
            ->where("In_Num",$id)
            ->select();
        $details = [];
        foreach($list as $k=>$v){
            $details[] = [
                "cinvcode" => $v["IM_Num"],
                "iquantity" => $v["InWeight"],
                "iunitcost" => $v["InNaxPrice"],
                "cfree1" => '',
                "cfree2" => $v["IM_LName"],
                "cfree3" => '',
                "inum" => $v["InCount"],
            ];
        }
        $data = [
            "main" => $main,
            "details" => $details
        ];
        $url = "/PostRdrecord01/Add";
        $result = api_post_u($url,$data);
        if($result["code"]==0){
            $this->firstModel->where("In_Num",$id)->update(["offer"=>1]);
            $res["code"] = 1;
            $res["msg"] = "成功";
        }else $res["msg"] = $result["ErrorMsg"];
        return json($res);
    }

    public function selectArrival()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $subsql = (new MaterialGetNoticeJgj())->alias("mgn")
                ->join(["boltstoreinsingle"=>"sid"],"mgn.MGN_ID=sid.MGN_ID2")
                ->field("mgn.MGN_ID,sum(sid.InCount) as sy_count,sum(sid.InWeight) as sy_weight")
                ->group("mgn.MGN_ID")
                ->buildSql();
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new MaterialGetNoticeJgj())->alias("mgn")
                ->join(["materialnote_jgj"=>"mn"],"mgn.MN_Num = mn.MN_Num")
                ->join([$subsql=>"ss"],"mgn.MGN_ID = ss.MGN_ID","left")
                ->join(["inventorymaterial"=>"im"],"mgn.IM_Num = im.IM_Num","left")
                ->join(["vendor"=>"v"],"v.V_Num = mn.V_Num","left")
                ->field("0 as MGN_ID,mgn.MGN_ID AS MGN_ID2,v.V_Num,v.V_Name,mgn.IM_Num,im.IM_Class,mgn.L_Name,im.IM_Spec,mgn.MGN_Length,mgn.MGN_Length as IM_Length,mgn.MGN_Count,(CASE WHEN IFNULL(ss.sy_count,0)=0 THEN mgn.MGN_Count ELSE (mgn.MGN_Count-ss.sy_count) END) as InCount,mgn.MGN_Weight,(CASE WHEN IFNULL(ss.sy_weight,0)=0 THEN mgn.MGN_Weight ELSE (mgn.MGN_Weight-ss.sy_weight) END) as InWeight,round(mgn.MGN_Weight/mgn.MGN_Count,2) as dz,mn.MN_Date,mn.MN_Num,ss.*,im.IM_Measurement,mgn.mgn_price as InNaxPrice,0 as InNaxSumPrice")
                ->where($where)
                // ->where("mn.WriteTime",">=",date("Y-m-d", strtotime(' -2 month')))
                ->order($sort, $order)
                ->having("InCount>0")
                ->select();
                // ->paginate($limit);
            foreach($list as &$v){
                $way = $this->_getWay($v["IM_Class"]);
                $v["way"] = $way;
                $v["MGN_Length"] = round($v["MGN_Length"],3);
                $v["MGN_Count"] = round($v["MGN_Count"],2);
                $v["MGN_Weight"] = round($v["MGN_Weight"],2);
                // $v["sy_count"] = round($v["sy_count"],2);
                // $v["sy_weight"] = round($v["sy_weight"],2);
                // $v["CTD_UnqualityCount"] = round($v["CTD_UnqualityCount"],2);
                $v["IM_Length"] = round($v["IM_Length"],3);
                $v["InCount"] = round($v["InCount"],2);
                $v["InWeight"] = round($v["InWeight"],2);
                $v["InNaxPrice"] = round($v["InNaxPrice"],5);
                $count = $way?$v["InWeight"]:$v["InCount"];
                $v["InNaxSumPrice"] = round($v["InNaxPrice"]*$count,5);
            }
            $result = array("total" => count($list), "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    protected function _getWay($imClass='')
    {
        $way = 0;
        foreach(["弹垫","扣紧螺母","薄螺母","U"] as $fvv){
            if(strpos($imClass,$fvv)!==FALSE){
                $way = 1;
                break;
            }
        }
        return $way;
    }
}
