<?php

namespace app\admin\controller\chain\sequence;

use app\admin\model\chain\lofting\ProduceTaskDetail;
use app\admin\model\chain\lofting\UnionProduceTaskView;
use app\admin\model\eipapi\api\ProductWorkView;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
/**
 * 排产
 *
 * @icon fa fa-circle-o
 */
class Sequence extends Backend
{
    
    /**
     * Sequence模型对象
     * @var \app\admin\model\chain\sequence\Sequence
     */
    protected $model = null,$detailModel= null,$partModel=null,$orderList = null,$machineList = null,$admin = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\sequence\Sequence;
        $this->detailModel = new \app\admin\model\chain\sequence\SequenceDetail;
        $this->partModel = new \app\admin\model\chain\sequence\SequencePart;
        $this->orderList = $this->_orderList();
        $this->machineList = $this->_machineList();
        $this->admin = \think\Session::get('admin');
        $assignList = [
            "orderList" => $this->orderList,
            "machineList" => $this->machineList,
        ];
        $this->view->assign($assignList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("s")
                ->join(["union_producetask_view"=>"pt"],"s.PT_Num=pt.PT_Num")
                ->join(["taskdetail"=>"td"],"td.TD_ID=pt.TD_ID")
                ->JOIN(["task"=>"t"],"t.T_Num=td.T_Num")
                ->field("s.*,td.TD_Pressure,td.TD_TypeName,t.t_project")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            // $list = $this->model
            //     ->where($where)
            //     ->order($sort, $order)
            //     ->paginate($limit);
            $data = [];
            foreach($list as $k=>$v){
                $data[$k] = $v->toArray();
                $data[$k]["choose_order"] = trim($v["choose_order"],",");
                foreach(explode(",",$v["choose_order"]) as $vv){
                    $data[$k][$vv] = $vv;
                }
            }
            $result = array("total" => $list->total(), "rows" => $data);

            return json($result);
        }
        return $this->view->fetch();
    }
    
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $exist = $this->model->where("PT_Num",$params["PT_Num"])->find();
                if($exist) $this->error('已存在该任务下达单的排产');
                $ex = (new UnionProduceTaskView())->where("PT_Num",$params["PT_Num"])->value("sect_field");
                $type = (new ProduceTaskDetail([],$ex))->getType($params["PT_Num"],$ex);
                $orderList = $this->orderList;
                //0既不焊接也不制弯 1焊接不制弯 2制弯不焊接 3all
                if($type==2) unset($orderList["TTGX05"],$orderList["TTGX04"]);
                else if($type==1) unset($orderList["TTGX07"]);
                else if(!$type) unset($orderList["TTGX05"],$orderList["TTGX04"],$orderList["TTGX07"]);
                $sNum = "PC".date("Ymd");
                $getOne = $this->model->where("sNum","LIKE",'PC'.date('Ymd').'%')->order("sNum desc")->value("right(sNum,3)");
                if($getOne) $num = str_pad(($getOne+1),3,0,STR_PAD_LEFT);
                else $num = "001";
                $sNum .= $num;
                $params["writer"] = $this->admin["username"];
                $params["sNum"] = $sNum;
                $detailList = [];
                $diff = strtotime($params["PlanFinishTime"])-strtotime($params["PlanStartTime"]);
                $day = intval($diff/86400/count($orderList));
                $js = 0;
                foreach($orderList as $k=>$v){
                    $detailList[] = [
                        "writer" => $this->admin["username"],
                        "sNum" => $sNum,
                        "orderId" => $k,
                        "PlanStartTime" => date("Y-m-d H:i:s",strtotime("+".($js*$day)." day",strtotime($params["PlanStartTime"]))),
                        "PlanFinishTime" => date("Y-m-d H:i:s",strtotime("+".(($js+1)*$day)." day",strtotime($params["PlanStartTime"])))
                    ];
                    $js++;
                }
                $params["choose_order"] = (",".implode(",",array_keys($orderList)).",");
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    if(!empty($detailList)) $this->detailModel->allowField(true)->saveAll($detailList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $baseOrderList = explode(",",trim($row["choose_order"],','));
                $delList = array_diff($baseOrderList,$params["choose_order"]);
                $existList = $this->detailModel->alias("d")
                    ->join(["pc_sequence_part"=>"sp"],"d.sdNum=sp.sdNum")
                    ->where([
                        "sNum" => ["=",$ids],
                        "orderId" => ["IN",$delList]
                    ])->column("orderId");
                if(!empty($existList)) $this->error(implode(',',$existList)."等工序已有排产内容，无法删除！");
                $addList = array_diff($params["choose_order"],$baseOrderList);
                $detailList = [];
                foreach($addList as $v){
                    $detailList[] = [
                        "writer" => $this->admin["username"],
                        "sNum" => $ids,
                        "orderId" => $v,
                        "PlanStartTime" => $params["PlanStartTime"],
                        "PlanFinishTime" => $params["PlanStartTime"]
                    ];
                }
                $params["choose_order"] = $params["choose_order"]?(",".implode(",",$params["choose_order"]).","):"";
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    if(!empty($detailList)) $this->detailModel->allowField(true)->saveAll($detailList);
                    if(!empty($delList)) $this->detailModel->where([
                        "sNum" => ["=",$ids],
                        "orderId" => ["IN",$delList]
                    ])->delete();
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function detail($operate=null,$ids=null)
    {
        if(!$operate or !$ids) $this->error(__('No Results were found'));
        $row = $this->model->alias("m")
            ->join(["pc_sequence_detail"=>"sd"],"m.sNum=sd.sNum")
            ->field("m.PT_Num,sd.*")
            ->where([
                "m.sNum" => ["=",$ids],
                "sd.orderId" => ["=",$operate]
            ])->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                foreach($params as $k=>$v){
                    if(!$v) unset($params[$k]);
                }
                $params["sdNum"] = $row["sdNum"];
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->detailModel->save($params,["sdNum"=>$row["sdNum"]]);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row",$row);
        $this->assignconfig("ids",$row["sdNum"]);
        $this->assignconfig("PT_Num",$row["PT_Num"]);
        $this->assignconfig("machineList",build_select("machine",[""=>"请选择"]+$this->machineList,['class'=>'form-control selectpicker',"data-rule"=>"required"]));
        return $this->view->fetch();
    }

    public function detailMachine()
    {
        [$sdNum,$PT_Num] = array_values($this->request->post());
        $ex = (new UnionProduceTaskView())->where("PT_Num",$PT_Num)->value("sect_field");
        $produceTaskDetailModel = new ProduceTaskDetail([],$ex);
        $list = $produceTaskDetailModel->alias("ptd")
            ->join([$ex."dtmaterialdetial"=>'dd'],"ptd.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->join(["pc_sequence_part"=>"sp"],["ptd.PTD_Material = sp.sMaterial","ptd.PTD_Specification = sp.sSpecification","sp.sdNum = ".$sdNum],'left')
            ->field("dd.DtMD_sStuff as sStuff,ptd.PTD_Material as sMaterial,ptd.PTD_Specification as sSpecification,ifnull(sp.machine,'') as machine,case dd.DtMD_sStuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,
            cast((case 
            when dd.DtMD_sStuff='角钢' then 
            SUBSTR(REPLACE(ptd.PTD_Specification,'∠',''),1,locate('*',REPLACE(ptd.PTD_Specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(ptd.PTD_Specification,'∠',''),locate('*',REPLACE(ptd.PTD_Specification,'∠',''))+1,2)*1
            when (dd.DtMD_sStuff='钢板' or dd.DtMD_sStuff='钢管') then 
            REPLACE(ptd.PTD_Specification,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where([
                "ptd.PT_Num" => ["=",$PT_Num]
            ])->order("clsort asc,bh asc,sSpecification asc")->group("dd.DtMD_sStuff,ptd.PTD_Material,ptd.PTD_Specification")->select();
        if($list) $list = collection($list)->toArray();
        else $list = [];
        return json(["data"=>$list,"code"=>1,"msg"=>"成功"]);
    }

    public function saveMachine()
    {
        [$sdNum,$machine,$data] = array_values($this->request->post());
        if(!$sdNum or !$data) return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
        $data = json_decode($data,true);
        $saveData = $delData = [];
        $result = false;
        Db::startTrans();
        try {
            foreach($data as $v){
                $this->partModel->where([
                    "sdNum" => ["=",$sdNum],
                    "sStuff" => ["=",$v["sStuff"]],
                    "sMaterial" => ["=",$v["sMaterial"]],
                    "sSpecification" => ["=",$v["sSpecification"]]
                ])->delete();
                $saveData[] = [
                    "sdNum" => $sdNum,
                    "sStuff" => $v["sStuff"],
                    "sMaterial" => $v["sMaterial"],
                    "sSpecification" => $v["sSpecification"],
                    "machine" => $machine
                ];
            }
            $result = $this->partModel->allowField(true)->saveAll($saveData);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["data"=>[],"code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["data"=>[],"code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["data"=>[],"code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            return json(["data"=>[],"code"=>1,"msg"=>"成功"]);
        } else {
            return json(["data"=>[],"code"=>0,"msg"=>"失败，稍后重试"]);
        }
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();

            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                }
                $detailNum = $this->detailModel->where("sNum", "in", $ids)->column("sdNum");
                $this->detailModel->where("sNum","in",$ids)->delete();
                $this->partModel->where("sdNum","in",$detailNum)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function updateSequence()
    {
        $pt_list = (new ProductWorkView())->order("PT_Num asc")->select();
        $mainList = [];
        $detailList = [];
        $num = 1;
        $orderList = $this->orderList;
        $order = ",".implode(",",array_keys($orderList)).",";
        foreach($pt_list as $k=>$v){
            $mainList[$k] = [
                "sNum" => "PC".date("Ymd").str_pad($num,3,0,STR_PAD_LEFT),
                "PT_Num" => $v["PT_Num"],
                "choose_order" => $order,
                "PlanStartTime" => $v["actualStartDate"],
                "PlanFinishTime" => date("Y-m-d H:i:s",strtotime("+2 month",strtotime($v["actualStartDate"]))),
                "writer" => "车间",
                "ActStartTime" => $v["actualStartDate"],
                "ActFinishTime" => $v["actualFinishDate"]
            ];
            foreach($orderList as $kk=>$vv){
                $detailList[] = [
                    "sNum" => $mainList[$k]["sNum"],
                    "orderId" => $kk,
                    "PlanStartTime" => $v["actualStartDate"],
                    "PlanFinishTime" => date("Y-m-d H:i:s",strtotime("+2 month",strtotime($v["actualStartDate"]))),
                    "writer" => "车间",
                    "ActStartTime" => $v["actualStartDate"],
                    "ActFinishTime" => $v["actualFinishDate"]
                ];
            }
            $num++;
        }
        $result = $this->model->allowField(true)->saveAll($mainList,false);
        $other_result = $this->detailModel->allowField(true)->saveAll($detailList);
        pri(count($result),1);
    }
}
