<?php

namespace app\admin\controller\chain\pack;

use app\admin\model\chain\lofting\BoltFastCreateMain;
use app\admin\model\chain\lofting\DhCooperate;
use app\admin\model\chain\lofting\DhCooperateDetail;
use app\admin\model\chain\lofting\TaskPart;
use app\admin\model\chain\lofting\TaskSect;
use app\admin\model\chain\sale\Sectconfigdetail;
use app\admin\model\chain\sale\TaskDetail;
use app\admin\model\chain\sale\TaskHeight;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 有问题 有时间应该改一下 这个index的主键应该为TD_ID方便很多
 */

/**
 * 专用段包装
 *
 * @icon fa fa-circle-o
 */
class Pack extends Backend
{
    
    /**
     * Pack模型对象
     * @var \app\admin\model\chain\pack\Pack
     */
    protected $model = null;
    protected $noNeedLogin = ["chooseMultiScd","multiPack"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\pack\Pack;
        $this->detailModel = new \app\admin\model\chain\pack\PackDetail;
        $this->privateModel = new \app\admin\model\chain\pack\PrivatePack;
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("p")
                ->join(["taskdetail"=>"td"],"p.TD_ID = td.TD_ID")
                ->join(["task"=>"t"],'t.T_Num = td.T_Num')
                ->join(["newoldtdtypename"=>"nottn"],"nottn.TD_ID = P.TD_ID","left")
                ->field("p.P_ID,p.SCD_ID,p.TD_ID,td.P_Name,td.TD_TypeName,td.T_Num,td.T_Num as 'td.T_Num',td.TD_Pressure,t.T_Company,t.t_project,t.t_shortproject,nottn.old_TypeName,p.WriteDate,p.Writer,p.Auditor,p.AuditorDate")
                ->where($where)
                ->where("p.P_Flag",1)
                ->order($sort, $order)
                ->group("td.TD_TypeName,td.T_Num,td.TD_Pressure,t.T_Company,t.t_project,t.t_shortproject")
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function towerListTable()
    {
        $params = $this->request->post();
        $tdId = $params["tdId"];
        $supplement = json_decode($params["supplement"],true);
        if(!$tdId) return json(["code"=>0,"msg"=>"失败"]);
        $where = [
            "m.TD_ID" => ["=",$tdId],
            "m.P_Flag" => ["=",1]
        ];
        $list = $this->model->alias("m")
            ->join(["sectconfigdetail"=>"scd"],"m.SCD_ID = scd.SCD_ID")
            ->field("m.*,scd.SCD_TPNum,scd.TH_Height,scd.SCD_Count,(case when m.Auditor='' then '未审核' else '已审核' end) as status")
            ->where($where)
            ->order("m.SCD_ID ASC")
            ->group("m.SCD_ID,m.TD_ID")
            ->select();
        $data = [];
        foreach($list as $k=>$v){
            $data[$k] = $v->toArray();
            foreach($supplement as $kk=>$vv){
                $data[$k][$kk] = $vv;
            }
        }
        return json(["code"=>1,"data"=>$data]);
    }

    public function towerPackageDetailsTable()
    {
        $params = $this->request->post();
        $pId = $params["pId"];
        $supplement = json_decode($params["supplement"],true);
        if(!$pId) return json(["code"=>0,"msg"=>"失败"]);
        $one = $this->model->where("P_ID",$pId)->find();
        $where = [
            "m.SCD_ID" => ["=",$one["SCD_ID"]],
            "m.P_Flag" => ["=",1]
        ];
        $list = $this->model->alias("m")
            ->join(["packdetail"=>"pd"],"m.P_ID = pd.P_ID")
            ->field("m.*,pd.PD_Name,pd.PD_Type,pd.PD_Content,pd.PD_ID,cast(REVERSE(LEFT(REVERSE(PD_Name),LOCATE('-',REVERSE(PD_Name))-1)) as unsigned) as number")
            ->where($where)
            ->order("number,pd.PD_Name ASC")
            ->select();
        $data = [];
        foreach($list as $k=>$v){
            $data[$k] = $v->toArray();
            $data[$k]["WriteDate"] = date("Y-m-d",strtotime($v["WriteDate"]));
            $data[$k]["AuditorDate"] = date("Y-m-d",strtotime($v["AuditorDate"]));
            foreach($supplement as $kk=>$vv){
                $data[$k][$kk] = $vv;
            }
        }
        return json(["code"=>1,"data"=>$data]);
    }

    public function packageDetailsTable()
    {
        $params = $this->request->post();
        $pdId = $params["pdId"];
        $scdId = $params["scdId"];
        if(!$pdId or !$scdId) return json(["code"=>0,"msg"=>"失败"]);
        $where = ["pp.PD_ID"=>["=",$pdId],"pp.SCD_ID"=>["=",$scdId]];
        $list = $this->privateModel->getPackList($where);
        return json(["code"=>1,"data"=>$list]);
    }

    public function chooseTower()
    {
         //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            // $list = (new ProduceTask())->alias("pt")
                // ->join(["taskdetail"=>"td"],"pt.TD_ID=td.TD_ID")
            $list = (new TaskDetail())->alias("td")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num")
                ->join(["compact"=>"c"],"t.C_Num=c.C_Num")
                ->join(["newoldtdtypename"=>"o"],"td.TD_ID = o.TD_ID","LEFT")
                ->field("td.TD_ID,c.PC_Num,t.T_Num,t.T_Num as 't.T_Num',t.C_Num,t.C_Num as 't.C_Num',td.P_Name,t.t_project as C_ProjectName,td.P_Num,td.TD_TypeName,td.TD_TypeName as 'td.TD_TypeName',td.TD_Pressure,t.T_Company,t.t_shortproject,c.Customer_Name,c.C_SaleMan,o.old_TypeName")
                ->where($where)
                ->order($sort,$order)
                ->group("td.TD_ID")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

     public function chooseScd($td_id=null)
     {

        $data = $bolt_where = [];
        $where = ["th.TD_ID"=>["=",$td_id]];
        $sect_list = (new Sectconfigdetail())->alias("s")
            ->join(["taskheight"=>"th"],"th.TH_ID=s.TH_ID")
            ->join(["taskdetail"=>"td"],"td.TD_ID=th.TD_ID")
            ->join(["task"=>"t"],"td.T_Num = t.T_Num")
            ->join(["pack"=>"p"],"p.SCD_ID = s.SCD_ID","LEFT")
            ->field("ifnull(p.P_ID,0) as P_ID,s.SCD_ID,t.t_project,td.TD_TypeName,s.SCD_TPNum,'' as PD_Sect,'未打包' as state,t.T_Num,s.SCD_Count,s.TH_Height,s.TD_Pressure")
            ->where($where)->group("s.SCD_ID")->select();
        foreach($sect_list as $v){
            $data[$v["SCD_ID"]] = $v->toArray();
            $bolt_where[] = $v["SCD_ID"];
        }
        $boltfast_list = (new BoltFastCreateMain())->field("AllPTSect,AllFDSect,BanSect,SCD_ID")->where("SCD_ID","IN",$bolt_where)->select();
        foreach($boltfast_list as $v){
            $data[$v["SCD_ID"]]["PD_Sect"] = trim(trim(($v["AllPTSect"].','.$v["AllFDSect"]),",").','.$v["BanSect"],",");
        }
        $pack_list = $this->model->where("SCD_ID","IN",$bolt_where)->select();
        foreach($pack_list as $v){
            isset($data[$v["SCD_ID"]])?$data[$v["SCD_ID"]]["state"]="已打包":"";
        }
        $this->assignconfig('content',array_values($data));
        return $this->view->fetch();
     }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                if(!$params["SCD_ID"] or !$params["TD_ID"]) $this->error(__('Parameter %s can not be empty', ''));
                $exist_one = $this->model->field("P_ID")->where([
                    "SCD_ID" => ["=",$params["SCD_ID"]],
                    "TD_ID" => ["=",$params["TD_ID"]]
                ])->find();
                if($exist_one) $this->success('success',null,$exist_one["P_ID"]);
                else{
                    $params = array_merge($params,["P_Flag"=>1,"Writer"=>$this->admin["nickname"],"WriteDate"=>date("Y-m-d H:i:s")]);
                    $result = false;
                    $result = $this->model::create($params);
                    if ($result) {
                        $this->success('success',null,$result["P_ID"]);
                    } else {
                        $this->error(__('No rows were inserted'));
                    }
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        // $row = $this->model->get($ids);
        
        $row = $this->model->alias("p")
            ->join(["sectconfigdetail"=>"scd"],"p.SCD_ID = scd.SCD_ID")
            ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
            ->join(["taskdetail"=>"td"],"td.TD_ID = p.TD_ID")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num")
            ->join(["newoldtdtypename"=>"n"],"p.TD_ID=n.TD_ID","left")
            ->field("th.TH_ID,p.P_ID,t.T_Num,t.t_project as C_ProjectName,n.old_TypeName,p.TD_ID,td.TD_TypeName,td.TD_Pressure,p.P_Count,th.TH_Height,p.SCD_ID,scd.SCD_TPNum as SCD,p.P_Memo,p.Auditor")
            ->where("P_ID",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        // $adminIds = $this->getDataLimitAdminIds();
        // if (is_array($adminIds)) {
        //     if (!in_array($row[$this->dataLimitField], $adminIds)) {
        //         $this->error(__('You have no permission'));
        //     }
        // }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("P_ID",$ids)->update($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $noPackList = $this->packList($ids);
        
        $scd_id = $row["SCD_ID"];
        $th_id = $row["TH_ID"];
        $td_id = $row["TD_ID"];
        // $msg = "";
        list($leftList,$msg) = $this->noPackDt($th_id,$scd_id,$ids);

        $this->view->assign("row", $row);
        $this->view->assign("noPackList", $noPackList);
        $this->view->assign("msg", $msg);
        $this->assignconfig('leftList',$leftList);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();

            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                }
                $delDetailList = $this->detailModel->where("P_ID","IN",$ids)->select();
                $detailArr = [];
                foreach($delDetailList as $v){
                    $detailArr[$v["PD_ID"]] = $v["PD_ID"];
                    $v->delete();
                }
                $this->privateModel->where("PD_ID","IN",$detailArr)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function updatePackName()
    {
        $p_id = $this->request->post("p_id/d");
        $change = $this->request->post("change/d");
        if(!$p_id) return json(["code"=>0,"msg"=>"有误，请刷新后重试"]);
        if(!$change) return json(["code"=>0,"msg"=>"有误，请重新填写"]);
        $one = $this->model->alias("m")
            ->join(["packdetail"=>"pd"],"m.P_ID = pd.P_ID")
            ->field("left(PD_Name,char_length(substr(PD_Name,locate('-',reverse(PD_Name),1)))) as name,PD_Name")
            ->where("m.P_ID",$p_id)
            ->group("name")
            ->select();
        foreach($one as $v){
            $pd_name = $v["PD_Name"];
            $change_before = substr($pd_name,0,strripos($pd_name,"-"));
            $change_after = substr($pd_name,0,stripos($pd_name,"+")+1).$change;
            $result = DB::query("update packdetail set PD_Name = REPLACE(PD_Name,'".$change_before."','".$change_after."') where `P_ID` = '".$p_id."'");
        }
        return json(["code"=>1,"msg"=>"成功"]);
        
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->model->where("P_ID",$num)->find();
        if($row["Auditor"]) return json(["code"=>0,"msg"=>"已审核"]);
        $update = [
            "Auditor" => $this->admin["nickname"],
            "AuditorDate" => date("Y-m-d H:i:s")
        ];
        $result = $this->model->update($update,["P_ID"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function mulAuditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        $tdList = $this->model->where("P_ID","IN",$ids)->group("TD_ID")->column("TD_ID");
        $result = $this->model->where("TD_ID","IN",$tdList)->update([
            "Auditor" => $this->admin["nickname"],
            "AuditorDate" => date("Y-m-d H:i:s")
        ]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"弃审失败，请稍后重试"]);
        $row = $this->model->where("P_ID",$num)->find();
        if(!$row["Auditor"]) return json(["code"=>0,"msg"=>"已弃审"]);

        $update = [
            "Auditor" => '',
            "AuditorDate" => "0000-00-00 00:00:00"
        ];
        $result = $this->model->update($update,["P_ID"=>$num]);
        if ($result) {
            $this->success("弃审成功！");
        } else {
            $this->error("弃审失败！");
        }
    }

    public function mulGiveup($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        $tdList = $this->model->where("P_ID","IN",$ids)->group("TD_ID")->column("TD_ID");
        $result = $this->model->where("TD_ID","IN",$tdList)->update([
            "Auditor" => "",
            "AuditorDate" => "0000-00-00 00:00:00"
        ]);
        if ($result) {
            return json(["code"=>1,"msg"=>"弃审成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败！"]);
        }
    }

    public function leftTable()
    {
        $num = $this->request->post("num");
        if(!$num){
            return json(["code"=>0,"msg"=>"有误"]);
        }
        $one = $this->model->alias("p")
            ->join(["sectconfigdetail"=>"scd"],"p.SCD_ID = scd.SCD_ID")
            ->field("p.TD_ID,scd.TH_ID,p.SCD_ID,p.P_ID")
            ->where("p.P_ID",$num)
            ->find();
        if(!$one){
            return json(["code"=>0,"msg"=>"有误"]);
        }
        list($leftList,$msg) = $this->noPackDt($one["TH_ID"],$one["SCD_ID"],$one["P_ID"]);
        // die;
        return json(["code"=>1,"data"=>$leftList,"msg"=>$msg]);
    }

    public function packSet($p_id=null,$pd_id = null)
    {
        if(!$p_id) $this->error(__('No Results were found'));
        $row = $this->model->where("P_ID",$p_id)->find();
        if($row["Auditor"]) return json(["code"=>0,"msg"=>"已审核"]);
        $scd_one = $this->model->alias("m")
            ->join(["sectconfigdetail"=>"scd"],"m.SCD_ID = scd.SCD_ID")
            ->field("scd.SCD_TPNum")
            ->where("P_ID",$p_id)
            ->find();
        $row = [
            "P_ID" => $p_id,
            "PD_ID" => 0,
            "PD_Name" => @$scd_one["SCD_TPNum"]."+",
            "packname" => "[全部]",
            "PD_Content" =>""
        ];
        $packList = $this->detailModel->field("PD_Name")->where("P_ID",$p_id)->order("PD_ID DESC")->find();
        if($packList) $row["packname"] = $packList["PD_Name"];
        if($pd_id){
            $packone = $this->detailModel->field("PD_ID,PD_Name,PD_Content")->where("PD_ID",$pd_id)->find();
            if($packone){
                $row["PD_ID"] = $packone["PD_ID"];
                $row["PD_Name"] = $packone["PD_Name"];
                $row["PD_Content"] = $packone["PD_Content"];
            }
        }
        $this->view->assign("row",$row);
        return $this->view->fetch();
    }

    public function packSetSave()
    {
        
        if($this->request->isPost()){
            list($row,$data) = array_values($this->request->post());
            $row = json_decode($row,true);
            $data = json_decode($data,true);
            $saveRow = $savePack = $sectName = $dcd_content = $dt_id = [];
            //单基数量的总重 单基数量的总和 最大长度 单基件数 电焊单基数量
            $PD_sumWeight = $PD_SumCount = $MaxLength = $Count_OnlyM = [];
            foreach($row as $v){
                $saveRow[$v["name"]] = $v["value"];
            }
            $tdList = $this->model->alias("p")->join(["taskdetail"=>"td"],"p.TD_ID=td.TD_ID")
                ->JOIN(["task"=>"t"],"t.T_Num=td.T_Num")->where("p.P_ID",$saveRow["P_ID"])->value("T_Sort");
            $ex = $this->_technologyEx()[$tdList];
            // $scd_id = $this->model->field("SCD_ID")->WHERE("P_ID",$row["P_ID"])->FIND();
            // $saveRow["PD_ID"] = $scd_id["SCD_ID"];
            // $sect_num_arr = [];
            // $sect_num_list = (new TaskSect())->field("TS_Name,TS_Count")->where("SCD_ID",$saveRow["PD_ID"])->select();
            // foreach($sect_num_list as $k=>$v){
            //     $sect_num_arr[$v["TS_Name"]] = $v["TS_Count"];
            // }
            $get_one = $this->detailModel->where([
                "P_ID" => ["=",$saveRow["P_ID"]],
                "PD_Name" => ["=",$saveRow["PD_Name"]],
                "PD_ID" => ["<>",$saveRow["PD_ID"]]
            ])->find();
            if($get_one) return json(["code"=>0,"msg"=>"打包失败，包名重复"]);
            $flag = $saveRow["PD_ID"]==0?1:0;
            $saveResult = $saveRow["PD_ID"]==0?$this->detailModel->insertGetId($saveRow):$saveRow["PD_ID"];
            if(!$saveResult) return json(["code"=>0,"msg"=>"打包失败，请稍后重试！"]);
            foreach($data as $k=>$v){
                $saveRow["SCD_ID"] = $v["SCD_ID"];
                $sectName[$v["TP_SectName"]] = $v["TP_SectName"];
                $dt_id[$v["true_DtMD_ID_PK"]] = $v["true_DtMD_ID_PK"];
                $content = [
                    "DtMD_ID_PK" => $v["DtMD_ID_PK"],
                    "PD_ID" => $saveResult,
                    "SCD_ID" => $v["SCD_ID"],
                    "DCD_ID" => $v["DCD_ID"],
                    "PL_Count" => $v["TP_PackCount"],
                    "PD_IsM" => $v["PD_IsM"],
                    "ifMaxLen" => 0,
                    "ifChaiFen" => 0,
                ];
                (!isset($v["PL_ID"]) or $v["PL_ID"]==0)?"":$content["PL_ID"]=$v["PL_ID"];
                $savePack[] = $content;
                if($v["DCD_ID"]) $dcd_content[$v["DCD_ID"]] = $content;
                $PD_sumWeight[] = $v["DtMD_fWeight"];
                $PD_SumCount[] = $v["TP_PackCount"];
                $MaxLength[] = $v["DtMD_iLength"];
                $Count_OnlyM[] = $v["welding_group"];
            }
            if(!empty($dcd_content)){
                $th_id = (new Sectconfigdetail())->get($saveRow["SCD_ID"]);
                if(!$th_id) return json(["code"=>0,"msg"=>"打包失败，请稍后重试！"]);
                $where = [
                    "dcd.DCD_ID" => ["IN",array_keys($dcd_content)],
                    "tp.TH_IDFK" => ["=",$th_id["TH_ID"]],
                    "tp.SCD_ID" => ["IN",[0,$saveRow["SCD_ID"]]]
                ];
                $dcd_list = (new DhCooperateDetail([],$ex))->alias("dcd")
                    ->join([$ex."dhcooperatesingle"=>"dcs"],"dcd.DCD_ID = dcs.DCD_ID")
                    ->join([$ex."dtmaterialdetial"=>"dd"],["dcs.DtMD_ID_PK = dd.DtMD_ID_PK","dcd.DCD_PartNum<>dd.DtMD_sPartsID"])
                    ->join(["taskpart"=>"tp"],"dd.DtMD_ID_PK = tp.DtMD_ID_PK")
					->join(["tasksect"=>"ts"],["ts.TS_Name=dcd.DCD_PartName","ts.SCD_ID=".$saveRow["SCD_ID"]])
                    ->field("tp.TP_ID,dcd.DCD_Count,dcs.DHS_Count,ts.TS_Count,(dcd.DCD_Count*dcs.DHS_Count*ts.TS_Count) as PL_Count,dd.DtMD_fUnitWeight,dd.DtMD_iLength,0 as DtMD_fWeight,dcd.DCD_ID")
                    ->where($where)
                    ->select();
                foreach($dcd_list as $k=>$v){
                    $v["DtMD_fWeight"] = round($v["DtMD_fUnitWeight"]*$v["PL_Count"],1);
                    $key = $v["DCD_ID"].'-'.$v["TP_ID"];
                    $dcd_tp_content[$key] = $dcd_content[$v["DCD_ID"]];
                    $dcd_tp_content[$key]["DtMD_ID_PK"] = $v["TP_ID"];
                    $dcd_tp_content[$key]["PL_Count"] = $v["PL_Count"];
                    $dcd_tp_content[$key]["PD_IsM"] = 0;
                    $PD_sumWeight[] = $v["DtMD_fWeight"];
                    $PD_SumCount[] = $v["PL_Count"];
                    $MaxLength[] = $v["DtMD_iLength"];
                    $savePack[] = $dcd_tp_content[$key];
                }
            }
            $edit = [
                "PD_Name" => $saveRow["PD_Name"],
                "PD_sumWeight" => array_sum($PD_sumWeight),
                "PD_SumCount" => array_sum($PD_SumCount),
                "MaxLength" => empty($MaxLength)?0:max($MaxLength),
                "PD_JHCount" => count($savePack),
                "Count_OnlyM" => array_sum($PD_sumWeight)-array_sum($Count_OnlyM),
            ];
            if($flag and isset($saveRow["SCD_ID"])){
                $P_ID = $saveRow["P_ID"];
                $subQuery = $this->model->alias("p")
                    ->join(["taskheight"=>"th"],"p.TD_ID = th.TD_ID")
                    ->join(["sectconfigdetail"=>"scd"],"th.TH_ID = scd.TH_ID")
                    ->JOIN(["tasksect"=>"ts"],"scd.SCD_ID = ts.SCD_ID")
                    ->field("scd.SCD_TPNum,th.TH_Height,scd.TD_TypeName,scd.SCD_ID,th.TD_ID")
                    ->where([
                        "p.P_ID" => ["=",$P_ID],
                        "ts.TS_Name" => ["IN",$sectName],
                        "p.SCD_ID" => ["=",$saveRow["SCD_ID"]]
                    ])->buildSql();
                $pack_pl_list = DB::QUERY("SELECT *,count(SCD_ID) AS num FROM (".$subQuery.") a where a.SCD_ID<>".$saveRow["SCD_ID"]." GROUP BY SCD_ID having num=".count($sectName)." order by SCD_TPNum asc");
                foreach($pack_pl_list as $k=>$v){
                    $pack_pl_list[$v["SCD_ID"]] = $v;
                    unset($pack_pl_list[$k]);
                }
                if(!empty($pack_pl_list)){
                    $private_list = $this->privateModel->alias("p")->join(["taskpart"=>"t"],"p.DtMD_ID_PK = t.TP_ID","left")->field("p.SCD_ID")->where([
                        "p.SCD_ID" => ["IN",array_keys($pack_pl_list)],
                        "t.DtMD_ID_PK" => ["IN",$dt_id]
                    ])->group("p.SCD_ID")->select();
                    if($private_list) $private_list = collection($private_list)->toArray();
                    else $private_list = [];
                    foreach($private_list as $v){
                        if(isset($pack_pl_list[$v["SCD_ID"]])) unset($pack_pl_list[$v["SCD_ID"]]);
                    }
                }else $pack_pl_list = [];
            }else $pack_pl_list = [];
            $result = false;
            Db::startTrans();
            try {
                $this->privateModel->where("PD_ID",$saveResult)->delete();
                $this->privateModel->allowField(true)->saveAll($savePack);
                $result = $this->detailModel->where("PD_ID",$saveResult)->update($edit);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
            if ($result !== false) {
                $packListSelect = build_select('pack_list', $this->packList($saveRow["P_ID"]), 0, ['class'=>'form-control selectpicker',"id"=>"pack_list"]);
                return json(["code"=>1,"msg"=>"打包成功","saveResult"=>$flag==1?$saveResult:0,"select_html"=>$packListSelect,"pack_pl_list"=>json_encode(array_values($pack_pl_list))]);
            } else {
                return json(["code"=>0,"msg"=>"打包失败，请稍后重试！"]);
            }
        }
        return json(["code"=>0,"msg"=>"打包失败，请稍后重试！"]);
    }

    public function packBatch($pd_id=null,$scd_list=null)
    {
        if(!$pd_id or !$scd_list) $this->error(__('No Results were found'));
        $scd_list = json_decode(base64_decode($scd_list),true);
        $this->assignconfig("scd_list",$scd_list);
        $this->assignconfig("pd_id",$pd_id);
        return $this->view->fetch();
    }

    public function packBatchSet()
    {
        $pd_id = $this->request->post("pd_id");
        $data = json_decode($this->request->post("data"),true);
        $pack_content = $this->model->alias("p")->join(["packdetail"=>"pd"],"p.P_ID = pd.P_ID")->where("pd.PD_ID",$pd_id)->find();
        $pack_list = $this->model->where("TD_ID","=",$pack_content["TD_ID"])->where("P_ID","<>",$pack_content["P_ID"])->select();
        $pack_scd_list = $pack_th_list = $all_sect_list = [];
        foreach($pack_list as $k=>$v){
            $pack_scd_list[$v["SCD_ID"]] = $v["P_ID"];
        }
        $pack_one = $this->detailModel->where("PD_ID",$pd_id)->find();
        if(!$pack_one) return json(["code"=>0,"msg"=>"批量打包失败"]);
        $pack_one = $pack_one->toArray();
        unset($pack_one["PD_ID"],$pack_one["P_ID"]);
        $private_detail = $this->privateModel->alias("pp")->join(["taskpart"=>"tp"],"pp.DtMD_ID_PK = tp.TP_ID")->field("tp.TP_ID,tp.DtMD_ID_PK,pp.DCD_ID,pp.PL_Count,pp.PD_IsM,pp.ifMaxLen,pp.ifChaiFen,tp.SCD_ID")->where("pp.PD_iD",$pd_id)->select();
        if($private_detail) $private_detail = collection($private_detail)->toArray();
        else return json(["code"=>0,"msg"=>"批量打包失败"]);
        $pack_save = $pack_detail_save = [];
        foreach($data as $v){
			$all_sect_list[$v["SCD_ID"]] = $v["SCD_ID"];
            $pack_save[$v["SCD_ID"]] = [
                "SCD_ID" => $v["SCD_ID"],
                "P_Count" => 1,
                "Writer" => $this->admin["nickname"],
                "WriterDate" => date("Y-m-d H:i:s"),
                "P_Flag" => 1,
                "TD_ID" => $v["TD_ID"],
                "SCD_TPNum" => $v["SCD_TPNum"]
            ];
            isset($pack_scd_list[$v["SCD_ID"]])?$pack_save[$v["SCD_ID"]]["P_ID"] = $pack_scd_list[$v["SCD_ID"]]:"";
        }
		$th_list = (new Sectconfigdetail())->field("SCD_ID,TH_ID")->where("SCD_ID","IN",$all_sect_list)->select();
        foreach($th_list as $k=>$v){
            $pack_th_list[$v["SCD_ID"]] = $v["TH_ID"];
        }
        $sect_list = array_keys($pack_save);

        $true_dt_list = $dy_tp_list=[];
        foreach($private_detail as $k=>$v){
			$private_detail[$v["DtMD_ID_PK"].'-'.$v["DCD_ID"]] = $v;
			unset($private_detail[$k]);
            $true_dt_list[$v["DtMD_ID_PK"]] = $v["DtMD_ID_PK"];
        }
        $taskpart_model = new TaskPart();
        $tp_list = $taskpart_model->where("DtMD_ID_PK","IN",$true_dt_list)->select();
        if($tp_list) $tp_list = collection($tp_list)->toArray();
        foreach($tp_list as $k=>$v){
            if($v["SCD_ID"]==0) $dy_tp_list[$v["DtMD_ID_PK"]][$v["TH_IDFK"]][0] = $v["TP_ID"];
            else $dy_tp_list[$v["DtMD_ID_PK"]][$v["TH_IDFK"]][$v["SCD_ID"]] = $v["TP_ID"];
        }
        $DtMD_ID_PK_list = [];
        $search_replace_arr = [];
        if(!empty($DtMD_ID_PK_list)){
            $sql = (new TaskPart())->where("SCD_ID","IN",$sect_list)->buildSql();
            $search_replace_list = Db::table($sql.' a')
                ->field("a.TP_ID,a.DtMD_ID_PK,a.SCD_ID")
                ->where('a.DtMD_ID_PK','IN',$DtMD_ID_PK_list)
                ->select();
            if($search_replace_list) $search_replace_list = collection($search_replace_list)->toArray();
            else $search_replace_list=[];
            foreach($search_replace_list as $k=>$v){
                $search_replace_arr[$v["DtMD_ID_PK"]][$v["SCD_ID"]] = $v["TP_ID"];
            }
        }
        $result = $detail_result = false;
        Db::startTrans();
        try {
            $result = $this->model->allowField(true)->saveAll($pack_save);
            foreach($result as $k=>$v){
                $pack_name = $pack_save[$k]["SCD_TPNum"].substr($pack_one["PD_Name"],strpos($pack_one["PD_Name"],'+'));
                $one = $this->detailModel->where([
                    "P_ID" => ["=",$v["P_ID"]],
                    "PD_Name" => ["=",$pack_name]
                ])->find();
                if($one) return json(["code"=>0,"msg"=>"存在同一杆塔包名重复的问题，请检查，稍后重试！"]);
                $pack_detail_save[$k] = $pack_one;
                $pack_detail_save[$k]["P_ID"] = $v["P_ID"];
                $pack_detail_save[$k]["PD_Name"] = $pack_name;
            }
            $detail_result = $this->detailModel->allowField(true)->saveAll($pack_detail_save);
            $private_pack_list = [];
            foreach($detail_result as $k=>$v){
                foreach($private_detail as $vv){
                    //foreach($pack_th_list as $kkk=>$vvv){
						$kkk = $k;
						$vvv = $pack_th_list[$kkk];
						
                        $private_pack_list[] = [
                            "DtMD_ID_PK" => $dy_tp_list[$vv["DtMD_ID_PK"]][$vvv][0]??$dy_tp_list[$vv["DtMD_ID_PK"]][$vvv][$kkk],
                            "PD_ID"=>$v["PD_ID"],
                            "SCD_ID"=>$kkk,
                            "DCD_ID" => $vv["DCD_ID"],
                            "PL_Count" => $vv["PL_Count"],
                            "PD_IsM" => $vv["PD_IsM"],
                            "ifMaxLen" => $vv["ifMaxLen"],
                            "ifChaiFen" => $vv["ifChaiFen"]
                        ];
                    //}
                }
            }
            $this->privateModel->allowField(true)->saveAll($private_pack_list);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"打包成功"]);
        } else {
            return json(["code"=>0,"msg"=>"打包失败，请稍后重试！"]);
        }
    }

    public function delPack()
    {
        if($this->request->isPost()){
            $pd_id = $this->request->post("pd_id");
            if($pd_id){
                $where = ["PD_ID"=>["=",$pd_id]];
                $list = $this->detailModel->where($where)->find();
                Db::startTrans();
                try {
                    $this->privateModel->where($where)->delete();
                    $list->delete();
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    return json(["code"=>0,"msg"=>$e->getMessage()]);
                } catch (PDOException $e) {
                    Db::rollback();
                    return json(["code"=>0,"msg"=>$e->getMessage()]);
                } catch (Exception $e) {
                    Db::rollback();
                    return json(["code"=>0,"msg"=>$e->getMessage()]);
                }
                $packListSelect = build_select('pack_list', $this->packList($list["P_ID"]), 0, ['class'=>'form-control selectpicker']);
                return json(["code"=>1,"msg"=>"删除成功","select_html"=>$packListSelect]);
            }
        }
        return json(["code"=>0,"msg"=>"删除失败"]);
    }

    public function packList($ids=null)
    {
        $noPackList = [0=>"[全部]"];
        if(!$ids) return $noPackList;
        $one = $this->model->get($ids);
        $noPackArr = $this->model->alias("m")
            ->join(["packdetail"=>"pd"],"m.P_ID = pd.P_ID")
            
            ->field("PD_ID,PD_Name,cast(REVERSE(LEFT(REVERSE(PD_Name),LOCATE('-',REVERSE(PD_Name))-1)) as unsigned) as number")
            ->where("SCD_ID",$one["SCD_ID"])
            ->order("number,PD_Name ASC")
            ->select();
        // $noPackArr = $this->detailModel->field("PD_ID,PD_Name")->where("P_ID",$ids)->order("PD_Name ASC")->select();
        foreach($noPackArr as $v){
            $noPackList[$v["PD_ID"]] = $v["PD_Name"];
        }
        return $noPackList;
    }

    public function packContent()
    {
        $pd_id = $this->request->post("pd_id");
        if(!$pd_id) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $list = $this->privateModel->getPackList(["pp.PD_ID"=>["=",$pd_id]]);
        $data = $dcd_data = $dcd_weight_list = [];
        $weight = 0;
        
        foreach($list as $v){
            // pri($v["DtMD_fWeight"],'');
            $weight += $v["DtMD_fWeight"];
            if($v["DCD_ID"] and $v["PD_IsM"]==0){
                isset($dcd_weight_list[$v["DCD_ID"]])?($dcd_weight_list[$v["DCD_ID"]] += $v["DtMD_fWeight"]):($dcd_weight_list[$v["DCD_ID"]] = $v["DtMD_fWeight"]);
            }else{
                if($v["DCD_ID"]) $dcd_data[$v["DCD_ID"]] = $v["id"];
                $data[$v["id"]] = $v->toArray();
            }
        }
        // pri($weight,1);
        foreach($dcd_weight_list as $k=>$v){
            $data[$dcd_data[$k]]["total_weight"] += $v;
        }
        return json(["code"=>1,"data"=>array_values($data),"weight"=>$weight]);
    }

    public function noPackDt($th_id=null,$scd_id=null,$p_id=null)
    {
        $leftList = [];
        if(!$scd_id or !$th_id) return $leftList;
        $weldList = $sect_list = [];
        
        $tdList = $this->model->alias("p")->join(["taskdetail"=>"td"],"p.TD_ID=td.TD_ID")
                ->JOIN(["task"=>"t"],"t.T_Num=td.T_Num")->where("p.P_ID",$p_id)->value("T_Sort");
        $ex = $this->_technologyEx()[$tdList];

        $sect_num_arr = (new TaskSect())->where("SCD_ID",$scd_id)->column("TS_Name,TS_Count");
        //已打包
        $subsql = $this->detailModel->alias("pd")
            ->join(["privatepack"=>"pp"],"pd.PD_ID = pp.PD_ID")
            ->field("pp.DtMD_ID_PK,pp.PL_ID,pp.DCD_ID,pp.PL_Count,pp.PD_IsM")
            ->where("pd.P_ID",$p_id)
            ->where("pp.SCD_ID",$scd_id)
            // ->group("pp.DtMD_ID_PK")
            ->select();
        //所有的
        $where = [
            "tp.TH_IDFK" => ["=",$th_id],
            "tp.SCD_ID" => ["IN",[$scd_id,0]]
        ];
        $all_list_sql = (new TaskPart())->alias("tp")
            ->join([$ex."dtmaterialdetial"=>"tt"],"tp.DtMD_ID_PK = tt.DtMD_ID_PK")
            ->field("tp.TP_ID,tp.DtMD_ID_PK,tp.TP_SectName,tp.TP_PartsID,tt.DtMD_sSpecification,tt.DtMD_sMaterial,tt.DtMD_iLength,tt.DtMD_fWidth,tp.TP_PackCount,round(tt.DtMD_fUnitWeight,2) as DtMD_fUnitWeight,round(tt.DtMD_fUnitWeight*tp.TP_PackCount,1) as DtMD_fWeight,tp.TP_Welding as 'welding',0 as 'welding_group',tt.DtMD_iFireBending,tt.DtMD_iBackGouging,tt.DtMD_fBackOff,tt.DtMD_DaBian,tt.DtMD_sRemark,CAST(tp.TP_SectName AS UNSIGNED) AS number_1,CAST(tp.TP_PartsID AS UNSIGNED) AS number_2,tp.TP_Welding,(CASE WHEN tp.TP_Welding<>0 THEN 0 ELSE -1 END) as 'PD_IsM',0 AS 'DCD_ID'")
            ->where($where)
            ->order("number_1 ASC,tp.TP_SectName ASC,number_2 ASC,tp.TP_PartsID ASC")
            // ->group("tp.DtMD_ID_PK")
            ->select();
        foreach($all_list_sql as $k=>$v){
            $sect_list[$v["DtMD_ID_PK"]] = $v->toArray();
            $leftList[$v["PD_IsM"].'-'.$v["TP_ID"].'-0'] = [
                "DtMD_ID_PK" => $v["TP_ID"],
                "TP_SectName" => $v["TP_SectName"],
                "TP_PartsID" => $v["TP_PartsID"],
                "DtMD_sSpecification" => $v["DtMD_sSpecification"],
                "DtMD_sMaterial" => $v["DtMD_sMaterial"],
                "DtMD_iLength" => $v["DtMD_iLength"],
                "DtMD_fWidth" => $v["DtMD_fWidth"],
                'TP_PackCount' => $v["TP_PackCount"],
                "DtMD_fUnitWeight" => $v["DtMD_fUnitWeight"],
                "DtMD_fWeight" => $v["DtMD_fWeight"],
                "welding" => $v["welding"],
                'welding_group' => $v["welding_group"],
                "DtMD_iFireBending" => $v["DtMD_iFireBending"],
                "DtMD_iBackGouging" => $v["DtMD_iBackGouging"],
                'DtMD_fBackOff' => $v["DtMD_fBackOff"],
                "DtMD_DaBian" => $v["DtMD_DaBian"],
                "DtMD_sRemark" => $v["DtMD_sRemark"],
                "TP_Welding" => $v["TP_Welding"],
                "PD_IsM" => $v["PD_IsM"],
                'DCD_ID' => $v["DCD_ID"],
                "SCD_ID" => $scd_id,
                "true_DtMD_ID_PK" => $v["DtMD_ID_PK"],
                "total_weight" => $v["DtMD_fWeight"]
                // "identification" => $v["PD_IsM"].'-'.$v["TP_ID"].'-0',
            ];
            if($v["TP_Welding"]!=0){
                $weldList[$v["DtMD_ID_PK"]] = $v["DtMD_ID_PK"];
            }
        }
        $count = $count_num = $weight = $m_count = $m_weight = 0;
        if(!empty($weldList)){
            $detailListWhere = [
                "ds.DtMD_ID_PK" => ["IN",$weldList]
            ];
            $detailList = (new DhCooperateDetail([],$ex))->alias("dd")
                ->join([$ex."dhcooperatesingle"=>"ds"],"dd.DCD_ID = ds.DCD_ID")
                ->join([$ex."dtmaterialdetial"=>"dtd"],"dtd.DtMD_ID_PK = ds.DtMD_ID_PK")
                ->field("ds.DtMD_ID_PK,ds.DCD_ID,(dd.DCD_Count*ds.DHS_Count) as 'DtMD_iUnitCount',DtMD_iWelding as 'welding',ds.DHS_Count as 'welding_group',dd.DCD_PartName,CAST(dd.DCD_PartName AS UNSIGNED) AS number_1,CAST(dd.DCD_PartNum AS UNSIGNED) AS number_2,dd.DCD_PartNum,(CASE WHEN dd.DCD_PartNum=dtd.DtMD_sPartsID THEN 1 ELSE 0 END) as 'PD_IsM'")
                ->where($detailListWhere)
                ->order("number_1,dd.DCD_PartName,number_2,dd.DCD_PartNum ASC,PD_IsM DESC")
                ->select();
            foreach($detailList as $k=>$v){
                $key = "0-".$sect_list[$v["DtMD_ID_PK"]]["TP_ID"].'-0';
                if(isset($leftList[$key]) and isset($sect_num_arr[$v["DCD_PartName"]])){
                    $DtMD_iUnitCount = $sect_num_arr[$v["DCD_PartName"]]*$v["DtMD_iUnitCount"];
                    $welding_group = $sect_num_arr[$v["DCD_PartName"]]*$v["welding_group"];
                    $leftList[$key]["TP_PackCount"] -= $DtMD_iUnitCount;
                    $leftList[$key]["DtMD_fWeight"] = $leftList[$key]["DtMD_fUnitWeight"]*$leftList[$key]["TP_PackCount"];
                    $leftList[$v["PD_IsM"]."-".$sect_list[$v["DtMD_ID_PK"]]["TP_ID"].'-'.$v["DCD_ID"]] = [
                        "DtMD_ID_PK" => $leftList[$key]["DtMD_ID_PK"],
                        "TP_SectName" => $leftList[$key]["TP_SectName"],
                        "TP_PartsID" => $leftList[$key]["TP_PartsID"],
                        "DtMD_sSpecification" => $leftList[$key]["DtMD_sSpecification"],
                        "DtMD_sMaterial" => $leftList[$key]["DtMD_sMaterial"],
                        "DtMD_iLength" => $leftList[$key]["DtMD_iLength"],
                        "DtMD_fWidth" => $leftList[$key]["DtMD_fWidth"],
                        'TP_PackCount' => $DtMD_iUnitCount,
                        "DtMD_fUnitWeight" => $leftList[$key]["DtMD_fUnitWeight"],
                        "DtMD_fWeight" => round($DtMD_iUnitCount*$leftList[$key]["DtMD_fUnitWeight"],1),
                        "welding" => $v["welding"],
                        'welding_group' => $welding_group,
                        "DtMD_iFireBending" => $leftList[$key]["DtMD_iFireBending"],
                        "DtMD_iBackGouging" => $leftList[$key]["DtMD_iBackGouging"],
                        'DtMD_fBackOff' => $leftList[$key]["DtMD_fBackOff"],
                        "DtMD_DaBian" => $leftList[$key]["DtMD_DaBian"],
                        "DtMD_sRemark" => $leftList[$key]["DtMD_sRemark"],
                        "TP_Welding" => $leftList[$key]["TP_Welding"],
                        "PD_IsM" => $v["PD_IsM"],
                        'DCD_ID' => $v["DCD_ID"],
                        "SCD_ID" => $leftList[$key]["SCD_ID"],
                        "true_DtMD_ID_PK" => $leftList[$key]["true_DtMD_ID_PK"],
                        // "identification" => $v["PD_IsM"]."-".$sect_list[$v["DtMD_ID_PK"]]["TP_ID"].'-'.$v["DCD_ID"]
                        "total_weight" => round($DtMD_iUnitCount*$leftList[$key]["DtMD_fUnitWeight"],1)
                    ];
                    if($leftList[$key]["TP_PackCount"]<=0) unset($leftList[$key]);
                    
                }
            }
        }
        foreach($subsql as $k=>$v){
            $key = $v["PD_IsM"].'-'.$v["DtMD_ID_PK"].'-'.$v["DCD_ID"];
            if(isset($leftList[$key])){
                $leftList[$key]["TP_PackCount"] -= $v["PL_Count"];
                $leftList[$key]["DtMD_fWeight"] = round($leftList[$key]["DtMD_fUnitWeight"]*$leftList[$key]["TP_PackCount"],1);
                if($leftList[$key]["TP_PackCount"]<=0) unset($leftList[$key]);
            }
        }
        $id_count = 0;
        $total_weight_list = $dcd_list = [];
        foreach($leftList as $k=>$v){
            $count_num += $v["TP_PackCount"];
            $weight += $v["DtMD_fWeight"];
            if($v["DCD_ID"]){
                $m_weight += $v["DtMD_fWeight"];
                if($v["PD_IsM"]==1){
                    $m_count += $v["TP_PackCount"];
                    $dcd_list[$v["DCD_ID"]] = $k;
                }else{
                    isset($total_weight_list[$v["DCD_ID"]])?$total_weight_list[$v["DCD_ID"]] += $v["DtMD_fWeight"]:$total_weight_list[$v["DCD_ID"]] = $v["DtMD_fWeight"];
                    unset($leftList[$k]);
                    continue;
                }
            }else if($v["welding"]!=0){
                $m_weight += $v["DtMD_fWeight"];
                $m_count += $v["TP_PackCount"];
            }
            $id_count ++;
            $leftList[$k]["id"] = $id_count;
        }
        foreach($total_weight_list as $k=>$v){
            isset($dcd_list[$k])?($leftList[$dcd_list[$k]]["total_weight"] += $v):0;
            
        }
        $msg = "总数 ".$count_num.' 总重:'.$weight.'kg 电焊数:'.$m_count.' 电焊重:'.$m_weight.'kg';
        return [array_values($leftList),$msg];

    }

    public function showWeld($pd_id = null)
    {
        if(!$pd_id) $this->error(__('No Results were found'));
        $where = ["pp.PD_ID"=>["=",$pd_id]];
        $list = $this->privateModel->getPackList($where);
        $this->assignconfig("list",$list);
        return $this->view->fetch();
    }

    public function packWeldCompact($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $tdList = $this->model->alias("p")->join(["taskdetail"=>"td"],"p.TD_ID=td.TD_ID")
                ->JOIN(["task"=>"t"],"t.T_Num=td.T_Num")->where("p.P_ID",$ids)->value("T_Sort");
        $ex = $this->_technologyEx()[$tdList];
        $td_id = $row["TD_ID"];
        $fy_list = (new DhCooperate([],$ex))->alias("dh")
            ->join([$ex."dhcooperatedetail"=>"dd"],"dh.DC_Num=dd.DC_Num")
            ->field("dd.DCD_PartNum,dd.DCD_PartName,cast(dd.DCD_PartName as unsigned) as number_1,dd.DCD_Count,cast(dd.DCD_PartNum as unsigned) as number,dd.DCD_Count as 'discount',0 as 'packnum'")
            ->where("dh.TD_ID",$td_id)
            ->order("number_1,dd.DCD_PartName,number,dd.DCD_PartNum")
            ->select();
        $fy_arr = [];
        foreach($fy_list as $v){
            $fy_arr[$v["DCD_PartNum"]] = $v->toArray();
        }
        $where = [
            "d.P_ID" => ["=",$ids],
            "pp.PD_IsM" => ["=",1]
        ];
        $pack_list = $this->detailModel->alias("d")
            ->join(["privatepack"=>"pp"],"d.PD_ID=pp.PD_ID")
            ->join(["taskpart"=>"tp"],"pp.DtMD_ID_PK = tp.TP_ID")
            ->join([$ex."dhcooperatedetail"=>"dd"],"pp.DCD_ID = dd.DCD_ID")
            ->field("pp.PL_Count,tp.TP_PartsID,dd.DCD_Count")
            ->where($where)
            ->order("tp.TP_PartsID")
            ->select();
        foreach($pack_list as $v){
            if(isset($fy_arr[$v["TP_PartsID"]])){
                $fy_arr[$v["TP_PartsID"]]["packnum"] = $v["DCD_Count"];
                $fy_arr[$v["TP_PartsID"]]["discount"] = $fy_arr[$v["TP_PartsID"]]["DCD_Count"]-$v["DCD_Count"];
            }
        }
        $this->assignconfig('content',array_values($fy_arr));
        return $this->view->fetch();        
    }
    
    public function multiPack()
    {
        if ($this->request->isPost()) {
            [$TD_ID,$params] = array_values($this->request->post());
            $params = $params?json_decode($params,true):[];
            if ($params) {
                $data = [];
                foreach($params as $k=>$v){
                    if($v["state"]=="未打包"){
                        $data[] = [
                            "SCD_ID" => $v["SCD_ID"],
                            "P_Count" => $v["SCD_Count"],
                            "Writer" => $this->admin["username"],
                            "Auditor" => $this->admin["username"],
                            "WriteDate" => date("Y-m-d H:i:s"),
                            "AuditorDate" => date("Y-m-d H:i:s"),
                            "P_Flag" => 1,
                            "TD_ID" => $TD_ID,
                            "PD_Name" => $v["SCD_TPNum"]."+1-1"
                        ];
                    }
                }
                $detailResult = false;
                if(!empty($data)){
                    Db::startTrans();
                    try {
                        $result = $this->model->allowField(true)->saveAll($data);
                        $detailData = [];
                        foreach($result as $v){
                            $detailData[] = [
                                "PD_Name" => $v["PD_Name"],
                                "P_ID" => $v["P_ID"]
                            ];
                        }
                        $detailResult = $this->detailModel->saveAll($detailData);
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                
                if ($detailResult !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    public function chooseMultiScd()
    {
        $td_id = $this->request->post("TD_ID");
        if(!$td_id) return json(["code"=>0,"msg"=>"没有塔型 请稍后再试","data"=>[]]);
        $data = $bolt_where = [];
        $where = ["th.TD_ID"=>["=",$td_id]];
        $sect_list = (new Sectconfigdetail())->alias("s")
            ->join(["taskheight"=>"th"],"th.TH_ID=s.TH_ID")
            ->join(["taskdetail"=>"td"],"td.TD_ID=th.TD_ID")
            ->join(["task"=>"t"],"td.T_Num = t.T_Num")
            ->join(["pack"=>"p"],"p.SCD_ID = s.SCD_ID","LEFT")
            ->field("ifnull(p.P_ID,0) as P_ID,s.SCD_ID,t.t_project,td.TD_TypeName,s.SCD_TPNum,'未打包' as state,t.T_Num,s.SCD_Count,s.TH_Height,s.TD_Pressure")
            ->where($where)->group("s.SCD_ID")->select();
        foreach($sect_list as $v){
            $data[$v["SCD_ID"]] = $v->toArray();
            $bolt_where[] = $v["SCD_ID"];
        }
        $pack_list = $this->model->where("SCD_ID","IN",$bolt_where)->select();
        foreach($pack_list as $v){
            isset($data[$v["SCD_ID"]])?$data[$v["SCD_ID"]]["state"]="已打包":"";
        }
        return json(["code"=>1,"msg"=>"成功","data"=>array_values($data)]);
    }
}
