<?php

namespace app\admin\controller\chain\pack;

// use app\admin\model\chain\lofting\BoltFastCreateMain;
// use app\admin\model\chain\lofting\DhCooperate;
// use app\admin\model\chain\lofting\DhCooperateDetail;
// use app\admin\model\chain\lofting\Dtmaterialdetail;
// use app\admin\model\chain\lofting\ProduceTask;
// use app\admin\model\chain\lofting\TaskPart;
// use app\admin\model\chain\sale\Sectconfigdetail;

use app\admin\model\chain\sale\Task;
use app\common\controller\Backend;
// use think\Db;
// use think\exception\PDOException;
// use think\exception\ValidateException;
// use Exception;

/**
 * 专用段包装
 *
 * @icon fa fa-circle-o
 */
class PackList extends Backend
{
    
    /**
     * Pack模型对象
     * @var \app\admin\model\chain\pack\PackList
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\pack\Pack;
        $this->detailModel = new \app\admin\model\chain\pack\PackDetail;
        $this->privateModel = new \app\admin\model\chain\pack\PrivatePack;
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("p")
                ->join(["taskdetail"=>"td"],"p.TD_ID = td.TD_ID")
                ->join(["task"=>"t"],'t.T_Num = td.T_Num')
                ->join(["compact"=>"c"],"t.C_Num = c.C_Num")
                ->join(["newoldtdtypename"=>"nottn"],"nottn.TD_ID = P.TD_ID","left")
                ->field("p.P_ID,p.TD_ID,td.P_Name,td.TD_TypeName,td.T_Num,td.T_Num as 'td.T_Num',td.TD_Pressure,t.T_Company,t.t_project,t.t_shortproject,nottn.old_TypeName,t.C_Num,c.PC_Num,SUM(P_Count) as count,0 as weight")
                ->where($where)
                ->where("p.P_Flag",1)
                ->order($sort, $order)
                ->group("td.TD_ID")
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function towerListTable()
    {
        $params = $this->request->post();
        $tdId = $params["tdId"];
        $supplement = json_decode($params["supplement"],true);
        if(!$tdId) return json(["code"=>0,"msg"=>"失败"]);
        $where = [
            "m.TD_ID" => ["=",$tdId],
            "m.P_Flag" => ["=",1]
        ];
        $list = $this->model->alias("m")
            ->join(["sectconfigdetail"=>"scd"],"m.SCD_ID = scd.SCD_ID")
            ->field("m.*,scd.SCD_TPNum,scd.TH_Height,scd.SCD_Count")
            ->where($where)
            ->order("m.SCD_ID ASC")
            ->group("m.SCD_ID,m.TD_ID")
            ->select();
        $data = [];
        foreach($list as $k=>$v){
            $data[$k] = $v->toArray();
            foreach($supplement as $kk=>$vv){
                $data[$k][$kk] = $vv;
            }
        }
        return json(["code"=>1,"data"=>$data]);
    }

    public function towerPackageDetailsTable()
    {
        $params = $this->request->post();
        $pId = $params["pId"];
        $supplement = json_decode($params["supplement"],true);
        if(!$pId) return json(["code"=>0,"msg"=>"失败"]);
        $one = $this->model->where("P_ID",$pId)->find();
        $where = [
            "m.SCD_ID" => ["=",$one["SCD_ID"]],
            "m.P_Flag" => ["=",1]
        ];
        $list = $this->model->alias("m")
            ->join(["packdetail"=>"pd"],"m.P_ID = pd.P_ID")
            ->field("m.*,pd.PD_Name,pd.PD_Type,pd.PD_Content,pd.PD_ID,cast(SUBSTR(pd.PD_Name,LOCATE('-',pd.PD_Name)+1) as unsigned) as number")
            ->where($where)
            ->order("number,pd.PD_Name ASC")
            ->select();
        $data = [];
        foreach($list as $k=>$v){
            $data[$k] = $v->toArray();
            $data[$k]["WriteDate"] = date("Y-m-d",strtotime($v["WriteDate"]));
            $data[$k]["AuditorDate"] = date("Y-m-d",strtotime($v["AuditorDate"]));
            foreach($supplement as $kk=>$vv){
                $data[$k][$kk] = $vv;
            }
        }
        return json(["code"=>1,"data"=>$data]);
    }

    public function edit($ids = null)
    {

        $where = [
            "m.TD_ID" => ["=",$ids],
            "m.P_Flag" => ["=",1]
        ];
        $list = $this->model->alias("m")
            ->join(["sectconfigdetail"=>"scd"],"m.SCD_ID = scd.SCD_ID")
            ->field("m.P_ID,scd.SCD_TPNum")
            ->where($where)
            ->order("m.SCD_ID ASC")
            ->group("m.SCD_ID,m.TD_ID")
            ->select();
        $scd_list = [["id"=>"","name"=>"全部"]];
        foreach($list as $k=>$v){
            $scd_list[] = ["id"=>$v["P_ID"],"name"=>$v["SCD_TPNum"]];
        }
        $this->view->assign("scd_list",json_encode($scd_list));
        $this->view->assign("ids",$ids);
        $this->assignconfig('ids',$ids);

        return $this->view->fetch();
    }

    public function scdList($ids = null)
    {
        $scd_list = [];
        if($ids){
            $where = [
                "m.TD_ID" => ["=",$ids],
                "m.P_Flag" => ["=",1]
            ];
            $list = $this->model->alias("m")
                ->join(["sectconfigdetail"=>"scd"],"m.SCD_ID = scd.SCD_ID")
                ->field("m.P_ID,scd.SCD_TPNum")
                ->where($where)
                ->order("m.SCD_ID ASC")
                ->group("m.SCD_ID,m.TD_ID")
                ->select();
            
            foreach($list as $k=>$v){
                if($k==0){
                    $scd_list[] = ["id"=>"all","name"=>"全部"];
                }
                $scd_list[] = ["id"=>$v["P_ID"],"name"=>$v["SCD_TPNum"]];
            }
        }
        return json(["list"=>$scd_list,"total"=>count($scd_list)]);
    }

    public function scdPackList($ids = null)
    {
        $scd_list = [];
        if($ids){
            $scd_p = $this->request->post("scd");
            $where = [
                "m.TD_ID" => ["=",$ids],
                "m.P_Flag" => ["=",1]
            ];
            if($scd_p) $where["pd.P_ID"] = ["=",$scd_p];
            $list = $this->model->alias("m")
                ->join(["packdetail"=>"pd"],"m.P_ID = pd.P_ID")
                ->field("pd.PD_Name,pd.PD_ID,pd.P_ID")
                ->where($where)
                ->order("pd.PD_Name ASC")
                ->select();
            
            foreach($list as $k=>$v){
                // if($k==0){
                //     $scd_list[] = ["id"=>"all","name"=>"全部"];
                // }
                $scd_list[] = ["id"=>$v["PD_ID"],"name"=>$v["PD_Name"]];
            }
        }
        return json(["list"=>$scd_list,"total"=>count($scd_list)]);
    }
    
    public function print($ids = null, $showMain=true, $showWithoutWeight=false, $factoryName="", $pack_list_pids="",$scdid="")
    {        
        $tdList = (new Task())->alias("t")->join(["taskdetail"=>"td"],"t.T_Num=td.T_Num")->where("TD_ID",$ids)->value("t.T_Sort");
        $ex = $this->_technologyEx()[$tdList];
        $where = [
            "p.TD_ID" => ["=",$ids],
            "p.P_Flag" => ["=",1]
        ];

        if($scdid && $scdid!='all'){
            $where['p.P_ID'] = ['=', $scdid];
        }
        
        $row = $this->model->alias("p")
                ->join(["sectconfigdetail"=>"scd"],"p.SCD_ID = scd.SCD_ID")
                ->join(["taskdetail"=>"td"],"p.TD_ID = td.TD_ID", 'left')
                ->join(["task"=>"t"],'t.T_Num = td.T_Num', 'left')
                ->join(["compact"=>"c"],"t.C_Num = c.C_Num", 'left')
                ->join(["newoldtdtypename"=>"nottn"],"nottn.TD_ID = P.TD_ID","left")
                ->field("scd.TH_Height, scd.SCD_Count, scd.SCD_TPNum, scd.TD_Pressure, SCD_Part,scd.SCD_SpPart, scd.SCD_Weight, 
                p.Writer, p.WriteDate, p.P_ID,p.SCD_ID,p.TD_ID,
                td.P_Name, td.TD_TypeName,td.T_Num,td.TD_Pressure,
                t.T_Company,t.t_project,t.t_shortproject,t.C_Num,
                c.PC_Num,c.Customer_Name,nottn.old_TypeName")
                ->where($where)
                ->order("p.SCD_ID ASC")
                ->group("p.SCD_ID,p.TD_ID")
                ->select();
        
        
        foreach ($row as $k => $v) {
            $v["WriteDate"] = date("Y-m-d", strtotime($v["WriteDate"]));
            $v["NowDate"] = date("Y-m-d");
            $v["WriteDate"] = date("Y-m-d", strtotime($v["WriteDate"]));
            $packwhere = [
                "m.SCD_ID" => ["=",$v['SCD_ID']],
                "m.P_Flag" => ["=",1]
            ];
            $packlist = $this->model->alias("m")
                ->join(["packdetail"=>"pd"],"m.P_ID = pd.P_ID", 'left')
                ->join(["privatepack"=>"pp"],"pp.PD_ID = pd.PD_ID", 'left')
                ->join(["taskpart"=>"tp"],"tp.TP_ID = pp.DtMD_ID_PK", 'left')
                ->join([$ex."dtmaterialdetial"=>"d"],"tp.DtMD_ID_PK = d.DtMD_ID_PK")
                ->field("m.*,pd.PD_Name,pd.PD_Type,pd.PD_Content,pd.PD_ID as pdid,pp.*,tp.TP_ID,tp.DtMD_ID_PK,tp.TP_PartsID,tp.TP_UnitCount,tp.TP_UnitWeight,tp.TP_SumCount,tp.TP_SectName,tp.TH_IDFK,tp.PD_ID,tp.PD_Count,tp.TP_Welding,tp.TP_PackCount,d.*, cast(substring_index(pd.PD_Name, '-', -1) as UNSIGNED) bh,round(d.DtMD_fUnitWeight*pp.PL_Count,2) as TP_SumWeight,
                case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
                SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
                else 0 end 	bjbhn")
                ->where($packwhere)
                ->order("bh, bjbhn ASC")
                ->select();
            $v['packlist'] = $packlist;
        }
        // $this->assignconfig("list",$list);
        $this->assignconfig("row", $row);
        $this->assignconfig('ids',$ids);
        $this->assignconfig('showMain',$showMain);
        $this->assignconfig('showWithoutWeight',$showWithoutWeight);
        $this->assignconfig('factoryName',$factoryName);
        $this->assignconfig('pack_list_pids', $pack_list_pids);
        $this->assignconfig('scd_list_id', $scdid);
        return $this->view->fetch();
    }

    public function printZlqd($ids = null)
    {

        $where = [
            "p.TD_ID" => ["=",$ids],
            "p.P_Flag" => ["=",1]
        ];
        
        $row = $this->model->alias("p")
                ->join(["sectconfigdetail"=>"scd"],"p.SCD_ID = scd.SCD_ID")
                ->join(["taskdetail"=>"td"],"p.TD_ID = td.TD_ID", 'left')
                ->join(["task"=>"t"],'t.T_Num = td.T_Num', 'left')
                ->join(["compact"=>"c"],"t.C_Num = c.C_Num", 'left')
                ->join(["newoldtdtypename"=>"nottn"],"nottn.TD_ID = P.TD_ID","left")
                ->field("scd.TH_Height, scd.SCD_Count, scd.SCD_TPNum, scd.TD_Pressure, SCD_Part,scd.SCD_SpPart, scd.SCD_Weight, 
                p.*,
                td.P_Name, td.TD_TypeName,td.T_Num,td.TD_Pressure,
                t.T_Company,t.t_project,t.t_shortproject,t.C_Num,
                c.PC_Num,c.Customer_Name,nottn.old_TypeName")
                ->where($where)
                ->order("p.SCD_ID ASC")
                ->group("p.SCD_ID,p.TD_ID")
                ->select();
        
        foreach ($row as $k => $v) {
            $v["WriteDate"] = date("Y-m-d", strtotime($v["WriteDate"]));
            $v["NowDate"] = date("Y-m-d");
            $v["WriteDate"] = date("Y-m-d", strtotime($v["WriteDate"]));
            $packwhere = [
                "m.SCD_ID" => ["=",$v['SCD_ID']],
                "m.P_Flag" => ["=",1]
            ];
            $packlist = $this->model->alias("m")
                ->join(["packdetail"=>"pd"],"m.P_ID = pd.P_ID")
                ->field("m.*, pd.*, cast(substring_index(pd.PD_Name, '-', -1) as UNSIGNED) bh")
                ->where($packwhere)
                ->order("bh ASC")
                ->select();
            // foreach($packlist as $kk=>$vv){
            //     $packlist[$kk]["TP_PackCount"]  = $v["SCD_Count"]*$vv["TP_PackCount"];
            //     $packlist[$kk]["TP_SumWeight"]  = $v["SCD_Count"]*$vv["TP_SumWeight"];
            // }
            $v['packlist'] = $packlist;
        }

        $this->assignconfig("row", $row);
        $this->assignconfig('ids',$ids);
   
        return $this->view->fetch();
    }
}
