<?php

namespace app\admin\controller\chain\pack;

use app\admin\model\chain\sale\Sectconfigdetail;
use app\common\controller\Backend;
use think\Cache;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
/**
 * 
 *
 * @icon fa fa-circle-o
 */
class MonthlyStatistics extends Backend
{
    
    /**
     * SaleInvoice模型对象
     * @var \app\admin\model\chain\pack\MonthlyStatistics
     */
    protected $model = null;
    protected $noNeedLogin = ["eipUpload"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\pack\SaleInvoice;
        $this->singleModel = new \app\admin\model\chain\pack\SaleInvoiceSingle;
        $this->mainModel = new \app\admin\model\chain\pack\SaleInvoiceMain;
        $this->detailModel = new \app\admin\model\chain\pack\SaleInvoiceDetail;
        $this->bsoModel = new \app\admin\model\chain\pack\BsoCount;
        $this->rkModel = new \app\admin\model\chain\pack\ProduceUnitMain;
        $this->rkDetailModel = new \app\admin\model\chain\pack\ProduceUnitIn;
        $defaultTime = date("Y-m-1 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        $this->admin = \think\Session::get('admin');

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        // dump(Cache::get('month/monthlystatistics')); 
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            if(!isset($filter["time"])) $this->error("请选择时间段！");
            Cache::rm('month/monthlystatistics'); 
            $time_list = explode(" - ",$filter["time"]);
            $where = ["PUM_Date"=>["<=",$time_list[1]]];
            if(isset($filter["PC_Num"])) $where["c.PC_Num"] = ["LIKE","%".$filter["PC_Num"].'%'];
            if(isset($filter["T_Num"])) $where["t.T_Num"] = ["LIKE","%".$filter["T_Num"].'%'];
            if(isset($filter["TD_TypeName"])) $where["scd.TD_TypeName"] = ["LIKE","%".$filter["TD_TypeName"].'%'];
            if(isset($filter["C_Project"])) $where["c.C_Project"] = ["LIKE","%".$filter["C_Project"].'%'];
            if(isset($filter["category"])) $where["pum.category"] = ["=",$filter["category"]];

            $kc_rk = [];
            $rk_list = $this->rkModel->alias("pum")
                ->join(["produceunitin"=>"pui"],"pum.PUM_Num = pui.PUM_Num")
                ->join(["sectconfigdetail"=>"scd"],"scd.SCD_ID = pui.SCD_ID")
                ->join(["taskheight"=>"th"],"th.TH_ID = scd.TH_ID")
                ->JOIN(["taskdetail"=>"td"],"th.TD_ID = td.TD_ID")
                ->JOIN(["task"=>"t"],"td.T_Num = t.T_Num")
                ->join(["compact"=>"c"],"t.C_Num = c.C_Num")
                ->where($where)
                ->field("pum.category,pui.SCD_ID,scd.TH_ID,sum(PU_Count) as kc_count,round(sum(PU_Weight),2) as kc_weight,c.PC_Num,t.T_Num,scd.TD_TypeName,scd.TH_Height,c.C_Project")
                ->order("pui.SCD_ID desc")
                ->group("scd.TH_ID")
                ->select();
            foreach($rk_list as $k=>$v){
                $kc_rk[$v["TH_ID"]] = $v->toArray();
                $kc_rk[$v["TH_ID"]]["by_rk_count"] = $kc_rk[$v["TH_ID"]]["by_rk_weight"] = $kc_rk[$v["TH_ID"]]["by_ck_count"] = $kc_rk[$v["TH_ID"]]["by_ck_weight"] = 0;
                $kc_rk[$v["TH_ID"]]["qc_count"] = $v["kc_count"];
                $kc_rk[$v["TH_ID"]]["qc_weight"] = $v["kc_weight"];
            }
            $ck_list = $this->model->alias("si")
                ->join(["saleinvoicesingle"=>"sis"],"si.SI_Num = sis.SI_Num")
                ->join(["sectconfigdetail"=>"scd"],"scd.SCD_ID = sis.SIS_ScdId")
                ->where("SI_Date","<=",$time_list[1])
                ->field("SIS_ScdId as SCD_ID,scd.TH_ID,sum(SIS_Count) as ck_count,round(sum(SIS_Weight),2) as ck_weight")
                ->order("sis.SIS_ScdId desc")
                ->group("scd.TH_ID")
                ->select();
            foreach($ck_list as $k=>$v){
                if(isset($kc_rk[$v["TH_ID"]])){
                    $kc_rk[$v["TH_ID"]]["kc_count"] -= $v["ck_count"];
                    $kc_rk[$v["TH_ID"]]["kc_weight"] -= $v["ck_weight"];
                    $kc_rk[$v["TH_ID"]]["qc_count"] -= $v["ck_count"];
                    $kc_rk[$v["TH_ID"]]["qc_weight"] -= $v["ck_weight"];
                }
            }
            
            $by_rk_list = $this->rkModel->alias("pum")
                ->join(["produceunitin"=>"pui"],"pum.PUM_Num = pui.PUM_Num")
                ->join(["sectconfigdetail"=>"scd"],"scd.SCD_ID = pui.SCD_ID")
                ->where("PUM_Date","between time",$time_list)
                ->field("pui.SCD_ID,scd.TH_ID,sum(PU_Count) as rk_count,round(sum(PU_Weight),2) as rk_weight")
                ->order("pui.SCD_ID desc")
                ->group("scd.TH_ID")
                ->select();
            foreach($by_rk_list as $v){
                if(isset($kc_rk[$v["TH_ID"]])){
                    $kc_rk[$v["TH_ID"]]["by_rk_count"] = $v["rk_count"];
                    $kc_rk[$v["TH_ID"]]["by_rk_weight"] = $v["rk_weight"];
                    $kc_rk[$v["TH_ID"]]["qc_count"] -= $v["rk_count"];
                    $kc_rk[$v["TH_ID"]]["qc_weight"] -= $v["rk_weight"];
                }
            }
            $by_ck_list = $this->model->alias("si")
                ->join(["saleinvoicesingle"=>"sis"],"si.SI_Num = sis.SI_Num")
                ->join(["sectconfigdetail"=>"scd"],"scd.SCD_ID = sis.SIS_ScdId")
                ->where("SI_Date","between time",$time_list)
                ->field("SIS_ScdId as SCD_ID,scd.TH_ID,sum(SIS_Count) as ck_count,round(sum(SIS_Weight),2) as ck_weight")
                ->order("sis.SIS_ScdId desc")
                ->group("scd.TH_ID")
                ->select();
            foreach($by_ck_list as $v){
                if(isset($kc_rk[$v["TH_ID"]])){
                    $kc_rk[$v["TH_ID"]]["by_ck_count"] = $v["ck_count"];
                    $kc_rk[$v["TH_ID"]]["by_ck_weight"] = $v["ck_weight"];
                    $kc_rk[$v["TH_ID"]]["qc_count"] += $v["ck_count"];
                    $kc_rk[$v["TH_ID"]]["qc_weight"] += $v["ck_weight"];
                }
            }
            $qc_count = $qc_weight = $y_rk_count = $y_ck_count = $y_rk_weight = $y_ck_weight = $qm_count = $qm_weight = 0;
            foreach($kc_rk as $k=>$v){
                if($v["kc_count"] <= 0 and $v["by_rk_count"] <= 0 and $v["by_ck_count"] <= 0 and $v["qc_count"] <= 0){
                    unset($kc_rk[$k]);
                }else if(($v["T_Num"]=='NO.20210079' and $v["TD_TypeName"]=='2B8-SSJ51') or ($v["T_Num"]=='NO.20210022' and $v["TD_TypeName"]=='5C3-SDJC') or ($v["T_Num"]=="NO.20220200") or ($v["T_Num"]=="NO.XN000001")){
                    unset($kc_rk[$k]);
                }else{
                    $kc_ck[$k]["kc_weight"] = round($v["kc_weight"],2);
                    $kc_ck[$k]["by_rk_weight"] = round($v["by_rk_weight"],2);
                    $kc_ck[$k]["by_ck_weight"] = round($v["by_ck_weight"],2);
                    $kc_ck[$k]["qc_weight"] = round($v["qc_weight"],2);
                    $qc_count += $v["qc_count"];
                    $qc_weight += $v["qc_weight"];
                    $y_rk_count += $v["by_rk_count"];
                    $y_rk_weight += $v["by_rk_weight"];
                    $y_ck_count += $v["by_ck_count"];
                    $y_ck_weight += $v["by_ck_weight"];
                    $qm_count += $v["kc_count"];
                    $qm_weight += $v["kc_weight"];
                }
            }
            $total = count($kc_rk);
            $kc_rk[] = ["TD_TypeName"=>"合计","qc_count"=>$qc_count,"qc_weight"=>round($qc_weight,2),"by_rk_count"=>$y_rk_count,"by_rk_weight"=>round($y_rk_weight,2),"by_ck_count"=>$y_ck_count,"by_ck_weight"=>round($y_ck_weight,2),"kc_count"=>$qm_count,"kc_weight"=>round($qm_weight,2)];
            $cache_result = Cache::set('month/monthlystatistics',json_encode($kc_rk),3600);
            $result = array("total" => $total, "rows" => array_values($kc_rk));
            return json($result);
        }
        return $this->view->fetch();
    }

    public function print($ids, $month){
        $title = date('Y年').$month.'月 成品出入库月统计';
        $list = json_decode(Cache::get('month/monthlystatistics')); 
        $this->assignconfig('list',$list);
        $this->assignconfig('zb', $this->admin['username']);
        $this->assignconfig('title', $title);
        return $this->view->fetch();
    }

    // public function eipUpload()
    // {
    //     $json_result = ["code"=>0,"msg"=>"失败"];
    //     $params = (new \app\admin\model\view\StateTowerView())->limit(3)->select();
    //     if(!$params) return json($json_result);
    //     $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
    //     $sectModel = new Sectconfigdetail();
    //     $saveData = [];
    //     $param_url = "eip-prod-store";
    //     foreach($params as $k=>$v){
    //         $operatetype = "ADD";
    //         $api_data = [
    //             "purchaserHqCode" => "SGCC",
    //             "supplierCode" => "1000014615",
    //             "supplierName" => "绍兴电力设备有限公司",
    //             "categoryCode"=>"60",
    //             "subclassCode"=>"60001",
    //             "productAmount" => $v["count"],
    //             "productCode" => $v["SCD_ID"],
    //             "eipMatCode" => "",
    //             "eipMatDes" => "",
    //             "storeCity" => "绍兴市",
    //             "poNo" => '',
    //             "poItemId" => ''
    //         ];
    //         if($v["tower_eip_upload"]){
    //             $operatetype = 'UPDATE';
    //             unset($api_data["dataSourceCreateTime"]);
    //         }
    //         $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$api_data);
    //         if($api_result["code"]==1) $sectModel->where("SCD_ID",$v["SCD_ID"])->update(["tower_eip_upload"=>1]);
    //         else $saveData[$v["TD_TypeName"].$v["SCD_TPNum"]] = $api_result["msg"];
    //     }
    //     $msg = "上传成功";
    //     if(!empty($saveData)) {
    //         $msg = "";
    //         foreach($saveData as $k=>$v){
    //             $msg .= $k.$v.";";
    //         }
    //         $json_result["msg"] = $msg;
    //     }else $json_result = ["code"=>1,"msg"=>$msg];
    //     return json($json_result);
    // }

}
