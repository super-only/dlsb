<?php

namespace app\admin\controller\chain\pack;

use app\admin\model\chain\pack\BsoCount;
use app\admin\model\chain\pack\Pack;
use app\admin\model\chain\sale\Sectconfigdetail;
use app\admin\model\chain\sale\Task;
use app\admin\model\jichu\yw\ShippingType;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
/**
 * 
 *
 * @icon fa fa-circle-o
 */
class SaleInvoice extends Backend
{
    
    /**
     * SaleInvoice模型对象
     * @var \app\admin\model\chain\pack\SaleInvoice
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\pack\SaleInvoice;
        $this->detailModel = new \app\admin\model\chain\pack\SaleInvoiceSingle;
        $this->mainModel = new \app\admin\model\chain\pack\SaleInvoiceMain;
        $this->ddetailModel = new \app\admin\model\chain\pack\SaleInvoiceDetail;
        $way_list = [];
        $way_row = (new ShippingType())->field("ST_Num,ST_Name")->order("st_selected DESC,ST_Num ASC")->select();
        foreach($way_row as $k=>$v){
            $way_list[$v["ST_Name"]] = $v["ST_Name"];
        }
        $ck_way = ["正常出库"=>"正常出库","预开票出库"=>"预开票出库"];
        $this->way_list = $way_list;
        $this->ck_way = $ck_way;
        $this->view->assign("way_list",$way_list);
        $this->view->assign("ck_way",$ck_way);
        $this->assignconfig("way_list",$way_list);
        $this->assignconfig("ck_way",$ck_way);
        $this->admin = \think\Session::get('admin');

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("si")
                ->join(["compact"=>"c"],"c.C_Num = si.C_Num")
                ->join(["task"=>"t"],"c.C_Num = t.C_Num")
                ->field("si.*,c.PC_Num,c.C_Project,group_concat(t.T_Num) as T_Num")
                ->where($where)
                ->order($sort, $order)
                ->group("c.C_Num,si.SI_Num")
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function content()
    {
        $ids = $this->request->post("ids");
        if($ids){
            $list = $this->mainModel->alias("m")
            ->join(["saleinvoicedetail"=>"sid"],"m.SIM_Num = sid.SI_Num","left")
            ->field("sum(SID_Weight) as sum_weight,SIM_CarNumber,SIM_Money,SIM_Person,SIM_fyphone,sum(NEWweight) as NEWweight,SIM_Num,m.SI_Num,SIM_Date")
            ->where("m.SI_Num",$ids)
            ->group("m.SIM_Num")
            ->order("m.SIM_Num")
            ->select();
            return json(["code"=>1,"data"=>$list]);
        }else{
            return json(["code"=>0,"msg"=>"空"]);
        }
        
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $SI_Num = $params["SI_Num"];
                if($SI_Num){
                    $one = $this->model->where("SI_Num",$SI_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("ym");
                    $one = $this->model->field("SI_Num")->where("SI_Num","LIKE",$month.'-%')->order("SI_Num DESC")->find();
                    if($one){
                        $num = substr($one["SI_Num"],5);
                        $SI_Num = $month.'-'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $SI_Num = $month.'-001';
                    // $month = date("Ymd");
                    // $one = $this->model->field("SI_Num")->where("SI_Num","LIKE","CK".$month.'-1%')->order("SI_Num DESC")->find();
                    // if($one){
                    //     $num = substr($one["SI_Num"],12);
                    //     $SI_Num = "CK".$month.'-1'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    // }else $SI_Num = "CK".$month.'-1001';
                }
                $params["SI_Num"] = $SI_Num;
                $params["Writer"] = $this->admin["nickname"];
                $sectSaveList = [];
                $msg = "";
                foreach($paramsTable["SIS_ID"] as $k=>$v){
                    $key = $paramsTable["SIS_ScdId"][$k];
                    if(isset($sectSaveList[$key])) $msg .= "第".($k+1)."行信息重复";
                    $sectSaveList[$key] = [
                        "SI_Num" => $params["SI_Num"],
                        "SIS_ID" => $v,
                        "SIS_ScdId" => $paramsTable["SIS_ScdId"][$k],
                        "SIS_HID" => $paramsTable["SIS_HID"][$k],
                        "SIS_TDID" => $paramsTable["SIS_TDID"][$k],
                        "SIS_TpNum" => $paramsTable["SIS_TpNum"][$k],
                        "SIS_Count" => $paramsTable["SIS_Count"][$k],
                        "SIS_Price" => $paramsTable["SIS_Price"][$k],
                        "SIS_Weight" => $paramsTable["SIS_Weight"][$k],
                        "SID_Memo" => $paramsTable["SID_Memo"][$k]
                    ];
                    if(!$v) unset($sectSaveList[$key]["SIS_ID"]);
                }
                if($msg) $this->error($msg);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        
                        $this->model->allowField(true)->save($params);
                        $result = $this->detailModel->allowField(true)->saveAll(array_values($sectSaveList));
                        
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                if ($result !== false) {
                    $this->success('成功！',null,$params["SI_Num"]);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig("js_way",1);
        return $this->view->fetch();
    }


    public function chooseDetail($c_num = null)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new Sectconfigdetail())->alias("scd")
                ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
                ->join(["taskdetail"=>"td"],"th.TD_ID = td.TD_ID")
                ->join(["task"=>"t"],"td.T_Num = t.T_Num")
                ->field("t.t_project,t.t_shortproject,td.TD_Pressure,scd.SCD_TPNum, 0 AS RK_Count,scd.SCD_Count AS SY_Count,scd.SCD_Count,trim(BOTH ',' FROM CONCAT(SCD_Part,',',SCD_SpPart)) as Part,scd.SCD_TPNum,t.T_Num,scd.TH_Height,td.TD_TypeName,(case when ifnull(SCD_ChangeWeight,0)=0 then scd.SCD_Weight else SCD_ChangeWeight end) as SCD_Weight,(case when ifnull(SCD_ChangeWeight,0)=0 then round(scd.SCD_Count*scd.SCD_Weight,2) else round(SCD_ChangeWeight*scd.SCD_Count,2) end) as SIS_Weight,scd.SCD_ID,th.TH_ID,td.TD_ID,round(scd.SCD_UnitPrice,2) as SCD_UnitPrice,(case when ifnull(SCD_ChangeWeight,0)=0 then round(scd.SCD_UnitPrice*scd.SCD_Weight,2) else round(SCD_ChangeWeight*scd.SCD_UnitPrice,2) end) as sum_price,round(SCD_Count*SCD_UnitPrice,2) as sum_price_unit")
                ->where($where)
                ->where("t.C_Num",$c_num)
                ->order($sort, $order)
                ->select();
            $pack_row = $this->model
                ->alias("m")
                ->join(["saleinvoicesingle"=>"sis"],"m.SI_Num = sis.SI_Num")
                ->field("SIS_ScdId,sum(SIS_Count) as sum_count")
                ->where("C_Num",$c_num)
                ->group("SIS_ScdId")
                ->select();
            $pack_list = $rows = [];
            foreach($pack_row as $k=>$v){
                $pack_list[$v["SIS_ScdId"]] = $v["sum_count"];
            }
            foreach($list as $k=>$v){
                $rows[$k] = $v->toArray();
                if(isset($pack_list[$v["SCD_ID"]])){
                    $c_count = $v["SCD_Count"]-$pack_list[$v["SCD_ID"]];
                    if($c_count<=0) unset($rows[$k]);
                    else{
                        $rows[$k]["SY_Count"] = $rows[$k]["SCD_Count"] = $c_count;
                        $rows[$k]["SIS_Weight"] = round($v["SCD_Count"]*$v["SCD_Weight"],2);
                        $rows[$k]["sum_price_unit"] = round($v["SCD_Count"]*$v["SCD_UnitPrice"],2);
                    }
                }
            }
            $result = array("total" => count($rows), "rows" => array_values($rows));

            return json($result);
        }
        $this->assignconfig("C_Num",$c_num);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->alias("m")
            ->join(["compact"=>"c"],"m.C_Num = c.C_Num")
            ->join(["task"=>"t"],"c.C_Num = t.C_Num")
            ->field("m.*,c.C_Num,c.C_Project,group_concat(t.T_Num) as T_Num,c.Customer_Name")
            ->where("m.SI_Num",$ids)
            ->group("t.C_Num")
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $sectSaveList = [];
                $msg = "";
                foreach($paramsTable["SIS_ID"] as $k=>$v){
                    $key = $paramsTable["SIS_ScdId"][$k];
                    if(isset($sectSaveList[$key])) $msg .= "第".($k+1)."行信息重复";
                    $sectSaveList[$key] = [
                        "SIS_ID" => $v,
                        "SI_Num" => $ids,
                        "SIS_ScdId" => $paramsTable["SIS_ScdId"][$k],
                        "SIS_HID" => $paramsTable["SIS_HID"][$k],
                        "SIS_TDID" => $paramsTable["SIS_TDID"][$k],
                        "SIS_TpNum" => $paramsTable["SIS_TpNum"][$k],
                        "SIS_Count" => $paramsTable["SIS_Count"][$k],
                        "SIS_Price" => $paramsTable["SIS_Price"][$k],
                        "SIS_Weight" => $paramsTable["SIS_Weight"][$k],
                        "SID_Memo" => $paramsTable["SID_Memo"][$k]
                    ];
                    if(!$v) unset($sectSaveList[$key]["SIS_ID"]);
                }
                if($msg) $this->error($msg);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        $this->model->allowField(true)->save($params,["SI_Num"=>$ids]);
                        $result = $this->detailModel->allowField(true)->saveAll(array_values($sectSaveList));
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $this->detailModel->alias("sis")
            ->join(["sectconfigdetail"=>"scd"],"sis.SIS_ScdID = scd.SCD_ID")
            ->join(["taskdetail"=>"td"])
            ->field("sis.SIS_ID,sis.SIS_ScdId,sis.SIS_HID,sis.SIS_TDID,TD_TypeName,TD_Pressure,TH_Height,SIS_TpNum,SIS_ID,SIS_ScdId,SIS_ID,SIS_ScdId,SIS_Count,scd.SCD_Weight as weight,SIS_Price,".($row["unit"]=="kg"?"round(SCD_Weight*SIS_Price,2) as sum_price":"round(SIS_Count*SIS_Price,2) as sum_price").",round(SIS_Count*SIS_Price,2) as sum_price_unit,SIS_Weight,SID_Memo")
            ->where("sis.SI_Num",$ids)
            ->order("SCD_ID")
            ->select();
        $weight_1 = $weight_2 = $count = $price = 0;
        foreach($list as $v){
            $weight_1 += $v["weight"];
            $weight_2 += $v["SIS_Weight"];
            $count += $v["SIS_Count"];
            $price += $v["sum_price"];
        }
        $row = array_merge($row,[
            "weight" => round($weight_1,2),
            "SIS_Weight" => round($weight_2,2),
            "SIS_Count" => $count,
            "sum_price" => round($price,2)
        ]);
        $tableField = $this->getTableField();
        $tableField[11][5] = $row["unit"]==1?"sum_price":"sum_price_unit";
        $this->view->assign("row",$row);
        $this->view->assign("list",$list);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig('ids',$ids);
        $this->assignconfig("js_way", $row["unit"]);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
            {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $one = $this->mainModel->where("SI_Num",$ids)->find();
            if($one) $this->error("已发运，无法删除！");

            $count = 0;
            Db::startTrans();
            try {
                $count += $this->model->where("SI_Num",$ids)->delete();
                $count += $this->detailModel->where("SI_Num",$ids)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function detailAdd($ids = "")
    {
        if (!$ids) {
            $this->error(__('No Results were found'));
        }
        $row = $this->model->alias("m")->join(["compact"=>"c"],"m.C_Num = c.C_Num")->field("m.*,c.C_Project")->where("SI_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        list($left_row,$pack_row) = $this->_getLeftRightRow($ids);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and !empty($paramsTable["choose"])) {
                $params = $this->preExcludeFields($params);
                $SIM_Num = $params["SIM_Num"];
                if($SIM_Num){
                    $one = $this->mainModel->where("SIM_Num",$SIM_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("Y");
                    $one = $this->mainModel->field("SIM_Num")->where("SIM_Num","LIKE","FY".$month.'%')->order("SIM_Num DESC")->find();
                    if($one){
                        $num = substr($one["SIM_Num"],6);
                        $SIM_Num = 'FY'.$month.str_pad(++$num,4,0,STR_PAD_LEFT);
                    }else $SIM_Num = 'FY'.$month.'0001';
                }
                $params["SIM_Num"] = $SIM_Num;
                $params["Writer"] = $this->admin["nickname"];
                $sectSaveList = $bsc_list = [];
                $msg = "";
                foreach($paramsTable["choose"] as $k=>$v){
                    $content = $pack_row[$v];
                    if(!isset($content["SID_ID"])) return json(["code"=>0,"msg"=>"库存不足"]);
                    if($content["BSO_Count"]<=0) $msg .= "包名为".$v."的库存不足<br>";
                    if($content["need_count"]<=0) $msg .= "包名为".$v."的不需要发运<br>";
                    $sectSaveList[$k] = [
                        "SID_ID" => $content["SID_ID"],
                        "SI_Num" => $params["SIM_Num"],
                        "SID_Count" => $content["need_count"],
                        "SID_Weight" => $content["PD_sumWeight"],
                        "SIS_ID" => $content["SIS_ID"],
                        "BSO_ID" => $content["BSO_ID"]
                    ];
                    $bsc_list[$k] = [
                        "BSO_ID" => $content["BSO_ID"],
                        "BSO_Count" => $content["BSO_Count"] - $content["need_count"],
                    ];
                    if(!$content["SID_ID"]) unset($sectSaveList[$k]["SID_ID"]);
                }
                if($msg) $this->error($msg);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        
                        $this->mainModel->allowField(true)->save($params);
                        $result = $this->ddetailModel->allowField(true)->saveAll(array_values($sectSaveList));
                        $result = (new BsoCount())->allowField(true)->saveAll(array_values($bsc_list));
                        
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                if ($result !== false) {
                    $this->success('成功！',null,$SIM_Num);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("left_row",$left_row);
        $this->view->assign("row",$row);
        $this->view->assign("pack_row",$pack_row);
        $leftTableField = $this->getLeftTableField();
        $rightTableField = $this->getRightTableField();
        $this->view->assign("leftTableField",$leftTableField);
        $this->view->assign("rightTableField",$rightTableField);
        return $this->view->fetch();
    }

    public function detailEdit($ids = "")
    {
        $row = $this->mainModel->alias("m")->join(["saleinvoice"=>"si"],"m.SI_Num = si.SI_Num")->join(["compact"=>"c"],"si.C_Num = c.C_Num")->field("m.*,c.C_Project,si.*")->where("SIM_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        list($left_row,$pack_row) = $this->_getLeftRightRow($row["SI_Num"]);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params) {
                $sectSaveList = $bsc_list = [];
                $msg = "";
                if(!empty($paramsTable["choose"])){
                    foreach($paramsTable["choose"] as $k=>$v){
                        if(!$v) continue;
                        $content = $pack_row[$v];
                        if($content["BSO_Count"]<=0) $msg .= "包名为".$v."的库存不足<br>";
                        if($content["need_count"]<=0) $msg .= "包名为".$v."的不需要发运<br>";
                        $sectSaveList[$k] = [
                            "SID_ID" => $content["SID_ID"],
                            "SI_Num" => $ids,
                            "SID_Count" => $content["need_count"],
                            "SID_Weight" => $content["PD_sumWeight"],
                            "SIS_ID" => $content["SIS_ID"],
                            "BSO_ID" => $content["BSO_ID"]
                        ];
                        $bsc_list[$k] = [
                            "BSO_ID" => $content["BSO_ID"],
                            "BSO_Count" => $content["BSO_Count"] - $content["need_count"],
                        ];
                        if(!$content["SID_ID"]) unset($sectSaveList[$k]["SID_ID"]);
                    }
                }
                if($msg) $this->error($msg);
                $result = false;
                
                Db::startTrans();
                try {
                    
                    $result = $this->mainModel->allowField(true)->save($params,["SIM_Num"=>$ids]);
                    if(!empty($sectSaveList)){
                        $this->ddetailModel->allowField(true)->saveAll(array_values($sectSaveList));
                        (new BsoCount())->allowField(true)->saveAll(array_values($bsc_list));
                    }
                    
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $this->view->assign("left_row",$left_row);
        $this->view->assign("row",$row);
        $this->view->assign("pack_row",$pack_row);
        $leftTableField = $this->getLeftTableField();
        $rightTableField = $this->getRightTableField();
        $this->view->assign("leftTableField",$leftTableField);
        $this->view->assign("rightTableField",$rightTableField);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->model->where("SI_Num",$num)->find();
        if($row["Auditor"]) return json(["code"=>0,"msg"=>"已审核"]);
        $update = [
            "Auditor" => $this->admin["nickname"],
            "Auditordate" => date("Y-m-d H:i:s")
        ];
        $result = $this->model->update($update,["SI_Num"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"弃审失败，请稍后重试"]);
        $row = $this->model->where("SI_Num",$num)->find();
        if(!$row["Auditor"]) return json(["code"=>0,"msg"=>"已弃审"]);

        $update = [
            "Auditor" => '',
            "Auditordate" => "0000-00-00 00:00:00"
        ];
        $result = $this->model->update($update,["SI_Num"=>$num]);
        if ($result) {
            $this->success("弃审成功！");
        } else {
            $this->error("弃审失败！");
        }
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败"]);
        $one = $this->model->alias("m")->join(["saleinvoicesingle"=>"sis"],"m.SI_Num = sis.SI_Num")->where("sis.SIS_ID",$num)->find();
        if(!$one) return json(["code"=>0,"msg"=>"删除失败"]);
        else if($one["Auditor"]) return json(["code"=>0,"msg"=>"已审核，无法删除"]);
        else{
            $main_one = $this->mainModel->where("SI_Num",$one["SI_Num"])->find();
            if($main_one)  return json(["code"=>0,"msg"=>"已发运，删除失败"]);
            $result = $this->detailModel->where("SIS_ID",$num)->delete();
            if($result){
                // (new BsoCount())->where("SCD_ID",$one["SIS_ScdId"])->delete();
                return json(["code"=>1,"msg"=>"成功"]);
            }
            else return json(["code"=>0,"msg"=>"删除失败"]);
        }
    }

    public function ctrlzContent()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"撤销失败"]);
        $one = $this->ddetailModel->where("SID_ID",$num)->find();
        if(!$one) return json(["code"=>0,"msg"=>"撤销失败"]);
        $one = $this->ddetailModel->where("SID_ID",$num)->find();
        $bsc_one = (new BsoCount())->get($one["BSO_ID"]);
        $result = 0;
        Db::startTrans();
        try {
            $result = $this->ddetailModel->where("SID_ID",$num)->delete();
            $bsc_one->save(["BSO_Count"=>$one["SID_Count"]+$bsc_one["BSO_Count"]]);
            
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $this->success();
        } else {
            $this->error(__('No rows were updated'));
        }
    }

    public function detailDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败"]);
        $one = $this->mainModel->where("SIM_Num",$num)->find();
        if(!$one) return json(["code"=>0,"msg"=>"删除失败"]);
        else if($one["Auditor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);
        else{
            $detail_list = $this->ddetailModel->where("SI_Num",$num)->select();
            $bso_list = [];
            foreach($detail_list as $k=>$v){
                $bso_list[$v["BSO_ID"]] = ["BSO_ID"=>$v["BSO_ID"],"BSO_Count"=>1];
            }
            $result = false;
            Db::startTrans();
            try {
                $result = $this->mainModel->where("SIM_Num",$num)->delete();
                $result = $this->ddetailModel->where("SI_Num",$num)->delete();
                $result = (new BsoCount())->allowField(true)->saveAll(array_values($bso_list));
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were updated'));
            }
        }
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["ID","SIS_ID","readonly","","hidden","SIS_ID"],
            ["ScdId","SIS_ScdId","readonly","","hidden","SCD_ID"],
            ["HID","SIS_HID","readonly","","hidden","TH_ID"],
            ["TDID","SIS_TDID","readonly","","hidden","TD_ID"],
            ["塔型","TD_TypeName","readonly","","","TD_TypeName"],
            ["电压","TD_Pressure","readonly","","","TD_Pressure"],
            ["呼高","TH_Height","readonly","","","TH_Height"],
            ["杆塔号","SIS_TpNum","readonly","","","SCD_TPNum"],
            ["发货数量","SIS_Count","",1,"","SCD_Count"],
            ["单净重","weight","readonly",0,"","SCD_Weight"],
            ["单价","SIS_Price","readonly",0,"","SCD_UnitPrice"],
            ["总价","sum_price","readonly",0,"hidden","sum_price"],
            ["总重(可修改)","SIS_Weight","",0,"","SIS_Weight"],
            ["备注","SID_Memo","","","","SID_Memo"]
        ];
        return $list;
    }

    //编辑table
    public function getLeftTableField()
    {
        $list = [
            ["塔型","TD_TypeName"],
            ["呼高","TH_Height"],
            ["杆塔","SCD_TPNum"],
            ["数量","SCD_Count"]
        ];
        return $list;
    }
    //编辑table
    public function getRightTableField()
    {
        $list = [
            ["SID_ID","SID_ID","hidden"],
            ["杆塔","SCD_TPNum",""],
            ["捆号","PD_Name",""],
            ["库存捆数","BSO_Count",""],
            ["已发运捆","SID_Count",""],
            ["需发运捆","need_count",""],
            ["最长件号长(mm)","MaxLength",""],
            // ["单重(kg)","PD_singleWeight",""],
            ["总重(kg)","PD_sumWeight",""]
        ];
        return $list;
    }

    public function print($ids = null)
    {
        $row = $this->model->alias("m")
            ->join(["compact"=>"c"],"m.C_Num = c.C_Num")
            ->join(["task"=>"t"],"c.C_Num = t.C_Num")
            ->field("m.*,c.C_Num,c.C_Project,group_concat(t.T_Num) as T_Num,c.Customer_Name")
            ->where("m.SI_Num",$ids)
            ->group("t.C_Num")
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        
        $list = $this->detailModel->alias("sis")
            ->join(["sectconfigdetail"=>"scd"],"sis.SIS_ScdID = scd.SCD_ID")
            ->join(["taskdetail"=>"td"])
            ->field("sis.SIS_ID,sis.SIS_ScdId,sis.SIS_HID,sis.SIS_TDID,TD_TypeName,TD_Pressure,TH_Height,(case when TD_TypeName=SIS_TpNum then '' else SIS_TpNum end) as SIS_TpNum ,SIS_ID,SIS_ScdId,SIS_ID,SIS_ScdId,SIS_Count,scd.SCD_Weight as weight, scd.SCD_Unit,SIS_Price,round(SCD_Weight*SIS_Price,2) as sum_price,SIS_Weight,SID_Memo")
            ->where("sis.SI_Num",$ids)
            ->order("SCD_ID")
            ->select();
        $row['SI_Date'] = date("Y-m-d", strtotime($row["SI_Date"]));
        $this->assignconfig('row',$row);
        $this->assignconfig('list',$list);
        return $this->view->fetch();
    }
    
    public function detailPrintMain($ids = "")
    {
        $one = $this->mainModel->get($ids);
        if(!$one) $this->error(__('No Results were found'));
        // $card_list = [""=>"全部"];
        $card_list = [];
        $list = $this->mainModel->where("SI_Num",$one["SI_Num"])->order("SIM_Num")->select();
        foreach($list as $v){
            $card_list[$v["SIM_Num"]] = $v["SIM_CarNumber"];
        }
        $this->view->assign("card_list",$card_list);
        $this->view->assign("ids",$ids);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function detailPrint($ids = "")
    {
        $row = $this->mainModel->alias("m")->join(["saleinvoice"=>"si"],"m.SI_Num = si.SI_Num")->join(["compact"=>"c"],"si.C_Num = c.C_Num")->field("m.*,c.C_Project,si.*")->where("SIM_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $list = $this->model->alias("si")
            ->join(["saleinvoicesingle"=>"sis"],"si.SI_Num=sis.SI_Num")
            ->join(["pack"=>"p"],"p.SCD_ID=sis.SIS_ScdId")
            ->join(["packdetail"=>"pd"],"p.P_ID=pd.P_ID")
            ->join(["bsocount"=>"bc"],["pd.PD_Name=bc.PD_Name","bc.SCD_ID=p.SCD_ID"])
            ->join(["sectconfigdetail"=>"scd"],"sis.SIS_ScdId = scd.SCD_ID")
            ->join(["saleinvoicedetail"=>"sid"],"sid.BSO_ID=bc.BSO_ID")
            ->field("scd.*,pd.PD_Name,p.SCD_ID,bc.BSO_Count,ifnull(sid.SID_Count,0) as SID_Count,sis.SIS_Count,(case when PD_sumWeight=0 then sis.SIS_Weight else PD_sumWeight end) as PD_sumWeight,pd.MaxLength,CAST(right(pd.PD_Name,locate('-',reverse(pd.PD_Name),1)-1) AS UNSIGNED) as number,bc.BSO_ID,IFNULL(sid.SID_ID,0) as SID_ID, cast(substring_index(pd.PD_Name, '-', -1) as UNSIGNED) bianhao,(case when SCD_ProductName='铁塔' then concat(TD_TypeName,',',scd.TH_Height,',',pd.PD_Name) else TD_TypeName end) as bh")
            ->where("sid.SI_Num",$ids)
            ->group("pd.PD_ID")
            // ->order('SCD_TPNum asc')
            ->order('p.SCD_ID,bianhao asc')
            ->select();
        // $list = $this->mainModel->alias("m")
        //     ->join(["saleinvoicedetail"=>"sid"],"m.SIM_Num = sid.SI_Num")
        //     ->join(["saleinvoicesingle"=>"sis"],"sis.SIS_ID = sid.SIS_ID")
        //     ->join(["sectconfigdetail"=>"scd"],"sis.SIS_ScdId = scd.SCD_ID")
        //     ->join(["bsocount"=>"bc"],"sid.BSO_ID = bc.BSO_ID")
        //     ->join(["pack"=>"p"],"p.SCD_ID = bc.SCD_ID")
        //     ->join(["packdetail"=>"pd"],"p.P_ID = pd.P_ID")
        //     ->field("m.*, sid.*, sis.*, scd.*, bc.*, pd.*, cast(substring_index(pd.PD_Name, '-', -1) as UNSIGNED) bianhao")
        //     ->where("m.SIM_Num",$ids)
        //     ->group("pd.PD_ID")
        //     ->order('SCD_TPNum asc')
        //     ->order('bianhao asc')
        //     ->select();
        $row["SI_Date"] = date("Y年m月d日", strtotime($row["SI_Date"]));
        $row["WriterDate"] = date("Y年m月d日", strtotime($row["WriterDate"]));
        $this->assignconfig('row',$row);
        $this->assignconfig('list',$list);
        // $this->assignconfig('pack_list',$pack_list);

        return $this->view->fetch();
    }

    /**
     * 选择合同
     */
    public function selectCompact()
    {
        //当前是否为关联查询
        $this->relationSearch = false;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new Task())->alias('t')
                    ->join([ 'compact'=>'c' ],'t.C_Num=c.C_Num')
                    ->where($where)
                    ->group("c.C_Num")
                    ->order("C_WriteDate desc")
                    // ->select(false);
                    ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    protected function _getLeftRightRow($ids)
    {
        $left_row = $pack_row = [];
        $left_list = $this->detailModel->alias("d")
            ->join(["sectconfigdetail"=>"scd"],"d.SIS_ScdId=scd.SCD_ID")
            ->join(["taskdetail"=>"td"],"d.SIS_TDID = td.TD_ID")
            ->field("td.TD_TypeName,scd.TH_Height,scd.SCD_TPNum,scd.SCD_Count,d.SIS_ScdId,d.SIS_ID,d.SIS_Count")
            ->where("d.SI_Num",$ids)
            ->order("scd.SCD_TPNum")->select();
        foreach($left_list as $k=>$v){
            $left_row[$v["SIS_ScdId"]] = [
                "TD_TypeName"=>$v["TD_TypeName"],
                "TH_Height"=>$v["TH_Height"],
                "SCD_TPNum"=>$v["SCD_TPNum"],
                "SCD_Count"=>$v["SCD_Count"],
                "SIS_ID"=>$v["SIS_ID"],
                "SIS_Count"=>$v["SIS_Count"]
            ];
        }
        $bsoPackList = (new BsoCount())->alias("bc")
            ->join(["packdetail"=>"pd"],"bc.PD_Name=pd.PD_Name")
            ->join(["pack"=>"p"],["p.P_ID=pd.P_ID","bc.SCD_ID=p.SCD_ID"])
            ->field("pd.PD_Name,p.SCD_ID,bc.BSO_Count,CAST(right(pd.PD_Name,locate('-',reverse(pd.PD_Name),1)-1) AS UNSIGNED) as number,pd.MaxLength,CAST(right(pd.PD_Name,locate('-',reverse(pd.PD_Name),1)-1) AS UNSIGNED) as number,bc.BSO_ID,PD_sumWeight, 0 as 'SID_Count', 0 as 'SIS_Count',0 as 'SID_ID'")
            ->where("bc.SCD_ID","IN",array_keys($left_row))
            ->order("bc.SCD_ID,number")
            ->select();
        foreach($bsoPackList as $v){
            $pack_row[$v["BSO_ID"]] = $v->toArray();
            $pack_row[$v["BSO_ID"]]["SCD_TPNum"] = $left_row[$v["SCD_ID"]]["SCD_TPNum"];
            $pack_row[$v["BSO_ID"]]["SIS_ID"] = $left_row[$v["SCD_ID"]]["SIS_ID"];
            $pack_row[$v["BSO_ID"]]["need_count"] = $left_row[$v["SCD_ID"]]["SIS_Count"];
        }
        $pack_list = $this->model->alias("si")
            ->join(["saleinvoicesingle"=>"sis"],"si.SI_Num=sis.SI_Num")
            ->join(["saleinvoicemain"=>"sim"],"sim.SI_Num=sis.SI_Num","left")
            ->join(["saleinvoicedetail"=>"sid"],["sim.SIM_Num=sid.SI_Num","sis.SIS_ID=sid.SIS_ID"],"left")
            ->field("ifnull(sid.SID_Count,0) as SID_Count,sis.SIS_Count,sis.SIS_Weight,IFNULL(sid.SID_ID,0) as SID_ID,BSO_ID")
            ->where("si.SI_Num",$ids)
            ->select();
        foreach($pack_list as $k=>$v){
            if(isset($pack_row[$v["BSO_ID"]])){
                $pack_row[$v["BSO_ID"]]["PD_sumWeight"]?"":$pack_row[$v["BSO_ID"]]["PD_sumWeight"] = $v["SIS_Weight"];
                $pack_row[$v["BSO_ID"]]["SID_Count"] = $v["SID_Count"];
                $pack_row[$v["BSO_ID"]]["SIS_Count"] = $v["SIS_Count"];
                $pack_row[$v["BSO_ID"]]["SID_ID"] = $v["SID_ID"];
                $pack_row[$v["BSO_ID"]]["need_count"] = round($v["SIS_Count"]-$v["SID_Count"]);
            }
        }
        return [$left_row,$pack_row];
    }
}
