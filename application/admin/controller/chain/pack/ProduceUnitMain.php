<?php

namespace app\admin\controller\chain\pack;

use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\model\chain\pack\BsoCount;
use app\admin\model\chain\pack\Pack;
use app\admin\model\chain\sale\Sectconfigdetail;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use ReflectionFunction;

/**
 * 成品入库
 *
 * @icon fa fa-circle-o
 */
class ProduceUnitMain extends Backend
{
    
    /**
     * ProduceUnitMain模型对象
     * @var \app\admin\model\chain\pack\ProduceUnitMain
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\pack\ProduceUnitMain;
        $this->detailModel = new \app\admin\model\chain\pack\ProduceUnitIn;
        $this->admin = \think\Session::get('admin');
        $this->vendList = $this->vendorNameList(0,["VC_Num"=>["=","06"]]);
        $this->view->assign("vend_list",$this->vendList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $func = new ReflectionFunction($where); 
            $where_content = $func->getStaticVariables();
            if(count($where_content["where"])==0) $list = [];
            else  $list = $this->model->alias("pum")
                ->join(["produceunitin"=>"pui"],"pum.PUM_Num = pui.PUM_Num")
                ->join(["taskdetail"=>"td"],"pum.TD_ID = td.TD_ID")
                ->join(["task"=>"t"],"td.T_Num = t.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->field("pum.*,c.Customer_Name,c.C_Num,t.T_Num,t.t_project,c.PC_Num,td.TD_TypeName,sum(pui.PU_Count) AS count,sum(pui.PU_Weight) as weight,pui.PU_PRICENS")
                ->group("pum.PUM_Num")
                ->where($where)
                ->order($sort, $order)
                ->order("pum.PUM_Num desc")
                ->select();
            $data = [];
            $count = $weight = 0;
            foreach($list as $k=>$v){
                $data[$k] = $v->toArray();
                $count += $v["count"];
                $weight += $v["weight"];
            }
            $data[] = ["PUM_Num"=>"合计","count"=>$count,"weight"=>round($weight,2)];
            $result = array("total" => count($list), "rows" => array_values($data));

            return json($result);
        }
        // $defaultTime = date("Y-m-1 00:00:00").' - '.date("Y-m-d 23:59:59");
        // $this->assignconfig("defaultTime",$defaultTime);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $params["Writer"] = $this->admin["nickname"];
                $PUM_Num = $params["PUM_Num"];
                if($PUM_Num){
                    $one = $this->model->where("PUM_Num",$PUM_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{

                    $month = date("Y");
                    $one = $this->model->field("PUM_Num")->where("PUM_Num","LIKE","RC".$month.'%')->order("PUM_Num DESC")->find();
                    if($one){
                        $num = substr($one["PUM_Num"],6);
                        $PUM_Num = 'RC'.$month.str_pad(++$num,4,0,STR_PAD_LEFT);
                    }else $PUM_Num = 'RC'.$month.'0001';

                    // $month = date("Ymd");
                    // $one = $this->model->field("PUM_Num")->where("PUM_Num","LIKE","RK".$month.'-1%')->order("PUM_Num DESC")->find();
                    // if($one){
                    //     $num = substr($one["PUM_Num"],12);
                    //     $PUM_Num = 'RC'.$month.'-1'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    // }else $PUM_Num = 'RC'.$month.'-1001';
                }
                $params["PUM_Num"] = $PUM_Num;
                $msg = "";
                $sectSaveList = $scd_id_list = [];
                foreach($paramsTable["PU_ID"] as $k=>$v){
                    $key = $paramsTable["SCD_ID"][$k];
                    if(isset($sectSaveList[$key])) $msg .= "第".($k+1)."行杆塔重复入库<br>";
                    if($paramsTable["jc_count"][$k]>$paramsTable["count"][$k]) $msg .= "第".($k+1)."行入库数量大于杆塔数量<br>";
                    $sectSaveList[$key] = [
                        "SCD_ID" => $key,
                        "PU_ID" => $v,
                        "PU_Count" => $paramsTable["jc_count"][$k],
                        "PU_Weight" => $paramsTable["PU_Weight"][$k]/($paramsTable["count"][$k]-$paramsTable["PU_Count"][$k])*$paramsTable["jc_count"][$k],
                        "PU_Memo" => $paramsTable["PU_Memo"][$k],
                        "PUM_Num" => $params["PUM_Num"],
                        "boltweight" => $paramsTable["boltweight"][$k]
                    ];
                    if(!$v){
                        unset($sectSaveList[$key]["PU_ID"]);
                        $scd_id_list[$key] = $paramsTable["TH_ID"][$k];
                    }
                }
                if($msg) $this->error($msg);

                $bso_rows = [];
                if(!empty($scd_id_list)){
                    $bso_list = (new Pack())->alias("p")
                        ->join(["packdetail"=>"pd"],"p.P_ID = pd.P_ID")
                        ->field("PD_Name,1 as BSO_Count,TD_ID,SCD_ID,0 as TH_ID")
                        ->where("SCD_ID","IN",array_keys($scd_id_list))
                        ->order("PD_Name asc")
                        ->select();
                    foreach($bso_list as $k=>$v){
                        $bso_rows[$k] = $v->toArray();
                        $bso_rows[$k]["TH_ID"] = $scd_id_list[$v["SCD_ID"]];
                        $bso_rows[$k]["BSO_Count"] = $sectSaveList[$v["SCD_ID"]]["PU_Count"];
                    }
                }
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        
                        $this->model::create($params);
                        $result = $this->detailModel->allowField(true)->saveAll(array_values($sectSaveList));
                        if(!empty($bso_rows)) (new BsoCount())->allowField(true)->saveAll(array_values($bso_rows));
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                
                if ($result !== false) {
                    $this->success('成功！',null,$params["PUM_Num"]);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        return $this->view->fetch();
    }

    public function chooseDetail($td_id = null)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new Sectconfigdetail())->alias("scd")
                ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
                ->join(["taskdetail"=>"td"],"th.TD_ID = td.TD_ID")
                ->join(["task"=>"t"],"td.T_Num = t.T_Num")
                ->field("scd.TH_ID,t.t_project,t.t_shortproject,td.TD_Pressure,scd.SCD_TPNum, 0 AS RK_Count,scd.SCD_Count AS SY_Count,scd.SCD_Count,trim(BOTH ',' FROM CONCAT(SCD_Part,',',SCD_SpPart)) as Part,scd.SCD_TPNum,t.T_Num,scd.TH_Height,td.TD_TypeName,(CASE WHEN ifnull(scd.SCD_ChangeWeight,0)=0 then scd.SCD_Weight else scd.SCD_ChangeWeight end) as SCD_Weight,scd.SCD_ID,th.TH_ID,td.TD_ID")
                ->where($where)
                ->where("td.TD_ID",$td_id)
                ->order($sort, $order)
                ->select();
            $pack_row = $this->model->alias("m")->join(["produceunitin"=>"pui"],"m.PUM_Num = pui.PUM_Num")->field("SCD_ID,SUM(PU_Count) AS PU_Count")->where("TD_ID",$td_id)->group("SCD_ID")->select();
            $pack_list = $rows = [];
            foreach($pack_row as $k=>$v){
                $pack_list[$v["SCD_ID"]] = $v["PU_Count"];
            }
            foreach($list as $k=>$v){
                $rows[$v["SCD_ID"]] = $v->toArray();
                if(isset($pack_list[$v["SCD_ID"]])){
                    $rows[$v["SCD_ID"]]["RK_Count"] = $pack_list[$v["SCD_ID"]];
                    $rows[$v["SCD_ID"]]["SY_Count"] -= $pack_list[$v["SCD_ID"]];
                }
                if($rows[$v["SCD_ID"]]["SY_Count"]<=0) unset($rows[$v["SCD_ID"]]);
            }
            $end_row = [];
            $scd_list = (new Pack())->field("SCD_ID")->where("SCD_ID","IN",array_keys($rows))->where("Auditor","<>","")->select();
            foreach($scd_list as $v){
                if(isset($rows[$v["SCD_ID"]])) $end_row[$v["SCD_ID"]] = $rows[$v["SCD_ID"]];
            }
            $result = array("total" => count($end_row), "rows" => array_values($end_row));

            return json($result);
        }
        $tableField = $this->getTableField();
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig("td_id",$td_id);
        return $this->view->fetch();
    }

    public function selectDetail()
    {
         //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\sale\Compact)->alias("c")
                ->join(["task"=>"t"],"t.C_Num=c.C_Num")
                ->join(["taskdetail"=>"d"],"t.T_Num = d.T_Num")
                ->join(["taskheight"=>"th"],"d.TD_ID = th.TD_ID")
                ->field("d.TD_ID,d.T_Num,d.P_Name,d.P_Num,d.TD_TypeName,d.TD_Pressure,c.PC_Num,c.C_Num,c.C_Project,c.C_Company,c.Customer_Name,c.C_SortProject,c.C_SaleMan,SUM(TD_Count*TH_Weight) as PT_sumWeight,sum(TD_Count) as T_Count,sum(TD_Count) as PT_sumCount,'未打包' as pack_state,'未下发' as fy_state,'未入库' as unit_state")
                ->where($where)
                ->group("d.TD_ID")
                ->order($sort,$order)
                ->paginate($limit);
            $rows = [];
            foreach($list as $v){
                $v["PT_sumWeight"] = round($v["PT_sumWeight"],2);
                $rows[$v["TD_ID"]] = $v->toArray();
            }
            $td_id_list = array_keys($rows);
            $unitList = $this->model->field("TD_ID")->where("TD_ID","in",$td_id_list)->select();
            foreach($unitList as $v){
                $rows[$v["TD_ID"]]["unit_state"] = "存在入库";
            }
            $stateList = (new Dtmaterial())->field("TD_ID")->where("TD_ID","in",$td_id_list)->select();
            foreach($stateList as $v){
                $rows[$v["TD_ID"]]["fy_state"] = "已下发";
            }
            $stateList = (new \app\admin\model\chain\lofting\MergTypeName())->field("TD_ID")->where("TD_ID","in",$td_id_list)->select();
            foreach($stateList as $v){
                $rows[$v["TD_ID"]]["fy_state"] = "已下发";
            }
            $pack_stateList = (new Pack())->field("TD_ID")->where("TD_ID","in",$td_id_list)->select();
            foreach($pack_stateList as $v){
                $rows[$v["TD_ID"]]["pack_state"] = "已打包";
            }
            $result = array("total" => $list->total(), "rows" => array_values($rows));

            return json($result);
        }
        return $this->view->fetch();
     }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->alias("pum")
            ->join(["taskdetail"=>"td"],"pum.TD_ID = td.TD_ID")
            ->join(["task"=>"t"],"td.T_Num = t.T_Num")
            ->field("pum.*,t.t_project,td.TD_TypeName")
            ->where("pum.PUM_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $msg = "";
                $sectSaveList = [];
                foreach($paramsTable["PU_ID"] as $k=>$v){
                    $key = $paramsTable["SCD_ID"][$k];
                    if(isset($sectSaveList[$key])) $msg .= "第".($k+1)."行杆塔重复入库<br>";
                    $sectSaveList[$key] = [
                        "SCD_ID" => $key,
                        "PU_ID" => $v,
                        "PU_Count" => $paramsTable["jc_count"][$k],
                        "PU_Weight" => ($paramsTable["PU_Count"][$k])?($paramsTable["PU_Weight"][$k]/($paramsTable["PU_Count"][$k])*$paramsTable["jc_count"][$k]):0,
                        "PU_Memo" => $paramsTable["PU_Memo"][$k],
                        "PUM_Num" => $ids,
                        "boltweight" => $paramsTable["boltweight"][$k]
                    ];
                    if(!$v){
                        $sectSaveList[$key]["PU_Weight"] = $paramsTable["PU_Weight"][$k]/($paramsTable["count"][$k]-$paramsTable["PU_Count"][$k])*$paramsTable["jc_count"][$k];
                        unset($sectSaveList[$key]["PU_ID"]);
                        $scd_id_list[$key] = $paramsTable["TH_ID"][$k];
                    }
                }
                if($msg) $this->error($msg);

                $bso_rows = [];
                if(!empty($scd_id_list)){
                    $bsoModel = new BsoCount();
                    $exist_bso_list = $bsoModel->where("SCD_ID","IN",array_keys($scd_id_list))->column("SCD_ID");
                    if($exist_bso_list) $this->error("已存在入库情况，请仔细核对");
                    $bso_list = (new Pack())->alias("p")
                        ->join(["packdetail"=>"pd"],"p.P_ID = pd.P_ID")
                        ->field("PD_Name,1 as BSO_Count,TD_ID,SCD_ID,0 as TH_ID")
                        ->where("SCD_ID","IN",array_keys($scd_id_list))
                        ->order("PD_Name asc")
                        ->select();
                    foreach($bso_list as $k=>$v){
                        $bso_rows[$k] = $v->toArray();
                        $bso_rows[$k]["TH_ID"] = $scd_id_list[$v["SCD_ID"]];
                        $bso_rows[$k]["BSO_Count"] = $sectSaveList[$v["SCD_ID"]]["PU_Count"];
                    }
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("PUM_Num",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    if(!empty($bso_rows)) $bsoModel->allowField(true)->saveAll(array_values($bso_rows));
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $this->detailModel->alias("pui")
            ->join(["sectconfigdetail"=>"scd"],"scd.SCD_ID = pui.SCD_ID")
            ->field("pui.PU_ID,scd.SCD_ID,scd.TD_TypeName,scd.TH_ID,scd.TH_Height,scd.SCD_TPNum,scd.SCD_Count as count,PU_Count,PU_Count as jc_count,PU_Weight,pui.boltweight,PU_Weight as z_PU_Weight,pu_price,PU_Memo")
            ->where("pui.PUM_Num",$ids)
            ->order("SCD_ID")
            ->select();
        $weight_1 = $weight_2 = $count = 0;
        foreach($list as $v){
            $weight_1 += $v["PU_Weight"];
            $weight_2 += $v["boltweight"];
            $count += $v["PU_Count"];
        }
        $row["count"] = $count;
        $row["PU_Weight"] = round($weight_1,2);
        $row["boltweight"] = round($weight_2,2);
        $row["category"] = $row["category"]?$row["category"]:"铁塔";
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("list",$list);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
            {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $one = $this->model->alias("pum")
                ->join(["produceunitin"=>"pui"],"pum.PUM_Num = pui.PUM_Num")
                ->join(["bsocount"=>"bc"],"bc.SCD_ID = pui.SCD_ID")
                ->where(["pum.PUM_Num"=>["=",$ids],"bc.BSO_Count"=>["=",0]])
                ->find();
            if($one) $this->error("已发运！");
            $this_one = $this->model->get($ids);
            if(!$this_one) $this->error("不存在该信息！");
            $count = 0;
            Db::startTrans();
            try {
                $count += $this->model->where("PUM_Num",$ids)->delete();
                $count += $this->detailModel->where("PUM_Num",$ids)->delete();
                $count += (new BsoCount())->where("TD_ID",$this_one["TD_ID"])->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败"]);

        $one = $this->model->alias("m")->join(["produceunitin"=>"pui"],"m.PUM_Num = pui.PUM_Num")->where("pui.PU_ID",$num)->find();
        if(!$one) return json(["code"=>0,"msg"=>"删除失败"]);
        else if($one["Auditor"]) return json(["code"=>0,"msg"=>"已审核，无法删除"]);
        else{
            $find_one = $this->detailModel->alias("pui")
                ->join(["bsocount"=>"bc"],"bc.SCD_ID = pui.SCD_ID")
                ->where(["pui.PU_ID"=>["=",$num],"bc.BSO_Count"=>["=",0]])
                ->find();
            if($find_one) return json(["code"=>0,"msg"=>"已发运，删除失败！"]);
            $count = 0;
            Db::startTrans();
            try {
                $count += $this->detailModel->where("PU_ID",$num)->delete();
                $count += (new BsoCount())->where("SCD_ID",$one["SCD_ID"])->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                return json(["code"=>1,"msg"=>"成功"]);
            } else {
                return json(["code"=>0,"msg"=>"删除失败"]);
            }
        }
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->model->where("PUM_Num",$num)->find();
        if($row["Auditor"]) return json(["code"=>0,"msg"=>"已审核"]);
        $update = [
            "Auditor" => $this->admin["nickname"],
            "AuditorDate" => date("Y-m-d H:i:s")
        ];
        $result = $this->model->update($update,["PUM_Num"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"弃审失败，请稍后重试"]);
        $row = $this->model->where("PUM_Num",$num)->find();
        if(!$row["Auditor"]) return json(["code"=>0,"msg"=>"已弃审"]);

        $update = [
            "Auditor" => '',
            "AuditorDate" => "0000-00-00 00:00:00"
        ];
        $result = $this->model->update($update,["PUM_Num"=>$num]);
        if ($result) {
            $this->success("弃审成功！");
        } else {
            $this->error("弃审失败！");
        }
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["PU_ID","PU_ID","readonly","","hidden","PU_ID"],
            ["SCD_ID","SCD_ID","readonly","","hidden","SCD_ID"],
            ["TH_ID","TH_ID","readonly","","hidden","TH_ID"],
            ["塔型","TD_TypeName","readonly","","","TD_TypeName"],
            ["呼高","TH_Height","readonly","","","TH_Height"],
            ["杆塔号","SCD_TPNum","readonly","","","SCD_TPNum"],
            //总数和总基数
            ["总数","count","readonly",1,"","SCD_Count"],
            //已进仓数量
            ["已进仓","PU_Count","readonly",0,"","RK_Count"],
            //进仓数量
            ["进仓数量","jc_count","",1,"","SY_Count"],
            ["总基重(kg)","PU_Weight","",0,"","SCD_Weight"],
            ["螺栓重(kg)","boltweight","",0,"","boltweight"],
            ["总重(kg)","z_PU_Weight","readonly",0,"","SCD_Weight"],
            // ["进仓单价","pu_price","",0,""],
            ["备注","PU_Memo","","","","PU_Memo"]
        ];
        return $list;
    }

    public function print($ids = null, $pu_lb = "")
    {
        $row = $this->model->alias("pum")
            ->join(["taskdetail"=>"td"],"pum.TD_ID = td.TD_ID")
            ->join(["task"=>"t"],"td.T_Num = t.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->field("pum.*,t.t_project,td.TD_TypeName,t.T_Num,c.Customer_Name")
            ->where("pum.PUM_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        
        $list = $this->detailModel->alias("pui")
            ->join(["sectconfigdetail"=>"scd"],"scd.SCD_ID = pui.SCD_ID")
            ->field("pui.PU_ID,scd.SCD_ID,scd.TD_TypeName,scd.TH_ID,scd.TH_Height,scd.SCD_TPNum,PU_Count as count,PU_Count,PU_Count as jc_count,PU_Weight,pui.boltweight,PU_Weight as z_PU_Weight,pu_price,PU_Memo")
            ->where("pui.PUM_Num",$ids)
            ->order("SCD_ID")
            ->select();
        $row['PUM_Date'] = date("Y-m-d", strtotime($row["PUM_Date"]));
        $row["category"] = $row["category"]?$row["category"]:"铁塔";
        $this->assignconfig('row',$row);
        $this->assignconfig('list',$list);
        $this->assignconfig('pu_lb',$pu_lb);
        return $this->view->fetch();
    }
}
