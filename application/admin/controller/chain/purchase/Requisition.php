<?php

namespace app\admin\controller\chain\purchase;

use app\admin\model\Admin;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Requisition extends Backend
{
    
    /**
     * Requisition模型对象
     * @var \app\admin\model\chain\purchase\Requisition
     */
    protected $model = null, $detailModel = null, $admin='';
    protected $noNeedLogin = ["selectRequisition","addMaterial","syncRequisition","selectOperator"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\purchase\ApplyList;
        $this->detailModel = new \app\admin\model\chain\purchase\ApplyDetails;
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

     /**
     * 查看
     */
    public function index()
    {
        list($deptList,$deptTree) = $this->deptType(1);
        $purchaseList = $this->purchaseList(1);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->where("Valid",0)
                ->order($sort, $order)
                ->paginate($limit);
            $row = [];
            foreach($list->items() as $k=>$v){
                $row[$k] = $v->toArray();
                $row[$k]["DD_Num"] = @$deptList[$v["DD_Num"]];
                $row[$k]["AL_PurchaseType"] = @$purchaseList[$v["AL_PurchaseType"]];
                $row[$k]["AL_ArriveDate"] = date("Y-m-d",strtotime($v["AL_ArriveDate"]));
                $row[$k]["AL_WriteDate"] = date("Y-m-d",strtotime($v["AL_WriteDate"]));
                $row[$k]["AL_WriterDate"] = date("Y-m-d",strtotime($v["AL_WriterDate"]));
            }
            $result = array("total" => $list->total(), "rows" => $row);

            return json($result);
        }
        $this->assignconfig("deptList",$deptList);
        $this->assignconfig("purchaseList",$purchaseList);
        return $this->view->fetch();
    }

    public function selectRequisition()
    {
        list($deptList,$deptTree) = $this->deptType(1);
        $purchaseList = $this->purchaseList(1);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->where("Valid",0)
                ->order($sort, $order)
                ->paginate($limit);
            $row = [];
            foreach($list->items() as $k=>$v){
                $row[$k] = $v->toArray();
                $row[$k]["DD_Num"] = @$deptList[$v["DD_Num"]];
                $row[$k]["AL_PurchaseType"] = @$purchaseList[$v["AL_PurchaseType"]];
                $row[$k]["AL_ArriveDate"] = date("Y-m-d",strtotime($v["AL_ArriveDate"]));
                $row[$k]["AL_WriteDate"] = date("Y-m-d",strtotime($v["AL_WriteDate"]));
                $row[$k]["AL_WriterDate"] = date("Y-m-d",strtotime($v["AL_WriterDate"]));
            }
            $result = array("total" => $list->total(), "rows" => $row);

            return json($result);
        }
        $this->assignconfig("deptList",$deptList);
        $this->assignconfig("purchaseList",$purchaseList);
        return $this->view->fetch();
    }

    public function importDetail($ids = null)
    {
        if ($this->request->isPost()) {
            $file = $this->request->post("filename");
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $data = [];
            $file = ROOT_PATH . DS . 'public' . DS . $file;
            try {
                $data = Excel::import($file, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($data)){
                    $this->error('空数据文件');
                }
                $data = array_slice($data,1);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
            $inventoryNumList = $this->inventoryNumList(1);
            $applyDetails = [];
            foreach($data as $v){
                if(isset($inventoryNumList[$v[1].'-'.$v[2]])){
                    $newsOne = $inventoryNumList[$v[1].'-'.$v[2]];
                    $applyDetails[] = [
                        "AD_ID" => "0",
                        "IM_Num" => $newsOne["IM_Num"],
                        "IM_Class" => $v[1]?$v[1]:'',
                        "IM_Spec" => $v[2]?$v[2]:'',
                        "L_Name" => $v[3]?$v[3]:"",
                        "AD_Length" => $v[4]?$v[4]:0,
                        "AD_Width" => $v[5]?$v[5]:0,
                        "AD_Count" => $v[6]?$v[6]:0,
                        "IM_PerWeight" => $newsOne["IM_PerWeight"],
                        "AD_Weight" => $v[7]?($v[7]*1000):0,
                        "ad_money" => 0,
                        "AD_Memo" => ''
                    ];
                }
            }
            if(!empty($applyDetails)){
                $field = $this->getFieldString($applyDetails);
                $this->success("导入成功",null,$field);
            }else $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function syncRequisition()
    {
        $iniWhere = [
            "al.Modifyer" => ["<>",""],
            "ad_cp_check" => ["=",0],
            "al.al_reciever" => ["=",$this->view->admin["id"]],
        ];
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("al")
                ->join(["applydetails"=>"ad"],"al.AL_Num=ad.AL_Num")
                ->join(["inventorymaterial"=>"im"],"ad.IM_Num = im.IM_Num")
                ->field("al.AL_Num,ad.AD_ID,ad.IM_Num,im.IM_Class,im.IM_Spec,ad.L_Name,round(ad.AD_Length/1000,3) as AD_Length,round(ad.AD_Width/1000,3) as AD_Width,ad.AD_Count,ad.AD_Weight,al.AL_Writer,al.AL_WriterDate,ad.AD_Memo,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case when im.IM_Class='角钢' then SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+ SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1 when (im.IM_Class='钢板' or im.IM_Class='法兰') then REPLACE(im.IM_Spec,'-','') else 0 end) as UNSIGNED) bh,ad.ad_cp_check,ad.AD_BuyCount,ad.AD_BuyWeight,im.IM_Measurement,(AD_Count-AD_BuyCount) as sy_count,(AD_Weight-AD_BuyWeight) as sy_weight")
                ->where($where)
                ->where($iniWhere)
                ->order("ad.ad_cp_check asc,al.AL_WriteDate desc,clsort asc,ad.L_Name asc,bh asc,AD_Length ASC")
                ->paginate($limit);
            foreach($list as &$v){
                foreach(["AD_Length","AD_Width","AD_Count","AD_Weight","AD_BuyWeight","AD_BuyCount"] as $vv){
                    $v[$vv] = round($v[$vv],3);
                }
            }
            
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function requisitionDetail()
    {
        $ids = $this->request->post("ids");
        if(!$ids) return json(["code"=>0,"msg"=>"有误"]);
        $list = $this->detailModel->alias("ad")
            ->join(["inventorymaterial"=>"im"],"ad.IM_Num = im.IM_Num")
            ->field("ad.AD_ID,ad.AL_Num,ad.IM_Num,im.IM_Class,im.IM_Spec,ad.L_Name,round(ad.AD_Length/1000,3) as AD_Length,round(ad.AD_Width/1000,3) as AD_Width,ad.AD_Count,ad.AD_Weight,ad.AD_Memo,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("ad.AL_Num",$ids)
            ->order("clsort,ad.L_Name,bh,AD_Length ASC")
            ->select();
        $sum_weight = $count = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["AD_Length"] = round($v['AD_Length'],3);
            $rows[$k]["AD_Width"] = round($v['AD_Width'],3);
            $rows[$k]["AD_Count"] = round($v['AD_Count'],2);
            $rows[$k]["AD_Weight"] = round($v['AD_Weight'],2);
            $sum_weight += $v["AD_Weight"];
            $count += $v["AD_Count"];
        }
        $rows[] = ["AD_ID"=>"合计","AD_Weight"=>round($sum_weight,2),"AD_Count"=>$count];
        return json(["code"=>1,"data"=>$rows]);
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $AL_Num = $params["AL_Num"];
                if($AL_Num){
                    $one = $this->model->where("AL_Num",$AL_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("Ym");
                    $one = $this->model->field("AL_Num")->where("AL_Num","LIKE","CG".$month.'-%')->order("AL_Num DESC")->find();
                    if($one){
                        $num = substr($one["AL_Num"],9);
                        $AL_Num = 'CG'.$month.'-'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $AL_Num = 'CG'.$month.'-001';
                }
                $params["AL_Num"] = $AL_Num;
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $result = $this->model::create($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$result["AL_ID"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        list($deptList,$deptTree) = $this->deptType(1);
        $purchaseList = $this->purchaseList(1);
        $this->view->assign("row",$row);
        $this->view->assign("deptList",$deptList);
        $this->view->assign("purchaseList",$purchaseList);
        $this->view->assign("tableField",$this->getTableField());
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            
            if ($params) {
                $params = $this->preExcludeFields($params);
                $sectSaveList = [];
                
                $saveField = [];
                foreach($this->getTableField() as $k=>$v){
                    $saveField[$v[1]] = $v[1];
                }
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk]["AL_Num"] = $row["AL_Num"];
                        if($k=="AD_ID" and $vv==0) continue;
                        else if($vv=="") continue;
                        else if($k=="AD_Length" or $k=="AD_Width") $sectSaveList[$kk][$k] = $vv*1000;
                        else $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("AL_ID",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row["writer"] = $this->admin["nickname"];
        $row["time"] = date("Y-m-d");
        if($row["al_reciever"]) $row["al_reciever_name"] = (new Admin())->where("id",$row["al_reciever"])->value("username");
        list($deptList,$deptTree) = $this->deptType(1);
        $purchaseList = $this->purchaseList(1);
        $list = $this->detailModel->alias("ad")
            ->join(["inventorymaterial"=>"im"],"ad.IM_Num = im.IM_Num")
            ->field("ad.AD_ID,ad.AL_Num,ad.IM_Num,im.IM_Class,im.IM_Spec,im.IM_PerWeight,ad.L_Name,round(ad.AD_Length/1000,3) as 'AD_Length',round(ad.AD_Width/1000,3) as 'AD_Width',ad.AD_Count,ad.AD_Weight,ad.AD_Memo,ad.ad_money,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("ad.AL_Num",$row["AL_Num"])
            ->order("clsort,ad.L_Name,bh,AD_Length ASC")
            ->select();
        $sum_weight = $count = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["AD_Length"] = round($v['AD_Length'],3);
            $rows[$k]["AD_Width"] = round($v['AD_Width'],3);
            $rows[$k]["AD_Count"] = round($v['AD_Count'],2);
            $rows[$k]["AD_Weight"] = round($v['AD_Weight'],2);
            $sum_weight += $v["AD_Weight"];
            $count += $v["AD_Count"];
        }
        $row["sum_weight"] = $sum_weight;
        $row["count"] = $count;
        list($limberList,$kj_limberList) = $this->getLimber();
        $this->view->assign("list",$rows);
        $this->view->assign("row",$row);
        $this->view->assign("deptList",$deptList);
        $this->view->assign("purchaseList",$purchaseList);
        $this->view->assign("tableField",$this->getTableField());
        $this->assignconfig('AL_Num',$row["AL_Num"]);
        $this->view->assign("limberList",$limberList);
        $this->assignconfig('limberList',$limberList);
        $this->assignconfig("kj_limberList",$kj_limberList);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function export($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $title = $row["AL_Num"];
        // DB::query("set @restnum=0;");
        $list = $this->detailModel->alias("ad")
            ->join(["inventorymaterial"=>"im"],"ad.IM_Num = im.IM_Num")
            ->field("ad.AD_ID,ad.AL_Num,ad.IM_Num,im.IM_Class,im.IM_Spec,im.IM_PerWeight,ad.L_Name,ad.AD_Length/1000 as 'AD_Length',ad.AD_Width/1000 as 'AD_Width',ad.AD_Count,round(ad.AD_Weight/1000,3) as AD_Weight,ad.AD_Memo,ad.ad_money,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when im.IM_Class='角钢' then 
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
            SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
            when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
            REPLACE(im.IM_Spec,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("ad.AL_Num",$row["AL_Num"])
            ->order("clsort,ad.L_Name,bh,AD_Length ASC")
            ->select();
        $sum_weight = $count = 0;
        foreach($list as $k=>$v){
            $list[$k]["ID"] = $k+1;
            $list[$k]["AD_Length"] = round($v['AD_Length'],3);
            $list[$k]["AD_Width"] = round($v['AD_Width'],3);
            $list[$k]["AD_Count"] = round($v['AD_Count'],2);
            $list[$k]["AD_Weight"] = round($v['AD_Weight'],2);
            $sum_weight += $v["AD_Weight"];
            $count += $v["AD_Count"];
        }
        $list[]=["ID"=>"合计","AD_Count"=>$count,"AD_Weight"=>$sum_weight];
        $header = [
            ['ID', 'ID'],
            ['材料名称', 'IM_Class'],
            ['规格', 'IM_Spec'],
            ['材质', 'L_Name'],
            ['长度(m)', 'AD_Length'],
            ['宽度(m)', 'AD_Width'],
            ['数量', 'AD_Count'],
            ['重量(吨)', 'AD_Weight'],
            ['备注', 'AD_Memo']
        ];

        return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
    
    }

    public function chooseMaterial($type=0)
    {
        $imModel = new \app\admin\model\jichu\ch\InventoryMaterial;
        $measureModel = new \app\admin\model\jichu\ch\Measurement;
        $imlist = $select = $measure = [];
        $ini_where = ["Valid"=>["=",1]];
        if($type == 2){
            $ini_where["IM_Num"] = ["LIKE","02%"];
        }
        $treeRows = $imModel->where($ini_where)->order("ParentNum ASC")->select();
        foreach($treeRows as $v){
            $imlist[$v["IM_Num"]] = [
                "id" => $v["IM_Num"],
                "parent" => $v["ParentNum"]==""?"#":$v["ParentNum"],
                "text" => $v['IM_BD_Other']?$v['IM_Spec']." ".$v['IM_BD_Other']:$v['IM_Spec'],
                "state" => [
                    "opened" => false,
                    "disabled" => false,
                    "selected" => false
                ]
            ];
            $select[$v["IM_Num"]] = $v["IM_Spec"];
        }
        $measureRows = $measureModel->field("M_Name")->order("M_ID ASC")->select();
        foreach($measureRows as $v){
            $measure[$v["M_Name"]] = $v["M_Name"];
        }
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            if(empty($filter)) $limit = 200;
            $list = $imModel
                ->where($ini_where)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["id"] = $k;
                $rows[$k]["ParentNum"] = $imlist[$v["ParentNum"]]["text"]??"";
                $rows[$k]["type"] = $select[substr($v['IM_Num'],0,2)];
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        $this->view->assign('treeList',$imlist);
        $this->view->assign('selectList',$select);
        $this->view->assign('measureList',$measure);
        $this->assignConfig("type",$type);
        return $this->view->fetch();
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        $existList = $this->model->alias("ap")
            ->join(["applydetails"=>"ad"],"ap.AL_Num=ad.AL_Num")
            ->join(["procurement_detail"=>"pd"],"ad.AD_ID=pd.AD_ID","LEFT")
            ->where("ad.AD_ID","=",$num)
            ->where(function ($query) {
                $query->where('ap.Modifyer',"<>","")->whereor('ifnull(pd.id,0)', "<>",0);
            })
            ->find();
        if($existList) $this->error("存在已审核或者已采购的状态，请核对无误后再删除操作");
        if($num){
            Db::startTrans();
            try {
                $this->detailModel->where("AD_ID",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    /**
     * 删除
     * 已审核无法删除
     * 已有合同无法删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            // $dhcooperatek_one = (new CDiagramBuyMain())->where("CDM_Num","in",$ids)->find();
            // if($dhcooperatek_one) $this->error("已生成图纸数据配段，无法删除");

            // $list = $this->model->where($pk, 'in', $ids)->select();
            $existList = $this->model->alias("ap")
                ->join(["applydetails"=>"ad"],"ap.AL_Num=ad.AL_Num")
                ->join(["procurement_detail"=>"pd"],"ad.AD_ID=pd.AD_ID","LEFT")
                ->where("ap.AL_ID","IN",$ids)
                ->where(function ($query) {
                    $query->where('ap.Modifyer',"<>","")->whereor('ifnull(pd.id,0)', "<>",0);
                })
                ->find();
            if($existList) $this->error("存在已审核或者已采购的状态，请核对无误后再删除操作");
            $count = false;
            Db::startTrans();
            try {
                $alNumList = $this->model->where($pk,"in",$ids)->column("AL_Num");
                $count = $this->model->where($pk, 'in', $ids)->delete();
                $this->detailModel->where("AL_Num", 'in', $ids)->delete();
                // foreach($list as $v){
                    // $task_detail_id[] = $v["AL_Num"];
                    // $v->update("");
                // }
                // $count += $this->detailModel->where($pk,"IN",$ids)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function addMaterial()
    {
        $params = $this->request->post("data");
        $params = json_decode($params,true);
        $field = $this->getFieldString($params);
        return json(["code"=>1,"data"=>$field]);
    }

    protected function getFieldString($params)
    {
        $tableField = $this->getTableField();
        list($limberList,$kj_limberList) = $this->getLimber();
        $field = "";
        foreach($params as $k=>$v){
            $field .= '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
            foreach($tableField as $vv){
                if($vv[1]=="L_Name"){
                    $field .= "<td ".$vv[6].">";

                    $field .= '<input type="text" class="small_input" list="typelist" name="table_row['.$vv[1].'][]" value="'.($v[$vv[1]]??'').'" placeholder="请选择"><datalist id="typelist">';
                    foreach($limberList as $vvv){
                        $field .= '<option value="'.$vvv.'">'.$vvv.'</option>';
                    }
                    $field .= "</datalist></td>";
                }else if(isset($v[$vv[1]])){
                    $field .= '<td '.$vv[6].'><input type="'.$vv[2].'" class="small_input" '.$vv[4].' name="table_row['.$vv[1].'][]" value="'.$v[$vv[1]].'"></td>';
                }else{
                    $field .= '<td '.$vv[6].'><input type="'.$vv[2].'" class="small_input" '.$vv[4].' name="table_row['.$vv[1].'][]" value="'.$vv[5].'"></td>';
                }
            }
            $field .= "</tr>";
        }
        return $field;
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["AD_ID","AD_ID","text","","readonly",0,"hidden"],
            ["IM_Num","IM_Num","text","","readonly","","hidden"],
            ["材料名称","IM_Class","text","","readonly","",""],
            ["规格","IM_Spec","text","","readonly","",""],
            ["材质","L_Name","text","","","",""],
            ["长度(m)","AD_Length","text","","","",""],
            ["宽度(m)","AD_Width","text","","","",""],
            ["数量","AD_Count","text","","","",""],
            ["比重","IM_PerWeight","text","","readonly",0,"hidden"],
            ["重量(kg)","AD_Weight","text","","",0,""],
            ["预计金额","ad_money","text","","",0,""],
            ["备注","AD_Memo","text","","","",""]
        ];
        return $list;
    }

    /**
     * 打印
     */
    public function printDetail($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        // dump($row);
        
        $row["writer"] = $this->admin["nickname"];
        $row["time"] = date("Y-m-d");
        
        $list = $this->detailModel->alias("ad")
        ->join(["inventorymaterial"=>"im"],"ad.IM_Num = im.IM_Num")
        ->field("ad.AD_ID,ad.AL_Num,ad.IM_Num,im.IM_Class,im.IM_Spec,im.IM_PerWeight,ad.L_Name,round(ad.AD_Length/1000,2) as 'AD_Length',round(ad.AD_Width/1000,2) as 'AD_Width',ad.AD_Count,ad.AD_Weight,ad.AD_Memo,ad.ad_money,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
        when im.IM_Class='角钢' then 
        SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
        SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
        when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
        REPLACE(im.IM_Spec,'-','')
        else 0
        end) as UNSIGNED) bh")
        ->where("ad.AL_Num",$row["AL_Num"])
        ->order("clsort,bh,AD_Length ASC")
        ->select();

        $mainInfos = [];
        $mainInfos['writer'] = $this->admin["nickname"];
        $mainInfos["time"] = date("Y-m-d");
        $mainInfos["AL_Num"] = $row["AL_Num"];
        
        $this->assignconfig('mainInfos',$mainInfos);
        $this->assignconfig('list',$list);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    /**
     * 点击打开新页面 接收人和审核人的选择
     * 有审核权限的人审核 有接收人才发送至接收人
     */
    public function selectOperator($type=0)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\Admin())
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
                
            $rows = $list->items();
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        $this->assignconfig("type",$type);
        return $this->view->fetch();
    }

    /**
     * auditor
     * 审核
     */
    public function auditor()
    {
        $num = $this->request->post("ids");
        $row = $this->model->get($num);
        if(!$row) $this->error("审核有误，请核对");
        if($row["Modifyer"]) $this->error("已审核，无需再次审核！");
        if(!$row["al_reciever"]) $this->error("未填写接收人！");

        $listCount = $this->detailModel->where("AL_Num",$row["AL_Num"])->group("AL_Num")->value("count(AD_ID) AS count");
        if(!$listCount) $this->error("无请购详情，请添加无误后重新审核！");

        $result = $this->model
            ->where("AL_ID",$num)
            ->update([
                "Modifyer" => $this->admin["nickname"],
                "ModifyTime" => date("Y-m-d H:i:s")
            ]);
        if ($result !== false) {
            $this->success("审核成功");
        } else {
            $this->error("审核失败");
        }
    }

    /**
     * 弃审
     */
    public function giveUp()
    {
        $num = $this->request->post("ids");
        $row = $this->model->get($num);
        if(!$row) $this->error("弃审有误，请核对");
        if(!$row["Modifyer"]) $this->error("未审核，无需弃审！");

        $listCount = $this->detailModel
            ->where("AL_Num",$row["AL_Num"])
            ->where("AD_BuyDate","<>","0000-00-00 00:00:00")
            ->group("AL_Num")
            ->value("count(AD_ID) AS count");
        if($listCount) $this->error("已进行采购，无法弃审！");
        $result = $this->model
            ->where("AL_ID",$num)
            ->update([
                "Modifyer" => '',
                "ModifyTime" => "0000-00-00 00:00:00"
            ]);
        if ($result !== false) {
            $this->success("弃审成功");
        } else {
            $this->error("弃审失败");
        }
    }
}
