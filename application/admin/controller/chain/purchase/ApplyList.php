<?php

namespace app\admin\controller\chain\purchase;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class ApplyList extends Backend
{
    
    /**
     * Requisition模型对象
     * @var \app\admin\model\chain\purchase\Requisition
     */
    protected $model = null, $detailModel = null, $admin='';

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\purchase\ApplyList;
        $this->detailModel = new \app\admin\model\chain\purchase\ApplyDetails;
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

     /**
     * 查看
     */
    public function index()
    {
        $this->assignconfig("status",[
            0 => "未采购完成",
            1 => "已采购"
        ]);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("al")
                ->join(["applydetails"=>"ad"],"al.AL_Num=ad.AL_Num")
                ->join(["inventorymaterial"=>"im"],"ad.IM_Num = im.IM_Num")
                ->field("al.AL_Num,ad.AD_ID,ad.IM_Num,im.IM_Class,im.IM_Spec,ad.L_Name,round(ad.AD_Length/1000,3) as AD_Length,round(ad.AD_Width/1000,3) as AD_Width,ad.AD_Count,ad.AD_Weight,al.AL_Writer,al.AL_WriterDate,ad.AD_Memo,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case when im.IM_Class='角钢' then SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+ SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1 when (im.IM_Class='钢板' or im.IM_Class='法兰') then REPLACE(im.IM_Spec,'-','') else 0 end) as UNSIGNED) bh,ad.ad_cp_check,ad.AD_BuyCount,ad.AD_BuyWeight,(AD_Count-AD_BuyCount) as sy_count,(AD_Weight-AD_BuyWeight) as sy_weight")
                ->where($where)
                ->where("al.Modifyer","<>","")
                ->where("al.al_reciever",$this->view->admin["id"])
                ->order("ad.ad_cp_check asc,al.AL_WriteDate desc,clsort asc,ad.L_Name asc,bh asc,AD_Length ASC")
                ->paginate($limit);
            foreach($list as &$v){
                foreach(["AD_Length","AD_Width","AD_Count","AD_Weight","AD_BuyWeight","AD_BuyCount","sy_count","sy_weight"] as $vv){
                    $v[$vv] = round($v[$vv],5);
                }
            }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 完成
     */
    public function auditor($ids = "")
    {
        $this->model = new \app\admin\model\chain\purchase\ApplyDetails();
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $result = $this->model->where($pk, 'in', $ids)->update([
                "ad_cp_check" => 1
            ]);
            if($result) $this->success("更新成功");
            else $this->error("更新失败");
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 取消
     */
    public function giveUp($ids = "")
    {
        $this->model = new \app\admin\model\chain\purchase\ApplyDetails();
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $result = $this->model->where($pk, 'in', $ids)->update([
                "ad_cp_check" => 0
            ]);
            if($result) $this->success("更新成功");
            else $this->error("更新失败");
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
}