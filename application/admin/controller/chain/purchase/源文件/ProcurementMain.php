<?php

namespace app\admin\controller\chain\purchase;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 合同列管理
 *
 * @icon fa fa-circle-o
 */
class ProcurementMain extends Backend
{
    
    /**
     * ProcurementMain模型对象
     * @var \app\admin\model\chain\purchase\ProcurementMain
     */
    protected $model = null,$admin='',$limberList = [],$wayList = [0=>"数量",1=>"重量"],$bzList = ["人民币"=>"人民币"],$procurementType = [0=>"原材料",1=>"机物料",2=>"紧固件",3=>"线缆"];
    protected $noNeedLogin = ["addMaterial","selectProcurement","selectProcurementList"];
    protected $relatedModel = null,$detailModel = null,$viewModel=null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\purchase\ProcurementMain;
        $this->relatedModel = new \app\admin\model\chain\purchase\ProcurementRelated;
        $this->detailModel = new \app\admin\model\chain\purchase\ProcurementDetail;
        $this->viewModel = new \app\admin\model\chain\purchase\DhProcurementView;
        $this->admin = \think\Session::get('admin');
        // $this->assignconfig("tableField",$tableField);
        //合同类型 币种 部门
        $purchaseList = $this->purchaseList(1);
        [$deptList] = $this->deptType(1);
        // $this->measurementList = $this->measurementList();
        $assign = [
            "tableField" => $this->getTableField(),
            "purchaseList" => $purchaseList,
            "deptList" => $deptList,
            "bzList" => $this->bzList,
            "wayList" => $this->wayList,
            "nickname" => $this->admin["username"],
            "procurement_type" => $this->procurementType,
            "vendorList" => $this->vendorNumList()
        ];
        $this->view->assign($assign);
        foreach($assign as $k=>$v){
            $this->assignconfig($k,$v);
        }
        [$limberList] = $this->getLimber();
        $this->limberList = $limberList;
    }
    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function selectProcurement($v_num='',$type=0)
    {
        $iniWhere = [];
        if($v_num) $iniWhere["unit"] = ["=",$v_num];
        //type后续使用
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->where($iniWhere)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        $this->assignconfig("v_num",$v_num);
        $this->assignconfig("type",$type);
        return $this->view->fetch();
    }

    public function selectProcurementList($v_num='',$type=0)
    {
        $iniWhere = [];
        if($v_num) $iniWhere["supplier"] = ["=",$v_num];
        $iniWhere["procurement_type"] = ["=",$type];
        //type后续使用
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->viewModel
                ->where($where)
                ->where($iniWhere)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        $this->assignconfig("v_num",$v_num);
        $this->assignconfig("type",$type);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            if(!$table) $this->error("合同详情不能为空");
            //pm_num HT202304-0001
            if(!$params["pm_num"]){
                $year = "HTY".date("Ym")."-";
                $lastOne = $this->model->where("pm_num","LIKE",$year."%")->order("pm_num desc")->value("pm_num");
                if($lastOne) $lastOne = str_pad((substr($lastOne,-4)+1),4,0,STR_PAD_LEFT);
                else $lastOne = "0001";
                $params["pm_num"] = $year.$lastOne;
            }else{
                $findOne = $this->model->where("pm_num","=",$params["pm_num"])->find();
                if($findOne) $this->error("该编号已存在，请稍后再试");
            }
            // $qgList = [];
            // $replaceList = json_decode($params["qg"],true);
            // foreach($replaceList as $k=>$v){
            //     $qgList[$v] = [
            //         "pm_num" => $params["pm_num"],
            //         "AL_Num" => $v
            //     ];
            // }
            unset($params["qg"]);
            $params["writer"] = $this->admin["nickname"];
            $params["writer_time"] = date("Y-m-d H:i:s");
            $saveTable = [];
            foreach($table["id"] as $k=>$v){
                $way = $table["way"][$k];
                $count = $way?($table["weight"][$k]?$table["weight"][$k]:0):($table["count"][$k]?$table["count"][$k]:0);
                $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                $price = $count?round($amount/$count,5):0;
                $saveTable[$k] = [
                    "id" => $v,
                    "pm_num" => $params["pm_num"],
                    "AD_ID" => $table["AD_ID"][$k],
                    "im_num" => $table["im_num"][$k],
                    "l_name" => $table["l_name"][$k],
                    "length" => $table["length"][$k]?$table["length"][$k]:0,
                    "width" => $table["width"][$k]?$table["width"][$k]:0,
                    "unit" => $table["unit"][$k],
                    "count" => $table["count"][$k]?$table["count"][$k]:0,
                    "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                    "way" => $way,
                    "amount" => $amount,
                    "price" => $price,
                    "memo" => $table["memo"][$k],
                ];
                if(!$v) unset($saveTable[$k]["id"]);
            }
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params);
                // if(!empty($qgList)) $qgResult = $this->relatedModel->allowField(true)->saveAll($qgList);
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->assignconfig("init_data",[]);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $flag = 1;
        if($row["auditor"]=="") $flag = 0;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $init_data = $this->relatedModel->where("pm_num",$ids)->column("AL_Num");
        // $row["qg"] = json_encode($init_data);
        if($row["procurement_type"] == 1){
            $type = 1;
            $detailList = $this->detailModel->alias("d")
            ->join(["wjinventoryrecord"=>"im"],"im.IR_Num=d.IM_Num")
            ->field("d.id,d.im_num,im.IR_Name as IM_Class,im.IR_Spec as IM_Spec,d.l_name,d.length,d.width,d.unit,d.count,d.weight,d.way,d.amount,d.price,d.memo,d.AD_ID")
            ->where("d.pm_num",$ids)
            ->select();
        }else{
            $type=0;
            $detailList = $this->detailModel->alias("d")
            ->join(["inventorymaterial"=>"im"],"d.im_num=im.IM_Num")
            ->field("d.id,d.im_num,im.IM_Class,im.IM_Spec,d.l_name,d.length,d.width,d.unit,d.count,d.weight,d.way,d.amount,d.price,d.memo,d.AD_ID")
            ->where("d.pm_num",$ids)
            ->select();
        }
        $detailList = $detailList?collection($detailList)->toArray():[];
        $detailIdList = $this->detailModel->where("pm_num",$ids)->column("id");
        $tbodyField = $this->_getTbodyField($detailList,$this->getTableField(),$type);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            $table = $this->request->post("table/a");
            if(!$table) $this->error("合同详情不能为空");
            $saveTable = [];
            foreach($table["id"] as $k=>$v){
                $way = $table["way"][$k];
                $count = $way?($table["weight"][$k]?$table["weight"][$k]:0):($table["count"][$k]?$table["count"][$k]:0);
                $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                $price = $count?round($amount/$count,5):0;
                $saveTable[$k] = [
                    "id" => $v,
                    "pm_num" => $ids,
                    "AD_ID" => $table["AD_ID"][$k],
                    "im_num" => $table["im_num"][$k],
                    "l_name" => $table["l_name"][$k],
                    "length" => $table["length"][$k]?$table["length"][$k]:0,
                    "width" => $table["width"][$k]?$table["width"][$k]:0,
                    "unit" => $table["unit"][$k],
                    "count" => $table["count"][$k]?$table["count"][$k]:0,
                    "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                    "way" => $way,
                    "amount" => $amount,
                    "price" => $price,
                    "memo" => $table["memo"][$k],
                ];
                if(!$v) unset($saveTable[$k]["id"]);
                else if(in_array($v,$detailIdList)!=-1){
                    $wz = in_array($v,$detailIdList);
                    unset($detailIdList[$wz]);
                }
            }

            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params,["pm_num"=>$ids]);
                $this->relatedModel->where("pm_num",$ids)->delete();
                // if(!empty($qgList)) $qgResult = $this->relatedModel->allowField(true)->saveAll($qgList);
                if(!empty($detailIdList)) $this->detailModel->where("id","in",$detailIdList)->delete();
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->view->assign("tbodyField", $tbodyField);
        $this->view->assign("row", $row);
        $this->assignconfig("init_data",$init_data);
        $this->assignconfig("ids",$ids);
        $this->view->assign("flag",$flag);
        $this->assignconfig("flag",$flag);
        return $this->view->fetch();
    }

    public function addMaterial($type=0)
    {
        $params = $this->request->post("data");
        $params = json_decode($params,true);
        $tableField = $this->getTableField();
        $field = $this->_getTbodyField($params,$tableField,$type);
        return json(["code"=>1,"data"=>$field]);
    }

    protected function _getTbodyField($params,$tableField,$type)
    {
        $field = "";
        foreach($params as $k=>$v){
            $field .= '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
            foreach($tableField as $vv){
                if($vv[1]=="l_name"){
                    $field .= "<td ".$vv[6].">";
                    $field .= '<input type="text" class="small_input" list="typelist" name="table['.$vv[1].'][]" value="'.$v[$vv[1]].'" placeholder="请选择"><datalist id="typelist">';
                    foreach($this->limberList as $vvv){
                        $field .= '<option value="'.$vvv.'">'.$vvv.'</option>';
                    }
                    $field .= "</datalist></td>";
                }else if($vv[1]=="way"){
                    if($type==0) $value = $v[$vv[1]]??1;
                    else $value = $v[$vv[1]]??0;
                    $field .= "<td ".$vv[6].">";
                    $field .= build_select('table['.$vv[1].'][]',$this->wayList,$value,['class'=>'form-control selectpicker',"id"=>"c-".$vv[1],"data-rule"=>"required"]);
                    $field .= "</td>";
                }else if($vv[1]=="unit"){
                    $field .= "<td ".$vv[6].">";
                    $field .= build_select('table['.$vv[1].'][]',$this->measurementList(),($v[$vv[1]]??$v["IM_Measurement"]),['class'=>'form-control selectpicker',"id"=>"c-".$vv[1],"data-rule"=>"required"]);
                    $field .= "</td>";
                // }else if(isset($v[$vv[1]])){
                //     $field .= '<td '.$vv[6].'><input type="'.$vv[2].'" class="small_input" '.$vv[4].' name="table_row['.$vv[1].'][]" value="'.$v[$vv[1]].'"></td>';
                }else{
                    $field .= '<td '.$vv[6].'><input type="'.$vv[2].'" class="small_input" '.$vv[4].' name="table['.$vv[1].'][]" value="'.($vv[1]=="id"?0:($v[$vv[1]]??$vv[5])).'"></td>';
                }
            }
            $field .= "</tr>";
        }
        return $field;
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->select();
            $newIdList = [];
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                    $newIdList[$v[$pk]] = $v[$pk];
                }
                $this->detailModel->where($pk,"IN",$newIdList)->delete();
                $this->relatedModel->where($pk,"IN",$newIdList)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function auditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->update([
                    "auditor" => $this->admin["username"],
                    "auditor_time" => date("Y-m-d H:i:s")
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function giveUp($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","<>","")->update([
                    "auditor" => '',
                    "auditor_time" => "0000-00-00 00:00:00"
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
    
    //编辑table
    public function getTableField()
    {
        $list = [
            ["id","id","text","","readonly",0,"hidden"],
            ["im_num","im_num","text","","readonly","","hidden"],
            ["AD_ID","AD_ID","text","","readonly","","hidden"],
            ["材料名称","IM_Class","text","","readonly","",""],
            ["规格","IM_Spec","text","","readonly","",""],
            ["材质","l_name","text","","","",""],
            ["长度(mm)","length","number","","","",""],
            ["宽度(mm)","width","number","","","",""],
            ["单位","unit","select","","","",""],
            ["数量","count","number","","","",""],
            // ["比重","IM_PerWeight","text","","readonly",0,"hidden"],
            ["重量","weight","number","","","",""],
            ["单价计算","way","select","","","",""],
            ["金额","amount","number","","","",""],
            ["单价","price","number","","readonly","",""],
            ["备注","memo","text","","","",""]
        ];
        return $list;
    }
}
