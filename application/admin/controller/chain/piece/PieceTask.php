<?php

namespace app\admin\controller\chain\piece;
use app\common\controller\Backend;
use think\Db;
/**
 * 生产任务下达
 *
 * @icon fa fa-circle-o
 */
class PieceTask extends Backend
{

    /**
     * ProduceTask模型对象
     * @var \app\admin\model\chain\lofting\ProduceTask
     */
    protected $model = null;
    protected $admin;
    // protected $noNeedRight = ['showLckPrint','showJgPrint','showZhPrint'];
    protected $noNeedRight = '*';
    protected $noNeedLogin = ['getCache',"eipUpload"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\lofting\UnionProduceTaskView;
        // $this->sectModel = new \app\admin\model\chain\lofting\ProduceTaskSect;
        // $this->sectShiModel = new \app\admin\model\chain\lofting\ProduceTaskSectShizu;
        // $this->flatModel = new \app\admin\model\chain\lofting\ProduceTaskFlat;
        $this->taskDModel = new \app\admin\model\chain\sale\TaskDetail;
        $this->sectconfigModel = new \app\admin\model\chain\sale\Sectconfigdetail;
        // $this->detailModel = new \app\admin\model\chain\lofting\ProduceTaskDetail;
        // $this->dhModel = new \app\admin\model\chain\lofting\ProduceDh;
        $this->admin = \think\Session::get('admin');
        $flag_list = [1=>"下达中",2=>"已下达",3=>"计件中",4=>"计件完成"];
        $this->assignconfig("flag_list",$flag_list);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

     public function index()
     {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $subSql = $this->model->alias("p")->join(["mergtypename"=>"mtn"],"p.TD_ID=mtn.mTD_ID","left")
            ->field("flag,PT_Num,PT_Company,C_ProjectName,PT_Number,PT_sumWeight,PT_szMemo,PT_PlanTime,Writer,WriterDate,Auditor,AuditorDate,PT_Memo,ifnull(mTD_ID,p.TD_ID) AS TD_ID")
            ->where("p.PT_Num","not like","PT%")->group("p.PT_Num")
            ->buildSql();
            $list = DB::TABLE($subSql)->alias("p")
                ->join(["taskdetail"=>"d"],"p.TD_ID = D.TD_ID")
                ->join(["task"=>"t"],"t.T_Num=d.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->field("p.*,d.P_Name,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num'")
                ->where($where)
                ->where("p.Auditor","<>","")
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
     }

   //显示加工明细打印界面
   public function showJgPrint($PT_Num=null){
        //封面
        $ptask = $this->model->alias("m")
            ->join(["taskdetail"=>"td"],"td.TD_ID=m.TD_ID")
            ->join("(SELECT t_num,customer_name FROM task t join compact c on t.C_Num=c.C_Num) tc","tc.t_num=td.t_num")
            // ->join("(select old_TypeName,TD_ID from NewOldTDTypeName) notd","notd.TD_ID=td.TD_ID",'left')
            ->field("m.*,td.TD_TypeName,td.TD_Pressure,tc.customer_name")
            ->where("PT_Num",$PT_Num)
            ->find();
        if(!$ptask){
            $this->error('未查询到数据');
        }
        $ptask=$ptask->toArray();
        $ptask['idate']=date('Y-m-d');
    //        $this->assignconfig('treedata',$res);
    //        $this->assignconfig('printdata',$printdata);
    //        $this->assign('printdata',$printdata);
        $this->assign('ptask',$ptask);
        $this->assignconfig('ptask',$ptask);

        //段数汇总统计
        db()->execute("call get_dhz('$PT_Num');");
        //呼高段数汇总
        $dhz=db()->query("select PT_Num,concat('(',GROUP_CONCAT(SCD_TPNum),')',TD_TypeName,'-',TH_Height ,' 段落: ') as  HeightStr ,sectStr,
                    concat('[共',ltrim(sum(SCD_Count)),SCD_Unit,']',OtherStr) as OtherStr,C_Project
                    from tSect 
                    group by PT_Num,scd_tpnum,TD_TypeName,TH_Height,sectStr,OtherStr,C_Project");
        //判断是否有不同工程
        $ishb=false;
        foreach($dhz as $v){
            if($v['C_Project']!=$ptask['C_ProjectName']){
                $ishb=true;
                break;
            }
        }
        $dhzarr=[];
        foreach($dhz as $v){
    //            $sectarr=explode(',',$v['sectStr']);
    //            foreach($sectarr as $kk=>$vv) {
    //                $tmparr = explode('/', $vv);
    //                $sectarr[$kk] = trim(array_pop($tmparr));
    //            }
    //            sort($sectarr);
    //            $v['sectStr']=implode(',',$sectarr);
            if(!$ishb){
                $dhzarr[]=$v['HeightStr'].$v['sectStr'].$v['OtherStr'];
            }else{
                $dhzarr[]='<span style="line-height: 1.1em;">'.$v['C_Project']."<br />".$v['HeightStr'].$v['sectStr'].$v['OtherStr'].'</span>';
            }
        }
        $this->assignconfig('dhzarr',$dhzarr);
        /*$dhz=
        * array (
            0 =>
            array (
                'PT_Num' => '2110-025',
                'HeightStr' => '(10#,197#,198#)500-MC21D-FZB2-39 段落: ',
                'sectStr' => '1,2,3,4,5,6,10,15,18',
                'OtherStr' => '[共3基] ',
                'C_Project' => '崇仁-吉安东500千伏线路工程',
            ),
            1 =>
            array (
                'PT_Num' => '2110-025',
                'HeightStr' => '(156#,195#,206#)500-MC21D-FZB2-42 段落: ',
                'sectStr' => '1,2,3,4,5,6,7,16,19',
                'OtherStr' => '[共3基] ',
                'C_Project' => '崇仁-吉安东500千伏线路工程',
            ),
            2 =>
            array (
                'PT_Num' => '2110-025',
                'HeightStr' => '(42#,43#,83#,205#,207#,229#,254#)500-MC21D-FZB2-45 段落: ',
                'sectStr' => '1,2,3,4,5,6,7,17,19',
                'OtherStr' => '[共7基] ',
                'C_Project' => '崇仁-吉安东500千伏线路工程',
            ),
            )
        */
        //段别统计
        db()->query("update tSect a join (select SCD_TPNum,GROUP_CONCAT(PTF_SectName) sectstr from ProduceTaskFlat where pt_Num='$PT_Num' group by SCD_TPNum) b on a.scd_tpnum=b.scd_tpnum set a.sectstr=b.sectstr;");
        $tSect=db()->query("select * from tSect;");
        $this->assignconfig('tSect',$tSect);
        $dbtj=[];
        foreach($tSect as $v){
            $sectarr=explode(',',$v['sectStr']);
            foreach($sectarr as $vv){
                $tmparr=explode('/',$vv);
                $vv=trim(array_pop($tmparr));
                
                $xpos=stripos($vv,'*');
                if($xpos!==false){
                    $xt=substr($vv,$xpos+1);
                    $vv=substr($vv,0,$xpos);
                    for($i=0;$i<$xt;$i++){
                        if(!isset($dbtj[$vv])){
                            $dbtj[$vv]=0;
                        }
                        $dbtj[$vv]=$dbtj[$vv]+1;
                    }
                }else{
                    if(!isset($dbtj[$vv])){
                        $dbtj[$vv]=0;
                    }
                    $dbtj[$vv]=$dbtj[$vv]+1;
                }
            }
        }
        ksort($dbtj);
        $dbtjarr=[];
        foreach($dbtj as $k=>$v){
            if(!isset($dbtjarr[$v])){
                $dbtjarr[$v]=[];
            }
            $dbtjarr[$v][]=$k;
        }
        $dbtj="";
        foreach($dbtjarr as $k=>$v){
            $tmp=implode(',',$v);
            $dbtj=$dbtj."[".$tmp."] X ".$k."<br/>";
        }
        
        $tsrows=db()->query("
            select GROUP_CONCAT(TS_Name order by TS_Name+1) ts,count from (	
            select PTS_Name as TS_Name,SUM(PTS_Count) as count 
                    from producetasksect 
                    where PT_Num='$PT_Num' 
                    group by PTS_Name 
                    order by cast(PTS_Name as UNSIGNED),PTS_Name ) a group by count  order by ts
            ");
        $dbtj="";
        foreach($tsrows as $k=>$v){
    //            $tmp=implode(',',$v);
            $dbtj=$dbtj."[".$v['ts']."] X ".$v['count']."<br/>";
        }
        
        $this->assignconfig('dbtjstr',$dbtj);

        //试组装
        db()->execute("call get_szz('$PT_Num');");
        //段落统计
        $ts=db()->query("select * from ts order by --pts_name;");
        $szzdltj=[];
        foreach($ts as $v){
            $PTS_Count=intval($v['PTS_Count']);
            if(!isset($szzdltj[$PTS_Count])){
                $szzdltj[$PTS_Count]=[];
            }
            $szzdltj[$PTS_Count][]=$v['PTS_Name'];
        }
        $szzdltjstr="";
        foreach($szzdltj as $k=>$v){
            $tmp=implode(',',$v);
            $szzdltjstr=$szzdltjstr."[".$tmp."] X ".$k;
        }
        $this->assignconfig('szzdltjstr',$szzdltjstr);
        //段落重量
        $tw=db()->query("select * from tw order by --pts_name;");
        $szzdlzlstr="";
        foreach($tw as $v){
            $szzdlzlstr=$szzdlzlstr.$v['PTS_Name']."[".$v['SW']."kg], ";
        }
        $this->assignconfig("szzdlzlstr",$szzdlzlstr);

        //明细1
        $mxdata = $this->detailModel->alias("d")
                ->join(["dtmaterialdetial"=>'dd'],"d.DtMD_ID_PK = dd.DtMD_ID_PK")
                ->field("case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
                    when dtmd_sstuff='角钢' then 
                    SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
                    SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
                    when (dtmd_sstuff='钢板' or dtmd_sstuff='法兰') then 
                    REPLACE(DtMD_sSpecification,'-','')
                    else 0
                    end) as UNSIGNED) bh,dd.DtMD_sMaterial cz,dd.DtMD_sSpecification gg,
                                sum(dd.DtMD_fUnitWeight*PTD_Count) zl,
                                sum(dd.DtMD_iUnitHoleCount*PTD_Count) zjks,
                                sum(dd.DtMD_fUnitWeight*PTD_Count*ifnull(DtMD_iWelding/DtMD_iWelding,0) ) dh,
                                sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_iCuttingAngle) qj,
                                sum(dd.DtMD_fUnitWeight*PTD_Count*ifnull(DtMD_iFireBending/DtMD_iFireBending,0) ) hq,
                                sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_fBackOff) cb,
                                sum(dd.DtMD_fUnitWeight*PTD_Count*ifnull(DtMD_KaiHeJiao/DtMD_KaiHeJiao,0) ) khj,
                                sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_DaBian) yb,
                                sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_iBackGouging) qg,
                                sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_GeHuo) gh,
                    sum(PTD_count) zjzl,DtMD_sType")
                ->where("d.PT_Num",$PT_Num)
                ->order("clsort,cz,bh,gg")
                ->group('cz,gg,dtmd_sstuff')
    //          ->limit(100)
                ->select();
            // dump($mxdata);
        $mxsl = $this->detailModel->alias("d")
            ->join(["dtmaterialdetial"=>'dd'],"d.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->where("d.PT_Num",$PT_Num)->count();
    //        $ar=$mxdata[0]->toArray();
        $mxdata1=[];
        $tmp=$mxdata[0]->toArray();
        $cz=$tmp['cz'];
        $sumarr=$tmp;
        $zjsumarr=$tmp;
        $mxdata1[]=$tmp;
        foreach($mxdata as $k=>$v){
            if($k==0) continue;
            $tmp=$v->toArray();
            if($cz!=$tmp['cz']) {
                $sumarr['gg']='合计';
                $sumarr['cz']='';
                $mxdata1[]=$sumarr;
                $cz=$tmp['cz'];
                $sumarr=$tmp;
            }else{
                foreach($sumarr as $kk=>$vv){
                    if(in_array($kk,['gg','cz','DtMD_sType'])===false){
                        $sumarr[$kk]=($sumarr[$kk]+$tmp[$kk]);
                    }
                }
            }
            $mxdata1[]=$tmp;
            foreach($zjsumarr as $kk=>$vv){
                if(in_array($kk,['gg','cz','DtMD_sType'])===false){
                    $zjsumarr[$kk]=($zjsumarr[$kk]+$tmp[$kk]);
                }
            }
        }
        $sumarr['gg']='合计';
        $sumarr['cz']='';
        $mxdata1[]=$sumarr;
        $zjsumarr['gg']='总记录';
        $zjsumarr['cz']=$mxsl.'项';
        $mxdata1[]=$zjsumarr;

        $this->assign('mxdata1',$mxdata1);
        $this->assignconfig('mxdata1',$mxdata1);
        //明细3
        $mxdata3 = $this->detailModel->alias("d")
            ->join(["dtmaterialdetial"=>'dd'],"d.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->field("case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
                when dtmd_sstuff='角钢' then 
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
                when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
                REPLACE(DtMD_sSpecification,'-','')
                else 0
                end) as UNSIGNED) bh,dd.DtMD_sMaterial cz,dd.DtMD_sSpecification gg,
                (dd.DtMD_iUnitHoleCount) zjks,
                DtMD_sPartsID bjbh,
                case when LOCATE('-',DtMD_sPartsID)>0 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
                else 0 end 	bjbhn
                ,DtMD_iLength cd,DtMD_fWidth kd,PTD_Count sl,d.PT_Sect,
                DtMD_sRemark bz,round(DtMD_fUnitWeight,2) zl,
                DtMD_iCuttingAngle,                      
                DtMD_iFireBending,
                DtMD_iWelding,
                DtMD_fBackOff,
                DtMD_KaiHeJiao,
                DtMD_DaBian,
                DtMD_iBackGouging,
                DtMD_ZuanKong,
                DtMD_iTorch,
                DtMD_GeHuo
                ")
            ->where("d.PT_Num",$PT_Num)
            ->order("bjbhn,clsort,cz,bh,gg,bjbh")
            ->select();

        $group="PT_Num,PTS_Name";
        $useField = "sum(PTS_Count) as PTS_Count,PT_Num,PTS_Name as TS_Name";
        $usedList = $this->sectModel->field($useField)
            ->where("PT_Num",$PT_Num)
            ->group($group)
            ->select();
        $data = $sectList = [];
        foreach($usedList as $v){
            $sectList[$v["TS_Name"]] = $v["PTS_Count"];
        }
        foreach($mxdata3 as $k=>$v){
            
            $mxdata3[$k]["DD_Count"] = isset($sectList[$v["PT_Sect"]])?round(($v["sl"]/$sectList[$v["PT_Sect"]])):$v["PTD_Count"];
        }
        // $this->assignconfig('zlsum',round($zlsum,2));
        $this->assignconfig('mxdata3',$mxdata3);
        return $this->view->fetch();
    }
}
