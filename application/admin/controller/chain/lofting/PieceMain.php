<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\PieceDetail;
use app\admin\model\chain\lofting\PieceOperator;
use app\admin\model\chain\lofting\ProduceTask;
use app\admin\model\chain\lofting\ProduceTaskDetail;
use app\admin\model\chain\lofting\UnionProduceTaskView;
use app\admin\model\chain\sequence\Sequence;
use app\admin\model\chain\sequence\SequenceDetail;
use app\common\controller\Backend;
use fast\Tree;
use think\Db;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class PieceMain extends Backend
{
    
    /**
     * PieceMain模型对象
     * @var \app\admin\model\chain\lofting\PieceMain
     */
    protected $model = null,$operatorModel=null,$contentModel=null,$admin=null,$machineArr=null,$orderArr=null,$viewModel=null;
    protected $noNeedLogin = ["eipUpload","selectPeople","selectxd","getPieceList","selectMachine","searchContent","pieceList"];
    protected $content = [1=>"普通",2=>"电焊",3=>"弯曲",4=>"切角",5=>"铲背",6=>"清根",7=>"打扁",8=>"开合角",9=>"钻孔",10=>'割豁',11=>"制弯打扁开合角",15=>"等离子割板",16=>"板件钢印",17=>"角钢钢印"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\lofting\PieceMain;
        $this->operatorModel = new \app\admin\model\chain\lofting\PieceOperator;
        $this->contentModel = new \app\admin\model\chain\lofting\PieceContent;
        $this->viewModel = new \app\admin\model\chain\lofting\PieceArrangeView;
        $this->admin = \think\Session::get('admin');
        $this->machineArr = $this->machineList(["type"=>["=",2]]);
        $this->orderArr = [
            "TTGX01" => "下料",
            "TTGX02" => "制孔",
            "TTGX04" => "装配/组对/组装",
            "TTGX05" => "焊接",
            "TTGX06" => "镀锌",
            "TTGX07" => "制弯"
        ];
        $this->view->assign("orderArr",$this->orderArr);
        $this->assignconfig("orderArr",$this->orderArr);
        $this->assignconfig("machine_arr",$this->machineArr[0]);
        
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("pm")
                // ->join(["piece_operator"=>"po"],"po.piece_main_id=pm.id")
                ->join(["piece_detail"=>"pd"],"pm.id=pd.piece_id","left")
                ->field("pm.*,pt_num,(case when auditor='' then 0 else 1 end) as status")
                ->where($where)
                ->order("status asc")
                ->order($sort, $order)
                ->group("pm.id")
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }
    
    /**
     * 添加
     */
    public function add()
    {
        $this->view->assign("machine_list",[""=>"请选择"]+$this->machineArr[1]);
        $this->assignconfig("ids",0);
        $this->assignconfig("pt_list",[]);
        $this->assignconfig("list",[]);
        $this->assignconfig("flag",0);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $sj_operator = [];
        $operatorList = $this->operatorModel->where("piece_main_id",$ids)->order("operator asc")->column("operator as title,operator,hours");
        foreach($operatorList as $v){
            $sj_operator[] = $v["operator"]."：".$v["hours"];
        }
        $row["operator"] = json_encode($operatorList);
        $row["sj_operator"] = implode(",",$sj_operator);
        //1审核 0未审
        $flag = $row["auditor"]?1:0;
        $row["flag"] = $flag;
        $piece_detail_model = new PieceDetail();
        $list = $piece_detail_model->alias("pd")
            ->join(["piece_content"=>"pc"],"pd.piece_content_id=pc.id")
            ->field("case when (length(parts_id)-length(replace(parts_id,'-','')))>1 then 
            SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),1,LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))-1)*10000+cast(SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))+1) as signed)
            when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=3 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1+1) as signed)
            when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=4 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),2+1) as signed)
            when LENGTH(cast(parts_id as signed))=3 then SUBSTR(cast(parts_id as signed),1,1)*10000000+cast(SUBSTR(cast(parts_id as signed),1+1) as signed)
            when LENGTH(cast(parts_id as signed))=4 then SUBSTR(cast(parts_id as signed),1,2)*10000000+cast(SUBSTR(cast(parts_id as signed),2+1) as signed)
            else 0 end order_1,cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) as order_2,workmanship,pd.pt_num,piece_content_id as id,pd.id as piece_detail_id,pid,parts_id,stuff,material,specification,
            -- (case when ".$flag."=1 then (surplus_sum_weight+pd.sum_weight) else surplus_sum_weight end) as surplus_sum_weight_info,
            -- (case when ".$flag."=1 then (surplus_hole_number+pd.hole_number) else surplus_hole_number end) as surplus_hole_number_info,
            surplus_sum_weight as surplus_sum_weight_info,
            surplus_hole_number as surplus_hole_number_info,
            --(case when ".$flag."=1 then (surplus_sum_weight+pd.sum_weight) else pd.sum_weight end) as sum_weight,
            --(case when ".$flag."=1 then (surplus_hole_number+pd.hole_number) else pd.hole_number end) as hole_number,
            pd.sum_weight as sum_weight,
            pd.hole_number as hole_number,
            pd.sum_weight as surplus_sum_weight,
            pd.hole_number as surplus_hole_number,
            pd.sum_weight as surplus_sum_weight_input,
            pd.hole_number as surplus_hole_number_input")
            ->order("pd.pt_num,workmanship,stuff,material,order_2,order_1,parts_id")
            ->where("piece_id",$ids)->select();
        if($list) $list = collection($list)->toArray();
        else $list = [];
        $edit_list = $search_list = [];
        $content = $this->content;
        foreach($list as $k=>$v){
            $v["workmanship"] = $content[$v["workmanship"]];
            // $v["surplus_sum_weight_input"] = build_input('surplus_sum_weight_input', 'number', $v["surplus_sum_weight"]);
            // $v["surplus_hole_number_input"] = build_input('surplus_hole_number_input', 'number', $v["surplus_hole_number"]);
            $edit_list[$v["pt_num"]][] = $v;
            // $search_list[$v["pt_num"]]["stuff"][$v["stuff"]] = $v["stuff"];
            // $search_list[$v["pt_num"]]["specification"][$v["specification"]] = $v["specification"];
            // $search_list[$v["pt_num"]]["material"][$v["material"]] = $v["material"];
            // $search_list[$v["pt_num"]]["workmanship"][$v["workmanship"]] = $v["workmanship"];
        }
        foreach($edit_list as $k=>$v){
            Tree::instance()->init($v);
            $edit_list[$k] = Tree::instance()->getTreeList(Tree::instance()->getTreeArray(0), 'parts_id');
        }
        $tower_list = (new UnionProduceTaskView())->alias("pt")
            ->join(["taskdetail"=>"td"],"pt.TD_ID = td.TD_ID")
            ->field("PT_Num,CONCAT(TD_TypeName,'(',PT_Num,')') as TD_TypeName")
            ->where("PT_Num","IN",array_keys($edit_list))->select();
        if($tower_list) $tower_list = collection($tower_list)->toArray();
        else $tower_list = [];
        $this->view->assign("tower_list",$tower_list);
        $this->view->assign("row", $row);
        $this->assignconfig("flag",$flag);
        $this->assignconfig("ids",$ids);
        $this->assignconfig("pt_list",array_keys($edit_list));
        $this->assignconfig("list",$edit_list);
        // $this->view->assign("search_list",$search_list);
        $this->view->assign("machine_list",[""=>"请选择"]+$this->machineArr[1]);
        return $this->view->fetch();
    }

    public function selectxd()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new UnionProduceTaskView())->alias("p")
                ->join(["taskdetail"=>"d"],"p.TD_ID = D.TD_ID")
                ->join(["task"=>"t"],"t.T_Num=d.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->field("0 as status,p.*,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num',c.C_Num,c.C_Num as 'c.C_Num',c.PC_Num,c.PC_Num as 'c.PC_Num',d.TD_Pressure")
                ->where($where)
                ->where("p.Auditor","<>","")
                ->where("p.flag","<>","4")
                ->order("p.WriterDate desc")
                ->paginate($limit);
            $data = [];
            foreach($list as $v){
                $data[$v["PT_Num"]] = $v->toArray();
                // $ptNumList[$v["PT_Num"]] = $v["PT_Num"];
            }
            $pieceList = $this->contentModel->where("pt_num","IN",array_keys($data))->group("pt_num")->column("pt_num");
            foreach($pieceList as $v){
                $data[$v]["status"] = 1;
            }

            $result = array("total" => $list->total(), "rows" => array_values($data));

            return json($result);
        }
        return $this->view->fetch();
    }

    //要继续更新
    public function genPiece($ids=null)
    {
        if(!$ids) return json(["code"=>0,"msg"=>"生成失败，请重试"]);
        $one = $this->contentModel->where("pt_num",$ids)->find();
        // if($one) $this->error("已存在计件内容，如需更新计件内容，请更新列表后点击更新按键");
        if($one) $this->error("已存在计件内容");
        $workmanship_list = ['DtMD_iWelding'=>[2,'电焊'],'DtMD_iCuttingAngle'=>[4,'切角'],'DtMD_fBackOff'=>[5,'铲背'],'DtMD_iBackGouging'=>[6,'清根'],'DtMD_GeHuo'=>[10,'割豁'],'total'=>[11,'制弯打扁开合角',['DtMD_iFireBending','DtMD_DaBian','DtMD_KaiHeJiao']]];
        $ex = (new UnionProduceTaskView())->where("PT_Num",$ids)->value("sect_field");
        $detail_list = (new ProduceTaskDetail([],$ex))->alias("d")
            ->join([$ex."dtmaterialdetial"=>"dd"],"d.DtMD_ID_PK = dd.DtMD_ID_PK")
            //DtMD_iWelding电焊DtMD_iFireBending弯曲DtMD_iCuttingAngle切角DtMD_fBackOff铲背DtMD_iBackGouging清根DtMD_DaBian打扁DtMD_KaiHeJiao开合角DtMD_ZuanKong钻孔DtMD_GeHuo割豁
            ->field("d.*,ifnull(DtMD_iWelding,0) as DtMD_iWelding,ifnull(DtMD_iFireBending,0) as DtMD_iFireBending,ifnull(DtMD_iCuttingAngle,0) as DtMD_iCuttingAngle,ifnull(DtMD_fBackOff,0) as DtMD_fBackOff,ifnull(DtMD_iBackGouging,0) as DtMD_iBackGouging,ifnull(DtMD_DaBian,0) as DtMD_DaBian,ifnull(DtMD_KaiHeJiao,0) as DtMD_KaiHeJiao,ifnull(DtMD_GeHuo,0) as DtMD_GeHuo,DtMD_sPartsID as parts_id,DtMD_sStuff as stuff,DtMD_sMaterial as material,DtMD_sSpecification as specification,PTD_sumWeight as sum_weight,(PTD_Count*DtMD_iUnitHoleCount) as hole_number")
            ->where("d.PT_Num",$ids)->select();
        if(!$detail_list) $this->error("未生成生产明细");
        else{
            $detail_list = collection($detail_list)->toArray();
            $end_save_list = $this->_getGenerPiece($detail_list,$workmanship_list);
            $piece_result = $this->contentModel->allowField(true)->saveAll($end_save_list);
            $result = (new ProduceTask([],$ex))->where("PT_Num",$ids)->update(["flag"=>2]);
            if($piece_result) $this->success("生成成功");
            else $this->error("生成失败，请稍后重试");
        }
    }

    //要继续更新
    public function updatePiece($ids=null)
    {
        return false;
        if(!$ids) return json(["code"=>0,"msg"=>"生成失败，请重试"]);
        $workmanship_list = ['DtMD_iWelding'=>[2,'电焊'],'DtMD_iCuttingAngle'=>[4,'切角'],'DtMD_fBackOff'=>[5,'铲背'],'DtMD_iBackGouging'=>[6,'清根'],'DtMD_ZuanKong'=>[9,'钻孔'],'DtMD_GeHuo'=>[10,'割豁'],'total'=>[11,'制弯打扁开合角',['DtMD_iFireBending','DtMD_DaBian','DtMD_KaiHeJiao']]];
        $detail_list = (new ProduceTaskDetail())->alias("d")
            ->join(["dtmaterialdetial"=>"dd"],"d.DtMD_ID_PK = dd.DtMD_ID_PK")
            //DtMD_iWelding电焊DtMD_iFireBending弯曲DtMD_iCuttingAngle切角DtMD_fBackOff铲背DtMD_iBackGouging清根DtMD_DaBian打扁DtMD_KaiHeJiao开合角DtMD_ZuanKong钻孔DtMD_GeHuo割豁
            ->field("d.*,ifnull(DtMD_iWelding,0) as DtMD_iWelding,ifnull(DtMD_iFireBending,0) as DtMD_iFireBending,ifnull(DtMD_iCuttingAngle,0) as DtMD_iCuttingAngle,ifnull(DtMD_fBackOff,0) as DtMD_fBackOff,ifnull(DtMD_iBackGouging,0) as DtMD_iBackGouging,ifnull(DtMD_DaBian,0) as DtMD_DaBian,ifnull(DtMD_KaiHeJiao,0) as DtMD_KaiHeJiao,ifnull(DtMD_ZuanKong,0) as DtMD_ZuanKong,ifnull(DtMD_GeHuo,0) as DtMD_GeHuo,DtMD_sPartsID as parts_id,DtMD_sStuff as stuff,DtMD_sMaterial as material,DtMD_sSpecification as specification,PTD_sumWeight as sum_weight,(PTD_Count*DtMD_iUnitHoleCount) as hole_number")
            ->where("d.PT_Num",$ids)->select();
        if(!$detail_list) $this->error("未生成生产明细");
        else{
            $detail_list = collection($detail_list)->toArray();
            $end_save_list = $this->_getGenerPiece($detail_list,$workmanship_list);
            $ini_list = collection($this->contentModel->field("CONCAT(parts_id,'-',stuff,'-',specification,'-',material,'-',workmanship) as main_field,id,sum_weight,hole_number,surplus_sum_weight,surplus_hole_number")->where("pt_num",$ids)->select())->toArray();
            foreach($ini_list as $k=>$v){
                if(isset($end_save_list[$v["main_field"]])){
                    $end_save_list[$v["main_field"]]["id"] = $v["id"];
                    $check_weight = ($end_save_list[$v["main_field"]]["sum_weight"]??0)-($v["sum_weight"]??0);
                    $check_weight = $check_weight<=0?0:$check_weight;
                    $check_hole = ($end_save_list[$v["main_field"]]["hole_number"]??0)-($v["hole_number"]??0);
                    $check_hole = $check_hole<=0?0:$check_hole;
                    $end_save_list[$v["main_field"]]["surplus_sum_weight"] = $v["surplus_sum_weight"]+$check_weight;
                    $end_save_list[$v["main_field"]]["surplus_hole_number"] = $v["surplus_hole_number"]+$check_hole;
                }
            }
            $piece_result = $this->contentModel->allowField(true)->saveAll($end_save_list);
            if($piece_result) $this->success("更新成功");
            else $this->error("更新失败，请稍后重试");
        }
    }

    public function getPieceList()
    {
        $num = $this->request->post("num");
        if($num){
            $list = $this->contentModel
                ->field("*,case when (length(parts_id)-length(replace(parts_id,'-','')))>1 then 
                SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),1,LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))-1)*10000+cast(SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))+1) as signed)
                when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=3 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1+1) as signed)
                when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=4 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),2+1) as signed)
                when LENGTH(cast(parts_id as signed))=3 then SUBSTR(cast(parts_id as signed),1,1)*10000000+cast(SUBSTR(cast(parts_id as signed),1+1) as signed)
                when LENGTH(cast(parts_id as signed))=4 then SUBSTR(cast(parts_id as signed),1,2)*10000000+cast(SUBSTR(cast(parts_id as signed),2+1) as signed)
                else 0 end order_1,cast((case 
                when stuff='角钢' then 
                SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
                SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
                when (stuff='钢板' or stuff='钢管') then 
                REPLACE(specification,'-','')
                else 0
                end) as UNSIGNED) as order_2,surplus_sum_weight as surplus_sum_weight_info,surplus_hole_number as surplus_hole_number_info,
                0 as piece_detail_id,
                0 as surplus_sum_weight,
                0 as surplus_hole_number,
                0 as surplus_sum_weight_input,
                0 as surplus_hole_number_input")
                ->where("pt_num",$num)
                ->order("workmanship,stuff,material,order_2,order_1,parts_id")
                ->select();
            if($list) $list = collection($list)->toArray();
            else return json(["code"=>0,"msg"=>"有误，稍后重试"]);
            $parent = [];
            $search_list = [
                "stuff" => [],
                "specification" => [],
                "material" => [],
                "workmanship" => []
            ];
            $content = $this->content;
            foreach($list as $k=>$v){
                if(!$v["parts_id"]){
                    $parent[$v["material"].'-'.$v["specification"].'-'.$v["workmanship"]] = $v["id"];
                    $list[$k]["pid"] = 0;
                }else{
                    $list[$k]["pid"] = $parent[$v["material"].'-'.$v["specification"].'-'.$v["workmanship"]];
                }
                $list[$k]["workmanship"] = $content[$v["workmanship"]];
                // $list[$k]["piece_detail_id"] = 0;
                // $list[$k]["surplus_sum_weight"] = 0;
                // $list[$k]["surplus_hole_number"] = 0;
                // $list[$k]["surplus_sum_weight_input"] = build_input('surplus_sum_weight_input', 'number', 0);
                // $list[$k]["surplus_hole_number_input"] = build_input('surplus_hole_number_input', 'number', 0);
                $search_list["stuff"][$v["stuff"]] = $v["stuff"];
                $search_list["specification"][$v["specification"]] = $v["specification"];
                $search_list["material"][$v["material"]] = $v["material"];
                $search_list["workmanship"][$v["workmanship"]] = $content[$v["workmanship"]];
            }
            Tree::instance()->init($list);
            $rulelist = Tree::instance()->getTreeList(Tree::instance()->getTreeArray(0), 'parts_id');
            $search_bulid = [];
            foreach($search_list as $k=>$v){
                $search_bulid[$k] = build_select($k, (empty($v)?[""=>"请输入"]:([""=>"请输入"]+$v)), '', ['class'=>'form-control selectpicker',"autocomplete"=>"off"]);
            }
            return json(["code"=>1,"data"=>["list"=>$rulelist,"search"=>array_values($search_bulid)]]);
        }
        return json(["code"=>0,"msg"=>"有误，稍后重试"]);
    }

    public function savePiece($ids=0)
    {
        $main_data = $this->request->post("main_data");
        if($main_data) $main_data = json_decode($main_data,true);
        else return json(["code"=>0,"msg"=>"主信息内容必须全写！"]);
        $main_news = $piece_news = $pt_num_list = $operator_list = [];
        foreach($main_data as $v){
            if($v["value"]) $main_news[$v["name"]] = $v["value"];
        }
        if(round(($main_news["zg_amount"]??0),2) and round(($main_news["zg_hours"]??0),2)) return json(["code"=>0,"msg"=>"杂工工资和杂工时间不能同时填写"]);
        // if(!($main_news["device"]??false)) return json(["code"=>0,"msg"=>"必须选择设备号"]);
        if(!($main_news["order"]??false)) return json(["code"=>0,"msg"=>"必须填写工序"]);
        if(!($main_news["operator"]??false)) return json(["code"=>0,"msg"=>"必须填写操作人员"]);
        $operator_list = json_decode($main_news["operator"],true);
        unset($main_news["operator"]);
        $main_news["writer"] = $this->admin["nickname"];
        $table_data = $this->request->post("table_data");
        $table_data = $table_data?json_decode($table_data,true):[];
        if(!empty($table_data)){
            if(!($main_news["device"]??false)) return json(["code"=>0,"msg"=>"必须选择设备号"]);
            foreach($table_data as $v){
                foreach($v as $vv){
                    $pt_num_list[$vv["pt_num"]] = $vv["pt_num"];
                    $surplus_hole_number = $vv["surplus_hole_number"]?$vv["surplus_hole_number"]:0;
                    $surplus_sum_weight = $vv["surplus_sum_weight"]?$vv["surplus_sum_weight"]:0;
                    $need_sum_weight = $vv["sum_weight"]?$vv["sum_weight"]:0;
                    $need_hole_number = $vv["hole_number"]?$vv["hole_number"]:0;
                    if($surplus_hole_number){
                        if(!$surplus_sum_weight) $surplus_sum_weight = $need_hole_number?round($need_sum_weight/$need_hole_number*$surplus_hole_number,2):0;
                    }
                    $content = [
                        "id" => $vv["piece_detail_id"],
                        // "piece_id" => $vv["id"],
                        "pt_num" => $vv["pt_num"],
                        "pid" => $vv["pid"]?$vv["pid"]:0,
                        // "piece_content_id" => $piece_mian_id,
                        "piece_content_id" => $vv["id"],
                        "sum_weight" => $surplus_sum_weight,
                        "hole_number" => $surplus_hole_number,
                        "need_sum_weight" => $need_sum_weight,
                        "need_hole_number" => $need_hole_number
                    ];
                    if(!$vv["piece_detail_id"]) unset($content["id"]);
                    if($vv["pid"]==0){
                        $piece_news[$vv["id"]] = $content;
                        isset($piece_news[$vv["id"]]["list"])?"":$piece_news[$vv["id"]]["list"] = [];
                    }else $piece_news[$vv["pid"]]["list"][$vv["id"]] = $content;
                }
            }
            $unionModel = (new UnionProduceTaskView());
            // $produce_task_model = (new ProduceTask());
            $piece_detail_model = (new PieceDetail());
            $pt_state = $unionModel->field("PT_Num,flag")->where([
                "PT_Num" => ["IN",$pt_num_list],
                "flag" => ["NOT IN",[2,3]]
            ])->find();
            if($pt_state) return json(["code"=>0,"msg"=>"生产任务单号为".$pt_state["PT_Num"]."的单子无法计件"]);
        }
        $del_list = [];
        $result = $delresult = false;
        $sequenceModel = new Sequence();
        $sequenceDetailModel = new SequenceDetail();
        Db::startTrans();
        try {
            if($ids){
                $this->model->save($main_news,["id"=>$ids]);
                $piece_mian_id = $ids;
            }else $piece_mian_id = $this->model->insertGetId($main_news);
            foreach($operator_list as $k=>$v){
                unset($operator_list[$k]["title"]);
                $operator_list[$k]["piece_main_id"] = $piece_mian_id;
            }
            $this->operatorModel->where("piece_main_id",$piece_mian_id)->delete();
            $this->operatorModel->allowField(true)->saveAll($operator_list);
            if(!empty($piece_news)){
                foreach($piece_news as $k=>$v){
                    $v["piece_id"] = $piece_mian_id;
                    $weight = $number = 0;
                    if(isset($v["list"])){
                        foreach($v["list"] as $kk=>$vv){
                            $vv["piece_id"] = $piece_mian_id;
                            $weight += $vv["sum_weight"];
                            $number += $vv["hole_number"];
                            $v["list"][$kk] = $vv;
                        }
                    }
                    if(!$weight and !$v["sum_weight"]){
                        $del_list[$v["piece_content_id"]] = $v["piece_content_id"];
                        unset($piece_news[$k]);
                        continue;
                    }else{
                        $v["sum_weight"] = $weight?$weight:$v["sum_weight"];
                        $v["hole_number"] = $number?$number:$v["hole_number"];
                    }
                    $piece_news[$k] = $v;
                }
                $piece_news_list = [];
                foreach($piece_news as $k=>$v){
                    foreach($v["list"] as $kk=>$vv){
                        $piece_news_list[] = $vv;
                    }
                    unset($v["list"]);
                    $piece_news_list[] = $v;
                }
                if(!empty($del_list)) $delresult = $piece_detail_model->where("piece_content_id","IN",$del_list)->whereOr("pid","IN",$del_list)->delete();
                $result = $piece_detail_model->allowField(true)->saveAll($piece_news_list);
                // $produce_task_model->field("PT_Num,flag")->where([
                //     "PT_Num" => ["IN",$pt_num_list]
                // ])->update(["flag"=>3]);
                //主信息 ptList
                [$mainSaveData,$mainList,$detailList] = $this->_pcPlanFun($main_news,$pt_num_list);
                $sequenceModel->allowField(true)->saveAll($mainSaveData,false);
                $sequenceModel->allowField(true)->saveAll($mainList);
                $sequenceDetailModel->allowField(true)->saveAll($detailList);
            }
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($piece_mian_id) {
            return json(["code"=>1,"msg"=>"保存成功","data"=>$piece_mian_id]);
        } else {
            return json(["code"=>0,"msg"=>__('No rows were updated')]);
        }
    }

    /**
     * main_news 主信息
     * pt_num_list 任务下达单号
     */
    protected function _pcPlanFun($main_news,$pt_num_list)
    {
        if(empty($main_news) or empty($pt_num_list)) return false;
        $sequenceModel = new Sequence();
        $sequenceDetailModel = new SequenceDetail();
        $orderArr = $this->_orderList();
        $whereMain = [
            "PT_Num"=>["IN",$pt_num_list]
        ];
        $ex = (new UnionProduceTaskView())->where($whereMain)->value("sect_field");
        $produceTaskDetailModel = new ProduceTaskDetail([],$ex);
        $sNum = "PC".date("Ymd");
        $getOne = $sequenceModel->where("sNum","LIKE",'PC'.date('Ymd').'%')->order("sNum desc")->value("right(sNum,3)");
        if($getOne) $num = $getOne+1;
        else $num = 1;
        // $sNum .= $num;
        [$mainList,$ptData] = $sequenceModel->getMainData($whereMain);
        $detailList = $sequenceDetailModel->getDetailData(["sNum"=>["IN",array_keys($mainList)]]);
        //获取最早时间和最晚时间以及判断是否完成
        $timeWhere = [
            "order" => ["<>",''],
            "pd.pt_num" => ["IN",$pt_num_list]
        ];
        $viewWhere = [
            "workmanship" => ["IN",[1,2,11]],
            "pt_num" => ["IN",$pt_num_list],
            "sj_sum_weight" => [">",0]
        ];
        $timeList = $this->model->getTimeData($timeWhere,$viewWhere);

        $mainSaveData = $detailSaveData = [];
        foreach($pt_num_list as $v){
            if(!isset($ptData[$v])){
                $type = $produceTaskDetailModel->getType($v,$ex);
                $orderList = $orderArr;
                //0既不焊接也不制弯 1焊接不制弯 2制弯不焊接 3all
                if($type==2) unset($orderList["TTGX05"],$orderList["TTGX04"]);
                else if($type==1) unset($orderList["TTGX07"]);
                else if(!$type) unset($orderList["TTGX05"],$orderList["TTGX04"],$orderList["TTGX07"]);
                $sjNum = $sNum.str_pad($num,3,0,STR_PAD_LEFT);
                $mainSaveData[] = ["sNum"=>$sjNum];
                $ptData[$v] = $sjNum;
                $mainList[$sjNum] = [
                    "sNum" => $sjNum,
                    "PT_Num" => $v,
                    "choose_order" => ",".implode(",",array_keys($orderList)).",",
                    "PlanStartTime" => $main_news["record_time"],
                    "PlanFinishTime" => date("Y-m-d H:i:s",strtotime("+45 day",strtotime($main_news["record_time"]))),
                    "writer" => $this->admin["username"],
                    "ActStartTime" => $main_news["record_time"],
                    "ActFinishTime" => "0000-00-00 00:00:00"
                ];
                $day = intval(45/count($orderList));
                $js = 0;
                foreach($orderList as $kk=>$vv){
                    $detailList[$sjNum][$kk] = [
                        "sNum" => $sjNum,
                        "orderId" => $kk,
                        "PlanStartTime" => date("Y-m-d H:i:s",strtotime("+".($js*$day)." day",strtotime($main_news["record_time"]))),
                        "PlanFinishTime" => date("Y-m-d H:i:s",strtotime("+".(($js+1)*$day)." day",strtotime($main_news["record_time"]))),
                        "writer" => $this->admin["username"],
                        "ActStartTime" => $kk==$main_news["order"]?$main_news["record_time"]:"0000-00-00 00:00:00",
                        "ActFinishTime" => "0000-00-00 00:00:00"
                    ];
                    $js++;
                }
                $num++;
            }
            if(isset($timeList[$v])){
                $flag = true;
                foreach($timeList[$v] as $lk=>$lv){
                    if(isset($detailList[$ptData[$v]][$lk])){
                        $detailList[$ptData[$v]][$lk]["ActStartTime"] = $lv["startTime"];
                        $detailList[$ptData[$v]][$lk]["ActFinishTime"] = $lv["endTime"];
                    }
                    if($lv["endTime"] == "0000-00-00 00:00:00") $flag = false;
                }
                if($flag){
                    $timeSortList = array_column($detailList[$ptData[$v]],"ActFinishTime");
                    rsort($timeSortList);
                    $maxTime = $timeSortList[0];
                    if($maxTime!="0000-00-00 00:00:00"){
                        $detailList[$ptData[$v]]["TTGX06"]["ActStartTime"] = $maxTime;
                        $detailList[$ptData[$v]]["TTGX06"]["ActFinishTime"] = $maxTime>$mainList[$ptData[$v]]["PlanFinishTime"]?date("Y-m-d H:i:s",strtotime("+5 day",strtotime($maxTime))):$mainList[$ptData[$v]]["PlanFinishTime"];
                        $mainList[$ptData[$v]]["ActFinishTime"] = $detailList[$ptData[$v]]["TTGX06"]["ActFinishTime"];
                    }
                    
                }
            }
        }
        foreach($detailList as $v){
            foreach($v as $vv){
                $detailSaveData[] = $vv;
            }
        }
        return [$mainSaveData,$mainList,$detailSaveData];
    }

    public function deletePiece($ids=0)
    {
        $num = $this->request->post("num");
        $num = trim($num,"#");
        if(!$ids or !$num) return json(["code"=>0,"msg"=>"删除失败"]);
        $is_exist_one = (new PieceDetail())->where([
            "piece_id" => ["=",$ids],
            "pt_num" => ["=",$num]
        ])->find();
        if(!$is_exist_one) return json(["code"=>1,"msg"=>"删除成功"]);
        $content_one = $this->model->get($ids);
        if($content_one["auditor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);
        $piece_detail_model = new PieceDetail();
        Db::startTrans();
        try {
            $result = $piece_detail_model->where([
                "piece_id" => ["=",$ids],
                "pt_num" => ["=",$num]
            ])->delete();
            $one = $piece_detail_model->where("pt_num",$num)->find();
            // if(!$one) (new ProduceTask())->where("pt_num",$num)->update(["flag"=>2]);
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        return json(["code"=>1,"msg"=>"删除成功"]);
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        list($msg,$content_list,$pt_num) = $this->examine($num,0);
        $piece_content_model = $this->contentModel;
        if($msg) return json(["code"=>0,"msg"=>$msg]);
        $result = false;
        Db::startTrans();
        try {
            $result = $piece_content_model->allowField(true)->saveAll($content_list);
            $main_result = $this->model->update(["auditor"=>$this->admin["nickname"],"auditor_time"=>date("Y-m-d H:i:s")],["id"=>$num]);
            $pt_exist_list = $piece_content_model->field("pt_num")->where([
                "parts_id"=>["=",""],
                "surplus_sum_weight"=>["<>",0],
                // "surplus_hole_number"=>["<>",0],
            ])->group("pt_num")->select();
            foreach($pt_exist_list as $v){
                unset($pt_num[$v["pt_num"]]);
            }
            // if(!empty($pt_num)) (new ProduceTask())->where("pt_num","IN",$pt_num)->update(["flag"=>4]);
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        return json(["code"=>1,"msg"=>"审核成功"]);
    }
    public function giveUp()
    {
        $num = $this->request->post("num");
        list($msg,$content_list,$pt_num) = $this->examine($num,1);
        $piece_content_model = $this->contentModel;
        if($msg) return json(["code"=>0,"msg"=>$msg]);
        $result = false;
        Db::startTrans();
        try {
            $result = $piece_content_model->allowField(true)->saveAll($content_list);
            $main_result = $this->model->update(["auditor"=>'',"auditor_time"=>"0000-00-00 00:00:00"],["id"=>$num]);
            $pt_exist_list = $piece_content_model->field("pt_num")->where([
                "parts_id"=>["=",""],
                "surplus_sum_weight"=>["<>",0],
                // "surplus_hole_number"=>["<>",0],
            ])->group("pt_num")->select();
            $pt_num = [];
            foreach($pt_exist_list as $v){
                $pt_num[$v["pt_num"]] = $v["pt_num"];
            }
            // if(!empty($pt_num)) (new ProduceTask())->where("pt_num","IN",$pt_num)->update(["flag"=>3]);
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        return json(["code"=>1,"msg"=>"弃审成功"]);
    }

    //flag 0审核 1弃审
    protected function examine($num,$flag=0)
    {
        $piece_content_model = $this->contentModel;
        $piece_detail_model = new PieceDetail();
        $detail_list_data = $pt_num = [];
        $detail_list = $piece_detail_model->field("id,pt_num,pid,piece_content_id,sum_weight,hole_number")->where("piece_id",$num)->select();
        foreach($detail_list as $v){
            $detail_list_data[$v["piece_content_id"]] = $v->toArray();
            $pt_num[$v["pt_num"]] = $v["pt_num"];
        }
        $content_list = $piece_content_model
                ->field("*,case when (length(parts_id)-length(replace(parts_id,'-','')))>1 then 
                SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),1,LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))-1)*10000+cast(SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))+1) as signed)
                when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=3 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1+1) as signed)
                when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=4 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),2+1) as signed)
                when LENGTH(cast(parts_id as signed))=3 then SUBSTR(cast(parts_id as signed),1,1)*10000000+cast(SUBSTR(cast(parts_id as signed),1+1) as signed)
                when LENGTH(cast(parts_id as signed))=4 then SUBSTR(cast(parts_id as signed),1,2)*10000000+cast(SUBSTR(cast(parts_id as signed),2+1) as signed)
                else 0 end order_1,cast((case 
                when stuff='角钢' then 
                SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
                SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
                when (stuff='钢板' or stuff='钢管') then 
                REPLACE(specification,'-','')
                else 0
                end) as UNSIGNED) as order_2")
                ->where("id","IN",array_keys($detail_list_data))
                ->order("workmanship,stuff,material,order_2,order_1,parts_id")
                ->select();
        $content_save_list = $content_arr = $sum_weight_more = $hole_number_more = [];
        foreach($content_list as $v){
            $key = $v["pt_num"].'-'.$v["stuff"].'-'.$v["specification"].'-'.$v["material"].'-'.$v["workmanship"];
            if(!isset($content_arr[$key])) $content_arr[$key] = ["main"=>[],"detail"=>[]];
            if($v["parts_id"]) $content_arr[$key]["detail"][$v["id"]] = $v->toArray();
            else $content_arr[$key]["main"] = $v->toArray();
        }
        foreach($content_arr as $k=>$v){
            $pid = $v["main"]["id"];
            // $content_end[$pid] = $v["main"];
            // $content_end[$pid]["detail"] = $v["detail"];
            if($flag==0){
                if($detail_list_data[$pid]["sum_weight"]==$v["main"]["surplus_sum_weight"]){
                    $content_save_list[$pid] = ["id"=>$pid ,"surplus_sum_weight"=>0,"surplus_hole_number"=>0];
                    foreach($v["detail"] as $kk=>$vv){
                        $content_save_list[$kk] = ["id"=>$kk ,"surplus_sum_weight"=>0,"surplus_hole_number"=>0];
                    }
                }else{
                    $surplus_sum_weight = $v["main"]["surplus_sum_weight"]-$detail_list_data[$pid]["sum_weight"];
                    $surplus_hole_number = $v["main"]["surplus_hole_number"]-$detail_list_data[$pid]["hole_number"];
                    if($surplus_sum_weight<0) $sum_weight_more[$pid] = $pid;
                    if($surplus_hole_number<0) $hole_number_more[$pid] = $pid;
                    $content_save_list[$pid] = [
                        "id"=>$pid,
                        "surplus_sum_weight" => $surplus_sum_weight,
                        "surplus_hole_number" => $surplus_hole_number
                    ];
                    foreach($v["detail"] as $kk=>$vv){
                        $surplus_sum_weight = $vv["surplus_sum_weight"]-$detail_list_data[$kk]["sum_weight"];
                        $surplus_hole_number = $vv["surplus_hole_number"]-$detail_list_data[$kk]["hole_number"];
                        if($surplus_sum_weight<0) $sum_weight_more[$kk] = $kk;
                        if($surplus_hole_number<0) $hole_number_more[$kk] = $kk;
                        $content_save_list[$kk] = [
                            "id"=>$kk,
                            "surplus_sum_weight"=>$surplus_sum_weight,
                            "surplus_hole_number"=>$surplus_hole_number
                        ];
                    }
                }
            }else{
                if($detail_list_data[$pid]["sum_weight"]==$v["main"]["sum_weight"]){
                    $content_save_list[$pid] = ["id"=>$pid ,"surplus_sum_weight"=>$v["main"]["sum_weight"],"surplus_hole_number"=>$v["main"]["hole_number"]];
                    foreach($v["detail"] as $kk=>$vv){
                        $content_save_list[$kk] = ["id"=>$kk ,"surplus_sum_weight"=>$vv["sum_weight"],"surplus_hole_number"=>$vv["hole_number"]];
                    }
                }else{
                    $surplus_sum_weight = $v["main"]["surplus_sum_weight"]+$detail_list_data[$pid]["sum_weight"];
                    $surplus_hole_number = $v["main"]["surplus_hole_number"]+$detail_list_data[$pid]["hole_number"];
                    if($surplus_sum_weight>$v["main"]["sum_weight"]) $sum_weight_more[$pid] = $pid;
                    if($surplus_hole_number>$v["main"]["hole_number"]) $hole_number_more[$pid] = $pid;
                    $content_save_list[$pid] = [
                        "id"=>$pid,
                        "surplus_sum_weight" => $surplus_sum_weight,
                        "surplus_hole_number" => $surplus_hole_number
                    ];
                    foreach($v["detail"] as $kk=>$vv){
                        $surplus_sum_weight = $vv["surplus_sum_weight"]+$detail_list_data[$kk]["sum_weight"];
                        $surplus_hole_number = $vv["surplus_hole_number"]+$detail_list_data[$kk]["hole_number"];
                        if($surplus_sum_weight>$vv["sum_weight"]) $sum_weight_more[$kk] = $kk;
                        if($surplus_hole_number>$vv["hole_number"]) $hole_number_more[$kk] = $kk;
                        $content_save_list[$kk] = [
                            "id"=>$kk,
                            "surplus_sum_weight"=>$surplus_sum_weight,
                            "surplus_hole_number"=>$surplus_hole_number
                        ];
                    }
                }
            }
        }
        $msg = "";
        if(!empty($sum_weight_more)) $msg .= "id为".implode(",",$sum_weight_more)."登记数量有误<br>";
        if(!empty($hole_number_more)) $msg .= "id为".implode(",",$hole_number_more)."登记孔数有误<br>";
        return [$msg,$content_save_list,$pt_num];
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $one = $this->model->get($ids);
            if($one["auditor"]) $this->error('已审核，删除失败');
            $count = 0;
            Db::startTrans();
            try {
                $count += $this->model->where("id",$ids)->delete();
                $count += (new PieceDetail())->where("piece_id",$ids)->delete();
                $count += (new PieceOperator())->where("piece_main_id",$ids)->update();
                Db::commit();
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function selectMachine()
    {
        $num = $this->request->post("num");
        $arr = [];
        if($num){
            $list = $this->model->alias("pm")
                ->join(["piece_operator"=>"po"],"pm.id=po.piece_main_id")
                ->field("po.operator")
                ->where([
                    "pm.device" => ["=",$num],
                    "po.operator" => ["<>",""]
                ])
                ->group("po.operator")
                ->order("record_time desc")
                ->select();
            foreach($list as $v){
                $arr[$v["operator"]] = $v["operator"];
            }
            // $arr = ["小金"=>"小金","小李"=>"小李"];
            $select_field = build_select('operator_recent', $arr, "", ['class'=>'form-control selectpicker',"id"=>"c-operator_recent","multiple"=>""]);
            return json(["code"=>1,"data"=>$select_field]);
        }
        $select_field = build_select('operator_recent', $arr, "", ['class'=>'form-control selectpicker',"id"=>"c-operator_recent","multiple"=>""]);
        return json(["code"=>0,"data"=>$select_field]);
    }

    public function delDetail($piece_id=0,$pid=0)
    {
        // ids为piece_id
        // 如果ids为0 则取pid是否有parts_id 则新增 1 直接删除 右侧数组的pid删除 2 单条重量孔数为0
        // 如果ids有值 则修改 则搜索piece_detail 是否有


        //type 1 pid 父集+子集所有 删除 右侧数组中也删除pid
        //type 0 pid 子集 某一列登记重量和孔数为0

        if(!$pid) $this->error("稍等重试");//return json(["code"=>0,"msg"=>"稍等重试"]);
        $piece_detail_model = new PieceDetail();
        $piece_content_model = $this->contentModel;
        if($piece_id){
            $one = $this->model->get($piece_id);
            if($one["auditor"]) $this->error('已审核，删除失败');

            $piece_detail_one = $piece_detail_model->where([
                "piece_id" => ["=",$piece_id],
                "piece_content_id" => ["=",$pid]
            ])->find();
            if($piece_detail_one){
                if($piece_detail_one["pid"]){
                    $update_result = $piece_detail_model->where("id",$piece_detail_one["id"])->update(["sum_weight"=>0,"hole_number"=>0]);
                    $this->success("成功",null,[0,$pid]);
                    // return json(["code"=>1,"data"=>["type"=>0,"piece_content_id"=>$pid]]);
                }else{
                    $this->where_1 = [
                        "piece_id" => ["=",$piece_id],
                        "piece_content_id" => ["=",$pid]
                    ];
                    $this->where_2 = [
                        "piece_id" => ["=",$piece_id],
                        "pid" => ["=",$pid]
                    ];
                    $del_result = $piece_detail_model->where(function ($query) {
                        $query->where($this->where_1);
                    })->whereOr(function ($query) {
                        $query->where($this->where_2);
                    })->delete();
                    $this->success("成功",null,[1,$pid]);
                    // return json(["code"=>1,"data"=>["type"=>1,"piece_content_id"=>$pid]]);
                }
            }else{
                $piece_content_one = $piece_content_model->where([
                    "id" => ["=",$pid],
                ])->find();
                if($piece_content_one){
                    if($piece_content_one["parts_id"]) $this->success("成功",null,[0,$pid]);
                    else $this->success("成功",null,[1,$pid]);// return json(["code"=>1,"type"=>1,"piece_content_id"=>$pid]);
                }else $this->error("稍等重试");//return json(["code"=>0,"msg"=>"稍等重试"]);
            }
        }else{
            $piece_content_one = $piece_content_model->where([
                "id" => ["=",$pid],
            ])->find();
            if($piece_content_one){
                if($piece_content_one["parts_id"]) return json(["code"=>1,"type"=>0,"piece_content_id"=>$pid]);
                else $this->success("成功",null,[1,$pid]);// return json(["code"=>1,"type"=>1,"piece_content_id"=>$pid]);
            }else $this->error("稍等重试");//return json(["code"=>0,"msg"=>"稍等重试"]);
        }
    }

    public function searchContent()
    {
        list($pt_num,$stuff,$specification,$material,$workmanship) = array_values($this->request->post());
        if(!$pt_num) return json(["code"=>0,"msg"=>"有误，稍后重试"]);
        $where = ["PT_Num"=>["=",$pt_num]];
        if($stuff) $where["stuff"] = ["=",$stuff];
        if($specification) $where["specification"] = ["=",$specification];
        if($material) $where["material"] = ["=",$material];
        if($workmanship) $where["workmanship"] = ["=",$workmanship];
        $list = $this->contentModel
            ->field("*,case when (length(parts_id)-length(replace(parts_id,'-','')))>1 then 
            SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),1,LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))-1)*10000+cast(SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))+1) as signed)
            when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=3 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1+1) as signed)
            when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=4 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),2+1) as signed)
            when LENGTH(cast(parts_id as signed))=3 then SUBSTR(cast(parts_id as signed),1,1)*10000000+cast(SUBSTR(cast(parts_id as signed),1+1) as signed)
            when LENGTH(cast(parts_id as signed))=4 then SUBSTR(cast(parts_id as signed),1,2)*10000000+cast(SUBSTR(cast(parts_id as signed),2+1) as signed)
            else 0 end order_1,cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) as order_2,surplus_sum_weight as surplus_sum_weight_info,surplus_hole_number as surplus_hole_number_info,
            0 as piece_detail_id,
            0 as surplus_sum_weight,
            0 as surplus_hole_number,
            0 as surplus_sum_weight_input,
            0 as surplus_hole_number_input")
            ->where($where)
            ->order("workmanship,stuff,material,order_2,order_1,parts_id")
            ->select();
        if($list) $list = collection($list)->toArray();
        else return json(["code"=>0,"msg"=>"有误，稍后重试"]);
        $parent = [];
        $content = $this->content;
        foreach($list as $k=>$v){
            if(!$v["parts_id"]){
                $parent[$v["material"].'-'.$v["specification"].'-'.$v["workmanship"]] = $v["id"];
                $list[$k]["pid"] = 0;
            }else{
                $list[$k]["pid"] = $parent[$v["material"].'-'.$v["specification"].'-'.$v["workmanship"]];
            }
            $list[$k]["workmanship"] = $content[$v["workmanship"]];
        }
        Tree::instance()->init($list);
        $rulelist = Tree::instance()->getTreeList(Tree::instance()->getTreeArray(0), 'parts_id');
        return json(["code"=>1,"data"=>$rulelist]);
    }

    public function pieceList($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $piece_detail_model = new PieceDetail();
        $list = $piece_detail_model->alias("pd")
            ->join(["piece_content"=>"pc"],"pd.piece_content_id=pc.id")
            ->field("case when (length(parts_id)-length(replace(parts_id,'-','')))>1 then 
            SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),1,LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))-1)*10000+cast(SUBSTR(SUBSTR(parts_id,LOCATE('-',parts_id)+1),LOCATE('-',SUBSTR(parts_id,LOCATE('-',parts_id)+1))+1) as signed)
            when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=3 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1+1) as signed)
            when LOCATE('-',parts_id)>0 and LENGTH(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed))=4 then SUBSTR(parts_id,1,LOCATE('-',parts_id)-1)*10000000+SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(parts_id,LOCATE('-',parts_id)+1) as signed),2+1) as signed)
            when LENGTH(cast(parts_id as signed))=3 then SUBSTR(cast(parts_id as signed),1,1)*10000000+cast(SUBSTR(cast(parts_id as signed),1+1) as signed)
            when LENGTH(cast(parts_id as signed))=4 then SUBSTR(cast(parts_id as signed),1,2)*10000000+cast(SUBSTR(cast(parts_id as signed),2+1) as signed)
            else 0 end order_1,cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) as order_2,workmanship,pd.pt_num,piece_content_id as id,pd.id as piece_detail_id,pid,parts_id,stuff,material,specification,
            pd.sum_weight as sum_weight,
            pd.hole_number as hole_number,
            pd.sum_weight as surplus_sum_weight,
            pd.hole_number as surplus_hole_number,
            pd.sum_weight as surplus_sum_weight_input,
            pd.hole_number as surplus_hole_number_input")
            ->order("pd.pt_num,workmanship,stuff,material,order_2,order_1,parts_id")
            ->where("piece_id",$ids)->select();
        if($list) $list = collection($list)->toArray();
        else $list = [];
        $edit_list = [];
        $content = $this->content;
        foreach($list as $k=>$v){
            if($v["sum_weight"] == 0 and $v["hole_number"] == 0) continue;
            $v["workmanship"] = $content[$v["workmanship"]];
            $edit_list[$v["pt_num"]][] = $v;
        }
        foreach($edit_list as $k=>$v){
            Tree::instance()->init($v);
            $edit_list[$k] = Tree::instance()->getTreeList(Tree::instance()->getTreeArray(0), 'parts_id');
        }
        $tower_list = (new UnionProduceTaskView())->alias("pt")
            ->join(["taskdetail"=>"td"],"pt.TD_ID = td.TD_ID")
            ->field("PT_Num,CONCAT(TD_TypeName,'(',PT_Num,')') as TD_TypeName")
            ->where("PT_Num","IN",array_keys($edit_list))->select();
        if($tower_list) $tower_list = collection($tower_list)->toArray();
        else $tower_list = [];
        $this->view->assign("tower_list",$tower_list);
        $this->view->assign("row", $row);
        $this->assignconfig("ids",$ids);
        $this->assignconfig("pt_list",array_keys($edit_list));
        $this->assignconfig("list",$edit_list);
        return $this->view->fetch();
    }

    public function selectPeople($machine = null,$ids = 0)
    {
        if(!$machine) $this->error("请先选择机器！");
        if($ids)  $list = $this->operatorModel->where("piece_main_id",$ids)->order("operator asc")->column("operator as title,operator,hours");
        else{
            $list = $this->model->alias("pm")
                ->join(["piece_operator"=>"po"],"pm.id=po.piece_main_id","left")
                ->where("pm.device",$machine)
                ->where("pm.record_time",">=",date("Y-m-d 00:00:00",strtotime("-10 days")))
                ->group("po.operator")
                ->order("po.operator")
                ->column("po.operator as title,po.operator,8 as hours");
        }
        $this->assignconfig("list",array_values($list));
        return $this->view->fetch();
    }

    public function editAmount($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(round(($params["zg_amount"]??0),2) and round(($params["zg_hours"]??0),2)) return json(["code"=>0,"msg"=>"杂工工资和杂工时间不能同时填写"]);
            $result = $this->model->where("id",$ids)->update([
                "zg_amount" => $params["zg_amount"],
                "zg_hours" => $params["zg_hours"]
            ]);
            if ($result != false) {
                $this->success();
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        $this->view->assign("row",$row);
        return $this->view->fetch();
    }

    // public function eipUpload()
    // {
    //     $id = $this->request->post("id");
    //     $json_result = ["code"=>0,"msg"=>"失败"];
    //     $where = [
    //         "id" => ["=",$id]
    //     ];
    //     $params = (new \app\admin\model\view\PieceDetailView())->where($where)->select();
    //     if(!$params) return json($json_result);
    //     $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
    //     $saveData = [];
    //     $param_url = "supplier-process-work";
    //     foreach($params as $k=>$v){
    //         $operatetype = "ADD";
    //         $api_data = [
    //             "purchaserHqCode" => "SGCC",
    //             "supplierCode" => "1000014615",
    //             "ipoNo" => $v["T_Num"],
    //             "deviceNo" => $this->machineArr[0][$v["device"]],
    //             "productBatchNo" => date("Y/m/d"),
    //             "processName" => $v["order"]?$v["order"]:'TTGX01',
    //             "categoryCode" => "60",
    //             "subclassCode" => "60001",
    //             "processCode" => "BG".$v["id"],
    //             "woNo" => $v["pt_num"],
    //             "dataSource" => 0,
    //             "dataSourceCreateTime"=>date("Y-m-d H:i:s"),
    //             "buyerProvince" => $v["PC_Place"]
    //         ];
    //         if($v["eip_upload"]) $operatetype = 'UPDATE';
    //         $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$api_data);
    //         if($api_result["code"]==1) $this->model->where("id",$id)->update(["eip_upload"=>1]);
    //         else $saveData[$v["id"]] = $api_result["msg"];
    //     }
    //     $msg = "上传成功";
    //     if(!empty($saveData)) {
    //         $msg = "";
    //         foreach($saveData as $k=>$v){
    //             $msg .= $k.$v.";";
    //         }
    //         $json_result["msg"] = $msg;
    //     }else $json_result = ["code"=>1,"msg"=>$msg];
    //     return json($json_result);
    // }
}
