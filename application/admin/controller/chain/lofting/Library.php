<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\BaseLibrary;
use app\admin\model\chain\lofting\BaseLibraryDetail;
use app\admin\model\chain\lofting\BaseLibrarySect;
use app\admin\model\chain\lofting\DhCooperatek;
use app\admin\model\chain\lofting\DhCooperatekDetail;
use app\admin\model\chain\lofting\DhCooperatekSingle;
use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\model\chain\lofting\Dtmaterialdetail;
use app\admin\model\chain\lofting\Dtsect;
use app\admin\model\jichu\ch\InventoryMaterial;
use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;

/**
 * 放样塔形库主管理
 *
 * @icon fa fa-circle-o
 */
class Library extends Technology
{
    
    /**
     * Library模型对象
     * @var \app\admin\model\chain\lofting\Library
     */
    protected $model = null,$detailModel=null,$sectModel=null,$slabModel=null,$admin=null;
    protected $noNeedLogin = ["copySect","downField","importView","export","editDetailAll","downDetailContent","downFieldList"];
    //0name 1field 2type 3class 4readonly 5defauld 6hidden
    protected $tableField = [
        ["DtS_ID_PK","DtS_ID_PK","text","","readonly",0,"hidden"],
        ["*段名","DtS_Name","text","","readonly data-rule='required'","",""],
        ["明细件号数","detailNum","text","","readonly",0,""],
        ["总重量","detailWeight","text","","readonly",0,""],
        ["制表人","DtS_sWriter","text","","readonly","",""],
        ["制表时间","DtS_dWriterTime","text","","readonly","",""],
        // ["审核人","DtS_sAuditor","text","","readonly","",""],
        // ["审核时间","DtS_sAuditDate","text","","readonly","",""],
        ["图号","DtS_sPictureNum","text","","","",""],
        ["备注","DtS_sRemark","text","","","",""],
    ];
    protected $delFlag = true;
    protected $editFlag = true;

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_field = \think\Session::get('technology_field');
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->model = new \app\admin\model\chain\lofting\Library([],$this->technology_ex);
        $this->detailModel = new \app\admin\model\chain\lofting\Fytxkdetail([],$this->technology_ex);
        $this->sectModel = new \app\admin\model\chain\lofting\Fytxksect([],$this->technology_ex);
        $this->slabModel = new \app\admin\model\chain\lofting\Fytxkslab([],$this->technology_ex);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                list($code,$msg) = $this->model->judRepete($params["DtM_sTypeName"],0);
                if(!$code) $this->error($msg);
                $result = false;
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('success',null,$result["DtM_iID_PK"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->tableField[5][5] = $this->admin["nickname"];
        $this->tableField[6][5] = date("Y-m-d");
        $this->view->assign("tableField",$this->tableField);
        return $this->view->fetch();
    }
    
    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row =  (new \app\admin\model\chain\lofting\Library([],$this->technology_ex))->where("DtM_iID_PK",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row["DtM_sAuditor"]) $this->editFlag = false;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            
            if(!$this->editFlag) $this->error("已审核，无法修改");
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params AND $paramsTable) {
                list($code,$msg) = $this->model->judRepete($params["DtM_sTypeName"],$ids);
                if(!$code) $this->error($msg);
                $params = $this->preExcludeFields($params);
                $sectSaveList = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $bjhList = [];
                foreach($sectSaveList as $k=>$v){
                    if(isset($bjhList[$v["DtS_Name"]])) return json(["code"=>0,'msg'=>"段号有重复，请进行修改！"]);
                    else $bjhList[$v["DtS_Name"]] = $v["DtS_Name"];
                    if($v["DtS_ID_PK"]==0) unset($sectSaveList[$k]["DtS_ID_PK"],$v["DtS_ID_PK"]);
                    foreach($v as $kk=>$vv){
                        if($vv=='') unset($sectSaveList[$k][$kk]);
                    }
                    $sectSaveList[$k]["DtM_iID_FK"] = $ids;
                }

                $result = false;
                Db::startTrans();
                try {
                    $result = $row->allowField(true)->save($params);
                    if(!empty($sectSaveList)) $this->sectModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $sectArr = [];
        //一组 直接链表 不过太慢了
        // $sectList = $this->sectModel->alias("s")
        //     ->join(["fytxkdetail"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK","left")
        //     ->field("count(d.DtMD_ID_PK) as detailNum,sum(d.DtMD_iUnitCount*d.DtMD_fUnitWeight) as detailWeight,d.DtMD_iSectID_FK,s.*,CAST(s.DtS_Name AS UNSIGNED) AS number")
        //     ->where("s.DtM_iID_FK",$ids)
        //     ->group("d.DtMD_iSectID_FK")
        //     ->order("number,s.DtS_Name ASC")
        //     ->select();

        // $count = count($sectArr);
        // $number = $weight = 0;
        
        // foreach($sectList as $k=>$v){
        //     $sectArr[$k] = $v->toArray();
        //     $sectArr[$k]["DtS_dWriterTime"] = date("Y-m-d",strtotime($v["DtS_dWriterTime"]));
        //     $sectArr[$k]["DtS_sAuditDate"] = $v["DtS_sAuditDate"]?date("Y-m-d",strtotime($v["DtS_sAuditDate"])):"";
        //     $number += $v["detailNum"];
        //     $weight += $v["detailWeight"];
        // }

        $sectList = $this->sectModel->field("*,CAST(DtS_Name AS UNSIGNED) as number")->where("DtM_iID_FK",$ids)->order("number,DtS_Name ASC")->select();
        foreach($sectList as $v){
            $sectArr[$v["DtS_ID_PK"]] = $v->toArray();
            $sectArr[$v["DtS_ID_PK"]]["detailNum"] = 0;
            $sectArr[$v["DtS_ID_PK"]]["detailWeight"] = 0;
            $sectArr[$v["DtS_ID_PK"]]["DtS_dWriterTime"] = date("Y-m-d",strtotime($v["DtS_dWriterTime"]));
            $sectArr[$v["DtS_ID_PK"]]["DtS_sAuditDate"] = $v["DtS_sAuditDate"]?date("Y-m-d",strtotime($v["DtS_sAuditDate"])):"";
        }
        $count = count($sectArr);
        $number = $weight = 0;
        $detailList = $this->detailModel->field("sum(DtMD_iUnitCount*DtMD_fUnitWeight) as detailWeight,count(DtMD_ID_PK) as detailNum,DtMD_iSectID_FK AS DtS_ID_PK")->where("DtMD_iSectID_FK","IN",array_keys($sectArr))->group("DtMD_iSectID_FK")->select();
        foreach($detailList as $v){
            if(isset($sectArr[$v["DtS_ID_PK"]])){
                $sectArr[$v["DtS_ID_PK"]] = array_merge($sectArr[$v["DtS_ID_PK"]],$v->toArray());
                $sectArr[$v["DtS_ID_PK"]]["detailWeight"] = round($sectArr[$v["DtS_ID_PK"]]["detailWeight"],1);
                $number += $v["detailNum"];
                $weight += $sectArr[$v["DtS_ID_PK"]]["detailWeight"];
            }
        }
        $this->tableField[4][5] = $this->admin["nickname"];
        $this->tableField[5][5] = date("Y-m-d");
        $this->tableField[1][4] = "data-rule='required'";
        $this->view->assign("row", $row);
        $this->view->assign("allCount",["count"=>$count,"number"=>$number,"weight"=>round($weight,1)]);
        $this->view->assign("sectList", $sectArr);
        $this->view->assign("tableField",$this->tableField);
        $this->view->assign("ids", $ids);
        $this->view->assign("editFlag", $this->editFlag);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $row =  (new \app\admin\model\chain\lofting\Library([],$this->technology_ex))->where("DtM_iID_PK",$ids)->find();
            if($row["DtM_sAuditor"]) $this->error("已审核，无法删除");
            $dhcooperatek_one = (new DhCooperatek([],$this->technology_ex))->alias("dk")->join([$this->technology_ex."fytxk"=>"f"],"f.DtM_iID_PK=dk.DtM_iID_PK")->where("f.DtM_iID_PK", 'in', $ids)->find();
            if($dhcooperatek_one) $this->error("已生成放样电焊库，无法删除");

            $list = $this->model->where($pk, 'in', $ids)->select();

            $taskDetail = $this->sectModel->field("DtS_ID_PK")->where("DtM_iID_FK",$ids)->select();
            $task_detail_id = [];
            
            $count = 0;
            Db::startTrans();
            try {
                foreach($taskDetail as $v){
                    $task_detail_id[] = $v["DtS_ID_PK"];
                    $v->delete();
                }
                $this->detailModel->where("DtMD_iSectID_FK","IN",$task_detail_id)->delete();
                foreach ($list as $v) {
                    $count += $v->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    // public function examine()
    // {
    //     $params = $this->request->post("data");
    //     $one = $this->model->get($params);
    //     if(!$one) return json(["code"=>0,"msg"=>"无该信息，请稍后重试"]);
    //     else if($one["DtM_sAuditor"]) return json(["code"=>0,"msg"=>"已审核本数据，无法进行段审核"]);
    //     $DtS_sAuditor = $this->admin["nickname"];
    //     $DtS_sAuditDate = date("Y-m-d H:i:s");
    //     $list = $this->sectModel->where("DtM_iID_FK","=",$params)->update(["DtS_sAuditor"=>$DtS_sAuditor,"DtS_sAuditDate"=>$DtS_sAuditDate]);
    //     if($list != false) return json(["code"=>1,"msg"=>"审核成功"]);
    //     else return json(["code"=>0,"msg"=>"没有数据更新"]);
    // }

    // public function giveUp()
    // {
    //     $params = $this->request->post("data");
    //     $one = $this->model->get($params);
    //     if(!$one) return json(["code"=>0,"msg"=>"无该信息，请稍后重试"]);
    //     else if($one["DtM_sAuditor"]) return json(["code"=>0,"msg"=>"已审核本数据，无法进行段弃审"]);
    //     $list = $this->sectModel->where("DtM_iID_FK","=",$params)->update(["DtS_sAuditor"=>'',"DtS_sAuditDate"=>"0000-00-00 00:00:00"]);
    //     if($list != false) return json(["code"=>1,"msg"=>"取消审核成功"]);
    //     else return json(["code"=>0,"msg"=>"没有数据更新"]);
    // }

    public function delDetail()
    {
        $num = $this->request->post("num");
        if($num){
            $one = $this->model->alias("m")->join([$this->technology_ex."fytxksect"=>"s"],"m.DtM_iID_PK = s.DtM_iID_FK")->where("s.DtS_ID_PK",$num)->find();
            if(!$one) return json(["code"=>0,"msg"=>"无该信息，请稍后重试"]);
            else if($one["DtM_sAuditor"]) return json(["code"=>0,"msg"=>"已审核本数据，无法进行段删除"]);
            $detailList = $this->detailModel->field("DtMD_ID_PK")->where("DtMD_iSectID_FK",$num)->select();
            Db::startTrans();
            try {
                
                $detailId = [];
                foreach($detailList as $v){
                    $detailId[] = $v["DtMD_ID_PK"];
                }
                $this->sectModel->where("DtS_ID_PK",$num)->delete();
                if(!empty($detailId)){
                    $this->detailModel->where("DtMD_iSectID_FK",$num)->delete();
                }
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>1,'msg'=>"删除成功"]);
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            $one = $this->model->alias("a")->join([$this->technology_ex."fytxksect"=>"b"],"a.DtM_iID_PK = b.DtM_iID_FK")
                ->join([$this->technology_ex."fytxkdetail"=>"c"],"b.DtS_ID_PK = c.DtMD_iSectID_FK")
                ->field("DtM_sAuditor,DtS_sAuditor")
                ->where("c.DtMD_ID_PK",$num)->find();
            if($one["DtM_sAuditor"]) return json(["code"=>0,'msg'=>"已审核，删除失败"]);
            Db::startTrans();
            try {
                $this->detailModel->where("DtMD_ID_PK",$num)->delete();
                // $this->slabModel->where("DtMD_ID_PK",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function detail($ids=null)
    {
        // $row = $this->sectModel->get($ids);
        $row = $this->model->alias("a")->join([$this->technology_ex."fytxksect"=>"b"],"a.DtM_iID_PK = b.DtM_iID_FK")
            // ->field("DtM_sAuditor,DtS_sAuditor")
            ->where("b.DtS_ID_PK",$ids)->find();
        $check_flag = true;
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row["DtM_sAuditor"]) $check_flag=false;
        $where = ["DtMD_iSectID_FK"=>["=",$ids]];
        // $DtMD_sPartsID?$where["DtMD_sPartsID"] = ["=",$DtMD_sPartsID]:"";
        // $DtMD_sStuff?$where["DtMD_sStuff"] = ["=",$DtMD_sStuff]:"";
        // $DtMD_sMaterial?$where["DtMD_sMaterial"] = ["=",$DtMD_sMaterial]:"";
        // $DtMD_sSpecification?$where["DtMD_sSpecification"] = ["=",$DtMD_sSpecification]:"";
        if ($this->request->isPost()) {
            if($check_flag==false) return json(["code"=>0,'msg'=>"已审核，修改失败"]);
            $params = $this->request->post("data");
            $symbol = $this->request->post("symbol");
            $symbol = $symbol?$symbol:'*';
            $num = $this->request->post("num");
            $num = $num?$num:1;
            $params = json_decode(str_replace('&quot;','"',$params), true);
            if ($params) {
                $detailSaveList = $slabList = $slabSave = $keyValue = [];
                $key = "";
                $biZhong = (new InventoryMaterial())->getIMPerWeight();
                foreach($params as $v){
                    $key = rtrim($v["name"],"[]");
                    isset($keyValue[$key])?"":$keyValue[$key] = [];
                    $keyValue[$key][] = $v["value"];
                }
                foreach($keyValue as $k=>$v){
                    foreach($v as $kk=>$vv){
                        if($k == "DtMD_iUnitCount") $detailSaveList[$kk][$k] = $vv==""?0:round(eval('return '.$vv.$symbol.$num.";"),2);
                        else if($k=="DtMD_sPartsID") $detailSaveList[$kk][$k] = substr($vv,0,1)=="0"?substr($vv,1):$vv;
                        else $detailSaveList[$kk][$k] = trim($vv);
                    }
                }
                $bjhList = [];
                foreach($detailSaveList as $k=>$v){
                    if(isset($bjhList[$v["DtMD_sPartsID"]])) return json(["code"=>0,'msg'=>"部件号有重复，请进行修改！"]);
                    else $bjhList[$v["DtMD_sPartsID"]] = $v["DtMD_sPartsID"];
                    if($v["DtMD_ID_PK"]==0) unset($detailSaveList[$k]["DtMD_ID_PK"],$v["DtMD_ID_PK"]);
                    else $slabList[] = $v["DtMD_ID_PK"];
                    // foreach($v as $kk=>$vv){
                    //     if($vv=='') unset($detailSaveList[$k][$kk]);
                    // }
                    $length = $v["DtMD_iLength"]?$v["DtMD_iLength"]*0.001:0;
                    $width = $v["DtMD_fWidth"]?$v["DtMD_fWidth"]*0.001:0;
                    
                    $DtMD_sStuff = "";
                    $v["DtMD_sSpecification"] = str_replace("L","∠",$v["DtMD_sSpecification"]);
                    $v["DtMD_sSpecification"] = str_replace("∟","∠",$v["DtMD_sSpecification"]);
                    $v["DtMD_sSpecification"] = str_replace("X","*",$v["DtMD_sSpecification"]);
                    $v["DtMD_sSpecification"] = str_replace("x","*",$v["DtMD_sSpecification"]);
                    $v["DtMD_sSpecification"] = str_replace("Φ","ф",$v["DtMD_sSpecification"]);
                    $firstField = mb_substr(trim($v["DtMD_sSpecification"]),0,1);
                    if($firstField == '∠') $DtMD_sStuff = '角钢';
                    elseif($firstField == '-') $DtMD_sStuff = '钢板';
                    elseif($firstField == '[') $DtMD_sStuff = '槽钢';
                    elseif($firstField == 'G') $DtMD_sStuff = '格栅板';
                    else{
                        preg_match_all("/\d+\.?\d*/",$v["DtMD_sSpecification"],$matches);
                        if(count($matches[0])>=2) $DtMD_sStuff = '钢管';
                        else $DtMD_sStuff = '圆钢';
                    }
                    $v["DtMD_sStuff"] = $detailSaveList[$k]["DtMD_sStuff"] = $DtMD_sStuff;

                    $area = $v["DtMD_sStuff"]!='钢板'?$length:$length*$width*abs($v["DtMD_sSpecification"]);

                    $DtMD_fUnitWeight = 0;
                    if($v["DtMD_sStuff"]=="圆钢"){
                        preg_match_all("/\d+\.?\d*/",$v["DtMD_sSpecification"],$matches);
                        $L = isset($matches[0][0])?$matches[0][0]:0;
                        $DtMD_fUnitWeight = round($L*$L*$length*0.00617,2);
                    }else if($v["DtMD_sStuff"]=="钢管"){
                        preg_match_all("/\d+\.?\d*/",$v["DtMD_sSpecification"],$matches);
                        $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                        $r = isset($matches[0][1])?$matches[0][1]/2*0.001:0;
                        if(strpos($v["DtMD_sSpecification"],"*")) $r = $R-(2*$r);
                        $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
                    }else if($v["DtMD_sStuff"]=="格栅板"){
                        $rou = ["G255/40/100W"=>32.1,"G253/40/100W"=>21.3];
                        $rou_one = isset($rou[$v["DtMD_sSpecification"]])?$rou[$v["DtMD_sSpecification"]]:0;
                        $DtMD_fUnitWeight = round($rou_one * $length*$width,4);
                    }else{
                        $DtMD_fUnitWeight = (isset($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]]))?round($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]] * $area,4):0;
                    }
                    // $DtMD_fUnitWeight = (isset($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]]))?round($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]] * $area,4):0;
                    $detailSaveList[$k]["DtMD_fUnitWeight"] = $DtMD_fUnitWeight?$DtMD_fUnitWeight:$v["DtMD_fUnitWeight"];
                    $detailSaveList[$k]["DtMD_iSectID_FK"] = $ids;
                    unset($detailSaveList[$k]["sumWeight"]);
                }
                Db::startTrans();
                try {
                    $result = $this->detailModel->allowField(true)->saveAll($detailSaveList);
                    // if($result){
                        // foreach($result as $v){
                            // if(!in_array($v["DtMD_ID_PK"],$slabList)){
                                // $slabSave[] = ["DtM_iID_PK"=>$row["DtM_iID_FK"],"DtMD_ID_PK"=>$v["DtMD_ID_PK"]];
                            // }
                        // }
                        // if(!empty($slabSave)) $this->slabModel->allowField(true)->saveAll($slabSave);
                    // }
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (PDOException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (Exception $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                }
                if ($result) {
                    return json(["code"=>1,'msg'=>"保存成功"]);
                } else {
                    return json(["code"=>0,'msg'=>"保存失败"]);
                }
            }
            return json(["code"=>0,'msg'=>"有误，请刷新后重试"]);
        }
        $DtS_Name = $row["DtS_Name"];
        $DtM_iID_FK = $row["DtM_iID_FK"];
        $sectList = $this->sectModel->field("DtS_ID_PK,DtS_Name,CAST(DtS_Name AS SIGNED) as number")->where("DtM_iID_FK",$DtM_iID_FK)->order("number")->select();
        $sectArr = $field = $detailList = [];
        foreach($sectList as $v){
            $sectArr[$v["DtS_ID_PK"]] = $v["DtS_Name"];
        }
        
        $tableField = $this->getDetailField();
        foreach($tableField as $v){
            if($v[1]!="sumWeight") $field[] = $v[1];
        }
        $detailList = $this->detailModel->field(implode(",",$field).",DtMD_iSectID_FK,CAST(DtMD_sPartsID AS SIGNED) as number,
        case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
        SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
        when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
        when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
        when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
        when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
        else 0 end 	bjbhn")->where($where)->order("bjbhn,number,DtMD_sPartsID ASC")->select();
        $number = $weight = $knumber = 0;
        $detailArr = $detailName = [];
        $isJudge = ["DtMD_fWidth","DtMD_iTorch","DtMD_iWelding","DtMD_iFireBending","DtMD_iCuttingAngle","DtMD_fBackOff","DtMD_iBackGouging","DtMD_DaBian","DtMD_KaiHeJiao","DtMD_ZuanKong","DtMD_iExcavationCount","DtMD_iAssembly","DtMD_ISZhuanYong"];
        foreach($detailList as $k=>$v){
            $v["DtMD_iUnitCount"] = round($v["DtMD_iUnitCount"],2);
            $detailArr[$k] = $v->toArray();
            foreach($isJudge as $vv){
                isset($v[$vv])?($detailArr[$k][$vv] = $v[$vv]?$v[$vv]:""):"";
            }
            $detailArr[$k]["is_int"] = 0;
            if(ctype_digit(''.$detailArr[$k]["DtMD_iUnitCount"]) and in_array($v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"], $detailName) == FALSE) $detailArr[$k]["is_int"] = 1;
            $detailArr[$k]["DtMD_fUnitWeight"] = round($detailArr[$k]["DtMD_fUnitWeight"],2);
            $detailArr[$k]["sumWeight"] = round($detailArr[$k]["DtMD_fUnitWeight"]*$detailArr[$k]["DtMD_iUnitCount"],2);
            $number += $detailArr[$k]["DtMD_iUnitCount"];
            $weight += round($detailArr[$k]["DtMD_fUnitWeight"]*$detailArr[$k]["DtMD_iUnitCount"],2);
            $knumber += $detailArr[$k]["DtMD_iUnitHoleCount"];
            $detailName[] = $v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"];
        }
        $count = count($detailArr);
        $this->view->assign("allCount",["count"=>$count,"number"=>$number,"weight"=>$weight,"knumber"=>$knumber]);
        $this->view->assign("DtS_Name", $DtS_Name);
        $this->view->assign("ids", $ids);
        $this->view->assign("sectArr", $sectArr);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("detailList",$detailArr);
        $this->view->assign("check_flag",$check_flag);
        return $this->view->fetch();
    }

    public function downField($ids=null)
    {
        // //设置过滤方法
        // $this->request->filter(['strip_tags', 'trim']);
        // if ($this->request->isAjax()) {
        //     //如果发送的来源是Selectpage，则转发到Selectpage
        //     if ($this->request->request('keyField')) {
        //         return $this->selectpage();
        //     }
        //     list($where, $sort, $order, $offset, $limit) = $this->buildparams();
        //     $list = $this->model->alias("f")
        //         ->join(["fytxksect"=>'s'],"f.DtM_iID_PK = s.DtM_iID_FK",'inner')
        //         ->field("f.*")
        //         ->where($where)
        //         ->order($sort, $order)
		// 		->group("f.DtM_iID_PK")
        //         ->paginate(50);
        //     $result = array("total" => $list->total(), "rows" => $list->items());

        //     return json($result);
        // }
        
        $detailList = $this->sectModel->alias("s")
            ->join([$this->technology_ex."fytxkdetail"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK","left")
            ->field("count(d.DtMD_ID_PK) as detailNum,s.DtS_Name,s.DtS_ID_PK,CAST(s.DtS_Name AS UNSIGNED) AS number")
            ->where("s.DtM_iID_FK",$ids)
            ->group("d.DtMD_iSectID_FK")
            ->order("number,s.DtS_Name ASC")
            ->select();
        $this->view->assign("selfTableContent",$detailList);
        // $this->view->assign("tableContent",$list->items());
        $this->view->assign("ids",$ids);
        return $this->view->fetch();
    }

    public function downFieldList($type="tower")
    {
        $params = $this->request->post();
        $where = [];
        $list = [];
        if($type=="tower"){
            foreach($params as $k=>$v){
                if($k == "ids") $where["DtM_iID_PK"] = ["<>",$v];
                else if($k!="DtM_sProject") $where[$k] = ["LIKE","%".$v."%"];
            }
            $list = $this->model->field("DtM_iID_PK,DtM_sPictureNum,DtM_sPressure,DtM_sTypeName")->order("DtM_iID_PK DESC")->where($where)->limit(50)->select();
        }else if($type == "project"){
            foreach($params as $k=>$v){
                if($k == "ids") $where["DtM_iID_PK"] = ["<>",$v];
                else $where["dt.".$k] = ["LIKE","%".$v."%"];
            }
            $list = (new Dtmaterial([],$this->technology_ex))->alias("dt")
                ->join([$this->technology_ex."taskdetail"=>"td"],"dt.TD_ID=td.TD_ID")
                ->field("dt.TD_ID,dt.DtM_iID_PK,dt.DtM_sProject,dt.DtM_sPressure,td.T_Num,dt.DtM_sTypeName")
                ->where($where)
                ->order("dt.DtM_iID_PK DESC")
                ->limit(50)
                ->select();
        }else{
            $where = [];
            if($params["DtM_sTypeName"]) $where["DtM_sTypeName"] = ["LIKE","%".$params["DtM_sTypeName"]."%"];
            if($params["DtM_sPressure"]) $where["DtM_sPressure"] = ["LIKE","%".$params["DtM_sPressure"]."%"];
            $list = (new BaseLibrary([],$this->technology_ex))->where($where)->limit(50)->order("DtM_iID_PK DESC")->select();
        }
        $data = [];
        foreach($list as $k=>$v){
            $data[$k] = $v->toArray();
            $data[$k]["type"] = $type;
        }
        return json(["code"=>1,"data"=>$data]);
    }

    public function downDetailContent()
    {
        list($type,$num) = array_values($this->request->post());
        $result = ["data"=>[],"code"=>0];
        if($num){
            $list = [];
            $model = $this->sectModel;
            if($type=="project") $model = new Dtsect([],$this->technology_ex);
            if($type=="base") $model = new BaseLibrarySect([],$this->technology_ex);
            $row = $model->field("DtS_ID_PK,DtS_Name,CAST(DtS_Name AS UNSIGNED) as number,'".$type."' as type")->where("DtM_iID_FK",$num)->order("number ASC")->select();
            foreach($row as $v){
                $list[] = $v->toArray();
            }
            $result = ["data"=>$list,"code"=>1];;
        }
        
        return json($result);
    }

    public function copySect($ids='')
    {
        $one =  (new \app\admin\model\chain\lofting\Library([],$this->technology_ex))->where("DtM_iID_PK",$ids)->find();
        if(!$one) return json(["code"=>0,'msg'=>"失败，请稍后重试"]);
        elseif($one["DtM_sAuditor"]) return json(["code"=>0,'msg'=>"已审核，导入失败"]);
        list($params,$way) = array_values($this->request->post());
        $params = json_decode(str_replace('&quot;','"',$params), true);
        // $dhlist = $this->model->alias("m")
        //     ->join(["dhcooperatek"=>"dh"],"m.DtM_iID_PK = dh.DtM_iID_PK")
        //     ->field("dh.DtM_iID_PK")
        //     ->where("dh.DtM_iID_PK",$ids)
        //     ->find();
        // if($dhlist) $this->error("已存在电焊组件，不能导入");
        if($params){
            $DtS_NameList = $delList = $addList = [];
            $type = "";
            foreach($params as $k=>$v){
                $type = $v["type"];
                unset($params[$k][0]);
                $DtS_NameList[] = $v["DtS_Name"];
                $addList[$v["DtS_ID_PK"]] = ["DtS_Name"=>$v["DtS_Name"],"DtM_iID_FK"=>$ids,"DtS_sWriter"=>$this->admin["nickname"],"DtS_dWriterTime"=>date("Y-m-d H:i:s"),"DtS_dModifyTime"=>date("Y-m-d H:i:s")];
            }
            
            if($type == "tower") $detailModel = $this->detailModel;
            elseif($type == "project") $detailModel = new Dtmaterialdetail();
            else $detailModel = new BaseLibraryDetail();

            $list = $this->sectModel->field("DtS_ID_PK,DtS_Name")->where("DtM_iID_FK",$ids)->select();
            Db::startTrans();
            try {
                foreach($list as $v){
                    if(in_array($v["DtS_Name"],$DtS_NameList)){
                        // if($way == "cover") $v->delete();
                        $delList[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                    }
                }
                if($way == "cover"){
                    $dhcooperatek_model = new DhCooperatek();
                    $dhcooperatek_detail_model = new DhCooperatekDetail();
                    $dhcooperatek_single_model = new DhCooperatekSingle();
                    $dh_num_list = $dcd_id_list = [];
                    $dts_name = array_keys($delList);
                    $dh_list = $dhcooperatek_model->field("DC_Num")->where("DtM_iID_PK",$ids)->select();
                    foreach($dh_list as $k=>$v){
                        $dh_num_list[] = $v["DC_Num"];
                    }
                    if(!empty($dh_num_list)){
                        $dh_detail_list = $dhcooperatek_detail_model->field("DCD_ID")->where([
                            "DC_Num" => ["IN",$dh_num_list],
                            "DCD_PartName" => ["IN",$dts_name]
                        ])->select();
                        foreach($dh_detail_list as $k=>$v){
                            $dcd_id_list[] = $v["DCD_ID"];
                            $v->delete();
                        }
                        if(!empty($dcd_id_list)) $dhcooperatek_single_model->where([
                            "DCD_ID" => ["IN",$dcd_id_list]
                        ])->delete();
                    }
                    $this->detailModel->where("DtMD_iSectID_FK","in",array_values($delList))->delete();
                }
                foreach($addList as $k=>$v){
                    if(isset($delList[$v["DtS_Name"]])) $addList[$k]["DtS_ID_PK"] = $delList[$v["DtS_Name"]];
                }

                $sectResult = $this->sectModel->allowField(true)->saveAll(array_values($addList));
                $idsList = $detailArr = [];
                foreach($sectResult as $v){
                    $idsList[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                }

                $detailList = $detailModel->field("*,(CASE WHEN LEFT(DtMD_sPartsID, 1) = '0' THEN SUBSTR(DtMD_sPartsID,2) ELSE DtMD_sPartsID END) as DtMD_sPartsID")->where("DtMD_iSectID_FK","in",array_keys($addList))->select();
                foreach($detailList as $k=>$v){
                    $detailArr[$k] = $v->toArray();
                    $detailArr[$k]["DtMD_iSectID_FK"] = $idsList[$addList[$v["DtMD_iSectID_FK"]]["DtS_Name"]];
                    unset($detailArr[$k]["DtMD_ID_PK"]);
                }
                $detailResult = $this->detailModel->allowField(true)->saveAll($detailArr);
                Db::commit();
                return json(["code"=>1,'msg'=>"成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
            
        }
        return json(["code"=>0,'msg'=>"失败"]);
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $one = $this->model->where("DtM_iID_PK",$num)->find();
        if(!$one) return json(["code"=>0,"msg"=>"审核失败，该信息不存在"]);
        elseif($one["DtM_sAuditor"])  return json(["code"=>0,"msg"=>"已审核"]);

        //判断内容是否有误
        //数量有误
        $number_one = $this->model->alias("m")
            ->join([$this->technology_ex."fytxksect"=>"s"],"m.DtM_iID_PK = s.DtM_iID_FK")
            ->join([$this->technology_ex."fytxkdetail"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("(case when cast(DtMD_iUnitCount as UNSIGNED)=DtMD_iUnitCount THEN 0 ELSE 1 END) as flag")
            ->where("DtM_iID_PK",$num)
            ->having("flag=1")
            ->find();
        if($number_one) return json(["code"=>0,"msg"=>"部件数量有误，请仔细核查"]);
        //是否有重复部件号
        $repeat_one = $this->model->alias("m")
            ->join([$this->technology_ex."fytxksect"=>"s"],"m.DtM_iID_PK = s.DtM_iID_FK")
            ->join([$this->technology_ex."fytxkdetail"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("count(DtMD_sPartsID) as group_num")
            ->where("DtM_iID_PK",$num)
            ->group("DtMD_sPartsID")
            ->having("group_num>1")
            ->find();
        if($repeat_one) return json(["code"=>0,"msg"=>"部件名有误，请仔细核查"]);

        $admin = $this->admin["nickname"];
        $result = $this->model->update(['DtM_iID_PK'=>$num,"DtM_sAuditor"=>$admin,"DtM_dTime"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"审核成功"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function giveUpA()
    {
        // echo 1;
        $num = $this->request->post("num");
        $one = $this->model->where("DtM_iID_PK",$num)->find();
        if(!$one) return json(["code"=>0,"msg"=>"弃核失败，该信息不存在"]);
        elseif($one["DtM_sAuditor"]=="")  return json(["code"=>0,"msg"=>"未审核"]);
        $result = $this->model->update(['DtM_iID_PK'=>$num,"DtM_sAuditor"=>'',"DtM_dTime"=>"0000-00-00 00:00:00"]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"弃审成功"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

    /**
     * 段和明细导入
     */
    public function importView($ids=null)
    { 
        if ($this->request->isPost()) {
            $one =  (new \app\admin\model\chain\lofting\Library([],$this->technology_ex))->where("DtM_iID_PK",$ids)->find();
            if(!$one) $this->error("导入失败，请稍后重试");
            elseif($one["DtM_sAuditor"]) $this->error("已审核，导入失败");
            $file = $this->request->post("filename");
            $way = $this->request->post("way");
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $data = [];
            $file = ROOT_PATH . DS . 'public' . DS . $file;
            try {
                $data = Excel::import($file, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($data)){
                    $this->error('空数据文件');
                }
                $data = array_slice($data,1);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
			[$sectList,$detailList] = $this->getImportViewData($data,$ids);
            $where = [
                "DtM_iID_FK" => ["=",$ids],
                "DtS_Name" => ["IN",array_keys($sectList)]
            ];
            $isE = $this->sectModel->where($where)->select();
            $result = false;
            $count = 0;
            Db::startTrans();
            try {
                $sect_id = [];
                foreach($isE as $v){
                    $sect_id[] = $v["DtS_ID_PK"];
                    if($way == "cover") $v->delete();
                    else{
                        $sectList[$v["DtS_Name"]]["DtS_ID_PK"] = $v["DtS_ID_PK"];
                        
                    }
                }
                if(!empty($sect_id) and $way=="cover"){
                    $dhcooperatek_model = new DhCooperatek([],$this->technology_ex);
                    $dhcooperatek_detail_model = new DhCooperatekDetail([],$this->technology_ex);
                    $dhcooperatek_single_model = new DhCooperatekSingle([],$this->technology_ex);
                    $dh_num_list = $dcd_id_list = [];
                    $dts_name = array_keys($sectList);
                    $dh_list = $dhcooperatek_model->field("DC_Num")->where("DtM_iID_PK",$ids)->select();
                    foreach($dh_list as $k=>$v){
                        $dh_num_list[] = $v["DC_Num"];
                    }
                    if(!empty($dh_num_list)){
                        $dh_detail_list = $dhcooperatek_detail_model->field("DCD_ID")->where([
                            "DC_Num" => ["IN",$dh_num_list],
                            "DCD_PartName" => ["IN",$dts_name]
                        ])->select();
                        foreach($dh_detail_list as $k=>$v){
                            $dcd_id_list[] = $v["DCD_ID"];
                            $v->delete();
                        }
                        if(!empty($dcd_id_list)) $dhcooperatek_single_model->where([
                            "DCD_ID" => ["IN",$dcd_id_list]
                        ])->delete();
                    }
                    $this->detailModel->where("DtMD_iSectID_FK","in",$sect_id)->delete();
                }
                $result = $this->sectModel->allowField(true)->saveAll($sectList);
                $contect = $saveDetailList = [];
                foreach($result as $v){
                    $contect[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                }
                foreach($detailList as $k=>$v){
                    $count += count($v);
                    foreach($v as $vv){
                        $saveDetailList[] = array_merge($vv,["DtMD_iSectID_FK"=>$contect[$k]]);
                    }
                }
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveDetailList);

                // if($way == "cover" and !empty($sectList)){
                    
                    
                // }
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success("成功导入".$count."条记录！");
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        return $this->view->fetch();
        
    }

    //BC级的修改
    public function editDetailAll()
    {
        $params = $this->request->post();
        list($sect_id,$type) = array_values($params);
        if(!$sect_id and !$type) return json(["code"=>0,"msg"=>"有误请重试"]);
        $one = $this->model->alias("a")->join([$this->technology_ex."fytxksect"=>"b"],"a.DtM_iID_PK = b.DtM_iID_FK")
            ->field("DtM_sAuditor,DtS_sAuditor")
            ->where("b.DtS_ID_PK",$sect_id)->find();
        if($one["DtM_sAuditor"]) return json(["code"=>0,"msg"=>"已审核，不能修改！"]);
        $where = ["DtMD_iSectID_FK" => ["=",$sect_id]];
        if($type == "editB") $where["DtMD_sMaterial"]=["NOT LIKE","%B"];
        else if($type == "editC") $where["DtMD_sMaterial"]=["NOT LIKE","%C"];
        else if($type == "giveupB") $where["DtMD_sMaterial"]=["LIKE","%B"];
        else $where["DtMD_sMaterial"]=["LIKE","%C"];
        $list = $this->detailModel->field("DtMD_sMaterial,DtMD_ID_PK")->where($where)->select();
        if(!$list) return json(["code"=>0,"msg"=>"没有可以修改的部件号"]);
        $saveList = [];
        foreach($list as $k=>$v){
            if($type == "editB") $field = $v["DtMD_sMaterial"]."B";
            else if($type == "editC") $field = $v["DtMD_sMaterial"]."C";
            else $field = substr($v["DtMD_sMaterial"],0,strlen($v["DtMD_sMaterial"])-1);
            $saveList[] = ["DtMD_ID_PK"=>$v["DtMD_ID_PK"],"DtMD_sMaterial"=>$field];
        }
        $count = false;
        Db::startTrans();
        try {
            $count = $this->detailModel->saveAll($saveList);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        }
        if($count){
            return json(["code"=>1,'msg'=>"成功"]);
        }else{
            return json(["code"=>0,"msg"=>"没有可以修改的部件号"]);
        }
    }

    public function export($ids){
        if(!$ids) return json(["code"=>0,"msg"=>"请稍后重试！"]);
        $one =  (new \app\admin\model\chain\lofting\Library([],$this->technology_ex))->where("DtM_iID_PK",$ids)->find();
        $title = $one["DtM_sTypeName"];
        $list = $this->sectModel->alias("s")
            ->join([$this->technology_ex."fytxkdetail"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("d.*,CAST(s.DtS_ID_PK AS UNSIGNED) AS number_1,CAST(d.DtMD_sPartsID AS UNSIGNED) AS number_2,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn,s.DtS_Name,round(DtMD_iUnitCount*DtMD_fUnitWeight,2) as sum_weight")
            ->where("s.DtM_iID_FK",$ids)
            ->order("number_1,bjbhn,number_2")
            ->select();
        [$list,$header] = $this->getExportData($list);

        return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
    }

    public function getDetailField(){
        $tableField = [
            ["DtMD_ID_PK","DtMD_ID_PK","text","","hidden","40",""],
            ["部件号","DtMD_sPartsID","text","required","","50",""],
            ["材质","DtMD_sMaterial","text","","","60",""],
            ["材料","DtMD_sStuff","text","","","40",""],
            ["规格","DtMD_sSpecification","text","","","70",""],
            ["长度(mm)","DtMD_iLength","text","","","50","checked"],
            ["宽度(mm)","DtMD_fWidth","text","","","50","checked"],
            ["厚度(mm)","DtMD_iTorch","text","","","50","checked"],
            ["单段数量(件)","DtMD_iUnitCount","text","integer","","40","checked"],
            ["单件重量(kg)","DtMD_fUnitWeight","text","","","60","checked"],
            ["总重量(kg)","sumWeight","text","","","60","checked"],
            ["单件孔数(个)","DtMD_iUnitHoleCount","text","integer","","50","checked"],
            ["类型","type","text","","","50","checked"],
            ["电焊(0/1/2/3)","DtMD_iWelding","text","between","","40","checked"],
            ["制弯(0/1/2)","DtMD_iFireBending","text","between","","40","checked"],
            ["切角切肢(0/1)","DtMD_iCuttingAngle","text","between","","40","checked"],
            ["铲背(0/1)","DtMD_fBackOff","text","","","40","checked"],
            ["清根(0/1)","DtMD_iBackGouging","text","between","","40","checked"],
            ["打扁(0/1)","DtMD_DaBian","text","between","","40","checked"],
            ["开合角(0/1/2)","DtMD_KaiHeJiao","text","three","","40","checked"],
            ["割豁(0/1)","DtMD_GeHuo","text","between","","40","checked"],
            ["钻孔(0/1)","DtMD_ZuanKong","text","between","","40","checked"],
            // ["钻孔数(个)","DtMD_iBoreholeCount","text","integer","hidden","40","checked"],
            ["备注","DtMD_sRemark","text","","","80","checked"],
            // ["开槽形式","DtMD_iExcavationCount","text","","","40","checked"],
            // ["打坡口(0/1)","DtMD_iAssembly","text","between","","40","checked"],
            // ["是否专用","DtMD_ISZhuanYong","text","between","","40","checked"],
            // ["类型","DtMD_sType","text","","","60","checked"],
            // ["边数","DtMD_iPerimeter","text","","","60","checked"],
            // ["周长","DtMD_Circle","text","","","60","checked"],
            // ["孔径28~38mm(A)","DtMD_KJ28T32","text","integer","","60","checked"],
            // ["孔径39*50mm(B)","DtMD_KJ33T42","text","integer","","60","checked"],
            // ["孔径50mm以上(C)","DtMD_KJ50UP","text","integer","","60","checked"],
            // ["安装孔直径","DtMD_iAperture","text","","","60","checked"],
            // ["分布圆直径","DtMD_iBending","text","","","60","checked"],
            // ["车工","DtMD_iAftertreatment","text","","","60","checked"],
            // ["压制","DtMD_iPressed","text","","","60","checked"]
        ];
        return $tableField;
    }

    public function showFytxkPrint($ids=null){
        // dump($ids);
        $idsArr = explode(',', $ids);
        $sectArr = $duanArr = $baseArr = [];
        $sectList = $this->sectModel->where("DtS_ID_PK","IN",$idsArr)->select();
        foreach($sectList as $v){
            $sectArr[$v["DtS_ID_PK"]] = $v->toArray();
            $baseArr[$v["DtM_iID_FK"]] = $v["DtM_iID_FK"];
        }
        $baseList = $this->model->where("DtM_iID_PK","IN",$baseArr)->SELECT();
        foreach($baseList as $v){
            $baseArr[$v["DtM_iID_PK"]] = $v->toArray();
        }
        foreach($idsArr as $id){
            $row = $sectArr[$id];
            if (!$row) {
                continue;
            }
            $model = $baseArr[$row['DtM_iID_FK']];
            $mainInfos = [];
            $mainInfos['DtS_Name'] = $row["DtS_Name"];
            // dump($row["DtS_Name"]);
            $mainInfos['DtM_sTypeName'] = $model['DtM_sTypeName'];
            $mainInfos['DtS_sWriter'] = $row['DtS_sWriter'];
            $mainInfos['DtS_sAuditor'] = $row['DtS_sAuditor'];
            $mainInfos['idate']=date('Y-m-d');
            $where = ["DtMD_iSectID_FK"=>["=",$id]];
            $field = $detailList = [];
            $tableField = $this->getDetailField();
            foreach($tableField as $v){
                if($v[1]=="sumWeight") continue;
                $field[] = $v[1];
            }
            $detailList = $this->detailModel
            ->field(implode(",",$field).",
            DtMD_iSectID_FK,CAST(DtMD_sPartsID AS SIGNED) as number,
            CAST(DtMD_sPartsID AS UNSIGNED) AS number_2,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn")
            
            ->where($where)
            ->order("number,bjbhn ASC")
            ->select();
            $number = $weight = $knumber = 0;
            $detailArr = $detailName = [];
            $isJudge = ["DtMD_iWelding","DtMD_iFireBending","DtMD_iCuttingAngle","DtMD_fBackOff","DtMD_iBackGouging","DtMD_DaBian","DtMD_KaiHeJiao","DtMD_ZuanKong","DtMD_iExcavationCount","DtMD_iAssembly","DtMD_ISZhuanYong"];
            foreach($detailList as $k=>$v){
                $detailArr[$k] = $v->toArray();
                foreach($isJudge as $vv){
                    isset($v[$vv])?($detailArr[$k][$vv] = $v[$vv]?$v[$vv]:""):"";
                }
                $detailArr[$k]["is_int"] = 0;
                if(ctype_digit(''.$detailArr[$k]["DtMD_iUnitCount"]) and in_array($v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"], $detailName) == FALSE) $detailArr[$k]["is_int"] = 1;
                $detailArr[$k]["DtMD_fUnitWeight"] = round($detailArr[$k]["DtMD_fUnitWeight"],2);
                $number += $detailArr[$k]["DtMD_iUnitCount"];
                $weight += $detailArr[$k]["DtMD_fUnitWeight"];
                $knumber += $detailArr[$k]["DtMD_iUnitHoleCount"];
                $detailName[] = $v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"];
            }
            $mainInfos['number'] = $number;
            $mainInfos['weight'] = (string)$weight;
            $mainInfos['knumber'] = $knumber;

            $duanData = [];
            $duanData['mainInfos'] = $mainInfos;
            $duanData['detailArr'] = $detailArr;
            array_push($duanArr, $duanData);
        }
    
        $this->assignconfig('duanArr',$duanArr);

        return $this->view->fetch();
    }

    public function showFytxkPrint2($ids=null){
        // dump($ids);
        $row = (new \app\admin\model\chain\lofting\Fytxksect([],$this->technology_ex))->WHERE("DtS_ID_PK",$ids)->FIND($ids);
        
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $model = (new \app\admin\model\chain\lofting\Library([],$this->technology_ex))->WHERE("DtM_iID_PK",$row['DtM_iID_FK'])->FIND($ids);
        // dump($model);
        $mainInfos = [];
        $mainInfos['DtS_Name'] = $row["DtS_Name"];
        $mainInfos['DtM_sTypeName'] = $model['DtM_sTypeName'];
        $mainInfos['DtS_sWriter'] = $row['DtS_sWriter'];
        $mainInfos['DtS_sAuditor'] = $row['DtS_sAuditor'];
        $mainInfos['idate']=date('Y-m-d');

        $where = ["DtMD_iSectID_FK"=>["in",$ids]];
        $field = $detailList = [];
        $tableField = $this->getDetailField();
        foreach($tableField as $v){
            if($v[1]=="sumWeight") continue;
            $field[] = $v[1];
        }
        $detailList = $this->detailModel
            ->field(implode(",",$field).",DtMD_iSectID_FK,CAST(DtMD_sPartsID AS SIGNED) as number,CASE WHEN locate('-',DtMD_sPartsID) THEN REPLACE(DtMD_sPartsID,'-','')*1000 ELSE DtMD_sPartsID END as number_1,CAST(DtMD_sPartsID AS UNSIGNED) AS number_2,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn")
            ->where($where)
            ->order("DtMD_sStuff,DtMD_sSpecification,DtMD_sMaterial,bjbhn ASC")
            ->select();
        $number = $weight = $knumber = 0;
        $detailArr = $detailName = [];
        $isJudge = ["DtMD_iWelding","DtMD_iFireBending","DtMD_iCuttingAngle","DtMD_fBackOff","DtMD_iBackGouging","DtMD_DaBian","DtMD_KaiHeJiao","DtMD_ZuanKong","DtMD_iExcavationCount","DtMD_iAssembly","DtMD_ISZhuanYong"];
        foreach($detailList as $k=>$v){
            $detailArr[$k] = $v->toArray();
            foreach($isJudge as $vv){
                isset($v[$vv])?($detailArr[$k][$vv] = $v[$vv]?$v[$vv]:""):"";
            }
            $detailArr[$k]["is_int"] = 0;
            if(ctype_digit(''.$detailArr[$k]["DtMD_iUnitCount"]) and in_array($v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"], $detailName) == FALSE) $detailArr[$k]["is_int"] = 1;
            $detailArr[$k]["DtMD_fUnitWeight"] = round($detailArr[$k]["DtMD_fUnitWeight"],2);
            $number += $detailArr[$k]["DtMD_iUnitCount"];
            $weight += $detailArr[$k]["DtMD_fUnitWeight"];
            $knumber += $detailArr[$k]["DtMD_iUnitHoleCount"];
            $detailName[] = $v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"];
        }
        $mainInfos['number'] = $number;
        $mainInfos['weight'] = (string)$weight;
        $mainInfos['knumber'] = $knumber;
        // dump($mainInfos);
        $this->view->assign("detailList",$detailArr);
        $this->assignconfig('detailArr',$detailArr);
        $this->assignconfig('mainInfos',$mainInfos);

        return $this->view->fetch();
    }
}
