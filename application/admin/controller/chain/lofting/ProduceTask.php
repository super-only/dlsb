<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\DhCooperateDetail;
// use app\admin\model\chain\lofting\DhCooperateSingle;
// use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\model\chain\lofting\MergTypeName;
// use app\admin\model\chain\lofting\Piece;
use app\admin\model\chain\lofting\PieceContent;
// use app\admin\model\chain\lofting\PieceDetail;
use app\admin\model\chain\lofting\ProduceTaskFlat;
use app\admin\model\chain\lofting\TaskSect;
use app\admin\model\chain\sale\TaskDetail;
use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use fast\Http;
use jianyan\excel\Excel;
use think\Cache;
use think\response\Json;
use Yurun\Util\Chinese\Number;

/**
 * 生产任务下达
 *
 * @icon fa fa-circle-o
 */
class ProduceTask extends Technology
{

    /**
     * ProduceTask模型对象
     * @var \app\admin\model\chain\lofting\ProduceTask
     */
    protected $model = null;
    protected $admin;
    // protected $noNeedRight = ['showLckPrint','showJgPrint','showZhPrint'];
    protected $noNeedRight = '*';
    protected $noNeedLogin = ['getCache', "eipUpload"];

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_xd = \think\Session::get('technology_xd');
        $this->technology_type = \think\Session::get('technology_type');
        $this->model = new \app\admin\model\chain\lofting\ProduceTask([],$this->technology_ex);
        $this->sectModel = new \app\admin\model\chain\lofting\ProduceTaskSect([],$this->technology_ex);
        $this->sectShiModel = new \app\admin\model\chain\lofting\ProduceTaskSectShizu([],$this->technology_ex);
        $this->flatModel = new \app\admin\model\chain\lofting\ProduceTaskFlat([],$this->technology_ex);
        $this->taskDModel = new \app\admin\model\chain\sale\TaskDetail;
        $this->sectconfigModel = new \app\admin\model\chain\sale\Sectconfigdetail;
        $this->detailModel = new \app\admin\model\chain\lofting\ProduceTaskDetail([],$this->technology_ex);
        $this->dhModel = new \app\admin\model\chain\lofting\ProduceDh([],$this->technology_ex);
        $this->admin = \think\Session::get('admin');
        $flag_list = [1 => "下达中", 2 => "已下达", 3 => "计件中", 4 => "计件完成"];
        $this->assignconfig("flag_list", $flag_list);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $subSql = $this->model->alias("p")->join(["mergtypename" => "mtn"], "p.TD_ID=mtn.mTD_ID", "left")
                ->field("flag,PT_Num,PT_Company,C_ProjectName,PT_Number,PT_sumWeight,PT_szMemo,PT_PlanTime,Writer,WriterDate,Auditor,AuditorDate,PT_Memo,ifnull(mTD_ID,p.TD_ID) AS TD_ID")
                ->where("p.PT_Num", "not like", "PT%")->group("p.PT_Num")
                ->buildSql();
            $list = DB::TABLE($subSql)->alias("p")
                ->join(["taskdetail" => "d"], "p.TD_ID = D.TD_ID")
                ->join(["task" => "t"], "t.T_Num=d.T_Num")
                ->join(["compact" => "c"], "c.C_Num = t.C_Num")
                // ->join(['mergtypename'=>"mtn"],"mtn.mTD_ID=")
                // LEFT JOIN MergTypeName b ON a.TD_ID=b.mTD_ID
                ->field("p.*,d.P_Name,d.TD_TypeName,c.Customer_Name,d.T_Num,d.T_Num as 'd.T_Num'")
                ->where($where)
                ->order($sort, $order)
                // ->select(false);
                // pri($list,1);
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function selectDetail()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\sale\Compact)->alias("c")
                ->join(["task" => "t"], "t.C_Num=c.C_Num")
                ->join(["taskdetail" => "d"], "t.T_Num = d.T_Num")
                // ->join(["taskheight"=>"th"],"d.TD_ID = th.TD_ID")
                ->join([$this->technology_ex."dtmaterial" => "dm"], "dm.TD_ID = d.TD_ID")
                ->field("d.TD_ID,d.T_Num,d.P_Name,d.P_Num,d.TD_TypeName,d.TD_Pressure,c.PC_Num,c.C_Num,c.C_Project,c.C_Company,c.Customer_Name,c.C_SortProject,c.C_SaleMan")
                ->where($where)
                ->where("c.produce_type",$this->technology_type)
                ->group("d.TD_ID")
                ->order($sort, $order)
                ->paginate($limit);
            $dList = $tdList = [];
            foreach ($list as $v) {
                $dList[$v["TD_ID"]] = $v->toArray();
                $dList[$v["TD_ID"]]["T_Count"] = $dList[$v["TD_ID"]]["TD_Count"] = $dList[$v["TD_ID"]]["PT_sumCount"] = $dList[$v["TD_ID"]]["PT_sumWeight"] = 0;
                $tdList[$v["TD_ID"]] = $v["TD_ID"];
            }
            $merge_list = (new MergTypeName())->where("mTD_ID", "IN", array_keys($dList))->select();
            foreach ($merge_list as $v) {
                $dList[$v["mTD_ID"]]["list"][$v["TD_ID"]] = $v["TD_ID"];
                $tdList[$v["TD_ID"]] = $v["mTD_ID"];
            }

            $countList = (new \app\admin\model\chain\sale\TaskHeight())
                ->field("sum(TD_Count) as TD_Count,TD_ID,SUM(TD_Count*TH_Weight) as PT_sumWeight")
                ->where("TD_ID", "in", array_keys($tdList))
                ->group("TD_ID")
                ->select();
            foreach ($countList as $v) {
                $dList[$tdList[$v["TD_ID"]]]["TD_Count"] += $v["TD_Count"];
                $dList[$tdList[$v["TD_ID"]]]["T_Count"] += $v["TD_Count"];
                $dList[$tdList[$v["TD_ID"]]]["PT_sumCount"] += $v["TD_Count"];
                $dList[$tdList[$v["TD_ID"]]]["PT_sumWeight"] += $v["PT_sumWeight"];
            }
            $result = array("total" => $list->total(), "rows" => array_values($dList));

            return json($result);
        }
        return $this->view->fetch();
    }

    public function release()
    {
        $num = $this->request->post("num");
        if (!$num) return json(["code" => 0, "msg" => '选择失败']);
        $mergetype = (new MergTypeName())->where("mTD_ID", $num)->select();
        $mergeList = [$num => $num];
        if ($mergetype) {
            foreach ($mergetype as $v) {
                $mergeList[$v["TD_ID"]] = $v["TD_ID"];
            }
        }

        list($field, $tableList) = $this->releaseTable($mergeList);
        $tableContent = listToTable($field, $tableList);
        return json(["code" => 1, "msg" => "success", "data" => $tableContent]);
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                if ($params["PT_Num"]) {
                    $one = $this->model->where("PT_Num",$params["PT_Num"])->find();
                    if ($one) $this->error("下达单号必须唯一，请重新输入或者自动编码");
                }
                $year = date("ym");
                $PT_Num_last = "";
                $lastOne = $this->model->field("PT_Num")->where("PT_Num", "LIKE", $year . '-%')->order("PT_Num DESC")->find();
                if ($lastOne) $PT_Num_last = $lastOne["PT_Num"];
                $PT_Num = $PT_Num_last != "" ? $year . ($this->technology_xd?$this->technology_xd:'-') . (str_pad(substr($PT_Num_last, -3) + 1, 3, 0, STR_PAD_LEFT)) : $year . ($this->technology_xd?$this->technology_xd:'-').'001';
                $params["PT_Company"] = '公司';
                $params["PT_Num"] = $PT_Num;
                $params["Writer"] = $this->admin["nickname"];
                $result = $this->model->allowField(true)->save($params);
                if ($result !== false) {
                    $this->success('success', null, $PT_Num);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function addMain()
    {
        $params = $this->request->post("row/a");
        if ($params) {
            $params = $this->preExcludeFields($params);
            if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                $params[$this->dataLimitField] = $this->auth->id;
            }
            $result = false;
            if ($params["PT_Num"]) {
                $one = $this->model->where("PT_Num",$params["PT_Num"])->find();
                if ($one) return json(["code" => 0, "msg" => "下达单号必须唯一，请重新输入或者自动编码"]);
            }
            $year = date("ym");
            $PT_Num_last = "";
            $lastOne = $this->model->field("PT_Num")->where("PT_Num", "LIKE", $year . '-%')->order("PT_Num DESC")->find();
            if ($lastOne) $PT_Num_last = $lastOne["PT_Num"];
            $PT_Num = $PT_Num_last != "" ? $year . '-' . (str_pad(substr($PT_Num_last, -3), 3, 0, STR_PAD_LEFT) + 1) : $year . '-001';
            $params["PT_Num"] = $PT_Num;
            $params["Writer"] = $this->admin["nickname"];
        }
        return json(["code" => 0, "msg" => __('Parameter %s can not be empty')]);
    }

    public function codePlane($ids = null)
    {
        if (!$ids) {
            $this->error(__('No Results were found'));
        }
        $this->view->assign("TD_ID", $ids);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->alias("m")
            ->join(["taskdetail" => "td"], "td.TD_ID=m.TD_ID")
            ->field("m.*,td.TD_TypeName,td.TD_Pressure")
            ->where("PT_Num", $ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        // $oldNameModel = new NewOldTdTypeName();
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $TD_ID = $params["TD_ID"];
            // $old_TypeName = $params["old_TypeName"];
            // $oldNameModel->where("TD_ID",$TD_ID)->update(["old_TypeName"=>$old_TypeName]);
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }

                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $TD_ID = $row["TD_ID"];

        $mergetype = (new MergTypeName())->where("mTD_ID", $TD_ID)->select();
        $mergeList = [$TD_ID => $TD_ID];
        if ($mergetype) {
            foreach ($mergetype as $v) {
                $mergeList[$v["TD_ID"]] = $v["TD_ID"];
            }
        }
        list($field, $tableList) = $this->releaseTable($mergeList);
        $bottomContent = listToTable($field, $tableList);


        // $leftTopList = $this->sectModel->field("PTS_ID,PT_Num,TS_Name")->where("PT_Num",$ids)->order("PTS_Num");

        $rightWhere = ["ptk.SCD_ID" => ["<>", 0], "ptk.PT_Num" => ["=", $ids]];
        $rightList = $this->sectconfigModel->alias("scf")
            ->join([$this->technology_ex."producetasksect" => "ptk"], "scf.SCD_ID = ptk.SCD_ID")
            ->field("scf.SCD_ID,scf.SCD_TPNum")
            ->where($rightWhere)
            ->order("scf.SCD_ID ASC")
            ->group("scf.SCD_ID")
            ->select();
        // $name = $oldNameModel->field("old_TypeName")->where("TD_ID",$TD_ID)->find();
        $row = $row->toArray();
        // $row["old_TypeName"] = $name?$name["old_TypeName"]:"";
        $rightList = objectToArray($rightList);
        $this->view->assign("row", $row);
        $this->view->assign("bottomTable", $bottomContent);
        $this->view->assign("rightList", $rightList);
        return $this->view->fetch();
    }

    public function taskReleaseDetail()
    {
        $params = $this->request->post();
        $PT_Num = $params["PT_Num"] ?? '';
        $TD_ID = $params["TD_ID"] ?? '';
        if (!$PT_Num or !$TD_ID) return json(["code" => 0, "msg" => "有误"]);
        $SCD_ID = $params["SCD_ID"] ?? [];
        $where = [
            "PT_Num" => ["=", $PT_Num]
        ];
        if (!empty($SCD_ID)) $where["SCD_ID"] = ["in", $SCD_ID];
        $list = $this->sectModel->field("*,CAST(PTS_Name AS UNSIGNED) as number")->where($where)->order("number ASC")->select();
        $list = objectToArray($list);

        $mergetype = (new MergTypeName())->where("mTD_ID", $TD_ID)->select();
        $mergeList = [$TD_ID => $TD_ID];
        $where = ["th.TD_ID" => ["=", $TD_ID]];
        if ($mergetype) {
            foreach ($mergetype as $v) {
                $mergeList[$v["TD_ID"]] = $v["TD_ID"];
            }
            $where = ["th.TD_ID" => ["IN", $mergeList]];
        }
        list($field, $content) = $this->releaseTable($mergeList, "");
        $groupArr = [];
        $groupList = (new \app\admin\model\chain\lofting\TaskSect)->alias("ts")
            ->join(["sectconfigdetail" => "sd"], "ts.SCD_ID=sd.SCD_ID")
            ->join(["taskheight" => "th"], "th.TH_ID=sd.TH_ID")
            ->field("TS_Count,TS_Name,ifBody,SCD_TPNum,CAST(TS_Name AS UNSIGNED) as number")
            ->where($where)
            ->order("number ASC")
            // ->group("TS_Name")
            ->select();
        foreach ($groupList as $v) {
            $name = $v["ifBody"] == 1 ? $v["TS_Name"] : $v["SCD_TPNum"] . ' / ' . $v["TS_Name"];
            $groupArr[$name] = $groupArr[$name] ?? $v["TS_Count"];
        }
        foreach ($list as $k => $v) {
            $name = $v["TS_Name"];
            $list[$k]["all_Count"] = $content['总段数'][$name];
            $list[$k]["is_Count"] = $content['总段数'][$name] - $v["PTS_Count"] - $content['剩余段数'][$name];
            $list[$k]["DHS_Height"] = $groupArr[$name] ?? 0;
        }
        return json(["code" => 1, "data" => $list]);
    }

    public function testSectionGroup($PT_Num = '', $ptsName = '')
    {
        if (!$PT_Num) $this->error(__('No Results were found'));
        if (!$ptsName) $this->error('请先选择试段组!');
        $ptsList = explode(",", trim($ptsName, ","));
        $list = $this->sectShiModel->where([
            "PT_Num" => ["=",$PT_Num],
            "PT_Sect" => ["IN",$ptsList]
        ])->column("PT_Sect,PT_shiCount");
        $row = [];
        foreach ($ptsList as $v) {
            $row[] = ["PT_Num" => $PT_Num, 'PT_Sect' => $v, "PT_shiCount" => build_input("PT_shiCount[]", "number", $list[$v]??1)];
        }
        $this->assignconfig("pt_num", $PT_Num);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function testSectionGroupSave($pt_num = '')
    {
        if (!$pt_num) return json(["code" => 0, "msg" => "保存失败请稍后重试"]);
        $params = $this->request->post("content");
        $content = json_decode(str_replace('&quot;','"',$params), true);
        // if(count($content)==0) return json(["code"=>0,"msg"=>"请保证存在试段组"]);
        $pt_sect = [];
        foreach ($content as $k => $v) {
            $matches = [];
            $pt_sect[] = $v["PT_Sect"];
        }
        Db::startTrans();
        try {
            $this->sectShiModel->where("PT_Num", $pt_num)->delete();

            if (!empty($content)) {
                $where = [
                    "PTS_Name" => ["in", $pt_sect],
                    "PT_Num" => ["=", $pt_num]
                ];
                $where_no = [
                    "PTS_Name" => ["not in", $pt_sect],
                    "PT_Num" => ["=", $pt_num]
                ];
                $this->sectModel->where($where)->update(['PTS_IFShi' => 1]);
                $this->sectModel->where($where_no)->update(['PTS_IFShi' => 0]);
                $this->sectShiModel->saveAll($content);
            } else {
                $this->sectModel->where(["PT_Num" => ["=", $pt_num]])->update(['PTS_IFShi' => 0]);
            }

            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        }
        return json(["code" => 1, "msg" => '保存成功']);

        // $this->sectModel->where($where)->update(['PTS_IFShi' => 1]);

    }

    public function generateProduction()
    {
        $pt_num = $this->request->post("PT_Num");
        $td_id = $this->request->post("TD_ID");
        if (!$td_id or !$pt_num) return json(["code" => 0, "msg" => "有误，请重试。"]);
        $dtm_iid_pk_one = (new \app\admin\model\chain\lofting\Dtmaterial([],$this->technology_ex))->field("DtM_iID_PK")->where("TD_ID", $td_id)->find();
        $where = ["ds.DtM_iID_FK" => ["=", $dtm_iid_pk_one["DtM_iID_PK"]]];
        $pts_name_list = $this->sectModel->field("PTS_Name")->where("PT_Num", $pt_num)->group("PTS_Name")->select();
        if (!$pts_name_list) return json(["code" => 0, "msg" => '请先进入二维平面选择杆塔']);
        $task_one = $this->model->where("PT_Num", $pt_num)->find();
        if (!$task_one) return json(["code" => 0, "msg" => '有误，请重试']);
        elseif ($task_one["flag"] >= 2 or $task_one["Auditor"]) return json(["code" => 2, "msg" => '生成生产明细成功']);
        $pts_list = [];
        foreach ($pts_name_list as $v) {
            $pts_list[] = $v["PTS_Name"];
        }
        $where["ds.DtS_Name"] = ["in", $pts_list];
        $list = (new \app\admin\model\chain\lofting\Dtsect([],$this->technology_ex))->alias("ds")
            ->join([$this->technology_ex."dtmaterialdetial" => "dd"], "ds.DtS_ID_PK = dd.DtMD_iSectID_FK")
            ->field("CAST(ds.DtS_Name AS UNSIGNED) as number,ds.DtS_Name as PT_Sect,dd.DtMD_ID_PK,dd.DtMD_iUnitCount as PTD_Count,dd.DtMD_fUnitWeight as PTD_SWeight,dd.DtMD_sMaterial as PTD_Material,dd.DtMD_sSpecification as PTD_Specification,dd.DtMD_iWelding as PTD_Flag")
            ->order("number ASC")
            ->where($where)
            ->select();
        $list = objectToArray($list);
        $szList = $this->sectShiModel->field("PT_Sect")->where("PT_Num", $pt_num)->select();
        $group = "PT_Num,PTS_Name";
        $useField = "sum(PTS_Count) as PTS_Count,PT_Num,PTS_Name as TS_Name";
        $usedList = $this->sectModel->field($useField)
            ->where("PT_Num", $pt_num)
            ->group($group)
            ->select();
        $szArr = $sectList = [];
        foreach ($usedList as $v) {
            $sectList[$v["TS_Name"]] = $v["PTS_Count"];
        }
        // pri($sectList,1);
        foreach ($szList as $v) {
            $szArr[] = $v["PT_Sect"];
        }
        $PT_sumCount = $PT_sumWeight = 0;
        foreach ($list as $k => $v) {
            unset($list[$k]["number"]);
            $list[$k]["PT_Num"] = $pt_num;
            $list[$k]["IF_Shi"] = in_array($v["PT_Sect"], $szArr);
            //需要问一下
            $list[$k]["tpaNum"] = 1;
            $list[$k]["DD_Name"] = '公司';
            $list[$k]["PTD_Count"] = isset($sectList[$v["PT_Sect"]]) ? @$v["PTD_Count"] * $sectList[$v["PT_Sect"]] : @$v["PTD_Count"];
            $list[$k]["PTD_SWeight"] = $v["PTD_SWeight"] ?? 0;
            $list[$k]["PTD_sumWeight"] = isset($sectList[$v["PT_Sect"]]) ? $list[$k]["PTD_Count"] * $list[$k]["PTD_SWeight"] : $list[$k]["PTD_SWeight"];
            $list[$k]["PTD_Flag"] = $v["PTD_Flag"] ?? 0;
            $list[$k]["PTD_Specification"] = $v["PTD_Specification"] ?? "";
            $PT_sumCount += $list[$k]["PTD_Count"];
            $PT_sumWeight += $list[$k]["PTD_sumWeight"];
        }
        // pri($list,1);
        Db::startTrans();
        try {
            $this->model->where("PT_Num", $pt_num)->update(["PT_sumCount" => $PT_sumCount, "PT_sumWeight" => $PT_sumWeight]);
            $this->detailModel->where("PT_Num", $pt_num)->delete();
            $result = $this->detailModel->allowField(true)->saveAll($list);
            // pri(count($list),count($result),$list,$result,1);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        }
        if ($result != false) {
            (new Http())->sendAsyncRequest("http://192.168.3.88/admin.php/chain/lofting/produce_task/savepdf/PT_Num/".$pt_num);
            return json(["code" => 1, "msg" => '生成生产明细成功']);
        } else {
            return json(["code" => 0, "msg" => __('No rows were inserted')]);
        }
    }

    public function viewProduction($ids = null)
    {
        $where = [
            "d.PT_Num" => ["=", $ids]
        ];
        $having = "";
        if ($this->request->isPost()) {
            $select_num = $this->request->post("num");
            if ($select_num) {
                switch ($select_num) {
                    case 1:
                        $where["dd.DtMD_sStuff"] = ["=", "角钢"];
                        break;
                    case 2:
                        $where["dd.DtMD_sStuff"] = ["=", "钢板"];
                        break;
                    case 3:
                        $where["dd.DtMD_sStuff"] = ["=", "钢管"];
                        break;
                    case 4:
                        $where["dd.DtMD_sStuff"] = ["=", "圆钢"];
                        break;
                    case 5:
                        $where["dd.DtMD_sStuff"] = ["=", "槽钢"];
                        break;
                    case 6:
                        $where["dd.DtMD_sStuff"] = ["=", "工字钢"];
                        break;
                    case 7:
                        $where["dd.DtMD_sStuff"] = ["not in", "角钢,钢板"];
                        break;
                    case 8:
                        $where["dd.DtMD_sStuff"] = ["=", "角钢"];
                        $having .= "bh < 70000";
                        // $where["bh"] = ["<","70000"];
                        break;
                    case 9:
                        $where["dd.DtMD_sStuff"] = ["=", "角钢"];
                        $having .= "bh >= 70000";
                        // $where["bh"] = [">=","70000"];
                        break;
                    default:
                        $where[1] = ["=", 1];
                }
            }

            $list = $this->detailModel->alias("d")
                ->join([$this->technology_ex."dtmaterialdetial" => 'dd'], "d.DtMD_ID_PK = dd.DtMD_ID_PK")
                ->field("CAST(d.PT_Sect AS UNSIGNED) AS number_1,d.PT_Sect,CAST(dd.DtMD_sPartsID AS UNSIGNED) AS number_2,dd.DtMD_sPartsID,dd.DtMD_sStuff,dd.DtMD_sMaterial,dd.DtMD_sSpecification,dd.DtMD_iLength,dd.DtMD_fWidth,ifnull(dd.DtMD_sType,'') as DtMD_sType,ifnull(dd.type,'') as type,ifnull(dd.DtMD_iTorch,0) as DtMD_iTorch,d.PTD_Count,dd.DtMD_iUnitHoleCount,(dd.DtMD_iUnitHoleCount*PTD_Count) as SumCount,round(dd.DtMD_fUnitWeight,1) as DtMD_fUnitWeight,round(dd.DtMD_fUnitWeight*PTD_Count,1) as SumWeight,dd.DtMD_sRemark,d.IF_Shi,dd.DtMD_iWelding,d.PTD_ID,
                case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
                SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
                else 0 end 	bjbhn,
                cast((case 
                when dtmd_sstuff='角钢' then 
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
                when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
                REPLACE(DtMD_sSpecification,'-','')
                else 0
                end) as UNSIGNED) bh")
                ->where($where)
                ->having($having)
                ->order("number_1,bjbhn,number_2,dd.DtMD_sPartsID asc")
                ->select();

            $group = "PT_Num,PTS_Name";
            $useField = "sum(PTS_Count) as PTS_Count,PT_Num,PTS_Name as TS_Name";
            $usedList = $this->sectModel->field($useField)
                ->where("PT_Num", $ids)
                ->group($group)
                ->select();

            $data = $sectList = [];
            foreach ($usedList as $v) {
                $sectList[$v["TS_Name"]] = $v["PTS_Count"];
            }
            foreach ($list as $k => $v) {
                $v["id"] = $k + 1;
                $data[$k] = $v->toArray();
                $data[$k]["SumWeight"] = round($v["SumWeight"], 2);
                $data[$k]["IF_Shi"] = $v["IF_Shi"] ? "是" : "否";
                $data[$k]["DD_Count"] = isset($sectList[$v["PT_Sect"]]) ? round(($v["PTD_Count"] / $sectList[$v["PT_Sect"]])) : $v["PTD_Count"];
            }
            return json(["code" => 1, "data" => $data, "count" => count($data)]);
            // $this->view->assign("product",$data);
        }
        $this->assignconfig('PT_Num', $ids);
        return $this->view->fetch();
    }

    public function exportProduction($ids = null)
    {
        if (!$ids) return json(["code" => 0, "msg" => "请稍后重试！"]);
        $list = $this->detailModel->alias("d")
            ->join([$this->technology_ex."dtmaterialdetial" => 'dd'], "d.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->field("CAST(d.PT_Sect AS UNSIGNED) AS number_1,d.PT_Sect,CAST(dd.DtMD_sPartsID AS UNSIGNED) AS number_2,dd.DtMD_sPartsID,dd.DtMD_sStuff,dd.DtMD_sMaterial,dd.DtMD_sSpecification,dd.DtMD_iLength,dd.DtMD_fWidth,dd.DtMD_sType,dd.DtMD_iTorch,d.PTD_Count,dd.DtMD_iUnitHoleCount,(dd.DtMD_iUnitHoleCount*PTD_Count) as SumCount,round(dd.DtMD_fUnitWeight,1) as DtMD_fUnitWeight,round(dd.DtMD_fUnitWeight*PTD_Count,1) as SumWeight,dd.DtMD_sRemark,d.IF_Shi,dd.DtMD_iWelding,d.PTD_ID")
            ->where("d.PT_Num", $ids)
            ->order("number_1,number_2 asc")
            ->select();
        $group = "PT_Num,PTS_Name";
        $useField = "sum(PTS_Count) as PTS_Count,PT_Num,PTS_Name as TS_Name";
        $usedList = $this->sectModel->field($useField)
            ->where("PT_Num", $ids)
            ->group($group)
            ->select();

        $data = $sectList = [];
        foreach ($usedList as $v) {
            $sectList[$v["TS_Name"]] = $v["PTS_Count"];
        }
        foreach ($list as $k => $v) {
            $data[$k] = $v->toArray();
            $data[$k]["DtMD_sSpecification"] = strtr($v["DtMD_sSpecification"], "*", "x");
            $data[$k]["SumWeight"] = round($v["SumWeight"], 2);
            $data[$k]["IF_Shi"] = $v["IF_Shi"] ? "是" : "否";
            $data[$k]["DD_Count"] = isset($sectList[$v["PT_Sect"]]) ? round(($v["PTD_Count"] / $sectList[$v["PT_Sect"]])) : $v["PTD_Count"];
        }

        $header = [
            ['段号', 'PT_Sect'],
            ['部件号', 'DtMD_sPartsID'],
            ['材料名', 'DtMD_sStuff'],
            ['材质', 'DtMD_sMaterial'],
            ['规格', 'DtMD_sSpecification'],
            ['长度(mm)', 'DtMD_iLength'],
            ['宽度(mm)', 'DtMD_fWidth'],
            ['类型', 'DtMD_sType'],
            ['厚度(mm)', 'DtMD_iTorch'],
            ['单段件数', 'DD_Count'],
            ['加工数', 'PTD_Count'],
            ['孔数', 'DtMD_iUnitHoleCount'],
            ['总孔数', 'SumCount'],
            ['单重(kg)', 'DtMD_fUnitWeight'],
            ['总重量', 'SumWeight'],
            ['备注', 'DtMD_sRemark'],
            ['试组装', 'IF_Shi'],
            ['电焊', 'DtMD_iWelding']
        ];

        return Excel::exportData($data, $header, $ids . '-清单-' . date('Ymd'));
    }

    public function weldingDetail()
    {
        $pt_num = $this->request->post("PT_Num");
        $td_id = $this->request->post("TD_ID");
        if (!$td_id or !$pt_num) return json(["code" => 0, "msg" => "有误，请重试。"]);
        //整理修改
        $szList = $this->sectShiModel->field("PT_Sect")->where("PT_Num", $pt_num)->select();
        $szArr = $dhList = [];
        foreach ($szList as $v) {
            $szArr[] = $v["PT_Sect"];
        }
        $dhList = $dcd_list = [];
        $where = ["dhp.TD_ID" => ["=", $td_id]];
        $pts_name_list = $this->sectModel->field("PTS_Name")->where("PT_Num", $pt_num)->group("PTS_Name")->select();
        if (!$pts_name_list) return json(["code" => 0, "msg" => '请先进入二维平面选择杆塔']);
        $task_one = $this->model->where("PT_Num", $pt_num)->find();
        if (!$task_one) return json(["code" => 0, "msg" => '有误，请重试']);
        // elseif ($task_one["flag"] == 2) return json(["code" => 1, "msg" => '生成电焊明细成功']);
        $pts_list = [];
        foreach ($pts_name_list as $v) {
            $pts_list[] = $v["PTS_Name"];
        }
        if (!empty($pts_list)) $where["dhd.DCD_PartName"] = ["in", $pts_list];

        $group = "PTS_Name";
        $useField = "sum(PTS_Count) as PTS_Count,PTS_Name";
        $usedList = $this->sectModel->field($useField)
            ->where("PT_Num", $pt_num)
            ->group($group)
            ->select();
        $sectList = [];
        foreach ($usedList as $v) {
            $sectList[$v["PTS_Name"]] = $v["PTS_Count"];
        }

        $dh = (new DhCooperateDetail([],$this->technology_ex))->alias("dhd")
            ->join([$this->technology_ex."dhcooperate" => "dhp"], "dhd.DC_Num = dhp.DC_Num")
            ->field("dhd.DCD_ID,dhd.DCD_Count,dhd.DCD_PartName")
            ->where($where)
            ->select();

        foreach ($dh as $v) {
            $dcd_list[$v["DCD_ID"]] = $v["DCD_ID"];
            $dhList[] = [
                "DCD_ID" => $v["DCD_ID"],
                "PDH_Count" => isset($sectList[$v["DCD_PartName"]]) ? $sectList[$v["DCD_PartName"]] * $v["DCD_Count"] : $v["DCD_Count"],
                "BCDCode" => "",
                "PT_Num" => $pt_num,
                "IsSZ" => in_array($v["DCD_PartName"], $szArr) ? 1 : 0,
                "SCD_ID" => 0,
                "SCD_TPNum" => ""
            ];
        }
        $existDh = $this->dhModel->where("PT_Num",$pt_num)->column("DCD_ID,PDH_Count,BCDCode,PT_Num,IsSZ,SCD_ID,SCD_TPNum");
        $array_diff = array_udiff($dhList,$existDh,'compareArrays');
        if(empty($array_diff)) return json(["code" => 1, "msg" => '生成电焊明细成功']);
        Db::startTrans();
        try {
            $this->dhModel->where("PT_Num", $pt_num)->delete();
            $result = $this->dhModel->saveAll($dhList);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code" => 1, "msg" => '生成电焊明细成功']);
        } else {
            return json(["code" => 0, "msg" => __('No rows were inserted')]);
        }
    }


    public function viewWelding($ids = null)
    {
        // $row = $this->dhModel->where("PT_Num",$ids)->find();
        // if (!$row) {
        //     $this->error(__('No Results were found'));
        // }
        $list = $this->dhModel->alias("d")
            ->join([$this->technology_ex."dhcooperatedetail" => 'dd'], "d.DCD_ID = dd.DCD_ID")
            ->join([$this->technology_ex."dhcooperatesingle" => 'ds'], "ds.DCD_ID = dd.DCD_ID")
            ->field("CAST(dd.DCD_PartName AS UNSIGNED) AS number_1,CAST(dd.DCD_PartNum AS UNSIGNED) AS number_2,dd.DCD_PartName,dd.DCD_PartNum,d.PDH_Count,dd.DCD_SWeight,(d.PDH_Count*dd.DCD_SWeight) as SumWeight,dd.DCD_Type,dd.DCD_Memo,SUM(ds.DHS_Count) as DHS_Count,d.DCD_ID,d.PDH_ID")
            ->where("d.PT_Num", $ids)
            ->group("d.DCD_ID")
            ->order("number_1,number_2,dd.DCD_PartNum asc")
            ->select();
        $list = objectToArray($list);
        foreach ($list as $k => $v) {
            unset($list[$k]["number_1"], $list[$k]["number_2"]);
            $list[$k]["DCD_SWeight"] = round($v["DCD_SWeight"], 2);
            $list[$k]["SumWeight"] = round($v["SumWeight"], 2);
        }
        $this->view->assign("welding", $list);
        $this->assignconfig('PT_Num', $ids);

        return $this->view->fetch();
    }

    public function viewWeldingDetail()
    {
        $params = $this->request->post();
        $dcd_id = $params["DCD_ID"];
        if (!$dcd_id) return json(["code" => 0, "msg" => "有误，请重试。"]);
        $list = (new \app\admin\model\chain\lofting\DhCooperateSingle([],$this->technology_ex))->alias("ds")
            ->join([$this->technology_ex."dhcooperatedetail" => "dht"], "ds.DCD_ID=dht.DCD_ID")
            ->join([$this->technology_ex."dtmaterialdetial" => "dd"], "ds.DtMD_ID_PK = dd.DtMD_ID_PK")
            // ->field("DtMD_sPartsID,DtMD_sStuff,DtMD_sMaterial,d..DtMD_sSpecification,dd.DtMD_fWidth,dd.DtMD_fUnitWeight,ds.DHS_Count,ds.DHS_Length,ds.DHS_Grade,ds.DHS_Memo")
            ->field("dd.DtMD_sPartsID,dd.DtMD_sStuff,dd.DtMD_sMaterial,dd.DtMD_sSpecification,dd.DtMD_fWidth,dd.DtMD_fUnitWeight,ds.DHS_Count,ds.DHS_Length,ds.DHS_Grade,ds.DHS_Memo,(dht.DCD_Count*ds.DHS_Count) as Sum_Count")
            ->where("ds.DCD_ID", $dcd_id)
            ->select();
        return json(["code" => 1, "data" => $list]);
    }

    public function joinView($ids = null)
    {
        $row = $this->model->where("PT_Num", $ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        $td_id = $row["TD_ID"];
        $where = ["th.TD_ID" => ["=", $td_id]];
        $whereFlat = ["ts.PTF_TDID" => ["=", $td_id]];
        $mergetype = (new MergTypeName())->where("mTD_ID", $td_id)->select();
        $mergeList = [$td_id => $td_id];
        if ($mergetype) {
            foreach ($mergetype as $v) {
                $mergeList[$v["TD_ID"]] = $v["TD_ID"];
            }
            $where = ["th.TD_ID" => ["IN", $mergeList]];
            $whereFlat = ["ts.PTF_TDID" => ["IN", $mergeList]];
        }
        $sectList = (new TaskSect())->alias("ts")
            ->join(["sectconfigdetail" => "sect"], "sect.SCD_ID=ts.SCD_ID")
            ->join(["taskheight" => "th"], "th.TH_ID = sect.TH_ID")
            ->field("TS_Name,th.TD_ID,sect.SCD_ID,SCD_Count,TS_Count,ifBody,cast(TS_Name as UNSIGNED) AS numger,sect.SCD_TPNum")
            ->where($where)
            ->order("numger asc")
            ->select();
        // $title = [["field"=>'choose', "title"=>'选择'],["field"=>'ctrlz', "title"=>'撤销'],["field"=>"SCD_ID","title"=>"SCD_ID","visible"=>"hidden"],["field"=>'name', "title"=>'杆塔'],["field"=>"count","title"=>"数量"]];
        $fieldTitle = $isSect = [];
        foreach ($sectList as $v) {
            $fieldTitle[$v["TS_Name"]] = ["name" => $v["TS_Name"], "ifBody" => $v["ifBody"]];
            $isSect[$v["SCD_ID"]]["TD_ID"] = $v["TD_ID"];
            $isSect[$v["SCD_ID"]]["name"] = $v["SCD_TPNum"];
            $isSect[$v["SCD_ID"]]["count"] = $v["SCD_Count"];
            $isSect[$v["SCD_ID"]]["duan"][$v["TS_Name"]] = $v["TS_Count"];
        }
        $useSectList = (new ProduceTaskFlat([],$this->technology_ex))->alias("ts")
            ->join(["sectconfigdetail" => "sect"], "sect.SCD_ID=ts.SCD_ID")
            ->field("PTF_SectName,sect.SCD_ID,SCD_Count,PTF_Flag,ifbody,cast(PTF_SectName as UNSIGNED) AS numger,sect.SCD_TPNum")
            ->where($whereFlat)
            ->order("numger asc")
            ->select();
        $isUseSect = [];
        foreach ($useSectList as $v) {
            $isUseSect[$v["SCD_ID"]]["name"] = $v["SCD_TPNum"];
            $isUseSect[$v["SCD_ID"]]["count"] = $v["SCD_Count"];
            $arr = explode(" / ", $v["PTF_SectName"]);
            $sectName = $arr[1] ?? $arr[0];
            $isUseSect[$v["SCD_ID"]]["duan"][$sectName] = $v["PTF_Flag"];
        }
        $title = [];
        $content = "<thead><tr><th width='40'>选择</th><th width='40'>撤销</th><th hidden>TD_ID</th><th hidden>SCD_ID</th><th width='80'>杆塔</th><th width='40'>数量</th>";
        foreach ($fieldTitle as $k => $v) {
            $title[] = ["field" => $v["name"], "title" => $v["name"] . ($v["ifBody"] == 1 ? "" : "(专)")];
            $content .= "<th width='45'>" . $v["name"] . ($v["ifBody"] == 1 ? "" : "(专)") . "</th>";
        }
        $content .= "</tr></thead><tbody>";
        $result = [];
        foreach ($isSect as $k => $v) {
            $result[$k] = ["SCD_ID" => $k, "name" => $v["name"], "count" => $v["count"]];
            $content .= '<tr><td><input class="small_input" type="checkbox" name="choose" value="1"></td><td><input class="small_input" type="checkbox" name="ctrlz" value="1"></td><td hidden><input class="small_input" type="text" readonly name="TD_ID" value="' . $v["TD_ID"] . '"></td><td hidden><input class="small_input" type="text" readonly name="SCD_ID" value="' . $k . '"></td><td><input class="small_input" type="text" readonly name="name" value="' . $v["name"] . '"></td><td><input class="small_input" type="text" readonly name="count" value="' . $v["count"] . '"></td>';
            foreach ($fieldTitle as $kk => $vv) {
                $result[$k][$kk] = isset($v["duan"][$kk]) ? ($v["duan"][$kk] - @$isUseSect[$k]["duan"][$kk]) : "";
                $content .= '<td><input class="small_input" type="text" name="' . $kk . '" data-value="' . ($result[$k][$kk] ? $result[$k][$kk] : 0) . '" value="' . $result[$k][$kk] . '"></td>';
            }
            $content .= "</tr>";
        }
        $content .= "</tbody>";
        list($field, $tableList) = $this->releaseTable($mergeList);
        $bottomContent = listToTable($field, $tableList);
        $this->view->assign("content", $content);
        $this->view->assign("bottomTable", $bottomContent);
        // $this->view->assign("titleField",$title);
        // $this->view->assign("result",array_values($result));
        return $this->view->fetch();
    }

    public function planeRelease()
    {
        $params = $this->request->post();
        list($pt_num, $data) = array_values($params);
        if (!$pt_num or !$data) return json(["code" => 0, "msg" => "保存信息失败"]);
        $data = json_decode(str_replace('&quot;','"',$data), true);
        $count = 0;
        $table = $sectList = [];
        $delList = [];
        foreach ($data as $k => $v) {
            if ($v["name"] == "choose") $count++;
            if ($v["name"] == "SCD_ID") {
                $sectList[$v["value"]] = $v["value"];
                $table[$count]["ctrlz"] == 1 ? $delList[$v["value"]] = $v["value"] : "";
            }
            $table[$count][$v["name"]] = $v["value"];
        }

        $tasksectArr = [];
        $tasksectList = (new TaskSect())->field("TS_Name,SCD_ID,ifBody,TS_Count")->where("SCD_ID", "IN", array_values($sectList))->select();
        foreach ($tasksectList as $v) {
            $tasksectArr[$v["SCD_ID"]][$v["TS_Name"]] = $v->toArray();
        }

        $faltList = $this->flatModel->field("PTF_SectName,PTF_Flag,PTF_TDID,PT_Num,SCD_TPNum,SCD_ID,ifbody")->where("PT_Num", $pt_num)->select();
        $faltArr = [];
        foreach ($faltList as $v) {
            if (in_array($v["SCD_ID"], $delList) == false) {
                $name = $v["PTF_SectName"] . '-' . $v["PTF_TDID"] . '-' . $v["PT_Num"] . '-' . $v["SCD_TPNum"] . '-' . $v["SCD_ID"];
                $faltArr[$name] = $v->toArray();
            }
        }
        // pri($faltArr,$table,1);
        foreach ($table as $k => $v) {
            if ($v["ctrlz"] != 1) {
                $newTable = array_slice($v, 6, count($v) - 6, true);
                foreach ($newTable as $nk => $nv) {
                    if (!$nv) continue;
                    $PTF_SectName = $tasksectArr[$v["SCD_ID"]][$nk]["ifBody"] == 0 ? $v["name"] . " / " . $nk : $nk;
                    $PTF_TDID = $v["TD_ID"];
                    $PT_Num = $pt_num;
                    $SCD_TPNum = $v["name"];
                    $SCD_ID = $v["SCD_ID"];
                    $name = $PTF_SectName . '-' . $PTF_TDID . '-' . $PT_Num . '-' . $SCD_TPNum . '-' . $SCD_ID;
                    isset($faltArr[$name]) ? $faltArr[$name]["PTF_Flag"] += $nv : $faltArr[$name] = [
                        "PTF_SectName" => $PTF_SectName,
                        "PTF_Flag" => $nv,
                        "PTF_TDID" => $PTF_TDID,
                        "PT_Num" => $PT_Num,
                        "SCD_TPNum" => $SCD_TPNum,
                        "SCD_ID" => $SCD_ID,
                        "ifbody" => $tasksectArr[$v["SCD_ID"]][$nk]["ifBody"] ?? 0
                    ];
                }
            }
        }
        // $ptsectArr = [];
        // $ptsect = $this->sectModel->field("PT_Num,TS_Name,PTS_Count,PTS_IFShi,PTH_ID,PTS_Name,SCD_ID")->where("PT_Num",$pt_num)->select();
        // foreach($ptsect as $k=>$v){
        //     $ptsectArr[$v["TS_Name"]] = $v->toArray();
        // }

        // foreach($table as $k=>$v){
        //     if($v["ctrlz"]==1){
        //         foreach($tasksectArr[$v["SCD_ID"]] as $tk=>$tv){
        //             if($tv["ifBody"]==1) $ptsectArr[$tk]["PTS_Count"] -= $tv["TS_Count"];
        //             else $ptsectArr[$v["name"]." / ".$tk]["PTS_Count"] -= $tv["TS_Count"];
        //         }
        //     }

        // }
        // foreach($table as $k=>$v){

        // }

        // pri($table);die;
        // $flat = $sect = [];
        // foreach($data as $k=>$v){
        //     $sect[$v["SCD_ID"]] = $v["SCD_ID"];
        //     $length = count($v)-4;
        //     $table = array_slice($v,1,$length,true);
        //     foreach($table as $kk=>$vv){
        //         if($vv !== ''){
        //             $flat[$v["SCD_ID"].' / '.$kk] = [
        //                 "name" => $kk,
        //                 "PTF_SectName" => $kk,
        //                 "PTF_Flag" => $vv,
        //                 "PTF_TDID" => $td_id,
        //                 "PT_Num" => $pt_num,
        //                 "SCD_TPNum" => $v["name"],
        //                 "SCD_ID" => $v["SCD_ID"],
        //                 "ifbody" => 1
        //             ];
        //         }
        //     }
        // }
        // $tasksectList = (new TaskSect())->field("TS_Name,SCD_ID,ifBody")->where("SCD_ID","IN",array_values($sect))->select();
        // foreach($tasksectList as $v){
        //     $field = $v["SCD_ID"].' / '.$v["TS_Name"];
        //     if(isset($flat[$field])){
        //         if($v["ifBody"]==0){
        //             $flat[$field]["PTF_SectName"] = $flat[$field]["SCD_TPNum"].' / '.$flat[$field]["PTF_SectName"];
        //             $flat[$field]["ifbody"] = 0;
        //         }
        //     }
        // }
        // $sectSave = [];
        // foreach($flat as $v){
        //     $sectSave[$v["PTF_SectName"]] = [
        //         "PT_Num" => $pt_num,
        //         "TS_Name" => $v["PTF_SectName"],
        //         "PTS_Count" => isset($sectSave[$v["PTF_SectName"]])?($sectSave[$v["PTF_SectName"]]["PTS_Count"]+$v["PTF_Flag"]):$v["PTF_Flag"],
        //         "PTS_IFShi" => 0,
        //         "PTH_ID" => 0,
        //         "PTS_Name" => $v["name"],
        //         "SCD_ID" => $v["PTF_SectName"]==$v["name"]?"0":$v["SCD_ID"]
        //     ];
        // }
        $result = false;
        Db::startTrans();
        try {
            $count = 0;
            $count += $this->sectModel->where("PT_Num", $pt_num)->delete();
            $count += $this->sectShiModel->where("PT_Num", $pt_num)->delete();
            $count += $this->flatModel->where("PT_Num", $pt_num)->delete();

            $this->detailModel->where("PT_Num", $pt_num)->delete();
            $this->dhModel->where("PT_Num", $pt_num)->delete();

            $result = $this->flatModel->allowField(true)->saveAll($faltArr);
            $addSectList = [];
            foreach ($result as $v) {
                $PTF_SectName_list = explode(" / ", $v["PTF_SectName"]);
                $TS_Name = $v["PTF_SectName"];
                $PTS_Name = end($PTF_SectName_list);
                if (isset($addSectList[$TS_Name])) {
                    $addSectList[$TS_Name]["PTS_Count"] += $v["PTF_Flag"];
                } else {
                    $addSectList[$TS_Name] = [
                        "PT_Num" => $pt_num,
                        "TS_Name" => $TS_Name,
                        "PTS_Count" => $v["PTF_Flag"],
                        "PTS_IFShi" => 0,
                        "PTH_ID" => 0,
                        "PTS_Name" => $PTS_Name,
                        "SCD_ID" => $v["ifbody"] == 1 ? 0 : $v["SCD_ID"]
                    ];
                }
            }
            $this->sectModel->allowField(true)->saveAll($addSectList);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code" => 0, "msg" => $e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code" => 1, "msg" => '保存信息成功']);
        } else {
            return json(["code" => 0, "msg" => __('No rows were inserted')]);
        }
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds)) {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $one = $this->model->where("PT_Num", $ids)->find();
            if (!$one) $this->error('无改信息，请稍后重试');
            else if ($one["Auditor"]) $this->error('已审核，删除失败！');
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where("PT_Num", $ids)->delete();

                $this->sectModel->where("PT_Num", $ids)->delete();
                $this->sectShiModel->where("PT_Num", $ids)->delete();
                $this->flatModel->where("PT_Num", $ids)->delete();
                $this->detailModel->where("PT_Num", $ids)->delete();

                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    //确认删除 后续修改
    public function comfirmDel($dcd = null, $ids = null)
    {
        if (!$dcd) $this->error(__("Invalid parameters"));
        $del = $this->dhModel->where("PDH_ID", $dcd)->delete();
        if ($del) {
            $this->success();
        } else {
            $this->error(__('No rows were deleted'));
        }
    }

    protected function releaseTable($num = [], $fieldSearch = 'TS_Name')
    {
        $group = "pts.PT_Num,TS_Name";
        if (empty($num)) return '';
        if (count($num) == 1) {
            $where = ["th.TD_ID" => ["=", current($num)]];
            $whereUse = ["pt.TD_ID" => ["=", current($num)]];
        } else {
            $where = ["th.TD_ID" => ["IN", $num]];
            $whereUse = ["pt.TD_ID" => ["IN", $num]];
        }
        $list = (new \app\admin\model\chain\lofting\TaskSect)->alias("ts")
            ->join(["sectconfigdetail" => "sd"], "ts.SCD_ID=sd.SCD_ID")
            ->join(["taskheight" => "th"], "th.TH_ID=sd.TH_ID")
            ->field("TS_Count,CAST(TS_Name AS UNSIGNED) as number,TS_Name,ifBody,SCD_TPNum")
            ->where($where)
            ->order("number ASC")
            ->select();
        $tableList = $field = $leafP = [];
        if ($list) {
            foreach ($list as $v) {
                if ($fieldSearch == "TS_Name") $name = $v["TS_Name"];
                else $name = $v["ifBody"] == 1 ? $v["TS_Name"] : $v["SCD_TPNum"] . ' / ' . $v["TS_Name"];
                $field[$name] = $name;
                $tableList["总段数"][$name] = isset($tableList["总段数"][$name]) ? $tableList["总段数"][$name] + $v["TS_Count"] : $v["TS_Count"];
            }
            $useField = "sum(pts.PTS_Count) as PTS_Count,pts.PT_Num";
            if ($fieldSearch == "TS_Name") $useField .= ",pts.PTS_Name as TS_Name";
            else $useField .= ",pts.TS_Name";
            $usedList = $this->model->alias("pt")
                ->join([$this->technology_ex.'producetasksect' => "pts"], "pt.PT_Num = pts.PT_Num")
                ->field($useField)
                ->where($whereUse)
                ->group($group)
                ->select();
            $name = '';
            $useArr = $leafP = [];
            $leafP = $tableList["总段数"];
            if ($usedList) {
                foreach ($usedList as $v) {
                    $name = $v["TS_Name"];
                    isset($useArr[$v["PT_Num"]][$name]) ? $useArr[$v["PT_Num"]][$name] += $v["PTS_Count"] : $useArr[$v["PT_Num"]][$name] = $v["PTS_Count"];
                }
                foreach ($useArr as $uk => $uv) {
                    $tableList[$uk] = $uv;
                    foreach ($field as $uvv) {
                        $leafP[$uvv] -= @$uv[$uvv];
                    }
                }
            } else {
                foreach ($tableList["总段数"] as $k => $v) {
                    $leafP[$k] = $v;
                }
            }
            $tableList["剩余段数"] = !empty($leafP) ? $leafP : $tableList["总段数"];
        }
        return [$field, $tableList];
    }

    public function setcache($value)
    {
        $key = com_create_guid();
        Cache::set($key, $value, 300);
        $this->success('设置成功', null, $key);
    }

    //显示流程卡打印界面
    public function showLckPrint($PT_Num = null, $ptdid = '', $isapi = false)
    {
        $ptdid = Cache::get($ptdid);
        $list = $this->detailModel->alias("d")
            ->join([$this->technology_ex."dtmaterialdetial" => 'dd'], "d.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->field("d.ptd_id id,case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn,cast((case 
            when dtmd_sstuff='角钢' then 
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
            when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
            REPLACE(DtMD_sSpecification,'-','')
            else 0
            end) as UNSIGNED) bh,dd.DtMD_sStuff cl,dd.DtMD_sMaterial cz,dd.DtMD_sSpecification gg,
            d.PT_Sect*1 dh,CAST(dd.DtMD_sPartsID AS UNSIGNED) AS number_2,dd.DtMD_sPartsID,dd.DtMD_iLength,dd.DtMD_fWidth,DtMD_iTorch, type,
            ifnull(dd.DtMD_sType,'') DtMD_sType,dd.DtMD_iTorch,d.PTD_Count,d.PTD_Count as JG_Count,dd.DtMD_iUnitHoleCount,
            (dd.DtMD_fUnitWeight*PTD_Count) as SumCount,dd.DtMD_fUnitWeight,(dd.DtMD_fUnitWeight*PTD_Count) as SumWeight,
            dd.DtMD_sRemark,d.IF_Shi,dd.DtMD_iWelding,dd.DtMD_iCuttingAngle,dd.DtMD_iFireBending,dd.DtMD_fBackOff,dd.DtMD_iBackGouging,dd.DtMD_DaBian,
                dd.DtMD_KaiHeJiao,dd.DtMD_ZuanKong,dd.DtMD_GeHuo")
            ->where("d.PT_Num", $PT_Num)
            ->where(function ($query) use ($ptdid) {
                if ($ptdid) {
                    $query->where("PTD_ID in ($ptdid)");
                }
            })
            ->order("clsort,dtmd_smaterial,bh,DtMD_sType,DtMD_sSpecification,dh,bjbhn,DtMD_sPartsID")
            //            ->limit(2)
            ->select();
        // C_ProjectName TD_TypeName
        $TD_ID = db()->table($this->technology_ex.'producetask')->where("pt_num='$PT_Num'")->value('TD_ID');
        $mergetype = (new MergTypeName())->where("mTD_ID", $TD_ID)->select();
        $mergeList = [$TD_ID => $TD_ID];
        if ($mergetype) {
            foreach ($mergetype as $v) {
                $mergeList[$v["TD_ID"]] = $v["TD_ID"];
            }
        }
        $project_arr = $sect_num_arr = $flat_td_list = [];
        $project_news = (new TaskDetail())->alias("td")->join(["task" => "t"], "td.T_Num = t.T_Num")->field("t_project as C_ProjectName,TD_TypeName,TD_ID")->where("td.TD_ID", "IN", $mergeList)->select();
        foreach ($project_news as $v) {
            $project_arr[$v["TD_ID"]] = $v->toArray();
        }
        $sect_num_list = $this->flatModel->field("PTF_TDID,CASE WHEN LOCATE(' / ',PTF_SectName)=0 THEN PTF_SectName ELSE SUBSTR(PTF_SectName,(LOCATE(' / ',PTF_SectName)+3)) END AS PTF_SectName,PTF_Flag")->where("PTF_TDID", "IN", $mergeList)->where("PT_Num", $PT_Num)->select();
        foreach ($sect_num_list as $v) {
            $flat_td_list[$v["PTF_TDID"]] = $v["PTF_TDID"];
            $sect_num_arr[$v["PTF_SectName"]] = ($sect_num_arr[$v["PTF_SectName"]] ?? 0) + $v["PTF_Flag"];
        }
        if (count($flat_td_list) == 1) $TD_ID = current($flat_td_list);
        $list = objectToArray($list);
        //$this->assignconfig('list',$list);
        $res = [];
        $printdata = [];
        $datapage = 1;
        // ProduceTaskFlat
        //foreach($sect_num_arr as $kk=>$vv){
        foreach ($list as $v) {
            $tmp = [];
            //材料 DtMD_sStuff cl
            //材质 DtMD_sMaterial cz
            //规格 DtMD_sSpecification gg
            //  段 PT_Sect dh
            // 条码 bcdcode
            $v["DtMD_iTorch"] = round($v["DtMD_iTorch"], 2);
            $cl = $v['cl'];
            $cz = $v['cz'];
            $gg = $v['gg'];
            $dh = $v['dh'];
            $id = 'id_' . $v['id'];
            $jh = $v['DtMD_sPartsID'];
            $v['pg'] = $datapage;
            //获取基数
            $v['js'] = $sect_num_arr[intval($v['dh'])];
            add_node($res, $cl, '#', $cl);
            add_node($res, $cl . '&' . $cz, $cl, $cz);
            add_node($res, $cl . '&' . $cz . '&' . $gg, $cl . '&' . $cz, $gg);
            add_node($res, $cl . '&' . $cz . '&' . $gg . '&' . $dh, $cl . '&' . $cz . '&' . $gg, $dh);
            $v = array_merge($v, $project_arr[$TD_ID]);
            add_node($res, $id, $cl . '&' . $cz . '&' . $gg . '&' . $dh, $jh, $v);
            $printdata[$id] = [
                'id' => $id,
                'parent' => $cl . '&' . $cz . '&' . $gg . '&' . $dh,
                'text' => $jh,
                'data' => $v
            ];
            $datapage = $datapage + 1;
        }
        //}
        //pri($field,$tableList,1);
        //任务单表头
        $ptask = $this->model->alias("m")
            ->join(["taskdetail" => "td"], "td.TD_ID=m.TD_ID")
            ->field("m.*,td.TD_TypeName,td.TD_Pressure")
            ->where("PT_Num", $PT_Num)
            ->find();
        $ptask['idate'] = date('Y-m-d');
        $this->assignconfig('treedata', $res);
        $this->assignconfig('printdata', $printdata);
        $this->assignconfig('pagecount', count($printdata));
        $this->assign('printdata', $printdata);
        $this->assign('ptask', $ptask);
        $this->assignconfig('ptask', $ptask);

        if ($isapi) {
            return $this->view->config;
        } else {
            return $this->view->fetch();
        }
    }

    //显示加工明细打印界面
    public function showJgPrint($PT_Num = null, $ptdid = '', $isapi = false,$ispdf=0)
    {

        $ptdid = Cache::get($ptdid);
        //封面
        $ptask = $this->model->alias("m")
            ->join(["taskdetail" => "td"], "td.TD_ID=m.TD_ID")
            ->join("(SELECT t_num,customer_name FROM task t join compact c on t.C_Num=c.C_Num) tc", "tc.t_num=td.t_num")
            // ->join("(select old_TypeName,TD_ID from NewOldTDTypeName) notd","notd.TD_ID=td.TD_ID",'left')
            ->field("m.*,td.TD_TypeName,td.TD_Pressure,tc.customer_name")
            ->where("PT_Num", $PT_Num)
            ->find();
        if (!$ptask) {
            $this->error('未查询到数据');
        }
        $ptask = $ptask->toArray();
        $ptask['idate'] = date('Y-m-d');
        //        $this->assignconfig('treedata',$res);
        //        $this->assignconfig('printdata',$printdata);
        //        $this->assign('printdata',$printdata);
        $this->assign('ptask', $ptask);
        $this->assignconfig('ptask', $ptask);

        //段数汇总统计
        db()->execute("call ".$this->technology_ex."get_dhz('$PT_Num');");
        //呼高段数汇总
        $dhz = db()->query("select PT_Num,concat('(',GROUP_CONCAT(SCD_TPNum),')',TD_TypeName,'-',TH_Height ,' 段落: ') as  HeightStr ,sectStr,
                concat('[共',ltrim(sum(SCD_Count)),SCD_Unit,']',OtherStr) as OtherStr,C_Project
                from tSect 
                group by PT_Num,scd_tpnum,TD_TypeName,TH_Height,sectStr,OtherStr,C_Project");
        //判断是否有不同工程
        $ishb = false;
        foreach ($dhz as $v) {
            if ($v['C_Project'] != $ptask['C_ProjectName']) {
                $ishb = true;
                break;
            }
        }
        $dhzarr = [];
        foreach ($dhz as $v) {
            //            $sectarr=explode(',',$v['sectStr']);
            //            foreach($sectarr as $kk=>$vv) {
            //                $tmparr = explode('/', $vv);
            //                $sectarr[$kk] = trim(array_pop($tmparr));
            //            }
            //            sort($sectarr);
            //            $v['sectStr']=implode(',',$sectarr);
            if (!$ishb) {
                $dhzarr[] = $v['HeightStr'] . $v['sectStr'] . $v['OtherStr'];
            } else {
                $dhzarr[] = '<span style="line-height: 1.1em;">' . $v['C_Project'] . "<br />" . $v['HeightStr'] . $v['sectStr'] . $v['OtherStr'] . '</span>';
            }
        }
        $this->assignconfig('dhzarr', $dhzarr);
        /*$dhz=
     * array (
          0 =>
          array (
            'PT_Num' => '2110-025',
            'HeightStr' => '(10#,197#,198#)500-MC21D-FZB2-39 段落: ',
            'sectStr' => '1,2,3,4,5,6,10,15,18',
            'OtherStr' => '[共3基] ',
            'C_Project' => '崇仁-吉安东500千伏线路工程',
          ),
          1 =>
          array (
            'PT_Num' => '2110-025',
            'HeightStr' => '(156#,195#,206#)500-MC21D-FZB2-42 段落: ',
            'sectStr' => '1,2,3,4,5,6,7,16,19',
            'OtherStr' => '[共3基] ',
            'C_Project' => '崇仁-吉安东500千伏线路工程',
          ),
          2 =>
          array (
            'PT_Num' => '2110-025',
            'HeightStr' => '(42#,43#,83#,205#,207#,229#,254#)500-MC21D-FZB2-45 段落: ',
            'sectStr' => '1,2,3,4,5,6,7,17,19',
            'OtherStr' => '[共7基] ',
            'C_Project' => '崇仁-吉安东500千伏线路工程',
          ),
        )
     */
        //段别统计
        db()->query("update tSect a join (select SCD_TPNum,GROUP_CONCAT(PTF_SectName) sectstr from ".$this->technology_ex."ProduceTaskFlat where pt_Num='$PT_Num' group by SCD_TPNum) b on a.scd_tpnum=b.scd_tpnum set a.sectstr=b.sectstr;");
        $tSect = db()->query("select * from tSect;");
        $this->assignconfig('tSect', $tSect);
        $dbtj = [];
        foreach ($tSect as $v) {
            $sectarr = explode(',', $v['sectStr']);
            foreach ($sectarr as $vv) {
                $tmparr = explode('/', $vv);
                $vv = trim(array_pop($tmparr));

                $xpos = stripos($vv, '*');
                if ($xpos !== false) {
                    $xt = substr($vv, $xpos + 1);
                    $vv = substr($vv, 0, $xpos);
                    for ($i = 0; $i < $xt; $i++) {
                        if (!isset($dbtj[$vv])) {
                            $dbtj[$vv] = 0;
                        }
                        $dbtj[$vv] = $dbtj[$vv] + 1;
                    }
                } else {
                    if (!isset($dbtj[$vv])) {
                        $dbtj[$vv] = 0;
                    }
                    $dbtj[$vv] = $dbtj[$vv] + 1;
                }
            }
        }
        ksort($dbtj);
        $dbtjarr = [];
        foreach ($dbtj as $k => $v) {
            if (!isset($dbtjarr[$v])) {
                $dbtjarr[$v] = [];
            }
            $dbtjarr[$v][] = $k;
        }
        $dbtj = "";
        foreach ($dbtjarr as $k => $v) {
            $tmp = implode(',', $v);
            $dbtj = $dbtj . "[" . $tmp . "] X " . $k . "<br/>";
        }

        $tsrows = db()->query("
		select GROUP_CONCAT(TS_Name order by TS_Name+1) ts,count from (	
        select PTS_Name as TS_Name,SUM(PTS_Count) as count 
				from ".$this->technology_ex."producetasksect 
				where PT_Num='$PT_Num' 
				group by PTS_Name 
				order by cast(PTS_Name as UNSIGNED),PTS_Name ) a group by count  order by ts
		");
        $dbtj = "";
        foreach ($tsrows as $k => $v) {
            //            $tmp=implode(',',$v);
            $dbtj = $dbtj . "[" . $v['ts'] . "] X " . $v['count'] . "<br/>";
        }

        $this->assignconfig('dbtjstr', $dbtj);

        //试组装
        db()->execute("call ".$this->technology_ex."get_szz('$PT_Num');");
        //段落统计
        $ts = db()->query("select * from ts order by --pts_name;");
        $szzdltj = [];
        foreach ($ts as $v) {
            $PTS_Count = intval($v['PTS_Count']);
            if (!isset($szzdltj[$PTS_Count])) {
                $szzdltj[$PTS_Count] = [];
            }
            $szzdltj[$PTS_Count][] = $v['PTS_Name'];
        }
        $szzdltjstr = "";
        foreach ($szzdltj as $k => $v) {
            $tmp = implode(',', $v);
            $szzdltjstr = $szzdltjstr . "[" . $tmp . "] X " . $k;
        }
        $this->assignconfig('szzdltjstr', $szzdltjstr);
        //段落重量
        $tw = db()->query("select * from tw order by --pts_name;");
        $szzdlzlstr = "";
        foreach ($tw as $v) {
            $szzdlzlstr = $szzdlzlstr . $v['PTS_Name'] . "[" . $v['SW'] . "kg], ";
        }
        $this->assignconfig("szzdlzlstr", $szzdlzlstr);

        //明细1
        $mxdata = $this->detailModel->alias("d")
            ->join([$this->technology_ex."dtmaterialdetial" => 'dd'], "d.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->field("case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
                when dtmd_sstuff='角钢' then 
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
                SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
                when (dtmd_sstuff='钢板' or dtmd_sstuff='法兰') then 
                REPLACE(DtMD_sSpecification,'-','')
                else 0
                end) as UNSIGNED) bh,dd.DtMD_sMaterial cz,dd.DtMD_sSpecification gg,
                            sum(dd.DtMD_fUnitWeight*PTD_Count) zl,
                            sum(dd.DtMD_iUnitHoleCount*PTD_Count) zjks,
							sum(dd.DtMD_fUnitWeight*PTD_Count*ifnull(DtMD_iWelding/DtMD_iWelding,0) ) dh,
							sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_iCuttingAngle) qj,
							sum(dd.DtMD_fUnitWeight*PTD_Count*ifnull(DtMD_iFireBending/DtMD_iFireBending,0) ) hq,
							sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_fBackOff) cb,
							sum(dd.DtMD_fUnitWeight*PTD_Count*ifnull(DtMD_KaiHeJiao/DtMD_KaiHeJiao,0) ) khj,
							sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_DaBian) yb,
							sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_iBackGouging) qg,
							sum(dd.DtMD_fUnitWeight*PTD_Count*DtMD_GeHuo) gh,
				sum(PTD_count) zjzl,DtMD_sType")
            ->where("d.PT_Num", $PT_Num)
            ->where(function ($query) use ($ptdid) {
                if ($ptdid) {
                    $query->where("PTD_ID in ($ptdid)");
                }
            })
            ->order("clsort,cz,bh,gg")
            ->group('cz,gg,dtmd_sstuff')
            //          ->limit(100)
            ->select();
        // dump($mxdata);
        $mxsl = $this->detailModel->alias("d")
            ->join([$this->technology_ex."dtmaterialdetial" => 'dd'], "d.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->where(function ($query) use ($ptdid) {
                if ($ptdid) {
                    $query->where("PTD_ID in ($ptdid)");
                }
            })
            ->where("d.PT_Num", $PT_Num)->count();
        //        $ar=$mxdata[0]->toArray();
        $mxdata1 = [];
        $tmp = $mxdata[0]->toArray();
        $cz = $tmp['cz'];
        $sumarr = $tmp;
        $zjsumarr = $tmp;
        $mxdata1[] = $tmp;
        foreach ($mxdata as $k => $v) {
            if ($k == 0) continue;
            $tmp = $v->toArray();
            if ($cz != $tmp['cz']) {
                $sumarr['gg'] = '合计';
                $sumarr['cz'] = '';
                $mxdata1[] = $sumarr;
                $cz = $tmp['cz'];
                $sumarr = $tmp;
            } else {
                foreach ($sumarr as $kk => $vv) {
                    if (in_array($kk, ['gg', 'cz', 'DtMD_sType']) === false) {
                        $sumarr[$kk] = ($sumarr[$kk] + $tmp[$kk]);
                    }
                }
            }
            $mxdata1[] = $tmp;
            foreach ($zjsumarr as $kk => $vv) {
                if (in_array($kk, ['gg', 'cz', 'DtMD_sType']) === false) {
                    $zjsumarr[$kk] = ($zjsumarr[$kk] + $tmp[$kk]);
                }
            }
        }
        $sumarr['gg'] = '合计';
        $sumarr['cz'] = '';
        $mxdata1[] = $sumarr;
        $zjsumarr['gg'] = '总记录';
        $zjsumarr['cz'] = $mxsl . '项';
        $mxdata1[] = $zjsumarr;

        $this->assign('mxdata1', $mxdata1);
        $this->assignconfig('mxdata1', $mxdata1);

        //明细2
        $mxdata = $this->detailModel->alias("d")
            ->join([$this->technology_ex."dtmaterialdetial" => 'dd'], "d.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->field("case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when dtmd_sstuff='角钢' then 
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
            when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
            REPLACE(DtMD_sSpecification,'-','')
            else 0
            end) as UNSIGNED) bh,dd.DtMD_sMaterial cz,dd.DtMD_sSpecification gg,
            (dd.DtMD_iUnitHoleCount*PTD_Count) zjks,
            DtMD_sPartsID bjbh,
            case when LOCATE('-',DtMD_sPartsID)>0 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn
            ,DtMD_iLength cd,DtMD_fWidth kd,PTD_Count sl,
            DtMD_sRemark bz,round(PTD_Count*DtMD_fUnitWeight,2) zl, DtMD_iTorch, type, 
            concat(
            case when DtMD_iWelding >= 1 then '电焊 ' else '' end,   
            case when DtMD_iFireBending >= 1 then '制弯 ' else '' end,
            case when DtMD_iCuttingAngle >= 1 then '切角 ' else '' end,                                                   
            case when DtMD_fBackOff >= 1 then '铲背 ' else '' end,
            case when DtMD_iBackGouging >= 1 then '清根 ' else '' end,            
            case when DtMD_DaBian >= 1 then '打扁 ' else '' end,      
            case when DtMD_KaiHeJiao >= 1 then '开合角 ' else '' end ,                  
            case when DtMD_GeHuo >= 1 then '割豁' else '' end,            
            case when DtMD_ZuanKong >= 1 then '钻孔' else '' end
            ) gy
            ")
            ->where("d.PT_Num", $PT_Num)
            ->where(function ($query) use ($ptdid) {
                if ($ptdid) {
                    $query->where("PTD_ID in ($ptdid)");
                }
            })
            ->order("clsort,cz,bh,gg, bjbhn,bjbh")
            //          ->group('cz,gg,dtmd_sstuff')
            //          ->limit(100)
            ->select();

        $mxdata2 = [];
        $tmp = $mxdata[0]->toArray();
        $xh = 1;
        $cz = $tmp['cz'];
        $gg = $tmp['gg'];
        $tmp['xh'] = $xh;
        $tmp['hb'] = 0;
        $ggsumarr = $tmp;
        $czsumarr = $tmp;
        $mxdata2[] = $tmp;
        $zssum = 0; //数量合计
        $zlsum = 0; //重量合计
        foreach ($mxdata as $k => $v) {
            $zssum = $zssum + $v['sl'];
            $zlsum = $zlsum + $v['zl'];
            if ($k == 0) continue;
            $tmp = $v->toArray();
            if ($gg != $tmp['gg'] or $cz != $tmp['cz']) {
                $mxdata2[count($mxdata2) - 1]['hb'] = 1;
                $mxdata2[] = [
                    'gg' => '小计',
                    'cd' => $ggsumarr['sl'],
                    'kd' => round($ggsumarr['zl'], 1),
                ];
                $gg = $tmp['gg'];
                $ggsumarr = $tmp;
            } else {
                $ggsumarr['sl'] = round($ggsumarr['sl'] + $tmp['sl'], 2);
                $ggsumarr['zl'] = round($ggsumarr['zl'] + $tmp['zl'], 2);
            }

            if ($cz != $tmp['cz']) {
                $mxdata2[count($mxdata2) - 1]['hb'] = 1;
                $xh = 0;
                $mxdata2[] = [
                    'gg' => '合计',
                    'cd' => $czsumarr['sl'],
                    'kd' => round($czsumarr['zl'], 1),
                ];;
                $cz = $tmp['cz'];
                $czsumarr = $tmp;
            } else {
                $czsumarr['sl'] = round($czsumarr['sl'] + $tmp['sl'], 2);
                $czsumarr['zl'] = round($czsumarr['zl'] + $tmp['zl'], 2);
            }
            $xh++;
            $tmp['xh'] = $xh;
            $tmp['hb'] = 0;
            $mxdata2[] = $tmp;
        }

        $mxdata2[count($mxdata2) - 1]['hb'] = 1;
        $mxdata2[] = [
            'gg' => '小计',
            'cd' => $ggsumarr['sl'],
            'kd' => round($ggsumarr['zl'], 1),
            'hb' => 1
        ];
        $mxdata2[] = [
            'gg' => '合计',
            'cd' => $czsumarr['sl'],
            'kd' => round($czsumarr['zl'], 1),
            'hb' => 1
        ];

        //明细3
        $mxdata3 = $this->detailModel->alias("d")
            ->join([$this->technology_ex."dtmaterialdetial" => 'dd'], "d.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->field("case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when dtmd_sstuff='角钢' then 
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
            when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
            REPLACE(DtMD_sSpecification,'-','')
            else 0
            end) as UNSIGNED) bh,dd.DtMD_sMaterial cz,dd.DtMD_sSpecification gg,
            (dd.DtMD_iUnitHoleCount) zjks,
            DtMD_sPartsID bjbh,
            case when LOCATE('-',DtMD_sPartsID)>0 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn
            ,DtMD_iLength cd,DtMD_fWidth kd,PTD_Count sl,d.PT_Sect,
            DtMD_sRemark bz,round(DtMD_fUnitWeight,2) zl,
            DtMD_iCuttingAngle,                      
            DtMD_iFireBending,
            DtMD_iWelding,
            DtMD_fBackOff,
            DtMD_KaiHeJiao,
            DtMD_DaBian,
            DtMD_iBackGouging,
            DtMD_ZuanKong,
            DtMD_iTorch,
            DtMD_GeHuo
            ")
            ->where("d.PT_Num", $PT_Num)
            ->where(function ($query) use ($ptdid) {
                if ($ptdid) {
                    $query->where("PTD_ID in ($ptdid)");
                }
            })
            ->order("bjbhn,clsort,cz,bh,gg,bjbh")
            ->select();

        $group = "PT_Num,PTS_Name";
        $useField = "sum(PTS_Count) as PTS_Count,PT_Num,PTS_Name as TS_Name";
        $usedList = $this->sectModel->field($useField)
            ->where("PT_Num", $PT_Num)
            ->group($group)
            ->select();
        $data = $sectList = [];
        foreach ($usedList as $v) {
            $sectList[$v["TS_Name"]] = $v["PTS_Count"];
        }
        foreach ($mxdata3 as $k => $v) {

            $mxdata3[$k]["DD_Count"] = isset($sectList[$v["PT_Sect"]]) ? round(($v["sl"] / $sectList[$v["PT_Sect"]])) : $v["PTD_Count"];
        }

        $this->assignconfig('mxdata2', $mxdata2);
        $this->assignconfig('zssum', $zssum);
        $this->assignconfig('zlsum', round($zlsum, 2));
        $this->assignconfig('mxdata3', $mxdata3);
        $this->assignconfig('ispdf', $ispdf);
        
        if ($isapi) {
            return $this->view->config;
        } else {
            return $this->view->fetch();
        }
    }
    //电焊打印-组焊清单
    public function showZhPrint($PT_Num = null)
    {
        $ptask = $this->model->alias("m")
            ->join(["taskdetail" => "td"], "td.TD_ID=m.TD_ID")
            ->join("(SELECT t_num,customer_name FROM task t join compact c on t.C_Num=c.C_Num) tc", "tc.t_num=td.t_num", "left")
            // ->join("(select old_TypeName,TD_ID from NewOldTDTypeName) notd","notd.TD_ID=td.TD_ID","left")
            ->field("m.*,td.TD_TypeName,tc.t_num,td.TD_Pressure,tc.customer_name")
            ->where("PT_Num", $PT_Num)
            ->find();
        $ptask = $ptask->toArray();
        $ptask['idate'] = date('Y-m-d');
        //        $this->assign('ptask',$ptask);
        $this->assignconfig('ptask', $ptask);

        $list = $this->dhModel->alias("d")
            ->join([$this->technology_ex."dhcooperatedetail" => 'dd'], "d.DCD_ID = dd.DCD_ID")
            ->join([$this->technology_ex."dhcooperatesingle" => 'ds'], "ds.DCD_ID = dd.DCD_ID")
            ->field("CAST(dd.DCD_PartName AS UNSIGNED) AS number_1,CAST(dd.DCD_PartNum AS UNSIGNED) AS number_2,dd.DCD_PartName,dd.DCD_PartNum,d.PDH_Count,dd.DCD_SWeight,(d.PDH_Count*dd.DCD_SWeight) as SumWeight,dd.DCD_Type,dd.DCD_Memo,SUM(ds.DHS_Count) as DHS_Count,d.DCD_ID,d.PDH_ID")
            ->where("d.PT_Num", $PT_Num)
            ->group("d.DCD_ID")
            ->order("number_1,number_2,dd.DCD_PartNum asc")
            ->select();
        $list = objectToArray($list);
        foreach ($list as $k => $v) {
            unset($list[$k]["number_1"], $list[$k]["number_2"]);
            $list[$k]["DCD_SWeight"] = round($v["DCD_SWeight"], 2);
            $list[$k]["SumWeight"] = round($v["SumWeight"], 2);
        }
        $this->assignconfig('list', $list);

        $data = db()->query("
             select case dcd_partNum when DtMD_sPartsID then 0 else 1 end st,a.bcdcode,a.PDH_ID ,a.PDH_Count*b.DHS_Count as DHS_sumCount,b.*,case when DtMD_fWidth>0 then replace(ltrim(DtMD_iLength)+'*'+ltrim(DtMD_fWidth),'.0','') 
            else replace(ltrim(DtMD_iLength),'.0','') end as LengthWidth,case when b.DCD_PartNum=b.DTMD_sPartsID then 0 when (left(DTMD_sPartsID,1) REGEXP '^[0-9.]$')=0 then 9999999 else 
            (b.DTMD_sPartsID) end as partSort  from ".$this->technology_ex."ProduceDH a inner join 
            (SELECT   DHCooperate.DC_Num, DHCooperate.DC_EditDate, DHCooperateDetail.DCD_PartNum, 
                        DHCooperateDetail.DCD_Count, DHCooperateDetail.DCD_SWeight, DHCooperateDetail.DCD_Type, 
                        DHCooperateDetail.DCD_Memo as DtMD_sRemark, DHCooperateDetail.DCD_ImageNum, DHCooperateDetail.DCD_ImageUrl, 
                        DHCooperateDetail.DCD_ID, DHCooperateSingle.DHS_ID, DHCooperateSingle.DtMD_ID_PK, 
                        DHCooperateSingle.DHS_Count, DtMaterialDetial.DtMD_fWidth as DHS_Length, DtMaterialDetial.DtMD_iLength as DHS_Thick, 
                        DHCooperateSingle.DHS_Height, DHCooperateSingle.DHS_Grade, DHCooperateSingle.DHS_Memo, 
                        DtMaterialDetial.DtMD_sPartsID, DtMaterialDetial.DtMD_sMaterial, DtMaterialDetial.DtMD_sSpecification, 
                        DtMaterialDetial.DtMD_fUnitWeight, DtMaterialDetial.DtMD_sStuff, DtMaterialDetial.DtMD_iLength, 
                        DtMaterialDetial.DtMD_fWidth, DHCooperateDetail.BCDCode, DHCooperateDetail.DCD_PartName, 
                        DHCooperateDetail.Writer, DHCooperateDetail.WriteDate, DHCooperateDetail.Auditor, 
                        DHCooperateDetail.AuditDate, DHCooperate.TD_ID, DHCooperate.DtM_iID_PK, 
                        DHCooperateSingle.DHS_isM, DtSect.DtS_Name, DtMaterialDetial.DtMD_iExcavationCount, 
                        DtMaterialDetial.DtMD_iUnitCount, DtMaterialDetial.DtMD_iWelding, 
                        DtMaterialDetial.DtMD_iCuttingAngle, DtMaterialDetial.DtMD_fBackOff, 
                        DtMaterialDetial.DtMD_iFireBending, DtMaterialDetial.DtMD_iBackGouging, 
                        DtMaterialDetial.DtMD_ZuanKong, DtMaterialDetial.DtMD_KaiHeJiao, 
                        DtMaterialDetial.DtMD_iBoreholeCount, DtMaterialDetial.DtMD_iAssembly, DtMaterialDetial.DtMD_DaBian, 
                        DtMaterialDetial.DtMD_ZiMa, DtMaterialDetial.DtMD_sType, DtMaterialDetial.DtMD_iTorch, 
                        DtMaterialDetial.DtMD_iPerimeter, DtMaterialDetial.DtMD_ISZhuanYong, 
                        DtMaterialDetial.DtMD_iUnitHoleCount, DtMaterialDetial.DtMD_iBending
                        FROM      ".$this->technology_ex."DHCooperate DHCooperate INNER JOIN
                        ".$this->technology_ex."DHCooperateDetail DHCooperateDetail ON DHCooperate.DC_Num = DHCooperateDetail.DC_Num INNER JOIN
                        ".$this->technology_ex."DHCooperateSingle DHCooperateSingle ON DHCooperateDetail.DCD_ID = DHCooperateSingle.DCD_ID INNER JOIN
                        ".$this->technology_ex."DtMaterialDetial DtMaterialDetial ON DHCooperateSingle.DtMD_ID_PK = DtMaterialDetial.DtMD_ID_PK INNER JOIN
                        ".$this->technology_ex."DtSect DtSect ON DtMaterialDetial.DtMD_iSectID_FK = DtSect.DtS_ID_PK) b 
            on a.DCD_ID=b.DCD_ID
            where  a.PDH_Count>0  and a.PT_Num='$PT_Num' order by pdh_id,st;
        ");

        $zj = $data[0]['DCD_PartNum'];
        $data[0]['hb'] = 0;
        $data[0]['hbh'] = 1;

        foreach ($data as $k => $v) {
            $data[$k]['SumWeight'] = 0;
            foreach ($list as $item) {
                if ($item['DCD_PartName'] == $v['DCD_PartName'] and $item['DCD_PartNum'] == $v['DCD_PartNum']) {
                    $data[$k]['DCD_Count'] = $item['PDH_Count'];
                    $data[$k]['SumWeight'] = $item['SumWeight'];
                }
            }
            if ($k == 0) continue;
            $data[$k]['hb'] = 0;
            $data[$k]['hbh'] = 0;
            if ($zj != $v['DCD_PartNum']) {
                $zj = $v['DCD_PartNum'];
                $data[$k - 1]['hb'] = 1;
                $data[$k]['hbh'] = 1;
            }
        }
        $data[$k]['hb'] = 1;
        $zssum = 0; //组数合计
        $zlsum = 0; //重量合计
        foreach ($data as $v) {
            if ($v['hbh'] == 1) {
                $zssum = $zssum + $v['DCD_Count'];
                // $zlsum=$zlsum+$v['DCD_SWeight']*$v['DCD_Count'];
                $zlsum = $zlsum + $v['SumWeight'];
            }
        }
        $this->assignconfig('mxdata', $data);
        $this->assignconfig('zssum', $zssum);
        $this->assignconfig('zlsum', round($zlsum, 2));
        return $this->view->fetch();
    }


    //打印转pdf
    public function topdf()
    {
        $html = input('data');
        ob_start();
        $html = '
        <link rel="stylesheet" href="./assets/libs/hiprint/css/hiprint.css">
        <link rel="stylesheet" href="./assets/libs/hiprint/css/print-lock.css">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' . $html;
        file_put_contents("tmp.html", $html);
        ob_end_clean();
        shell_exec("wkhtmltopdf -q http://www.tower.cc/tmp.html tmp.pdf");
        //        header("Content-type:application/pdf");
        //        header("Content-Disposition:attachment;filename=tmp.pdf");
        //        echo file_get_contents("tmp.pdf");
        return $this->success('test');
    }

    public function printlck()
    {

        $data = input('js');
        $pt = input('pt');
        $key = $pt . '-' . time() . mt_rand(1, 999);
        //        exec("E:\项目\绍兴电力设备ERP\A5打印程序\A5.exe d:\ test.pdf {$data['js']}",$arr);
        cache($key, $data, 3600);
        //        $cmd="E:\项目\绍兴电力设备ERP\A5打印程序\A5.exe E:\\Codes\\tower\\20220208tower.com\\tower.com\\runtime test.pdf {$data}";
        //        exec($cmd,$arr);
        //        dump($cmd,$arr);

        $url = $_SERVER['HTTP_ORIGIN'] . "/admin.php/chain/lofting/produce_task/getCache/key/" . $key;
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/uploads';
        $filename = $key . '.pdf';
        //        $cmd="E:\项目\绍兴电力设备ERP\A5打印程序\A5.exe";
        $cmd = "A5.exe $dir $filename $url";
        //         pri($cmd,1);
        //halt($cmd);
        exec($cmd, $arr, $return_var);

        // dump($cmd,$arr,$return_var);
        $fname = $_SERVER['HTTP_ORIGIN'] . '/uploads/' . $filename;
        if (file_exists($dir . '/' . $filename)) {
            return $this->success('成功', $fname);
        } else {
            return $this->error('失败');
        }
    }


    //前端pdf访问接口
    public function getpdf($PT_Num,$ifg,$ptdid=''){
        $ptd_ids = Cache::get($ptdid);
       
        $isa5=true;
        $isjg=true;
        if($ifg=='a5'){            
            $isjg=false;         
        }else if($ifg=='jg'){
            $isa5=false;              
        }

        if(!$ptd_ids){
            //获取数据库pdf
            $respdf=db('devpdf')->where(['PT_Num' => $PT_Num])->find();
            $savefg=boolval($respdf);
            if($savefg){
                if($isa5){
                    $savefg=$savefg && $respdf['A5'];
                }
                if($isjg){
                    $savefg=$savefg && $respdf['mx2datamb'] && $respdf['mx3datamb'] && $respdf['mx4datamb'];
                }
            }
           
            if(!$savefg){
                $this->savepdf($PT_Num,$isa5,$isjg);
                $respdf=db('devpdf')->where(['PT_Num' => $PT_Num])->find();
            }
            if(!$respdf){
                return $this->error('pdf不存在');
            }
            return $this->success('成功',null,$respdf);

        }else{
            //直接生成特定数据pdf
            $res=$this->savepdf($PT_Num,$isa5,$isjg,$ptdid);
           
            $respdf=[];
            $respdf['A5']=$res['A5']['path'] ?? '';
            $respdf['mx2datamb']=$res['Jg']['mx2datamb']['path'] ?? '';
            $respdf['mx3datamb']=$res['Jg']['mx3datamb']['path'] ?? '';
            $respdf['mx4datamb']=$res['Jg']['mx4datamb']['path'] ?? '';
            return $this->success('成功',null,$respdf);
        }
    }

    //更新pdf数据库
    public function updatepdf($PT_Num,$da){
        if(!db('devpdf')->where(['PT_Num' => $PT_Num])->find()){
            db('devpdf')->insert(['PT_Num' => $PT_Num]);    
        }
        db('devpdf')->where(['PT_Num' => $PT_Num])->update($da);
    }

    //生成对应生产任务下达单的A5打印，以及加工明细表的pdf文件,并保存路径到数据库
    public function savepdf($PT_Num, $a5 = true, $jg = true,$ptd_ids='')
    {
        $serverurl='http://localhost:9527';
        debug('begin');
        $ra5 = [];
        if ($a5 === true) {
            //组合A5打印原始数据
            $A5data = $this->showLckPrint($PT_Num, $ptd_ids, true);
            // dump($A5data);
            //组合拼接数据
            // $treedata=$A5data['treedata'];
            $printdata = $A5data['printdata'];
            $ptask = $A5data['ptask'];
            $printjson = [];
            $pgcount = count($printdata);
            $pgnum = 0;
            foreach ($printdata as $k => $vo) {
                $pgnum = $pgnum + 1;
                $pd = [
                    'PT_DHmemo' => $ptask['PT_DHmemo'],
                    'PT_Num' => $ptask['PT_Num'],
                    'dh' => $vo['data']['dh'],
                    'C_ProjectName' => $vo['data']['C_ProjectName'],
                    'TD_TypeName' => $vo['data']['TD_TypeName'],
                    'PTD_Count' => $vo['data']['PTD_Count'],
                    'dzm' => '打钢印:' . ($ptask['PT_GYMemo'] ? $ptask['PT_GYMemo'] : ''),
                    'js' => $vo['data']['js'],
                    'DtMD_sPartsID' => $vo['data']['DtMD_sPartsID'],
                    'cl' => $vo['data']['cl'],
                    'cz' => $vo['data']['cz'],
                    'gg' => $vo['data']['gg'],
                    'DtMD_iLength' => $vo['data']['DtMD_iLength'],
                    'DtMD_iTorch' => floatval($vo['data']['DtMD_iTorch']) ? $vo['data']['DtMD_iTorch'] : '',
                    'DtMD_fWidth' => floatval($vo['data']['DtMD_fWidth']) ? $vo['data']['DtMD_fWidth'] : '',
                    'DtMD_iUnitHoleCount' => $vo['data']['DtMD_iUnitHoleCount'],
                    'zks' => ($vo['data']['PTD_Count'] * $vo['data']['DtMD_iUnitHoleCount']),
                    'zl' => round(($vo['data']['PTD_Count'] * $vo['data']['DtMD_fUnitWeight']), 1),
                    'DtMD_sRemark' => $vo['data']['DtMD_sRemark'],
                    'Writer' => $ptask['Writer'],
                    'Auditor' => $ptask['Auditor'],
                    'idate' => $ptask['idate'],
                    'DtMD_sType' => $vo['data']['DtMD_sType'],
                    'IF_Shi' => $vo['data']['IF_Shi'],
                    'f_IF_Shi' => $vo['data']['IF_Shi'] ? '试制' : '',
                    'f_DtMD_iWelding' => $vo['data']['DtMD_iWelding'] >= 1 ? "电焊" : '',
                    'f_DtMD_iFireBending' => $vo['data']['DtMD_iFireBending'] >= 1 ? "制弯" : '',
                    'f_DtMD_iCuttingAngle' => $vo['data']['DtMD_iCuttingAngle'] >= 1 ? "切角" : '',
                    'f_DtMD_fBackOff' => $vo['data']['DtMD_fBackOff'] >= 1 ? "铲背" : '',
                    'f_DtMD_iBackGouging' => $vo['data']['DtMD_iBackGouging'] >= 1 ? "清根" : '',
                    'f_DtMD_DaBian' => $vo['data']['DtMD_DaBian'] >= 1 ? "打扁" : '',
                    'f_DtMD_KaiHeJiao' => $vo['data']['DtMD_KaiHeJiao'] >= 1 ? "开合角" : '',
                    'f_DtMD_ZuanKong' => $vo['data']['DtMD_ZuanKong'] >= 1 ? "钻孔" : '',
                    'f_DtMD_GeHuo' => $vo['data']['DtMD_GeHuo'] >= 1 ? "割豁" : '',
                    'pg' => '第 ' . $pgnum . ' 页，共' . $pgcount . '页'
                ];
                $printjson[] = $pd;
            }
            $printjson = ['data' => $printjson];

            
            // db('devpdf')->where(['PT_Num' => $PT_Num])->update(['a5'=>'']);
            // $ra5 = api_post_hp('http://localhost:9527/A5', $printjson, ['PT_Num' => $PT_Num]);                        
            // db('devpdf')->where(['PT_Num' => $PT_Num])->update(['a5'=>'']);
            // dump($ra5);
            //$ptd_ids 为空时，为全部打印，否则为特定数据打印，不计入数据库
            !$ptd_ids && $this->updatepdf($PT_Num,['A5'=>'','errA5'=>'']);
            $rstr= api_post_hp($serverurl.'/A5', $printjson, ['PT_Num' => $PT_Num]);
            $rjson = json_decode(str_replace('&quot;','"',$rstr), true);                       
            $ra5=$rjson;
            if($rjson['err']==0){
                //更新                                
                $path=substr($rjson['data'],stripos($rjson['data'],'public')+6,100);                
                $path=preg_replace('/\\\+/i','/',$path);
                !$ptd_ids && $this->updatepdf($PT_Num,['A5'=>$path]);
                $ra5['path']=$path;
            }else{
                !$ptd_ids && $this->updatepdf($PT_Num,['errA5'=>$rjson['msg']]);
            }            

        }
        $rjg = [];
        if ($jg === true) {
            //组合加工明细表数据
            $Jgdata = $this->showJgPrint($PT_Num, $ptd_ids, true);
            // dump($Jgdata);
            //组合拼接数据
            $ptask = $Jgdata['ptask'];
            $mxdata1 = $Jgdata['mxdata1'];
            $mxdata2 = $Jgdata['mxdata2'];
            $mxdata3 = $Jgdata['mxdata2'];
            $mxdata4 = $Jgdata['mxdata3'];
            $zssum = $Jgdata['zssum'];
            $zlsum = $Jgdata['zlsum'];
            $DtMD_sType = $mxdata1[0]['DtMD_sType'] != 0 ? $mxdata1[0]['DtMD_sType'] : '';

            //明细2
            $mx2datamb = [
                'TD_TypeName' => $ptask['TD_TypeName'],
                'C_ProjectName' => $ptask['C_ProjectName'],
                'idate' => $ptask['idate'],
                'zbr' => $ptask['Writer'],
                'shr' => $ptask['Auditor'],
                'dzm' => $ptask['PT_GYMemo'],
                'hj' => "总数量：" . $zssum . ' 件，总重量：' . round($zlsum) . ' kg',
                'DtMD_sType' => $DtMD_sType,
                'PT_DHmemo' => $ptask['PT_DHmemo']
            ];
            $tb2 = [];
            foreach ($mxdata2 as $k => $v) {
                $tmp = [
                    'xh' => $v['xh'] ?? '',
                    'bjbh' => $v['bjbh'] ?? '',
                    'cz' => $v['cz'] ?? '',
                    'gg' => $v['gg'] ?? '',
                    'cd' => $v['cd'] ?? '',
                    'kd' => ($v['kd'] ??'')?:'',
                    'DtMD_iTorch' => $v['DtMD_iTorch'] ?? '',
                    'type' => $v['type'] ?? '',
                    'sl' => $v['sl'] ?? '',
                    'pl' => '',
                    'bz' => ($v['gy'] ?? '') . ($v['bz'] ?? ''),
                    'zl' => ($v['zl'] ?? '') ? round(floatval($v['zl'] ?? ''), 1) : '',
                    'hb' => $v['hb'] ?? '',
                    'zjks' => $v['zjks'] ?? '',
                    'gy' => '制弯、切角、开合角'
                ];
                $tb2[] = $tmp;
                // if($k>20) break;
            }

            $mx2datamb['tb'] = $tb2;

            // echo  json_encode($mx2datamb, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE); 
            // exit;  

            !$ptd_ids && $this->updatepdf($PT_Num,['mx2datamb'=>'','errmx2datamb'=>'']);
            $rstr= api_post_hp($serverurl.'/Jg/mx2datamb', $mx2datamb, ['PT_Num' => $PT_Num]);   
            $rjson = json_decode(str_replace('&quot;','"',$rstr), true);
            $rjg['mx2datamb']=$rjson;
            if($rjson['err']==0){
                //更新                                
                $path=substr($rjson['data'],stripos($rjson['data'],'public')+6,100);     
                $path=preg_replace('/\\\+/i','/',$path);           
                !$ptd_ids && $this->updatepdf($PT_Num,['mx2datamb'=>$path]);
                $rjg['mx2datamb']['path']=$path;
            }else{
                !$ptd_ids && $this->updatepdf($PT_Num,['errmx2datamb'=>$rjson['msg']]);
            }
                    
            //明细3
            $mx3datamb = [
                'TD_TypeName' => $ptask['TD_TypeName'],
                'C_ProjectName' => $ptask['C_ProjectName'],
                'idate' => $ptask['idate'],
                'zbr' => $ptask['Writer'],
                'shr' => $ptask['Auditor'],
                'dzm' => $ptask['PT_GYMemo'],
                'hj' => "总数量：" . $zssum . ' 件，总重量：' . round($zlsum) . ' kg',
                'DtMD_sType' => $DtMD_sType,
                'PT_DHmemo' => $ptask['PT_DHmemo']
            ];
            $tb3 = [];
            function pval3($v, $key)
            {
                $val = $v[$key] ?? '';
                if (in_array($v['gg'], ['合计', '小计'])) {
                    return round(floatval($val));
                } else {
                    return round(floatval($val), 1);
                }
                // return $val?floatval($val).toFixed(['合计','小计'].indexOf($v['gg'])>-1?0:1):'';
            }
            foreach ($mxdata3 as $k => $v) {
                $tmp = [
                    'xh' => $v['xh'] ?? '',
                    'bjbh' => $v['bjbh'] ?? '',
                    'cz' => $v['cz'] ?? '',
                    'gg' => $v['gg'] ?? '',
                    'cd' => $v['cd'] ?? '',
                    'kd' => ($v['kd'] ??'')?:'',
                    'sl' => $v['sl'] ?? '',
                    'DtMD_iTorch' => $v['DtMD_iTorch'] ?? '',
                    'type' => $v['type'] ?? '',
                    'pl' => '',
                    'bz' => $v['bz'] ?? '',
                    'zl' => pval3($v, 'zl'),
                    'hb' => $v['hb'] ?? '',
                    'zjks' => $v['zjks'] ?? '',
                    'gy' => $v['gy'] ?? ''
                ];
                $tb3[] = $tmp;
            }

            $mx3datamb['tb'] = $tb3;

            // echo  json_encode($mx3datamb, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE); 
            // exit;

            !$ptd_ids && $this->updatepdf($PT_Num,['mx3datamb'=>'','errmx3datamb'=>'']);
            $rstr= api_post_hp($serverurl.'/Jg/mx3datamb', $mx3datamb, ['PT_Num' => $PT_Num]);                                    
            $rjson = json_decode(str_replace('&quot;','"',$rstr), true);        
            $rjg['mx3datamb']=$rjson;
            if($rjson['err']==0){
                //更新                                
                $path=substr($rjson['data'],stripos($rjson['data'],'public')+6,100);    
                $path=preg_replace('/\\\+/i','/',$path);            
                !$ptd_ids && $this->updatepdf($PT_Num,['mx3datamb'=>$path]);
                $rjg['mx3datamb']['path']=$path;
            }else{
                !$ptd_ids && $this->updatepdf($PT_Num,['errmx3datamb'=>$rjson['msg']]);
            }

            //明细4
            $mx4datamb = [
                'tx' => $ptask['TD_TypeName'],
                'dw' => '',
                'rq' => $ptask['idate'],
                'zb' => $ptask['Writer'],
                'sh' => $ptask['Auditor'],
            ];
            $tb4 = [];
            foreach ($mxdata4 as $k => $v) {
                $lszl = ($v['zl'] ?? 0) - ($v['dh'] ?? 0);
                if ($lszl) {
                    $lszl = round($lszl, 1);
                    if ($lszl == 0) {
                        $lszl = '';
                    }
                } else {
                    $lszl = '';
                }
                $tmp = [
                    'ljbh' => $v['bjbh'], //零件编号
                    'cz' => $v['cz'] == "Q235B" ? "" : $v['cz'], //材质
                    'gg' => $v['gg'], //规格
                    'cd' =>($v['cd'] ??'')?:'', //长度
                    'kd' => ($v['kd'] ??'')?:'', //宽度
                    'DtMD_iTorch' =>($v['DtMD_iTorch'] ??'')?:'',
                    'djjs' => $v['DD_Count'], //单段件数
                    'zsl' => $v['sl'], //总数量
                    'djzl' => $v['zl'], //重量（kg）
                    'ks' => $v['zjks'], //单件孔数
                    'dh' => floatval($v['DtMD_iWelding']) ?: '', //电焊
                    'wq' => floatval($v['DtMD_iFireBending'])  ?: '', //弯曲
                    'qj' => floatval($v['DtMD_iCuttingAngle'])  ?: '', //切角
                    'cb' => floatval($v['DtMD_fBackOff'])  ?: '', //铲背
                    'qg' => floatval($v['DtMD_iBackGouging'])  ?: '', //清根
                    'db' => floatval($v['DtMD_DaBian'])  ?: '', //打扁
                    'khj' => floatval($v['DtMD_KaiHeJiao'])  ?: '', //开合角
                    'zk' => floatval($v['DtMD_ZuanKong'])  ?: '', //钻孔
                    'gh' => floatval($v['DtMD_GeHuo'])  ?: '',
                    'bz' => $v['bz'], //备注
                ];
                $tb4[] = $tmp;
            }

            $mx4datamb['tb'] = $tb4;

            // echo  json_encode($mx4datamb, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE); 
            // exit;

            !$ptd_ids && $this->updatepdf($PT_Num,['mx4datamb'=>'','errmx4datamb'=>'']);
            $rstr= api_post_hp($serverurl.'/Jg/mx4datamb', $mx4datamb, ['PT_Num' => $PT_Num]);                                    
            $rjson = json_decode(str_replace('&quot;','"',$rstr), true);                
            $rjg['mx4datamb']=$rjson;
            if($rjson['err']==0){
                //更新                                
                $path=substr($rjson['data'],stripos($rjson['data'],'public')+6,100);  
                $path=preg_replace('/\\\+/i','/',$path);              
                !$ptd_ids && $this->updatepdf($PT_Num,['mx4datamb'=>$path]);
                $rjg['mx4datamb']['path']=$path;
            }else{
                !$ptd_ids && $this->updatepdf($PT_Num,['errmx4datamb'=>$rjson['msg']]);
            }
        }
        debug('end');
        
        if($ptd_ids){
            return (['A5' => $ra5, 'Jg' => $rjg,'tt'=>debug('begin','end',1).'s']);
        }else{
            return Json(['A5' => $ra5, 'Jg' => $rjg,'tt'=>debug('begin','end',1).'s']);
        }
        
        
    }

    public function getCache($key)
    {
        return cache($key);
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $produce_task_one = $this->model->where("PT_Num", $num)->find();
        if ($produce_task_one["Auditor"]) return json(["code" => 0, "msg" => "已审核，审核失败"]);
        // if ($admin == "李国明" or $admin == "金杨东") {
            // $one = $this->contentModel->where("pt_num",$num)->find();
            // if($one) $this->error("已存在计件内容，如需更新计件内容，请更新列表后点击更新按键");
            // if($one) $end_save_list=[];
            // else{
            //     $workmanship_list = ['DtMD_iWelding'=>[2,'电焊'],'DtMD_iCuttingAngle'=>[4,'切角'],'DtMD_fBackOff'=>[5,'铲背'],'DtMD_iBackGouging'=>[6,'清根'],'DtMD_ZuanKong'=>[9,'钻孔'],'DtMD_GeHuo'=>[10,'割豁'],'total'=>[11,'制弯打扁开合角',['DtMD_iFireBending','DtMD_DaBian','DtMD_KaiHeJiao']]];
            //     $detail_list = $this->detailModel->alias("d")
            //         ->join(["dtmaterialdetial"=>"dd"],"d.DtMD_ID_PK = dd.DtMD_ID_PK")
            //         //DtMD_iWelding电焊DtMD_iFireBending弯曲DtMD_iCuttingAngle切角DtMD_fBackOff铲背DtMD_iBackGouging清根DtMD_DaBian打扁DtMD_KaiHeJiao开合角DtMD_ZuanKong钻孔DtMD_GeHuo割豁
            //         ->field("d.*,ifnull(DtMD_iWelding,0) as DtMD_iWelding,ifnull(DtMD_iFireBending,0) as DtMD_iFireBending,ifnull(DtMD_iCuttingAngle,0) as DtMD_iCuttingAngle,ifnull(DtMD_fBackOff,0) as DtMD_fBackOff,ifnull(DtMD_iBackGouging,0) as DtMD_iBackGouging,ifnull(DtMD_DaBian,0) as DtMD_DaBian,ifnull(DtMD_KaiHeJiao,0) as DtMD_KaiHeJiao,ifnull(DtMD_ZuanKong,0) as DtMD_ZuanKong,ifnull(DtMD_GeHuo,0) as DtMD_GeHuo,DtMD_sPartsID as parts_id,DtMD_sStuff as stuff,DtMD_sMaterial as material,DtMD_sSpecification as specification,PTD_sumWeight as sum_weight,(PTD_Count*DtMD_iUnitHoleCount) as hole_number")
            //         ->where("d.PT_Num",$num)->select();
            //     if(!$detail_list) return json(["code"=>0,"msg"=>"未生成生产明细"]);
            //     else{
            //         $detail_list = collection($detail_list)->toArray();
            //         $end_save_list = $this->_getGenerPiece($detail_list,$workmanship_list);
            //     }
            // }
            // Db::startTrans();
            // try {
            //     // if(!empty($end_save_list)) $piece_result = (new PieceContent())->allowField(true)->saveAll($end_save_list);
            //     $result = $this->model->where("PT_Num",$num)->update(['PT_Num' => $num,"flag"=>1,'Auditor'=>$admin,"AuditorDate"=>date("Y-m-d H:i:s")]);
            //     Db::commit();
            // } catch (PDOException $e) {
            //     Db::rollback();
            //     return json(["code"=>0,"msg"=>$e->getMessage()]);
            // } catch (Exception $e) {
            //     Db::rollback();
            //     return json(["code"=>0,"msg"=>$e->getMessage()]);
            // }
            $result = $this->model->where("PT_Num", $num)->update(['PT_Num' => $num, 'Auditor' => $admin, "AuditorDate" => date("Y-m-d H:i:s")]);
            if ($result != false) {
                return json(["code" => 1, "msg" => "成功", "content" => "1"]);
            } else {
                return json(["code" => 0, "msg" => "失败"]);
            }
        // } else return json(["code" => 0, "msg" => "失败"]);
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        // if ($admin == "李国明" or $admin == "金杨东") {
            // $one = $this->model->where("PT_Num",$num)->find();
            // if($one["flag"]==3) return json(["code"=>0,"msg"=>"已开始计件，无法弃审"]);
            $one = $this->model->where("PT_Num", $num)->find();
            $flag = $one["flag"]>2?$one["flag"]:1;
            $result = $this->model->where("PT_Num", $num)->update(['PT_Num' => $num, 'Auditor' => "", "AuditorDate" => "0000-00-00 00:00:00","flag"=>$flag]);
            if ($result !== false) {
                if($flag==1) (new PieceContent())->where("PT_Num", $num)->delete();
                return json(["code" => 1, "msg" => "成功", "content" => "0"]);
            } else {
                return json(["code" => 0, "msg" => "失败"]);
            }
        // } else return json(["code" => 0, "msg" => "失败"]);
    }

    // public function eipUpload()
    // {
    //     $PT_Num = $this->request->post("PT_Num");
    //     $json_result = ["code"=>0,"msg"=>"失败"];
    //     $where = [
    //         "T_Sort" => ["=","铁塔"],
    //         "PT_Num" => ["=",$PT_Num]
    //     ];
    //     $params = (new \app\admin\model\view\ProduceTaskView())->field("T_Num,PT_Num,TD_TypeName,PT_Number,WriterDate,T_Date,flag,eip_upload")->where($where)->find();
    //     if(!$params) return json($json_result);
    //     $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
    //     $saveData = [];
    //     $param_url = "Eipsupplier-wo";
    //     $operatetype = "ADD";
    //     $api_data = [
    //         "purchaserHqCode" => "SGCC",
    //         "ipoNo" => $params["T_Num"],
    //         "supplierCode" => "1000014615",
    //         "supplierName" => "绍兴电力设备有限公司",
    //         "woNo" => $params["PT_Num"],
    //         "categoryCode" => "60",
    //         "subclassCode" => "60001",
    //         "materialsCode" => $params["TD_TypeName"],
    //         "amount" => $params["PT_Number"],
    //         "unit" => "基",
    //         "planStartDate" => date("Y-m-d",strtotime($params["WriterDate"])),
    //         "planFinishDate" => date("Y-m-d",strtotime($params["T_Date"])),
    //         "woStatus" => $params["flag"]==4?5:3,
    //         "dataSource" => 0,
    //         "dataSourceCreateTime"=>date("Y-m-d H:i:s")
    //     ];
    //     if($params["eip_upload"]){
    //         $operatetype = 'UPDATE';
    //         unset($api_data["woStatus"]);
    //     }
    //     $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$api_data);
    //     if($api_result["code"]==1) $this->model->where("PT_Num",$PT_Num)->update(["eip_upload"=>1]);
    //     else $saveData[$params["PT_Num"]] = $api_result["msg"];
    //     $msg = "上传成功";
    //     if(!empty($saveData)) {
    //         $msg = "";
    //         foreach($saveData as $k=>$v){
    //             $msg .= $k.$v.";";
    //         }
    //         $json_result["msg"] = $msg;
    //     }else $json_result = ["code"=>1,"msg"=>$msg];
    //     return json($json_result);
    // }


    // public function piece($ids=null)
    // {
    //     $piece_one = $this->model->get($ids);
    //     if (!$piece_one) {
    //         $this->error(__('No Results were found'));
    //     }
    //     $row = (new Piece())->alias("p")
    //         ->join(["piece_detail"=>"pd"],"p.ID = pd.piece_id","LEFT")
    //         ->join(["dtmaterialdetial"=>'dd'],"p.DtMD_ID_PK = dd.DtMD_ID_PK","LEFT")
    //         ->field("dd.DtMD_sSpecification gg,
    //             (dd.DtMD_iUnitHoleCount*PTD_Count) zjks,
    //             DtMD_sPartsID bjbh,DtMD_iLength cd,DtMD_fWidth kd,PTD_Count sl,
    //             DtMD_sRemark bz,round(PTD_Count*DtMD_fUnitWeight,2) zl,
    //             SUM(pd.count) as count,SUM(pd.weight) as weight,SUM(pd.hole) as hole,p.ID,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
    //             SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
    //             when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
    //             when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
    //             when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
    //             when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
    //             else 0 end 	bjbhn,DtMD_sMaterial as cz
    //             ")
    //         ->where("p.PT_Num",$ids)->group("p.ID")->order("bjbhn,DtMD_sPartsID")->select();
    //     if (!$row) {
    //         $this->error(__('No Results were found'));
    //     }else{
    //         $row = collection($row)->toArray();
    //         foreach($row as $k=>$v){
    //             $row[$k]["flag"] = 0;
    //             if($v["sl"]==$v["count"] and $v["zjks"]==$v["hole"]) $row[$k]["flag"] = 1;
    //         }
    //     }

    //     // if ($this->request->isPost()) {
    //     //     $result = $this->model->where("PT_Num",$ids)->update(["piece_writer"=>$this->admin["nickname"]]);
    //     //     if($result) $this->success("保存成功！");
    //     // }
    //     $this->view->assign("piece_one",$piece_one);
    //     $this->assignconfig('ids',$ids);
    //     $this->assignconfig("row",$row);
    //     return $this->view->fetch();
    // }

    // public function editPiece($piece_id=null)
    // {
    //     $piece_one = $this->model->alias("pt")
    //         ->join(["piece"=>"p"],"pt.PT_Num = p.PT_Num")
    //         ->join(["dtmaterialdetial"=>'dd'],"p.DtMD_ID_PK = dd.DtMD_ID_PK")
    //         ->field("dd.DtMD_sMaterial cz,dd.DtMD_sSpecification gg,DtMD_iLength cd,DtMD_fWidth kd,PTD_Count sl,round(PTD_Count*DtMD_fUnitWeight,2) zl,(dd.DtMD_iUnitHoleCount*PTD_Count) zjks,pt.piece_auditor,'未完成' as finish,p.PT_Num,DtMD_sPartsID bjbh")
    //         ->where("p.ID",$piece_id)
    //         ->find();
    //     if (!$piece_one) {
    //         $this->error(__('No Results were found'));
    //     }
    //     $piece_detail_model = new PieceDetail();
    //     if ($this->request->isPost()) {
    //         $params = $this->request->post("table_row/a");
    //         $save_list = [];
    //         foreach($params["ID"] AS $k=>$v){
    //             $save_list[$k] = [
    //                 "ID" => $v,
    //                 "piece_id" => $piece_id,
    //                 "PT_Num" => $piece_one["PT_Num"],
    //                 "count" => $params["count"][$k]?$params["count"][$k]:0,
    //                 "weight" => $params["weight"][$k]?$params["weight"][$k]:0,
    //                 "hole" => $params["hole"][$k]?$params["hole"][$k]:0,
    //                 "writer_time" => $params["writer_time"][$k]?$params["writer_time"][$k]:(date("Y-m-d")),
    //                 "team" => $params["team"][$k]
    //             ];
    //             if(!$v) unset($save_list[$k]["ID"]);
    //         }
    //         $result = $piece_detail_model->allowField(true)->saveAll($save_list);
    //         $this->model->update(["piece_writer"=>$this->admin["nickname"],"piece_writer_time"=>date("Y-m-d")],["PT_Num"=>$piece_one["PT_Num"]]);
    //         if($result) $this->success();
    //         else $this->error(__('No rows were updated'));
    //     }
    //     $list = $piece_detail_model->where("piece_id",$piece_id)->order("writer_time asc")->select();
    //     if($list) $list = collection($list)->toArray();
    //     else $list = [];
    //     $count = $hole = 0;
    //     foreach($list as $v){
    //         $count += $v["count"];
    //         $hole += $v["hole"];
    //     }
    //     $msg = "合计：  数量：<span id='piece_count'>".$count."</span>  孔数：<span id='piece_hole'>".$hole."</span>";
    //     if($piece_one["sl"] == $count and $piece_one["zjks"]==$hole) $piece_one["finish"] = "完成";
    //     $this->view->assign("piece_one",$piece_one);
    //     $this->assignconfig("piece_one",[$piece_one->toArray()]);
    //     $this->view->assign("list",$list);
    //     $pieceField = $this->pieceTableField();
    //     $this->view->assign("pieceField",$pieceField);
    //     $this->view->assign("msg",$msg);
    //     $this->assignconfig("pieceField",$pieceField);
    //     $this->assignconfig("piece_id",$piece_id);
    //     return $this->view->fetch();
    // }

    // public function delPiece()
    // {
    //     $num = $this->request->post("num");
    //     if($num){
    //         $one = $this->model->alias("pt")
    //             ->join(["piece"=>"p"],"pt.PT_Num = p.PT_Num")
    //             ->join(["piece_detail"=>"pd"],"p.ID = pd.piece_id")
    //             ->where("pd.ID",$num)
    //             ->find();
    //         if(!$one) return json(["code"=>0,'msg'=>"失败！"]);
    //         else if($one["piece_auditor"]!='') return json(["code"=>0,'msg'=>"已审核，删除失败！"]);
    //         else{
    //             (new PieceDetail())->where("ID",$num)->delete();
    //             return json(["code"=>1,'msg'=>"成功！"]);
    //         }

    //     }
    //     return json(["code"=>1,'msg'=>"成功！"]);
    // }

    // public function pieceAuditor()
    // {
    //     $ID = $this->request->post("num");
    //     $admin = $this->admin["nickname"];
    //     $result = $this->model->update(['piece_auditor'=>$admin,"piece_auditor_time"=>date("Y-m-d H:i:s")],['PT_Num' => $ID]);
    //     if ($result !== false) {
    //         return json(["code"=>1,"msg"=>"审核成功"]);
    //     } else {
    //         return json(["code"=>0,"msg"=>"审核失败"]);
    //     }
    // }

    // public function pieceGiveUp()
    // {
    //     $ID = $this->request->post("num");
    //     $result = $this->model->update(['piece_auditor'=>"","piece_auditor_time"=>"0000-00-00 00:00:00"],['PT_Num' => $ID]);
    //     if ($result !== false) {
    //         return json(["code"=>1,"msg"=>"弃审成功"]);
    //     } else {
    //         return json(["code"=>0,"msg"=>"弃审失败"]);
    //     }
    // }

    //整单删除
    // public function delAllPiece()
    // {
    //     $pt_num = $this->request->post("num");
    //     $result = false;
    //     Db::startTrans();
    //     try {
    //         $result = (new PieceDetail())->where("PT_Num",$pt_num)->delete();
    //         $this->model->where("PT_Num",$pt_num)->update(["piece_writer"=>"","piece_writer_time"=>date("Y-m-d H:i:s")]);
    //         Db::commit();
    //     } catch (PDOException $e) {
    //         Db::rollback();
    //         return json(["code"=>0,"msg"=>$e->getMessage()]);
    //     } catch (Exception $e) {
    //         Db::rollback();
    //         return json(["code"=>0,"msg"=>$e->getMessage()]);
    //     }
    //     if ($result !== false) {
    //         return json(["code"=>1,"msg"=>"删除成功"]);
    //     } else {
    //         return json(["code"=>0,"msg"=>"删除失败"]);
    //     }
    // }


    // public function summary($ids = null)
    // {
    //     $list = (new piece())->alias("p")
    //         ->join(["piece_detail"=>"pd"],"p.ID = pd.piece_id","left")
    //         ->join(["dtmaterialdetial"=>'dd'],"p.DtMD_ID_PK = dd.DtMD_ID_PK","left")
    //         ->field("cast((case 
    //             when dtmd_sstuff='角钢' then 
    //             SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
    //             SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
    //             when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
    //             REPLACE(DtMD_sSpecification,'-','')
    //             else 0
    //             end) as UNSIGNED) bh,dd.DtMD_sMaterial cz,dd.DtMD_sSpecification gg,
    //             (dd.DtMD_iUnitHoleCount*PTD_Count) zjks,
    //             DtMD_sPartsID bjbh,DtMD_iLength cd,DtMD_fWidth kd,PTD_Count sl,
    //             DtMD_sRemark bz,round(PTD_Count*DtMD_fUnitWeight,2) zl,pd.*,'未完成' as 'finish',p.ID,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
    //             SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
    //             when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
    //             when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
    //             when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
    //             when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
    //             else 0 end 	bjbhn")
    //         ->where("p.PT_Num",$ids)
    //         ->order("bjbhn,DtMD_sPartsID")
    //         ->select();
    //     $data = [];
    //     foreach($list as $v){
    //         if(!isset($data[$v["ID"]])){
    //             $data[$v["ID"]] = [
    //                 "bjbh" => $v["bjbh"],
    //                 "cz" => $v["cz"],
    //                 "gg" => $v["gg"],
    //                 "cd" => $v["cd"],
    //                 "kd" => $v["kd"],
    //                 "sl" => $v["sl"],
    //                 "zl" => $v["zl"],
    //                 "zjks" => $v["zjks"],
    //                 "finish" => $v["finish"],
    //                 "piece" =>[]
    //             ];
    //         }
    //         $data[$v["ID"]]["piece"][] = [
    //             "count" => $v["count"],
    //             "weight" => $v["weight"],
    //             "hole" => $v["hole"],
    //             "writer_time" => $v["writer_time"]?date("Y-m-d",strtotime($v["writer_time"])):'',
    //             "team" => $v["team"]
    //         ];
    //     }
    //     foreach($data as $k=>$v){
    //         $row = count($data[$k]["piece"]);
    //         if(count($v["piece"])==1 and !$v["piece"][0]["count"]){
    //             $data[$k]["row"] = $row;
    //             continue;
    //         }
    //         $count = $weight = $hole = 0;
    //         foreach($v["piece"] as $kk=>$vv){
    //             $count += $vv["count"];
    //             $weight += $vv["weight"];
    //             $hole += $vv["hole"];
    //         }
    //         $data[$k]["piece"][] = [
    //             "team" => "合计",
    //             "count" => $count,
    //             "weight" => $weight,
    //             "hole" => $hole,
    //             "writer_time" => ''
    //         ];
    //         $data[$k]["row"] = $row+1;
    //         if($count==$v["sl"] and $hole==$v["zjks"]) $data[$k]["finish"] = "完成";
    //     }
    //     $this->view->assign("data",array_values($data));
    //     $this->view->assign("summary_field",$this->pieceSummaryField());
    //     $this->view->assign("other_field",$this->pieceSummaryOtherField());
    //     return $this->view->fetch();
    // }

    // protected function pieceTableField()
    // {
    //     $tableField = [
    //         ["ID","ID","text","0","hidden"],
    //         ["数量","count","text","0",""],
    //         ["重量","weight","text","0",""],
    //         ["孔数","hole","text","0",""],
    //         ["日期","writer_time","text",date("Y-m-d"),""],
    //         ["班组","team","text","",""]
    //     ];
    //     return $tableField;
    // }
    // protected function pieceSummaryField()
    // {
    //     $tableField = [
    //         ["部件编号","bjbh"],
    //         ["材质","cz"],
    //         ["规格","gg"],
    //         ["长度","cd"],
    //         ["宽度","kd"],
    //         ["数量","sl"],
    //         ["重量","zl"],
    //         ["总孔数","zjks"],
    //         ["完成情况","finish"]
    //     ];
    //     return $tableField;
    // }
    // protected function pieceSummaryOtherField()
    // {
    //     $tableField = [
    //         ["班组","team"],
    //         ["数量","count"],
    //         ["重量","weight"],
    //         ["孔数","hole"],
    //         ["日期","writer_time"]
    //     ];
    //     return $tableField;
    // }
}
