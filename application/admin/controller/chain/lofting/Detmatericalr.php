<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\BaseLibrary;
use app\admin\model\chain\lofting\BaseLibraryDetail;
use app\admin\model\chain\lofting\DhCooperate;
use app\admin\model\chain\lofting\DhCooperateDetail;
use app\admin\model\chain\lofting\DhCooperateSingle;
use app\admin\model\chain\lofting\Dtsect;
use app\admin\model\chain\lofting\Fytxkdetail;
use app\admin\model\chain\lofting\Library;
use app\admin\model\chain\lofting\MergTypeName;
use app\admin\model\chain\lofting\ProduceTask;
use app\admin\model\chain\lofting\TaskPart;
use app\admin\model\chain\lofting\TaskSect;
use app\admin\model\chain\material\PcNcfile;
use app\admin\model\chain\sale\Sectconfigdetail;
use app\admin\model\chain\sale\TaskHeight;
use app\admin\model\jichu\ch\InventoryMaterial;
use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use fast\Http;
use jianyan\excel\Excel;
use ZipArchive;

/**
 * 塔型库主管理
 *
 * @icon fa fa-circle-o
 */
class Detmatericalr extends Technology
{
    
    /**
     * Detmatericalr模型对象
     * @var \app\admin\model\chain\lofting\Dtmaterial
     */
    protected $model = null;
    protected $meList,$admin,$detailModel=null,$sectModel=null,$where = [];
    protected $delFlag = true;
    protected $editFlag = true;
    protected $noNeedLogin = ["downFieldList","closeTowerSave","selectProject","copyTowerSect","partImport","detailImport","bomBzView","bomBzAdd","bomBzDel"];

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_field = \think\Session::get('technology_field');
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_type = \think\Session::get('technology_type');
        $this->model = new \app\admin\model\chain\lofting\Dtmaterial([],$this->technology_ex);
        $this->detailModel = new \app\admin\model\chain\lofting\Dtmaterialdetail([],$this->technology_ex);
        $this->sectModel = new \app\admin\model\chain\lofting\Dtsect([],$this->technology_ex);
        $meList = $this->dtmerialList();
        $this->meList = $meList;
        $this->admin = \think\Session::get('admin');
        $this->view->assign("metrialtype",$meList);
        $this->assignconfig("metrialtype",$meList);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("m")
                ->join(["taskdetail"=>"d"],"d.TD_ID=m.TD_ID","left")
                ->join(["task"=>"t"],"d.T_Num=t.T_Num","left")
                ->field("m.T_Company,t.T_Num,t.T_Sort,m.DtM_sProject,m.DtM_sTypeName as 'm.DtM_sTypeName',m.DtM_sTypeName,m.DtM_Type,m.DtM_sPressure,m.DtM_iID_PK,m.TD_ID,DtM_sRemark")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $dList = $in = $mTD_ID = [];
            foreach($list as $v){
                $dList[$v["DtM_iID_PK"]] = $v->toArray();
                $dList[$v["DtM_iID_PK"]]["task.T_Num"] = $v["T_Num"];
                $dList[$v["DtM_iID_PK"]]['DtM_Type'] = $this->meList[$v["DtM_Type"]];
                $in[$v["TD_ID"]] = $v["TD_ID"];
            }

            $commonList = (new \app\admin\model\chain\lofting\MergTypeName())->field("mTD_ID")->where("mTD_ID","in",array_values($in))->select();
            foreach($commonList as $v){
                $mTD_ID[] = $v["mTD_ID"];
            }
            foreach($dList as $k=>$v){
                if(in_array($v["TD_ID"],$mTD_ID)) $dList[$k]["common"] = 1;
                else $dList[$k]["common"] = 0;
            }
            $result = array("total" => $list->total(), "rows" => array_values($dList));

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                // list($code,$msg) = $this->model->judRepete($params["DtM_sTypeName"],0);
                // if(!$code) $this->error($msg);
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('成功！',null,$result["DtM_iID_PK"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $tableField = $this->getTableField();
        $tableField[4][5] = $this->admin["nickname"];
        $tableField[5][5] = date("Y-m-d");
        $this->view->assign("tableField",$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->alias("m")
            ->join(["taskdetail"=>"d"],"d.TD_ID=m.TD_ID","left")
            ->join(["task"=>"t"],"d.T_Num=t.T_Num","left")
            ->field("m.TD_ID,t.C_Num,m.T_Company,t.T_Num,m.DtM_sTypeName,t.T_Sort,m.DtM_sProject,m.DtM_sSortProject,m.DtM_Type,m.DtM_sPressure,m.DtM_iID_PK,m.DtM_sAuditor,DtM_sRemark,m.DtM_sAuditor,m.DtM_dTime")
            ->where("m.DtM_iID_PK",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row["DtM_sAuditor"]) $this->editFlag = false;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            if(!$this->editFlag) $this->error("已审核，无法修改");
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params) {
                // list($code,$msg) = $this->model->judRepete($params["DtM_sTypeName"],$ids);
                // if(!$code) $this->error($msg);
                $params = $this->preExcludeFields($params);
                $sectSaveList = [];
                        
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $bjhList = [];
                foreach($sectSaveList as $k=>$v){
                    if(isset($bjhList[$v["DtS_Name"]])) return json(["code"=>0,'msg'=>"段号有重复，请进行修改！"]);
                    else $bjhList[$v["DtS_Name"]] = $v["DtS_Name"];
                    if($v["DtS_ID_PK"]==0) unset($sectSaveList[$k]["DtS_ID_PK"],$v["DtS_ID_PK"]);
                    foreach($v as $kk=>$vv){
                        if($vv=='') unset($sectSaveList[$k][$kk]);
                    }
                    $sectSaveList[$k]["DtM_iID_FK"] = $ids;
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->allowField(true)->where("DtM_iID_PK",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->sectModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $companyList = (new \app\admin\model\chain\sale\Compact())->get($row["C_Num"]);
        $row["Customer_Name"] = $companyList["Customer_Name"];
        $tdMerge = (new MergTypeName())->where("mTD_ID",$row["TD_ID"])->column("TD_ID");
        if(!$tdMerge) $tdMerge = [$row["TD_ID"]];
        $sectIfBody = (new TaskHeight())->alias("th")
            ->join(["sectconfigdetail"=>"scd"],"th.TH_ID = scd.TH_ID")
            ->join(["tasksect"=>"ts"],"scd.SCD_ID = ts.SCD_ID")
            ->where("th.TD_ID","IN",$tdMerge)
            ->group("ts.TS_Name")
            ->column("ts.TS_Name,ifBody");
        $sectArr = [];
        $isJudge = [0=>1,1=>0];
        $sectList = $this->sectModel->field("*,CAST(DtS_Name AS UNSIGNED) as number,0 as 'detailNum',0 as 'detailWeight'")->where("DtM_iID_FK",$ids)->order("number,DtS_Name ASC")->select();
        foreach($sectList as $v){
            $sectArr[$v["DtS_ID_PK"]] = $v->toArray();
            // $sectArr[$v["DtS_ID_PK"]]["detailNum"] = 0;
            $sectArr[$v["DtS_ID_PK"]]["ifBody"] = isset($sectIfBody[$v["DtS_Name"]])?$isJudge[$sectIfBody[$v["DtS_Name"]]]:'0';
            $sectArr[$v["DtS_ID_PK"]]["DtS_dWriterTime"] = $v["DtS_dWriterTime"];
            $sectArr[$v["DtS_ID_PK"]]["DtS_sAuditDate"] = $v["DtS_sAuditDate"]?date("Y-m-d H:i:s",strtotime($v["DtS_sAuditDate"])):"";
        }
        $count = count($sectArr);
        $number = $weight = 0;
        $detailList = $this->detailModel->field("sum(DtMD_iUnitCount*DtMD_fUnitWeight) as detailWeight,count(DtMD_ID_PK) as detailNum,DtMD_iSectID_FK AS DtS_ID_PK")->where("DtMD_iSectID_FK","IN",array_keys($sectArr))->group("DtMD_iSectID_FK")->select();
        foreach($detailList as $v){
            if(isset($sectArr[$v["DtS_ID_PK"]])){
                $sectArr[$v["DtS_ID_PK"]] = array_merge($sectArr[$v["DtS_ID_PK"]],$v->toArray());
                $sectArr[$v["DtS_ID_PK"]]["detailWeight"] = round($sectArr[$v["DtS_ID_PK"]]["detailWeight"],1);
                $number += $v["detailNum"];
                $weight += $sectArr[$v["DtS_ID_PK"]]["detailWeight"];
            }
        }
        $this->view->assign("row", $row);
        $this->view->assign("sectList", $sectArr);
        $this->view->assign("allCount",["count"=>$count,"number"=>$number,"weight"=>round($weight,1)]);
        $tableField = $this->getTableField();
        $this->view->assign("ids", $ids);
        $tableField[5][5] = $this->admin["nickname"];
        $tableField[6][5] = date("Y-m-d");
        $this->view->assign("tableField",$tableField);
        $this->view->assign("flag", $this->editFlag);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function detail($ids=null)
    {
        $row = $this->model->alias("a")->join([$this->technology_ex."dtsect"=>"b"],"a.DtM_iID_PK = b.DtM_iID_FK")
            // ->field("DtM_sAuditor,DtS_sAuditor")
            ->where("b.DtS_ID_PK",$ids)->find();
        $check_flag = true;
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row["DtM_sAuditor"]) $check_flag=false;
        if ($this->request->isPost()) {
            if($check_flag==false) return json(["code"=>0,'msg'=>"已审核，修改失败"]);
            $params = $this->request->post("data");
            $symbol = $this->request->post("symbol");
            $symbol = $symbol?$symbol:'*';
            $num = $this->request->post("num");
            $num = $num?$num:1;
            $params = json_decode(str_replace('&quot;','"',$params), true);
            if ($params) {
                $detailSaveList = $keyValue = [];
                $key = "";
                $biZhong = (new InventoryMaterial())->getIMPerWeight();
                foreach($params as $v){
                    $key = rtrim($v["name"],"[]");
                    isset($keyValue[$key])?"":$keyValue[$key] = [];
                    $keyValue[$key][] = $v["value"];
                }
                
                foreach($keyValue as $k=>$v){
                    foreach($v as $kk=>$vv){
                        if($k == "DtMD_iUnitCount") $detailSaveList[$kk][$k] = $vv==""?0:round(eval('return '.$vv.$symbol.$num.";"),2);
                        else if($k=="DtMD_sPartsID") $detailSaveList[$kk][$k] = substr($vv,0,1)=="0"?substr($vv,1):$vv;
                        else $detailSaveList[$kk][$k] = trim($vv);
                    }
                }
                
                $bjhList = [];
                foreach($detailSaveList as $k=>$v){
                    if(isset($bjhList[$v["DtMD_sPartsID"]])) return json(["code"=>0,'msg'=>"部件号有重复，请进行修改！"]);
                    else $bjhList[$v["DtMD_sPartsID"]] = $v["DtMD_sPartsID"];
                    if($v["DtMD_ID_PK"]==0) unset($detailSaveList[$k]["DtMD_ID_PK"],$v["DtMD_ID_PK"]);
                    // foreach($v as $kk=>$vv){
                    //     if($vv=='') unset($detailSaveList[$k][$kk]);
                    // }

                    $DtMD_sStuff = "";
                    $v["DtMD_sSpecification"] = str_replace("L","∠",$v["DtMD_sSpecification"]);
                    $v["DtMD_sSpecification"] = str_replace("∟","∠",$v["DtMD_sSpecification"]);
                    $v["DtMD_sSpecification"] = str_replace("X","*",$v["DtMD_sSpecification"]);
                    $v["DtMD_sSpecification"] = str_replace("x","*",$v["DtMD_sSpecification"]);
                    $v["DtMD_sSpecification"] = str_replace("Φ","ф",$v["DtMD_sSpecification"]);
                    if(!$v["DtMD_sStuff"]){
                        $firstField = mb_substr(trim($v["DtMD_sSpecification"]),0,1);
                        if($firstField == '∠') $DtMD_sStuff = '角钢';
                        elseif($firstField == '-') $DtMD_sStuff = '钢板';
                        elseif($firstField == '[') $DtMD_sStuff = '槽钢';
                        elseif($firstField == 'G') $DtMD_sStuff = '格栅板';
                        else{
                            preg_match_all("/\d+\.?\d?/",$v["DtMD_sSpecification"],$matches);
                            if(count($matches[0])>=2){
                                if($this->technology_type==1) $DtMD_sStuff = '钢管';
                                else $DtMD_sStuff = '法兰';
                            }else $DtMD_sStuff = '圆钢';
                        }
                        
                        $v["DtMD_sStuff"] = $detailSaveList[$k]["DtMD_sStuff"] = $DtMD_sStuff;
                    }
                    

                    $length = $v["DtMD_iLength"]?$v["DtMD_iLength"]*0.001:0;
                    $width = $v["DtMD_fWidth"]?$v["DtMD_fWidth"]*0.001:0;
                    $area = $v["DtMD_sStuff"]!='钢板'?$length:$length*$width*abs($v["DtMD_sSpecification"]);
                    $DtMD_fUnitWeight = 0;
                    if($v["DtMD_sStuff"]=="圆钢"){
                        preg_match_all("/\d+\.?\d?/",$v["DtMD_sSpecification"],$matches);
                        $L = isset($matches[0][0])?$matches[0][0]:0;
                        $DtMD_fUnitWeight = round($L*$L*$length*0.00617,2);
                    }else if($v["DtMD_sStuff"]=="钢管" or $v["DtMD_sStuff"]=="法兰"){
                        preg_match_all("/\d+\.?\d?/",$v["DtMD_sSpecification"],$matches);
                        $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                        $r = isset($matches[0][1])?$matches[0][1]/2*0.001:0;
                        if(strpos($v["DtMD_sSpecification"],"*")) $r = $R-(2*$r);
                        $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
                    }else if($v["DtMD_sStuff"]=="格栅板"){
                        $rou = ["G255/40/100W"=>32.1,"G253/40/100W"=>21.3];
                        $rou_one = isset($rou[$v["DtMD_sSpecification"]])?$rou[$v["DtMD_sSpecification"]]:0;
                        $DtMD_fUnitWeight = round($rou_one * $length*$width,4);
                    }else{
                        $DtMD_fUnitWeight = (isset($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]]))?round($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]] * $area,4):0;
                    }
                    // $DtMD_fUnitWeight = (isset($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]]))?round($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]] * $area,4):0;
                    $detailSaveList[$k]["DtMD_fUnitWeight"] = $DtMD_fUnitWeight?$DtMD_fUnitWeight:$v["DtMD_fUnitWeight"];
                    $detailSaveList[$k]["DtMD_iSectID_FK"] = $ids;
                }
                Db::startTrans();
                try {
                    $result = $this->detailModel->allowField(true)->saveAll($detailSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (PDOException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (Exception $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                }
                if ($result) {
                    return json(["code"=>1,'msg'=>"保存成功"]);
                } else {
                    return json(["code"=>0,'msg'=>"保存失败"]);
                }
            }
            return json(["code"=>0,'msg'=>__('Parameter %s can not be empty')]);
        }
        $DtS_Name = $row["DtS_Name"];
        $DtM_iID_FK = $row["DtM_iID_FK"];
        $sectList = $this->sectModel->field("DtS_ID_PK,DtS_Name,CAST(DtS_Name AS SIGNED) as number")->where("DtM_iID_FK",$DtM_iID_FK)->order("number")->select();
        $sectArr = $field = $detailList = [];
        foreach($sectList as $v){
            $sectArr[$v["DtS_ID_PK"]] = $v["DtS_Name"];
        }
        
        $tableField = $this->getDetailField();
        foreach($tableField as $v){
            if($v[1]!="sumWeight") $field[] = $v[1];
        }
        $end_data = $this->detailModel->getEndData($field,$ids,$row["TD_ID"]);
        $this->view->assign("allCount",["count"=>$end_data['count'],"number"=>$end_data['number'],"weight"=>$end_data['weight'],"knumber"=>$end_data['knumber']]);
        $this->view->assign("DtS_Name", $DtS_Name);
        $this->view->assign("ids", $ids);
        $this->assignconfig("dids",$row["DtM_iID_PK"]);
        $this->view->assign("sectArr", $sectArr);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("detailList",$end_data['detailArr']);
        $this->view->assign("check_flag",$check_flag);
        $this->assignconfig("check_flag",$check_flag);
        return $this->view->fetch();
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        // $one = $this->detailModel->alias("dd")
        //     ->join(["taskpart"=>"tp"],"tp.DtMD_ID_PK = dd.DtMD_ID_PK")
        //     ->where("dd.DtMD_ID_PK",$num)
        //     ->find();
        // if($one) return json(["code"=>0,'msg'=>"该明细已调拨工程明细，删除失败！"]);
        if($num){
            $one = $this->model->alias("a")->join(["dtsect"=>"b"],"a.DtM_iID_PK = b.DtM_iID_FK")
                ->join([$this->technology_ex."dtmaterialdetial"=>"c"],"b.DtS_ID_PK = c.DtMD_iSectID_FK")
                ->field("*")
                ->where("c.DtMD_ID_PK",$num)->find();
            $del = (new PcNcfile())->where(["PartNCPath"=>["=",$one["TD_ID"]."-".$one["DtMD_sPartsID"]]])->delete();
            if($one["DtM_sAuditor"]) return json(["code"=>0,'msg'=>"已审核，删除失败"]);
            Db::startTrans();
            try {
                $this->detailModel->where("DtMD_ID_PK",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function delDetail()
    {
        $num = $this->request->post("num");
        if($num){
            $one = $this->model->alias("m")->join([$this->technology_ex."dtsect"=>"s"],"m.DtM_iID_PK = s.DtM_iID_FK")->where("s.DtS_ID_PK",$num)->find();
            if(!$one) return json(["code"=>0,"msg"=>"无该信息，请稍后重试"]);
            else if($one["DtM_sAuditor"]) return json(["code"=>0,"msg"=>"已审核本数据，无法进行段删除"]);
            // $one = $this->sectModel->alias("ds")
            //     ->join(["dtmaterialdetial"=>"dd"],"ds.DtS_ID_PK = dd.DtMD_iSectID_FK")
            //     ->join(["taskpart"=>"tp"],"tp.DtMD_ID_PK = dd.DtMD_ID_PK")
            //     ->where("ds.DtS_ID_PK",$num)
            //     ->find();
            // if($one) return json(["code"=>0,'msg'=>"该段已调拨工程明细，删除失败！"]);
            Db::startTrans();
            try {
                $detailList = $this->detailModel->where("DtMD_iSectID_FK",$num)->column("DtMD_ID_PK,DtMD_sPartsID");
                $detailId = array_keys($detailList);
                $this->sectModel->where("DtS_ID_PK",$num)->delete();
                (new PcNcfile())->where(["TD_ID"=>["=",$one["TD_ID"]],"PartName"=>["IN",$detailList]])->delete();
                if(!empty($detailId)){
                    $this->detailModel->where("DtMD_iSectID_FK",$num)->delete();
                }
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
            return json(["code"=>1,'msg'=>"删除成功！"]);
        }
        return json(["code"=>0,'msg'=>"删除失败！"]);
    }

    // public function copySect($ids='')
    // {
    //     if(!$ids) return json(["code"=>0,'msg'=>'调拨失败']);

    //     $td_one = $this->model->field("TD_ID")->where("DtM_iID_PK",$ids)->find();
    //     if($td_one) $td_id = $td_one["TD_ID"];

    //     $sectArr = (new TaskSect())->sectAllocation($ids,$td_id,$this->admin["nickname"]);
    //     $isSectArr = [];
    //     $isSectList = $this->sectModel->field("DtS_Name")->where("DtM_iID_FK",$ids)->select();
    //     foreach($isSectList as $v){
    //         $isSectArr[$v["DtS_Name"]] = $v["DtS_Name"];
    //     }
    //     $diff = array_diff_key($sectArr,$isSectArr);
    //     $result = $this->sectModel->allowField(true)->saveAll($diff);
    //     if($result !== false) return json(["code"=>1,'msg'=>"调拨成功"]);
    // }

    public function copyTowerSect($ids='')
    {
        $one = $this->model->where("DtM_iID_PK",$ids)->find();
        if(!$one) return json(["code"=>0,'msg'=>"失败，请稍后重试"]);
        elseif($one["DtM_sAuditor"]) return json(["code"=>0,'msg'=>"已审核，导入失败"]);
        list($params,$way) = array_values($this->request->post());
        $params = json_decode(str_replace('&quot;','"',$params), true);
        if($params){
            // $td_one = $this->model->field("TD_ID")->where("DtM_iID_PK",$ids)->find();
            $td_id = $one["TD_ID"];
            // $resultUp = (new Sectconfigdetail())->alias("sect")
            //     ->join(["taskheight"=>"th"],"sect.TH_ID = th.TH_ID")
            //     ->where("sect.SCD_FYFlag",1)
            //     ->where("th.TD_ID",$td_id)
            //     ->find();
            // if($resultUp) return json(["code"=>0,'msg'=>'复制失败，工程部件明细已调拨']);
            $merge_rows = $merge_where = [];
            $merge_list = (new \app\admin\model\chain\lofting\MergTypeName())->where("mTD_ID",$td_id)->select();
            foreach($merge_list as $v){
                $merge_rows[] = $v["TD_ID"];
            }
            if(!empty($merge_rows)) $merge_where["th.TD_ID"]= ["IN",$merge_rows];
            ELSE $merge_where["th.TD_ID"]= ["=",$td_id];
            $sectList = (new TaskSect())->alias("ts")
                ->join(["sectconfigdetail"=>"sd"],"sd.SCD_ID = ts.SCD_ID")
                ->join(["taskheight"=>"th"],"sd.TH_ID = th.TH_ID")
                ->field("ts.TS_Name as DtS_Name,CAST(ts.TS_Name AS UNSIGNED) as number")
                ->where($merge_where)
                ->order("number")
                ->select();
            $onlySect = [];
            foreach($sectList as $v){
                $onlySect[$v["DtS_Name"]] = $v["DtS_Name"];
            }

            $sectArr = $isSectArr = [];
            foreach($sectList as $v){
                $sectArr[$v["DtS_Name"]] = $v->toArray();
            }

            $DtS_NameList = $delList = $addList = $cannotEdit = [];
            $type = "";
            foreach($params as $k=>$v){
                $type = $v["type"];
                unset($params[$k][0]);
                if(in_array($v["DtS_Name"],$onlySect) !== false){
                    $DtS_NameList[] = $v["DtS_Name"];
                    $addList[$v["DtS_ID_PK"]] = ["DtS_Name"=>$v["DtS_Name"],"DtM_iID_FK"=>$ids,"DtS_sWriter"=>$this->admin["nickname"],"DtS_dWriterTime"=>date("Y-m-d H:i:s"),"DtS_dModifyTime"=>date("Y-m-d H:i:s")];
                }
            }
            if($type == "tower") $detailModel = new Fytxkdetail([],$this->technology_ex);
            elseif($type == "project") $detailModel = $this->detailModel;
            else $detailModel = new BaseLibraryDetail([],$this->technology_ex);

            $list = $this->sectModel->field("DtS_ID_PK,DtS_Name")->where("DtM_iID_FK",$ids)->select();
            Db::startTrans();
            try {
                foreach($list as $v){
                    // if($v["SCD_FYFlag"]==1){
                        // $cannotEdit[] = $v["DtS_Name"];
                    // }else 
                    if(in_array($v["DtS_Name"],$DtS_NameList)){
                        // if($way == "cover") $v->delete();
                        $delList[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                    }
                }
                if($way == "cover"){
                    $dhcooperatek_model = new DhCooperate([],$this->technology_ex);
                    $dhcooperatek_detail_model = new DhCooperateDetail([],$this->technology_ex);
                    $dhcooperatek_single_model = new DhCooperateSingle([],$this->technology_ex);
                    $dh_num_list = $dcd_id_list = [];
                    $dts_name = array_keys($delList);
                    $dh_list = $dhcooperatek_model->field("DC_Num")->where("DtM_iID_PK",$ids)->select();
                    foreach($dh_list as $k=>$v){
                        $dh_num_list[] = $v["DC_Num"];
                    }
                    if(!empty($dh_num_list)){
                        $dh_detail_list = $dhcooperatek_detail_model->field("DCD_ID")->where([
                            "DC_Num" => ["IN",$dh_num_list],
                            "DCD_PartName" => ["IN",$dts_name]
                        ])->select();
                        foreach($dh_detail_list as $k=>$v){
                            $dcd_id_list[] = $v["DCD_ID"];
                            $v->delete();
                        }
                        if(!empty($dcd_id_list)) $dhcooperatek_single_model->where([
                            "DCD_ID" => ["IN",$dcd_id_list]
                        ])->delete();
                    }
                    $partIdList = $this->detailModel->where("DtMD_iSectID_FK","in",array_values($delList))->column("DtMD_sPartsID");
                    $this->detailModel->where("DtMD_iSectID_FK","in",array_values($delList))->delete();
                    (new PcNcfile())->where(["TD_ID"=>["=",$td_id],"PartName"=>["IN",$partIdList]])->delete();
                }
                foreach($addList as $k=>$v){
                    if(isset($delList[$v["DtS_Name"]])) $addList[$k]["DtS_ID_PK"] = $delList[$v["DtS_Name"]];
                }

                $sectResult = $this->sectModel->allowField(true)->saveAll(array_values($addList));
                $idsList = $detailArr = [];
                foreach($sectResult as $v){
                    $idsList[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                }

                $detailList = $detailModel->field("*,(CASE WHEN LEFT(DtMD_sPartsID, 1) = '0' THEN SUBSTR(DtMD_sPartsID,2) ELSE DtMD_sPartsID END) as DtMD_sPartsID")->where("DtMD_iSectID_FK","in",array_keys($addList))->select();
                foreach($detailList as $k=>$v){
                    $detailArr[$k] = $v->toArray();
                    $detailArr[$k]["DtMD_iSectID_FK"] = $idsList[$addList[$v["DtMD_iSectID_FK"]]["DtS_Name"]];
                    unset($detailArr[$k]["DtMD_ID_PK"]);
                }
                $detailResult = $this->detailModel->allowField(true)->saveAll($detailArr);
                Db::commit();
                return json(["code"=>1,'msg'=>"成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }

            
            
        }
        return json(["code"=>0,'msg'=>"失败"]);
    }

    public function closeTower($ids){
        $TD_ID = $this->model->field("TD_ID")->where("DtM_iID_PK",$ids)->find();
        $rows = [];
        $list = (new \app\admin\model\chain\lofting\MergTypeName())->where("mTD_ID",$TD_ID["TD_ID"])->select();
        foreach($list as $v){
            $rows[] = $v["TD_ID"];
        }
        $detailList = (new \app\admin\model\chain\sale\TaskDetail())->alias("d")
            ->join(["task"=>"t"],"t.T_Num=d.T_Num",'LEFT')
            ->field("t.T_Num,t.C_Num,t.t_project,d.TD_ID,d.TD_Pressure,d.TD_TypeName")
            ->where("d.TD_ID","in",$rows)
            ->select();
        $this->view->assign("TD_ID",$TD_ID["TD_ID"]);
        $this->view->assign("rows",$detailList);
        return $this->view->fetch();
    }

    public function closeTowerSave(){
        $data = $this->request->post("data");
        $data = json_decode(str_replace('&quot;','"',$data), true);
        $tdId = $this->request->post("tdId");
        if(!$data or !$tdId) return json(["code"=>0,'msg'=>"失败"]);
        $mergeModel = new \app\admin\model\chain\lofting\MergTypeName();
        $mergeModel->where("mTD_ID",$tdId)->delete();
        if(!in_array($tdId,$data)) $data[] = $tdId;
        if(count($data)==1 and $data[0]==$tdId) return json(["code"=>1,'msg'=>"成功"]);
        $addParams = [];
        foreach($data as $v){
            $addParams[] = ["TD_ID"=>$v,"mTD_ID"=>$tdId];
        }
        $result = $mergeModel->saveAll($addParams,true);
        if($result) return json(["code"=>1,'msg'=>"成功"]);
        else return json(["code"=>0,'msg'=>"失败"]);
    }

    public function downField($ids=null)
    {
        $sectArr=[];
        if($ids){
            $td_one = $this->model->field("TD_ID")->where("DtM_iID_PK",$ids)->find();
            if($td_one) $td_id = $td_one["TD_ID"];

            $sectArr = (new TaskSect())->sectAllocation($ids,$td_id,$this->admin["nickname"]);
        }
        $this->view->assign("selfTableContent",json_encode(array_values($sectArr)));
        $this->view->assign("selfArr",json_encode(array_keys($sectArr)));
        $this->view->assign("ids",$ids);
        return $this->view->fetch();
    }

    public function downFieldList($type="tower")
    {
        $params = $this->request->post();
        $where = [];
        $list = [];
        if($type=="tower"){
            unset($params["DtM_sProject"],$params["ids"]);
            foreach($params as $k=>$v){
                $where[$k] = ["LIKE","%".$v."%"];
            }
            $list = (new Library([],$this->technology_ex))->field("DtM_iID_PK,DtM_sPictureNum,DtM_sPressure,DtM_sTypeName")->order("DtM_iID_PK DESC")->where($where)->limit(50)->select();
        }else if($type == "project"){
            foreach($params as $k=>$v){
                if($k == "ids") $where["DtM_iID_PK"] = ["<>",$v];
                else $where["dt.".$k] = ["LIKE","%".$v."%"];
            }
            $list = $this->model->alias("dt")
                ->join(["taskdetail"=>"td"],"dt.TD_ID=td.TD_ID")
                ->field("dt.TD_ID,dt.DtM_iID_PK,dt.DtM_sProject,dt.DtM_sPressure,td.T_Num,dt.DtM_sTypeName")
                ->where($where)
                ->order("dt.DtM_iID_PK DESC")
                ->limit(50)
                ->select();
        }else{
            $where = [];
            if($params["DtM_sTypeName"]) $where["DtM_sTypeName"] = ["LIKE","%".$params["DtM_sTypeName"]."%"];
            if($params["DtM_sPressure"]) $where["DtM_sPressure"] = ["LIKE","%".$params["DtM_sPressure"]."%"];
            $list = (new BaseLibrary([],$this->technology_ex))->where($where)->limit(50)->order("DtM_iID_PK DESC")->select();
        }
        $data = [];
        foreach($list as $k=>$v){
            $data[$k] = $v->toArray();
            $data[$k]["type"] = $type;
        }
        return json(["code"=>1,"data"=>$data]);
    }

    public function selectProject()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\sale\TaskDetail())->alias("d")
                ->join(["task"=>"t"],"d.T_Num=t.T_Num","left")
                ->join(["compact"=>"c"],"c.C_Num=t.C_Num","left")
                ->field("d.TD_ID,t.C_Num,t.C_Num as 't.C_Num',c.PC_Num,t.T_Num,t.T_Num as 't.T_Num',t.T_Sort,t.T_Company,t.T_WriterDate,t.t_project,t.t_shortproject,c.Customer_Name,d.TD_TypeName,d.TD_Pressure")
                ->where($where)
                ->where("c.produce_type",$this->technology_type)
                // ->where("NOT EXISTS (SELECT 'x' FROM dtmaterial dt WHERE dt.TD_ID=d.TD_ID)")
                ->order($sort, $order)
                ->paginate($limit);
            $dList = [];
            foreach($list as $v){
                $dList[$v["TD_ID"]] = $v->toArray();
                // $dList[$v["TD_ID"]]["t.C_Num"] = $v["C_Num"];
                $dList[$v["TD_ID"]]["TD_TypeNameGY"] = $dList[$v["TD_ID"]]["TD_TypeName"];
                $dList[$v["TD_ID"]]["TD_Count"] = 0;
            }
            $countList = (new \app\admin\model\chain\sale\TaskHeight())
                ->field("sum(TD_Count) as TD_Count,TD_ID")
                ->where("TD_ID","in",array_keys($dList))
                ->group("TD_ID")
                ->select();
            foreach($countList as $v){
                $dList[$v["TD_ID"]]["TD_Count"] = $v["TD_Count"];
            }
            $result = array("total" => $list->total(), "rows" => array_values($dList));

            return json($result);
        }
        return $this->view->fetch();
    }

    public function copyDetail()
    {
        $ids = $this->request->post("ids");
 
        $td_one = $this->model->alias("dt")->join([$this->technology_ex."dtsect"=>"ds"],"ds.DtM_iID_FK = dt.DtM_iID_PK")->where("dt.DtM_iID_PK",$ids)->find();
        // $td_id = [];
        if($td_one){
            // if($td_one["DtM_sAuditor"]) return json(["code"=>0,'msg'=>"已审核，调拨失败"]);
            $td_list = [];
            $mTD_ID = (new MergTypeName())->where("mTD_ID",$td_one["TD_ID"])->select();
            if($mTD_ID){
                foreach($mTD_ID as $v){
                    $td_list[$v["TD_ID"]] = $v["TD_ID"];
                }
                $wheretaskList = ["th.TD_ID"=>["IN",$td_list]];
                $this->where = $where = ["TD_ID"=>["IN",$td_list]];
                
            }else{
                $wheretaskList = ["th.TD_ID"=>["=",$td_one["TD_ID"]]];
                $this->where = $where = ["TD_ID"=>["=",$td_one["TD_ID"]]];
            }
        }
        else return json(["code"=>0,'msg'=>"没有段明细，调拨失败"]);
        // $this->ids= $td_id;

        $taskheightModel = new TaskHeight();
        $dtmaterialdetialList = $this->sectModel->alias("ds")
            ->join([$this->technology_ex."dtmaterialdetial"=>"dd"],"ds.DtS_ID_PK = dd.DtMD_iSectID_FK")
            ->field("ifnull(dd.DtMD_iWelding,0) as TP_Welding,dd.DtMD_iUnitCount as TP_PackCount,dd.DtMD_ID_PK as DtMD_ID_PK,dd.DtMD_sPartsID as TP_PartsID,dd.DtMD_iUnitCount as TP_UnitCount,ifnull(dd.DtMD_fUnitWeight,0) as TP_UnitWeight,dd.DtMD_iUnitCount as TP_SumCount,dd.DtMD_fUnitWeight as TP_SumWeight,ds.DtS_Name as TP_SectName,CAST(ds.DtS_Name AS UNSIGNED) AS number_1,CAST(dd.DtMD_sPartsID AS UNSIGNED) AS number_2")
            ->where("ds.DtM_iID_FK",$ids)
            // ->where("NOT EXISTS ( SELECT 'x' FROM taskpart tp WHERE tp.DtMD_ID_PK=dd.DtMD_ID_PK)")
            ->order("number_1,DtS_Name,number_2,DtMD_sPartsID ASC")
            ->select();
        //得到段id
        
        $taskList = $taskheightModel->alias("th")
            ->join(["sectconfigdetail"=>"scd"],'th.TH_ID = scd.TH_ID')
            ->join(["tasksect"=>"ts"],"ts.SCD_ID = scd.SCD_ID")
            ->field("th.TD_ID as TD_ID,scd.TH_ID as TH_IDFK,ts.TS_Name,ts.TS_Count,scd.SCD_ID as SCD_ID,scd.SCD_Count as TH_Count,ts.ifBody")
            ->where($wheretaskList)
            ->select();
        $scd_list = $commonList = [];
        foreach($taskList as $v){
            if($v["ifBody"]==1){
                isset($commonList[$v["TS_Name"]][$v["TH_IDFK"]])?($commonList[$v["TS_Name"]][$v["TH_IDFK"]]["TH_Count"] += $v["TH_Count"]):$commonList[$v["TS_Name"]][$v["TH_IDFK"]] = $v->toArray();
            }else $scd_list[$v["TS_Name"]][$v["SCD_ID"]] = $v->toArray();
        }
        $data = [];
        foreach($dtmaterialdetialList as $v){
            
            if(isset($commonList[$v["TP_SectName"]])){
                foreach($commonList[$v["TP_SectName"]] as $ck=>$cv){
                    $content = $v->toArray();
                    $content =array_merge($content,$cv);
                    $content["SCD_ID"] = 0;
                    $content["TP_PackCount"] = $content["TP_SumCount"] = $content["TP_UnitCount"] = $content["TS_Count"]*$content["TP_UnitCount"];
                    $content["TP_SumCount"] = $content["TH_Count"]*$content["TP_PackCount"];
                    $content["TP_SumWeight"] = round($content["TP_UnitWeight"]*$content["TP_SumCount"],2);
                    $data[$ck.'-0-'.$content["DtMD_ID_PK"]] = $content;
                }
            }else if(isset($scd_list[$v["TP_SectName"]])){
                foreach($scd_list[$v["TP_SectName"]] as $cv){
                    $content = $v->toArray();
                    $content =array_merge($content,$cv);
                    $content["TP_PackCount"] = $content["TP_SumCount"] = $content["TP_UnitCount"] = $content["TS_Count"]*$content["TP_UnitCount"];
                    $content["TP_SumWeight"] = round($content["TP_UnitWeight"]*$content["TP_SumCount"],2);
                    $data[$cv["TH_IDFK"].'-'.$content["SCD_ID"].'-'.$content["DtMD_ID_PK"]] = $content;
                }
            }
        }
        $taskpartmodel = new TaskPart();
        $partList = $taskpartmodel->field("TH_IDFK,SCD_ID,DtMD_ID_PK,TP_ID")->where($where)->select();
        $part_arr = [];
        foreach($partList as $v){
            $part_arr[$v["TH_IDFK"].'-'.$v["SCD_ID"].'-'.$v["DtMD_ID_PK"]] = $v["TP_ID"];
            isset($data[$v["TH_IDFK"].'-'.$v["SCD_ID"].'-'.$v["DtMD_ID_PK"]])?$data[$v["TH_IDFK"].'-'.$v["SCD_ID"].'-'.$v["DtMD_ID_PK"]]["TP_ID"] = $v["TP_ID"]:"";
        }
        // $add_list = array_diff_key($data,$part_arr);
        // $edit_list = array_intersect_key($data,$part_arr);
        $del_list = array_values(array_diff_key($part_arr,$data));
        $result=false;
        Db::startTrans();
        try {
            
            $result = $taskheightModel->where($where)->update(["TP_Writer"=>$this->admin["nickname"],"TP_WriteDate"=>date("Y-m-d H:i:s")]);
            
            $resultUp = (new \app\admin\model\chain\sale\Sectconfigdetail())
                ->where('TH_ID','IN',function($query){
                    $query->table('taskheight')->field("TH_ID")->where($this->where);
                })->update(["SCD_FYFlag"=>1]);
            $resultDelCon = $taskpartmodel->where("TP_ID","IN",$del_list)->delete();
            $resultCon = $taskpartmodel->allowField(true)->saveAll(array_values($data));
            Db::commit();
            return json(["code"=>1,'msg'=>"成功"]);
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        }
        if($result !== false){
            return json(["code"=>1,'msg'=>"成功"]);
        }else{
            return json(["code"=>0,'msg'=>"失败"]);
        }
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $row = $this->model->where("DtM_iID_PK",$ids)->find();
            if($row["DtM_sAuditor"]) $this->error("已审核，无法删除");
            $dhcooperatek_one = (new DhCooperate([],$this->technology_ex))->alias("dk")->join([$this->technology_ex."dtmaterial"=>"f"],"f.DtM_iID_PK=dk.DtM_iID_PK")->where("f.DtM_iID_PK", 'in', $ids)->find();
            if($dhcooperatek_one) $this->error("已生成放样电焊件，无法删除");

            // $pack_one = $this->model->alias("dt")
            //     ->join(["pack"=>"p"],"p.TD_ID = p.TD_ID")
            //     ->where("dt.DtM_iID_PK","IN",$ids)
            //     ->find();
            // if($pack_one) $this->error("已存在打包，无法删除");

            $list = $this->model->where($pk, 'in', $ids)->select();

            $taskDetail = $this->sectModel->field("DtS_ID_PK")->where("DtM_iID_FK",$ids)->select();
            $task_detail_id = [];
            $count = 0;
            $taskHeightModel = new TaskHeight();
            $pcNfileModel = new PcNcfile();
            Db::startTrans();
            try {
                foreach($taskDetail as $v){
                    $task_detail_id[] = $v["DtS_ID_PK"];
                    $v->delete();
                }
                $taskId = $upList = $taskheightId = [];
                if(!empty($task_detail_id)) $this->detailModel->where("DtMD_iSectID_FK","IN",$task_detail_id)->delete();
                foreach ($list as $v) {
                    $count += $v->delete();
                    $taskId[$v["TD_ID"]] = $v["TD_ID"];
                }
                
                $pcNfileModel->where("TD_ID","in",$taskId)->delete();
                if(!empty($taskId)){
                    $towerTask = (new MergTypeName())->where("mTD_ID","IN",$taskId)->select();
                    foreach($towerTask as $k=>$v){
                        $taskId[$v["TD_ID"]] = $v["TD_ID"];
                        $v->delete();
                    }
                    $resultTask = $taskHeightModel->field("TH_ID")->where('TD_ID',"in",$taskId)->select();
                    
                    foreach($resultTask as $v){
                        $upList[] = ["TP_Writer"=>"","TP_WriteDate"=>"0000:00:00 00:00:00","TH_ID"=>$v["TH_ID"]];
                        $taskheightId[] = $v["TH_ID"];
                    }
                }
                if(!empty($delWhere)){
                    $delWhere = ["TD_ID"=>["IN",$taskId]];
                    (new TaskPart())->where()->delete($delWhere);
                }

                if(!empty($taskheightId)){
                    $taskHeightModel->saveAll($upList);
                    (new Sectconfigdetail())->where('TH_ID',"in",$taskheightId)->update(["SCD_FYFlag"=>0]);
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    // public function examine()
    // {
    //     $params = $this->request->post("data");
    //     $one = $this->model->get($params);
    //     if(!$one) return json(["code"=>0,"msg"=>"无该信息，请稍后重试"]);
    //     else if($one["DtM_sAuditor"]) return json(["code"=>0,"msg"=>"已审核本数据，无法进行段审核"]);
    //     $DtS_sAuditor = $this->admin["nickname"];
    //     $DtS_sAuditDate = date("Y-m-d H:i:s");
    //     $list = $this->sectModel->where("DtM_iID_FK","=",$params)->update(["DtS_sAuditor"=>$DtS_sAuditor,"DtS_sAuditDate"=>$DtS_sAuditDate]);
    //     if($list != false) return json(["code"=>1,"msg"=>"审核成功"]);
    //     else return json(["code"=>0,"msg"=>"没有数据更新"]);
    // }

    // public function giveUp()
    // {
    //     $params = $this->request->post("data");
    //     $one = $this->model->get($params);
    //     if(!$one) return json(["code"=>0,"msg"=>"无该信息，请稍后重试"]);
    //     else if($one["DtM_sAuditor"]) return json(["code"=>0,"msg"=>"已审核本数据，无法进行段弃审"]);
    //     $list = $this->sectModel->where("DtM_iID_FK","=",$params)->update(["DtS_sAuditor"=>'',"DtS_sAuditDate"=>"0000-00-00 00:00:00"]);
    //     if($list != false) return json(["code"=>1,"msg"=>"取消审核成功"]);
    //     else return json(["code"=>0,"msg"=>"没有数据更新"]);
    // }

    public function auditor()
    {
        $num = $this->request->post("num");
        $one = $this->model->where("DtM_iID_PK",$num)->find();
        if(!$one) return json(["code"=>0,"msg"=>"审核失败，该信息不存在"]);
        elseif($one["DtM_sAuditor"])  return json(["code"=>0,"msg"=>"已审核"]);

        //判断内容是否有误
        //数量有误
        $number_one = $this->model->alias("m")
            ->join([$this->technology_ex."dtsect"=>"s"],"m.DtM_iID_PK = s.DtM_iID_FK")
            ->join([$this->technology_ex."dtmaterialdetial"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("(case when cast(DtMD_iUnitCount as UNSIGNED)=DtMD_iUnitCount THEN 0 ELSE 1 END) as flag")
            ->where("DtM_iID_PK",$num)
            ->having("flag=1")
            ->find();
        if($number_one) return json(["code"=>0,"msg"=>"部件数量有误，请仔细核查"]);
        //是否有重复部件号
        $repeat_one = $this->model->alias("m")
            ->join([$this->technology_ex."dtsect"=>"s"],"m.DtM_iID_PK = s.DtM_iID_FK")
            ->join([$this->technology_ex."dtmaterialdetial"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("count(DtMD_sPartsID) as group_num")
            ->where("DtM_iID_PK",$num)
            ->group("DtMD_sPartsID")
            ->having("group_num>1")
            ->find();
        if($repeat_one) return json(["code"=>0,"msg"=>"部件名有误，请仔细核查"]);
        //判断是否还有未配置的段
        $td_id_list = (new MergTypeName())->where("mTD_ID",$one["TD_ID"])->column("TD_ID");
        if($td_id_list) $td_list = $td_id_list;
        else $td_list = [$one["TD_ID"]];
        //判断是否文件齐全
        // $detailPartCount = $this->sectModel->alias("dts")
        //     ->join(["dtmaterialdetial"=>"dtmd"],"dts.DtS_ID_PK=dtmd.DtMD_iSectID_FK")
        //     ->where("dts.DtM_iID_FK",$num)->where("dtmd.DtMD_sStuff","in",["角钢","钢板"])->value("count(dtmd.DtMD_ID_PK) as detailPartCount");
        // $filePartCount = (new PcNcfile())->where("TD_ID",$one["TD_ID"])->value("count(ID) as filePartCount");
        // if($detailPartCount!=$filePartCount) return json(["code"=>0,"msg"=>"NC文件未齐全，请仔细核查"]);
        //需配置段
        $need_count_list = (new TaskSect())->alias("ts")
            ->join(["sectconfigdetail"=>"scd"],"ts.SCD_ID = scd.SCD_ID")
            ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
            ->field("TS_Name,count(TS_Name) as need_count")
            ->WHERE("th.TD_ID","IN",$td_list)
            ->GROUP("ts.TS_Name")
            ->SELECT();
        $dsectModel = new Dtsect([],$this->technology_ex);
        $exist_count_list = $dsectModel->alias("ds")
            ->join([$this->technology_ex."dtmaterial"=>"dm"],"ds.DtM_iID_FK = dm.DtM_iID_PK")
            ->field("DtS_Name,count(DtS_Name) as need_count")
            ->WHERE("dm.TD_ID","=",$one["TD_ID"])
            ->GROUP("ds.DtS_Name")
            ->select();
        if(count($need_count_list) != count($exist_count_list)) return json(["code"=>0,"msg"=>"配段不匹配，请仔细核查"]);
        //同步生产任务下达
        $produceTaskModel = new ProduceTask();
        $produce_task_list = $produceTaskModel->where("TD_ID",$one["TD_ID"])->where("Auditor","<>","")->column("PT_Num");
        if($produce_task_list){
            $produceTaskSectModel = new \app\admin\model\chain\lofting\ProduceTaskSect([],$this->technology_ex);
            $pts_name_list = $produceTaskSectModel->field("PT_Num,PTS_Name")->where("PT_Num","IN",$produce_task_list)->group("PT_Num,PTS_Name")->select();
            $pts_list = [];
            foreach($pts_name_list as $v){
                $pts_list[$v["PT_Num"]][$v["PTS_Name"]] = $v["PTS_Name"];
            }
            if(!empty($pts_list)){
                $dtm_iid_pk_one = (new \app\admin\model\chain\lofting\Dtmaterial([],$this->technology_ex))->field("DtM_iID_PK")->where("TD_ID",$one["TD_ID"])->find();
                $where = ["ds.DtM_iID_FK"=>["=",$dtm_iid_pk_one["DtM_iID_PK"]]];
                $partNameList = [];
                array_map(function ($value) use (&$partNameList) {
                    $partNameList = array_merge($partNameList, array_values($value));
                }, $pts_list);
                $where["ds.DtS_Name"] = ["in",$partNameList];
                $list = (new \app\admin\model\chain\lofting\Dtsect([],$this->technology_ex))->alias("ds")
                ->join([$this->technology_ex."dtmaterialdetial"=>"dd"],"ds.DtS_ID_PK = dd.DtMD_iSectID_FK")
                ->field("CAST(ds.DtS_Name AS UNSIGNED) as number,ds.DtS_Name as PT_Sect,dd.DtMD_ID_PK,dd.DtMD_iUnitCount as PTD_Count,dd.DtMD_fUnitWeight as PTD_SWeight,dd.DtMD_sMaterial as PTD_Material,dd.DtMD_sSpecification as PTD_Specification,dd.DtMD_iWelding as PTD_Flag")
                ->order("number ASC")
                ->where($where)
                ->select();
                if($list) $list = collection($list)->toArray();
                else $list = [];
                $szList = (new \app\admin\model\chain\lofting\ProduceTaskSectShizu([],$this->technology_ex))->field("PT_Num,PT_Sect")->where("PT_Num","IN",$produce_task_list)->select();
                $group="PT_Num,PT_Num,PTS_Name";
                $useField = "sum(PTS_Count) as PTS_Count,PT_Num,PTS_Name as TS_Name";
                $usedList = $produceTaskSectModel->field($useField)
                    ->where("PT_Num","IN",$produce_task_list)
                    ->group($group)
                    ->select();
                $szArr = $sectList = [];
                foreach($usedList as $v){
                    $sectList[$v["PT_Num"]][$v["TS_Name"]] = $v["PTS_Count"];
                }
                foreach($szList as $v){
                    $szArr[$v["PT_Num"]][] = $v["PT_Sect"];
                }
                $addData = [];
                foreach($produce_task_list as $v){
                    $pt_news[$v] = [
                        "PT_Num" => $v,
                        "PT_sumCount"=>0,
                        "PT_sumWeight"=>0
                    ];
                    isset($sectList[$v])?"":$sectList[$v] = [];
                    isset($szArr[$v])?"":$szArr[$v] = [];
                    foreach($list as $vv){
                        if(in_array($vv["PT_Sect"],$pts_list[$v]) !== false){
                            unset($vv["number"]);
                            $vv["PT_Num"] = $v;
                            $vv["IF_Shi"] = in_array($vv["PT_Sect"],$szArr[$v]);
                            $vv["tpaNum"] = 1;
                            $vv["DD_Name"] = '公司';
                            $vv["PTD_Count"] = isset($sectList[$v][$vv["PT_Sect"]])?$vv["PTD_Count"]*$sectList[$v][$vv["PT_Sect"]]:$vv["PTD_Count"];
                            $vv["PTD_SWeight"] = $vv["PTD_SWeight"]??0;
                            $vv["PTD_sumWeight"] = isset($sectList[$v][$vv["PT_Sect"]])?$vv["PTD_Count"]*$vv["PTD_SWeight"]:$vv["PTD_SWeight"];
                            $vv["PTD_Flag"] = $vv["PTD_Flag"]??0;
                            $vv["PTD_Specification"] = $vv["PTD_Specification"]??"";
                            $addData[] = $vv;
                            $pt_news[$v]["PT_sumCount"] += $vv["PTD_Count"];
                            $pt_news[$v]["PT_sumWeight"] += $vv["PTD_sumWeight"];
                        }
                    }
                }
            }
        }
        $result = false;
        $producetaskResutl = false;
        Db::startTrans();
        try {
            $admin = $this->admin["nickname"];
            $result = $this->model->update(['DtM_iID_PK'=>$num,"DtM_sAuditor"=>$admin,"DtM_dTime"=>date("Y-m-d H:i:s")]);

            if($produce_task_list){
                $produceTaskModel->allowField(true)->saveAll($pt_news);
                $produceTaskDetailModel = new \app\admin\model\chain\lofting\ProduceTaskDetail([],$this->technology_ex);
                $produceTaskDetailModel->where("PT_Num","IN",$produce_task_list)->delete();
                $producetaskResutl = $produceTaskDetailModel->allowField(true)->saveAll($addData);
                foreach($produce_task_list as $v){
                    (new Http())->sendAsyncRequest("http://192.168.3.88/admin.php/chain/lofting/produce_task/savepdf/PT_Num/".$v);
                }
            }
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            if($producetaskResutl !== false) return json(["code"=>2,"msg"=>"成功！"]);
            else return json(["code"=>1,"msg"=>"成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"失败！请稍后重试！"]);
        }
    }

    public function giveUpA()
    {
        $num = $this->request->post("num");
        $one = $this->model->where("DtM_iID_PK",$num)->find();
        if(!$one) return json(["code"=>0,"msg"=>"弃核失败，该信息不存在"]);
        elseif($one["DtM_sAuditor"]=="")  return json(["code"=>0,"msg"=>"未审核"]);
        $result = $this->model->update(['DtM_iID_PK'=>$num,"DtM_sAuditor"=>'',"DtM_dTime"=>"0000-00-00 00:00:00"]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"失败！请稍后重试！"]);
        }
    }

    /**
     * 段和明细导入
     */
    public function importView($ids=null)
    {
        if ($this->request->isPost()) {
            $one = $this->model->where("DtM_iID_PK",$ids)->find();
            if(!$one) $this->error("导入失败，请稍后重试");
            elseif($one["DtM_sAuditor"]) $this->error("已审核，导入失败");
            $file = $this->request->post("filename");
            $way = $this->request->post("way");
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $data = [];
            $file = ROOT_PATH . DS . 'public' . DS . $file;
            try {
                $data = Excel::import($file, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($data)){
                    $this->error('空数据文件');
                }
                $data = array_slice($data,1);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }

            $td_id = 0;
            $td_one = $this->model->field("TD_ID")->where("DtM_iID_PK",$ids)->find();
            if($td_one) $td_id = $td_one["TD_ID"];
            else return json(["code"=>0,'msg'=>'导入失败！']);
            // $resultUp = (new Sectconfigdetail())->alias("sect")
            //     ->join(["taskheight"=>"th"],"sect.TH_ID = th.TH_ID")
            //     ->where("sect.SCD_FYFlag",1)
            //     ->where("th.TD_ID",$td_id)
            //     ->find();
            // if($resultUp) return json(["code"=>0,'msg'=>'导入失败，工程部件明细已调拨']);

            $count = 0;
            [$sectList,$detailList] = $this->getImportViewData($data,$ids);
            $where = [
                "DtM_iID_FK" => ["=",$ids],
                "DtS_Name" => ["IN",array_keys($sectList)]
            ];
            $isE = $this->sectModel->where($where)->select();
            $result = false;
            // $cannotEdit = [];
            Db::startTrans();
            try {
                $sect_id = [];
                foreach($isE as $v){
                    $sect_id[] = $v["DtS_ID_PK"];
                    if($way == "cover") $v->delete();
                    else{
                        $sectList[$v["DtS_Name"]]["DtS_ID_PK"] = $v["DtS_ID_PK"];
                    }
                }
                if(!empty($sect_id) and $way=="cover"){
                    $dhcooperatek_model = new DhCooperate([],$this->technology_ex);
                    $dhcooperatek_detail_model = new DhCooperateDetail([],$this->technology_ex);
                    $dhcooperatek_single_model = new DhCooperateSingle([],$this->technology_ex);
                    $dh_num_list = $dcd_id_list = [];
                    $dts_name = array_keys($sectList);
                    $dh_list = $dhcooperatek_model->field("DC_Num")->where("DtM_iID_PK",$ids)->select();
                    foreach($dh_list as $k=>$v){
                        $dh_num_list[] = $v["DC_Num"];
                    }
                    if(!empty($dh_num_list)){
                        $dh_detail_list = $dhcooperatek_detail_model->field("DCD_ID")->where([
                            "DC_Num" => ["IN",$dh_num_list],
                            "DCD_PartName" => ["IN",$dts_name]
                        ])->select();
                        foreach($dh_detail_list as $k=>$v){
                            $dcd_id_list[] = $v["DCD_ID"];
                            $v->delete();
                        }
                        if(!empty($dcd_id_list)) $dhcooperatek_single_model->where([
                            "DCD_ID" => ["IN",$dcd_id_list]
                        ])->delete();
                    }
                    $partIdList = $this->detailModel->where("DtMD_iSectID_FK","in",array_values($sect_id))->column("DtMD_sPartsID");
                    $this->detailModel->where("DtMD_iSectID_FK","in",$sect_id)->delete();
                    (new PcNcfile())->where(["TD_ID"=>["=",$td_id],"PartName"=>["IN",$partIdList]])->delete();
                }
                // foreach($sectList as $k=>$v){
                //     if(in_array($k,$cannotEdit)) unset($sectList[$k]);
                // }
                $result = $this->sectModel->allowField(true)->saveAll($sectList);
                $contect = $saveDetailList = [];
                foreach($result as $v){
                    $contect[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                }
                foreach($detailList as $k=>$v){
                    $count += count($v);
                    foreach($v as $vv){
                        $saveDetailList[] = array_merge($vv,["DtMD_iSectID_FK"=>$contect[$k]]);
                    }
                }
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveDetailList);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success("成功导入".$count."条记录！");
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        return $this->view->fetch();
        
    }

    public function batchSetting($ids=null){
        if ($this->request->isPost()) {
            $standard = $this->request->post("standard");
            $sectList = $this->sectModel->field("DtS_ID_PK")->where("DtM_iID_FK",$ids)->select();
            $sectArr = [];
            foreach($sectList as $v){
                $sectArr[] = $v["DtS_ID_PK"];
            }
            $result = false;
            Db::startTrans();
            try {
                $result = $this->detailModel->where('DtMD_iSectID_FK', "in", $sectArr)->update(['DtMD_sType' => $standard]);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        return $this->view->fetch();
    }

    //BC级的修改
    public function editDetailAll()
    {
        $params = $this->request->post();
        list($sect_id,$type) = array_values($params);
        if(!$sect_id and !$type) return json(["code"=>0,"msg"=>"有误请重试"]);
        $one = $this->model->alias("a")->join([$this->technology_ex."dtsect"=>"b"],"a.DtM_iID_PK = b.DtM_iID_FK")
            ->field("DtM_sAuditor,DtS_sAuditor")
            ->where("b.DtS_ID_PK",$sect_id)->find();
        if($one["DtM_sAuditor"]) return json(["code"=>0,"msg"=>"已审核，不能修改！"]);
        $where = ["DtMD_iSectID_FK" => ["=",$sect_id]];
        if($type == "editB") $where["DtMD_sMaterial"]=["NOT LIKE","%B"];
        else if($type == "editC") $where["DtMD_sMaterial"]=["NOT LIKE","%C"];
        else if($type == "giveupB") $where["DtMD_sMaterial"]=["LIKE","%B"];
        else $where["DtMD_sMaterial"]=["LIKE","%C"];
        $list = $this->detailModel->field("DtMD_sMaterial,DtMD_ID_PK")->where($where)->select();
        if(!$list) return json(["code"=>0,"msg"=>"没有可以修改的部件号"]);
        $saveList = [];
        foreach($list as $k=>$v){
            if($type == "editB") $field = $v["DtMD_sMaterial"]."B";
            else if($type == "editC") $field = $v["DtMD_sMaterial"]."C";
            else $field = substr($v["DtMD_sMaterial"],0,strlen($v["DtMD_sMaterial"])-1);
            $saveList[] = ["DtMD_ID_PK"=>$v["DtMD_ID_PK"],"DtMD_sMaterial"=>$field];
        }
        $count = false;
        Db::startTrans();
        try {
            $count = $this->detailModel->saveAll($saveList);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        }
        if($count){
            return json(["code"=>1,'msg'=>"成功"]);
        }else{
            return json(["code"=>0,"msg"=>"没有可以修改的部件号"]);
        }
    }

    public function partImport($ids=null)
    {
        $one = $this->model->where("DtM_iID_PK",$ids)->find();
        $td_id = $one["TD_ID"];
        $list = $this->sectModel->alias("s")
            ->join([$this->technology_ex."dtmaterialdetial"=>"dd"],"s.DtS_ID_PK = dd.DtMD_iSectID_FK")
            ->where("DtM_iID_FK",$ids)
            ->where("DtMD_sStuff","IN",["角钢","钢板"])
            ->column("DtMD_sPartsID,(case DtMD_sStuff when '角钢' then 'dat' else 'dxf' end) as file_type");
        $PcNcFile = new PcNcfile();
        $nc_where = ["TD_ID"=>["=",$td_id]];
        $filesList = $PcNcFile->getFileUrl($nc_where);
        if ($this->request->isPost()) {
            $filename = $this->request->post("filename");
            if(!$filename) $this->error("不存在文件名，请稍后重试");
            $zip = new ZipArchive;
            $ncData = [];
            // $zip->open(ROOT_PATH . 'public' .$filename,\ZipArchive::CREATE);
            // 是否存在压缩包
            if ($zip->open(ROOT_PATH . 'public' .$filename, \ZipArchive::CREATE) === true){
                //压缩包解压到指定位置
                $zip->extractTo(ROOT_PATH . 'public' . DS .'ncfiles');
                $name = $zip->getNameIndex(0);
                $zip->close();
                // rmdir(ROOT_PATH . 'public' .$filename);
                $name = trim($name,'/');
                $oldfieldname = ROOT_PATH . 'public' . DS .'ncfiles' .DS.$name;
                $newfieldname = ROOT_PATH . 'public' . DS .'ncfiles' .DS.$td_id;
                if(!is_dir($newfieldname)){
                    mkdir($newfieldname,0777);
                }
                clearstatcache($newfieldname);
                $fileList = scandir($oldfieldname);
                $fileList = array_slice($fileList,2);
                foreach($fileList as $v){
                    if(!isset(pathinfo($v)["filename"]) or !pathinfo($v)["filename"]) continue;
                    if(!isset(pathinfo($v)["extension"]) or !pathinfo($v)["extension"]) continue;
                    $partName = pathinfo($v)["filename"];
                    $extension = pathinfo($v)["extension"];
                    if(file_exists($newfieldname.DS.$v)) unlink($newfieldname.DS.$v);
                    if(isset($list[$partName]) and $extension == $list[$partName]){
                        //文件类型判断相同
                        if($extension == $list[$partName])
                            $ncData["".$partName] = [
                                "TD_ID" => $td_id,
                                "PartName" => $partName,
                                "PartNCPath" => $td_id.'-'.$partName,
                                "datStream" => DS.'ncfiles' .DS.$td_id.DS.$v
                            ];
                            isset($filesList[$partName])?$ncData[$partName]["ID"]=$filesList[$partName]["ID"]:"";
                    }
                    if(isset($list["0".$partName])){
                        //文件类型判断相同
                        if($extension == $list["0".$partName])
                            $ncData["0".$partName] = [
                                "TD_ID" => $td_id,
                                "PartName" => "0".$partName,
                                "PartNCPath" => $td_id.'-0'.$partName,
                                "datStream" => DS.'ncfiles' .DS.$td_id.DS.$v
                            ];
                            isset($filesList["0".$partName])?$ncData["0".$partName]["ID"]=$filesList["0".$partName]["ID"]:"";
                    }
                    $fl_array = preg_grep('/^(\d+)\-+[0]?'.$partName.'$/',array_keys($list));
                    if(!empty($fl_array)){
                        foreach($fl_array as $fv){
                            $duan = substr($fv,0,stripos($fv,'-'));
                            $true_url = DS.'ncfiles'.DS.$td_id.DS.$fv.DS.$list[$fv];
                            if($duan<5) continue;
                            if($extension != $list[$fv]) continue;
                            if(isset($ncData[$fv]) and $ncData[$fv]==$true_url) continue;
                            $ncData["".$fv] = [
                                "TD_ID" => $td_id,
                                "PartName" => $fv,
                                "PartNCPath" => $td_id.'-'.$fv,
                                "datStream" => DS.'ncfiles' .DS.$td_id.DS.$v
                            ];
                            isset($filesList[$fv])?$ncData[$fv]["ID"]=$filesList[$fv]["ID"]:"";
                        }
                    }
                    rename($oldfieldname.DS.$v,$newfieldname.DS.$v);
                }
                rmdir($oldfieldname);
            }
            // $end_file = array_merge($filesList,$ncData);
            // $end_file = 
            // pri(($filesList+$ncData),1);
            $result = $PcNcFile->allowField(true)->saveAll($ncData);
            if ($result) {
                $this->success();
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        return $this->view->fetch();
    }

    public function test()
    {
        $arr_1 = [
            "1106" => [
                "PartName" => 1106,
                "ID" => 24,
                "datStream" => "\\ncfiles\\10151\\1106.dat"
            ]
        ];

        $arr_2 = [
            "1106" => [
                "TD_ID" => 10151,
                "PartName" => 1106,
                "PartNCPath" => "10151-1106",
                "datStream" => "\\ncfiles\\10151\\1107.dat"
            ]
        ];

        $arr = $arr_1+$arr_2;
        pri($arr,1);
    }

    public function detailImport($ids=null)
    {
        $row = $this->model->alias("d")
            ->join([$this->technology_ex."dtsect"=>"ds"],"d.DtM_iID_PK=ds.DtM_iID_FK")
            ->join([$this->technology_ex."dtmaterialdetial"=>"dmd"],"ds.DtS_ID_PK=dmd.DtMD_iSectID_FK")
            ->where("DtMD_ID_PK",$ids)
            ->find();
        if(!$row) $this->error("请刷新后重试！");
        $base_extension = $row["DtMD_sStuff"]=="角钢"?".dat":".dxf";
        $td_id = $row["TD_ID"];
        $DtMD_sPartsID = $row["DtMD_sPartsID"];
        $PcNcFile = new PcNcfile();
        $filesOne = $PcNcFile->where("PartNCPath",$td_id.'-'.$DtMD_sPartsID)->find();
        if($filesOne) $row["ncfile"] = $filesOne["datStream"];
        else $row["ncfile"] = "";
        if ($this->request->isPost()) {
            $postRow = $this->request->post();
            $filename = $postRow["row"]["file"];
            $extension = pathinfo($filename)["extension"];
            if($base_extension!=".".$extension) $this->error("文件类型有误，请检查后重新上传");
            $result = false;
            if(!$filename){
                if($row["ncfile"]) $result = $PcNcFile->where("id",$filesOne["ID"])->delete();
            }else{
                $oldfieldname = ROOT_PATH . 'public' . $filename;
                $wz = substr($oldfieldname,strripos($oldfieldname,'.'));
                $newfieldname = ROOT_PATH . 'public' . DS .'ncfiles' .DS.$td_id;
                if(!is_dir($newfieldname)){
                    mkdir($newfieldname,0777);
                }
                $newfieldname .= DS.$DtMD_sPartsID.$wz;
                if(file_exists($newfieldname)) unlink($newfieldname);
                $result = rename($oldfieldname,$newfieldname);
                $ncData[$DtMD_sPartsID] = [
                    "TD_ID" => $td_id,
                    "PartName" => $DtMD_sPartsID,
                    "PartNCPath" => $td_id.'-'.$DtMD_sPartsID,
                    "datStream" => DS.'ncfiles' .DS.$td_id.DS.$DtMD_sPartsID.$wz
                ];
                if($filesOne) $ncData[$DtMD_sPartsID]["ID"] = $filesOne["ID"];
                $result = $PcNcFile->allowField(true)->saveAll($ncData);
            }
            if ($result) {
                $this->success();
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        $this->view->assign("row",$row);
        return $this->view->fetch();
    }

    public function export($ids=null){
        if(!$ids) return json(["code"=>0,"msg"=>"请稍后重试！"]);
        $one = $this->model->where("DtM_iID_PK",$ids)->find();
        $title = $one["DtM_sProject"].'-'.$one["DtM_sTypeName"];
        $list = $this->sectModel->alias("s")
            ->join([$this->technology_ex."dtmaterialdetial"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("d.*,CAST(s.DtS_ID_PK AS UNSIGNED) AS number_1,CAST(d.DtMD_sPartsID AS UNSIGNED) AS number_2,CAST(s.DtS_ID_PK AS UNSIGNED) AS number_1,CAST(d.DtMD_sPartsID AS UNSIGNED) AS number_2,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn,s.DtS_Name,round(DtMD_iUnitCount*DtMD_fUnitWeight,2) as sum_weight")
            ->where("s.DtM_iID_FK",$ids)
            ->order("number_1,bjbhn,number_2")
            ->select();
        [$list,$header] = $this->getExportData($list);

        return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
    }

    public function getTableField()
    {
        $list = [
            ["DtS_ID_PK","DtS_ID_PK","text","","readonly",0,"hidden"],
            ["*段名","DtS_Name","text","","data-rule='required'","",""],
            ["明细件号数","detailNum","text","","readonly",0,""],
            ["总重量(kg)","detailWeight","text","","readonly",0,""],
            ["是否专用","ifBody","text","","readonly",'0',""],
            ["制表人","DtS_sWriter","text","","readonly","",""],
            ["制表时间","DtS_dWriterTime","text","","readonly","",""],
            // ["审核人","DtS_sAuditor","text","","readonly","",""],
            // ["审核时间","DtS_sAuditDate","text","","readonly","",""],
            ["备注","DtS_sRemark","text","","","",""],
            // ["公用","DtS_ifPublic","checkbox","","","",""],
            // ["难度系数","DtS_Coefficient","text","","","",""],
            // ["套用","DtS_Type","checkbox","","","",""],
            // ["图号","DtS_sPictureNum","text","","","",""]
        ];
        return $list;
    }

    public function getDetailField(){
        $tableField = [
            ["URL","datStream","text","disabled","disabled",'110',""],
            ["DtMD_ID_PK","DtMD_ID_PK","text","","hidden","40",""],
            ["部件号","DtMD_sPartsID","text","data-rule='required'","","50",""],
            ["材质","DtMD_sMaterial","text","","","60",""],
            ["材料","DtMD_sStuff","text","","","40",""],
            ["规格","DtMD_sSpecification","text","","","70",""],
            ["长度(mm)","DtMD_iLength","text","","","50","checked"],
            ["宽度(mm)","DtMD_fWidth","text","","","50","checked"],
            ["厚度(mm)","DtMD_iTorch","text","","","50","checked"],
            ["单段数量(件)","DtMD_iUnitCount","text","data-rule='integer'","","40","checked"],
            ["单件重量(kg)","DtMD_fUnitWeight","text","","","60","checked"],
            ["总重量(kg)","sumWeight","text","","","60","checked"],
            ["单件孔数(个)","DtMD_iUnitHoleCount","text","data-rule='integer'","","50","checked"],
            ["类型","type","text","","","50","checked"],
            ["电焊(0/1/2/3)","DtMD_iWelding","text","data-rule='between'","","40","checked"],
            ["制弯(0/1/2)","DtMD_iFireBending","text","data-rule='between'","","40","checked"],
            ["切角切肢(0/1)","DtMD_iCuttingAngle","text","data-rule='between'","","40","checked"],
            ["铲背(0/1)","DtMD_fBackOff","text","","","40","checked"],
            ["清根(0/1)","DtMD_iBackGouging","text","data-rule='between'","","40","checked"],
            ["打扁(0/1)","DtMD_DaBian","text","data-rule='between'","","40","checked"],
            ["开合角(0/1/2)","DtMD_KaiHeJiao","text","data-rule='three'","","40","checked"],
            ["割豁(0/1)","DtMD_GeHuo","text","data-rule='between'","","40","checked"],
            ["钻孔(0/1)","DtMD_ZuanKong","text","data-rule='between'","","40","checked"],
            ["备注","DtMD_sRemark","text","","","80","checked"],
            // ["钻孔数(个)","DtMD_iBoreholeCount","text","integer","hidden","40","checked"],
            // ["开槽形式","DtMD_iExcavationCount","text","","","40","checked"],
            // ["打坡口(0/1)","DtMD_iAssembly","text","between","","40","checked"],
            // ["是否专用","DtMD_ISZhuanYong","text","between","","40","checked"],
            // ["类型","DtMD_sType","text","","","60","checked"],
            // ["边数","DtMD_iPerimeter","text","","","60","checked"],
            // ["周长","DtMD_Circle","text","","","60","checked"],
            // ["孔径28~38mm(A)","DtMD_KJ28T32","text","integer","","60","checked"],
            // ["孔径39*50mm(B)","DtMD_KJ33T42","text","integer","","60","checked"],
            // ["孔径50mm以上(C)","DtMD_KJ50UP","text","integer","","60","checked"],
            // ["安装孔直径","DtMD_iAperture","text","","","60","checked"],
            // ["分布圆直径","DtMD_iBending","text","","","60","checked"],
            // ["车工","DtMD_iAftertreatment","text","","","60","checked"],
            // ["压制","DtMD_iPressed","text","","","60","checked"]
        ];
        return $tableField;
    }

    public function orderByDjhprint($ids=null)
    {
        $model = $this->model->where("DtM_iID_PK",$ids)->find();
        if (!$model) {
            $this->error(__('No Results were found'));
        }
        $sectList = $this->sectModel->field("DtS_ID_PK, DtS_sWriter, DtS_sAuditor, DtS_Name,CAST(DtS_Name AS SIGNED) as number")->where("DtM_iID_FK",$ids)->order("number")->select();
        $duanArr = [];
        foreach($sectList as $sect){
            $mainInfos = [];
            $mainInfos['DtS_Name'] = $sect["DtS_Name"];
            $mainInfos['DtM_sTypeName'] = $model['DtM_sTypeName'];
            $mainInfos['DtS_sWriter'] = $sect['DtS_sWriter'];
            $mainInfos['DtS_sAuditor'] = $sect['DtS_sAuditor'];
            $mainInfos['idate']=date('Y-m-d');
            $field = $detailList = [];
            $where = ["DtMD_iSectID_FK"=>["=",$sect['DtS_ID_PK']]];
            $tableField = $this->getDetailField();
            foreach($tableField as $vv){
                if($vv[1]=="sumWeight" or $vv[1]=="datStream") continue;
                $field[] = $vv[1];
            }
            $detailList = $this->detailModel
                ->field(implode(",",$field).",DtMD_iSectID_FK,
                CAST(DtMD_sPartsID AS UNSIGNED) AS number_2,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
                SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
                else 0 end bjbhn")
                ->where($where)
                ->order("bjbhn ASC")
                ->select();
            $number = $weight = $knumber = 0;
            $detailArr = $detailName = [];
            $isJudge = ["DtMD_iWelding","DtMD_iFireBending","DtMD_iCuttingAngle","DtMD_fBackOff","DtMD_iBackGouging","DtMD_DaBian","DtMD_KaiHeJiao","DtMD_ZuanKong","DtMD_iExcavationCount","DtMD_iAssembly","DtMD_ISZhuanYong"];
            foreach($detailList as $k=>$v){
                $detailArr[$k] = $v->toArray();
                foreach($isJudge as $vv){
                    isset($v[$vv])?($detailArr[$k][$vv] = $v[$vv]?$v[$vv]:""):"";
                }
                $detailArr[$k]["is_int"] = 0;
                if(ctype_digit(''.$detailArr[$k]["DtMD_iUnitCount"]) and in_array($v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"], $detailName) == FALSE) $detailArr[$k]["is_int"] = 1;
                $detailArr[$k]["DtMD_fUnitWeight"] = round($detailArr[$k]["DtMD_fUnitWeight"],2);
                $number += $detailArr[$k]["DtMD_iUnitCount"];
                $weight += $detailArr[$k]["DtMD_fUnitWeight"];
                $knumber += $detailArr[$k]["DtMD_iUnitHoleCount"];
                $detailName[] = $v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"];
            }
            $mainInfos['number'] = $number;
            $mainInfos['weight'] = (string)$weight;
            $mainInfos['knumber'] = $knumber;

            $duanData = [];
            $duanData['mainInfos'] = $mainInfos;
            $duanData['detailArr'] = $detailArr;
            array_push($duanArr, $duanData);
        }
        $count = count($detailArr);

        $this->assignconfig("allCount",["count"=>$count,"number"=>$number,"weight"=>$weight,"knumber"=>$knumber]);
        $this->assignconfig('duanArr',$duanArr);

        return $this->view->fetch();
    }

    public function orderByGgxhprint($ids=null)
    {
        $model = $this->model->where("DtM_iID_PK",$ids)->find();
        if (!$model) {
            $this->error(__('No Results were found'));
        }
        $sect_ids = [];
        $sectList = $this->sectModel->field("DtS_ID_PK, DtS_sWriter, DtS_sAuditor, DtS_Name,CAST(DtS_Name AS SIGNED) as number")->where("DtM_iID_FK",$ids)->order("number")->select();
        $sectArr = $field = $detailList = [];
        foreach($sectList as $v){
            $sectArr[$v["DtS_ID_PK"]] = $v["DtS_Name"];
            array_push($sect_ids, $v['DtS_ID_PK']);
        }
        $where = ["DtMD_iSectID_FK"=>["in",$sect_ids]];
        $tableField = $this->getDetailField();
        foreach($tableField as $v){
            if($v[1]=="sumWeight" or $v[1]=="datStream") continue;
            $field[] = $v[1];
        }
        $detailList = $this->detailModel
            ->field(implode(",",$field).",DtMD_iSectID_FK,CAST(DtMD_sPartsID AS SIGNED) as number,CASE WHEN locate('-',DtMD_sPartsID) THEN REPLACE(DtMD_sPartsID,'-','')*1000 ELSE DtMD_sPartsID END as number_1,CAST(DtMD_sPartsID AS UNSIGNED) AS number_2,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn")
            ->where($where)
            ->order("DtMD_sStuff,DtMD_sSpecification,DtMD_sMaterial,bjbhn ASC")
            ->select();
        $number = $weight = $knumber = 0;
        $detailArr = $detailName = [];
        $isJudge = ["DtMD_iWelding","DtMD_iFireBending","DtMD_iCuttingAngle","DtMD_fBackOff","DtMD_iBackGouging","DtMD_DaBian","DtMD_KaiHeJiao","DtMD_ZuanKong","DtMD_iExcavationCount","DtMD_iAssembly","DtMD_ISZhuanYong"];
        foreach($detailList as $k=>$v){
            $detailArr[$k] = $v->toArray();
            foreach($isJudge as $vv){
                isset($v[$vv])?($detailArr[$k][$vv] = $v[$vv]?$v[$vv]:""):"";
            }
            $detailArr[$k]["is_int"] = 0;
            if(ctype_digit(''.$detailArr[$k]["DtMD_iUnitCount"]) and in_array($v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"], $detailName) == FALSE) $detailArr[$k]["is_int"] = 1;
            $detailArr[$k]["DtMD_fUnitWeight"] = round($detailArr[$k]["DtMD_fUnitWeight"],2);
            $number += $detailArr[$k]["DtMD_iUnitCount"];
            $weight += $detailArr[$k]["DtMD_fUnitWeight"];
            $knumber += $detailArr[$k]["DtMD_iUnitHoleCount"];
            $detailName[] = $v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"];
        }
        $count = count($detailArr);

        $mainInfos = [];
        $mainInfos['DtM_sTypeName'] = $model['DtM_sTypeName'];
        $mainInfos['DtS_sWriter'] = $sectList[0]['DtS_sWriter'];
        $mainInfos['DtS_sAuditor'] = $sectList[0]['DtS_sAuditor'];
        $mainInfos['idate']=date('Y-m-d');
        $mainInfos['number'] = $number;
        $mainInfos['weight'] = (string)$weight;
        $mainInfos['knumber'] = $knumber;
        $this->assignconfig("allCount",["count"=>$count,"number"=>$number,"weight"=>$weight,"knumber"=>$knumber]);
        $this->assignconfig("detailArr",$detailArr);
        $this->assignconfig('mainInfos',$mainInfos);

        return $this->view->fetch();
    }

    public function gjmxPrint($ids=null)
    {
        $model = $this->model->where("DtM_iID_PK",$ids)->find();
        if (!$model) {
            $this->error(__('No Results were found'));
        }
        $sectList = $this->sectModel->field("DtS_ID_PK, DtS_sWriter, DtS_sAuditor, DtS_Name,CAST(DtS_Name AS SIGNED) as number")->where("DtM_iID_FK",$ids)->order("number")->select();
        $duanArr = [];
        foreach($sectList as $sect){
            $mainInfos = [];
            $mainInfos['DtS_Name'] = $sect["DtS_Name"];
            $mainInfos['DtM_sTypeName'] = $model['DtM_sTypeName'];
            $mainInfos['DtS_sWriter'] = $sect['DtS_sWriter'];
            $mainInfos['DtS_sAuditor'] = $sect['DtS_sAuditor'];
            $mainInfos['idate']=date('Y-m-d');
            $field = $detailList = [];
            $where = ["DtMD_iSectID_FK"=>["=",$sect['DtS_ID_PK']]];
            $tableField = $this->getDetailField();
            foreach($tableField as $vv){
                if($vv[1]=="sumWeight" or $vv[1]=="datStream") continue;
                $field[] = $vv[1];
            }
            $detailList = $this->detailModel
                ->field(implode(",",$field).",DtMD_iSectID_FK,CAST(DtMD_sPartsID AS SIGNED) as number,CASE WHEN locate('-',DtMD_sPartsID) THEN REPLACE(DtMD_sPartsID,'-','')*1000 ELSE DtMD_sPartsID END as number_1,
                CAST(DtMD_sPartsID AS UNSIGNED) AS number_2,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
                SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
                when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
                when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
                else 0 end bjbhn")
                ->where($where)
                ->order("bjbhn,number_1,number,DtMD_sPartsID ASC")
                ->select();
            $number = $weight = $knumber = 0;
            $detailArr = $detailName = [];
            $isJudge = ["DtMD_iWelding","DtMD_iFireBending","DtMD_iCuttingAngle","DtMD_fBackOff","DtMD_iBackGouging","DtMD_DaBian","DtMD_KaiHeJiao","DtMD_ZuanKong","DtMD_iExcavationCount","DtMD_iAssembly","DtMD_ISZhuanYong"];
            foreach($detailList as $k=>$v){
                $detailArr[$k] = $v->toArray();
                foreach($isJudge as $vv){
                    isset($v[$vv])?($detailArr[$k][$vv] = $v[$vv]?$v[$vv]:""):"";
                }
                $detailArr[$k]["is_int"] = 0;
                if(ctype_digit(''.$detailArr[$k]["DtMD_iUnitCount"]) and in_array($v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"], $detailName) == FALSE) $detailArr[$k]["is_int"] = 1;
                $detailArr[$k]["DtMD_fUnitWeight"] = round($detailArr[$k]["DtMD_fUnitWeight"],2);
                $number += $detailArr[$k]["DtMD_iUnitCount"];
                $weight += $detailArr[$k]["DtMD_fUnitWeight"];
                $knumber += $detailArr[$k]["DtMD_iUnitHoleCount"];
                $detailName[] = $v["DtMD_iSectID_FK"]."-".$v["DtMD_sPartsID"];
            }
            $mainInfos['number'] = $number;
            $mainInfos['weight'] = (string)$weight;
            $mainInfos['knumber'] = $knumber;

            $duanData = [];
            $duanData['mainInfos'] = $mainInfos;
            $duanData['detailArr'] = $detailArr;
            array_push($duanArr, $duanData);
        }
        
        $count = count($detailArr);

        $this->assignconfig("allCount",["count"=>$count,"number"=>$number,"weight"=>$weight,"knumber"=>$knumber]);
        $this->assignconfig('duanArr',$duanArr);

        return $this->view->fetch();
    }

    public function bomBzView($ids = '')
    {
        $row = $this->model->alias("d")
            ->join(["dtsect"=>"ds"],"d.DtM_iID_PK = ds.DtM_iID_FK")
            ->where("ds.DtS_ID_PK",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("d")
                ->join(["dtsect"=>"ds"],"d.DtM_iID_PK = ds.DtM_iID_FK")
                ->join(["dtmaterialdetial"=>"dd"],"ds.DtS_ID_PK = dd.DtMD_iSectID_FK")
                ->field("ds.DtS_Name,dd.DtMD_ID_PK,dd.DtMD_sPartsID,dd.DtMD_sStuff,dd.DtMD_sMaterial,dd.DtMD_sSpecification,dd.create_time")
                ->where($where)
                ->where("d.DtM_iID_PK",$row["DtM_iID_PK"])
                ->where("set_bz",1)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function bomBzAdd()
    {
        $idList = $this->request->post("id_list");
        $idArr = explode(",",$idList);
        if(!empty($idArr)){
            $result = $this->detailModel->where("DtMD_ID_PK","IN",$idArr)->update(["set_bz"=>1]);
        }
        return json(["code"=>1,"msg"=>"成功","data"=>[]]);
    }

    public function bomBzDel($ids = null)
    {
        $result = $this->detailModel->where("DtMD_ID_PK",$ids)->delete();
        if($result) $this->success();
        else $this->error("删除失败");
    }

    // public function toEmpty()
    // {
    //     [$type,$ids] = array_values($this->request->post());
    //     if($type){
    //         $list = $this->model->alias("dt")
    //             ->join([$this->technology_ex."dtsect"=>"dts"],"dt.DtM_iID_PK=dts.DtM_iID_FK")
    //             ->join([$this->technology_ex."dtmaterialdetial"=>"dtmd"],"dts.DtS_ID_PK=dtmd.DtMD_iSectID_FK")
    //             ->join(["pc_ncfile"=>"nc"],["dt.TD_ID=nc.TD_ID","dtmd.DtMD_sPartsID="])
    //             ->where()
    //     }
    // }
}
