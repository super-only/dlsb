<?php

namespace app\admin\controller\chain\lofting;

use app\common\controller\Backend;
use think\Db;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class PieceArrange extends Backend
{
    
    /**
     * PieceMain模型对象
     * @var \app\admin\model\chain\lofting\PieceArrange
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\lofting\PieceMain;
        $this->detailModel = new \app\admin\model\chain\lofting\PieceDetail;
        $this->contentModel = new \app\admin\model\chain\lofting\PieceContent;
        $this->viewModel = new \app\admin\model\chain\lofting\PieceArrangeView;
        $this->ship_list = $ship_list = [1=>["normal","普通下料"],11=>["group","制弯打扁开合角"],2=>["welding","电焊"],4=>["cut","切角"],5=>["backhoe","铲背"]];
        $this->assignconfig("ship_list",$ship_list);
        $this->view->assign("ship_list",$ship_list);
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            // if(isset($filter["pt_num"])) $where["c.PC_Num"] = ["LIKE","%".$filter["PC_Num"].'%'];
            // if(!isset($filter["record_time"])) $this->error("请选择时间段！");
            $time_where = [];
            if(isset($filter["record_time"])){
                $record_time = $filter["record_time"];
                $record_time = str_replace(' - ', ',', $record_time);
                $arr = array_slice(explode(',', $record_time), 0, 2);
                $sym = 'BETWEEN';
                
                //当出现一边为空时改变操作符
                if ($arr[0] === '') {
                    $sym = '<=';
                    $arr = $arr[1];
                } elseif ($arr[1] === '') {
                    $sym = '>=';
                    $arr = $arr[0];
                }
                $time_where["record_time"] = [ str_replace('RANGE', 'BETWEEN', $sym) . ' TIME', $arr];
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $pt_num = $this->model->alias("m")
                ->join(["piece_detail"=>"d"],"m.id=d.piece_id")
                ->join(["producetask"=>"pt"],"d.pt_num=pt.PT_Num")
                ->join(["taskdetail"=>"td"],"pt.TD_ID = td.TD_ID")
                ->field("d.pt_num")
                ->where($where)
                ->group("d.pt_num")
                ->order("d.pt_num desc")
                ->paginate($limit);
            // $pt_num = $this->detailModel->alias("a")
            //     ->join(["producetask"=>"pt"],"pt.PT_Num=a.pt_num")

            // list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            // $pt_num = $this->viewModel
                // ->where($where)
                // ->order($sort, $order)
                // ->group("pt_num")
                // ->limit($limit)
                // ->paginate($limit);
            $ptNumList = [];
            foreach($pt_num as $v){
                $ptNumList[$v["pt_num"]] = $v["pt_num"];
            }
        
            $sql_one = $this->contentModel
                ->field("pt_num,sum(case when workmanship=1 then (case when stuff='角钢' then ( case when (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+ SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1)<50000 then sum_weight/2 else sum_weight end) when stuff='钢板' then ( case when specification='-10' or specification='-12' or specification='-14' then sum_weight/2 else sum_weight end) else sum_weight end) else sum_weight end)  AS sum_weight,sum(hole_number) AS hole_number,workmanship")
                ->where("parts_id",'=','')
                ->where("pt_num","IN",$ptNumList)
                ->group("pt_num,workmanship")
                ->buildSql();
            $sql_two = $this->model->alias("m")
                ->join(["piece_detail"=>"pd"],"m.id=pd.piece_id")
                ->join(["piece_content"=>"pc"],"pc.id=pd.piece_content_id")
                ->field("pd.pt_num,sum(case when workmanship=1 then (case when stuff='角钢' then ( case when (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+ SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1)<50000 then pd.sum_weight/2 else pd.sum_weight end) when stuff='钢板' then ( case when specification='-10' or specification='-12' or specification='-14' then pd.sum_weight/2 else pd.sum_weight end) else pd.sum_weight end) else pd.sum_weight end)  AS sum_weight,sum(pd.hole_number) AS hole_number,pc.workmanship")
                ->where("pd.pid","=",0)
                ->where("pd.pt_num","IN",$ptNumList)
                ->where($time_where)
                ->group("pd.pt_num,pc.workmanship")
                ->buildSql();
            $sql_three = DB::table($sql_one)->alias("a")
                ->join([$sql_two=>"b"],["a.pt_num=b.pt_num","a.workmanship=b.workmanship"],"left")
                ->field("a.pt_num,a.sum_weight as ll_sum_weight,ifnull(b.sum_weight,0) as sj_sum_weight,a.hole_number as ll_hole_number,ifnull(b.hole_number,0) as sj_hole_number,a.workmanship")
                ->buildSql();

            $list = DB::TABLE($sql_three)->alias("v")
                ->join(["producetask"=>"pt"],"v.pt_num=pt.PT_Num")
                ->join(["taskdetail"=>"td"],"pt.TD_ID=td.TD_ID")
                ->field("v.*,td.TD_TypeName")
                ->order("v.pt_num desc")
                ->select();
            $data = [];
            $ship_list = $this->ship_list;
            foreach($list as $k=>$v){
                isset($data[$v["pt_num"]])?"":$data[$v["pt_num"]] = [
                    "pt_num" => $v["pt_num"],
                    "TD_TypeName" => $v["TD_TypeName"],
                    "ll_normal" => 0,
                    "sj_normal" => 0,
                    "ll_group" => 0,
                    "sj_group" => 0,
                    "ll_welding" => 0,
                    "sj_welding" => 0,
                    "ll_cut" => 0,
                    "sj_cut" => 0,
                    "ll_backhoe" => 0,
                    "sj_backhoe" => 0,
                ];
                if(isset($ship_list[$v["workmanship"]])){
                    $data[$v["pt_num"]]["ll_".$ship_list[$v["workmanship"]][0]] = round($v["ll_sum_weight"],2);
                    $data[$v["pt_num"]]["sj_".$ship_list[$v["workmanship"]][0]] = round($v["sj_sum_weight"],2);
                }
            }
            $result = array("total" => $pt_num->total(), "rows" => array_values($data));

            return json($result);
        }
        return $this->view->fetch();
    }
    
    public function detail($type=1,$ids=null)
    {
        $row = $this->viewModel->alias("v")
        ->join(["producetask"=>"pt"],"v.pt_num=pt.PT_Num")
        ->join(["taskdetail"=>"td"],"pt.TD_ID=td.TD_ID")
        ->field("td.TD_TypeName as TD_TypeName,v.*")
        ->where([
            "v.workmanship" => ["=",$type],
            "v.pt_num" => ["=",$ids]
        ])->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $list = $this->contentModel
        ->field("id,cast((case 
        when stuff='角钢' then 
        SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
        SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
        when (stuff='钢板' or stuff='钢管') then 
        REPLACE(specification,'-','')
        else 0
        end) as UNSIGNED) as order_1,pt_num,(case when workmanship=1 then (case when stuff='角钢' then ( case when (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+ SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1)<50000 then sum_weight/2 else sum_weight end) when stuff='钢板' then ( case when specification='-10' or specification='-12' or specification='-14' then sum_weight/2 else sum_weight end) else sum_weight end) else sum_weight end) as sum_weight,hole_number,stuff,specification,material,workmanship,(case when workmanship=1 then (case when stuff='角钢' then ( case when (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+ SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1)<50000 then surplus_sum_weight/2 else surplus_sum_weight end) when stuff='钢板' then ( case when specification='-10' or specification='-12' or specification='-14' then surplus_sum_weight/2 else surplus_sum_weight end) else surplus_sum_weight end) else surplus_sum_weight end) as surplus_sum_weight,surplus_hole_number,((case when workmanship=1 then (case when stuff='角钢' then ( case when (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+ SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1)<50000 then sum_weight/2 else sum_weight end) when stuff='钢板' then ( case when specification='-10' or specification='-12' or specification='-14' then sum_weight/2 else sum_weight end) else sum_weight end) else sum_weight end)-(case when workmanship=1 then (case when stuff='角钢' then ( case when (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+ SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1)<50000 then surplus_sum_weight/2 else surplus_sum_weight end) when stuff='钢板' then ( case when specification='-10' or specification='-12' or specification='-14' then surplus_sum_weight/2 else surplus_sum_weight end) else surplus_sum_weight end) else surplus_sum_weight end)) as use_sum_weight,(hole_number-surplus_hole_number) as use_hole_number")
        ->where([
            "pt_num" => ["=",$ids],
            "workmanship" => ["=",$type],
            "parts_id" => ["=",""]
        ])->order("stuff,order_1,material")->select();
        // pri($list,1);
        if($list) $list = collection($list)->toArray();
        else $list = [];
        $sum_data = [
            "sum_weight" => 0,
            "hole_number" => 0,
            "surplus_sum_weight" => 0,
            "surplus_hole_number" => 0,
            "use_sum_weight" => 0,
            "use_hole_number" => 0,
        ];
        foreach($list as $k=>$v){
            foreach($sum_data as $kk=>$vv){
                if(isset($v[$kk])){
                    $sum_data[$kk] += $v[$kk];
                }
            }
        }
        $sum_data["pt_num"] = $ids;
        $sum_data["stuff"] = "总计";
        $list[] = $sum_data;
        $this->assignconfig("list",$list);
        $this->assignconfig("type",$type);
        $row["ll_sum_weight"] = round($row["ll_sum_weight"],2);
        $row["sj_sum_weight"] = round($row["sj_sum_weight"],2);
        $row["ll_hole_number"] = round($row["ll_hole_number"],2);
        $row["sj_hole_number"] = round($row["sj_hole_number"],2);
        $this->view->assign("row",$row);
        return $this->view->fetch();
    }

    public function timeline($content_id=null)
    {
        $list = $this->detailModel->alias("pd")
        ->join(["piece_main"=>"pm"],"pd.piece_id=pm.id")
        ->join(["piece_content"=>"pc"],"pc.id=pd.piece_content_id")
        ->field("pm.record_time,pm.writer,pm.create_time,pm.auditor,pm.auditor_time,sum(case when workmanship=1 then (case when stuff='角钢' then ( case when (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+ SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1)<50000 then pd.sum_weight/2 else pd.sum_weight end) when stuff='钢板' then ( case when specification='-10' or specification='-12' or specification='-14' then pd.sum_weight/2 else pd.sum_weight end) else pd.sum_weight end) else pd.sum_weight end)  AS sum_weight,pd.hole_number,pc.stuff,pc.specification,pc.material")
        ->where([
            "pd.piece_content_id" => ["=",$content_id]
        ])->order("pm.record_time desc")->select();
        if($list) $list = collection($list)->toArray();
        else $list = [];
        $sum_data = [
            "sum_weight" => 0,
            "hole_number" => 0
        ];
        foreach($list as $k=>$v){
            foreach($sum_data as $kk=>$vv){
                if(isset($v[$kk])){
                    $sum_data[$kk] += $v[$kk];
                }
            }
            if(!$v["stuff"]) unset($list[$k]);
        }
        $sum_data["stuff"] = "总计";
        $list[] = $sum_data;
        $this->assignconfig("list",$list);
        return $this->view->fetch();
    } 

    public function timelineSum($pt_num=null,$type=0)
    {
        $list = $this->detailModel->alias("pd")
        ->join(["piece_main"=>"pm"],"pd.piece_id=pm.id")
        ->join(["piece_content"=>"pc"],"pc.id=pd.piece_content_id")
        ->field("pm.record_time,pm.writer,pm.create_time,pm.auditor,pm.auditor_time,(case when workmanship=1 then (case when stuff='角钢' then ( case when (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+ SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1)<50000 then pd.sum_weight/2 else pd.sum_weight end) when stuff='钢板' then ( case when specification='-10' or specification='-12' or specification='-14' then pd.sum_weight/2 else pd.sum_weight end) else pd.sum_weight end) else pd.sum_weight end)  AS sum_weight,pd.hole_number,pc.stuff,pc.specification,pc.material")
        ->where([
            "pd.pt_num" => ["=",$pt_num],
            "pd.pid" => ["=",0],
            "pc.workmanship" => ["=",$type]
        ])->order("pm.record_time desc")->select();
        if($list) $list = collection($list)->toArray();
        else $list = [];
        $sum_data = [
            "sum_weight" => 0,
            "hole_number" => 0
        ];
        foreach($list as $k=>$v){
            foreach($sum_data as $kk=>$vv){
                if(isset($v[$kk])){
                    $sum_data[$kk] += $v[$kk];
                }
            }
        }
        $sum_data["stuff"] = "总计";
        $list[] = $sum_data;
        $this->assignconfig("list",$list);
        return $this->view->fetch();
    }
}
