<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\TaskPart;
use app\admin\model\chain\pack\Pack;
use app\admin\model\chain\sale\TaskHeight;
use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class ComponentDetails extends Technology
{
    protected $taskDModel = null, $taskHModel = null, $taskSModel = null,$where = [];
    protected $admin;
    protected $noNeedLogin = ["tableContent","heightLinkage"];
    //名称 字段 类型 规则 默认值
    protected $tableArr = [
        ["TH_ID","TH_ID","text","","hidden"],
        ["*杆塔号","SCD_TPNum","text","data-rule='required'",""],
        ["标段","BiaoDuan","text","",""],
        ["线路","SCD_lineName","text","",""],
        ["*电压等级","TD_Pressure","text","data-rule='required'",""],
        ["*塔形","TD_TypeName","text","data-rule='required'",""],
        ["*呼高","TH_Height","text","data-rule='required'",""],
        ["*产品名称","SCD_ProductName","text","data-rule='required'",""],
        ["*单位","SCD_Unit","text","data-rule='required'",""],
        ["*数量","SCD_Count","number","data-rule='required'",""],
        ["专用段","SCD_SpPart","text","",""],
        ["A段位","SCD_APart","text","",""],
        ["A腿长(米)","SCD_ALength","number","",""],
        ["B段位","SCD_BPart","text","",""],
        ["B腿长(米)","SCD_BLength","number","",""],
        ["C段位","SCD_CPart","text","",""],
        ["C腿长(米)","SCD_CLength","number","",""],
        ["D段位","SCD_DPart","text","",""],
        ["D腿长(米)","SCD_DLength","number","",""],
        ["单重(kg)","SCD_Weight","number","",""],
        ["单价(元/kg)","SCD_UnitPrice","number","",""],
        ["总重量(kg)","Sum_Weight","number","",""],
        ["转角度数","SCD_Corner","text","",""],
        ["基础形式","SCD_BStyle","text","",""],
        ["挂点","SCD_MountPoint","text","",""],
        ["交货日期","SCD_SendDate","text","",""],
        ["基础半根开","SCD_BGK","text","",""],
        ["地脚螺栓直径","SCD_StoneBoltZJ","text","",""],
        ["备注","SCD_Memo","text","",""]
    ];

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_field = \think\Session::get('technology_field');
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_type = \think\Session::get('technology_type');
        $this->taskDModel = new \app\admin\model\chain\sale\TaskDetail;
        $this->taskHModel = new \app\admin\model\chain\sale\TaskHeight;

        $this->admin = \think\Session::get('admin');
        
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->taskDModel->alias("d")
                ->join(["task"=>"t"],"d.T_Num=t.T_Num")
                ->join(["compact"=>"c"],"c.C_Num=t.C_Num")
                ->join(["taskheight"=>"h"],"h.TD_ID = d.TD_ID")
                ->field("0 as 'TD_Count',d.TD_ID,t.C_Num,t.C_Num AS 't.C_Num',c.PC_Num,c.PC_Num AS 'c.PC_Num',t.T_Num,t.T_Num AS 't.T_Num',t.T_Sort,t.T_Company,t.T_WriterDate,t.t_project,d.TD_TypeName,d.TD_Pressure")
                ->where($where)
                ->where("h.TP_Writer","<>","")
                ->where("c.produce_type",$this->technology_type)
                ->order($sort, $order)
                ->group("d.TD_ID")
                ->paginate($limit);
            $dList = [];
            foreach($list as $v){
                $dList[$v["TD_ID"]] = $v->toArray();
            }
            $th_list = $this->taskHModel->alias("h")
                ->join(["sectconfigdetail"=>"s"],"h.TH_ID=s.TH_ID")
                ->field("sum(s.SCD_Count) as SCD_Count,h.TD_ID")
                ->where("h.TD_ID","IN",array_keys($dList))
                ->group("h.TD_ID")
                ->select();
            foreach($th_list as $k=>$v){
                $dList[$v["TD_ID"]]["TD_Count"] = $v["SCD_Count"];
            }
            $result = array("total" => $list->total(), "rows" => array_values($dList));

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 展示
     */
    public function edit($ids = null)
    {
        $row = $this->taskDModel->alias("d")
                ->join(["task"=>"t"],"d.T_Num=t.T_Num")
                ->join(["compact"=>"c"],"c.C_Num=t.C_Num")
                ->field("d.TD_Pressure,d.TD_ID,t.C_Num,c.PC_Num,t.T_Num,t.T_Sort,t.T_Company,t.T_WriterDate,t.t_project,d.TD_TypeName,d.TD_Pressure,c.Customer_Name")
                ->where("d.TD_ID",$ids)
                ->find();
        //有问题 数量到底是scdcount还是td_count
        $th_list = $this->taskHModel->alias("h")
                ->join(["sectconfigdetail"=>"s"],"h.TH_ID=s.TH_ID")
                ->field("sum(s.SCD_Count) as SCD_Count")
                ->where("h.TD_ID",$ids)
                ->group("h.TD_ID")
                ->find();
        $row["SCD_Count"] = $th_list?$th_list["SCD_Count"]:0;
        $this->view->assign("row", $row);
        $this->view->assign("ids",$ids);
        return $this->view->fetch();
    }

    /**
     * 表格展示
     */
    public function tableContent()
    {
        $params = $this->request->post();
        $ids = $params["td_id"]??"";
        if(!$ids) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $where = ["tp.TD_ID"=>["=",$ids]];
        $height = $params["height"]??"";
        $tpNum = $params["towerNum"]??"";
        if(strripos($tpNum,'-')!==FALSE){
            $tpNum = substr($tpNum,0,strripos($tpNum,'-'));
        }
        $part = $params["part"]??"";
        if(strripos($part,'/')!==FALSE){
            $part_list = explode("/",$part);
            $part = trim($part_list[1]);
        }
        $height?$where["th.TH_ID"] = ["=",$height]:"";
        $tpNum?$where["tp.SCD_ID"] = ["IN",[$tpNum,0]]:"";
        $part?$where["tp.TP_SectName"] = ["=",$part]:"";

        $select_search = $params["select_search"]??[];
        $select_arr = [];
        foreach($select_search as $k=>$v){
            foreach($v as $kk=>$vv){
                if($vv) $select_arr[$kk] = $vv;
            }
        }

        $row = (new TaskPart())->alias("tp")
            ->join([$this->technology_ex."dtmaterialdetial"=>"dd"],"tp.DtMD_ID_PK=dd.DtMD_ID_PK")
            ->join(["taskheight"=>"th"],"tp.TH_IDFK = th.TH_ID")
            ->field("th.TH_Height,CAST(th.TH_Height AS UNSIGNED) AS number_3,tp.TP_SectName,tp.TP_PartsID,dd.DtMD_sMaterial,dd.DtMD_sStuff,dd.DtMD_sSpecification,dd.DtMD_iLength,dd.DtMD_fWidth,dd.DtMD_iTorch,dd.DtMD_GeHuo,dd.type,tp.TP_UnitCount,tp.TP_PackCount,tp.TP_SumCount,dd.DtMD_iUnitCount,round(tp.TP_UnitWeight,2) as TP_UnitWeight,round(tp.TP_SumWeight,2) as TP_SumWeight,dd.DtMD_sRemark,dd.DtMD_iWelding,dd.DtMD_iFireBending,dd.DtMD_iCuttingAngle,dd.DtMD_fBackOff,dd.DtMD_iBackGouging,dd.DtMD_DaBian,dd.DtMD_KaiHeJiao,dd.DtMD_ZuanKong,dd.DtMD_ISZhuanYong,dd.DtMD_sType,CAST(tp.TP_SectName AS UNSIGNED) number_1,CAST(tp.TP_PartsID AS UNSIGNED) number_2,tp.SCD_ID,case when LOCATE('-',DtMD_sPartsID)>0 then SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1)
            else 1 end as aaa,	case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn")
            ->where($where)
            ->order("number_3,number_1,bjbhn,tp.TP_SectName,number_2,tp.TP_PartsID");
        if($tpNum) $row = $row->group("tp.DtMD_ID_PK");
        $row = $row->select();
        $th_list = $this->taskHModel->alias("h")
            ->join(["sectconfigdetail"=>"s"],"h.TH_ID=s.TH_ID")
            ->field("s.SCD_ID,s.SCD_TPNum")
            ->where("h.TD_ID",$ids)->select();
        $scd_list =[];
        foreach($th_list as $k=>$v){
            $scd_list[$v["SCD_ID"]] = $v["SCD_TPNum"];
        }
        $list = $DtMD_sStuff_list = $DtMD_sSpecification_list = [];
        $number = $weight = 0;
        foreach($row as $k=>$v){
            $flag = 1;
            foreach($select_arr as $kk=>$vv){
                if($v[$kk] != $vv) $flag=0;
            }

            if($flag){
                $list[$k] = $v->toArray();
                $v["SCD_ID"]==0?"":$list[$k]["TP_PartsID"] = @$scd_list[$v["SCD_ID"]].' / '.$list[$k]["TP_PartsID"];
                $number += $v["TP_UnitCount"];
                $weight += $v["TP_SumWeight"];
            }

            $DtMD_sStuff_list[$v["DtMD_sMaterial"]] = $v["DtMD_sMaterial"];
            $DtMD_sSpecification_list[$v["DtMD_sSpecification"]] = $v["DtMD_sSpecification"];
        }
        $weight = round($weight,2);
        $count = count($list);
        $select_list = [
            "DtMD_sMaterial_select" => build_select('DtMD_sMaterial', [""=>""]+$DtMD_sStuff_list, $select_arr["DtMD_sMaterial"]??'', ['class'=>'form-control selectpicker',"id"=>"c-DtMD_sMaterial"]),
            "DtMD_sSpecification_select" => build_select('DtMD_sSpecification', [""=>""]+$DtMD_sSpecification_list, $select_arr["DtMD_sSpecification"]??'', ['class'=>'form-control selectpicker',"id"=>"c-DtMD_sSpecification"])
        ];
        return json(["code"=>1,"msg"=>"success","data"=>array_values($list),"sum"=>["count"=>$count,"number"=>$number,"weight"=>$weight],"select_list"=>$select_list]);
    }

    public function heightLinkage()
    {
        $params = $this->request->post();
        $td_id = $params["td_id"];
        if(!$td_id) return json(["code"=>0,"msg"=>"有误请重试。"]);
        $height = $params["height"]??"";
        $towerNum = $params["towerNum"]?$params["towerNum"]:"";
        // PRI($params,1);
        $selectList = [];
        $fieldList = $heightList = $tpNumList = $partList = [0=>"请选择"];
        $th_list = $this->taskHModel->alias("h")
            ->join(["sectconfigdetail"=>"s"],"h.TH_ID=s.TH_ID")
            ->field("h.TH_ID,h.TD_ID,s.SCD_ID,s.SCD_TPNum,s.TH_Height,s.SCD_SpPart,s.SCD_Part")
            ->where("h.TD_ID",$td_id)->select();
        foreach($th_list as $v){
            $heightList[$v["TH_ID"]] = $v["TH_Height"];
            //记得改
            isset($tpNumList[$v["TH_ID"]])?$tpNumList[$v["TH_ID"]]:$tpNumList[$v["TH_ID"]]=[0=>"请选择"];
            $tpNumList[$v["TH_ID"]][$v["SCD_ID"]] = $v["TH_Height"].'-'.$v["SCD_TPNum"];
            $commonPart = explode(',',$v["SCD_Part"]);
            $privatePart = explode(',',($v["SCD_TPNum"].' / '.str_replace(",",(','.$v["SCD_TPNum"].' / '),$v["SCD_SpPart"])));
            $arr = [];
            $partArr = array_merge($commonPart,$privatePart);
            foreach($partArr as $vv){
                $partNum = explode("*",$vv);
                $vv = current($partNum);
                $arr[$vv] = $vv;
            }
            $partList[$v["TH_ID"]][$v["SCD_ID"]] = array_merge([0=>"请选择"],$arr);
            $fieldList[$v["TH_ID"]][$v["SCD_ID"]] = $partArr;
        }
        $selectList = [
            "height" => build_select('height',$heightList,$height,['id'=>"height"]),
            "tpNum"  => ($height!="" and !empty($tpNumList))?build_select('tpNum',$tpNumList[$height],$towerNum,['class'=>'form-control selectpicker','id'=>"tpNum"]):build_select('tpNum',[],NULL,['class'=>'form-control selectpicker','id'=>"tpNum"]),
            "part"   => ($towerNum!="" and !empty($partList))?build_select('part',$partList[$height][$towerNum],'',['class'=>'form-control selectpicker','id'=>"part"]):build_select('part',[],NULL,['class'=>'form-control selectpicker','id'=>"part"]),
            "fieldList" => ($towerNum!="" and !empty($fieldList))?'配段：'.implode(",",$fieldList[$height][$towerNum]):"配段"
        ];
        return json(["data"=>$selectList,"code"=>1]);
        
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $this->where = $where = ["TD_ID"=>["=",$ids]];
            $one = (new Pack())->where($where)->find();
            if($one) $this->error("已经打包，无法删除");
            $taskheightModel = new TaskHeight();
            $taskpartmodel = new TaskPart();
            $count = false;
            Db::startTrans();
            try {
                $count = $taskheightModel->where($where)->update(["TP_Writer"=>'',"TP_WriteDate"=>date("0000-00-00 00:00:00")]);
            
                $resultUp = (new \app\admin\model\chain\sale\Sectconfigdetail())
                    ->where('TH_ID','IN',function($query){
                        $query->table('taskheight')->field("TH_ID")->where($this->where);
                    })->update(["SCD_FYFlag"=>0]);
                $result = $taskpartmodel->where($where)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function getDetailField(){
        $tableField = [
            ["呼高","TH_Height"],
            ["段位名","DtS_Name"],
            ["部件号","DtMD_sPartsID"],
            ["材质","DtMD_sMaterial"],
            ["材料","DtMD_sStuff"],
            ["规格","DtMD_sSpecification"],
            ["长度(mm)","DtMD_iLength"],
            ["宽度(mm)","DtMD_fWidth"],
            // ["厚度(mm)","DtMD_iTorch"],
            ["单基数量","TP_UnitCount"],
            ["包装数量","TP_PackCount"],
            ["总数量","TP_SumCount"],
            ["单段数量","DtMD_iUnitCount"],
            ["单件重量(kg)","TP_UnitWeight"],
            ["总重量(kg)","TP_SumWeight"],
            ["备注","DtMD_sRemark"],
            ["电焊","DtMD_iWelding"],
            ["制弯","DtMD_iFireBending"],
            ["切角","DtMD_iCuttingAngle"],
            ["铲背","DtMD_fBackOff"],
            ["清根","DtMD_iBackGouging"],
            ["打扁","DtMD_DaBian"],
            ["开合角","DtMD_KaiHeJiao"],
            ["开合角","DtMD_GeHuo"],
            ["钻孔","DtMD_ZuanKong"],
            // ["是否专用","DtMD_ISZhuanYong"],
            ["类型","DtMD_sType"]
        ];
        return $tableField;
    }
}
