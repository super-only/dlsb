<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\jichu\ch\InventoryMaterial;
use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;

/**
 * 塔型基础库
 *
 * @icon fa fa-circle-o
 */
class BaseLibrary extends Technology
{
    
    /**
     * BaseLibrary模型对象
     * @var \app\admin\model\chain\lofting\BaseLibrary
     */
    protected $model = null,$sectModel=null,$detailModel=null,$admin=null;

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_field = \think\Session::get('technology_field');
        $this->technology_ex = \think\Session::get('technology_ex');
        // $this->technology_type = \think\Session::get('technology_type');
        // $this->technology_xd = \think\Session::get('technology_xd');
        $this->model = new \app\admin\model\chain\lofting\BaseLibrary([],$this->technology_ex);
        $this->sectModel = new \app\admin\model\chain\lofting\BaseLibrarySect([],$this->technology_ex);
        $this->detailModel = new \app\admin\model\chain\lofting\BaseLibraryDetail([],$this->technology_ex);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                list($code,$msg) = $this->model->judRepete($params["DtM_sTypeName"],0);
                if(!$code) $this->error($msg);
                $result = false;
                $params["DtM_sAuditor"] = $this->admin["nickname"];
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('success',null,$result["DtM_iID_PK"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        // pri($this->model->where("DtM_iID_PK",$ids),1);
        // pri($this->model,'');
        // $model = (new \app\admin\model\chain\lofting\BaseLibrary([],$this->technology_ex));
        $row =  (new \app\admin\model\chain\lofting\BaseLibrary([],$this->technology_ex))->where("DtM_iID_PK",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $this->view->assign("row", $row);
        $this->view->assign("tableField",$this->getDetailField());
        $this->assignconfig("ids",$ids);
        $this->assignconfig("task_name",$row["DtM_sTypeName"]);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                list($code,$msg) = $this->model->judRepete($params["DtM_sTypeName"],$ids);
                if(!$code) $this->error($msg);
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    $result = $row->allowField(true)->save($params);
                    // if(!empty($sectSaveList)) $this->sectModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->sectModel->alias("bls")
            ->join([$this->technology_ex."baselibrarydetail"=>"bld"],"bls.DtS_ID_PK = bld.DtMD_iSectID_FK")
            ->field("case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn,CAST(bls.DtS_Name AS UNSIGNED) AS number_1,DtS_Name,CAST(bld.DtMD_sPartsID AS UNSIGNED) AS number_2,bld.*,(case when DtMD_fWidth=0 then '' else DtMD_fWidth end) as DtMD_fWidth")
            ->where("bls.DtM_iID_FK",$ids)
            ->order("number_1,DtS_Name,bjbhn,number_2,DtMD_sPartsID asc")
            // ->select(false);pri($list,1);
            ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 段和明细导入
     */
    public function importView($ids=null)
    { 
        if ($this->request->isPost()) {
            $file = $this->request->post("filename");
            $way = $this->request->post("way");
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $data = [];
            $file = ROOT_PATH . DS . 'public' . DS . $file;
            try {
                $data = Excel::import($file, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($data)){
                    $this->error('空数据文件');
                }
                $data = array_slice($data,1);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
			[$sectList,$detailList] = $this->getImportViewData($data,$ids);
            $where = [
                "DtM_iID_FK" => ["=",$ids],
                "DtS_Name" => ["IN",array_keys($sectList)]
            ];
            $isE = $this->sectModel->where($where)->select();
            $result = false;
            $count = 0;
            Db::startTrans();
            try {
                $sect_id = [];
                foreach($isE as $v){
                    $sect_id[] = $v["DtS_ID_PK"];
                    if($way == "cover") $v->delete();
                    else{
                        $sectList[$v["DtS_Name"]]["DtS_ID_PK"] = $v["DtS_ID_PK"];
                        
                    }
                }
                if(!empty($sect_id) and $way=="cover") $this->detailModel->where("DtMD_iSectID_FK","in",$sect_id)->delete();
                $result = $this->sectModel->allowField(true)->saveAll($sectList);
                $contect = $saveDetailList = [];
                foreach($result as $v){
                    $contect[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                }
                foreach($detailList as $k=>$v){
                    $count += count($v);
                    foreach($v as $vv){
                        $saveDetailList[] = array_merge($vv,["DtMD_iSectID_FK"=>$contect[$k]]);
                    }
                }
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveDetailList);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success("成功导入".$count."条记录！");
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        return $this->view->fetch();
        
    }

    public function export($ids){
        if(!$ids) return json(["code"=>0,"msg"=>"请稍后重试！"]);
        $one =  (new \app\admin\model\chain\lofting\BaseLibrary([],$this->technology_ex))->where("DtM_iID_PK",$ids)->find();
        $title = $one["DtM_sTypeName"];
        $list = collection($this->sectModel->alias("bls")
            ->join([$this->technology_ex."baselibrarydetail"=>"bld"],"bls.DtS_ID_PK = bld.DtMD_iSectID_FK")
            ->field("case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn,CAST(bls.DtS_Name AS UNSIGNED) AS number_1,CAST(bld.DtMD_sPartsID AS UNSIGNED) AS number_2,bls.DtS_Name,bld.*,round(DtMD_iUnitCount*DtMD_fUnitWeight,2) as sum_weight")
            ->where("bls.DtM_iID_FK",$ids)
            ->order("number_1,DtS_Name,bjbhn,number_2,DtMD_sPartsID asc")
            ->select())->toArray();
        
        [$list,$header] = $this->getExportData($list);

        return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
    }

    public function openEdit($ids=null)
    {
        $row =  (new \app\admin\model\chain\lofting\BaseLibrary([],$this->technology_ex))->where("DtM_iID_PK",$ids)->find();
        if (!$row) {
            return json(["code"=>0,"msg"=>__('No Results were found')]);
        }
        $fieldList = $this->getDetailField();
        if ($this->request->isPost()) {
            $params = $this->request->post("data");
            /*******注意 */
            $params = json_decode(str_replace('&quot;','"',$params), true);
            $newFieldList = [];
            foreach($fieldList as $k=>$v){
                $newFieldList[$v[1]] = $k+2;
            }
            if ($params) {
                $params = $this->preExcludeFields($params);
                $biZhong = (new InventoryMaterial())->getIMPerWeight();
                $sectList = $detailList = [];
                foreach($params as $v){
                    foreach($v as $pk=>$pv){
                        $v[$pk] = trim($pv);
                    }
                    $v[$newFieldList["DtMD_iUnitCount"]] = round($v[$newFieldList["DtMD_iUnitCount"]],2);
                    if(floor($v[$newFieldList["DtMD_iUnitCount"]])!==$v[$newFieldList["DtMD_iUnitCount"]]) return json(["code"=>0,"msg"=>"部件号为".$v[$newFieldList["DtMD_sPartsID"]]."的单基数量不能为小数"]);
                    // if($v[$newFieldList['DtS_Name']]===''){
                        $Dts_Name = 0;
                        $DtMD_sPartsID = $v[$newFieldList['DtMD_sPartsID']];
                        $Dts_Name = $DtMD_sStuff = $DtMD_sMaterial = "";
                        if(substr($DtMD_sPartsID,0,2)=='00'){
                            $Dts_Name = 0;
                        }else{
                            if(strpos($DtMD_sPartsID,"-") > 0){
                                $left_h_part = substr($DtMD_sPartsID,0,strpos($DtMD_sPartsID,"-"));
                                preg_match_all('/(\d+)|([^\d]+)/',$left_h_part,$matches);
                                $Dts_Name = $matches[0][0]??0;
                            }else{
                                $part = (int)$DtMD_sPartsID;
                                if(strlen($part)<=2) $Dts_Name = (int)$part;
                                else if(strlen($part)==3) $Dts_Name = substr($part,0,1);
                                else $Dts_Name = substr($part,0,2);
                            }
                        }
                        if(strlen($Dts_Name)>1){
                            $Dts_Name = substr($Dts_Name,0,1)==0?substr($Dts_Name,1):$Dts_Name;
                        }
                    // }else $Dts_Name = $v[$newFieldList['DtS_Name']];
                    $sectList[$Dts_Name] = [
                        "DtS_Name" => $Dts_Name,
                        "DtM_iID_FK" => $ids,
                        "DtS_sWriter" => $this->admin["nickname"],
                        "DtS_dWriterTime" => date("Y-m-d H:i:s"),
                        "DtS_dModifyTime" => date("Y-m-d H:i:s")
                    ];
                    $DtMD_sPartsID = $v[$newFieldList['DtMD_sPartsID']];
                    $DtMD_sMaterial = $v[$newFieldList['DtMD_sMaterial']]==''?'Q235B':$v[$newFieldList['DtMD_sMaterial']];
                    $v[$newFieldList['DtMD_sSpecification']] = str_replace("L","∠",$v[$newFieldList['DtMD_sSpecification']]);
                    $v[$newFieldList['DtMD_sSpecification']] = str_replace("∟","∠",$v[$newFieldList['DtMD_sSpecification']]);
                    $v[$newFieldList['DtMD_sSpecification']] = str_replace("X","*",$v[$newFieldList['DtMD_sSpecification']]);
                    $v[$newFieldList['DtMD_sSpecification']] = str_replace("x","*",$v[$newFieldList['DtMD_sSpecification']]);
                    $v[$newFieldList['DtMD_sSpecification']] = str_replace("Φ","ф",$v[$newFieldList['DtMD_sSpecification']]);
                    $firstField = mb_substr(trim($v[$newFieldList['DtMD_sSpecification']]),0,1);
                    $DtMD_sStuff = "";
                    
                    if($firstField == '∠') $DtMD_sStuff = '角钢';
                    elseif($firstField == '-') $DtMD_sStuff = '钢板';
                    elseif($firstField == '[') $DtMD_sStuff = '槽钢';
                    elseif($firstField == 'G') $DtMD_sStuff = '格栅板';
                    else{
                        preg_match_all("/\d+\.?\d*/",$v[$newFieldList["DtMD_sSpecification"]],$matches);
                        if(count($matches[0])==2) $DtMD_sStuff = '钢管';
                        else $DtMD_sStuff = '圆钢';
                    }
                    
                    $length = $v[$newFieldList['DtMD_iLength']]?$v[$newFieldList['DtMD_iLength']]*0.001:0;
                    $width = $v[$newFieldList['DtMD_fWidth']]?$v[$newFieldList['DtMD_fWidth']]*0.001:0;
                    $area = $DtMD_sStuff!='钢板'?$length:$length*$width*abs($v[$newFieldList['DtMD_sSpecification']]);

                    $DtMD_fUnitWeight = 0;
                    if($DtMD_sStuff=="圆钢"){
                        preg_match_all("/\d+\.?\d*/",$v[$newFieldList['DtMD_sSpecification']],$matches);
                        $L = isset($matches[0][0])?$matches[0][0]:0;
                        $DtMD_fUnitWeight = round($L*$L*$length*0.00617,2);
                    }else if($DtMD_sStuff=="钢管"){
                        preg_match_all("/\d+\.?\d*/",$v[$newFieldList["DtMD_sSpecification"]],$matches);
                        $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                        $r = isset($matches[0][1])?$matches[0][1]/2*0.001:0;
                        if(strpos($v[$newFieldList["DtMD_sSpecification"]],"*")) $r = $R-(2*$r);
                        $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
                    }else if($DtMD_sStuff=="格栅板"){
                        $rou = ["G255/40/100W"=>32.1,"G253/40/100W"=>21.3];
                        $rou_one = isset($rou[$v[$newFieldList["DtMD_sSpecification"]]])?$rou[$v[$newFieldList["DtMD_sSpecification"]]]:0;
                        $DtMD_fUnitWeight = round($rou_one * $length*$width,4);
                    }else{
                        $DtMD_fUnitWeight = (isset($biZhong[$DtMD_sStuff][$v[$newFieldList['DtMD_sSpecification']]]))?round($biZhong[$DtMD_sStuff][$v[$newFieldList['DtMD_sSpecification']]] * $area,4):0;
                    }
                    $detailList[$Dts_Name][] = [
                        'DtMD_sPartsID' => $DtMD_sPartsID,
                        'DtMD_sStuff' => $DtMD_sStuff,
                        'DtMD_sMaterial' => $DtMD_sMaterial,
                        'DtMD_sSpecification' => $v[$newFieldList['DtMD_sSpecification']],
                        'DtMD_iLength' => $v[$newFieldList['DtMD_iLength']]?$v[$newFieldList["DtMD_iLength"]]:0,
                        'DtMD_fWidth' => $v[$newFieldList['DtMD_fWidth']]?$v[$newFieldList["DtMD_fWidth"]]:0,
                        'DtMD_iTorch' => $v[$newFieldList['DtMD_iTorch']]?$v[$newFieldList["DtMD_iTorch"]]:0,
                        'DtMD_iUnitCount' => $v[$newFieldList["DtMD_iUnitCount"]],
                        'DtMD_fUnitWeight' => $DtMD_fUnitWeight,
                        'DtMD_iUnitHoleCount' => $v[$newFieldList['DtMD_iUnitHoleCount']],
                        'type' => $v[$newFieldList["type"]],
                        'DtMD_iWelding' => (isset($v[$newFieldList['DtMD_iWelding']]) and $v[$newFieldList['DtMD_iWelding']])?$v[$newFieldList['DtMD_iWelding']]:0,
                        'DtMD_iFireBending' => (isset($v[$newFieldList['DtMD_iFireBending']]) and $v[$newFieldList['DtMD_iFireBending']])?$v[$newFieldList['DtMD_iFireBending']]:0,
                        'DtMD_iCuttingAngle' => (isset($v[$newFieldList['DtMD_iCuttingAngle']]) and $v[$newFieldList['DtMD_iCuttingAngle']])?$v[$newFieldList['DtMD_iCuttingAngle']]:0,
                        'DtMD_fBackOff' => (isset($v[$newFieldList['DtMD_fBackOff']]) and $v[$newFieldList['DtMD_fBackOff']])?$v[$newFieldList['DtMD_fBackOff']]:0,
                        'DtMD_iBackGouging' => (isset($v[$newFieldList['DtMD_iBackGouging']]) and $v[$newFieldList['DtMD_iBackGouging']])?$v[$newFieldList['DtMD_iBackGouging']]:0,
                        'DtMD_DaBian' => (isset($v[$newFieldList['DtMD_DaBian']]) and $v[$newFieldList['DtMD_DaBian']])?$v[$newFieldList['DtMD_DaBian']]:0,
                        'DtMD_KaiHeJiao' => (isset($v[$newFieldList['DtMD_KaiHeJiao']]) and $v[$newFieldList['DtMD_KaiHeJiao']])?$v[$newFieldList['DtMD_KaiHeJiao']]:0,
                        'DtMD_ZuanKong' => (isset($v[$newFieldList['DtMD_ZuanKong']]) and $v[$newFieldList['DtMD_ZuanKong']])?$v[$newFieldList['DtMD_ZuanKong']]:0,
                        'DtMD_GeHuo' => (isset($v[$newFieldList['DtMD_GeHuo']]) and $v[$newFieldList['DtMD_GeHuo']])?$v[$newFieldList['DtMD_GeHuo']]:0,
                        'DtMD_sRemark' => $v[$newFieldList['DtMD_sRemark']]
                    ];
                }
                $result = false;
                $where = [
                    "DtM_iID_FK" => ["=",$ids]
                ];
                $isE = $this->sectModel->where($where)->select();
                Db::startTrans();
                try {
                    $sect_id = $saveDetailList = [];
                    foreach($isE as $v){
                        $sect_id[] = $v["DtS_ID_PK"];
                        $v->delete();
                    }
                    $this->detailModel->where("DtMD_iSectID_FK","IN",$sect_id)->delete();
                    $result = $this->sectModel->allowField(true)->saveAll($sectList);
                    $contect = $saveDetailList = [];
                    foreach($result as $v){
                        $contect[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                    }
                    foreach($detailList as $k=>$v){
                        foreach($v as $vv){
                            $saveDetailList[] = array_merge($vv,["DtMD_iSectID_FK"=>$contect[$k]]);
                        }
                    }
                    $detailResult = $this->detailModel->allowField(true)->saveAll($saveDetailList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    return json(["code"=>0,"msg"=>$e->getMessage()]);
                } catch (PDOException $e) {
                    Db::rollback();
                    return json(["code"=>0,"msg"=>$e->getMessage()]);
                } catch (Exception $e) {
                    Db::rollback();
                    return json(["code"=>0,"msg"=>$e->getMessage()]);
                }
                if ($result !== false) {
                    return json(["code"=>1,"msg"=>"保存成功！"]);
                } else {
                    return json(["code"=>0,"msg"=>__('No rows were updated')]);
                }
            }
            return json(["code"=>0,"msg"=>__('Parameter %s can not be empty', '')]);
        }
        $list = $this->sectModel->alias("bls")
            ->join([$this->technology_ex."baselibrarydetail"=>"bld"],"bls.DtS_ID_PK = bld.DtMD_iSectID_FK")
            ->field("case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn,bls.DtS_Name,CAST(bls.DtS_Name AS UNSIGNED) AS number_1,CAST(bld.DtMD_sPartsID AS UNSIGNED) AS number_2,bld.*,(case when DtMD_fWidth=0 then '' else DtMD_fWidth end) as DtMD_fWidth, type, DtMD_GeHuo ")
            ->where("bls.DtM_iID_FK",$ids)
            ->order("number_1,DtS_Name,bjbhn,number_2,DtMD_sPartsID asc")
            ->select();
        $nameList = $this->sectModel->field("DtS_Name,CAST(DtS_Name AS UNSIGNED) AS number_1")->where("DtM_iID_FK",$ids)->group("DtS_Name")->order("number_1,DtS_Name")->select();
        $nameArr = [];
        foreach($nameList as $k=>$v){
            $nameArr[$v["DtS_Name"]] = $v["DtS_Name"];
        }
        $this->view->assign("row", $row);
        $this->view->assign("list",$list);
        $this->view->assign("tableField",$fieldList);
        $this->view->assign("nameArr",$nameArr);
        $this->assignconfig("tableField",$fieldList);
        $this->assignconfig("ids",$ids);
        $this->assignconfig("task_name",$row["DtM_sTypeName"]);
        $this->assignconfig("tableCopyField",$this->getCopyDetailField());
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();

            $taskDetail = $this->sectModel->field("DtS_ID_PK")->where("DtM_iID_FK",$ids)->select();
            $task_detail_id = [];
            
            $count = 0;
            Db::startTrans();
            try {
                foreach($taskDetail as $v){
                    $task_detail_id[] = $v["DtS_ID_PK"];
                    $v->delete();
                }
                $this->detailModel->where("DtMD_iSectID_FK","IN",$task_detail_id)->delete();
                foreach ($list as $v) {
                    $count += $v->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function getDetailField(){
        $tableField = [
            ['段号', 'DtS_Name',"text","data-rule='required'"],
            ['零件编号', 'DtMD_sPartsID',"text","data-rule='required'"],
            ['材质', 'DtMD_sMaterial',"text",""],
            ['规格', 'DtMD_sSpecification',"text",""],
            ['长度(mm)', 'DtMD_iLength',"text",""],
            ['宽度(mm)', 'DtMD_fWidth',"text",""],
            ['厚度(mm)', 'DtMD_iTorch',"text",""],
            ['单基数量', 'DtMD_iUnitCount',"text","data-rule='required'"],
            // ['单基重量', 'DtMD_fUnitWeight'],
            ['孔数', 'DtMD_iUnitHoleCount',"text",""],
            ['类型', 'type',"text",""],
            ['电焊(0/1/2/3)', 'DtMD_iWelding',"text",""],
            ['制弯(0/1/2)', 'DtMD_iFireBending',"text",""],
            ['切角(0/1)', 'DtMD_iCuttingAngle',"text",""],
            ['铲背(0/1)', 'DtMD_fBackOff',"text",""],
            ['清根(0/1)', 'DtMD_iBackGouging',"text",""],
            ['打扁(0/1)', 'DtMD_DaBian',"text",""],
            ['开合角(0/1/2)', 'DtMD_KaiHeJiao',"text",""],
            ['割豁(0/1)', 'DtMD_GeHuo',"text",""],
            ['钻孔(0/1)', 'DtMD_ZuanKong',"text",""],
            ['备注', 'DtMD_sRemark',"text",""]
        ];
        return $tableField;
    }

    /**
     * 打印
     */
    public function print($ids = null)
    {
        $row =  (new \app\admin\model\chain\lofting\BaseLibrary([],$this->technology_ex))->where("DtM_iID_PK",$ids)->find();

        if (!$row) {
            $this->error(__('No Results were found'));
        }
        
        $list = $this->sectModel->alias("bls")
            ->join([$this->technology_ex."baselibrarydetail"=>"bld"],"bls.DtS_ID_PK = bld.DtMD_iSectID_FK")
            ->field("case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn,CAST(bls.DtS_Name AS UNSIGNED) AS number_1,DtS_Name,CAST(bld.DtMD_sPartsID AS UNSIGNED) AS number_2,bld.*,(case when DtMD_fWidth=0 then '' else DtMD_fWidth end) as DtMD_fWidth")
            ->where("bls.DtM_iID_FK",$ids)
            ->order("number_1,DtS_Name,bjbhn,number_2,DtMD_sPartsID asc")
            ->select();
        $this->assignconfig("row", $row);
        $this->assignconfig("list",$list);
        $row['DtM_dTime'] = date("Y-m-d",strtotime($row["DtM_dTime"]));
        
        $this->assignconfig("task_name",$row["DtM_sTypeName"]);
        return $this->view->fetch();
    }

    public function getCopyDetailField(){
        $tableField = [
            ["URL","",""],
            ["DtMD_ID_PK","","hidden"],
            ["DtMD_sPartsID","required",""],
            ["DtMD_sMaterial","",""],
            ["DtMD_sStuff","",""],
            ["DtMD_sSpecification","",""],
            ["DtMD_iLength","required",""],
            ["DtMD_fWidth","required",""],
            ["DtMD_iTorch","required",""],
            ["DtMD_iUnitCount","",""],
            ["DtMD_fUnitWeight","",""],
            ["sumWeight","",""],
            ["DtMD_iUnitHoleCount","",""],
            ["type","",""],
            ["DtMD_iWelding","",""],
            ["DtMD_iFireBending","",""],
            ["DtMD_iCuttingAngle","",""],
            ["DtMD_fBackOff","",""],
            ["DtMD_iBackGouging","",""],
            ["DtMD_DaBian","",""],
            ["DtMD_KaiHeJiao","",""],
            ["DtMD_GeHuo","",""],
            ["DtMD_ZuanKong","",""],
            // ["DtMD_iBoreholeCount","","hidden"],
            ["DtMD_sRemark","",""],
            // ["DtMD_iExcavationCount","",""],
            // ["DtMD_iAssembly","",""],
            // ["DtMD_ISZhuanYong","",""]
        ];
        return $tableField;
    }
}
