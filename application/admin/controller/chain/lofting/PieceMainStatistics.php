<?php

namespace app\admin\controller\chain\lofting;

use app\common\controller\Backend;
use think\Db;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class PieceMainStatistics extends Backend
{
    
    /**
     * PieceMain模型对象
     * @var \app\admin\model\chain\lofting\PieceMain
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\lofting\PieceMain;
        $this->admin = \think\Session::get('admin');
        $ship_list = [1=>"普通",2=>"电焊",3=>"弯曲",4=>"切角",5=>"铲背",6=>"清根",7=>"打扁",8=>"开合角",9=>"钻孔",10=>'割豁',11=>"制弯打扁开合角"];
        $this->assignconfig("ship_list",$ship_list);
        $defaultTime = date("Y-m-1 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        $this->machineArr = $this->machineList();
        $this->assignconfig("machine_arr",$this->machineArr[0]);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $where_content = ["pd.pid"=>["=",0],"pm.auditor"=>["<>",'']];
            $where_time = [];
            $where_people = [];
            (isset($filter["device"]) and $filter["device"])?$where_content["pm.device"] = ["=",$filter["device"]]:"";
            (isset($filter["TD_TypeName"]) and $filter["TD_TypeName"])?$where_content["td.TD_TypeName"] = ["=",$filter["TD_TypeName"]]:"";
            (isset($filter["workmanship"]) and $filter["workmanship"])?$where_content["pc.workmanship"] = ["=",$filter["workmanship"]]:"";
            (isset($filter["operator"]) and $filter["operator"])?$where_content["po.operator"] = ["LIKE","%".$filter["operator"]."%"]:"";
            (isset($filter["record_time"]) and $filter["record_time"])?$where_time["pm.record_time"] = ["between time",array_values(explode(" - ",$filter["record_time"]))]:"";
            $this->where_time = $where_time;
            
            $people_query = "(select piece_main_id,group_concat(operator) as operator from piece_operator where operator<>'' GROUP BY piece_main_id)";
            $pj_query = DB::table("piece_main")->alias("pm")
                ->join(["piece_detail"=>"pd"],"pm.id=pd.piece_id")
                ->join(["piece_content"=>"pc"],"pd.piece_content_id = pc.id")
                ->join(["producetask"=>"pt"],"pd.PT_Num = pt.PT_Num")
                ->join(["taskdetail"=>"td"],"pt.TD_ID = td.TD_ID")
                ->join([$people_query=>"po"],"po.piece_main_id=pm.id")
                // ->field("pm.id,pm.record_time,pm.device,td.TD_TypeName,pc.workmanship,sum(pd.sum_weight) as sum_weight,sum(pd.hole_number) as hole_number ")
                ->field("pm.id,pm.record_time,pm.device,pd.PT_Num,td.TD_TypeName,pc.workmanship,sum(case when stuff='角钢' then ( case when (SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+ SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1)<50000 then pd.sum_weight/2 else pd.sum_weight end) when stuff='钢板' then ( case when specification='-10' or specification='-12' or specification='-14' then pd.sum_weight/2 else pd.sum_weight end) else pd.sum_weight end) as sum_weight,sum(pd.hole_number) as hole_number,po.operator")
                ->where($where_content)
                ->where(function ($query) {
                    $query->where($this->where_time);
                })
                ->group("pm.id,pd.PT_Num,td.TD_ID,pc.workmanship,pm.device")
                ->select();
            $list = [];
            if($pj_query) $list = collection($pj_query)->toArray();

            // $people_query = "(select piece_main_id,count(id) as count from piece_operator GROUP BY piece_main_id)";
            // $summary_query = DB::table($pj_query)->alias("a")
            //     ->join([$people_query=>"b"],"a.id=b.piece_main_id")
            //     ->field("b.piece_main_id,record_time,device,TD_TypeName,workmanship,round(sum_weight/count,2) as sum_weight,round(hole_number/count) as hole_number")
            //     ->buildSql();
            // $list = DB::table($summary_query)->alias("c")
            //     ->join(["piece_operator"=>"po"],"po.piece_main_id=c.piece_main_id")
            //     ->where($where_people)
            //     ->order("po.piece_main_id,po.operator")
            //     ->select(false);
            //     pri($list,1);
            $weight = $hole = 0;
            foreach($list as $k=>$v){
                $list[$k]["sum_weight"] = round($v["sum_weight"],2);
                $weight += $v["sum_weight"];
                $hole += $v["hole_number"];
            }
            $list[] = ["operator"=>"合计","sum_weight"=>round($weight,2),"hole_number"=>$hole];
            $result = array("total" => count($list), "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
}
