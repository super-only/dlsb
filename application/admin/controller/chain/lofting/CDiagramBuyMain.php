<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\CompactDiagramMain;
use app\admin\model\chain\lofting\CompactDiagramSect;
use app\admin\model\chain\sale\TaskHeight;
use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class CDiagramBuyMain extends Technology
{
    
    /**
     * CDiagramBuyMain模型对象
     * @var \app\admin\model\chain\lofting\CDiagramBuyMain
     */
    protected $model = null;
    protected $noNeedLogin = ["arrange"];

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_xd = \think\Session::get('technology_xd');
        $this->model = new \app\admin\model\chain\lofting\CDiagramBuyMain([],$this->technology_ex);
        $this->sectModel = new \app\admin\model\chain\lofting\CDiagramBuy([],$this->technology_ex);
        $this->admin = \think\Session::get('admin');
        $this->cdModel = new \app\admin\model\chain\lofting\CompactDiagramMain([],$this->technology_ex);
        $this->cdSectModel = new \app\admin\model\chain\lofting\CompactDiagramSect([],$this->technology_ex);
        $this->cdDetailModel = new \app\admin\model\chain\lofting\CompactDiagram([],$this->technology_ex);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
    */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("cdbm")
                ->join([$this->technology_ex."compactdiagrammain"=>"cdm"],"cdbm.CDM_Num = cdm.CDM_Num","left")
                ->join(["taskDetail"=>"td"],"cdm.TD_ID=td.TD_ID","left")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num","left")
                ->field("cdbm.*,cdm.TD_ID,cdm.CDM_CProject,cdm.CDM_TypeName,cdm.CDM_Pressure,(CASE WHEN cdm.TD_ID='0' THEN '未关联' ELSE t.C_Num END) as C_Num")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params) {
                $where = [
                    "CDM_Num" => ["=",$params["CDM_Num"]],
                    "CDBM_Mark" => ["=",$params["CDBM_Mark"]]
                ];
                $one = $this->model->where($where)->find();
                if($one) return json(["code"=>0,'msg'=>"已存在改批次的数据，请重新填写批次"]);

                $year = date("Y",time());
                $findOne = $this->model->where("CDBM_Num",'LIKE','TL'.$year.'%')->order("CDBM_Num DESC")->find();

                if(isset($findOne["CDBM_Num"])){
                    $num = substr($findOne["CDBM_Num"],6);
                    $num = $year.$this->technology_xd.str_pad((++$num),4,0,STR_PAD_LEFT);
                }else $num = $year.$this->technology_xd.'0001';
                $params["CDBM_Num"] = 'TL'.$num;

                $sectSaveList = [];
                        
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        if($k=="CDB_ID" and $vv==0) continue;
                        $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $bjhList = [];
                foreach($sectSaveList as $k=>$v){
                    if(isset($bjhList[$v["CDB_SectName"]])) return json(["code"=>0,'msg'=>"段号有重复，请进行修改！"]);
                    else $bjhList[$v["CDB_SectName"]] = $v["CDB_SectName"];
                    foreach($v as $kk=>$vv){
                        if($vv=='') unset($sectSaveList[$k][$kk]);
                    }
                    $sectSaveList[$k]["CDBM_Num"] = $params["CDBM_Num"];
                }

                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model::create($params);
                    if(!empty($sectSaveList)) $this->sectModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                
                if ($result) {
                    $this->success('成功！',null,$params["CDBM_Num"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = ["writer"=>$this->admin["nickname"],"time"=>date("Y-m-d H:i:s")];
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->alias("cdbm")
            ->join([$this->technology_ex."compactdiagrammain"=>"cdm"],"cdm.CDM_Num = cdbm.CDM_Num","left")
            ->join(["taskdetail"=>"d"],"cdm.TD_ID = d.TD_ID","left")
            ->join(["task"=>"t"],"t.T_Num = d.T_Num","left")
            ->field("cdbm.CDBM_Num,cdm.CDM_CProject,cdm.CDM_Num,cdm.CDM_TypeName,cdm.CDM_Pressure,t.C_Num,cdbm.CDBM_Mark,cdbm.SCDCount,cdbm.Writer,cdbm.WriteDate,cdbm.Auditor,cdbm.AuditDate,cdbm.CDBM_Remark,cdbm.TD_ID,cdbm.Auditor,cdbm.AuditDate")
            ->where("cdbm.CDBM_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $sectSaveList = [];
                        
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        if($k=="CDB_ID" and $vv==0) continue;
                        $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $bjhList = [];
                foreach($sectSaveList as $k=>$v){
                    if(isset($bjhList[$v["CDB_SectName"]])) return json(["code"=>0,'msg'=>"段号有重复，请进行修改！"]);
                    else $bjhList[$v["CDB_SectName"]] = $v["CDB_SectName"];
                    foreach($v as $kk=>$vv){
                        if($vv=='') unset($sectSaveList[$k][$kk]);
                    }
                    $sectSaveList[$k]["CDBM_Num"] = $ids;
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("CDBM_Num",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->sectModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $sectList = $this->sectModel->field("CDB_SectName,CDB_ID,CDB_Count,CAST(CDB_SectName as unsigned) as number")->where("CDBM_Num",$ids)->order("number,CDB_SectName ASC")->select();
        $tableField = $this->getTableField();
        $this->view->assign("tableField",$tableField);
        $this->view->assign("row", $row);
        $this->assignconfig("auditor",$row["Auditor"]);
        $this->view->assign("sectList", objectToArray($sectList));
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function chooseDiagram()
    {
         //设置过滤方法
         $this->request->filter(['strip_tags', 'trim']);
         if ($this->request->isAjax()) {
             //如果发送的来源是Selectpage，则转发到Selectpage
             if ($this->request->request('keyField')) {
                 return $this->selectpage();
             }
             list($where, $sort, $order, $offset, $limit) = $this->buildparams();
             $list = (new CompactDiagramMain([],$this->technology_ex))->alias("cdm")
                ->join(["taskdetail"=>"td"],"cdm.TD_ID = td.TD_ID","left")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num","left")
                ->field("(CASE WHEN cdm.TD_ID='0' THEN '未关联' ELSE td.T_Num END) as 'td.T_Num',(CASE WHEN cdm.TD_ID='0' THEN '未关联' ELSE t.C_Num END) as 't.C_Num',cdm.CDM_Num,cdm.TD_ID,cdm.CDM_CProject,CDM_TypeName,CDM_Pressure")
                ->where($where)
                ->order($sort,$order)
                ->paginate($limit);
 
             $result = array("total" => $list->total(), "rows" => $list->items());
 
             return json($result);
         }
         return $this->view->fetch();
    }

    public function getChooseDiagram()
    {
        $cdm_num = $this->request->post("CDM_Num");
        if(!$cdm_num) return json(["code"=>0,"msg"=>"有误稍后重试"]);
        $list = (new CompactDiagramSect([],$this->technology_ex))->field("CDS_Name as 'CDB_SectName',1 as 'CDB_Count',cast(CDS_Name as unsigned) as number,0 as 'CDB_ID'")->where("CDM_Num",$cdm_num)->order("number,CDS_Name asc")->select();
        $tableField = $this->getTableField();
        $content = "";
        foreach($list as $v){
            $content .= '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
            foreach($tableField as $kk=>$vv){
                $content .= '<td '.$vv[6].'><input class="small_input" type="'.$vv[2].'" '.$vv[4].' name="table_row['.$vv[1].'][]" value="'.$v[$vv[1]].'"></td>';
            }
        }
        return json(["code"=>1,"data"=>$content]);
    }

    public function delDetail()
    {
        $num = $this->request->post("num");
        if($num){
            Db::startTrans();
            try {
                $this->sectModel->where("CDB_ID",$num)->delete();
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>1,'msg'=>"删除成功！"]);
    }

    public function allocationDiagram()
    {
        $CDBM_Num = $this->request->post("CDBM_Num");
        $CDBM_Num = $CDBM_Num=='自动编码'?"":$CDBM_Num;
        $TD_ID = $this->request->post("TD_ID");
        if(!$TD_ID) return json(["code"=>0,"msg"=>"未关联塔型不能调拨杆塔汇总配段"]);
        $list = (new TaskHeight())->alias("th")
            ->join(["sectconfigdetail"=>"scd"],"scd.TH_ID = th.TH_ID")
            ->join(["tasksect"=>"ts"],"ts.SCD_ID = scd.SCD_ID")
            ->field("TS_Name as 'CDB_SectName',0 as 'CDB_ID',sum(TS_Count) as 'CDB_Count',cast(TS_Name as unsigned) as number")
            ->where("th.TD_ID",$TD_ID)
            ->order("number,TS_Name asc")
            ->group("TS_Name")
            ->select();

        $tableField = $this->getTableField();
        $content = "";
        $save = [];
        foreach($list as $v){
            $content .= '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
            foreach($tableField as $kk=>$vv){
                $content .= '<td '.$vv[6].'><input class="small_input" type="'.$vv[2].'" '.$vv[4].' name="table_row['.$vv[1].'][]" value="'.$v[$vv[1]].'"></td>';
            }
            $save[] = ["CDB_Count"=>$v["CDB_Count"],"CDBM_Num"=>$CDBM_Num,"CDB_SectName"=>$v["CDB_SectName"]];
        }
        if($CDBM_Num and $save){
            $result = false;
            Db::startTrans();
            try {
                $this->sectModel->where("CDBM_Num",$CDBM_Num)->delete();
                $result = $this->sectModel->allowField(true)->saveAll($save);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
            if ($result !== false) {
                return json(["code"=>1,"data"=>$content]);
            } else {
                return json(["code"=>0,"msg"=>"调拨失败，请稍后重试"]);
            }
        }
        return json(["code"=>1,"data"=>$content]);
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();
            $count = 0;
            Db::startTrans();
            try {
                foreach($list as $v){
                    $taskDetail[] = $v["CDBM_Num"];
                    $count += $v->delete();
                }
                $count += $this->sectModel->where("CDBM_Num","IN",$taskDetail)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }


    //编辑table
    public function getTableField()
    {
        $list = [
            ["CDB_ID","CDB_ID","text","","readonly",0,"hidden"],
            ["*段名","CDB_SectName","text","","data-rule='required'","",""],
            ["*段数","CDB_Count","text","","data-rule='required'",1,""]
        ];
        return $list;
    }

    /**
     * 打印汇总表
     */
    public function hzbPrint($ids = null)
    {
        // dump($ids);
        $row = $this->model->alias("cdbm")
            ->where("cdbm.CDBM_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        $cdRow = $this->cdModel->where("CDM_Num", $row['CDM_Num'])->find();
        
        $list = $this->model->alias("cdbm")
        ->join([$this->technology_ex."cdiagrambuy"=>"cdb"], "cdb.CDBM_Num=cdbm.CDBM_Num")
        ->join([$this->technology_ex."compactdiagramsect"=>"cds"], "cds.CDM_Num=cdbm.CDM_Num and cds.CDS_Name = cdb.CDB_SectName", "left")
        ->join([$this->technology_ex."compactdiagram"=>"cd"], "cds.CDS_ID=cd.CDS_ID")
        ->field("cds.CDS_Name, 
            case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when dtmd_sstuff='角钢' then 
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
            when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
            REPLACE(DtMD_sSpecification,'-','')
            else 0
            end) as UNSIGNED) bh,
        cd.*, sum(cd.DtMD_SumWeight*cdb.CDB_Count) as groupWeightSum, sum(cd.DtMD_Count*cdb.CDB_Count) as groupCountSum")
        ->where("cdbm.CDBM_Num", $ids)
        ->group("DtMD_sStuff,DtMD_sMaterial,DtMD_sSpecification") 
        ->order("clsort,DtMD_sMaterial,bh")
        ->select();

        $detailList = [];
        $mainInfos = [];
        foreach($list as $v){
            $v = $v->toArray();
            array_push($detailList, $v);
        }

        $mainInfos['CDM_CProject'] = $cdRow["CDM_CProject"];
        $mainInfos['CDM_TypeName'] = $cdRow["CDM_TypeName"];
        $mainInfos['Writer'] = $row["Writer"];
        $mainInfos['Auditor'] = $row["Auditor"];
        $mainInfos['idate']=date('Y-m-d');
        
        $this->assignconfig('ids',$ids);
        $this->assignconfig('detailList', $detailList);
        $this->assignconfig('mainInfos', $mainInfos);
        return $this->view->fetch();
    }

    /**
    * 打印部件明细(按长度排序)
    */
    public function bjmxPrint($ids = null)
    {
        // dump($ids);
        $row = $this->model->alias("cdbm")
            ->where("cdbm.CDBM_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        $cdRow = $this->cdModel->where("CDM_Num", $row['CDM_Num'])->find();
        
        $list = $this->model->alias("cdbm")
        ->join([$this->technology_ex."cdiagrambuy"=>"cdb"], "cdb.CDBM_Num=cdbm.CDBM_Num")
        ->join([$this->technology_ex."compactdiagramsect"=>"cds"], "cds.CDM_Num=cdbm.CDM_Num and cds.CDS_Name = cdb.CDB_SectName", "left")
        ->join([$this->technology_ex."compactdiagram"=>"cd"], "cds.CDS_ID=cd.CDS_ID")
        ->field("cds.CDS_Name, 
            case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when dtmd_sstuff='角钢' then 
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
            when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
            REPLACE(DtMD_sSpecification,'-','')
            else 0
            end) as UNSIGNED) bh,
        cd.*, cd.DtMD_SumWeight*cdb.CDB_Count as WeightSum, cd.DtMD_Count*cdb.CDB_Count as sl")
        ->where("cdbm.CDBM_Num", $ids)
        ->order("clsort,DtMD_sMaterial,bh,DtMD_iLength desc")
        ->select();

        $detailList = [];
        $mainInfos = [];
        foreach($list as $v){
            $v = $v->toArray();
            array_push($detailList, $v);
        }

        $mainInfos['CDM_CProject'] = $cdRow["CDM_CProject"];
        $mainInfos['CDM_TypeName'] = $cdRow["CDM_TypeName"];
        $mainInfos['Writer'] = $row["Writer"];
        $mainInfos['Auditor'] = $row["Auditor"];
        $mainInfos['idate']=date('Y-m-d');
        
        $this->assignconfig('ids',$ids);
        $this->assignconfig('detailList', $detailList);
        $this->assignconfig('mainInfos', $mainInfos);
        return $this->view->fetch();
    }

    /**
    * 打印部件明细(按编号排序)
    */
    public function bjmxPrintByBh($ids = null)
    {
        // dump($ids);
        $row = $this->model->alias("cdbm")
            ->where("cdbm.CDBM_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        $cdRow = $this->cdModel->where("CDM_Num", $row['CDM_Num'])->find();
        
        $list = $this->model->alias("cdbm")
        ->join([$this->technology_ex."cdiagrambuy"=>"cdb"], "cdb.CDBM_Num=cdbm.CDBM_Num")
        ->join([$this->technology_ex."compactdiagramsect"=>"cds"], "cds.CDM_Num=cdbm.CDM_Num and cds.CDS_Name = cdb.CDB_SectName", "left")
        ->join([$this->technology_ex."compactdiagram"=>"cd"], "cds.CDS_ID=cd.CDS_ID")
        ->field("cds.CDS_Name, 
            case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when dtmd_sstuff='角钢' then 
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
            when (dtmd_sstuff='钢板' or dtmd_sstuff='法兰') then 
            REPLACE(DtMD_sSpecification,'-','')
            else 0
            end) as UNSIGNED) bh,
            case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
        SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*1000000000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*1000000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
        when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*1000000000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*1000000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
        when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*1000000000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*1000000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
        when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*1000000000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
        when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*1000000000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
        else 0 end bjbhn,
        cd.*, cd.DtMD_SumWeight*cdb.CDB_Count as WeightSum, cd.DtMD_Count*cdb.CDB_Count as sl,CAST(cdb.CDB_SectName as UNSIGNED) AS number,cdb.CDB_SectName")
        ->where("cdbm.CDBM_Num", $ids)
        ->order("clsort,DtMD_sMaterial,bh,number,cdb.CDB_SectName,bjbhn asc")
        ->select();

        $detailList = [];
        $mainInfos = [];
        foreach($list as $v){
            $v = $v->toArray();
            array_push($detailList, $v);
        }

        $mainInfos['CDM_CProject'] = $cdRow["CDM_CProject"];
        $mainInfos['CDM_TypeName'] = $cdRow["CDM_TypeName"];
        $mainInfos['Writer'] = $row["Writer"];
        $mainInfos['Auditor'] = $row["Auditor"];
        $mainInfos['idate']=date('Y-m-d');
        
        $this->assignconfig('ids',$ids);
        $this->assignconfig('detailList', $detailList);
        $this->assignconfig('mainInfos', $mainInfos);
        return $this->view->fetch();
    }
    
    public function export($ids=null)
    {
        $row = $this->model->alias("cdbm")
            ->join([$this->technology_ex."compactdiagrammain"=>"cdm"],"cdbm.CDM_Num = cdm.CDM_Num")
            ->where("cdbm.CDBM_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }

        $title = $row["CDM_TypeName"];
        $list = $this->model->alias("cdbm")
        ->join([$this->technology_ex."cdiagrambuy"=>"cdb"], "cdb.CDBM_Num=cdbm.CDBM_Num")
        ->join([$this->technology_ex."compactdiagramsect"=>"cds"], "cds.CDM_Num=cdbm.CDM_Num and cds.CDS_Name = cdb.CDB_SectName", "left")
        ->join([$this->technology_ex."compactdiagram"=>"cd"], "cds.CDS_ID=cd.CDS_ID")
        ->field("cds.CDS_Name, 
            case dtmd_sstuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when dtmd_sstuff='角钢' then 
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
            when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
            REPLACE(DtMD_sSpecification,'-','')
            else 0
            end) as UNSIGNED) bh,
        cd.*, cd.DtMD_SumWeight*cdb.CDB_Count as WeightSum, cd.DtMD_Count*cdb.CDB_Count as sl")
        ->where("cdbm.CDBM_Num", $ids)
        ->order("clsort,DtMD_sMaterial,bh,DtMD_iLength desc")
        ->select();
        // $list = $this->model->alias("cdbm")
        // ->join(["cdiagrambuy"=>"cdb"], "cdb.CDBM_Num=cdbm.CDBM_Num")
        // ->join(["compactdiagramsect"=>"cds"], "cds.CDM_Num=cdbm.CDM_Num and cds.CDS_Name = cdb.CDB_SectName", "left")
        // ->join(["compactdiagram"=>"cd"], "cds.CDS_ID=cd.CDS_ID")
        // ->field("cds.CDS_Name,cd.*, cd.DtMD_SumWeight*cdb.CDB_Count as WeightSum, cd.DtMD_Count*cdb.CDB_Count as sl,
        // DtMD_sPartsID bjbh,
        // case when LOCATE('-',DtMD_sPartsID)>0 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed)
        // when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
        // when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
        // else 0 end 	bjbhn")
        // ->where("cdbm.CDBM_Num", $ids)
        // ->order("cds.CDS_Name,bjbhn")
        // ->select();
        $end_list = [];
        foreach($list as $k=>$v){
            $end_list[$k] = $v->toArray();
            $end_list[$k]["id"] = $k+1;
        }
        
        $header = [
            ['序号', 'id'],
            ['部件号', 'DtMD_sPartsID'],
            ['材质', 'DtMD_sMaterial'],
            ['规格', 'DtMD_sSpecification'],
            ['长度', 'DtMD_iLength'],
            ['宽度', 'DtMD_fWidth'],
            ['总数量', 'sl'],
            ['总重量', 'WeightSum']
        ];

        return Excel::exportData($end_list, $header, $title . date('Ymd'));
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $update = [
            "Auditor" => $this->admin["nickname"],
            "AuditDate" => date("Y-m-d H:i:s")
        ];
        $result = $this->model->update($update,["CDBM_Num"=>$num]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $update = [
            "Auditor" => '',
            "AuditDate" => "0000-00-00 00:00:00"
        ];
        $result = $this->model->update($update,["CDBM_Num"=>$num]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function arrange($ids = null)
    {
        $cdbmList = explode(",",$ids);
        $getCDiagramViewModel = new \app\admin\model\chain\lofting\GetCDiagramView([],$this->technology_ex);
        $list = $getCDiagramViewModel->field("CDBM_Num,CDM_TypeName,DtMD_sStuff,DtMD_sMaterial,DtMD_sSpecification,sum(weight) as weight,case DtMD_sStuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,
        cast((case 
        when DtMD_sStuff='角钢' then 
        SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
        SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
        when (DtMD_sStuff='钢板' or DtMD_sStuff='钢管') then 
        REPLACE(DtMD_sSpecification,'-','')
        else 0
        end) as UNSIGNED) bh")->where("CDBM_Num","IN",$cdbmList)->order("clsort,DtMD_sStuff,DtMD_sMaterial,bh,DtMD_sSpecification")->group("CDBM_Num,DtMD_sStuff,DtMD_sMaterial,DtMD_sSpecification")->select();

        $data = $field = [];
        foreach($list as $k=>$v){
            $field[$v["CDBM_Num"]] = $v["CDM_TypeName"];
            $key = $v["DtMD_sStuff"].','.$v["DtMD_sMaterial"].','.$v["DtMD_sSpecification"];
            isset($data[$key])?"":$data[$key] = [
                "DtMD_sStuff" => $v["DtMD_sStuff"],
                "DtMD_sMaterial" => $v["DtMD_sMaterial"],
                "DtMD_sSpecification" => $v["DtMD_sSpecification"],
                "list" => []
            ];
            $data[$key]["list"][$v["CDBM_Num"]] = $v["weight"];
        }
        ksort($field);
        $content = ["DtMD_sStuff","DtMD_sMaterial","DtMD_sSpecification"];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $styleArray = [
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER, //水平居中
                'vertical' => Alignment::VERTICAL_CENTER, //垂直居中
            ],
        ];

        $l_count = 4;
        $cellsData = [
            ["A1", "材料"],
            ["B1", "材质"],
            ["C1", "规格"],
        ];
        foreach($field as $v){
            $cellsData[] = [(Coordinate::stringFromColumnIndex($l_count)).'1',$v];
            $l_count++;
        }
        $cellsData[] = [(Coordinate::stringFromColumnIndex($l_count)).'1',"合计"];

        $h_count = 2;
        foreach($data as $v){
            $l_count = 1;
            foreach($content as $cv){
                $cellsData[] = [(Coordinate::stringFromColumnIndex($l_count)).$h_count,$v[$cv]];
                $l_count++;
            }
            foreach($field as $fk=>$fv){
                $cellsData[] = [(Coordinate::stringFromColumnIndex($l_count)).$h_count,$v["list"][$fk]??''];
                $l_count++;
            }
            $cellsData[] = [(Coordinate::stringFromColumnIndex($l_count)).$h_count,"=sum(D".$h_count.":".(Coordinate::stringFromColumnIndex($l_count-1)).$h_count.")"];
            $h_count++;
        }
        $cellsData[] = ["B".$h_count,"总计"];
        $end_count = 4;
        foreach(array_values($field) as $fk=>$fv){
            $cellsData[] = [(Coordinate::stringFromColumnIndex($end_count+$fk)).$h_count,"=sum(".(Coordinate::stringFromColumnIndex($end_count+$fk))."1:".(Coordinate::stringFromColumnIndex($end_count+$fk)).($h_count-1).")"];
        }
        foreach($cellsData as $v){
            $sheet->setCellValue($v[0],$v[1]);
        }
        $fileName = '汇总'. date('Ymd');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx'); //按照指定格式生成Excel文件
        $this->excelBrowserExport($fileName, 'Xlsx');
        $writer->save('php://output');
    }

    /**
	 * 输出到浏览器(需要设置header头)
	 * @param string $fileName 文件名
	 * @param string $fileType 文件类型
	 */
	public function excelBrowserExport($fileName, $fileType) {

	    //文件名称校验
	    if(!$fileName) {
	        trigger_error('文件名不能为空', E_USER_ERROR);
	    }

	    //Excel文件类型校验
	    $type = ['Excel2007', 'Xlsx', 'Excel5', 'xls'];
	    if(!in_array($fileType, $type)) {
	        trigger_error('未知文件类型', E_USER_ERROR);
	    }

	    if($fileType == 'Excel2007' || $fileType == 'Xlsx') {
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
	        header('Cache-Control: max-age=0');
	    } else { //Excel5
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment;filename="'.$fileName.'.xls"');
	        header('Cache-Control: max-age=0');
	    }
	}
}
