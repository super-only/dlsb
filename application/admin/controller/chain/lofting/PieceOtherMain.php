<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\UnionProduceTaskView;
use app\common\controller\Backend;
use think\Db;
use Exception;

/**
 * 等离子/钢印
 *
 * @icon fa fa-circle-o
 */
class PieceOtherMain extends Backend
{
    
    /**
     * PieceOtherMain模型对象
     * @var \app\admin\model\chain\lofting\PieceOtherMain
     */
    protected $model = null,$operatorModel=null,$detailModel=null,$admin='',$machineArr,$orderArr,$contentModel;
    protected $noNeedLogin = ["getPieceList","searchContent","selectPeople"];
    protected $content = [1=>"普通",2=>"电焊",3=>"弯曲",4=>"切角",5=>"铲背",6=>"清根",7=>"打扁",8=>"开合角",9=>"钻孔",10=>'割豁',11=>"制弯打扁开合角",15=>"等离子割板",16=>"板件钢印",17=>"角钢钢印"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\lofting\PieceOtherMain;
        $this->operatorModel = new \app\admin\model\chain\lofting\PieceOtherOperator;
        $this->detailModel = new \app\admin\model\chain\lofting\PieceOtherDetail;
        $this->contentModel = new \app\admin\model\chain\lofting\PieceContent;
        $this->admin = \think\Session::get('admin');
        $this->machineArr = $this->machineList(["type"=>["=",2]])[0];
        $this->machineArr[15] = "等离子割板";
        $this->orderArr = [
            "TTGX01" => "下料",
            "TTGX02" => "制孔",
            "TTGX04" => "装配/组对/组装",
            "TTGX05" => "焊接",
            "TTGX06" => "镀锌",
            "TTGX07" => "制弯"
        ];
        $this->view->assign("orderArr",$this->orderArr);
        $this->assignconfig("orderArr",$this->orderArr);
        $this->view->assign("machineArr",$this->machineArr);
        $this->assignconfig("machine_arr",$this->machineArr);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("pm")
                // ->join(["piece_operator"=>"po"],"po.piece_main_id=pm.id")
                ->join(["piece_other_detail"=>"pd"],"pm.id=pd.piece_id","left")
                ->field("pm.*,pt_num,(case when auditor='' then 0 else 1 end) as status")
                ->where($where)
                ->order("status asc")
                ->order($sort, $order)
                ->group("pm.id")
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }
    
    public function savePiece($ids=0)
    {
        $main_data = $this->request->post("main_data");
        if($main_data) $main_data = json_decode($main_data,true);
        else return json(["code"=>0,"msg"=>"主信息内容必须全写！"]);
        $main_news = $piece_news = $pt_num_list = $operator_list = [];
        foreach($main_data as $v){
            if($v["value"]) $main_news[$v["name"]] = $v["value"];
        }
        if(round(($main_news["zg_amount"]??0),2) and round(($main_news["zg_hours"]??0),2)) return json(["code"=>0,"msg"=>"杂工工资和杂工时间不能同时填写"]);
        if(!($main_news["device"]??false)) return json(["code"=>0,"msg"=>"必须选择设备号"]);
        if(!($main_news["order"]??false)) return json(["code"=>0,"msg"=>"必须填写工序"]);
        if($main_news["device"]==15){
            if(!($main_news["operator"]??false)) return json(["code"=>0,"msg"=>"必须填写操作人员"]);
        }else{
            $main_news["operator"] = '';
            $main_news["zg_amount"] = $main_news["zg_hours"] = 0;
        }
        $operator_list = $main_news["operator"]?json_decode($main_news["operator"],true):[];
        unset($main_news["operator"]);
        $main_news["writer"] = $this->admin["nickname"];
        $table_data = $this->request->post("table_data");
        $table_data = $table_data?json_decode($table_data,true):[];
        if(!empty($table_data)){
            foreach($table_data as $v){
                foreach($v as $vv){
                    if($vv["sum_weight"]<=0) continue;
                    $pt_num_list[$vv["pt_num"]] = $vv["pt_num"];
                    // $hole_number = $vv["hole_number"]?$vv["hole_number"]:0;
                    $sum_weight = $vv["sum_weight"]?$vv["sum_weight"]:0;

                    $content = [
                        "id" => $vv["piece_detail_id"],
                        // "piece_id" => $vv["id"],
                        "pt_num" => $vv["pt_num"],
                        "stuff" => $vv["stuff"],
                        "specification" => $vv["specification"],
                        "material" => $vv["material"],
                        "workmanship" => $main_news["device"],
                        // "piece_content_id" => $piece_mian_id,
                        "sum_weight" => $sum_weight,
                        // "hole_number" => $hole_number,
                    ];
                    if(!$vv["piece_detail_id"]) unset($content["id"]);
                    $piece_news[$vv["id"]] = $content;
                }
            }
            $unionModel = (new UnionProduceTaskView());
            // $produce_task_model = (new ProduceTask());
            $pt_state = $unionModel->field("PT_Num,flag")->where([
                "PT_Num" => ["IN",$pt_num_list],
                "flag" => ["NOT IN",[2,3]]
            ])->find();
            if($pt_state) return json(["code"=>0,"msg"=>"生产任务单号为".$pt_state["PT_Num"]."的单子无法计件"]);
        }
        $del_list = [];
        $result = $delresult = false;
        Db::startTrans();
        try {
            if($ids){
                $this->model->save($main_news,["id"=>$ids]);
                $piece_mian_id = $ids;
            }else $piece_mian_id = $this->model->insertGetId($main_news);
            foreach($operator_list as $k=>$v){
                unset($operator_list[$k]["title"]);
                $operator_list[$k]["piece_main_id"] = $piece_mian_id;
            }
            $this->operatorModel->where("piece_main_id",$piece_mian_id)->delete();
            $this->operatorModel->allowField(true)->saveAll($operator_list);
            if(!empty($piece_news)){
                foreach($piece_news as $k=>$v){
                    $piece_news[$k]["piece_id"] = $piece_mian_id;
                }
                $result = $this->detailModel->allowField(true)->saveAll($piece_news);
            }
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($piece_mian_id) {
            return json(["code"=>1,"msg"=>"保存成功","data"=>$piece_mian_id]);
        } else {
            return json(["code"=>0,"msg"=>__('No rows were updated')]);
        }
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $sj_operator = [];
        $operatorList = $this->operatorModel->where("piece_main_id",$ids)->order("operator asc")->column("operator as title,operator,hours");
        foreach($operatorList as $v){
            $sj_operator[] = $v["operator"]."：".$v["hours"];
        }
        $row["operator"] = json_encode($operatorList);
        $row["sj_operator"] = implode(",",$sj_operator);
        //1审核 0未审
        $flag = $row["auditor"]?1:0;
        $row["flag"] = $flag;
        $list = $this->detailModel->field("id as piece_detail_id,piece_id,pt_num,stuff,specification,material,concat(stuff,specification,material) as id,sum_weight,hole_number,cast((case 
        when stuff='角钢' then 
        SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
        SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
        when (stuff='钢板' or stuff='钢管') then 
        REPLACE(specification,'-','')
        else 0
        end) as UNSIGNED) as order_2")->where("piece_id",$ids)->order("stuff,order_2,specification,material")->select();
        $edit_list = [];
        foreach($list as $k=>$v){
            $edit_list[$v["pt_num"]][] = $v->toArray();
        }
        $ptList = array_keys($edit_list);
        $tower_list = (new UnionProduceTaskView())->alias("pt")
            ->join(["taskdetail"=>"td"],"pt.TD_ID = td.TD_ID")
            ->field("PT_Num,CONCAT(TD_TypeName,'(',PT_Num,')') as TD_TypeName")
            ->where("PT_Num","IN",$ptList)->select();
        if($tower_list) $tower_list = collection($tower_list)->toArray();
        else $tower_list = [];
        $this->view->assign("tower_list",$tower_list);
        $this->view->assign("row", $row);
        $this->assignconfig("flag",$flag);
        $this->assignconfig("ids",$ids);
        $this->assignconfig("pt_list",$ptList);
        $this->assignconfig("list",$edit_list);
        return $this->view->fetch();
    }

    public function getPieceList()
    {
        $num = $this->request->post("num");
        if($num){
            $list = $this->contentModel->field("pt_num,stuff,specification,material,concat(stuff,specification,material) as id,cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) as order_2,0 as sum_weight,0 as hole_number,0 as piece_detail_id")->where("pt_num",$num)->group("stuff,specification,material")->order("stuff,order_2,specification,material")->select();
            if($list) $list = collection($list)->toArray();
            else return json(["code"=>0,"msg"=>"有误，稍后重试"]);
            $search_list = [
                "stuff" => [],
                "specification" => [],
                "material" => [],
                "workmanship" => []
            ];
            $content = $this->content;
            foreach($list as $k=>$v){
                $search_list["stuff"][$v["stuff"]] = $v["stuff"];
                $search_list["specification"][$v["specification"]] = $v["specification"];
                $search_list["material"][$v["material"]] = $v["material"];
            }
            $search_bulid = [];
            foreach($search_list as $k=>$v){
                $search_bulid[$k] = build_select($k, (empty($v)?[""=>"请输入"]:([""=>"请输入"]+$v)), '', ['class'=>'form-control selectpicker',"autocomplete"=>"off"]);
            }
            return json(["code"=>1,"data"=>["list"=>$list,"search"=>array_values($search_bulid)]]);
        }
        return json(["code"=>0,"msg"=>"有误，稍后重试"]);
    }

    public function searchContent()
    {
        list($pt_num,$stuff,$specification,$material) = array_values($this->request->post());
        if(!$pt_num) return json(["code"=>0,"msg"=>"有误，稍后重试"]);
        $where = ["PT_Num"=>["=",$pt_num]];
        if($stuff) $where["stuff"] = ["=",$stuff];
        if($specification) $where["specification"] = ["=",$specification];
        if($material) $where["material"] = ["=",$material];
        $list = $this->contentModel->field("pt_num,stuff,specification,material,concat(stuff,specification,material) as id,cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) as order_2,0 as sum_weight,0 as hole_number,0 as piece_detail_id")->where($where)->group("stuff,specification,material")->order("stuff,order_2,specification,material")->select();
        if($list) $list = collection($list)->toArray();
        else return json(["code"=>0,"msg"=>"有误，稍后重试"]);
        return json(["code"=>1,"data"=>$list]);
    }
    
    public function delDetail($piece_id=0,$pid=0)
    {
        if(!$pid) $this->error("稍等重试");
        $pid = base64_decode($pid);
        if($piece_id){
            $one = $this->model->get($piece_id);
            if($one["auditor"]) $this->error('已审核，删除失败');

            $piece_detail_one = DB::QUERY("select * from piece_other_detail where piece_id=".$piece_id." and concat(stuff,specification,material) = '".$pid."'");
            $idList = array_column($piece_detail_one,'id');
            if($piece_detail_one){
                $result = $this->detailModel->where("id","IN",$idList)->delete();
                if(!$result) $this->error("稍等重试");
            }
        }
        $this->success("成功",null,[1,$pid]);
    }

    public function deletePiece($ids=0)
    {
        $num = $this->request->post("num");
        $num = trim($num,"#");
        if(!$ids or !$num) return json(["code"=>0,"msg"=>"删除失败"]);
        $is_exist_one = $this->detailModel->where([
            "piece_id" => ["=",$ids],
            "pt_num" => ["=",$num]
        ])->find();
        if(!$is_exist_one) return json(["code"=>1,"msg"=>"删除成功"]);
        $content_one = $this->model->get($ids);
        if($content_one["auditor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);
        Db::startTrans();
        try {
            $result = $this->detailModel->where([
                "piece_id" => ["=",$ids],
                "pt_num" => ["=",$num]
            ])->delete();
            $one = $this->detailModel->where("pt_num",$num)->find();
            // if(!$one) (new ProduceTask())->where("pt_num",$num)->update(["flag"=>2]);
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        return json(["code"=>1,"msg"=>"删除成功"]);
    }

    public function selectPeople($machine = null,$ids = 0)
    {
        if(!$machine) $this->error("请先选择机器！");
        if($ids)  $list = $this->operatorModel->where("piece_main_id",$ids)->order("operator asc")->column("operator as title,operator,hours");
        else{
            $list = $this->model->alias("pm")
                ->join(["piece_other_operator"=>"po"],"pm.id=po.piece_main_id","left")
                ->where("pm.device",$machine)
                ->where("pm.record_time",">=",date("Y-m-d 00:00:00",strtotime("-10 days")))
                ->group("po.operator")
                ->order("po.operator")
                ->column("po.operator as title,po.operator,8 as hours");
        }
        $this->assignconfig("list",array_values($list));
        return $this->view->fetch();
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $result = $this->model->save(['auditor'=>$admin,"auditor_time"=>date("Y-m-d H:i:s")],["id"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $result = $this->model->update(['auditor'=>"","auditor_time"=>"0000-00-00 00:00:00"],["id"=>$num]);
        if ($result) {
            return json(["code"=>1,"msg"=>"弃审成功"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

}
