<?php

namespace app\admin\controller\chain\lofting;

use app\common\controller\Backend;
/**
 * 
 *
 * @icon fa fa-circle-o
 */
class MaterialTotal extends Backend
{
    
    /**
     * MaterialReplace模型对象
     * @var \app\admin\model\chain\lofting\MaterialTotal
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\lofting\MaterialReplace;
        $this->detailModel = new \app\admin\model\chain\lofting\MaterialReplaceDetail;
        $deptList = [""=>"请选择"];
        $deptModel = (new \app\admin\model\jichu\jg\Deptdetail())
            ->field("DD_Name")
            ->where(["Valid"=>1])
            ->order(["ParentNum"=>"ASC","DD_Num"=>"ASC"])
            ->select();
        foreach($deptModel as $v){
            $deptList[$v["DD_Name"]] = $v["DD_Name"];
        }
        $this->view->assign("deptList",$deptList);
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->detailModel->alias("mrt")
                ->join(["materialreplace"=>"mr"],"mrt.MR_Num = mr.MR_Num")
                ->join(["taskdetail"=>"td"],"td.TD_ID = mr.TD_ID")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num")
                ->field("mr.MR_Num,mr.WriterDate,mrt.NewLimber,mrt.NewType,mrt.NewWeight,mrt.OldPart,mrt.IM_Class,mrt.NewLength,mrt.NewWidth,mrt.NewCount,mrt.NewClass,mrt.PT_Num,t.t_project,td.TD_TypeName")
                ->where($where)
                ->order($sort, $order)
                ->select();
            $data = [];
            $count = $weight = 0;
            foreach($list as $k=>$v){
                $data[$k] = $v->toArray();
                $data[$k]["NewCount"] = round($v["NewCount"],2);
                $data[$k]["NewWeight"] = round($v["NewWeight"],2);
                $count += $v["NewCount"];
                $weight += $v["NewWeight"];
            }
            $data[] = ["NewLimber"=>"合计","NewCount"=>$count,"NewWeight"=>$weight];
            $result = array("total" => count($list), "rows" => $data);

            return json($result);
        }
        $defaultTime = date("Y-m-d 00:00:00",strtotime("-7 day")).' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        return $this->view->fetch();
    }
}
