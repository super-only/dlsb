<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\CDiagramBuyMain;
use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\model\chain\lofting\Dtmaterialdetail;
use app\admin\model\chain\lofting\Dtsect;
use app\admin\model\chain\lofting\Fytxkdetail;
use app\admin\model\chain\lofting\Fytxksect;
use app\admin\model\chain\lofting\Library;
use app\admin\model\jichu\ch\InventoryMaterial;
use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;

/**
 * 图纸数据
 *
 * @icon fa fa-circle-o
 */
class CompactDiagramMain extends Technology
{
    
    /**
     * CompactDiagramMain模型对象
     * @var \app\admin\model\chain\lofting\CompactDiagramMain
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_xd = \think\Session::get('technology_xd');
        $this->technology_type = \think\Session::get('technology_type');
        $this->model = new \app\admin\model\chain\lofting\CompactDiagramMain([],$this->technology_ex);
        $this->sectModel = new \app\admin\model\chain\lofting\CompactDiagramSect([],$this->technology_ex);
        $this->detailModel = new \app\admin\model\chain\lofting\CompactDiagram([],$this->technology_ex);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
    */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("cdm")
                ->join(["taskDetail"=>"td"],"cdm.TD_ID=td.TD_ID","left")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num","left")
                ->join(["compact"=>"c"],"t.C_Num=c.C_Num","left")
                ->field("(CASE WHEN cdm.TD_ID='0' THEN '未关联' ELSE td.T_Num END) as 'td.T_Num',(CASE WHEN cdm.TD_ID='0' THEN '未关联' ELSE c.PC_Num END) as 'c.PC_Num',(CASE WHEN cdm.TD_ID='0' THEN '未关联' ELSE t.C_Num END) as 't.C_Num',(CASE WHEN cdm.TD_ID='0' THEN '未关联' ELSE t.t_shortproject END) as 't.t_shortproject',cdm.CDM_Num,cdm.TD_ID,cdm.CDM_CProject,CDM_TypeName,CDM_Pressure,cdm.TD_Type,cdm.Writer,cdm.WriteDate,cdm.Auditor,cdm.AuditeDate")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $year = date("Y",time());
                $findOne = $this->model->where("CDM_Num",'LIKE','DT'.$year.'%')->order("CDM_Num DESC")->find();

                if(isset($findOne["CDM_Num"])){
                    $num = substr($findOne["CDM_Num"],6);
                    $num = $year.$this->technology_xd.str_pad((++$num),4,0,STR_PAD_LEFT);
                }else $num = $year.$this->technology_xd.'0001';
                $params["CDM_Num"] = 'DT'.$num;
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('成功！',null,$result["CDM_Num"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = ["writer"=>$this->admin["nickname"],"time"=>date("Y-m-d H:i:s")];
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->alias("cd")
            ->join(["taskdetail"=>"d"],"cd.TD_ID = d.TD_ID","left")
            ->join(["task"=>"t"],"t.T_Num = d.T_Num","left")
            ->field("cd.*,t.C_Num,t.T_Num,t.t_shortproject")
            ->where("cd.CDM_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $sectSaveList = [];
                        
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        if($k=="CDS_ID" and $vv==0) continue;
                        $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $bjhList = [];
                foreach($sectSaveList as $k=>$v){
                    if(isset($bjhList[$v["CDS_Name"]])) return json(["code"=>0,'msg'=>"段号有重复，请进行修改！"]);
                    else $bjhList[$v["CDS_Name"]] = $v["CDS_Name"];
                    foreach($v as $kk=>$vv){
                        if($vv=='') unset($sectSaveList[$k][$kk]);
                    }
                    $sectSaveList[$k]["CDM_Num"] = $ids;
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("CDM_Num",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->sectModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $sectArr = [];
        $number = $weight = 0;
        $sectList = $this->sectModel->alias("s")
            ->join([$this->technology_ex."compactdiagram"=>'d'],"s.CDS_ID = d.CDS_ID","left")
            ->field("s.*,count(d.DtMD_Count) as count,sum(d.DtMD_SumWeight) as weight,CAST(s.CDS_Name as UNSIGNED) AS number")
            ->where("s.CDM_Num",$ids)
            ->group("s.CDS_ID")
            ->order("number,s.CDS_Name asc")
            ->select();
        foreach($sectList as $v){
            $v = $v->toArray();
            $v["count"] = $v["count"]?$v["count"]:0;
            $v["weight"] = $v["weight"]?$v["weight"]:0;
            $v["number"] = $v["number"]?$v["number"]:0;
            $sectArr[] = $v;
            $number += $v["number"];
            $weight += $v["weight"];
            
        }
        $count = count($sectArr);
        $tableField = $this->getTableField();
        $this->view->assign("allCount",["count"=>$count,"number"=>$number,"weight"=>$weight]);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("row", $row);
        $this->assignconfig("auditor",$row["Auditor"]);
        $this->view->assign("sectList", $sectArr);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function chooseTower($type=0)
    {
         //设置过滤方法
         $this->request->filter(['strip_tags', 'trim']);
         if ($this->request->isAjax()) {
             //如果发送的来源是Selectpage，则转发到Selectpage
             if ($this->request->request('keyField')) {
                 return $this->selectpage();
             }
             $ini_where = [];
             if(!$type) $ini_where["c.produce_type"] = ["=",$this->technology_type];
             list($where, $sort, $order, $offset, $limit) = $this->buildparams();
             $list = (new \app\admin\model\chain\sale\Compact)->alias("c")
                 ->join(["task"=>"t"],"t.C_Num=c.C_Num")
                 ->join(["taskdetail"=>"d"],"t.T_Num = d.T_Num")
                 ->field("d.TD_ID,d.T_Num,d.T_Num as 'd.T_Num',d.P_Name,d.P_Num,d.TD_TypeName,d.TD_Pressure,c.PC_Num,c.C_Num,c.C_Num as 'c.C_Num',c.C_Project,c.C_Company,c.Customer_Name,c.C_SortProject,c.C_SaleMan")
                 ->where($where)
                 ->where($ini_where)
                 ->order($sort,$order)
                 ->paginate($limit);
 
             $result = array("total" => $list->total(), "rows" => $list->items());
 
             return json($result);
         }
         $this->assignconfig("type",$type);
         return $this->view->fetch();
    }

    /**
     * 段和明细导入
     */
    public function importView($ids=null)
    { 
        if ($this->request->isPost()) {
            $file = $this->request->post("filename");
            $way = $this->request->post("way");
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $data = [];
            $file = ROOT_PATH . DS . 'public' . DS . $file;
            try {
                $data = Excel::import($file, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($data)){
                    $this->error('空数据文件');
                }
                $data = array_slice($data,1);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
			$biZhong = (new InventoryMaterial())->getIMPerWeight();
            $count = 0;
            $sectList = $detailList = [];
            foreach($data as $v){
                if($v[0]=='') continue;
                foreach($v as $pk=>$pv){
                    $v[$pk] = trim($pv);
                }
                $CDS_Name = $DtMD_sPartsID = $DtMD_sStuff = $DtMD_sMaterial = "";
                $CDS_Name = $v[0];
                $sectList[$CDS_Name] = [
                    "CDS_Name" => $CDS_Name,
                    "CDS_Count" => 0,
                    "CDM_Num" => $ids
                ];
                $DtMD_sPartsID = $v[1];
                $DtMD_sMaterial = $v[2]==''?'Q235B':$v[2];
                $v[4] = str_replace("L","∠",$v[4]);
                $v[4] = str_replace("∟","∠",$v[4]);
                $v[4] = str_replace("X","*",$v[4]);
                $v[4] = str_replace("x","*",$v[4]);
                $v[4] = str_replace("Φ","ф",$v[4]);

                foreach([["L","∠"],["∟","∠"],["X","*"],["x","*"],["Φ","ф"]] as $sv){
                    $v[4] = str_replace($sv[0],$sv[1],$v[4]);
                }
                $firstField = mb_substr(trim($v[4]),0,1);


                // $DtMD_sStuff = $v[3];
                $DtMD_sStuff = "";
            
                if($firstField == '∠') $DtMD_sStuff = '角钢';
                elseif($firstField == '-') $DtMD_sStuff = '钢板';
                elseif($firstField == '[') $DtMD_sStuff = '槽钢';
                else{
                    preg_match_all("/\d+\.?\d*/",$v[4],$matches);
                    if(count($matches[0])==2) $DtMD_sStuff = '钢管';
                    else $DtMD_sStuff = '圆钢';
                }
                $DtMD_sStuff = $v[3]?$v[3]:$DtMD_sStuff;
                

				$length = $v[5]?$v[5]*0.001:0;
                $width = $v[6]?$v[6]*0.001:0;

                $area = $DtMD_sStuff!='钢板'?$length:$length*$width*abs($v[4]);
                $DtMD_iTorch = 0;
                $DtMD_fUnitWeight = 0;
                if($DtMD_sStuff=="圆钢"){
                    preg_match_all("/\d+\.?\d*/",$v[4],$matches);
                    $L = isset($matches[0][0])?$matches[0][0]:0;
                    $DtMD_fUnitWeight = round($L*$L*$length*0.00617,2);
                }else if($DtMD_sStuff=="钢管"){
                    preg_match_all("/\d+\.?\d*/",$v[4],$matches);
                    $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                    $r = isset($matches[0][1])?$matches[0][1]/2*0.001:0;
                    if(strpos($v[4],"*")) $r = $R-(2*$r);
                    $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
                }else if($DtMD_sStuff=="格栅板"){
                    $rou = ["G255/40/100W"=>32.1,"G253/40/100W"=>21.3];
                    $rou_one = isset($rou[$v[4]])?$rou[$v[4]]:0;
                    $DtMD_fUnitWeight = round($rou_one * $length*$width,4);
                }else{
                    // preg_match_all("/\d+\.?\d*/",$v[4],$matches);.
                    preg_match_all("/\d+\.?\d*/",$v[4],$matches);
                    $DtMD_iTorch = end($matches[0]);
                    $DtMD_fUnitWeight = (isset($biZhong[$DtMD_sStuff][$v[4]]))?round($biZhong[$DtMD_sStuff][$v[4]] * $area,4):0;
                }
                $DtMD_iTorch = $v[7]?$v[7]:$DtMD_iTorch;
                $detailList[$CDS_Name][] = [
                    'DtMD_sPartsID' => $DtMD_sPartsID,
                    'DtMD_sStuff' => $DtMD_sStuff,
                    'DtMD_sMaterial' => $DtMD_sMaterial,
                    'DtMD_sSpecification' => $v[4]?$v[4]:"",
                    'DtMD_iLength' => $v[5]?$v[5]:0,
                    'DtMD_fWidth' => $v[6]?$v[6]:0,
                    'DtMD_iTorch' => $DtMD_iTorch,
                    'DtMD_Count' => $v[8]?$v[8]:0,
                    'DtMD_fUnitWeight' => $DtMD_fUnitWeight,
                    'DtMD_fDisUnitWeight' => $DtMD_fUnitWeight*$v[8],
                    'DtMD_SumWeight' => $DtMD_fUnitWeight*$v[8],
                    'type' => (isset($v[12]) and $v[12])?$v[12]:"",
                    'DtMD_sRemark' => (isset($v[22]) and $v[22])?$v[22]:""
                ];
            }
            $where = [
                "CDM_Num" => ["=",$ids],
                "CDS_Name" => ["IN",array_keys($sectList)]
            ];
            $isE = $this->sectModel->where($where)->select();
            $result = false;
            Db::startTrans();
            try {
                $sect_id = [];
                foreach($isE as $v){
                    $sect_id[] = $v["CDS_ID"];
                    if($way == "cover") $v->delete();
                    else{
                        $sectList[$v["CDS_Name"]]["CDS_ID"] = $v["CDS_ID"];
                    }
                }
                if(!empty($sect_id) and $way=="cover") $this->detailModel->where("CDS_ID","in",$sect_id)->delete();
                $result = $this->sectModel->allowField(true)->saveAll($sectList);
                $contect = $saveDetailList = [];
                foreach($result as $v){
                    $contect[$v["CDS_Name"]] = $v["CDS_ID"];
                }
                foreach($detailList as $k=>$v){
                    $count += count($v);
                    foreach($v as $vv){
                        $saveDetailList[] = array_merge($vv,["CDS_ID"=>$contect[$k]]);
                    }
                }
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveDetailList);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success("成功导入".$count."条记录！");
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        return $this->view->fetch();
        
    }

    public function downField($ids=null)
    {
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function downFieldList($type="tower")
    {
        $params = $this->request->post();
        $where = [];
        $list = [];
        if($type=="tower"){
            foreach($params as $k=>$v){
                if($k == "typeName") $where["DtM_sTypeName"] = ["LIKE","%".$v."%"];
                if($k == "pressure") $where["DtM_sPressure"] = ["LIKE","%".$v."%"];
            }
            $list = (new Library([],$this->technology_ex))->field("DtM_iID_PK,DtM_sPictureNum,DtM_sPressure,DtM_sTypeName")->order("DtM_iID_PK DESC")->where($where)->limit(50)->select();
        }else if($type=="project"){
            foreach($params as $k=>$v){
                if($k == "typeName") $where["dt.DtM_sTypeName"] = ["LIKE","%".$v."%"];
                if($k == "pressure") $where["dt.DtM_sPressure"] = ["LIKE","%".$v."%"];
            }
            $list = (new Dtmaterial([],$this->technology_ex))->alias("dt")
                ->join(["taskdetail"=>"td"],"dt.TD_ID=td.TD_ID")
                ->field("dt.TD_ID,dt.DtM_iID_PK,dt.DtM_sProject,dt.DtM_sPressure,td.T_Num,dt.DtM_sTypeName")
                ->where($where)
                ->order("dt.DtM_iID_PK DESC")
                ->limit(50)
                ->select();
        }else{
            foreach($params as $k=>$v){
                if($k == "ids") $where["CDM_Num"] = ["<>",$v];
                if($k == "typeName") $where["CDM_TypeName"] = ["LIKE","%".$v."%"];
                if($k == "pressure") $where["CDM_Pressure"] = ["LIKE","%".$v."%"];
            }
            $list = $this->model->field("CDM_Num,CDM_CProject,CDM_TypeName,CDM_Pressure")->order("CDM_Num DESC")->where($where)->limit(50)->select();
        }
        $data = [];
        foreach($list as $k=>$v){
            $data[$k] = $v->toArray();
            $data[$k]["type"] = $type;
        }
        return json(["code"=>1,"data"=>$data]);
    }

    public function downDetailContent()
    {
        list($type,$num) = array_values($this->request->post());
        $result = ["data"=>[],"code"=>0];
        if($num){
            $list = [];
            if($type == "drawing"){
                $row = $this->sectModel->field("CDS_ID,CDS_Name,CAST(CDS_Name AS UNSIGNED) as number,'".$type."' as type")->where("CDM_Num",$num)->order("number,CDS_Name ASC")->select();
            }else{
                $model = (new Fytxksect([],$this->technology_ex));
                if($type=="project") $model = new Dtsect([],$this->technology_ex);
                $row = $model->field("DtS_ID_PK,DtS_Name,CAST(DtS_Name AS UNSIGNED) as number,'".$type."' as type")->where("DtM_iID_FK",$num)->order("number ASC")->select();
            }
            $result = ["data"=>objectToArray($row),"code"=>1];;
        }
        
        return json($result);
    }

    public function copySect($ids='')
    {
        list($params,$way) = array_values($this->request->post());
        $params = json_decode(str_replace('&quot;','"',$params), true);
        if($params){
            $DtS_NameList = $delList = $addList = [];
            $type = "";
            foreach($params as $k=>$v){
                $type = $v["type"];
                unset($params[$k][0]);
                if($type == "drawing"){
                    $DtS_NameList[] = $v["CDS_Name"];
                    $addList[$v["CDS_ID"]] = ["CDS_Name"=>$v["CDS_Name"],"CDM_Num"=>$ids];
                }else{
                    $DtS_NameList[] = $v["DtS_Name"];
                    $addList[$v["DtS_ID_PK"]] = ["CDS_Name"=>$v["DtS_Name"],"CDM_Num"=>$ids];
                }
            }

            $list = $this->sectModel->field("CDS_ID,CDS_Name")->where("CDM_Num",$ids)->select();
            Db::startTrans();
            try {
                foreach($list as $v){
                    if(in_array($v["CDS_Name"],$DtS_NameList)){
                        // if($way == "cover") $v->delete();
                        $delList[$v["CDS_Name"]] = $v["CDS_ID"];
                    }
                }
                if($way == "cover") $result = $this->detailModel->where("CDS_ID","in",array_values($delList))->delete();
                foreach($addList as $k=>$v){
                    if(isset($delList[$v["CDS_Name"]])) $addList[$k]["CDS_ID"] = $delList[$v["CDS_Name"]];
                }
                // pri($addList);die;
                $sectResult = $this->sectModel->allowField(true)->saveAll(array_values($addList));
                $idsList = $detailArr = [];
                foreach($sectResult as $v){
                    $idsList[$v["CDS_Name"]] = $v["CDS_ID"];
                }
                if($type == "drawing") $detailList = $this->detailModel->field("DtMD_sPartsID,DtMD_sStuff,DtMD_sMaterial,DtMD_sSpecification,DtMD_iLength,ifnull(DtMD_fWidth,0),DtMD_fUnitWeight,DtMD_Count,DtMD_fDisUnitWeight,DtMD_sRemark,DtMD_SumWeight,CDS_ID")->where("CDS_ID","in",array_keys($addList))->select();
                else if($type == "tower") $detailList = (new Fytxkdetail([],$this->technology_ex))->field("DtMD_sPartsID,DtMD_sStuff,DtMD_sMaterial,DtMD_sSpecification,DtMD_iLength,ifnull(DtMD_fWidth,0),DtMD_fUnitWeight,DtMD_iUnitCount as DtMD_Count,(DtMD_iUnitCount*DtMD_fUnitWeight) as DtMD_fDisUnitWeight,DtMD_sRemark,(DtMD_iUnitCount*DtMD_fUnitWeight) as DtMD_SumWeight,DtMD_iSectID_FK as CDS_ID")->where("DtMD_iSectID_FK","in",array_keys($addList))->select();
                else $detailList = (new Dtmaterialdetail([],$this->technology_ex))->field("DtMD_sPartsID,DtMD_sStuff,DtMD_sMaterial,DtMD_sSpecification,DtMD_iLength,,ifnull(DtMD_fWidth,0),DtMD_fUnitWeight,DtMD_iUnitCount as DtMD_Count,(DtMD_iUnitCount*DtMD_fUnitWeight) as DtMD_fDisUnitWeight,DtMD_sRemark,(DtMD_iUnitCount*DtMD_fUnitWeight) as DtMD_SumWeight,DtMD_iSectID_FK as CDS_ID")->where("DtMD_iSectID_FK","in",array_keys($addList))->select();
                foreach($detailList as $k=>$v){
                    $detailArr[$k] = $v->toArray();
                    $detailArr[$k]["CDS_ID"] = $idsList[$addList[$v["CDS_ID"]]["CDS_Name"]];
                }
                
                
                $detailResult = $this->detailModel->allowField(true)->saveAll($detailArr);
                Db::commit();
                return json(["code"=>1,'msg'=>"成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
            
        }
        return json(["code"=>0,'msg'=>"失败"]);
    }

    public function delDetail()
    {
        $num = $this->request->post("num");
        // $detailList = $this->detailModel->field("CD_ID")->where("CDS_ID",$num)->select();
        if($num){
            Db::startTrans();
            try {
                
                // $detailId = [];
                // foreach($detailList as $v){
                //     $detailId[] = $v["CD_ID"];
                // }
                $this->sectModel->where("CDS_ID",$num)->delete();
                // if(!empty($detailId)){
                $this->detailModel->where("CDS_ID",$num)->delete();
                // }
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>1,'msg'=>"删除成功！"]);
    }

    public function detail($ids=null)
    {
        // $row = $this->sectModel->get($ids);
        $row = $this->sectModel->alias("s")->join([$this->technology_ex."compactdiagrammain"=>"cdm"],"s.CDM_Num = cdm.CDM_Num")->where("CDS_ID",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $where = ["CDS_ID"=>["=",$ids]];
        // $DtMD_sPartsID?$where["DtMD_sPartsID"] = ["=",$DtMD_sPartsID]:"";
        // $DtMD_sStuff?$where["DtMD_sStuff"] = ["=",$DtMD_sStuff]:"";
        // $DtMD_sMaterial?$where["DtMD_sMaterial"] = ["=",$DtMD_sMaterial]:"";
        // $DtMD_sSpecification?$where["DtMD_sSpecification"] = ["=",$DtMD_sSpecification]:"";
        if ($this->request->isPost()) {
            $params = $this->request->post("data");
            $symbol = $this->request->post("symbol");
            $symbol = $symbol?$symbol:'*';
            $num = $this->request->post("num");
            $num = $num?$num:1;
            $params = json_decode(str_replace('&quot;','"',$params), true);
            if ($params) {
                $detailSaveList = $slabList = $slabSave = $keyValue = [];
                $key = "";
                $biZhong = (new InventoryMaterial())->getIMPerWeight();
                foreach($params as $v){
                    $key = rtrim($v["name"],"[]");
                    isset($keyValue[$key])?"":$keyValue[$key] = [];
                    $keyValue[$key][] = $v["value"];
                }
                foreach($keyValue as $k=>$v){
                    foreach($v as $kk=>$vv){
                        if($k == "DtMD_Count") $detailSaveList[$kk][$k] = $vv==""?0:round(eval('return '.$vv.$symbol.$num.";"),2);
                        else $detailSaveList[$kk][$k] = trim($vv);
                    }
                }
                $bjhList = [];
                foreach($detailSaveList as $k=>$v){
                    if(isset($bjhList[$v["DtMD_sPartsID"]])) return json(["code"=>0,'msg'=>"部件号有重复，请进行修改！"]);
                    else $bjhList[$v["DtMD_sPartsID"]] = $v["DtMD_sPartsID"];
                    if($v["CD_ID"]==0) unset($detailSaveList[$k]["CD_ID"],$v["CD_ID"]);
                    else $slabList[] = $v["CD_ID"];
                    foreach($v as $kk=>$vv){
                        if($vv=='') unset($detailSaveList[$k][$kk]);
                    }

                    $firstField = mb_substr(trim($v['DtMD_sSpecification']),0,1);
                    $DtMD_sStuff = "";
                    
                    if($firstField == '∠') $DtMD_sStuff = '角钢';
                    elseif($firstField == '-') $DtMD_sStuff = '钢板';
                    elseif($firstField == '[') $DtMD_sStuff = '槽钢';
                    elseif($firstField == 'G') $DtMD_sStuff = '格栅板';
                    else{
                        preg_match_all("/\d+\.?\d*/",$v["DtMD_sSpecification"],$matches);
                        if(count($matches[0])>=2) $DtMD_sStuff = '钢管';
                        else $DtMD_sStuff = '圆钢';
                    }
                    $detailSaveList[$k]["DtMD_sStuff"] = $v["DtMD_sStuff"] = $DtMD_sStuff;

                    $length = $v["DtMD_iLength"]?$v["DtMD_iLength"]*0.001:0;
                    $width = $v["DtMD_fWidth"]?$v["DtMD_fWidth"]*0.001:0;
                    $area = $v["DtMD_sStuff"]!='钢板'?$length:$length*$width*abs($v["DtMD_sSpecification"]);

                    $DtMD_fUnitWeight = 0;
                    if($v["DtMD_sStuff"]=="圆钢"){
                        preg_match_all("/\d+\.?\d*/",$v["DtMD_sSpecification"],$matches);
                        $L = isset($matches[0][0])?$matches[0][0]:0;
                        $DtMD_fUnitWeight = round($L*$L*$length*0.00617,2);
                    }else if($v["DtMD_sStuff"]=="钢管"){
                        preg_match_all("/\d+\.?\d*/",$v["DtMD_sSpecification"],$matches);
                        $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                        $r = isset($matches[0][1])?$matches[0][1]/2*0.001:0;
                        if(strpos($v["DtMD_sSpecification"],"*")) $r = $R-(2*$r);
                        $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
                    }else if($v["DtMD_sStuff"]=="格栅板"){
                        $rou = ["G255/40/100W"=>32.1,"G253/40/100W"=>21.3];
                        $rou_one = isset($rou[$v["DtMD_sSpecification"]])?$rou[$v["DtMD_sSpecification"]]:0;
                        $DtMD_fUnitWeight = round($rou_one * $length*$width,4);
                    }else{
                        $DtMD_fUnitWeight = (isset($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]]))?round($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]] * $area,4):0;
                    }
                    // $DtMD_fUnitWeight = (isset($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]]))?round($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]] * $area,4):0;
                    $detailSaveList[$k]["DtMD_fUnitWeight"] = $DtMD_fUnitWeight;
                    $detailSaveList[$k]["DtMD_fDisUnitWeight"] = round($DtMD_fUnitWeight*$v["DtMD_Count"],2);
                    $detailSaveList[$k]["DtMD_SumWeight"] = round($DtMD_fUnitWeight*$v["DtMD_Count"],2);
                    $detailSaveList[$k]["CDS_ID"] = $ids;
                }
                Db::startTrans();
                try {
                    $result = $this->detailModel->allowField(true)->saveAll($detailSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (PDOException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (Exception $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                }
                if ($result) {
                    return json(["code"=>1,'msg'=>"保存成功"]);
                } else {
                    return json(["code"=>0,'msg'=>"保存失败"]);
                }
            }
            return json(["code"=>0,'msg'=>"有误，请刷新后重试"]);
        }
        $CDS_Name = $row["CDS_Name"];
        $CDM_Num = $row["CDM_Num"];
        $sectList = $this->sectModel->field("CDS_ID,CDS_Name,CAST(CDS_Name AS SIGNED) as number")->where("CDM_Num",$CDM_Num)->order("number")->select();
        $sectArr = $field = $detailList = [];
        foreach($sectList as $v){
            $sectArr[$v["CDS_ID"]] = $v["CDS_Name"];
        }
        
        $tableField = $this->getDetailField();
        foreach($tableField as $v){
            $field[] = $v[1];
        }
        $detailList = $this->detailModel->field(implode(",",$field).",CDS_ID,CAST(DtMD_sPartsID AS SIGNED) as number,case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
        SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
        when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
        when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
        when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
        when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
        else 0 end bjbhn")->where($where)->order("bjbhn,number,DtMD_sPartsID ASC")->select();
        $number = $weight = $knumber = 0;
        $detailArr = $detailName = [];
        $isJudge = ["DtMD_fWidth","DtMD_iTorch"];
        foreach($detailList as $k=>$v){
            $detailArr[$k] = $v->toArray();
            foreach($isJudge as $vv){
                isset($v[$vv])?($detailArr[$k][$vv] = $v[$vv]?$v[$vv]:""):"";
            }
            $detailArr[$k]["is_int"] = 0;
            if($detailArr[$k]["DtMD_Count"] and in_array($v["CDS_ID"]."-".$v["DtMD_sPartsID"], $detailName) == FALSE) $detailArr[$k]["is_int"] = 1;
            $number += $detailArr[$k]["DtMD_Count"];
            $weight += $detailArr[$k]["DtMD_SumWeight"];
            // $knumber += $detailArr[$k]["DtMD_iUnitHoleCount"];
            $detailName[] = $v["CDS_ID"]."-".$v["DtMD_sPartsID"];
        }
        $count = count($detailArr);
        $this->view->assign("allCount",["count"=>$count,"number"=>$number,"weight"=>$weight]);
        $this->view->assign("sectArr", $sectArr);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("detailList",$detailArr);
        $this->view->assign("row",$row);
        $this->assignconfig("auditor",$row["Auditor"]);
        // $this->view->assign("ids",$ids);
        $this->assignconfig('CDS_ID',$ids);
        $this->assignconfig('CDS_Name',$CDS_Name);
        return $this->view->fetch();
    }

    //BC级的修改
    public function editDetailAll()
    {
        $params = $this->request->post();
        list($sect_id,$type) = array_values($params);
        if(!$sect_id and !$type) return json(["code"=>0,"msg"=>"有误请重试"]);
        $where = ["CDS_ID" => ["=",$sect_id]];
        if($type == "editB") $where["DtMD_sMaterial"]=["NOT LIKE","%B"];
        else if($type == "editC") $where["DtMD_sMaterial"]=["NOT LIKE","%C"];
        else if($type == "giveupB") $where["DtMD_sMaterial"]=["LIKE","%B"];
        else $where["DtMD_sMaterial"]=["LIKE","%C"];
        $list = $this->detailModel->field("DtMD_sMaterial,CD_ID")->where($where)->select();
        if(!$list) return json(["code"=>0,"msg"=>"没有可以修改的部件号"]);
        $saveList = [];
        foreach($list as $k=>$v){
            if($type == "editB") $field = $v["DtMD_sMaterial"]."B";
            else if($type == "editC") $field = $v["DtMD_sMaterial"]."C";
            else $field = substr($v["DtMD_sMaterial"],0,strlen($v["DtMD_sMaterial"])-1);
            $saveList[] = ["CD_ID"=>$v["CD_ID"],"DtMD_sMaterial"=>$field];
        }
        $count = false;
        Db::startTrans();
        try {
            $count = $this->detailModel->saveAll($saveList);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,'msg'=>$e->getMessage()]);
        }
        if($count){
            return json(["code"=>1,'msg'=>"成功"]);
        }else{
            return json(["code"=>0,"msg"=>"没有可以修改的部件号"]);
        }
    }
    
    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            Db::startTrans();
            try {
                $this->detailModel->where("CD_ID",$num)->delete();
                // $this->slabModel->where("DtMD_ID_PK",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $dhcooperatek_one = (new CDiagramBuyMain([],$this->technology_ex))->where("CDM_Num","in",$ids)->find();
            if($dhcooperatek_one) $this->error("已生成图纸数据配段，无法删除");

            $list = $this->model->where($pk, 'in', $ids)->select();

            $taskDetail = $this->sectModel->field("CDS_ID")->where("CDM_Num","in",$ids)->select();
            $task_detail_id = [];
            
            $count = 0;
            Db::startTrans();
            try {
                foreach($taskDetail as $v){
                    $task_detail_id[] = $v["CDS_ID"];
                    $v->delete();
                }
                $this->detailModel->where("CDS_ID","IN",$task_detail_id)->delete();
                foreach ($list as $v) {
                    $count += $v->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $update = [
            "Auditor" => $this->admin["nickname"],
            "AuditeDate" => date("Y-m-d H:i:s")
        ];
        $result = $this->model->update($update,["CDM_Num"=>$num]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $update = [
            "Auditor" => '',
            "AuditeDate" => "0000-00-00 00:00:00"
        ];
        $result = $this->model->update($update,["CDM_Num"=>$num]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["CDS_ID","CDS_ID","text","","readonly",0,"hidden"],
            ["CDS_Count","CDS_Count","text","","readonly",1,"hidden"],
            ["*段名","CDS_Name","text","","data-rule='required'","",""],
            ["部件数","count","text","","readonly",0,""],
            ["重量(kg)","weight","text","","readonly",0,""]
        ];
        return $list;
    }

    public function getDetailField(){
        $tableField = [
            ["CD_ID","CD_ID","text","","hidden","40",""],
            ["部件号","DtMD_sPartsID","text","required","","60",""],
            ["材质","DtMD_sMaterial","text","","","60",""],
            ["材料","DtMD_sStuff","text","","","40",""],
            ["规格","DtMD_sSpecification","text","","","70",""],
            ["长度(mm)","DtMD_iLength","text","","","50","checked"],
            ["宽度(mm)","DtMD_fWidth","text","","","50","checked"],
            ["厚度(mm)","DtMD_iTorch","text","","","50","checked"],
            ["类型","type","text","","","50","checked"],
            ["单基数量","DtMD_Count","text","required","","40","checked"],
            ["单件重量(kg)","DtMD_fUnitWeight","text","","","60","checked"],
            ["总重量(kg)","DtMD_SumWeight","text","","","50","checked"],
            // ["单件差重","DtMD_fDisUnitWeight","text","","","50","checked"],
            ["备注","DtMD_sRemark","text","","","80","checked"],
            // ["电焊(0/1)","DtMD_iWelding","text","between","","40","checked"],
            // ["制弯(0/1)","DtMD_iFireBending","text","between","","40","checked"],
            // ["切角切肢(0/1)","DtMD_iCuttingAngle","text","between","","40","checked"],
            // ["铲背(0/1)","DtMD_fBackOff","text","","","40","checked"],
            // ["清根(0/1)","DtMD_iBackGouging","text","between","","40","checked"],
            // ["打扁(0/1)","DtMD_DaBian","text","between","","40","checked"],
            // ["开合角(0/1/2)","DtMD_KaiHeJiao","text","three","","40","checked"],
            // ["钻孔(0/1)","DtMD_ZuanKong","text","between","","40","checked"],
            // ["钻孔数(个)","DtMD_iBoreholeCount","text","integer","","40","checked"],
            // ["开槽形式","DtMD_iExcavationCount","text","","","40","checked"],
            // ["打坡口(0/1)","DtMD_iAssembly","text","between","","40","checked"],
            // ["是否专用","DtMD_ISZhuanYong","text","between","","40","checked"],
            // ["类型","DtMD_sType","text","","","60","checked"],
            // ["边数","DtMD_iPerimeter","text","","","60","checked"],
            // ["周长","DtMD_Circle","text","","","60","checked"],
            // ["孔径28~38mm(A)","DtMD_KJ28T32","text","integer","","60","checked"],
            // ["孔径39*50mm(B)","DtMD_KJ33T42","text","integer","","60","checked"],
            // ["孔径50mm以上(C)","DtMD_KJ50UP","text","integer","","60","checked"],
            // ["安装孔直径","DtMD_iAperture","text","","","60","checked"],
            // ["分布圆直径","DtMD_iBending","text","","","60","checked"],
            // ["车工","DtMD_iAftertreatment","text","","","60","checked"],
            // ["压制","DtMD_iPressed","text","","","60","checked"]
        ];
        return $tableField;
    }

    public function getTrueWeight()
    {
        return false;
        $detail_list = $this->detailModel->where("CDS_ID",">=",35337)->order('CD_ID asc')->select();
        if($detail_list) $detail_list = collection($detail_list)->toArray();
        else $detail_list = [];
        $biZhong = (new InventoryMaterial())->getIMPerWeight();
        foreach($detail_list as $k=>$v){
            $length = $v["DtMD_iLength"]?$v["DtMD_iLength"]*0.001:0;
            $width = $v["DtMD_fWidth"]?$v["DtMD_fWidth"]*0.001:0;
            $area = $v["DtMD_sStuff"]!='钢板'?$length:$length*$width*abs($v["DtMD_sSpecification"]);

            $DtMD_fUnitWeight = 0;
            if($v["DtMD_sStuff"]=="圆钢"){
                preg_match_all("/\d+\.?\d*/",$v["DtMD_sSpecification"],$matches);
                $L = isset($matches[0][0])?$matches[0][0]:0;
                $DtMD_fUnitWeight = round($L*$L*$length*0.00617,2);
            }else if($v["DtMD_sStuff"]=="钢管"){
                preg_match_all("/\d+\.?\d*/",$v["DtMD_sSpecification"],$matches);
                $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                $r = isset($matches[0][1])?$matches[0][1]/2*0.001:0;
                if(strpos($v["DtMD_sSpecification"],"*")) $r = $R-(2*$r);
                $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
            }else{
                $DtMD_fUnitWeight = (isset($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]]))?round($biZhong[$v["DtMD_sStuff"]][$v["DtMD_sSpecification"]] * $area,4):0;
            }
            $detail_list[$k]["DtMD_fUnitWeight"] = $DtMD_fUnitWeight;
            $detail_list[$k]["DtMD_fDisUnitWeight"] = round($DtMD_fUnitWeight*$v["DtMD_Count"],2);
            $detail_list[$k]["DtMD_SumWeight"] = round($DtMD_fUnitWeight*$v["DtMD_Count"],2);
        }
        $result = $this->detailModel->allowField(true)->saveAll($detail_list);
        
    }
}
