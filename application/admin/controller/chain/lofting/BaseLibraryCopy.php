<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\jichu\ch\InventoryMaterial;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;

/**
 * 塔型基础库
 *
 * @icon fa fa-circle-o
 */
class BaseLibraryCopy extends Backend
{
    
    /**
     * BaseLibrary模型对象
     * @var \app\admin\model\chain\lofting\BaseLibraryCopy
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\lofting\BaseLibrary;
        $this->sectModel = new \app\admin\model\chain\lofting\BaseLibrarySect;
        $this->detailModel = new \app\admin\model\chain\lofting\BaseLibraryDetail;
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $result = false;
                $params["DtM_sAuditor"] = $this->admin["nickname"];
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('success',null,$result["DtM_iID_PK"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    $result = $row->allowField(true)->save($params);
                    // if(!empty($sectSaveList)) $this->sectModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $this->sectModel->alias("bls")
            ->join(["baselibrarydetail"=>"bld"],"bls.DtS_ID_PK = bld.DtMD_iSectID_FK")
            ->field("case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn,CAST(bls.DtS_Name AS UNSIGNED) AS number_1,DtS_Name,CAST(bld.DtMD_sPartsID AS UNSIGNED) AS number_2,bld.*,(case when DtMD_fWidth=0 then '' else DtMD_fWidth end) as DtMD_fWidth")
            ->where("bls.DtM_iID_FK",$ids)
            ->order("number_1,DtS_Name,bjbhn,number_2,DtMD_sPartsID asc")
            ->select();
        $this->view->assign("row", $row);
        $this->view->assign("list",$list);
        $this->view->assign("tableField",$this->getDetailField());
        $this->assignconfig("ids",$ids);
        $this->assignconfig("task_name",$row["DtM_sTypeName"]);
        return $this->view->fetch();
    }

    /**
     * 段和明细导入
     */
    public function importView($ids=null)
    { 
        if ($this->request->isPost()) {
            $file = $this->request->post("filename");
            $way = $this->request->post("way");
            $suffix = ucfirst(substr($file,strrpos($file,".")+1));
            $data = [];
            $file = ROOT_PATH . DS . 'public' . DS . $file;
            try {
                $data = Excel::import($file, $startRow = 1, $hasImg = false, $suffix, $imageFilePath = null);
                if(empty($data)){
                    $this->error('空数据文件');
                }
                $data = array_slice($data,1);
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
			$biZhong = (new InventoryMaterial())->getIMPerWeight();
            $count = 0;
            $sectList = $detailList = [];
            foreach($data as $v){
                if($v[0]=='') continue;
                $Dts_Name = $DtMD_sPartsID = $DtMD_sStuff = $DtMD_sMaterial = "";
                if(substr($v[0],0,2)=='00') $Dts_Name = 0;
                else{
                    if(strpos($v[0],"-") > 0){
                        $left_h_part = substr($v[0],0,strpos($v[0],"-"));
                        preg_match_all('/(\d+)|([^\d]+)/',$left_h_part,$matches);
                        $Dts_Name = $matches[0][0]??0;
                    }else{
                        $part = (int)$v[0];
                        if(strlen($part)<=2) $Dts_Name = (int)$part;
                        else if(strlen($part)==3) $Dts_Name = substr($part,0,1);
                        else $Dts_Name = substr($part,0,2);
                    }
                }
                if(strlen($Dts_Name)>1){
                    $Dts_Name = substr($Dts_Name,0,1)==0?substr($Dts_Name,1):$Dts_Name;
                }

                // if((strlen($v[0])-strlen(str_replace('-','',$v[0])))>1){
                //     $part_num = substr($v[0],0,strpos($v[0],'-'))*10000000+substr(substr($v[0],strpos($v[0],'-')),0,strpos('-',substr($v[0],strpos($v[0],'-'))))*10000+intval(substr(substr($v[0],strpos($v[0],'-')),strpos('-',substr($v[0],strpos($v[0],'-')))));
                // }else if((strlen($v[0])-strlen(str_replace('-','',$v[0])))==1 and strlen(intval(substr($v[0],strpos($v[0],'-')+1)))==3){
                //     $part_num = substr($v[0],0,strpos($v[0],'-'))*10000000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),0,1)*10000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),1);
                // }else if((strlen($v[0])-strlen(str_replace('-','',$v[0])))==1 and strlen(intval(substr($v[0],strpos($v[0],'-')+1)))==4){
                //     $part_num = substr($v[0],0,strpos($v[0],'-'))*10000000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),0,2)*10000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),2);
                // }else if((strlen($v[0])-strlen(str_replace('-','',$v[0])))==1 and strlen(intval(substr($v[0],strpos($v[0],'-')+1)))<3){
                //     $part_num = substr($v[0],0,strpos($v[0],'-'))*10000000+substr(intval(substr($v[0],strpos($v[0],'-')+1)),2);
                // }else if(strlen(intval($v[0]))==3){
                //     $part_num = substr(intval($v[0]),0,1)*10000000+substr(intval($v[0]),1);
                // }else if(strlen(intval($v[0]))==4){
                //     $part_num = substr(intval($v[0]),0,2)*10000000+substr(intval($v[0]),2);
                // }else{
                //     $part_num = 0;
                // }
                // $Dts_Name = (int)($part_num/10000000);
                
                $sectList[$Dts_Name] = [
                    "DtS_Name" => $Dts_Name,
                    "DtM_iID_FK" => $ids,
                    "DtS_sWriter" => $this->admin["nickname"],
                    "DtS_dWriterTime" => date("Y-m-d H:i:s"),
                    "DtS_dModifyTime" => date("Y-m-d H:i:s")
                ];
                $DtMD_sPartsID = $v[0];
                $DtMD_sMaterial = $v[1]==''?'Q235B':$v[1];
				$v[2] = str_replace("L","∠",$v[2]);
				$v[2] = str_replace("∟","∠",$v[2]);
				$v[2] = str_replace("X","*",$v[2]);
                $v[2] = str_replace("x","*",$v[2]);
				$v[2] = str_replace("Φ","ф",$v[2]);
				$firstField = mb_substr(trim($v[2]),0,1);
				$DtMD_sStuff = "";
                
				if($firstField == '∠') $DtMD_sStuff = '角钢';
				elseif($firstField == '-') $DtMD_sStuff = '钢板';
                elseif($firstField == '[') $DtMD_sStuff = '槽钢';
				else{
                    preg_match_all("/\d+/",$v[2],$matches);
                    if(count($matches[0])>=2) $DtMD_sStuff = '法兰';
                    else $DtMD_sStuff = '圆钢';
                }
				
				$length = $v[3]?$v[3]*0.001:0;
				$width = $v[4]?$v[4]*0.001:0;
                $area = $DtMD_sStuff!='钢板'?$length:$length*$width*abs($v[2]);

                $DtMD_fUnitWeight = 0;
                if($DtMD_sStuff=="圆钢"){
                    // preg_match_all("/\d+/",$v[2],$matches);
                    $L = isset($matches[0][0])?$matches[0][0]:0;
                    $DtMD_fUnitWeight = round($L*$L*0.00617,2);
                }else if($DtMD_sStuff=="法兰"){
                    // preg_match_all("/\d+/",$v[2],$matches);
                    $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                    $r_y = $matches[0][1]??0;
                    $r_y += $matches[0][2]??0;
                    $r = $r_y?$r_y/2*0.001:0;
                    $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
                }else{
                    $DtMD_fUnitWeight = (isset($biZhong[$DtMD_sStuff][$v[2]]))?round($biZhong[$DtMD_sStuff][$v[2]] * $area,4):0;
                }
                $detailList[$Dts_Name][] = [
                    'DtMD_sPartsID' => $DtMD_sPartsID,
                    'DtMD_sStuff' => in_array($DtMD_sStuff,["圆钢","法兰"])?"钢管":$DtMD_sStuff,
                    'DtMD_sMaterial' => $DtMD_sMaterial,
                    'DtMD_sSpecification' => $v[2],
                    'DtMD_iLength' => $v[3]?$v[3]:0,
                    'DtMD_fWidth' => $v[4]?$v[4]:0,
                    'DtMD_iUnitCount' => $v[5],
                    'DtMD_fUnitWeight' => $v[6]==""?$DtMD_fUnitWeight:($DtMD_fUnitWeight?$DtMD_fUnitWeight:round($v[6]/$v[5],2)),
                    'DtMD_iUnitHoleCount' => $v[7],
                    'DtMD_iWelding' => $v[8]==""?0:1,
                    'DtMD_iFireBending' => $v[9]==""?0:1,
                    'DtMD_iCuttingAngle' => $v[10]==""?0:1,
                    'DtMD_fBackOff' => $v[11]==""?0:1,
                    'DtMD_iBackGouging' => $v[12]==""?0:1,
                    'DtMD_DaBian' => $v[13]==""?0:1,
                    'DtMD_KaiHeJiao' => $v[14]==""?0:1,
                    'DtMD_ZuanKong' => $v[15]==""?0:1,
                    'DtMD_sRemark' => $v[16]
                ];
            }
            $where = [
                "DtM_iID_FK" => ["=",$ids],
                "DtS_Name" => ["IN",array_keys($sectList)]
            ];
            $isE = $this->sectModel->where($where)->select();
            $result = false;
            Db::startTrans();
            try {
                $sect_id = [];
                foreach($isE as $v){
                    $sect_id[] = $v["DtS_ID_PK"];
                    if($way == "cover") $v->delete();
                    else{
                        $sectList[$v["DtS_Name"]]["DtS_ID_PK"] = $v["DtS_ID_PK"];
                        
                    }
                }
                if(!empty($sect_id) and $way=="cover") $this->detailModel->where("DtMD_iSectID_FK","in",$sect_id)->delete();
                $result = $this->sectModel->allowField(true)->saveAll($sectList);
                $contect = $saveDetailList = [];
                foreach($result as $v){
                    $contect[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                }
                foreach($detailList as $k=>$v){
                    $count += count($v);
                    foreach($v as $vv){
                        $saveDetailList[] = array_merge($vv,["DtMD_iSectID_FK"=>$contect[$k]]);
                    }
                }
                $detailResult = $this->detailModel->allowField(true)->saveAll($saveDetailList);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success("成功导入".$count."条记录！");
            } else {
                $this->error(__('No rows were updated'));
            }
        }
        return $this->view->fetch();
        
    }

    public function export($ids){
        if(!$ids) return json(["code"=>0,"msg"=>"请稍后重试！"]);
        $one = $this->model->get($ids);
        $title = $one["DtM_sTypeName"];
        $list = $this->sectModel->alias("bls")
            ->join(["baselibrarydetail"=>"bld"],"bls.DtS_ID_PK = bld.DtMD_iSectID_FK")
            ->field("case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn,CAST(bls.DtS_Name AS UNSIGNED) AS number_1,CAST(bld.DtMD_sPartsID AS UNSIGNED) AS number_2,bld.*")
            ->where("bls.DtM_iID_FK",$ids)
            ->order("number_1,DtS_Name,bjbhn,number_2,DtMD_sPartsID asc")
            ->select();
        foreach($list as $k=>$v){
            $list[$k]["DtMD_sSpecification"] = strtr($v["DtMD_sSpecification"],"*","x");
            $list[$k]["DtMD_iWelding"] = $v["DtMD_iWelding"]==1?"√":"";
            $list[$k]["DtMD_iFireBending"] = $v["DtMD_iFireBending"]==1?"√":"";
            $list[$k]["DtMD_iCuttingAngle"] = $v["DtMD_iCuttingAngle"]==1?"√":"";
            $list[$k]["DtMD_fBackOff"] = $v["DtMD_fBackOff"]==1?"√":"";
            $list[$k]["DtMD_iBackGouging"] = $v["DtMD_iBackGouging"]==1?"√":"";
            $list[$k]["DtMD_DaBian"] = $v["DtMD_DaBian"]==1?"√":"";
            $list[$k]["DtMD_KaiHeJiao"] = $v["DtMD_KaiHeJiao"]==1?"√":"";
            $list[$k]["DtMD_ZuanKong"] = $v["DtMD_ZuanKong"]==1?"√":"";
        }
        
        $header = [
            ['零件编号', 'DtMD_sPartsID'],
            ['材质', 'DtMD_sMaterial'],
            ['规格', 'DtMD_sSpecification'],
            ['长度(mm)', 'DtMD_iLength'],
            ['宽度(mm)', 'DtMD_fWidth'],
            ['单基数量', 'DtMD_iUnitCount'],
            ['单基重量', 'DtMD_fUnitWeight'],
            ['孔数', 'DtMD_iUnitHoleCount'],
            ['电焊', 'DtMD_iWelding'],
            ['弯曲', 'DtMD_iFireBending'],
            ['切角', 'DtMD_iCuttingAngle'],
            ['铲背', 'DtMD_fBackOff'],
            ['清根', 'DtMD_iBackGouging'],
            ['打扁', 'DtMD_DaBian'],
            ['开合角', 'DtMD_KaiHeJiao'],
            ['钻孔', 'DtMD_ZuanKong'],
            ['备注', 'DtMD_sRemark']
        ];

        return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
    }

    public function openEdit($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            return json(["code"=>0,"msg"=>__('No Results were found')]);
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("data");
            $params = json_decode($params,true);
            if ($params) {
                $params = $this->preExcludeFields($params);
                $count = 0;
                $keyValue = [];
                foreach($params as $v){
                    if($v["name"]=="DtMD_sPartsID") $count++;
                    $keyValue[$count][$v["name"]] = $v["value"];
                }
                $biZhong = (new InventoryMaterial())->getIMPerWeight();
                $sectList = $detailList = [];
                foreach($keyValue as $v){
                    $Dts_Name = 0;
                    $DtMD_sPartsID = $v['DtMD_sPartsID'];
                    $Dts_Name = $DtMD_sStuff = $DtMD_sMaterial = "";
                    if(substr($DtMD_sPartsID,0,2)=='00'){
                        $Dts_Name = 0;
                    }else{
                        if(strpos($DtMD_sPartsID,"-") > 0){
                            $left_h_part = substr($DtMD_sPartsID,0,strpos($DtMD_sPartsID,"-"));
                            preg_match_all('/(\d+)|([^\d]+)/',$left_h_part,$matches);
                            $Dts_Name = $matches[0][0]??0;
                        }else{
                            $part = (int)$DtMD_sPartsID;
                            if(strlen($part)<=2) $Dts_Name = (int)$part;
                            else if(strlen($part)==3) $Dts_Name = substr($part,0,1);
                            else $Dts_Name = substr($part,0,2);
                        }
                    }
                    if(strlen($Dts_Name)>1){
                        $Dts_Name = substr($Dts_Name,0,1)==0?substr($Dts_Name,1):$Dts_Name;
                    }
                    $sectList[$Dts_Name] = [
                        "DtS_Name" => $Dts_Name,
                        "DtM_iID_FK" => $ids,
                        "DtS_sWriter" => $this->admin["nickname"],
                        "DtS_dWriterTime" => date("Y-m-d H:i:s"),
                        "DtS_dModifyTime" => date("Y-m-d H:i:s")
                    ];
                    $DtMD_sPartsID = $v['DtMD_sPartsID'];
                    $DtMD_sMaterial = $v['DtMD_sMaterial']==''?'Q235B':$v['DtMD_sMaterial'];
                    $v['DtMD_sSpecification'] = str_replace("L","∠",$v['DtMD_sSpecification']);
                    $v['DtMD_sSpecification'] = str_replace("∟","∠",$v['DtMD_sSpecification']);
                    $v['DtMD_sSpecification'] = str_replace("X","*",$v['DtMD_sSpecification']);
                    $v['DtMD_sSpecification'] = str_replace("x","*",$v['DtMD_sSpecification']);
                    $v['DtMD_sSpecification'] = str_replace("Φ","ф",$v['DtMD_sSpecification']);
                    $firstField = mb_substr(trim($v['DtMD_sSpecification']),0,1);
                    $DtMD_sStuff = "";
                    
                    if($firstField == '∠') $DtMD_sStuff = '角钢';
                    elseif($firstField == '-') $DtMD_sStuff = '钢板';
                    elseif($firstField == '[') $DtMD_sStuff = '槽钢';
                    else{
                        preg_match_all("/\d+/",$v["DtMD_sSpecification"],$matches);
                        if(count($matches[0])>=2) $DtMD_sStuff = '法兰';
                        else $DtMD_sStuff = '圆钢';
                    }
                    
                    $length = $v['DtMD_iLength']?$v['DtMD_iLength']*0.001:0;
                    $width = $v['DtMD_fWidth']?$v['DtMD_fWidth']*0.001:0;
                    $area = $DtMD_sStuff!='钢板'?$length:$length*$width*abs($v['DtMD_sSpecification']);

                    $DtMD_fUnitWeight = 0;
                    if($DtMD_sStuff=="圆钢"){
                        // preg_match_all("/\d+/",$v['DtMD_sSpecification'],$matches);
                        $L = isset($matches[0][0])?$matches[0][0]:0;
                        $DtMD_fUnitWeight = round($L*$L*0.00617,2);
                    }else if($DtMD_sStuff=="法兰"){
                        // preg_match_all("/\d+/",$v['DtMD_sSpecification'],$matches);
                        $R = isset($matches[0][0])?$matches[0][0]/2*0.001:0;
                        $r_y = $matches[0][1]??0;
                        $r_y += $matches[0][2]??0;
                        $r = $r_y?$r_y/2*0.001:0;
                        $DtMD_fUnitWeight = round(3.14159*($R*$R - $r*$r)*$length*7850,2);
                    }else{
                        $DtMD_fUnitWeight = (isset($biZhong[$DtMD_sStuff][$v['DtMD_sSpecification']]))?round($biZhong[$DtMD_sStuff][$v['DtMD_sSpecification']] * $area,4):0;
                    }
                    $detailList[$Dts_Name][] = [
                        'DtMD_sPartsID' => $DtMD_sPartsID,
                        'DtMD_sStuff' => in_array($DtMD_sStuff,["圆钢","法兰"])?"钢管":$DtMD_sStuff,
                        'DtMD_sMaterial' => $DtMD_sMaterial,
                        'DtMD_sSpecification' => $v['DtMD_sSpecification'],
                        'DtMD_iLength' => $v['DtMD_iLength']?$v["DtMD_iLength"]:0,
                        'DtMD_fWidth' => $v['DtMD_fWidth']?$v["DtMD_fWidth"]:0,
                        'DtMD_iUnitCount' => $v["DtMD_iUnitCount"],
                        'DtMD_fUnitWeight' => $DtMD_fUnitWeight,
                        'DtMD_iUnitHoleCount' => $v['DtMD_iUnitHoleCount'],
                        'DtMD_iWelding' => isset($v['DtMD_iWelding'])?1:0,
                        'DtMD_iFireBending' => isset($v['DtMD_iFireBending'])?1:0,
                        'DtMD_iCuttingAngle' => isset($v['DtMD_iCuttingAngle'])?1:0,
                        'DtMD_fBackOff' => isset($v['DtMD_fBackOff'])?1:0,
                        'DtMD_iBackGouging' => isset($v['DtMD_iBackGouging'])?1:0,
                        'DtMD_DaBian' => isset($v['DtMD_DaBian'])?1:0,
                        'DtMD_KaiHeJiao' => isset($v['DtMD_KaiHeJiao'])?1:0,
                        'DtMD_ZuanKong' => isset($v['DtMD_ZuanKong'])?1:0,
                        'DtMD_sRemark' => $v['DtMD_sRemark']
                    ];
                }
                $result = false;
                $where = [
                    "DtM_iID_FK" => ["=",$ids]
                ];
                $isE = $this->sectModel->where($where)->select();
                Db::startTrans();
                try {
                    $sect_id = $saveDetailList = [];
                    foreach($isE as $v){
                        $sect_id[] = $v["DtS_ID_PK"];
                        $v->delete();
                    }
                    $this->detailModel->where("DtMD_iSectID_FK","IN",$sect_id)->delete();
                    $result = $this->sectModel->allowField(true)->saveAll($sectList);
                    $contect = $saveDetailList = [];
                    foreach($result as $v){
                        $contect[$v["DtS_Name"]] = $v["DtS_ID_PK"];
                    }
                    foreach($detailList as $k=>$v){
                        foreach($v as $vv){
                            $saveDetailList[] = array_merge($vv,["DtMD_iSectID_FK"=>$contect[$k]]);
                        }
                    }
                    $detailResult = $this->detailModel->allowField(true)->saveAll($saveDetailList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    return json(["code"=>0,"msg"=>$e->getMessage()]);
                } catch (PDOException $e) {
                    Db::rollback();
                    return json(["code"=>0,"msg"=>$e->getMessage()]);
                } catch (Exception $e) {
                    Db::rollback();
                    return json(["code"=>0,"msg"=>$e->getMessage()]);
                }
                if ($result !== false) {
                    return json(["code"=>1,"msg"=>"保存成功！"]);
                } else {
                    return json(["code"=>0,"msg"=>__('No rows were updated')]);
                }
            }
            return json(["code"=>0,"msg"=>__('Parameter %s can not be empty', '')]);
        }
        $list = $this->sectModel->alias("bls")
            ->join(["baselibrarydetail"=>"bld"],"bls.DtS_ID_PK = bld.DtMD_iSectID_FK")
            ->field("case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end bjbhn,CAST(bls.DtS_Name AS UNSIGNED) AS number_1,CAST(bld.DtMD_sPartsID AS UNSIGNED) AS number_2,bld.*,(case when DtMD_fWidth=0 then '' else DtMD_fWidth end) as DtMD_fWidth")
            ->where("bls.DtM_iID_FK",$ids)
            ->order("number_1,DtS_Name,bjbhn,number_2,DtMD_sPartsID asc")
            ->select();
        $this->view->assign("row", $row);
        $this->view->assign("list",$list);
        $this->view->assign("tableField",$this->getDetailField());
        $this->assignconfig("tableField",$this->getDetailField());
        $this->assignconfig("ids",$ids);
        $this->assignconfig("task_name",$row["DtM_sTypeName"]);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();

            $taskDetail = $this->sectModel->field("DtS_ID_PK")->where("DtM_iID_FK",$ids)->select();
            $task_detail_id = [];
            
            $count = 0;
            Db::startTrans();
            try {
                foreach($taskDetail as $v){
                    $task_detail_id[] = $v["DtS_ID_PK"];
                    $v->delete();
                }
                $this->detailModel->where("DtMD_iSectID_FK","IN",$task_detail_id)->delete();
                foreach ($list as $v) {
                    $count += $v->delete();
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function getDetailField(){
        $tableField = [
            ['零件编号', 'DtMD_sPartsID',"text","data-rule='required'"],
            ['材质', 'DtMD_sMaterial',"text",""],
            ['规格', 'DtMD_sSpecification',"text",""],
            ['长度(mm)', 'DtMD_iLength',"text",""],
            ['宽度(mm)', 'DtMD_fWidth',"text",""],
            ['单基数量', 'DtMD_iUnitCount',"text","data-rule='required'"],
            // ['单基重量', 'DtMD_fUnitWeight'],
            ['孔数', 'DtMD_iUnitHoleCount',"text",""],
            ['电焊', 'DtMD_iWelding',"checkbox",""],
            ['弯曲', 'DtMD_iFireBending',"checkbox",""],
            ['切角', 'DtMD_iCuttingAngle',"checkbox",""],
            ['铲背', 'DtMD_fBackOff',"checkbox",""],
            ['清根', 'DtMD_iBackGouging',"checkbox",""],
            ['打扁', 'DtMD_DaBian',"checkbox",""],
            ['开合角', 'DtMD_KaiHeJiao',"checkbox",""],
            ['钻孔', 'DtMD_ZuanKong',"checkbox",""],
            ['备注', 'DtMD_sRemark',"text",""]
        ];
        return $tableField;
    }

}
