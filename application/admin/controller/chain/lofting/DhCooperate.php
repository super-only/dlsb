<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\DhCooperatekDetail;
use app\admin\model\chain\lofting\Dtmaterialdetail;
use app\admin\model\chain\lofting\MergTypeName;
use app\admin\model\chain\lofting\ProduceDh;
use app\admin\model\chain\lofting\ProduceTask;
use app\admin\model\chain\lofting\ProduceTaskSectShizu;
use app\admin\model\chain\lofting\TaskSect;
use app\admin\model\chain\sale\Sectconfigdetail;
use app\admin\model\chain\sale\TaskHeight;
use app\admin\controller\Technology;
use app\admin\model\chain\lofting\DhCooperateSingle;
use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class DhCooperate extends Technology
{
    
    /**
     * DhCooperate模型对象
     * @var \app\admin\model\chain\lofting\DhCooperate
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_field = \think\Session::get('technology_field');
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->model = new \app\admin\model\chain\lofting\DhCooperate([],$this->technology_ex);
        $this->detailModel = new \app\admin\model\chain\lofting\DhCooperateDetail([],$this->technology_ex);
        $this->singleModel = new \app\admin\model\chain\lofting\DhCooperateSingle([],$this->technology_ex);
        //dtmaterial dtsect dtmaterialdetial
        $this->dtmaterModel = new \app\admin\model\chain\lofting\Dtmaterial([],$this->technology_ex);
        $this->admin = \think\Session::get('admin');

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("k")
                ->join([$this->technology_ex."dtmaterial"=>"f"],"k.DtM_iID_PK = f.DtM_iID_PK")
                ->join(["taskdetail"=>"td"],"k.TD_ID = td.TD_ID")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num")
                ->field("k.DC_Num,k.DC_Editdate,k.DC_Editer,k.DC_AuditorDate,f.T_Company,k.DC_Memo,f.DtM_sProject,f.DtM_sTypeName,f.DtM_sTypeName as 'f.DtM_sTypeName',f.DtM_sPressure,t.T_Num,t.T_Num as 't.T_Num',t.T_Sort")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->where("DC_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $dhcooperatek_one = (new ProduceTask([],$this->technology_ex))->alias("dk")->join([$this->technology_ex."dhcooperate"=>"f"],"f.TD_ID=dk.TD_ID")->where("f.DC_Num", $ids)->find();
        $flag = $dhcooperatek_one?false:true;
        $row = $row->toArray();
        $DtM_iID_PK = $row["DtM_iID_PK"];

        $list = $this->dtmaterModel->alias("d")
                ->join(["taskdetail"=>"td"],"d.TD_ID=td.TD_ID")
                ->join(["task"=>"t"],"td.T_Num=t.T_Num")
                ->join(["compact"=>"c"],"t.C_Num=c.C_Num")
                ->field("d.T_Company,t.C_Num,d.DtM_sProject,d.DtM_sTypeName,d.DtM_sPressure,c.Customer_Name as CustomerName")
                ->where("DtM_iID_PK",$DtM_iID_PK)
                ->find();
        $row = array_merge($row,$list->toArray());
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $this->view->assign("row", $row);
        $this->view->assign("ids",$row["DC_Num"]);
        $this->view->assign("flag", $flag);
        return $this->view->fetch();
    }

    public function weldingDrawing()
    {
        $params = $this->request->post();
        $ids = $params["ids"]??"";
        if(!$ids) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $DCD_PartName = $params["DCD_PartName"]??"";
        $DCD_PartNum = $params["DCD_PartNum"]??"";
        $DCD_Type = $params["DCD_Type"]??"";
        $where = ["DC_Num"=>["=",$ids]];
        $DCD_PartName?$where["DCD_PartName"] = ["=",$DCD_PartName]:"";
        $DCD_PartNum?$where["DCD_PartNum"] = ["=",$DCD_PartNum]:"";
        $DCD_Type?$where["DCD_Type"] = ["=",$DCD_Type]:"";
        $list = $this->detailModel->field("*,CAST(DCD_PartName AS UNSIGNED) as number_1,CAST(DCD_PartNum AS UNSIGNED) as number_2,
            case when (length(DCD_PartNum)-length(replace(DCD_PartNum,'-','')))>1 then 
            SUBSTR(DCD_PartNum,1,LOCATE('-',DCD_PartNum)-1)*10000000+SUBSTR(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1),1,LOCATE('-',SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1))-1)*10000+cast(SUBSTR(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1),LOCATE('-',SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1))+1) as signed)
            when LOCATE('-',DCD_PartNum)>0 and LENGTH(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed))=3 then SUBSTR(DCD_PartNum,1,LOCATE('-',DCD_PartNum)-1)*10000000+SUBSTR(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed),1+1) as signed)
            when LOCATE('-',DCD_PartNum)>0 and LENGTH(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed))=4 then SUBSTR(DCD_PartNum,1,LOCATE('-',DCD_PartNum)-1)*10000000+SUBSTR(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed),2+1) as signed)
            when LENGTH(cast(DCD_PartNum as signed))=3 then SUBSTR(cast(DCD_PartNum as signed),1,1)*10000000+cast(SUBSTR(cast(DCD_PartNum as signed),1+1) as signed)
            when LENGTH(cast(DCD_PartNum as signed))=4 then SUBSTR(cast(DCD_PartNum as signed),1,2)*10000000+cast(SUBSTR(cast(DCD_PartNum as signed),2+1) as signed)
            else 0 end 	bjbhn")->where($where)->order("number_1,bjbhn,DCD_PartNum,number_2 ASC")->select();
        $rows = [];
        foreach($list as $k=>$v){
            $rows[] = $v->toArray();
            $rows[$k]["DCD_SWeight"] = round($v["DCD_SWeight"],2);
            $rows[$k]["DCD_Memo_Input"] = build_input('DCD_Memo_Input', 'text', $v["DCD_Memo"]);
            $rows[$k]["DCD_Type_Input"] = build_input('DCD_Type_Input', 'text', $v["DCD_Type"]);
        }
        // pri($rows,1);
        return json(["code"=>1,"data"=>$rows]);

    }

    public function detailDrawing()
    {
        $DCD_ID = $this->request->post("DCD_ID");
        if(!$DCD_ID) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $where = ["s.DCD_ID"=>$DCD_ID];
        $arr = $this->singleModel->detailList($where,[],$this->technology_ex);
        $field = [
            "DHS_ID" => ["hidden",1,"readonly"],
            "DCD_ID" => ["hidden",1,"readonly"],
            "DtMD_sPartsID" => ["",0,""],
            "DtMD_sStuff" => ["",0,""],
            "DtMD_sMaterial" => ["",0,""],
            "DtMD_sSpecification" => ["",0,""],
            "DHS_Thick" => ["",0,""],
            "DHS_Height" => ["",0,""],
            "DtMD_fUnitWeight" => ["",0,""],
            "DHS_Count" => ["",1,""],
            "DHS_Length" => ["",1,""],
            "DHS_Grade" => ["",1,""],
            "DHS_Memo" => ["",1,""]
        ];
        $tableContent = "";
        foreach($arr as $k=>$v){
            $tableContent .= "<tr>";
            foreach($field as $kk=>$vv){
                $tableContent .= "<td ".$vv[0].">".($vv[1]?build_input($kk,"text",$v[$kk],[$vv[2]=>""]):$v[$kk])."</td>";
            }
            $tableContent .= "</tr>";
        }
        return json(["code"=>1,"data"=>$tableContent]);
    }
    //已更改
    public function drawingAssemblyDetail($dc_num = null, $dcd=null,$ids=null)
    {
        if($dcd){
            $row = $this->detailModel->alias("d")
                ->join([$this->technology_ex."dhcooperate"=>"k"],"d.DC_Num = k.DC_Num")
                ->join([$this->technology_ex."dtmaterial"=>"f"],"f.DtM_iID_PK = k.DtM_iID_PK")
                ->field("d.*,f.DtM_sTypeName,f.DtM_sPressure,f.DtM_iID_PK,k.TD_ID
                ")
                ->where("d.DCD_ID",$dcd)
                ->find();
        }else{
            $row = $this->model->alias("m")
                ->join([$this->technology_ex."dtmaterial"=>"f"],"f.DtM_iID_PK = m.DtM_iID_PK")
                ->where("DC_Num",$dc_num)
                ->find();
        }
        
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $row = $row->toArray();
        $duanList = [0=>"请选择"];
        $duanWhere = [
            "s.DtM_iID_FK" => ["=",$row["DtM_iID_PK"]],
            "d.DtMD_iWelding" => ["<>",0]
        ];
        $duanArr = (new \app\admin\model\chain\lofting\Dtsect([],$this->technology_ex))->alias("s")
            ->join([$this->technology_ex."dtmaterialdetial"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("s.Dts_Name,CAST(s.Dts_Name AS UNSIGNED) as number")
            ->field("s.Dts_Name")
            ->where($duanWhere)
            ->group("s.Dts_Name")
            ->order("number")
            ->select();
        foreach($duanArr as $v){
            $duanList[$v["Dts_Name"]] = $v["Dts_Name"];
        }

        $this->view->assign("memoList",$this->memo());
        $this->view->assign("chooseDuan",$duanList);
        $this->view->assign("row",$row);
        // $this->view->assign("tableContent",$arr);
        return $this->view->fetch();
    }
    
    public function drawingAssemblySituation($dc_num = null)
    {
        $dhList = $this->model->where("DC_Num",$dc_num)->find();
        if(!$dhList) $this->error(__('No Results were found'));

        $tdId = $dhList["TD_ID"];

        $sectWhere = $tdSectArr = [];
        $mTD_ID = (new MergTypeName())->where("mTD_ID",$tdId)->select();
        if($mTD_ID){
            foreach($mTD_ID as $v){
                $td_list[$v["TD_ID"]] = $v["TD_ID"];
            }
            $sectWhere = ["th.TD_ID"=>["IN",$td_list]];
            
        }else $sectWhere = ["th.TD_ID"=>["=",$tdId]];

        // $sectWhere = [
        //     "th.TD_ID" => ["=",$tdId]
        // ];
        $tdSectList = (new TaskHeight())->alias("th")
            ->join(["sectconfigdetail"=>"sd"],"th.TH_ID=sd.TH_ID")
            ->join(["tasksect"=>"ts"],"ts.SCD_ID=sd.SCD_ID")
            ->field("ts.TS_Name,sum(ts.TS_Count) as count,ts.ifBody")
            ->where($sectWhere)
            ->group("ts.TS_Name")
            ->select();
        foreach($tdSectList as $v){
            $tdSectArr[$v["TS_Name"]] = ["count"=>$v["count"],"ifbody"=>$v["ifBody"]];
        }
        $dtm_iid_pk = $dhList["DtM_iID_PK"];
        $where = ["s.DtM_iID_FK"=>["=",$dtm_iid_pk],"d.DtMD_iWelding"=>["<>",0]];
        $detailList = (new \app\admin\model\chain\lofting\Dtsect([],$this->technology_ex))->alias("s")
            ->join([$this->technology_ex."dtmaterialdetial"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("1 AS 'common',d.DtMD_sMaterial,d.DtMD_sSpecification,s.DtS_Name,d.DtMD_ID_PK,d.DtMD_sPartsID,sum(d.DtMD_iUnitCount) as preCount,CAST(s.Dts_Name AS UNSIGNED) as number_1,CAST(d.DtMD_sPartsID AS UNSIGNED) as number_2,concat(s.DtS_Name,' / ',d.DtMD_sPartsID) as name")
            ->where($where)
            ->order("number_1,number_2,d.DtMD_sMaterial ASC")
            ->group("name")
            ->select();
        $thisList = $this->detailModel->alias("dd")
            ->join([$this->technology_ex."dhcooperatesingle"=>"ds"],"dd.DCD_ID = ds.DCD_ID")
            ->join([$this->technology_ex."dtmaterialdetial"=>"dtd"],"ds.DtMD_ID_PK = dtd.DtMD_ID_PK")
            ->join([$this->technology_ex."dtsect"=>"dts"],"dts.DtS_ID_PK = dtd.DtMD_iSectID_FK")
            ->field("dd.DCD_PartName,dts.DtS_Name,sum((dd.DCD_Count * ds.DHS_Count)) as isCount,concat(dts.DtS_Name,' / ',dtd.DtMD_sPartsID) as act_name")
            ->where("DC_Num",$dc_num)
            ->group("act_name,dd.DCD_PartName,dts.DtS_Name")
            ->select();
        $thisArr = $detailArr = $isNoDeal = [];
        foreach($thisList as $v){
            if(!isset($thisArr[$v["act_name"]])) $thisArr[$v["act_name"]] = isset($tdSectArr[$v["DCD_PartName"]])?$tdSectArr[$v["DCD_PartName"]]["count"]*$v["isCount"]:$v["isCount"];
            else $thisArr[$v["act_name"]] += isset($tdSectArr[$v["DCD_PartName"]])?$tdSectArr[$v["DCD_PartName"]]["count"]*$v["isCount"]:$v["isCount"];
        }
        foreach($detailList as $v){
            $name = $v["name"];
            $msg = "";
            $detailArr[$name] = [
                "name" => $name,
                "preCount" => isset($tdSectArr[$v["DtS_Name"]])?round($tdSectArr[$v["DtS_Name"]]["count"]*$v["preCount"]):round($v["preCount"]),
                "isCount" => $thisArr[$name]??0,
                "common" => $v["common"]
            ];

            if($detailArr[$name]["isCount"]==0) $isNoDeal[] = [
                "DtS_Name" => $v["DtS_Name"],
                "DtMD_sPartsID" => $v["DtMD_sPartsID"],
                "DtMD_sMaterial" => $v["DtMD_sMaterial"],
                "DtMD_sSpecification" => $v["DtMD_sSpecification"]
            ];

            if($detailArr[$name]["preCount"] > $detailArr[$name]["isCount"]) $msg = "未满配置";
            elseif($detailArr[$name]["preCount"] == $detailArr[$name]["isCount"]){
                $msg = "完成配置";
                $detailArr[$name]["common"] = 0;
            }else  $msg = "超出配置";
            $detailArr[$name]["detail"] = $msg;
        }
        $this->view->assign("row",array_values($detailArr));
        $this->view->assign("isNoDeal",json_encode($isNoDeal));
        return $this->view->fetch();
    }
    
    public function copyWelding()
    {
        return $this->view->fetch();
    }

    public function confirmModification()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"无法复制"]);
        else  return json(["code"=>1]);
        // $td_id = $this->model->where("DC_Num",$num)->find();
        // if($td_id) $one = (new \app\admin\model\chain\lofting\ProduceTask([],$this->technology_ex))->where("TD_ID",$td_id["TD_ID"])->find();
        // if($one) return json(["code"=>0,"msg"=>"无法复制"]);
    }

    public function copyWeldingProject()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("dh")
                ->join(["taskdetail"=>"td"],"dh.TD_ID = td.TD_ID")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num")
                ->field("dh.DC_Num,t.C_Num,t.T_Num,t.T_Num as 't.T_Num',t.t_project,t_shortproject,td.TD_TypeName,td.TD_ID")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function copyWeldingTower()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\lofting\DhCooperatek([],$this->technology_ex))->alias("k")
                ->join([$this->technology_ex."fytxk"=>"f"],"k.DtM_iID_PK = f.DtM_iID_PK")
                ->field("k.DC_Num,f.DtM_sTypeName,f.DtM_sTypeName as TD_TypeName")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function searchParam()
    {
        $params = $this->request->post();
        list($num,$type) = array_values($params);
        if(!$num or !$type) return json(["code"=>0,"msg"=>"未找到，请重新选择"]);
        $sectList = $sectArr = [];
        if($type==1){
            $sectList = $this->detailModel->field("DCD_PartName,CAST(DCD_PartName AS UNSIGNED) as number")->where("DC_Num",$num)->group("DCD_PartName")->order("number")->select();
        }else{
            $sectList = (new DhCooperatekDetail([],$this->technology_ex))->field("DCD_PartName,CAST(DCD_PartName AS UNSIGNED) as number")->where("DC_Num",$num)->group("DCD_PartName")->order("number")->select();
        }
        foreach($sectList as $v){
            $sectArr[] = ["name"=>$v["DCD_PartName"]];
        }
        return json(["code"=>1,"data"=>$sectArr]);
    }

    public function copyTower()
    {
        $params = $this->request->post();
        //选择的段名 copy过来的dc_num 1.电焊组合表dhcooperate，2.电焊组合库dhcooperatek 本身的dc_num
        list($name_arr,$copy_dc_num,$choose,$dc_num) = array_values($params);
        if(!$copy_dc_num or !$dc_num) return json(["code"=>0,"msg"=>"复制失败"]);
        $selfList = $selfArr = [];

        $one = $this->model->field("TD_ID")->where("DC_Num",$dc_num)->find();
        if($one) $td_id = $one["TD_ID"];
        if(!$td_id) return json(["code"=>0,"msg"=>"复制失败"]);
        //本身的焊接段和明细号
        $selfArr = $selfSect = $selfBjNum = [];
        $where = [
            "dt.TD_ID" => ["=",$td_id],
            "dtd.DtMD_iWelding" => ["<>",0]
        ];
        $selfList = $this->dtmaterModel->alias("dt")
            ->join([$this->technology_ex."dtsect"=>"dts"],"dt.DtM_iID_PK = dts.DtM_iID_FK")
            ->join([$this->technology_ex."dtmaterialdetial"=>"dtd"],"dts.DtS_ID_PK = dtd.DtMD_iSectID_FK")
            ->field("CONCAT(dts.DtS_Name,' / ',dtd.DtMD_sPartsID) as name,dts.DtS_Name,dtd.DtMD_sPartsID,dtd.DtMD_ID_PK,dtd.DtMD_sStuff,dtd.DtMD_sMaterial,dtd.DtMD_sSpecification,dtd.DtMD_fUnitWeight,dtd.DtMD_iLength,dtd.DtMD_fWidth,ifnull(dtd.DtMD_sRemark,'') as DtMD_sRemark,dtd.DtMD_iUnitCount")
            ->where($where)
            ->select();
        foreach($selfList as $v){
            $selfArr[$v["name"]] = ["id"=>$v["DtMD_ID_PK"],"weight"=>$v["DtMD_fUnitWeight"],"DHS_Thick"=>$v["DtMD_iLength"],"DHS_Height"=>$v["DtMD_fWidth"],"DHS_Memo"=>$v["DtMD_sRemark"],"count"=>$v["DtMD_iUnitCount"]];
            $selfSect[$v["DtS_Name"]] = $v["DtS_Name"];
			$selfBjNum[$v["DtMD_sPartsID"]] = $v["DtMD_sPartsID"];
        }
        if(empty($selfArr)) return json(["code"=>0,"msg"=>"复制失败"]);

        //已存在的主电焊段
        $exist_main_sect = $this->detailModel->field("DCD_PartName")->where("DC_Num",$dc_num)->group("DCD_PartName")->column("DCD_PartName");
        $chooseWhere = $contrast = $contrastDetail = $chooseCoopList = [];
        $chooseWhere["dm.DC_Num"] = ["=",$copy_dc_num];
        $chooseWhere["dm.DCD_PartName"] = ["in",array_diff(array_intersect($name_arr,$selfSect),$exist_main_sect)];
        if($choose == 1){
            $chooseCoopList = $this->detailModel->alias("dm")
                ->join([$this->technology_ex."dhcooperatesingle"=>"ds"],"dm.DCD_ID = ds.DCD_ID")
                ->join([$this->technology_ex."dtmaterialdetial"=>"dtd"],"dtd.DtMD_ID_PK = ds.DtMD_ID_PK")
                ->join([$this->technology_ex."dtsect"=>"dts"],"dts.DtS_ID_PK = dtd.DtMD_iSectID_FK")
                // ->field("CONCAT(dts.DtS_Name,' / ',dm.DCD_PartNum,' / ',dtd.DtMD_sStuff,' / ',dtd.DtMD_sMaterial) as name,dtd.DtMD_sPartsID,ds.*,dm.DCD_PartNum,dm.DCD_PartName,dm.DCD_Count")
                ->field("CONCAT(dts.DtS_Name,' / ',dtd.DtMD_sPartsID) as name,dtd.DtMD_sPartsID,ds.*,dm.DCD_PartNum,dm.DCD_PartName,dm.DCD_Count")
                ->where($chooseWhere)
                ->select();
            
        }else{
            $chooseCoopList = (new DhCooperatekDetail([],$this->technology_ex))->alias("dm")
                ->join([$this->technology_ex."dhcooperateksingle"=>"ds"],"dm.DCD_ID = ds.DCD_ID")
                ->join([$this->technology_ex."fytxkdetail"=>"dtd"],"dtd.DtMD_ID_PK = ds.DtMD_ID_PK")
                ->join([$this->technology_ex."fytxksect"=>"dts"],"dts.DtS_ID_PK = dtd.DtMD_iSectID_FK")
                ->field("CONCAT(dts.DtS_Name,' / ',dtd.DtMD_sPartsID) as name,dtd.DtMD_sPartsID,ds.*,dm.DCD_PartNum,dm.DCD_PartName,dm.DCD_Count")
                ->where($chooseWhere)
                ->select();
        }
        if(empty($chooseCoopList)) return json(["code"=>0,"msg"=>"复制失败"]);
        foreach($chooseCoopList as $v){
            $contrast[$v["DHS_ID"]] = $v["name"];
            $contrastDetail[$v["DHS_ID"]] = $v->toArray();
        }
        
        $int_result = array_intersect($contrast,array_keys($selfArr));
        if(!$int_result) return json(["code"=>0,"msg"=>"复制失败"]);
        $result = array_intersect_key($contrastDetail,$int_result);
        $detailOne = $this->detailModel->field("DCD_ID")->order("DCD_ID DESC")->find();
        $count_id = $detailOne?$detailOne["DCD_ID"]+1:1;
        $sum_weight = $dcd_part_name_list = [];
        foreach($result as $k=>$v){
			if(isset($selfBjNum[$v["DCD_PartNum"]])){
				$dcd_part_name_list[] = $v["DCD_PartName"];
				$name = $v["DCD_PartName"].' / '.$v["DCD_PartNum"];
				if(!isset($saveDetail[$name])){
					$saveDetail[$name] = [
						"DCD_ID" => $count_id,
						"DCD_PartName" => $v["DCD_PartName"],
						"DCD_PartNum" => $v["DCD_PartNum"],
						"DCD_Count" => $selfArr[$v["name"]]["count"]/$v["DHS_Count"],
						"DCD_SWeight" => 0,
						"DC_Num" => $dc_num,
						"Writer" => $this->admin["nickname"],
						"WriteDate" => date("Y-m-d H:i:s")
					];
					$count_id++;
				}
				$saveSingle[$name][$v["DtMD_sPartsID"]] = [
					"DCD_ID" => $saveDetail[$name]["DCD_ID"],
					"DtMD_ID_PK" => $selfArr[$v["name"]]["id"],
					"DHS_Count" => $v["DHS_Count"],
					"DHS_Length" => $v["DHS_Length"],
					"DHS_Thick" => $selfArr[$v["name"]]["DHS_Thick"],
					"DHS_Height" => $selfArr[$v["name"]]["DHS_Height"],
					"DHS_Grade" => $v["DHS_Grade"],
					"DHS_Memo" => $selfArr[$v["name"]]["DHS_Memo"],
					"DHS_isM" => $v["DHS_isM"]
				];
				$sum = $v["DHS_Count"]*$selfArr[$v["name"]]["weight"];
				$sum_weight[$name] = isset($sum_weight[$name])?$sum_weight[$name]+$sum:$sum;
			}
        }

        $singleArr = [];
        foreach($saveSingle as $k=>$v){
            foreach($v as $kk=>$vv){
                $singleArr[] = $vv;
            }
        }
        foreach($sum_weight as $k=>$v){
            $saveDetail[$k]["DCD_SWeight"] = $v;
        }
        $dcd_id_arr = [];
        if(!empty($dcd_part_name_list)){
            //自己本身保存的段
            $isSaveSectList = $this->detailModel->field("DCD_PartName,DCD_ID")->where("DC_Num",$dc_num)->select();
            $isSaveSectArr = [];
            foreach($isSaveSectList as $k=>$v){
                $isSaveSectArr[$v["DCD_PartName"]][] = $v["DCD_ID"];
            }
            $interset = array_intersect(array_keys($isSaveSectArr),$dcd_part_name_list);
            
            foreach($interset as $v){
                foreach($isSaveSectArr[$v] as $kk=>$vv){
                    $dcd_id_arr[] = $vv;
                }
            }
        }
        $result = false;
        Db::startTrans();
        try {
            if(!empty($dcd_id_arr)){
                $this->detailModel->where("DCD_ID","IN",$dcd_id_arr)->delete();
                $this->singleModel->where("DCD_ID","IN",$dcd_id_arr)->delete();
            }
            $result = $this->detailModel->allowField(true)->saveAll($saveDetail,false);
            $this->singleModel->allowField(true)->saveAll($singleArr);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if($result !== false){
            return json(["code"=>1,"msg"=>'复制成功']);
        }else{
            return json(["code"=>0,"msg"=>"复制失败"]);
        }
        
    }

    public function leftTable()
    {
        $params = $this->request->post();
        list($DtM_iID_PK,$DtS_Name,$DCD_PartName,$DCD_PartNum) = array_values($params);
        if(!$DtM_iID_PK) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $where = ["s.DtM_iID_FK"=>["=",$DtM_iID_PK],"d.DtMD_iWelding"=>["<>",0]];
        if($DtS_Name) $where["s.DtS_Name"] = ["=",$DtS_Name];
        if($DCD_PartName){
            $partArr = explode(",",$DCD_PartName);
            $where["d.DtMD_sPartsID"] = ["in",$partArr];
        }
        if($DCD_PartNum) $where["d.DtMD_sRemark"] = ["LIKE","%".$DCD_PartName."%"];
        
        $detailList = (new \app\admin\model\chain\lofting\Dtsect([],$this->technology_ex))->alias("s")
            ->join([$this->technology_ex."dtmaterialdetial"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("s.DtS_Name,d.DtMD_ID_PK,d.DtMD_sPartsID,d.DtMD_iUnitCount,d.DtMD_sRemark,d.DtMD_sStuff,d.DtMD_sMaterial,d.DtMD_sSpecification,d.DtMD_iLength,d.DtMD_fWidth,d.DtMD_iWelding,d.DtMD_fUnitWeight,d.DtMD_iUnitHoleCount,d.DtMD_iCuttingAngle,d.DtMD_fBackOff,d.DtMD_iFireBending,d.DtMD_iPressed,d.DtMD_ISZhuanYong,CAST(s.Dts_Name AS UNSIGNED) as number_1,CAST(d.DtMD_sPartsID AS UNSIGNED) as number_2,
            case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn")
            ->where($where)
            ->order("number_1,bjbhn,DtMD_sPartsID,number_2 ASC")
            ->select();
		$data = [];
		foreach($detailList as $k=>$v){
			$v["DtMD_iUnitCount"] = round($v["DtMD_iUnitCount"],2);
			$data[] = $v->toArray();
		}
        return json(["code"=>1,"data"=>$data]);

    }

    public function rightTable(){
        $params = $this->request->post();
        $DCD_ID = $params["DCD_ID"]??"";
        if(!$DCD_ID) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $field = [
            ["DtMD_sPartsID","text","readonly",""],
            ["DtMD_sStuff","text","readonly",""],
            ["DtMD_sMaterial","text","readonly",""],
            ["DtMD_sSpecification","text","readonly",""],
            ["DHS_Thick","text","readonly",""],
            ["DHS_Height","text","readonly",""],
            ["DHS_Count","text","",""],
            ["DtMD_fUnitWeight","text","",""],
            ["DHS_isM","text","",""],
            ["DHS_Length","text","",""],
            ["DHS_Memo","text","",""],
            ["DHS_ID","text","readonly","hidden"],
            ["DCD_ID","text","readonly","hidden"],
            ["DtMD_ID_PK","text","readonly","hidden"]
        ];
        $where = ["s.DCD_ID"=>$DCD_ID];
        $detailList = $this->singleModel->detailList($where,[],$this->technology_ex);
        $dcdList = [];
        $tableField = '';
        foreach($detailList as $k=>$v){
            $dcdList[] = $v["DtMD_ID_PK"];
            $tableField .= '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger table_del"><i class="fa fa-trash"></i></a></td>';
            foreach($field as $vv){
                $tableField .= '<td '.$vv[3].'>'.build_input($vv[0],$vv[1],$v[$vv[0]],["class"=>"small_input",$vv[2]=>""]).'</td>';
            }
            $tableField .= "</tr>";
        }
        return json(["code"=>1,"data"=>$tableField,"rightDcdList"=>$dcdList]);
    }

    public function saveDetail()
    {
        $params = json_decode(str_replace('&quot;','"',$this->request->post('data')), true);
        list($row,$tableToArr) = array_values($params);
        if (!$row or !$tableToArr) {
            $this->error(__('No Results were found'));
        }
        $tableRow = [];
        $count = count($tableToArr)/14;
        for($i=0;$i<$count;$i++){
            for($j=0;$j<14;$j++){
                $key = 14*$i+$j;
                $tableRow[$i][$tableToArr[$key]["name"]] = $tableToArr[$key]["value"];
            }
        }
        $list = $tableList = $dt_list = [];
        foreach($row as $v){
            $list[$v["name"]] = $v["value"];
        }
        if($list["DCD_Count"]==false) return json(["code"=>0,"msg"=>"组数不能为0"]);
        $list["Writer"] = $this->admin["nickname"];
        $list["WriteDate"] = date("Y-m-d H:i:s");
        $TD_ID = $list["TD_ID"];
        foreach($tableRow as $k=>$v){
            if($v["DHS_Count"]==false) return json(["code"=>0,"msg"=>"单组件数不能为0"]);
            $name = $list["DCD_PartName"]."(-)".$v["DtMD_sPartsID"];
            $tableList[$name] = $v;
            $dt_list[$v["DtMD_ID_PK"]] = ["num"=>$v["DHS_Count"]*$list["DCD_Count"],"name"=>$v["DtMD_sPartsID"]];
        }
        if(in_array($list["DCD_PartName"].'(-)'.$list["DCD_PartNum"],array_keys($tableList))==FALSE) return json(["code"=>0,"msg"=>"电焊件号不存在于放样补件明细中。请重新输入!"]);
        $this->list = $list;
        $find_one = $this->detailModel->alias("d")
            ->join([$this->technology_ex."dhcooperatesingle"=>"s"],"d.DCD_ID=s.DCD_ID")
            ->join([$this->technology_ex."dtmaterialdetial"=>"dd"],"dd.DtMD_ID_PK = s.DtMD_ID_PK")
            ->where("d.DC_Num",$list["DC_Num"])
            ->where("d.DCD_ID","<>",$list["DCD_ID"])
            ->where(function ($query) {
                $query->where('d.DCD_PartNum', '=', $this->list["DCD_PartNum"])->whereOr('dd.DtMD_sPartsID', '=', $this->list["DCD_PartNum"]);
            })->find();
        if($find_one) return json(["code"=>0,"msg"=>"该组件号已经被使用作为组件号，或该件号在其他件号的电焊明细中，不能再次作为组件号。请重新输入!"]);
        $sect_ini_where = $td_list = [];
        $mTD_ID = (new MergTypeName())->where("mTD_ID",$TD_ID)->select();
        if($mTD_ID){
            foreach($mTD_ID as $v){
                $td_list[$v["TD_ID"]] = $v["TD_ID"];
            }
            $sect_ini_where = ["th.TD_ID"=>["IN",$td_list]];
            
        }else $sect_ini_where = ["th.TD_ID"=>["=",$TD_ID]];
        $scd_id_list = (new Sectconfigdetail())->getScdIdList($sect_ini_where);

        $sectWhere = [
            "SCD_ID" => ["IN",$scd_id_list]
        ];
        $scd_num_arr = [];
        $scd_num_list = (new TaskSect())->field("TS_Name,sum(TS_Count) as TS_Count")->where($sectWhere)->group("TS_Name")->select();
        foreach($scd_num_list as $v){
            $scd_num_arr[$v["TS_Name"]] = $v["TS_Count"];
        }
        $fydetailList = $dhkdetailList = [];
        $dt_key = array_keys($dt_list);
        //写完成产下达明细 考虑是否可以整理
        $fydetail = (new Dtmaterialdetail([],$this->technology_ex))->alias("dd")
            ->join([$this->technology_ex."dtsect"=>"ds"],"dd.DtMD_iSectID_FK = ds.DtS_ID_PK")
            ->field("dd.DtMD_iUnitCount,dd.DtMD_ID_PK,ds.DtS_Name")->where("DtMD_ID_PK","IN",$dt_key)->select();
        foreach($fydetail as $v){
            $fydetailList[$v["DtMD_ID_PK"]] = isset($scd_num_arr[$v["DtS_Name"]])?$v["DtMD_iUnitCount"]*$scd_num_arr[$v["DtS_Name"]]:$v["DtMD_iUnitCount"];
        }
        $where = [
            "d.DC_Num" => ["=",$list["DC_Num"]],
            "s.DtMD_ID_PK" => ["IN",$dt_key],
            "s.DCD_ID" => ["<>",$list["DCD_ID"]]
        ];
        $dhkdetail = $this->singleModel->alias("s")
            ->join([$this->technology_ex."dhcooperatedetail"=>"d"],"s.DCD_ID = d.DCD_ID")
            ->field("d.DCD_PartName,sum(d.DCD_Count*s.DHS_Count) as sumCount,s.DtMD_ID_PK")
            ->where($where)
            ->group("s.DtMD_ID_PK")
            ->select();
        foreach($dhkdetail as $v){
            $dhkdetailList[$v["DtMD_ID_PK"]] = ["count"=>$v["sumCount"],"DCD_PartName"=>$v["DCD_PartName"]];
        }
        $msg = "";
        foreach($dt_list as $k=>$v){
            $agenum = isset($dhkdetailList[$k])?$dhkdetailList[$k]["count"]*$scd_num_arr[$dhkdetailList[$k]["DCD_PartName"]]:0;
            $allnum = isset($fydetailList[$k])?$fydetailList[$k]:0;
            $diff_num = $v["num"]*$scd_num_arr[$list["DCD_PartName"]]+$agenum-$allnum;
            if($diff_num>0) $msg .= $v["name"]."的件数已经超出放样数据中的件数".$diff_num."件。";
        }
        if($msg != "") $msg .= "是否忽略问题?";
        else $msg = "success";
        return json(["code"=>1,"msg"=>$msg,"data"=>["list"=>$list,"tableList"=>json_encode($tableList)]]);

    }

    public function saveDetailSect()
    {
        $params = json_decode(str_replace('&quot;','"',$this->request->post('data')), true);
        $save_list = [];
        $DC_Num = "";
        foreach($params as $v){
            $save_list[$v["DCD_ID"]] = [
                "DCD_ID" => $v["DCD_ID"],
                "DCD_Memo" => $v["DCD_Memo"],
                "DCD_Type" => $v["DCD_Type"]
            ];
            $DC_Num = $v["DC_Num"];
        }
        $produce_task_list = $this->model->alias("dh")
            ->join([$this->technology_ex."producetask"=>"pt"],"dh.TD_ID = pt.TD_ID")
            ->field("pt.PT_Num")
            ->where("DC_Num",$DC_Num)
            ->where("pt.Auditor","<>","")
            ->column("PT_Num,dh.TD_ID");
        $td_id = current($produce_task_list);
        $produce_task_list = array_keys($produce_task_list);
        if($produce_task_list){
            $pts_list = $dhList = [];
            $szList = (new \app\admin\model\chain\lofting\ProduceTaskSectShizu([],$this->technology_ex))->field("PT_Num,PT_Sect")->where("PT_Num","IN",$produce_task_list)->select();
            $szArr = $dhList = [];
            foreach($szList as $v){
                $szArr[$v["PT_Num"]][$v["PT_Sect"]] = $v["PT_Sect"];
            }
            $sectModel = new \app\admin\model\chain\lofting\ProduceTaskSect([],$this->technology_ex);
            $pts_name_list = $sectModel->field("PT_Num,PTS_Name")->where("PT_Num","IN",$produce_task_list)->group("PT_Num,PTS_Name")->select();
            foreach($pts_name_list as $v){
                $pts_list[$v["PT_Num"]][$v["PTS_Name"]] = $v["PTS_Name"];
            }
            $group="PTS_Name,PT_Num";
            $useField = "sum(PTS_Count) as PTS_Count,PTS_Name,PT_Num";
            $usedList = $sectModel->field($useField)
                ->where("PT_Num","IN",$produce_task_list)
                ->group($group)
                ->select();
            $sectList = [];
            foreach($usedList as $v){
                $sectList[$v["PT_Num"]][$v["PTS_Name"]] = $v["PTS_Count"];
            }
            if(!empty($pts_list)) {
                $where = ["dhp.TD_ID"=>["=",$td_id]];
                $partNameList = [];
                array_map(function ($value) use (&$partNameList) {
                    $partNameList = array_merge($partNameList, array_values($value));
                }, $pts_list);
                $where["dhd.DCD_PartName"] = ["in",$partNameList];
                $dh = $this->detailModel->alias("dhd")
                    ->join([$this->technology_ex."dhcooperate"=>"dhp"],"dhd.DC_Num = dhp.DC_Num")
                    ->field("dhd.DCD_ID,dhd.DCD_Count,dhd.DCD_PartName")
                    ->where($where)
                    ->select();
                
                foreach($produce_task_list as $pv){
                    isset($sectList[$pv])?"":$sectList[$pv] = [];
                    isset($szArr[$pv])?"":$szArr[$pv] = [];
                    foreach($dh as $v){
                        if(in_array($v["DCD_PartName"],$pts_list[$pv]) !== false){
                            $dhList[] = [
                                "DCD_ID" => $v["DCD_ID"],
                                "PDH_Count" => isset($sectList[$pv][$v["DCD_PartName"]])?$sectList[$pv][$v["DCD_PartName"]]*$v["DCD_Count"]:$v["DCD_Count"],
                                "BCDCode" => "",
                                "PT_Num" => $pv,
                                "IsSZ" => in_array($v["DCD_PartName"],$szArr[$pv])?1:0,
                                "SCD_ID" => 0,
                                "SCD_TPNum" => ""
                            ];
                        }
                    }
                }
            }
            
        }
        $result = false;
        Db::startTrans();
        try {
            $result = $this->detailModel->allowField(true)->saveAll($save_list);
            $dhModel = new \app\admin\model\chain\lofting\ProduceDh([],$this->technology_ex);
            if(!empty($pts_list)){
                $dhModel->where("PT_Num","IN",$produce_task_list)->delete();
                $result = $dhModel->saveAll($dhList);
            }
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if($result) return json(["code"=>1,"msg"=>"保存成功！"]);
        else return json(["code"=>0,"msg"=>"保存失败！请刷新后重试！"]);
    }

    // public function saveEditDetail()
    // {
    //     $params = json_decode($this->request->post('data'),true);
    //     if (!$params) {
    //         return json(["code"=>0,"msg"=>"保存失败！请刷新后重试！"]);
    //     }
    //     if (empty($params)) {
    //         return json(["code"=>0,"msg"=>"无须保存！"]);
    //     }
    //     $tableRow = [];
    //     $count = count($params)/5;
    //     for($i=0;$i<$count;$i++){
    //         for($j=0;$j<5;$j++){
    //             $key = 5*$i+$j;
    //             $tableRow[$i][$params[$key]["name"]] = $params[$key]["value"];
    //         }
    //     }
    //     $result = false;
    //     Db::startTrans();
    //     try {
    //         $result = $this->singleModel->allowField(true)->saveAll(array_values($tableRow));
    //         Db::commit();
    //     } catch (PDOException $e) {
    //         Db::rollback();
    //         return json(["code"=>0,"msg"=>$e->getMessage()]);
    //     } catch (Exception $e) {
    //         Db::rollback();
    //         return json(["code"=>0,"msg"=>$e->getMessage()]);
    //     }
    //     if ($result) {

    //         $dcd_id = $tableRow[0]["DCD_ID"]??0;
    //         if($dcd_id) $this->updateProduct($dcd_id);

    //         return json(["code"=>1,"msg"=>"保存成功"]);
    //     } else {
    //         return json(["code"=>0,"msg"=>__('No rows were deleted')]);
    //     }
        
    // }

    public function sureSaveDetail()
    {
        $params = $this->request->post();
        list($list,$tableList) = array_values($params);
        if(!$list or !$tableList) return json(["code"=>0,"msg"=>"有误请重试"]);
        $tableList = json_decode(str_replace('&quot;','"',$tableList), true);
        $sum = 0;
        $dtm_model = new Dtmaterialdetail([],$this->technology_ex);
        foreach($tableList as $v){
            // $one = $dtm_model->field("DtMD_fUnitWeight")->where("DtMD_ID_PK",$v["DtMD_ID_PK"])->find();
            $sum += round($v["DHS_Count"]*$v["DtMD_fUnitWeight"],2);
        }
        Db::startTrans();
        try {
            
            if(isset($list["DCD_ID"]) and $list["DCD_ID"]=="") unset($list["DCD_ID"]);
            $list["DCD_SWeight"] = $sum;
            $result = $this->detailModel->allowField(true)->saveAll([$list]);
            $DCD_ID = $list["DCD_ID"]??$result[0]["DCD_ID"];
            if($DCD_ID){
                
                $this->singleModel->where("DCD_ID",$DCD_ID)->delete();
                foreach($tableList as $k=>$v){
                    unset($tableList[$k]["DtMD_fUnitWeight"],$tableList[$k]["DHS_ID"]);
                    $tableList[$k]["DCD_ID"] = $DCD_ID;
                }
                $this->singleModel->allowField(true)->saveAll(array_values($tableList));
            }else{
                return json(["code"=>0,"msg"=>'保存失败请重试']);
            }
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if($result !== false){
            // if($DCD_ID) $this->updateProduct($DCD_ID);
            return json(["code"=>1,"msg"=>'保存成功',"dc_num"=>$result[0]["DC_Num"],"dcd"=>$DCD_ID]);
        }else{
            return json(["code"=>0,"msg"=>"保存失败，请重试"]);
        }
        
    }
    //晚点修改
    protected function updateProduct($dcd_id = null)
    {
        $one = $this->detailModel->alias("dd")
                ->join([$this->technology_ex."dhcooperate"=>"d"],"dd.DC_Num = d.DC_Num")
                ->join([$this->technology_ex."producetask"=>"pt"],"d.TD_ID = pt.TD_ID")
                ->field("pt.PT_Num,pt.TD_ID")
                ->where("dd.DCD_ID",$dcd_id)
                ->find();
        if($one){
            $dhone = (new ProduceDh([],$this->technology_ex))->where("PT_Num",$one["PT_Num"])->find();
            if($dhone){
                //整理修改
                $szList = (new ProduceTaskSectShizu([],$this->technology_ex))->field("PT_Sect")->where("PT_Num",$one["PT_Num"])->select();
                $szArr = $dhList = [];
                foreach($szList as $v){
                    $szArr[] = $v["PT_Sect"];
                }
                $dhList = $detailList = $dcd_list = [];
                $dh = $this->detailModel->alias("dhd")
                    ->join([$this->technology_ex."dhcooperate"=>"dhp"],"dhd.DC_Num = dhp.DC_Num")
                    ->field("dhd.DCD_ID,dhd.DCD_Count,dhd.DCD_PartName")
                    ->where("dhp.TD_ID",$one["TD_ID"])
                    ->select();
                foreach($dh as $v){
                    $dcd_list[$v["DCD_ID"]] = $v["DCD_ID"];
                    $dhList[] = [
                        "DCD_ID" => $v["DCD_ID"],
                        "PDH_Count" => $v["DCD_Count"],
                        "BCDCode" => "",
                        "PT_Num" => $one["PT_Num"],
                        "IsSZ" => in_array($v["DCD_PartName"],$szArr)?1:0,
                        "SCD_ID" => 0,
                        "SCD_TPNum" => ""
                    ];
                }
                $detail = $this->singleModel->alias("dhs")
                    ->join([$this->technology_ex."dtmaterialdetial"=>"dtd"],"dhs.DtMD_ID_PK = dtd.DtMD_ID_PK")
                    ->join([$this->technology_ex."dtsect"=>"dts"],"dts.DtS_ID_PK = dtd.DtMD_iSectID_FK")
                    ->field($one["PT_Num"]." as PT_Num,dts.DtS_Name as PT_Sect,dtd.DtMD_iUnitCount as PTD_Count,dtd.DtMD_fUnitWeight as PTD_SWeight,(dtd.DtMD_iUnitCount*dtd.DtMD_fUnitWeight) as PTD_sumWeight,1 as tpaNum,'公司' as DD_Name,dtd.DtMD_ID_PK,dtd.DtMD_sMaterial as PTD_Material,dtd.DtMD_sSpecification as PTD_Specification,0 as PTD_Flag,1 as PTD_Valid")
                    ->where("dhs.DCD_ID","IN",array_values($dcd_list))
                    ->select();
                foreach($detail as $k=>$v){
                    $detailList[$k] = $v->toArray(); 
                    $detailList[$k]["PTD_SWeight"] = $v["PTD_SWeight"]==""?0:$v["PTD_SWeight"];
                    $detailList[$k]["PTD_sumWeight"] = $v["PTD_sumWeight"]==""?0:$v["PTD_sumWeight"];
                    $detailList[$k]["IF_Shi"] = in_array($v["PT_Sect"],$szArr)?1:0;
                }
                Db::startTrans();
                try {
                    $this->dhModel->where("PT_Num",$one["PT_Num"])->delete();
                    $this->detailModel->where("PT_Num",$one["PT_Num"])->delete();
                    $result = $this->dhModel->saveAll($dhList);
                    $result = $this->detailModel->saveAll($detailList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                } catch (PDOException $e) {
                    Db::rollback();
                } catch (Exception $e) {
                    Db::rollback();
                }
            }
        }
    }

    public function delDetail($DCD_ID=NULL)
    {
        // $params = $this->request->post("num");
        if(!$DCD_ID) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);
        $dcd_id_list = explode(",",$DCD_ID);
        Db::startTrans();
        try {
            $this->detailModel->where("DCD_ID", "IN", $dcd_id_list)->delete();
            $this->singleModel->where("DCD_ID", "IN", $dcd_id_list)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        return json(["code"=>1,"msg"=>"删除成功","DCD_ID"=>$DCD_ID]);
    }

    public function selectTower(){
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\lofting\Library([],$this->technology_ex))
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $year = date("Y");
                $find = $this->model->field("DC_Num")->where("DC_Num","like","DH".$year.'%')->order("DC_Num DESC")->find();
                if($find) $DC_Num = "DH".(substr($find["DC_Num"],2)+1);
                else $DC_Num = "DH".$year."0001";
                $params["DC_Num"] = $DC_Num;
                $params["DC_EditDate"] = date("Y-m-d H:i:s");
                $params["DC_Editer"] = $this->admin["nickname"];
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('success',null,$DC_Num);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    public function selectProject()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->dtmaterModel->alias("d")
                ->join(["taskdetail"=>"td"],"d.TD_ID=td.TD_ID")
                ->join(["task"=>"t"],"td.T_Num=t.T_Num")
                ->join(["compact"=>"c"],"t.C_Num=c.C_Num")
                ->field("d.DtM_iID_PK,d.TD_ID,d.T_Company,t.C_Num,t.T_Num as 't.Tum',t.T_Num,d.DtM_sSortProject,d.DtM_sProject,d.DtM_sTypeName,d.DtM_sPressure,c.Customer_Name as CustomerName,c.PC_Num,c.C_SaleType,td.P_Num")
                ->where("NOT EXISTS ( SELECT 'x' FROM dhcooperate dh WHERE d.TD_ID=dh.TD_ID )")
                ->where($where)
                ->order($sort, $order)
                // ->select(false);
                ->paginate($limit);
            $dList = $tdList = [];
            foreach($list as $v){
                $dList[$v["TD_ID"]] = $v->toArray();
                $dList[$v["TD_ID"]]["TD_Count"] = 0;
                $tdList[$v["TD_ID"]] = $v["TD_ID"];
            }
            $merge_list = (new MergTypeName())->where("mTD_ID","IN",array_keys($dList))->select();
            foreach($merge_list as $v){
                $dList[$v["mTD_ID"]]["list"][$v["TD_ID"]] = $v["TD_ID"];
                $tdList[$v["TD_ID"]] = $v["mTD_ID"];
            }

            $countList = (new \app\admin\model\chain\sale\TaskHeight())
                ->field("sum(TD_Count) as TD_Count,TD_ID")
                ->where("TD_ID","in",array_keys($tdList))
                ->group("TD_ID")
                ->select();
            foreach($countList as $v){
                $dList[$tdList[$v["TD_ID"]]]["TD_Count"] += $v["TD_Count"];
            }
            $result = array("total" => $list->total(), "rows" => array_values($dList));

            return json($result);
        }
        return $this->view->fetch();
    }

    public function typeList($DCD_ID = null)
    {
        $row = $this->detailModel->where("DCD_ID",$DCD_ID)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $result = $this->detailModel->where("DCD_ID",$DCD_ID)->update($params);
                if ($result != false) {
                    $this->success("成功",'',$params["DCD_Type"]);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = [
            ""=>"请选择",
            "主管焊件"=>"主管焊件",
            "支管 两头插板"=>"支管 两头插板",
            "支管 两头插板中间节点板"=>"支管 两头插板中间节点板",
            "支管 两头插板中间相贯管"=>"支管 两头插板中间相贯管",
            "支管 两头法兰"=>"支管 两头法兰",
            "支管 两头法兰中间节点板"=>"支管 两头法兰中间节点板",
            "支管 两头法兰中间相贯管"=>"支管 两头法兰中间相贯管",
            "支管 一头法兰一头插板"=>"支管 一头法兰一头插板",
            "支管 一头法兰 一头插板中间节点板"=>"支管 一头法兰 一头插板中间节点板",
            "支管 一头法兰一头插板中间相贯管"=>"支管 一头法兰一头插板中间相贯管",
            "扶手焊件"=>"扶手焊件",
            "爬梯焊件"=>"爬梯焊件",
            "联板角钢焊件"=>"联板角钢焊件",
            "其他焊件"=>"其他焊件",
        ];
        $this->view->assign("row",$row);
        $this->view->assign("list",$list);
        return $this->view->fetch();
    }

    public function memoList($DCD_ID = null)
    {
        $row = $this->detailModel->where("DCD_ID",$DCD_ID)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $result = $this->detailModel->where("DCD_ID",$DCD_ID)->update($params);
                if ($result != false) {
                    $this->success("成功",'',$params["DCD_Memo"]);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = [
            "带脚钉",
            "两端焊",
            "组焊",
            "中间焊",
            "自焊",
            "开口焊",
            "焊塔脚",
            "假脚",
            "一级焊缝",
            "二级焊缝",
            "一端焊"
        ];
        $this->view->assign("row",$row);
        $this->view->assign("list",$list);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $dhcooperatek_one = (new ProduceTask([],$this->technology_ex))->alias("dk")->join([$this->technology_ex."dhcooperate"=>"f"],"f.TD_ID=dk.TD_ID")->where("f.DC_Num", $ids)->find();
            if($dhcooperatek_one) $this->error("已下达，无法删除");
            $list = $this->model->where($pk, 'in', $ids)->select();

            $detailDel = $singleDel = [];
            $joinDetailList = $this->detailModel->field("DCD_ID")->where($pk, 'in', $ids)->select();
            foreach($joinDetailList as $v){
                $detailDel[] = $v["DCD_ID"];
            }
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                }
                $detailCount = $this->detailModel->where($pk, 'in', $ids)->delete();
                !empty($detailDel)?$singleCount = $this->singleModel->where("DCD_ID",'in',$detailDel)->delete():"";
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    protected function memo()
    {
        $list = ["带脚钉","组焊","自焊","焊塔脚","一级焊缝","二级焊缝","一端焊","二端焊","中间焊","开口焊"];
        return $list;
    }

}
