<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class TowerSection extends Technology
{
    protected $sectModel = null,$taskDModel = null, $taskHModel = null, $taskSModel = null;
    protected $admin;
    protected $noNeedLogin = ["contrast","selectProject"];
    //名称 字段 类型 规则 默认值
    protected $tableArr = [
        ["SCD_ID","SCD_ID","text","readonly","hidden","60"],
        ["TH_ID","TH_ID","text","readonly","hidden","60"],
        ["杆塔号","SCD_TPNum","text","readonly","","80"],
        ["*呼高","TH_Height","text","readonly","","60"],
        ["专用段","SCD_SpPart","text","","","150"],
        // ["*电压等级","TD_Pressure","text","data-rule='required' readonly","","60"],
        // ["*塔型","TD_TypeName","text","data-rule='required' readonly","","60"],
        // ["*产品名称","SCD_ProductName","text","data-rule='required' readonly","","60"],
        ["*单位","SCD_Unit","text","readonly","","60"],
        ["*数量","task_count","text","readonly","","60"],
        ["单重(kg)","SCD_Weight","text","","","80"],
        ["单价","SCD_UnitPrice","text","","","60"],
        ["总重量(kg)","Sum_Weight","text","","","80"],
        ["备注","SCD_Memo","text","","","150"],
        ["标段","BiaoDuan","text","","","60"],
        ["线路","SCD_lineName","text"," readonly","","60"],
        ["A段位","SCD_APart","text","","","60"],
        ["A腿长(米)","SCD_ALength","text","","","60"],
        ["B段位","SCD_BPart","text","","","60"],
        ["B腿长(米)","SCD_BLength","text","","","60"],
        ["C段位","SCD_CPart","text","","","60"],
        ["C腿长(米)","SCD_CLength","text","","","60"],
        ["D段位","SCD_DPart","text","","","60"],
        ["D腿长(米)","SCD_DLength","text","","","60"],
        ["转角度数","SCD_Corner","text","","","60"],
        ["基础形式","SCD_BStyle","text","","","60"],
        ["挂点","SCD_MountPoint","text","","","60"],
        ["交货日期","SCD_SendDate","text","","","100"],
        ["基础半根开","SCD_BGK","text","","","60"],
        ["地脚螺栓直径","SCD_StoneBoltZJ","text","","","60"]
        
    ];

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_field = \think\Session::get('technology_field');
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_type = \think\Session::get('technology_type');
        // $this->compactModel = new \app\admin\model\chain\sale\Compact;
        $this->sectModel = new \app\admin\model\chain\sale\Sectconfigdetail;
        // $this->Model = new \app\admin\model\chain\sale\Task;
        $this->taskDModel = new \app\admin\model\chain\sale\TaskDetail;
        $this->taskHModel = new \app\admin\model\chain\sale\TaskHeight;
        $this->taskSModel = new \app\admin\model\chain\lofting\TaskSect;

        $this->admin = \think\Session::get('admin');
        
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->taskDModel->alias("d")
                ->join(["task"=>"t"],"d.T_Num=t.T_Num","left")
                ->join(["compact"=>"c"],"c.C_Num=t.C_Num","left")
                ->field("d.TD_ID,t.C_Num,t.C_Num AS 't.C_Num',c.PC_Num,t.T_Num,t.T_Num as 't.T_Num',t.T_Sort,t.T_Company,t.T_WriterDate,d.TD_TypeName as 'd.TD_TypeName',t.t_project,d.TD_TypeName,d.TD_Pressure")
                ->where("c.produce_type",$this->technology_type)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $dList = [];
            foreach($list as $v){
                $dList[$v["TD_ID"]] = $v->toArray();
                $dList[$v["TD_ID"]]["TD_TypeNameGY"] = $dList[$v["TD_ID"]]["TD_TypeName"];
                $dList[$v["TD_ID"]]["T_WriterDate"] = date("Y-m-d",strtotime($v["T_WriterDate"]));
                $dList[$v["TD_ID"]]["TD_Count"] = 0;
                $dList[$v["TD_ID"]]["state"] = "未下达";
            }
            $countList = $this->taskHModel
                ->field("sum(TD_Count) as TD_Count,TD_ID")
                ->where("TD_ID","in",array_keys($dList))
                ->group("TD_ID")
                ->select();
            foreach($countList as $v){
                $dList[$v["TD_ID"]]["TD_Count"] = $v["TD_Count"];
            }
            $stateList = (new Dtmaterial())->field("TD_ID")->where("TD_ID","in",array_keys($dList))->select();
            foreach($stateList as $k=>$v){
                isset($dList[$v["TD_ID"]])?$dList[$v["TD_ID"]]["state"] = "已下达":"";
            }
            $rows = (new \app\admin\model\chain\lofting\MergTypeName())->field("TD_ID")->where("TD_ID","in",array_keys($dList))->select();
            foreach($rows as $v){
                isset($dList[$v["TD_ID"]])?$dList[$v["TD_ID"]]["state"] = "已下达":"";
            }
            $result = array("total" => $list->total(), "rows" => array_values($dList));

            return json($result);
        }
        return $this->view->fetch();
    }

    public function contrast()
    {
        $ids = $this->request->post("ids");
        if(!$ids) return json(["code"=>0,"data"=>[]]);
        $tableList = $this->sectModel->alias("s")
            ->join(["taskheight"=>"h"],"s.TH_ID=h.TH_ID")
            ->field("s.*,(s.SCD_Weight*s.SCD_Count) as Sum_Weight")
            ->where("h.TD_ID",$ids)
            ->select();
        $data = [];
        foreach($tableList as $k=>$v){
            $data[$k] = $v->toArray();
            $data[$k]["AuditeDate"] = $v["Auditor"]?date("Y-m-d",strtotime($v["AuditeDate"])):"";
            $data[$k]["WriteDate"] = date("Y-m-d",strtotime($v["WriteDate"]));
        }
        return json(["code"=>1,"data"=>array_values($data)]);
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->taskDModel->alias("d")
            ->join(["task"=>"t"],"d.T_Num=t.T_Num","left")
            ->join(["compact"=>"c"],"c.C_Num=t.C_Num","left")
            ->field("d.TD_ID,t.C_Num,c.PC_Num,t.t_shortproject,c.Customer_Name,t.T_Num,t.T_Sort,t.T_Company,t.T_WriterDate,t.t_project,d.TD_TypeName,d.TD_Pressure")
            ->where("d.TD_ID",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $commonList = [];
                if($params["SCD_Part"]==="0") $commonField=0;
                else{
                    $commonField = str_replace(" ","",$params["SCD_Part"]);
                    $commonField = trim(str_replace("，",",",$params["SCD_Part"]),',');
                }
                $tableRow = [];
                $secd_id = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $tableRow[$kk]["SCD_Part"] = $commonField;
                        $tableRow[$kk][$k] = $vv;
                    }
                }
                foreach($tableRow as $k=>$v){
                    $secd_id[] = $v["SCD_ID"];
                    $tableRow[$k]["SCD_Part"] = $v["SCD_Part"] = trim(str_replace("，",",",$v["SCD_Part"]),',');
                    $tableRow[$k]["SCD_SpPart"] = $v["SCD_SpPart"] = trim(str_replace("，",",",$v["SCD_SpPart"]),',');
                    $scd_part = explode(",",trim(str_replace("，",",",$v["SCD_Part"]),','));
                    foreach($scd_part as $sv){
                        $duan_count = explode("*",trim($sv,','));
                        $sectSave[$v["SCD_ID"]][$duan_count[0]] = [
                            "TS_Name" => $duan_count[0],
                            "TS_Count" => isset($duan_count[1])?($duan_count[1]*$v["task_count"]):$v["task_count"],
                            "SCD_ID" => $v["SCD_ID"],
                            "ifBody" => "1",
                        ];
                    }
                    if($v["SCD_SpPart"]!=''){
                        $scd_sppart = explode(",",trim($v["SCD_SpPart"],','));
                        foreach($scd_sppart as $sv){
                            $duan_count = explode("*",trim(str_replace("，",",",$sv),','));
                            //不知道是不是 有待考究
                            // isset($sectSave[$v["SCD_ID"]][$duan_count[0]])?$duan_count[0] .= '(1)':'';
                            $sectSave[$v["SCD_ID"]][$duan_count[0]] = [
                                "TS_Name" => $duan_count[0],
                                "TS_Count" => isset($duan_count[1])?($duan_count[1]):(1),
                                "SCD_ID" => $v["SCD_ID"],
                                "ifBody" => "0",
                            ];
                        }
                    }
                }
                foreach($sectSave as $k=>$v){
                    foreach($v as $kk=>$vv){
                        array_push($commonList,$vv);
                    }
                }
                $result = false;
                Db::startTrans();
                try {
                    
                    // $result = $this->taskDModel->allowField(true)->save($params);
                    if(!empty($tableRow)) $this->sectModel->allowField(true)->saveAll($tableRow);
                    if(!empty($commonList)) {
                        $this->taskSModel->where("SCD_ID",'in',$secd_id)->delete();
                        $result = $this->taskSModel->allowField(true)->saveAll($commonList);
                    }
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $countList = $this->taskHModel
            ->field("sum(TD_Count) as TD_Count,TD_ID,TH_ID")
            ->where("TD_ID",$ids)
            ->find();
        $secList = [];
        if($countList){
            $secList = $this->sectModel
                ->field("SCD_Part,Auditor")
                ->where("TH_ID",$countList["TH_ID"])
                ->find();
        }
        $row["TD_Count"] = $countList["TD_Count"]??0;
        $row["SCD_Part"] = $secList["SCD_Part"]!==''?str_replace(",","，",$secList["SCD_Part"]):"";
        $row["Auditor"] = $secList["Auditor"]??"";
        $tableList = $this->sectModel->alias("s")
                ->join(["taskheight"=>"h"],"s.TH_ID=h.TH_ID")
                ->field("s.*,(s.SCD_Weight*s.SCD_Count) as Sum_Weight")
                ->where("h.TD_ID",$ids)
                ->select();
        foreach($tableList as $k=>$v){
            $v["SCD_SpPart"] = $v["SCD_SpPart"]?str_replace(",","，",$v["SCD_SpPart"]):"";
            $tableList[$k] = $v->toArray();
        }
        $this->view->assign("tableArr",$this->tableArr);
        $this->view->assign("tableList",$tableList);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function selectProject()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }

            $listDt = (new Dtmaterial([],$this->technology_ex))->field("TD_ID")->select();
            $listDtList = [];
            foreach($listDt as $v){
                $listDtList[] = $v["TD_ID"];
            }

            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->taskDModel->alias("d")
                ->join(["task"=>"t"],"d.T_Num=t.T_Num","left")
                ->join(["compact"=>"c"],"c.C_Num=t.C_Num","left")
                ->join(["newoldtdtypename"=>"nn"],"nn.TD_ID=d.TD_ID","left")
                ->field("d.P_Num,d.TD_ID,t.C_Num,t.C_Num AS 't.C_Num',c.PC_Num,t.T_Num,t.T_Num AS 't.T_Num',t.T_Sort,t.T_Company,t.T_WriterDate,t.t_project,t.t_shortproject,c.Customer_Name,d.TD_TypeName,d.TD_TypeName AS 'd.TD_TypeName',d.TD_Pressure,nn.old_TypeName")
                ->where("d.TD_ID","not in",$listDtList)
                ->where($where)
                ->where("c.produce_type",$this->technology_type)
                ->order($sort, $order)
                ->paginate($limit);
            $dList = [];
            foreach($list as $v){
                $dList[$v["TD_ID"]] = $v->toArray();
                $dList[$v["TD_ID"]]["TD_TypeNameGY"] = $v["old_TypeName"];
                $dList[$v["TD_ID"]]["TD_Count"] = 0;
            }
            $countList = $this->taskHModel
                ->field("sum(TD_Count) as TD_Count,TD_ID")
                ->where("TD_ID","in",array_keys($dList))
                ->group("TD_ID")
                ->select();
            foreach($countList as $v){
                $dList[$v["TD_ID"]]["TD_Count"] = $v["TD_Count"];
            }
            $result = array("total" => $list->total(), "rows" => array_values($dList));

            return json($result);
        }
        return $this->view->fetch();
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $list = $this->taskHModel->field("TH_ID")->where("TD_ID",$num)->select();
        $th_id_list = [];
        foreach($list as $v){
            $th_id_list[] = $v["TH_ID"];
        }
        $result = $this->sectModel->where("TH_ID","IN",$th_id_list)->update(['Auditor'=>$admin,"AuditeDate"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }

    public function giveUp()
    {
        // echo 1;
        $num = $this->request->post("num");
        $list = $this->taskHModel->field("TH_ID")->where("TD_ID",$num)->select();
        $th_id_list = [];
        foreach($list as $v){
            $th_id_list[] = $v["TH_ID"];
        }
        $result = $this->sectModel->where("TH_ID","IN",$th_id_list)->update(['Auditor'=>""]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"success"]);
        } else {
            return json(["code"=>0,"msg"=>"error"]);
        }
    }
}
