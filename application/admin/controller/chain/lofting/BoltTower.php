<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\BoltFastCreateDetail;
use app\admin\model\chain\lofting\BoltFastCreateMain;
use app\admin\model\chain\sale\Sectconfigdetail;
use app\admin\model\jichu\ch\AtBoltFastInput;
use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 螺栓塔型库主管理
 *
 * @icon fa fa-circle-o
 */
class BoltTower extends Technology
{
    
    /**
     * BoltTower模型对象
     * @var \app\admin\model\chain\lofting\BoltTower
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_type = \think\Session::get('technology_type');
        $this->model = new \app\admin\model\chain\lofting\BoltTower([],$this->technology_ex);
        $this->sectModel = new \app\admin\model\chain\lofting\BoltTowerSect([],$this->technology_ex);
        $this->detailModel = new \app\admin\model\chain\lofting\BoltTowerDetail([],$this->technology_ex);
        $this->hbModel = new \app\admin\model\chain\lofting\HeightBolt([],$this->technology_ex);
        $this->bsectModel = new \app\admin\model\chain\lofting\BoltSect([],$this->technology_ex);
        $this->bdetailModel = new \app\admin\model\chain\lofting\BoltDetail([],$this->technology_ex);
        $this->fastDetailModel = new BoltFastCreateDetail([],$this->technology_ex);
        $this->fastMainModel = new BoltFastCreateMain([],$this->technology_ex);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = false;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->paginate($limit);

            foreach ($list as $row) {
                $row->visible(['BTXK_ID','Writer','WriteDate','BTXK_Memo','TD_TypeName','TD_Pressure']);
                
            }

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('成功！',null,$result["BTXK_ID"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = ["writer"=>$this->admin["nickname"],"time"=>date("Y-m-d H:i:s")];
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->where("BTXK_ID",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $sectSaveList = [];
                        
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $bjhList = [];
                foreach($sectSaveList as $k=>$v){
                    if(isset($bjhList[$v["BTXKS_SectName"]])) return json(["code"=>0,'msg'=>"段号有重复，请进行修改！"]);
                    else $bjhList[$v["BTXKS_SectName"]] = $v["BTXKS_SectName"];
                    if($v["BTXKS_ID"]==0) unset($sectSaveList[$k]["BTXKS_ID"],$v["BTXKS_ID"]);
                    foreach($v as $kk=>$vv){
                        if($vv=='') unset($sectSaveList[$k][$kk]);
                    }
                    $sectSaveList[$k]["BTXK_ID"] = $ids;
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->allowField(true)->where("BTXK_ID",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->sectModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $sectArr = [];
        $number = $weight = 0;
        $sectList = $this->sectModel->alias("s")
            ->join([$this->technology_ex."bolttxkdetail"=>'d'],"s.BTXKS_ID = d.BTXKS_ID","left")
            ->field("s.*,sum(d.BD_SumCount) as count,sum(d.BD_SumWeight) as weight,CAST(s.BTXKS_SectName as UNSIGNED) AS number")
            ->where("s.BTXK_ID",$ids)
            ->group("s.BTXKS_ID")
            ->order("number,s.BTXKS_SectName asc")
            ->select();
        foreach($sectList as $v){
            $v = $v->toArray();
            $v["count"] = $v["count"]?$v["count"]:0;
            $v["weight"] = $v["weight"]?$v["weight"]:0;
            $v["number"] = $v["number"]?$v["number"]:0;
            $sectArr[] = $v;
            $number += $v["count"];
            $weight += $v["weight"];
            
        }
        $count = count($sectArr);
        $tableField = $this->getTableField();
        $this->view->assign("allCount",["count"=>$count,"number"=>$number,"weight"=>$weight]);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("row", $row);
        $this->view->assign("sectList", $sectArr);
        $this->view->assign("ids", $ids);
        return $this->view->fetch();
    }

    //编辑table中某一个detail的删除
    public function delDetail()
    {
        $num = $this->request->post("num");
        if($num){
            Db::startTrans();
            try {
                // $detailList = $this->detailModel->field("DtMD_ID_PK")->where("DtMD_iSectID_FK",$num)->select();
                // $detailId = [];
                // foreach($detailList as $v){
                //     $detailId[] = $v["DtMD_ID_PK"];
                // }
                $this->sectModel->where("BTXKS_ID",$num)->delete();
                $this->detailModel->where("BTXKS_ID",$num)->delete();
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>1,'msg'=>"成功！"]);
    }

    //编辑table中某一个detail的编辑页面
    public function detail($ids=null)
    {
        $row = $this->sectModel->where("BTXKS_ID",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        
        if ($this->request->isPost()) {
            $params = $this->request->post("data");
            $params = json_decode(str_replace('&quot;','"',$params), true);
            if ($params) {
                $detailSaveList = $keyValue = [];
                $key = "";
                foreach($params as $v){
                    $key = rtrim($v["name"],"[]");
                    isset($keyValue[$key])?"":$keyValue[$key] = [];
                    $keyValue[$key][] = $v["value"];
                }
                foreach($keyValue as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $detailSaveList[$kk][$k] = $vv;
                    }
                }
                $contentList = [];
                foreach($detailSaveList as $k=>$v){
                    unset($detailSaveList[$k]["BTXKD_ID"],$v["BTXKD_ID"]);
                    foreach(["BD_SJSumCount","BD_SJSingelWeight","BD_SumCount","BD_SingelWeight"] as $vv){
                        $v[$vv] = $v[$vv]?$v[$vv]:0;
                    }

                    $key = $v["BD_MaterialName"].'-'.$v["BD_Type"].'-'.$v["BD_Limber"].'-'.$v["BD_Lenth"];
                    if(!isset($contentList[$key])){
                        $contentList[$key] = $v;
                    }else{
                        foreach(["BD_SJSumCount","BD_SJSingelWeight","BD_SumCount","BD_SingelWeight"] as $vvv){
                            $contentList[$key][$vvv] += $v[$vvv];
                        }
                    }
                    $contentList[$key]["BD_SJSumWeight"] = round($contentList[$key]["BD_SJSumCount"]*$contentList[$key]["BD_SJSingelWeight"],2);
                    $contentList[$key]["BD_SumWeight"] = round($contentList[$key]["BD_SumCount"]*$contentList[$key]["BD_SingelWeight"],2);
                    $contentList[$key]["BTXKS_ID"] = $ids;
                }
                Db::startTrans();
                try {
                    $this->detailModel->where("BTXKS_ID",$ids)->delete();
                    $result = $this->detailModel->allowField(true)->saveAll(array_values($contentList));
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (PDOException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (Exception $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                }
                if ($result) {
                    return json(["code"=>1,'msg'=>"保存成功"]);
                } else {
                    return json(["code"=>0,'msg'=>"保存失败"]);
                }
            }
            return json(["code"=>0,'msg'=>__('Parameter %s can not be empty')]);
        }
        // $BTXKS_SectName = $row["BTXKS_SectName"];
        $BTXK_ID = $row["BTXK_ID"];
        $sectArr = [];
        $sectList = $this->sectModel->field("BTXKS_SectName,BTXKS_ID,CAST(BTXKS_SectName AS UNSIGNED) AS number")->where("BTXK_ID",$BTXK_ID)->order("number,BTXKS_SectName ASC")->select();
        foreach($sectList as $v){
            $sectArr[$v["BTXKS_ID"]] = $v["BTXKS_SectName"];
        }


        $tableField = $this->getDetailField();
        foreach($tableField as $v){
            $v[0]!=""?$field[] = $v[1]:"";
        }
        $detailArr = Db::query("select S.BTXKS_SectName,B.*,case when BD_Flag=0 then '普通' else '防盗' end as BD_sFlag,case when BD_MaterialName='螺栓' then 0  when BD_MaterialName='脚钉' then 1  when BD_MaterialName='垫圈'  then 2 when instr('扣紧',BD_MaterialName)>0 then 3 when BD_MaterialName='防盗帽圈' then 4 else 5 end as materialSort, BD_Type  from ".$this->technology_ex."bolttxkdetail B inner join ".$this->technology_ex."bolttxksect S on B.BTXKS_ID=S.BTXKS_ID  where  (1=1)   and   B.BTXKS_ID=:ids  order by BD_Flag,MaterialSort,BD_MaterialName,BD_Limber,BD_Type,BD_Other",["ids"=>$ids]);
        // $detailList = $this->detailModel->field(implode(",",$field))->where("BTXKS_ID",$ids)->order("BD_MaterialName,BD_Type ASC")->select();
        // $detailArr = [];
        // foreach($detailList as $k=>$v){
        //     $detailArr[$k] = $v->toArray();
        // }
        $this->view->assign("sectArr", $sectArr);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("row", $row);
        $this->view->assign("ids", $ids);
        // $this->view->assign("BTXKS_SectName", $BTXKS_SectName);

        $this->view->assign("detailList",$detailArr);
        
        return $this->view->fetch();
    }

    public function detailTable()
    {
        $params = $this->request->post();
        //一级id 二级id 二级change——id
        list($ids,$detail_id,$searchNum) = array_values($params);
        if(!$ids or !$detail_id or !$searchNum) return json(["code"=>0,"msg"=>"error"]);
        $BTXKS_one = $this->sectModel->find($searchNum);
        $BTXKS_SectName = $BTXKS_one["BTXKS_SectName"];
        $tableField = $this->getDetailField();
        foreach($tableField as $v){
            $v[0]!=""?$field[] = $v[1]:"";
        }
        unset($field[2]);
        $detailList = $this->detailModel->field(implode(",",$field))->where("BTXKS_ID",$searchNum)->order("BD_MaterialName,BD_Type ASC")->select();
        $tableContent = '';
        foreach($detailList as $k=>$v){
            $v["BD_sFlag"] = $v["BD_Flag"]==1?"防盗":"普通";
            $tableContent .= '<tr><td>'.($k+1).'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td><td>'. $BTXKS_SectName.'</td>';
            foreach($tableField as $vv){
                if($vv[1]=="") $tableContent .= '<td><input type="text" class="small_input" value=""></td>';
                elseif($vv[1]=="BD_sFlag") $tableContent .= '<td '.$vv[4].'><input type="'.$vv[2].'" readonly class="small_input" data-rule="'.$vv[3].'" name="'.$vv[1].'[]" value="'.($v[$vv[1]]??$v[$vv[1]]).'"></td>';
                else $tableContent .= '<td '.$vv[4].'><input type="'.$vv[2].'" class="small_input" data-rule="'.$vv[3].'" name="'.$vv[1].'[]" value="'.($v[$vv[1]]??$v[$vv[1]]).'"></td>';
            }
            $tableContent .="</tr>";
        }
        return json(["code"=>1,"data"=>$tableContent]);
    }

    public function rightTable()
    {
        $params = $this->request->post();
        $where = [];
        foreach($params as $k=>$v){
            if($v!="") $where[$k] = ["like","%".$v."%"];
        }
        $list = (new AtBoltFastInput())->where($where)->order("FastInPut,BD_MaterialName,BD_Limber,BD_Type ASC")->select();
        return json(["code"=>1,"data"=>$list]);
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            Db::startTrans();
            try {
                $this->detailModel->where("BTXKD_ID",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    //配置杆塔螺栓数
    public function configure($ids='')
    {
        if(!$ids) $this->error(__('No Results were found'));
        $one = $this->model->WHERE("BTXK_ID",$ids)->FIND();
        if($one){
            $this->view->assign("ids",$ids);
            $this->view->assign("tower",$one["TD_TypeName"]);
            return $this->view->fetch();
        }else{
            $this->error(__('No Results were found'));
        }
        
    }

    //配置杆塔螺栓数选择
    public function chooseCompact($tower='')
    {
        if(!$tower) $this->error(__('No Results were found'));
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\sale\TaskDetail())->alias("d")
                ->join(["task"=>"t"],"d.T_Num=t.T_Num","left")
                ->join(["compact"=>"c"],"c.C_Num=t.C_Num","left")
                ->field("d.TD_ID as TD_ID,t.C_Num as C_Num,c.PC_Num,t.T_Num,t.T_Sort,t.T_Company,t.T_WriterDate,t.t_project,t.t_shortproject,c.Customer_Name,d.TD_TypeName,d.TD_Pressure")
                ->where("c.produce_type",$this->technology_type)
                // ->where("d.TD_TypeName",$tower)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $dList = [];
            foreach($list as $v){
                $dList[$v["TD_ID"]] = $v->toArray();
                $dList[$v["TD_ID"]]["TD_TypeNameGY"] = $dList[$v["TD_ID"]]["TD_TypeName"];
                $dList[$v["TD_ID"]]["TD_Count"] = 0;
            }
            $countList = (new \app\admin\model\chain\sale\TaskHeight())
                ->field("count(TD_Count) as TD_Count,TD_ID")
                ->where("TD_ID","in",array_keys($dList))
                ->group("TD_ID")
                ->select();
            foreach($countList as $v){
                $dList[$v["TD_ID"]]["TD_Count"] = $v["TD_Count"];
            }
            $result = array("total" => $list->total(), "rows" => array_values($dList));

            return json($result);
        }
        $this->view->assign("tower",$tower);
        $this->assignconfig("TD_TypeName",$tower);
        return $this->view->fetch();
    }

    public function sectconfigdetail()
    {
        $TD_ID = $this->request->post("TD_ID");
        if(!$TD_ID) return json(["code"=>0,"msg"=>"error"]);
        $list = (new Sectconfigdetail())->alias("scd")
            ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID",'left')
            ->join([$this->technology_ex."boltfastcreatemain"=>"bfct"],"bfct.SCD_ID = scd.SCD_ID","left")
            ->field("scd.SCD_ID,scd.SCD_TPNum,scd.TH_Height,scd.SCD_Part,scd.SCD_SpPart,bfct.BTXK_ID")
            ->where("th.TD_ID",$TD_ID)
            ->order("scd.SCD_ID asc")
            ->select();
        $data = [];
        foreach($list as $k=>$v){
            $data[$k] = $v->toArray();
            $data[$k]["id"] = $k+1;
            $sect = trim($v["SCD_Part"]).','.trim($v["SCD_SpPart"]);
            $sectList = explode(",",$sect);
            $sectArr = [];
            foreach($sectList as $v){
                $sectArr[$v] = $v;
            }
            ksort($sectArr);
            $data[$k]["sect"] = !empty($sectArr)?implode(",",$sectArr):"";
        }
        return json(["code"=>1,"data"=>$data]);
    }

    public function createmain()
    {
        $SCD_ID = $this->request->post("SCD_ID");
        if(!$SCD_ID) return json(["code"=>0,"msg"=>"有误，请刷新后重试"]);
        $list = (new BoltFastCreateMain([],$this->technology_ex))->find($SCD_ID);
        if($list){
            $list = $list->toArray();
            $list["ifDPPDTD_P"] = ($list["ifDPPDTD"]==1 or $list["ifDPPDTD"]==3)?1:0;
            $list["ifDPPDTD_T"] = ($list["ifDPPDTD"]==2 or $list["ifDPPDTD"]==3)?1:0;
            $banfd = $list["BanSect"];
            $banfd_list = [];
            foreach(explode(",",$banfd) as $k=>$v){
                $detail_list = explode("*",trim($v));
                if(isset($detail_list[0]) and $detail_list[0]) $banfd_list[$detail_list[0]] = isset($detail_list[1])?$detail_list[1]:1;
            }
            $table = "";
            if($banfd){
                $where = [
                    "SCD_ID" => ['=',$SCD_ID],
                    "SetSect" => ['IN',array_keys($banfd_list)]
                ];
                $detailList = (new BoltFastCreateDetail([],$this->technology_ex))->where($where)->order("SetSect,BD_MaterialName,BD_Limber,BD_Type,BD_Other ASC")->select();
                $field = ["SetSect","BD_MaterialName","BD_Limber","BD_Type","BD_Lenth","BD_Other","BD_SumCount","BD_PTCount","BD_FDCount","F_ID"];
                foreach($detailList as $k=>$v){
                    $table .= '<tr><td>'.($k+1).'</td><td><a href="javascript:;" class="btn btn-xs btn-danger table_del"><i class="fa fa-trash"></i></a></td>';
                    foreach($field as $vv){
                        $type = "";
                        $readonly = "";
                        if($vv=="F_ID") $type = "hidden";
                        if(!in_array($vv,["BD_PTCount","BD_FDCount","BD_Lenth"])) $readonly = "readonly";
                        $table .= '<td '.$type.'><input name="'.$vv.'" class="small_input" type="text" value="'.$v[$vv].'" '.$readonly.'></td>';
                    }
                    $table .= "</tr>";
                }
            }
            
            $list["table"] = $table;
            return json(["code"=>1,"data"=>$list]);
        }else{
            return json(["code"=>0,"msg"=>"请填写段落"]);
        }
    }

    public function createmainDetail()
    {
        $BTXK_ID = $this->request->post("BTXK_ID");
        $SCD_ID = $this->request->post("SCD_ID");
        $ban = $this->request->post("ban");
        $ban = str_replace("，",",",$ban);
        $sectNameArr = explode(",",trim($ban));
        $sectNameList = [];
        foreach($sectNameArr as $k=>$v){
            $detail_list = explode("*",trim($v));
            if(isset($detail_list[0]) and $detail_list[0]) $sectNameList[$detail_list[0]] = isset($detail_list[1])?$detail_list[1]:1;
        }
        if(!$BTXK_ID or !$sectNameList) return json(["code"=>0,"msg"=>"有误，请刷新后重试"]);
        $where = [
            "BTXK_ID" => ["=",$BTXK_ID],
            "BTXKS_SectName" => ["in",array_keys($sectNameList)]
        ];
        $sectList = $this->sectModel->field("BTXKS_ID,BTXKS_SectName,CAST(BTXKS_SectName AS UNSIGNED) as number")->where($where)->order("number,BTXKS_SectName ASC")->select();
        if(!$sectList) return json(["code"=>0,"msg"=>"无段"]);
        $sectArr = $detailArr = [];
        foreach($sectList as $k=>$v){
            $sectArr[$v["BTXKS_ID"]] = $v["BTXKS_SectName"];
        }
        $detailList = $this->detailModel->where("BTXKS_ID","in",array_keys($sectArr))->order("BD_MaterialName,BD_Limber,BD_Type,BD_Other ASC")->select();
        foreach($detailList as $v){
            // $BD_SumCount = $v["BD_SumCount"]*$sectNameList[$sectArr[$v["BTXKS_ID"]]];
            $BD_SumCount = $v["BD_SumCount"];
            $key = $sectArr[$v["BTXKS_ID"]].'-'.$v["BD_MaterialName"].'-'.$v["BD_Type"].'-'.$v["BD_Limber"].'-'.$BD_SumCount;
            $detailArr[$key] = $v->toArray();
            $detailArr[$key]["SetSect"] = $sectArr[$v["BTXKS_ID"]];
            $detailArr[$key]["BD_SumCount"] = $BD_SumCount;
            $detailArr[$key]["BD_PTCount"] = $BD_SumCount;
            $detailArr[$key]["BD_FDCount"] = 0;
            $detailArr[$key]["F_ID"] = '';
        }
        ksort($detailArr);
        $where = [
            "SCD_ID" => ["=",$SCD_ID],
            "SetSect" => ["IN",$ban]
        ];
        $detailLaterList = (new BoltFastCreateDetail([],$this->technology_ex))->where($where)->order("SetSect,BD_MaterialName,BD_Limber ASC")->select();
        foreach($detailLaterList as $v){
            $key = $v["SetSect"].'-'.$v["BD_MaterialName"].'-'.$v["BD_Type"].'-'.$v["BD_SumCount"];
            isset($detailArr[$key])?$detailArr[$key]= $v->toArray():"";
        }
        $field = ["SetSect","BD_MaterialName","BD_Limber","BD_Type","BD_Lenth","BD_Other","BD_SumCount","BD_PTCount","BD_FDCount","F_ID"];
        $table = "";
        foreach(array_values($detailArr) as $k=>$v){
            $table .= '<tr><td>'.($k+1).'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
            foreach($field as $vv){
                if($vv == "BD_PTCount" or $vv=="BD_FDCount") $table .= '<td><input name="'.$vv.'" class="small_input" type="text" value="'.$v[$vv].'"></td>';
                elseif($vv=="F_ID") $table .= '<td hidden><input name="'.$vv.'" class="small_input" type="text" value="'.$v[$vv].'"></td>';
                else $table .= '<td ><input name="'.$vv.'" class="small_input" type="text" value="'.$v[$vv].'" readonly></td>';
            }
            $table .= "</tr>";
        }
        return json(["code"=>1,"data"=>$table]);

    }

    public function configureSave()
    {
        $form = $this->request->post("form");
        $form = json_decode(str_replace('&quot;','"',$form), true);
        $saveMain = [];
        $SCD_ID = $BTXK_ID = $ifDPPDTD_P = $ifDPPDTD_T = 0;
        $sect_ban = $sect_all = $sect_dao = $sect_arr = [];
        foreach($form as $v){
            if($v["name"] == "SCD_ID"){
                $SCD_ID = $v["value"];
            }elseif($v["name"] == "BTXK_ID"){
                $BTXK_ID = $v["value"];
            }elseif($v["name"] == "ifDPPDTD_P"){
                $ifDPPDTD_P = $v["value"];
            }elseif($v["name"] == "ifDPPDTD_T"){
                $ifDPPDTD_T = $v["value"];
            }elseif($v["name"] == "AllPTSect"){
                $v["value"] = str_replace("，",",",trim($v["value"]));
                $arr = explode(",",$v["value"]);
                foreach($arr as $vv){
                    if($vv!=""){
                        $key = explode("*",$vv)[0];
                        $sect_all[$key] = $sect_arr[$key] = $key;
                    }
                }
            }elseif($v["name"] == "AllFDSect"){
                $v["value"] = str_replace("，",",",trim($v["value"]));
                $arr = explode(",",$v["value"]);
                foreach($arr as $vv){
                    if($vv!=""){
                        $key = explode("*",$vv)[0];
                        $sect_dao[$key] = $sect_arr[$key] = $key;
                    }
                }
            }elseif($v["name"] == "BanSect"){
                $v["value"] = str_replace("，",",",trim($v["value"]));
                $arr = explode(",",$v["value"]);
                foreach($arr as $vv){
                    if($vv!=""){
                        $key = explode("*",$vv)[0];
                        $sect_ban[$key] = $sect_arr[$key] = $key;
                    }
                }
            }
            $saveMain[$v["name"]] = $v["value"];
        }
        if(count($sect_arr) != (count($sect_all)+count($sect_dao)+count($sect_ban))) return json(["code"=>0,"msg"=>"请仔细核对段落的填写"]);

        $saveMain["ifDPPDTD"] = $ifDPPDTD_P + $ifDPPDTD_T;
        $saveMain["Writer"] = $this->admin["nickname"];
        unset($saveMain["ifDPPDTD_P"],$saveMain["ifDPPDTD_T"]);
        if(!$SCD_ID or !$BTXK_ID) return json(["code"=>0,"msg"=>"保存失败，请刷新重试"]);

        $table = $this->request->post("table");
        $table = json_decode(str_replace('&quot;','"',$table), true)??[];
        // if(!$table) return json(["code"=>0,"msg"=>"保存失败，请刷新重试"]);
        $saveDetail = [];
        $count = count($table)/10;
        for($i=0;$i<$count;$i++){
            $saveDetail[$i]["SCD_ID"] = $SCD_ID;
            for($j=0;$j<10;$j++){
                $key = 10*$i+$j;
                $saveDetail[$i][$table[$key]["name"]] = $table[$key]["value"];
            }
            if(in_array($saveDetail[$i]["BD_MaterialName"],["扣紧螺母","弹片","垫片","平垫"])){
                $saveDetail[$i]["BD_PTCount"] = $saveDetail[$i]["BD_SumCount"];
                $saveDetail[$i]["BD_FDCount"] = 0;
            }
            unset($saveDetail[$i]["F_ID"]);
        }
        $merge_list = array_merge($sect_all,$sect_dao);
        if($merge_list){
            $where = [
                "BTXK_ID" => ["=",$BTXK_ID],
                "BTXKS_SectName" => ["IN",$merge_list]
            ];
            $detailList = $this->detailModel->alias("dm")
                ->join([$this->technology_ex."bolttxksect"=>"sm"],"dm.BTXKS_ID=sm.BTXKS_ID","left")
                ->field("dm.BD_MaterialName,dm.BD_Limber,dm.BD_Type,dm.BD_Lenth,dm.BD_Other,dm.BD_SumCount,sm.BTXKS_SectName as SetSect")
                ->where($where)
                ->order("sm.BTXKS_SectName,sm.BTXK_ID ASC")
                ->select();
            foreach($detailList as $v){
                if(in_array($v["SetSect"],$sect_all)) $merge_list = ["BD_PTCount"=>$v["BD_SumCount"],"BD_FDCount"=>0,"SCD_ID"=>$SCD_ID];
                else{
                    if(in_array($v["BD_MaterialName"],["扣紧螺母","弹片","垫片","平垫"])) $merge_list = ["BD_PTCount"=>$v["BD_SumCount"],"BD_FDCount"=>0,"SCD_ID"=>$SCD_ID];
                    else $merge_list = ["BD_PTCount"=>0,"BD_FDCount"=>$v["BD_SumCount"],"SCD_ID"=>$SCD_ID];
                }
                $saveDetail[] = array_merge($v->toArray(),$merge_list);
            }
            
        }
        $result = false;
        Db::startTrans();
        try {
            $this->fastMainModel->where("SCD_ID",$SCD_ID)->delete();
            $this->fastDetailModel->where("SCD_ID",$SCD_ID)->delete();
            $result = $this->fastMainModel->insert($saveMain);
            if(!empty($saveDetail)) $result = $this->fastDetailModel->allowField(true)->saveAll($saveDetail);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>'保存成功',"data"=>$saveMain]);
        } else {
            return json(["code"=>0,"msg"=>'保存失败']);
        }
    }

    
    //复制螺栓塔型库
    public function copyLs($TD_TypeName='')
    {
        if($this->request->isPost()){
            $params = $this->request->post("TD_TypeName");
            $where = [];
            if($params) $TD_TypeName = $params;
            else $TD_TypeName="";
            if($TD_TypeName) $where = ["TD_TypeName"=>["LIKE","%".$TD_TypeName."%"]];
            $list = $this->model->where($where)->order("BTXK_ID DESC")->select();
            return json(["code"=>1,"data"=>$list]);
        }
        $this->view->assign("TD_TypeName",$TD_TypeName);
        return $this->view->fetch();
    }

    public function copyLsDetail()
    {
        $BTXK_ID = $this->request->post("BTXK_ID");
        if(!$BTXK_ID) return json(["code"=>0,"msg"=>"有误"]);
        $sectList = $this->sectModel->alias("s")
            ->join([$this->technology_ex."bolttxkdetail"=>'d'],"s.BTXKS_ID = d.BTXKS_ID","left")
            ->field("s.*,sum(d.BD_SumCount) as count,sum(d.BD_SumWeight) as weight,CAST(s.BTXKS_SectName as UNSIGNED) AS number")
            ->where("s.BTXK_ID",$BTXK_ID)
            ->group("s.BTXKS_ID")
            ->order("number,s.BTXKS_SectName asc")
            ->select();
        foreach($sectList as $v){
            $v = $v->toArray();
            $v["count"] = $v["count"]?$v["count"]:0;
            $v["weight"] = $v["weight"]?$v["weight"]:0;
            $v["number"] = $v["number"]?$v["number"]:0;
            $sectArr[] = $v;
        }
        return json(["code"=>1,"data"=>$sectArr]);

    }

    public function copyDetail()
    {
        $params = $this->request->post();
        $ids = $params["ids"];
        $sectId = $params["sectId"];
        if(!$ids or count($sectId)==0) return json(["code"=>0,"msg"=>"复制失败"]);
        $copySect = $this->sectModel->field("*,CAST(BTXKS_SectName AS UNSIGNED) AS number")->where("BTXKS_ID","in",$sectId)->whereOr("BTXK_ID",$ids)->order("number,BTXKS_SectName DESC")->select();
        
        $copySectList = $copySectIdList = $isSectList = $delSectId = [];
        foreach($copySect as $v){
            if($v["BTXK_ID"]!=$ids){
                $copySectList[$v["BTXKS_SectName"]] = [
                    "BTXK_ID" => $ids,
                    "BTXKS_SectName" => $v["BTXKS_SectName"],
                    "BTXKS_Memo" => $v["BTXKS_Memo"]
                ];
                $copySectIdList[$v["BTXKS_ID"]] = $v["BTXKS_SectName"];
            }else{
                $isSectList[$v["BTXKS_SectName"]] = [
                    "BTXKS_ID" => $v["BTXKS_ID"],
                    "BTXK_ID" => $ids,
                    "BTXKS_SectName" => $v["BTXKS_SectName"],
                    "BTXKS_Memo" => $v["BTXKS_Memo"]
                ];
            }
        }
        if(empty($copySectList)) return json(["code"=>0,"msg"=>"复制失败"]);
        foreach($copySectList as $k=>$v){
            if(isset($isSectList[$k])){
                $copySectList[$k]["BTXKS_ID"] = $isSectList[$k]["BTXKS_ID"];
                $delSectId[] = $isSectList[$k]["BTXKS_ID"];
            }
        }
        $detailList = $this->detailModel->where("BTXKS_ID","IN",array_keys($copySectIdList))->order("BTXKS_ID,BTXKD_ID ASC")->select();
        if(!$detailList) return json(["code"=>0,"msg"=>"复制失败"]);
        $result = false;
        Db::startTrans();
        try {
            if(!empty($delSectId)) $this->detailModel->where("BTXKS_ID","IN",$delSectId)->delete();
            $result = $this->sectModel->allowField(true)->saveAll($copySectList);
            $sectIdName = $addDetailList = [];
            foreach($result as $v){
                $sectIdName[$v["BTXKS_SectName"]] = $v["BTXKS_ID"];
            }
            foreach($detailList as $k=>$v){
                $addDetailList[$k] = $v->toArray();
                unset($addDetailList[$k]["BTXKD_ID"]);
                $addDetailList[$k]["BTXKS_ID"] = $sectIdName[$copySectIdList[$v["BTXKS_ID"]]];
            }
            $result = $this->detailModel->allowField(true)->saveAll($addDetailList);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>'复制成功']);
        } else {
            return json(["code"=>0,"msg"=>'复制失败']);
        }
    }

    //1.普通的单帽双帽螺栓以及脚钉会根据选择生成平垫弹垫和扣紧螺母，
    //2.双帽螺栓不会出现防盗的情况，（否）
    //3.滚珠式防盗螺栓只根据选择生成平垫，弹垫不生成
    //4.其他的防盗式的螺栓以及所有防盗式的脚钉会根据选择生成平垫弹垫和扣紧螺母
    public function createSave()
    {
        $scd_id = $this->request->post("SCD_ID");
        $BTXK_ID = $this->request->post("BTXK_ID");
        if(!$BTXK_ID or !$scd_id) return json(["code"=>0,"msg"=>"生成螺栓数据失败"]);
        $main_one = $this->fastMainModel->where("SCD_ID",$scd_id)->find();
        if(!$main_one) return json(["code"=>0,"msg"=>"请先保存"]);
        $fdtype = $main_one["ifFDType"];
        $kjlm = $main_one["ifKJLM"]==1?"扣紧螺母":"";
        $blm = $main_one["ifBLM"]==1?"薄螺母":"";
        $pd = ($main_one["ifDPPDTD"]==1 or $main_one["ifDPPDTD"]==3)?"平垫":"";
        $td = ($main_one["ifDPPDTD"]==2 or $main_one["ifDPPDTD"]==3)?"弹垫":"";
        //searchwhere是用于存需要生成的东西
        $searchWhere = [];
        foreach([$kjlm,$blm,$pd,$td] as $v){
            if($v!="") $searchWhere[] = $v;
        }
        $kpt_list = (new AtBoltFastInput())->where("BD_MaterialName","IN",$searchWhere)->select();
        $kpt_arr = [];
        foreach($kpt_list as $v){
            $kpt_arr[$v["BD_MaterialName"].'-'.$v["BD_Type"]] = $v->toArray();
        }
        $allPT = $main_one["AllPTSect"];
        $allFD = $main_one["AllFDSect"];
        $ban = $main_one["BanSect"];
        $sect = $allPT.','.$allFD.','.$ban;
        $sectList = $sectSave = $detailSave = $listkpd = [];
        foreach(explode(',',$sect) as $v){
            $detail_list = explode("*",trim($v));
            if(isset($detail_list[0]) and $detail_list[0]) $sectList[$detail_list[0]] = isset($detail_list[1])?$detail_list[1]:1;
        }
        ksort($sectList);
        $hbWhere = [
            "SCD_ID" => ["=",$scd_id]
        ];
        $heightboltlist = ["Writer"=>$this->admin["nickname"],"SCD_ID"=>$scd_id];
        $one = $this->hbModel->where($hbWhere)->find();
        $boltdetailArr = [];
        $boltdetail = $this->detailModel->alias("d")
            ->join([$this->technology_ex."bolttxksect"=>"s"],"d.BTXKS_ID = s.BTXKS_ID")
            ->where("s.BTXK_ID",$BTXK_ID)
            ->select();
        foreach($boltdetail as $v){
            $key = $v["BTXKS_SectName"].'-'.$v["BD_MaterialName"].'-'.$v["BD_Type"].'-'.$v["BD_Limber"];
            $boltdetailArr[$key] = $v->toArray();
        }
        $where = [
            "SCD_ID" => ["=",$scd_id],
            "SetSect" => ["IN",array_keys($sectList)]
        ];
        $detailList = $this->fastDetailModel->field("*,cast(SetSect as unsigned) as number")->where($where)->order("number asc")->select();
        foreach($detailList as $v){
            $v["BD_SumCount"] = $sectList[$v["SetSect"]]*$v["BD_SumCount"];
            $v["BD_PTCount"] = $sectList[$v["SetSect"]]*$v["BD_PTCount"];
            $v["BD_FDCount"] = $sectList[$v["SetSect"]]*$v["BD_FDCount"];
            $key = $v["SetSect"].'-'.$v["BD_MaterialName"].'-'.$v["BD_Type"].'-'.$v["BD_Limber"];
            $values = $boltdetailArr[$key]??["BD_SingelWeight"=>0,"BD_SJType"=>$v["BD_Type"],"BD_SJOther"=>"","BD_SJLenth"=>0,"BD_SJSumCount"=>0,"BD_SJSumWeight"=>0,"BD_SJSingelWeight"=>0,"BD_DisWeight"=>0,"BD_Memo"=>""];
            $copylist = [];
            if($v["BD_PTCount"] != 0){
                $copylist[] = ["BD_Flag"=>0,"BD_SumCount"=>$v["BD_PTCount"],"BD_SingelWeight"=>$values["BD_SingelWeight"],"BD_SumWeight"=>round($values["BD_SingelWeight"]*$v["BD_PTCount"],2),"BD_MaterialName"=>in_array($v["BD_MaterialName"],["扣紧螺母","薄螺母","弹片","垫片","平垫"])?$v["BD_MaterialName"]:"普通".$v["BD_MaterialName"],"if"=>""];
            }
            if($v["BD_FDCount"] != 0){
                if(in_array($v["BD_MaterialName"],["单帽螺栓","双帽螺栓"])) $name = "螺栓";
                else $name = $v["BD_MaterialName"];
                $copylist[] = ["BD_Flag"=>1,"BD_SumCount"=>$v["BD_FDCount"],"BD_SingelWeight"=>$values["BD_SingelWeight"],"BD_SumWeight"=>round($values["BD_SingelWeight"]*$v["BD_FDCount"],2),"BD_MaterialName"=>'防盗'.$name.$fdtype,"if"=>"防盗"];
            }
            preg_match('/\d+/', $v["BD_Type"], $matches);
            $BD_Type = "M".$matches[0];
            foreach($copylist as $copyv){
                $detailSaveName = $v["SetSect"].'-'.$copyv["BD_MaterialName"].'-'.$v["BD_Type"].'-'.$v["BD_Limber"].'-'.$v["BD_Lenth"].'-'.$v["BD_Lenth"].'-'.$copyv["BD_Flag"];
                if(isset($detailSave[$detailSaveName])){
                    $detailSave[$detailSaveName]["BD_SumCount"] += $copyv["BD_SumCount"];
                    $detailSave[$detailSaveName]["BD_SumWeight"] += $copyv["BD_SumWeight"];
                }else{
                    $detailSave[$detailSaveName] = [
                        "BD_SectName" => $v["SetSect"],
                        "BD_MaterialName" => $copyv["BD_MaterialName"],
                        "BD_Type" => $v["BD_Type"],
                        "BD_Limber" => $v["BD_Limber"],
                        "BD_Other" => $v["BD_Other"],
                        "BD_Lenth" => $v["BD_Lenth"],
                        "BD_SumCount" => $copyv["BD_SumCount"],
                        "BD_SingelWeight" => $copyv["BD_SingelWeight"],
                        "BD_SumWeight" => $copyv["BD_SumWeight"],
                        "BD_SJType" => $values["BD_SJType"],
                        "BD_SJOther" => $values["BD_SJOther"],
                        "BD_SJLenth" => $values["BD_SJLenth"],
                        "BD_SJSumCount" => $values["BD_SJSumCount"],
                        "BD_SJSumWeight" => $values["BD_SJSumWeight"],
                        "BD_SJSingelWeight" => $values["BD_SJSingelWeight"],
                        "BD_DisWeight" => $values["BD_DisWeight"],
                        "BD_Memo" => $values["BD_Memo"],
                        "BD_Flag" => $copyv["BD_Flag"],
                    ];
                }
                if(strstr($v["BD_MaterialName"],"螺栓")!==false or strstr($v["BD_MaterialName"],"脚钉")!==false){
                    foreach($searchWhere as $kpd){
                        if($copyv["BD_MaterialName"]=="防盗螺栓滚珠式" and ($kpd=="扣紧螺母" or $kpd=="薄螺母" or $kpd=="弹垫")) continue;
                        isset($listkpd[$v["SetSect"].'-'.$kpd.'-'.$BD_Type])?"":$listkpd[$v["SetSect"].'-'.$kpd.'-'.$BD_Type] = ["name"=>$kpd,"BD_Type"=>$BD_Type,"count"=>0,"BD_Flag"=>0,"BD_SectName"=>$v["SetSect"]];
                        $listkpd[$v["SetSect"].'-'.$kpd.'-'.$BD_Type]["count"] += $copyv["BD_SumCount"];
                    }
                }
                
            }
            
        }
        foreach($listkpd as $v){
            $key = $v["name"].'-'.$v["BD_Type"];
            $kpt_value = $kpt_arr[$key]??["BD_Limber"=>0,"BD_Other"=>'',"BD_Lenth"=>0,"BD_PerWeight"=>0];
            $detailSaveName = $v["BD_SectName"].'-'.$v["name"].'-'.$v["BD_Type"].'-'.$kpt_value["BD_Limber"].'-'.$kpt_value["BD_Lenth"].'-'.$kpt_value["BD_Lenth"].'-0';
            if(isset($detailSave[$detailSaveName])){
                $detailSave[$detailSaveName]["BD_SumCount"] += $v["count"];
                $detailSave[$detailSaveName]["BD_SumWeight"] += round($kpt_value["BD_PerWeight"]*$v["count"],2);
            }else{
                $detailSave[$detailSaveName] = [
                    "BD_SectName" => $v["BD_SectName"],
                    "BD_MaterialName" => $v["name"],
                    "BD_Type" => $v["BD_Type"],
                    "BD_Limber" => $kpt_value["BD_Limber"],
                    "BD_Other" => $kpt_value["BD_Other"],
                    "BD_Lenth" => $kpt_value["BD_Lenth"],
                    "BD_SumCount" => $v["count"],
                    "BD_SingelWeight" => $kpt_value["BD_PerWeight"],
                    "BD_SumWeight" => round($kpt_value["BD_PerWeight"]*$v["count"],2),
                    "BD_SJType" => $v["BD_Type"],
                    "BD_SJOther" => $kpt_value["BD_Other"],
                    "BD_SJLenth" => $kpt_value["BD_Lenth"],
                    "BD_SJSumCount" => 0,
                    "BD_SJSumWeight" => 0,
                    "BD_SJSingelWeight" => 0,
                    "BD_DisWeight" => 0,
                    "BD_Memo" => '',
                    "BD_Flag" => 0,
                ];
            }
        }
        $result = false;
        Db::startTrans();
        try {

            if($one){
                $HB_ID = $one["HB_ID"];
                $heightboltlist["HB_ID"] = $one["HB_ID"];
                $this->hbModel->update($heightboltlist);
            }else{
                $HB_ID = $this->hbModel->insertGetId($heightboltlist);
            }
            
            $this->bsectModel->where("HB_ID",$HB_ID)->delete();
            $this->bdetailModel->where("HB_ID",$HB_ID)->delete();

            foreach($sectList as $k=>$v){
                $sectSave[$k] = ["BS_SectName"=>$k,"HB_ID"=>$HB_ID];
            }
            
            $result = $this->bsectModel->allowField(true)->saveAll(array_values($sectSave));
            foreach($result as $v){
                $sectSave[$v["BS_SectName"]]["BS_ID"] = $v["BS_ID"];
            }

            foreach($detailSave as $k=>$v){
                $detailSave[$k]["HB_ID"] = $HB_ID;
                $detailSave[$k]["BS_ID"] = $sectSave[$v["BD_SectName"]]["BS_ID"];
            }
            
            $result = $this->bdetailModel->allowField(true)->saveAll(array_values($detailSave));
            
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>'保存成功']);
        } else {
            return json(["code"=>0,"msg"=>'保存失败']);
        }

    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["BTXKS_ID","BTXKS_ID","text","","readonly",0,"hidden"],
            ["*段名","BTXKS_SectName","text","","data-rule='required'","",""],
            ["单段数量","count","text","","readonly",0,""],
            ["单段重量","weight","text","","readonly",0,""],
            ["备注","BTXKS_Memo","text","","","",""],
            
        ];
        return $list;
    }

    public function getDetailField(){
        $tableField = [
            ["BTXKD_ID","BTXKD_ID","text","","hidden","","40"],
            // ["段名","BTXKS_SectName","text","required",""],
            //暂时不知道类型是干嘛用的 先省略 稍后问
            ["类型","BD_Flag","text","readonly","hidden","","40"],
            ["类型","BD_sFlag","text","readonly","","","60"],
            ["材料名称","BD_MaterialName","text","","","BD_MaterialName","80"],
            ["等级","BD_Limber","text","","","BD_Limber","35"],
            ["实际规格","BD_Type","text","","","BD_Type","75"],
            ["实际总数","BD_SumCount","text","data-rule='required'","","","40"],
            ["实际无扣长","BD_Lenth","text","","","BD_Lenth","35"],
            ["另附规格","BD_Other","text","","","BD_Other","30"],
            ["单重","BD_SingelWeight","text","","","BD_PerWeight","40"],
            ["实际总重","BD_SumWeight","text","","","","50"],
            ["备注","BD_Memo","text","","","","50"],
            ["","","text","","","","10"],
            ["图纸规格","BD_SJType","text","","","BD_Type","75"],
            ["图纸无扣长","BD_SJLenth","text","","","BD_Lenth","35"],
            ["图纸另附规格","BD_SJOther","text","","","BD_Other","30"],
            ["图纸总数","BD_SJSumCount","text","","","","20"],
            ["图纸单重","BD_SJSingelWeight","text","","","BD_PerWeight","50"],
            ["图纸总重","BD_SJSumWeight","text","","","","20"]
        ];
        return $tableField;
    }
}
