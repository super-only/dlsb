<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\BoltFastCreateMain;
use app\admin\model\chain\lofting\BoltScdSummary;
use app\admin\model\chain\lofting\BoltScdSummaryMain;
use app\admin\model\chain\sale\Compact;
use app\admin\model\chain\sale\Sectconfigdetail;
use app\admin\model\jichu\ch\AtBoltFastInput;
use app\admin\model\jichu\jg\Factoryname;
use app\admin\controller\Technology;
use app\admin\model\chain\fastening\ApplydetailsJgj;
use app\admin\model\chain\fastening\ApplylistJgj;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use think\Cache;

/**
 *
 *
 * @icon fa fa-circle-o
 */
class HeightBolt extends Technology
{

    /**
     * HeightBolt模型对象
     * @var \app\admin\model\chain\lofting\HeightBolt
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_type = \think\Session::get('technology_type');
        $this->technology_field = \think\Session::get('technology_field');
        $this->model = new \app\admin\model\chain\lofting\HeightBolt([],$this->technology_ex);
        $this->sectModel = new \app\admin\model\chain\lofting\BoltSect([],$this->technology_ex);
        $this->detailModel = new \app\admin\model\chain\lofting\BoltDetail([],$this->technology_ex);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


     /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("hb")
                ->join(["sectconfigdetail"=>'scd'],"hb.SCD_ID=scd.SCD_ID","LEFT")
                ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
                ->join(["taskdetail"=>"td"],"td.TD_ID = th.TD_ID")
                ->join(["task"=>"t"],"t.T_Num=td.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->field("hb.HB_ID,t.T_Company,t.C_Num,t.T_Num,t.t_project,t.t_shortproject,t.T_Sort,scd.SCD_TPNum,scd.TD_TypeName,scd.TD_Pressure,scd.TH_Height,scd.SCD_Count,c.PC_Num,hb.Writer,hb.HB_WDate")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 新增
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = array_merge($params,["Writer"=>$this->admin["nickname"],"HB_WDate"=>date("Y-m-d H:i:s")]);
                $result = false;
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('success',null,$result["HB_ID"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $tableField = $this->getTableField();
        $this->view->assign("tableField",$tableField);
        return $this->view->fetch();
    }

    public function chooseTower()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new Sectconfigdetail())->alias("scd")
                ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
                ->join(['taskdetail'=>"td"],"th.TD_ID = td.TD_ID")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                // 任务单号td.T_Num 产品名称td.P_Name 塔型td.TD_TypeName 工程编号c.PC_Num 合同号c.C_Num 工程名称t.t_project 工程简称t.t_shortproject 客户c.Customer_Name 总基数count 电压等级td.TD_Pressure 总重(kg)td.T_SumWeight
                ->field("td.T_Num,td.P_Name,td.TD_TypeName,c.PC_Num,c.C_Num,t.t_project,t.t_shortproject,c.Customer_Name,sum(scd.SCD_Count) as count,td.TD_Pressure,t.T_SumWeight,td.TD_ID,c.C_Company")
                ->where("scd.SCD_ID","NOT IN","select 'x' from ".$this->technology_ex."heightbolt where heightbolt")
                ->where($where)
                ->where("c.produce_type",$this->technology_type)
                ->group("td.TD_ID")
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        return $this->view->fetch();
    }

    public function choosePole($td_id=null)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new Sectconfigdetail())->alias("scd")
                ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
                ->join(['taskdetail'=>"td"],"th.TD_ID = td.TD_ID")
                // 塔型td.TD_TypeName 杆塔号scd.SCD_TPNum 配段信息pd_news 配段基数scd.SCD_Count 任务单号td.T_Num 电压等级td.TD_Pressure
                ->field("td.TD_TypeName,scd.SCD_TPNum,scd.SCD_ID,td.TD_Pressure,scd.SCD_Count,td.T_Num,CASE WHEN scd.SCD_Part='' THEN scd.SCD_SpPart WHEN scd.SCD_SpPart='' THEN scd.SCD_Part ELSE concat(scd.SCD_Part,',',scd.SCD_SpPart) END AS pd_news,th.TH_Height")
                ->where("scd.SCD_ID","NOT IN","select 'x' from ".$this->technology_ex."heightbolt where heightbolt")
                ->where("td.td_id",$td_id)
                ->where($where)
                ->where("scd.SCD_ProductName",$this->technology_field)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        $this->assignconfig("td_id",$td_id);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->alias("hb")
            ->join(["sectconfigdetail"=>'scd'],"hb.SCD_ID=scd.SCD_ID","LEFT")
            ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
            ->join(["taskdetail"=>"td"],"td.TD_ID = th.TD_ID")
            ->join(["task"=>"t"],"t.T_Num=td.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->field("hb.HB_ID,t.T_Company,t.C_Num,t.T_Num,t.t_project,scd.SCD_TPNum,scd.TD_TypeName,scd.TD_Pressure,scd.TH_Height,c.Customer_Name,hb.Writer,hb.HB_WDate,hb.HB_Memo,hb.SCD_ID")
            ->where("HB_ID",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $sectArr = [];
        $number = $weight = 0;
        $sectList = $this->sectModel->alias("s")
            ->join([$this->technology_ex."boltdetail"=>'d'],"s.BS_ID = d.BS_ID","left")
            ->field("s.*,sum(d.BD_SumCount) as count,sum(d.BD_SumWeight) as weight,CAST(s.BS_SectName as UNSIGNED) AS number")
            ->where("s.HB_ID",$ids)
            ->group("s.BS_ID")
            ->order("number,s.BS_SectName asc")
            ->select();
        //修改
        foreach($sectList as $v){
            $v = $v->toArray();
            $v["count"] = $v["count"]?$v["count"]:0;
            $v["weight"] = $v["weight"]?$v["weight"]:0;
            $v["number"] = $v["number"]?$v["number"]:0;
            $sectArr[$v["BS_ID"]] = $v;
            $number += $v["count"];
            $weight += $v["weight"];

        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($paramsTable) {
                $params = $this->preExcludeFields($params);
                $sectSaveList = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk][$k] = $vv;
                    }
                }
                $bjhList = [];
                foreach($sectSaveList as $k=>$v){
                    if(isset($bjhList[$v["BS_SectName"]])) return json(["code"=>0,'msg'=>"段号有重复，请进行修改！"]);
                    else $bjhList[$v["BS_SectName"]] = $v["BS_SectName"];
                    if($v["BS_ID"]==0) unset($sectSaveList[$k]["BS_ID"],$v["BS_ID"]);
                    else if(isset($sectArr[$v["BS_ID"]])) unset($sectArr[$v["BS_ID"]]);
                    foreach($v as $kk=>$vv){
                        if($vv=='') unset($sectSaveList[$k][$kk]);
                    }
                    $sectSaveList[$k]["HB_ID"] = $ids;
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->allowField(true)->where("HB_ID",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->sectModel->allowField(true)->saveAll($sectSaveList);
                    if(!empty($sectList)){
                        $this->sectModel->where(["BS_ID"=>["IN",array_keys($sectArr)]])->delete();
                        $this->detailModel->where(["BS_ID"=>["IN",array_keys($sectArr)]])->delete();
                    }
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        
        $count = count($sectArr);
        $tableField = $this->getTableField();
        $this->view->assign("allCount",["count"=>$count,"number"=>$number,"weight"=>$weight]);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("row", $row);
        $this->view->assign("sectList", array_values($sectArr));
        $this->view->assign("ids", $ids);
        return $this->view->fetch();
    }

    public function copyLs($ids=null)
    {
        $this->view->assign("ids",$ids);
        return $this->view->fetch();
    }

    public function copyList(){
        $hb_id = $this->request->post("HB_ID");
        if(!$hb_id) return json(["code"=>0,"msg"=>"有误，请稍后重试"]);
        $list = $this->sectModel->field("BS_ID,BS_SectName,cast(BS_SectName as unsigned) as number")->where("HB_ID",$hb_id)->order("number,BS_SectName ASC")->select();
        return json(["code"=>1,"data"=>$list]);
    }

    public function copyDetail()
    {
        $params = $this->request->post();
        list($ids,$sectId) = array_values($params);
        if(!$ids) return json(["code"=>0,"msg"=>"有误，请刷新后重试！"]);

        $selfArr = $this->sectModel->where("HB_ID",$ids)->select();
        $selfList = $sectName = $copyDetailList = [];
        foreach($selfArr as $v){
            $selfList[$v["BS_SectName"]] = $v->toArray();
        }

        $copySectArr = $this->sectModel->where("BS_ID","IN",$sectId)->select();
        $copyDetailArr = $this->detailModel->where("BS_ID","IN",$sectId)->select();

        foreach($copySectArr as $v){
            $sectName[$v["BS_ID"]] = $v["BS_SectName"];
            isset($selfList[$v["BS_SectName"]])?"":$selfList[$v["BS_SectName"]] = ["BS_SectName"=>$v["BS_SectName"],"HB_ID"=>$ids];
        }
        $result = false;
        Db::startTrans();
        try {
            $result = $this->sectModel->allowField(true)->saveAll($selfList);
            $sectBS = [];
            foreach($result as $v){
                $sectBS[$v["BS_SectName"]] = $v["BS_ID"];
            }
            foreach($copyDetailArr as $k=>$v){
                $copyDetailList[$k] = $v->toArray();
                unset($copyDetailList[$k]["BD_ID"]);
                $copyDetailList[$k]["HB_ID"] = $ids;
                $copyDetailList[$k]["BS_ID"] = @$sectBS[$sectName[$v["BS_ID"]]];
            }

            $this->detailModel->allowField(true)->saveAll($copyDetailList);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"复制成功"]);
        } else {
            return json(["code"=>0,"msg"=>__('No rows were updated')]);
        }
    }

    public function chooseScd($ids=null)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("hb")
                ->join(["sectconfigdetail"=>'scd'],"hb.SCD_ID=scd.SCD_ID","LEFT")
                ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
                ->join(["taskdetail"=>"td"],"td.TD_ID = th.TD_ID")
                ->join(["task"=>"t"],"t.T_Num=td.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->field("hb.HB_ID,t.T_Company,t.C_Num,t.T_Num,t.t_project,t.t_shortproject,t.T_Sort,scd.SCD_TPNum,scd.TD_TypeName,scd.TD_Pressure,scd.TH_Height,scd.SCD_Count,c.PC_Num,hb.Writer,hb.HB_WDate")
                ->where("hb.HB_ID","<>",$ids)
                ->where($where)
                ->where("c.produce_type",$this->technology_type)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->view->assign("ids",$ids);
        return $this->view->fetch();
    }

    //编辑table中某一个detail的删除
    public function delDetail()
    {
        $num = $this->request->post("num");
        if($num){
            Db::startTrans();
            try {
                // $detailList = $this->detailModel->field("DtMD_ID_PK")->where("DtMD_iSectID_FK",$num)->select();
                // $detailId = [];
                // foreach($detailList as $v){
                //     $detailId[] = $v["DtMD_ID_PK"];
                // }
                $this->sectModel->where("BS_ID",$num)->delete();
                $this->detailModel->where("BS_ID",$num)->delete();
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>1,'msg'=>"成功！"]);
    }

    //编辑table中某一个detail的编辑页面
    public function detail($ids=null)
    {
        $row = $this->sectModel->where("BS_ID",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $detailArr = Db::query("select S.BS_SectName,B.*,case when BD_Flag=0 then '普通' else '防盗' end as BD_sFlag,case when BD_MaterialName='螺栓' then 0  when BD_MaterialName='脚钉' then 1  when BD_MaterialName='垫圈'  then 2 when instr('扣紧',BD_MaterialName)>0 then 3 when BD_MaterialName='防盗帽圈' then 4 else 5 end as materialSort, BD_Type  from ".$this->technology_ex."boltdetail B inner join ".$this->technology_ex."boltsect S on B.BS_ID=S.BS_ID  where  (1=1)   and   B.BS_ID=:ids  order by BD_Flag,MaterialSort,BD_MaterialName,BD_Limber,BD_Type,BD_Other",["ids"=>$ids]);
        if ($this->request->isPost()) {
            $params = $this->request->post("data");
            $params = json_decode(str_replace('&quot;','"',$params), true);
            if ($params) {
                $detailSaveList = $keyValue = [];
                $key = "";
                foreach($params as $v){
                    $key = rtrim($v["name"],"[]");
                    isset($keyValue[$key])?"":$keyValue[$key] = [];
                    $keyValue[$key][] = $v["value"];
                }
                foreach($keyValue as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $detailSaveList[$kk][$k] = $vv;
                    }
                }
                $contentList = [];
                $pt_fd_arr = [0=>"普通",1=>"防盗"];
                // $iniList = [];
                foreach($detailSaveList as $k=>$v){
                    // if($v["BD_ID"]) $iniList[$v["BD_ID"]] = $v["BD_ID"];
                    unset($detailSaveList[$k]["BD_ID"],$v["BD_ID"]);
                    foreach(["BD_SumCount","BD_SingelWeight"] as $vv){
                        $v[$vv] = $v[$vv]?$v[$vv]:0;
                    }

                    if($pt_fd_arr[$v["BD_Flag"]] != $v["BD_sFlag"]){
                        $v["BD_Flag"] = trim($v["BD_sFlag"])=="普通"?0:1;
                    }

                    $key = $v["BD_MaterialName"].'-'.$v["BD_Type"].'-'.$v["BD_Limber"].'-'.$v["BD_Lenth"].'-'.$v["BD_Flag"];
                    if(!isset($contentList[$key])){
                        $contentList[$key] = $v;
                    }else{
                        foreach(["BD_SumCount","BD_SingelWeight"] as $vvv){
                            $contentList[$key][$vvv] += $v[$vvv];
                        }
                    }
                    // $contentList[$key]["BD_SJSumWeight"] = round($contentList[$key]["BD_SJSumCount"]*$contentList[$key]["BD_SJSingelWeight"],2);
                    $contentList[$key]["BD_SumWeight"] = round($contentList[$key]["BD_SumCount"]*$contentList[$key]["BD_SingelWeight"],2);
                    $contentList[$key]["BS_ID"] = $ids;
                    $contentList[$key]["BD_SectName"] = $row["BS_SectName"];
                    $contentList[$key]["HB_ID"] = $row["HB_ID"];
                }
                Db::startTrans();
                try {
                    $this->detailModel->where("BS_ID",$ids)->delete();
                    $result = $this->detailModel->allowField(true)->saveAll($contentList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (PDOException $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                } catch (Exception $e) {
                    Db::rollback();
                    return json(["code"=>0,'msg'=>$e->getMessage()]);
                }
                if ($result) {
                    return json(["code"=>1,'msg'=>"保存成功"]);
                } else {
                    return json(["code"=>0,'msg'=>"保存失败"]);
                }
            }
            return json(["code"=>0,'msg'=>__('Parameter %s can not be empty')]);
        }
        // $BTXKS_SectName = $row["BTXKS_SectName"];
        $HB_ID = $row["HB_ID"];
        $sectArr = [];
        $sectList = $this->sectModel->field("BS_SectName,BS_ID,CAST(BS_SectName AS UNSIGNED) AS number")->where("HB_ID",$HB_ID)->order("number,BS_SectName ASC")->select();
        foreach($sectList as $v){
            $sectArr[$v["BS_ID"]] = $v["BS_SectName"];
        }


        $tableField = $this->getDetailField();
        foreach($tableField as $v){
            $v[0]!=""?$field[] = $v[1]:"";
        }
        $this->view->assign("sectArr", $sectArr);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("row", $row);
        $this->view->assign("ids", $ids);
        // $this->view->assign("BTXKS_SectName", $BTXKS_SectName);

        $this->view->assign("detailList",$detailArr);

        return $this->view->fetch();
    }

    public function detailTable()
    {
        $params = $this->request->post();
        //一级id 二级id 二级change——id
        list($ids,$detail_id,$searchNum) = array_values($params);
        if(!$ids or !$detail_id or !$searchNum) return json(["code"=>0,"msg"=>"error"]);
        $BTXKS_one = $this->sectModel->find($searchNum);
        $BS_SectName = $BTXKS_one["BS_SectName"];
        $tableField = $this->getDetailField();
        foreach($tableField as $v){
            $v[0]!=""?$field[] = $v[1]:"";
        }
        unset($field[2]);
        $detailList = $this->detailModel->field(implode(",",$field))->where("BS_ID",$searchNum)->order("BD_MaterialName,BD_Type ASC")->select();
        $tableContent = '';
        foreach($detailList as $k=>$v){
            $v["BD_sFlag"] = $v["BD_Flag"]==1?"防盗":"普通";
            $tableContent .= '<tr><td>'.($k+1).'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td><td>'. $BS_SectName.'</td>';
            foreach($tableField as $vv){
                if($vv[1]=="") $tableContent .= '<td><input type="text" class="small_input" value=""></td>';
                else $tableContent .= '<td '.$vv[4].'><input type="'.$vv[2].'" class="small_input" data-rule="'.$vv[3].'" name="'.$vv[1].'[]" value="'.($v[$vv[1]]??$v[$vv[1]]).'"></td>';
            }
            $tableContent .="</tr>";
        }
        return json(["code"=>1,"data"=>$tableContent]);
    }

    public function rightTable()
    {
        $params = $this->request->post();
        $where = [];
        foreach($params as $k=>$v){
            if($v!="") $where[$k] = ["like","%".$v."%"];
        }
        $list = (new AtBoltFastInput())->where($where)->order("BD_MaterialName,BD_Limber,BD_Type ASC ASC")->select();
        return json(["code"=>1,"data"=>$list]);
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            Db::startTrans();
            try {
                $this->detailModel->where("BD_ID",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function summary($ids=null)
    {
        if(!$ids){
            $this->error(__('No Results were found'));
        }
        $row = $this->model->alias("hb")
            ->join(["sectconfigdetail"=>'scd'],"hb.SCD_ID=scd.SCD_ID","LEFT")
            ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
            ->join(["taskdetail"=>"td"],"td.TD_ID = th.TD_ID")
            ->join(["task"=>"t"],"t.T_Num=td.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->field("hb.HB_ID,t.t_project,scd.SCD_TPNum,scd.TD_TypeName,scd.TD_Pressure,scd.TH_Height,hb.SCD_ID,c.PC_Num")
            ->where("HB_ID",$ids)
            ->find();
        // pri($row,1);
        if(!$row){
            $this->error(__('No Results were found'));
        }
        $scdsummaryModel = new BoltScdSummaryMain([],$this->technology_ex);
        $BoltScdSummaryModel = new BoltScdSummary([],$this->technology_ex);
        $shList = $scdsummaryModel->where("SCD_ID",$row["SCD_ID"])->find();
        if ($this->request->isPost()) {
            $ls_loss = $this->request->post("LS_Loss");
            $ls_loss = $ls_loss?$ls_loss:0;
            $params = $this->request->post("content");
            $params = $params?json_decode(str_replace('&quot;','"',$params), true):[];
            if (!empty($params)) {
                $summaryList = [];
                foreach($params as $v){
                    $BD_SumCount = $v["BD_Flag"]==1?($v["1-all"]??0):($v["0-all"]??0);
                    $BD_LossCoount = $v["BD_Flag"]==1?($v["1-loss"]??0):($v["0-loss"]??0);
                    $weight = ($BD_SumCount+$BD_LossCoount)*$v["weight"];
                    $summaryList[] = [
                        "SCD_ID" => $row["SCD_ID"],
                        "BD_MaterialName" => $v["BD_MaterialName"],
                        "BD_Type" => $v["BD_Type"],
                        "BD_Limber" => $v["BD_Limber"],
                        "BD_Lenth" => $v["BD_Lenth"],
                        "BD_SumCount" => $BD_SumCount,
                        "BD_LossCoount" => $BD_LossCoount,
                        "BD_Flag" => $v["BD_Flag"],
                        "BD_SumWeight" => $weight
                    ];
                }
                $result = false;
                Db::startTrans();
                try {
                    $params = ["LS_Loss" => $ls_loss];
                    $params["SCD_ID"] = $row["SCD_ID"];
                    if(!$shList) $result = $scdsummaryModel->insert($params);
                    else $result = $scdsummaryModel->update($params);
                    $BoltScdSummaryModel->where("SCD_ID",$row["SCD_ID"])->delete();
                    $BoltScdSummaryModel->allowField(true)->saveAll(array_values($summaryList));
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = $row->toArray();
        $sectList = (new BoltFastCreateMain([],$this->technology_ex))->where("SCD_ID",$row["SCD_ID"])->find();
        $sectArr = [];
        if($sectList){
            $sect = $sectList["AllPTSect"].",".$sectList["AllFDSect"].",".$sectList["BanSect"];
            foreach(explode(",",$sect) as $v){
                if($v!="") $sectArr[$v] = $v;
            }
            ksort($sectArr);
        }
        $row["sect"] = empty($sectArr)?"":implode(",",$sectArr);
        $row["LS_Loss"] = 5;
        if($shList) $row["LS_Loss"] = $shList["LS_Loss"];
        $this->view->assign("row", $row);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function summaryDetail()
    {
        $HB_ID = $this->request->post("HB_ID");
        if(!$HB_ID) return json(["code"=>0,"msg"=>"请刷新后重试"]);
        $one = $this->model->where("HB_ID",$HB_ID)->find();
        if(!$one) return json(["code"=>0,"msg"=>"请刷新后重试"]);
        $SCD_ID = $one["SCD_ID"];
        $LS_Loss = $this->request->post("LS_Loss")??0;
        list($summaryList,$content,$field) = $this->dateDeal($HB_ID,$SCD_ID,$LS_Loss);
        
        // pri($summaryList,1);
        if(empty($content)) $field = [];
        // else{
        //     Db::startTrans();
        //     try {
        //         $BoltScdSummaryModel = new BoltScdSummary();
        //         $BoltScdSummaryModel->where("SCD_ID",$SCD_ID)->delete();
        //         $BoltScdSummaryModel->allowField(true)->saveAll(array_values($summaryList));
        //         Db::commit();
        //     } catch (ValidateException $e) {
        //         Db::rollback();
        //         return json(["code"=>0,"msg"=>$e->getMessage()]);
        //     } catch (PDOException $e) {
        //         Db::rollback();
        //         return json(["code"=>0,"msg"=>$e->getMessage()]);
        //     } catch (Exception $e) {
        //         Db::rollback();
        //         return json(["code"=>0,"msg"=>$e->getMessage()]);
        //     }
        // }
        return json(["code"=>1,"data"=>["field"=>$field,"content"=>array_values($content)]]);

    }

    private function dateDeal($HB_ID,$SCD_ID,$LS_Loss,$type=1)
    {
        $list = $this->detailModel->field("BD_SectName,BD_MaterialName,BD_Type,BD_Limber,BD_SectName,BD_Lenth,BD_SumCount,BD_Flag,BD_SingelWeight")->where("HB_ID",$HB_ID)->order("BD_Flag DESC,BD_MaterialName ASC,BD_Type ASC,BD_Limber ASC")->select();
        $sectPT = $sectFD = [];
        $content = [];
        foreach($list as $v){
            $v["BD_Flag"]==0?($sectPT[$v["BD_SectName"]] = $v["BD_SectName"]):($sectFD[$v["BD_SectName"]] = $v["BD_SectName"]);
            $key = $v["BD_MaterialName"].'-'.$v["BD_Type"].'-'.$v["BD_Limber"].'-'.$v["BD_Lenth"].'-'.$v["BD_Flag"];
            isset($content[$key])?"":$content[$key] = [
                "BD_MaterialName" => $v["BD_MaterialName"],
                "BD_Limber" => $v["BD_Limber"],
                "BD_Type" => $v["BD_Type"],
                "BD_Lenth" => $v["BD_Lenth"],
                "BD_Flag" => $v["BD_Flag"],
                "weight" => $v["BD_SingelWeight"]
            ];
            $flagKey = $v["BD_Flag"].'-'.$v["BD_SectName"];
            isset($content[$key][$flagKey])?$content[$key][$flagKey] += $v["BD_SumCount"]:$content[$key][$flagKey] = $v["BD_SumCount"];
        }
        ksort($sectPT);
        ksort($sectFD);

        $field = [
            ["field"=>"id","title"=>"序号"],
            ["checkbox"=>"true","field"=>"choose"],
            ["field"=>'BD_MaterialName', "title"=>"名称"],
            ["field"=>'BD_Limber', "title"=>"等级"],
            ["field"=>'BD_Type', "title"=>"规格"],
            ["field"=>'weight', "title"=>"单重","visible"=>false],
            ["field"=>'BD_Lenth', "title"=>"无扣长"],
        ];
        if(!empty($sectPT)){
            foreach($sectPT as $v){
                $field[] = ["field"=>"0-".$v,"title"=>$v];
            }
            $field[] = ["field"=>"0-all","title"=>"普通(套)用量"];
            $field[] = ["field"=>"0-loss","title"=>"损耗"];
        }
        if(!empty($sectFD)){
            foreach($sectFD as $v){
                $field[] = ["field"=>"1-".$v,"title"=>$v];
            }
            $field[] = ["field"=>"1-all","title"=>"防盗(套)用量"];
            $field[] = ["field"=>"1-loss","title"=>"损耗"];
        }
        $summaryList = [];
        $count = 0;
        foreach($content as $k=>$v){
            $countPT = $countFD = 0;
            $count++;
            if(!empty($sectPT)){
                foreach($sectPT as $vv){
                    $content[$k]["0-".$vv] = $content[$k]["0-".$vv]??0;
                    $countPT += $content[$k]["0-".$vv];
                }
                $content[$k]["0-all"] = $countPT;
                $content[$k]["0-loss"] = ((($countPT*$LS_Loss/100)<1) && (($countPT*$LS_Loss/100)!=0))?1:round($countPT*$LS_Loss/100);
            }
            if(!empty($sectFD)){
                foreach($sectFD as $vv){
                    $content[$k]["1-".$vv] = $content[$k]["1-".$vv]??0;
                    $countFD += $content[$k]["1-".$vv];
                }
                $content[$k]["1-all"] = $countFD;
                $content[$k]["1-loss"] = ((($countFD*$LS_Loss/100)<1) && (($countFD*$LS_Loss/100)!=0))?1:round($countFD*$LS_Loss/100);
            }
            $content[$k]["id"] = $count;
            $summaryList[$k] = $v;
            $summaryList[$k]['SCD_ID'] = $SCD_ID;
            $summaryList[$k]['BD_SumCount'] = ($content[$k]["0-all"]??0)+($content[$k]["1-all"]??0);
            $summaryList[$k]['BD_LossCoount'] = ($content[$k]["0-loss"]??0)+($content[$k]["1-loss"]??0);
        }
        return [$summaryList,$content,$field];
    }

    public function setcache(){
        $value = $this->request->post("value");
        $key = "heightbolt";
        Cache::set($key,$value,30);
        $this->success('设置成功',null,$key);
    }

    public function export($HB_ID=0,$LS_Loss=0,$key='')
    {
        if(!$HB_ID) return '请确保无误后重试';
        $one = $this->model->alias("hb")
            ->join(["sectconfigdetail"=>'scd'],"hb.SCD_ID=scd.SCD_ID","LEFT")
            ->join(["taskheight"=>"th"],"scd.TH_ID = th.TH_ID")
            ->join(["taskdetail"=>"td"],"td.TD_ID = th.TD_ID")
            ->join(["task"=>"t"],"t.T_Num=td.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->field("hb.HB_ID,t.t_project,scd.SCD_TPNum,scd.TD_TypeName,scd.TD_Pressure,scd.TH_Height,hb.SCD_ID,c.PC_Num")
            ->where("HB_ID",$HB_ID)
            ->find();
        if(!$one) return '请确保无误后重试';
        $SCD_ID = $one["SCD_ID"];
        list($summaryList,$content,$field) = $this->dateDeal($HB_ID,$SCD_ID,$LS_Loss);
        $content = Cache::get($key);
        $content = $content?json_decode(str_replace('&quot;','"',$content), true):[];
        if(empty($content)) return '请确保无误后重试';
        else{
            $length = count(current($content));
            $spreadsheet = new Spreadsheet();
	        $sheet = $spreadsheet->getActiveSheet();
            $styleArray = [
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER, //水平居中
                    'vertical' => Alignment::VERTICAL_CENTER, //垂直居中
                ],
            ];
            $sheet->mergeCells("A1:".(Coordinate::stringFromColumnIndex($length-1))."2");
            $sheet->setCellValue('A1','绍兴电力设备有限公司单基螺栓表');
            $sheet->mergeCells("A3:".(Coordinate::stringFromColumnIndex($length-1))."3");
            $sheet->setCellValue('A3','工程编号：'.$one["PC_Num"]." 工程名称：".$one["t_project"]." 塔型：".$one["TD_TypeName"]);
            $sheet->mergeCells("A4:".(Coordinate::stringFromColumnIndex($length-1))."4");
            $sheet->setCellValue('A4','呼高：'.$one["TH_Height"]." 杆塔号：".$one["SCD_TPNum"]);
            $sheet->getstyle("A1:".(Coordinate::stringFromColumnIndex($length-1))."4")->applyFromArray($styleArray);
            $count = 0;
            foreach($field as $v){
                if(!isset($v["title"]) or $v["field"]=="weight") continue;
                $count++;
                $sheet->setCellValue((Coordinate::stringFromColumnIndex($count)).'5',$v["title"]);
                $lie_count = 5;
                foreach($content as $vv){
                    $lie_count++;
                    $sheet->setCellValue((Coordinate::stringFromColumnIndex($count)).$lie_count,$vv[$v["field"]]?$vv[$v["field"]]:"");
                }
            }

            $fileName = '单基螺栓汇总'. date('Ymd');

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx'); //按照指定格式生成Excel文件
            $this->excelBrowserExport($fileName, 'Xlsx');
            $writer->save('php://output');
        }
    }

    public function print($ids = null)
    {
        if(!$ids){
            $this->error(__('No Results were found'));
        }
        $list = (new Compact())->alias('c')
            ->join(["task"=>"t"],"c.C_Num = t.C_Num")
            ->field("t.C_Num,t.T_Company,t.t_project")
            ->where("t.C_Num",$ids)
            ->select();
        $row = (new Factoryname())->get(6);
        $this->view->assign("row", $row);
        $this->view->assign("printData", json_encode($list));
        return $this->view->fetch();

    }

    public function printRight()
    {
        $C_Num = $this->request->post("C_Num");
        if(!$C_Num) return json(["code"=>0,"msg"=>"有误请刷新后重试"]);
        $list = $this->model->alias("hb")
            ->join(["sectconfigdetail"=>"scd"],"hb.SCD_ID = scd.SCD_ID")
            ->join(["salegt"=>"s"],"s.SGT_ID = scd.SGT_ID")
            ->join(["taskheight"=>"th"],"scd.TH_ID=th.TH_ID")
            ->join(["taskdetail"=>"td"],"td.TD_ID=th.TD_ID")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num")
            ->field("td.T_Num,scd.TD_TypeName,scd.SCD_ID,scd.TH_Height,scd.SCD_Count,scd.SCD_TPNum,scd.SCD_Part,scd.SCD_SpPart,scd.TH_ID,scd.bolt")
            ->where("t.C_Num",$C_Num)
            ->select();
        $data = [];
        foreach($list as $k=>$v){
            $key = $v["TH_ID"]."-".$v["TH_Height"]."-".$v["SCD_Part"]."-".$v["SCD_SpPart"]."-".$v["bolt"];
            isset($data[$key])?"":$data[$key] = [
                "T_Num" => $v["T_Num"],
                "TD_TypeName" => $v["TD_TypeName"],
                "TH_Height" => $v["TH_Height"],
                "bolt_List" => [],
                "SCD_TPNum_List" => [],
                "SCD_ID_List" => [],
                "SCD_Count_List" => []
            ];
            $data[$key]["bolt_List"][] = $v["bolt"];
            $data[$key]["SCD_TPNum_List"][] = $v["SCD_TPNum"];
            $data[$key]["SCD_ID_List"][] = $v["SCD_ID"];
            $data[$key]["SCD_Count_List"][] = $v["SCD_Count"];
        }
        foreach($data as $k=>$v){
            $data[$k]["SCD_TPNum"] = implode(",",$v["SCD_TPNum_List"]);
            $data[$k]["SCD_ID"] = implode("_",$v["SCD_ID_List"]);
            $data[$k]["SCD_Count"] = array_sum($v["SCD_Count_List"]);
            $data[$k]["bolt"] = array_sum($v["bolt_List"]);
            unset($data[$k]["SCD_TPNum_List"],$data[$k]["SCD_ID_List"],$data[$k]["SCD_Count_List"],$data[$k]["bolt_List"]);
        }
        return json(["code"=>1,"data"=>array_values($data)]);
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["BS_ID","BS_ID","text","","readonly",0,"hidden"],
            ["*段名","BS_SectName","text","","data-rule='required'","",""],
            ["单段数量","count","text","","readonly",0,""],
            ["单段重量","weight","text","","readonly",0,""],
            // ["备注","BS_Memo","text","","","",""],

        ];
        return $list;
    }

    public function getDetailField(){
        $tableField = [
            ["BD_ID","BD_ID","text","","hidden",""],
            // ["段名","BTXKS_SectName","text","required",""],
            //暂时不知道类型是干嘛用的 先省略 稍后问
            ["类型","BD_Flag","text","readonly","hidden",""],
            ["类型","BD_sFlag","text","","",""],
            ["材料名称","BD_MaterialName","text","","","BD_MaterialName"],
            ["等级","BD_Limber","text","","","BD_Limber"],
            // ["图纸规格","BD_SJType","text","","","BD_Type"],
            // ["图纸无扣长","BD_SJLenth","text","","","BD_Lenth"],
            // ["图纸另附规格","BD_SJOther","text","","","BD_Other"],
            // ["图纸总数","BD_SJSumCount","text","","",""],
            // ["图纸单重","BD_SJSingelWeight","text","","","BD_PerWeight"],
            // ["图纸总重","BD_SJSumWeight","text","","",""],
            // ["","","text","","",""],
            ["实际规格","BD_Type","text","","","BD_Type"],
            ["实际总数","BD_SumCount","text","data-rule='required'","",""],
            ["实际无扣长(mm)","BD_Lenth","text","","","BD_Lenth"],
            ["单重","BD_SingelWeight","text","","","BD_PerWeight"],
            ["实际总重","BD_SumWeight","text","","",""],
            ["另附规格","BD_Other","text","","","BD_Other"],
            ["备注","BD_Memo","text","","",""]
        ];
        return $tableField;
    }

    protected function _getData($scds)
    {
        $scdlist = [];
        $scds_arr=explode(',',implode(",",explode("_",$scds)));
        $scdListNews = (new Sectconfigdetail())->where("SCD_ID","IN",$scds_arr)->column("SCD_ID,SCD_TPNum,SCD_Count,bolt");
        foreach(explode(",",$scds) as $v){
            $arr = explode("_",$v);
            foreach($arr as $vv){
                $scdlist[$arr[0]][$vv] = [
                    "SCD_TPNum" => $scdListNews[$vv]["SCD_TPNum"],
                    "SCD_Count" => $scdListNews[$vv]["SCD_Count"]
                ];
            }
        }
        $scds = implode(",",array_keys($scdlist));
        //判断$scds
        //获取数据
        $db=db();
        $db->query("drop table if exists t1;");
        $db->query("drop table if exists tTuhao;");
        $db->query("create TEMPORARY table t1 as select a.scd_id,b.td_id from sectconfigdetail a join taskheight b on a.th_id=b.th_id where scd_id in ($scds);");
        $db->query("create TEMPORARY table tTuhao as select distinct DtMaterial.TD_ID,ifnull(DtM_sPictureNum,'') AS DtM_sPictureNum    from ".$this->technology_ex."DtMaterial DtMaterial inner join t1 on DtMaterial.TD_ID=t1.TD_ID;");
        $data=$db->query("                        
            SELECT  customer_name,T_Num, C_Num,TD_TypeName,ifnull(old_TypeName,'/') as old_TypeName,TH_Height,C_Project,
            CASE WHEN instr(SCD_TPNum,'WX+')>0 THEN '' ELSE SCD_TPNum END AS SCD_TPNum,SCD_Count,'' AS HB_Memo,Writer,
            NOW() AS HB_WDate,
            BoltSCDSummary.SCD_ID,case when BD_Flag=0 AND BD_MaterialName NOT LIKE '%垫片%' AND BD_MaterialName NOT LIKE '%扣紧%'
              and instr(BD_MaterialName,'普通')<=0 then concat('普通',BD_MaterialName)
            when BD_Flag=1 AND BD_MaterialName NOT LIKE '%垫片%' AND BD_MaterialName NOT LIKE '%扣紧%'
            and instr(BD_MaterialName,'防')<=0 then concat('防盗',BD_MaterialName) else BD_MaterialName end as BD_MaterialName
            , BD_Type,
            case when instr(BD_Type,'*')>0 then cast( substring(BD_Type,instr(BD_Type,'*')+1,CHAR_LENGTH(BD_Type)-instr(BD_Type,'*')) as decimal(18,1))
            else 0.0 end as Lenth,
            case when BD_Limber is not null and CHAR_LENGTH(BD_Limber)>0 then BD_Limber else '' end as BD_Limber,
            BD_SumCount as BD_SumCount,BD_LossCoount AS BD_LossCount, concat((case when BD_Lenth>0 then concat('无扣长',ltrim(BD_Lenth),'MM ') else '' end) ,BD_Other) as BD_Remark ,
            ifnull(BD_SumWeight,0) as BD_SumWeight,
            case when BD_MaterialName like '%防盗螺栓%' then -100
            when BD_MaterialName like '%普通脚钉%' then -90
            when BD_MaterialName like '%防盗脚钉%' then -80
            when BD_MaterialName like '%垫片%' then -70
            when BD_MaterialName like '%普通单帽螺栓%' then -60
            when BD_MaterialName like '%弹垫%' then -50
            when BD_MaterialName like '%平垫%' then -40
            when BD_MaterialName like '%普通双帽螺栓%' then -30
            when BD_MaterialName like '%螺栓%' and BD_Flag=0 then 0 when BD_MaterialName like '%螺栓%' and BD_Flag=1 then 1
            when BD_MaterialName like '%脚钉%' and BD_Flag=0 then 2 when BD_MaterialName like '%脚钉%' and BD_Flag=1 then 3 
            when BD_MaterialName like '%垫%' then 4 else 5 end as NameSort,
            case when BD_Other like '%双帽%' then 1 else 0 end as OtherSort,
            st.SCD_TPNum AS scdsort,'绍兴电力设备有限公司' as B
            from ".$this->technology_ex."BoltSCDSummary BoltSCDSummary INNER JOIN t1 ON BoltSCDSummary.SCD_ID=t1.SCD_ID
              inner join (                
             select  hb.writer,hb.scd_id,scd_tpnum,t.t_num,t.c_num,td.TD_TypeName,'' as old_TypeName,th.th_height,c.C_Project,scd_count,customer_name
                from ".$this->technology_ex."HeightBolt hb
                join sectconfigdetail scd on hb.scd_id=scd.scd_id
                join taskheight th on scd.th_id=th.th_id
                join taskdetail td on th.td_id=td.td_id
                join task t on td.t_num=t.t_num
                join compact c on t.c_num=c.c_num         
            )st on BoltSCDSummary.SCD_ID=st.SCD_ID
              Left join tTuhao on t1.TD_ID=tTuhao.TD_ID
            -- left join (select IM_Measurement,im_class,im_spec from inventorymaterial) im on im_class=BD_MaterialName and im_spec=BD_Type              
            
            order BY C_Num,C_Project,T_Num,TD_TypeName,old_TypeName,TH_Height,scdsort,st.SCD_TPNum ,NameSort,BD_MaterialName,OtherSort,BD_Flag,BD_Type,BD_Limber,Lenth
        ");
		
        $mxdata=[];
        foreach($data as $v){
            if(!isset($mxdata[$v['scdsort']])){
                $mxdata[$v['scdsort']]=[];
            }
            $v['idate']=date('Y-m-d');
            $v['hb']=0;
            $v['hbh']=0;
            if(count($mxdata[$v['scdsort']])>=1){
                if($mxdata[$v['scdsort']][count($mxdata[$v['scdsort']])-1]['BD_MaterialName']!=$v['BD_MaterialName']){
                    $mxdata[$v['scdsort']][count($mxdata[$v['scdsort']])-1]['hb']=1;
                    $v['hbh']=1;
                }
            }
            if(count($mxdata[$v['scdsort']])==0){
                $v['hbh']=1;
            }
//			$v['imemo']='备注：'.$imemo;

            $v["scd_id"] = implode(",",array_keys($scdlist[$v["SCD_ID"]]));
            $v["SCD_TPNum"] = implode(",",array_column($scdlist[$v["SCD_ID"]],"SCD_TPNum"));
            $v["scd_count"] = array_sum(array_column($scdlist[$v["SCD_ID"]],"SCD_Count"));
            $length = 0;
            preg_match_all("/\d+MM/",$v["BD_Remark"],$matches);
            if(!empty($matches) and !empty($matches[0])) $length = trim($matches[0][0],"MM");
            $v["length"] = $length;
            $mxdata[$v['scdsort']][]=$v;
        }
        return [$mxdata,$scdListNews];
    }

    /**
     * 传值请购
     */
    public function apply($scds)
    {
        [$mxdata,$scdListNews] = $this->_getData($scds);
        foreach($scdListNews AS $sv){
            if($sv["bolt"]) $this->error($sv["SCD_TPNum"]."已请购");
        }
        $inventoryList = $this->inventoryNumList(1);
        $mainList = $detailList = [];
        $month = date("Ym");
        $applyModel = (new ApplylistJgj());
        $applyDetailModel = (new ApplydetailsJgj());
        $one = $applyModel->field("AL_Num")->where("AL_Num","LIKE","CGJ".$month.'-%')->order("AL_Num DESC")->find();
        $num = 0;
        if($one) $num = substr($one["AL_Num"],10);
        foreach($mxdata as $k=>$v){
            $num += 1;
            $AL_Num = 'CGJ'.$month.'-'.str_pad($num,3,0,STR_PAD_LEFT);
            $mainList[$AL_Num] = [
                "AL_Num" => $AL_Num,
                "AL_ArriveDate" => "0000-00-00 00:00:00",
                "DD_Num" => "0204",
                "AL_ApplyPepo" => $this->admin["nickname"],
                "AL_WriteDate" => date("Y-m-d H:i:s"),
                "AL_PurchaseType" => "01",
                "AL_Writer" => $this->admin["nickname"],
                "AL_WriterDate" => date("Y-m-d H:i:s"),
                "AL_ProjectName" => $v[0]["C_Project"],
                "al_c_num" => $v[0]["c_num"],
                "al_t_num" => $v[0]["t_num"],
                "scd_id" => $v[0]["scd_id"],
                "scd_count" => $v[0]["scd_count"]
            ];
            foreach($v as $kk=>$vv){
                $im_num = "";
                $newName = mb_substr($vv["BD_MaterialName"],0,2)=="普通"?mb_substr($vv["BD_MaterialName"],2):$vv["BD_MaterialName"];
                if(isset($inventoryList[$vv["BD_MaterialName"]."-".$vv["BD_Type"]])) $im_num = $inventoryList[$vv["BD_MaterialName"]."-".$vv["BD_Type"]]["IM_Num"];
                else if(isset($inventoryList[$newName."-".$vv["BD_Type"]])) $im_num = $inventoryList[$newName."-".$vv["BD_Type"]]["IM_Num"];
                else $this->error("紧固件材料库中不存在".$vv["BD_MaterialName"].$vv["BD_Type"]);
                $detailList[] = [
                    "AL_Num" => $AL_Num,
                    "IM_Num" => $im_num,
                    "IM_Class" => $vv["BD_MaterialName"],
                    "IM_Spec" => $vv["BD_Type"],
                    "L_Name" => $vv["BD_Limber"]?$vv["BD_Limber"]:'',
                    "IM_PerWeight" => round($vv["BD_SumWeight"]/($vv["BD_LossCount"]+$vv["BD_SumCount"]),3),
                    "AD_Length" => $vv["length"],
                    "AD_SCount" => $vv["BD_LossCount"],
                    "AD_BCount" => $vv["BD_SumCount"],
                    "AD_Count" => $vv["BD_LossCount"]+$vv["BD_SumCount"],
                    "AD_Weight" => $vv["BD_SumWeight"]
                ];
            }
        }
        $result = false;
        Db::startTrans();
        try {
            $result = $applyModel->allowField(true)->saveAll($mainList);
            $applyDetailModel->allowField(true)->saveAll($detailList);
            (new Sectconfigdetail())->where("SCD_ID","IN",array_keys($scdListNews))->update([
                "bolt" => 1
            ]);
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $this->success("保存成功");
        } else {
            $this->error(__('No rows were updated'));
        }
    }

    //螺栓汇总打印
    public function showHzPrint($scds){
        [$mxdata,$scdListNews] = $this->_getData($scds);
        $this->assignconfig('mxdata',$mxdata);
        return $this->view->fetch();
    }

	 //螺栓汇总打印不分杆塔
    public function showHzPrintNoscd($scds){
        // pri($scds,1);
        //判断$scds
        $scds = implode(",",explode("_",$scds));
        // $scds_arr = explode(",",implode(",",explode("_",$scds)));
        $scds_arr=explode(',',$scds);
        $js=count($scds_arr);
        //获取数据
        $db=db();
        $db->query("drop table if exists t1;");
        $db->query("drop table if exists tTuhao;");
        $db->query("drop table if exists tall;");
        $db->query("create TEMPORARY table t1 as select a.scd_id,b.td_id from sectconfigdetail a join taskheight b on a.th_id=b.th_id where scd_id in ($scds);");
        $db->query("create TEMPORARY table tTuhao as select distinct DtMaterial.TD_ID,ifnull(DtM_sPictureNum,'') AS DtM_sPictureNum    from ".$this->technology_ex."DtMaterial DtMaterial inner join t1 on DtMaterial.TD_ID=t1.TD_ID;");


        $db->query("create TEMPORARY table tall as 
                        SELECT customer_name,T_Num,C_Num,'多基汇总' AS TD_TypeName,'多基汇总' AS old_TypeName,'多基汇总' AS th_height,C_Project,
                        '多基汇总' AS SCD_TPNum,SCD_Count,'' AS HB_Memo,Writer,
                        NOW() AS HB_WDate,
                        BoltSCDSummary.SCD_ID,
                        case when BD_Flag=0 AND BD_MaterialName NOT LIKE '%垫片%' AND BD_MaterialName NOT LIKE '%扣紧%'
                         and instr(BD_MaterialName,'普通')<=0 then concat('普通',BD_MaterialName)
                        when BD_Flag=1 AND BD_MaterialName NOT LIKE '%垫片%' AND BD_MaterialName NOT LIKE '%扣紧%'
                        and instr(BD_MaterialName,'防')<=0 then concat('防盗',BD_MaterialName) else BD_MaterialName end as BD_MaterialName
                        ,case when @wt='wx' and instr(BD_Type,'*')>0 then substring(BD_Type,1,instr(BD_Type,'*')-1)
                        else BD_Type end as BD_Type,
                        case when instr(BD_Type,'*')>0 then cast( substring(BD_Type,instr(BD_Type,'*')+1,CHAR_LENGTH(BD_Type)-instr(BD_Type,'*')) as decimal(18,1))
                        else 0.0 end as Lenth,
                        case when BD_Limber is not null and CHAR_LENGTH(BD_Limber)>0 then BD_Limber else '' end as BD_Limber,
                        BD_SumCount as BD_SumCount,BD_LossCoount AS BD_LossCount
                        , concat(case when BD_Lenth>0 then concat('无扣长',ltrim(BD_Lenth),'MM ') else '' end ,ifnull(BD_Other,'')) as BD_Remark ,
                        ifnull(BD_SumWeight,0) as BD_SumWeight,
                        case when BD_MaterialName like '%防盗螺栓%' then -100
                        when BD_MaterialName like '%普通脚钉%' then -90
                        when BD_MaterialName like '%防盗脚钉%' then -80
                        when BD_MaterialName like '%垫片%' then -70
                        when BD_MaterialName like '%普通单帽螺栓%' then -60
                        when BD_MaterialName like '%弹垫%' then -50
                        when BD_MaterialName like '%平垫%' then -40
                        when BD_MaterialName like '%普通双帽螺栓%' then -30
                        when BD_MaterialName like '%螺栓%' and BD_Flag=0 then 0 when BD_MaterialName like '%螺栓%' and BD_Flag=1 then 1
                        when BD_MaterialName like '%脚钉%' and BD_Flag=0 then 2 when BD_MaterialName like '%脚钉%' and BD_Flag=1 then 3 
                        when BD_MaterialName like '%垫%' then 4 else 5 end as NameSort,
                        case when BD_Other like '%双帽%' then 1 else 0 end as OtherSort,
                        0 AS scdsort,'绍兴电力设备有限公司'  as B,BD_Flag
                        from ".$this->technology_ex."BoltSCDSummary BoltSCDSummary INNER JOIN t1 ON BoltSCDSummary.SCD_ID=t1.SCD_ID
                          inner join (                
                             select  hb.writer,hb.scd_id,scd_tpnum,t.t_num,t.c_num,td.TD_TypeName,'' as old_TypeName,th.th_height,c.C_Project,scd_count,customer_name
                                from ".$this->technology_ex."HeightBolt hb
                                join sectconfigdetail scd on hb.scd_id=scd.scd_id
                                join taskheight th on scd.th_id=th.th_id
                                join taskdetail td on th.td_id=td.td_id
                                join task t on td.t_num=t.t_num
                                join compact c on t.c_num=c.c_num         
                            )st on BoltSCDSummary.SCD_ID=st.SCD_ID
                          Left join tTuhao on t1.TD_ID=tTuhao.TD_ID
                        order BY C_Num,C_Project,T_Num,TD_TypeName,old_TypeName,TH_Height,scdsort,st.SCD_TPNum ,NameSort,BD_MaterialName,OtherSort,BD_Flag,BD_Type,BD_Limber,Lenth");
//        halt(db()->query("select * from tall"));
        $data=db()->query("SELECT  customer_name,T_Num,c_num,TD_TypeName,old_TypeName,th_height,C_Project,SCD_TPNum,SUM(SCD_Count) AS SCD_Count,HB_Memo,MAX(Writer) AS writer,HB_WDate,0 AS SCD_ID
                                ,BD_MaterialName,BD_Type,Lenth,BD_Limber,(BD_Remark) AS BD_Remark,NameSort,OtherSort,scdsort, B,
                                SUM(BD_SumCount) AS BD_SumCount,SUM(BD_LossCount) AS BD_LossCount,SUM(BD_SumWeight) AS BD_SumWeight
                                FROM tall
                                GROUP BY T_Num,C_Num,TD_TypeName,old_TypeName,TH_Height,C_Project,SCD_TPNum,HB_Memo,HB_WDate
                                ,BD_MaterialName,BD_Type,Lenth,BD_Limber,NameSort,OtherSort,scdsort, B,BD_Flag,BD_Remark
                                order BY C_Num,C_Project,T_Num,TD_TypeName,old_TypeName,NameSort,BD_MaterialName,OtherSort,BD_Flag,BD_Type,BD_Limber,Lenth");

        $mxdata=[];
        foreach($data as $v){
            if(!isset($mxdata[$v['scdsort']])){
                $mxdata[$v['scdsort']]=[];
            }
            $v['idate']=date('Y-m-d');
            $v['hb']=0;
            $v['hbh']=0;
            if(count($mxdata[$v['scdsort']])>=1){
                if($mxdata[$v['scdsort']][count($mxdata[$v['scdsort']])-1]['BD_MaterialName']!=$v['BD_MaterialName']){
                    $mxdata[$v['scdsort']][count($mxdata[$v['scdsort']])-1]['hb']=1;
                    $v['hbh']=1;
                }
            }
            if(count($mxdata[$v['scdsort']])==0){
                $v['hbh']=1;
            }
//			$v['imemo']='备注：'.$imemo;
            $mxdata[$v['scdsort']][]=$v;
        }
        $this->assignconfig('mxdata',$mxdata);
        $this->assignconfig('js',$js);
        return $this->view->fetch();
    }

    /**
	 * 输出到浏览器(需要设置header头)
	 * @param string $fileName 文件名
	 * @param string $fileType 文件类型
	 */
	public function excelBrowserExport($fileName, $fileType) {

	    //文件名称校验
	    if(!$fileName) {
	        trigger_error('文件名不能为空', E_USER_ERROR);
	    }

	    //Excel文件类型校验
	    $type = ['Excel2007', 'Xlsx', 'Excel5', 'xls'];
	    if(!in_array($fileType, $type)) {
	        trigger_error('未知文件类型', E_USER_ERROR);
	    }

	    if($fileType == 'Excel2007' || $fileType == 'Xlsx') {
	        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
	        header('Cache-Control: max-age=0');
	    } else { //Excel5
	        header('Content-Type: application/vnd.ms-excel');
	        header('Content-Disposition: attachment;filename="'.$fileName.'.xls"');
	        header('Cache-Control: max-age=0');
	    }
	}

    public function allDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);

        $row = $this->model->where("HB_ID",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"该条信息已删除，或者请稍后重试"]);
        $result=0;
        Db::startTrans();
        try {
            $result += $this->model->where("HB_ID",$num)->delete();
            $result += $this->sectModel->where("HB_ID",$num)->delete();
            $result += $this->detailModel->where("HB_ID",$num)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result != false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

}
