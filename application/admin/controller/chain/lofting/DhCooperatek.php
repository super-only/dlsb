<?php

namespace app\admin\controller\chain\lofting;

use app\admin\model\chain\lofting\Fytxkdetail;
use app\admin\controller\Technology;
use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 放样电焊件库
 *
 * @icon fa fa-circle-o
 */
class DhCooperatek extends Technology
{
    
    /**
     * DhCooperatek模型对象
     * @var \app\admin\model\chain\lofting\DhCooperatek
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_field = \think\Session::get('technology_field');
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->model = new \app\admin\model\chain\lofting\DhCooperatek([],$this->technology_ex);
        $this->detailModel = new \app\admin\model\chain\lofting\DhCooperatekDetail([],$this->technology_ex);
        $this->singleModel = new \app\admin\model\chain\lofting\DhCooperatekSingle([],$this->technology_ex);
        $this->admin = \think\Session::get('admin');

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("k")
                ->join([$this->technology_ex."fytxk"=>"f"],"k.DtM_iID_PK = f.DtM_iID_PK")
                ->field("k.*,f.DtM_sTypeName,f.DtM_sPressure")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row =  (new \app\admin\model\chain\lofting\DhCooperatek([],$this->technology_ex))->where("DC_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $row = $row->toArray();
        $DtM_iID_PK = $row["DtM_iID_PK"];
        $fytxk_one = (new \app\admin\model\chain\lofting\Library([],$this->technology_ex))->field("DtM_sTypeName,DtM_sPressure")->where("DtM_iID_PK",$DtM_iID_PK)->find();
        $row["DtM_sTypeName"] = $fytxk_one["DtM_sTypeName"];
        $row["DtM_sPressure"] = $fytxk_one["DtM_sPressure"];
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        // if ($this->request->isPost()) {
        //     $params = $this->request->post("row/a");
        //     if ($params) {
        //         $params = $this->preExcludeFields($params);
        //         $result = false;
        //         Db::startTrans();
        //         try {
        //             //是否采用模型验证
        //             if ($this->modelValidate) {
        //                 $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
        //                 $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
        //                 $row->validateFailException(true)->validate($validate);
        //             }
        //             $defaultKey = $this->keyList();
        //             foreach($defaultKey as $v){
        //                 if(isset($params[$v]) && $params[$v]==1) $this->model->where($v,1)->update([$v => 0]);
        //             }
        //             $result = $row->allowField(true)->save($params);
        //             Db::commit();
        //         } catch (ValidateException $e) {
        //             Db::rollback();
        //             $this->error($e->getMessage());
        //         } catch (PDOException $e) {
        //             Db::rollback();
        //             $this->error($e->getMessage());
        //         } catch (Exception $e) {
        //             Db::rollback();
        //             $this->error($e->getMessage());
        //         }
        //         if ($result !== false) {
        //             $this->success();
        //         } else {
        //             $this->error(__('No rows were updated'));
        //         }
        //     }
        //     $this->error(__('Parameter %s can not be empty', ''));
        // }
        $this->view->assign("row", $row);
        $this->view->assign("ids",$row["DC_Num"]);
        return $this->view->fetch();
    }

    public function weldingDrawing()
    {
        $params = $this->request->post();
        $ids = $params["ids"]??"";
        if(!$ids) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $DCD_PartName = $params["DCD_PartName"]??"";
        $DCD_PartNum = $params["DCD_PartNum"]??"";
        $DCD_Type = $params["DCD_Type"]??"";
        $where = ["DC_Num"=>["=",$ids]];
        $DCD_PartName?$where["DCD_PartName"] = ["=",$DCD_PartName]:"";
        $DCD_PartNum?$where["DCD_PartNum"] = ["=",$DCD_PartNum]:"";
        $DCD_Type?$where["DCD_Type"] = ["=",$DCD_Type]:"";
        $list = $this->detailModel->alias("d")->field("*,CAST(DCD_PartName AS UNSIGNED) as number_1,CAST(DCD_PartNum AS UNSIGNED) as number_2,
            case when (length(DCD_PartNum)-length(replace(DCD_PartNum,'-','')))>1 then 
            SUBSTR(DCD_PartNum,1,LOCATE('-',DCD_PartNum)-1)*10000000+SUBSTR(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1),1,LOCATE('-',SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1))-1)*10000+cast(SUBSTR(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1),LOCATE('-',SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1))+1) as signed)
            when LOCATE('-',DCD_PartNum)>0 and LENGTH(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed))=3 then SUBSTR(DCD_PartNum,1,LOCATE('-',DCD_PartNum)-1)*10000000+SUBSTR(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed),1+1) as signed)
            when LOCATE('-',DCD_PartNum)>0 and LENGTH(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed))=4 then SUBSTR(DCD_PartNum,1,LOCATE('-',DCD_PartNum)-1)*10000000+SUBSTR(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DCD_PartNum,LOCATE('-',DCD_PartNum)+1) as signed),2+1) as signed)
            when LENGTH(cast(DCD_PartNum as signed))=3 then SUBSTR(cast(DCD_PartNum as signed),1,1)*10000000+cast(SUBSTR(cast(DCD_PartNum as signed),1+1) as signed)
            when LENGTH(cast(DCD_PartNum as signed))=4 then SUBSTR(cast(DCD_PartNum as signed),1,2)*10000000+cast(SUBSTR(cast(DCD_PartNum as signed),2+1) as signed)
            else 0 end 	bjbhn")->where($where)->order("number_1,bjbhn,DCD_PartNum,number_2 ASC")->select();
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            $rows[$k]["id"] = $k+1;
            $rows[$k]["DCD_SWeight"] = round($v["DCD_SWeight"],2);
            $rows[$k]["DCD_Memo_Input"] = build_input('DCD_Memo_Input', 'text', $v["DCD_Memo"]);
            $rows[$k]["DCD_Type_Input"] = build_input('DCD_Type_Input', 'text', $v["DCD_Type"]);
        }
        return json(["code"=>1,"data"=>$rows]);

    }

    public function detailDrawing()
    {
        $DCD_ID = $this->request->post("DCD_ID");
        if(!$DCD_ID) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $where = ["s.DCD_ID"=>$DCD_ID];
        $arr = $this->singleModel->detailList($where);
        $field = [
            "DHS_ID" => ["hidden",1],
            "DtMD_sPartsID" => ["",0],
            "DtMD_sStuff" => ["",0],
            "DtMD_sMaterial" => ["",0],
            "DtMD_sSpecification" => ["",0],
            "DHS_Thick" => ["",0],
            "DHS_Height" => ["",0],
            "DtMD_fUnitWeight" => ["",0],
            "DHS_Count" => ["",1],
            "DHS_Length" => ["",1],
            "DHS_Grade" => ["",1],
            "DHS_Memo" => ["",1]
        ];
        $tableContent = "";
        foreach($arr as $k=>$v){
            $tableContent .= "<tr>";
            foreach($field as $kk=>$vv){
                $tableContent .= "<td ".$vv[0].">".($vv[1]?build_input($kk,"text",$v[$kk]):$v[$kk])."</td>";
            }
            $tableContent .= "</tr>";
        }
        return json(["code"=>1,"data"=>$tableContent]);
    }

    public function drawingAssemblyDetail($dc_num = null, $dcd=null,$ids=null)
    {
        if($dcd){
            $row = $this->detailModel->alias("d")
                ->join([$this->technology_ex."dhcooperatek"=>"k"],"d.DC_Num = k.DC_Num")
                ->join([$this->technology_ex."fytxk"=>"f"],"f.DtM_iID_PK = k.DtM_iID_PK")
                ->field("d.*,f.DtM_sTypeName,f.DtM_sPressure,f.DtM_iID_PK")
                ->where("d.DCD_ID",$dcd)
                ->find();
        }else{
            $row = $this->model->alias("m")
                ->join([$this->technology_ex."fytxk"=>"f"],"f.DtM_iID_PK = m.DtM_iID_PK")
                ->where("DC_Num",$dc_num)
                ->find();
        }
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $row = $row->toArray();
        $duanList = [0=>"请选择"];
        $duanWhere = [
            "s.DtM_iID_FK" => ["=",$row["DtM_iID_PK"]],
            "d.DtMD_iWelding" => ["<>",0]
        ];
        $duanArr = (new \app\admin\model\chain\lofting\Fytxksect([],$this->technology_ex))->alias("s")
            ->join([$this->technology_ex."fytxkdetail"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("s.Dts_Name,CAST(s.Dts_Name AS UNSIGNED) as number")
            ->where($duanWhere)
            ->group("s.Dts_Name")
            ->order("number")
            ->select();
        foreach($duanArr as $v){
            $duanList[$v["Dts_Name"]] = $v["Dts_Name"];
        }

        $this->view->assign("memoList",$this->memo());
        $this->view->assign("chooseDuan",$duanList);
        $this->view->assign("row",$row);
        // $this->view->assign("tableContent",$arr);
        return $this->view->fetch();
    }
    
    public function leftTable()
    {
        $params = $this->request->post();
        list($DtM_iID_PK,$DtS_Name,$DCD_PartName,$DCD_PartNum) = array_values($params);
        if(!$DtM_iID_PK) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $where = ["s.DtM_iID_FK"=>["=",$DtM_iID_PK],"d.DtMD_iWelding"=>["<>",0]];
        if($DtS_Name) $where["s.DtS_Name"] = ["=",$DtS_Name];
        if($DCD_PartName){
            $partArr = explode(",",$DCD_PartName);
            $where["d.DtMD_sPartsID"] = ["in",$partArr];
        }
        if($DCD_PartNum) $where["d.DtMD_sRemark"] = ["LIKE","%".$DCD_PartName."%"];
        
        $detailList = (new \app\admin\model\chain\lofting\Fytxksect([],$this->technology_ex))->alias("s")
            ->join([$this->technology_ex."fytxkdetail"=>"d"],"s.DtS_ID_PK = d.DtMD_iSectID_FK")
            ->field("s.DtS_Name,d.DtMD_ID_PK,d.DtMD_sPartsID,d.DtMD_iUnitCount,d.DtMD_sRemark,d.DtMD_sStuff,d.DtMD_sMaterial,d.DtMD_sSpecification,d.DtMD_iLength,d.DtMD_fWidth,d.DtMD_iWelding,d.DtMD_fUnitWeight,d.DtMD_iUnitHoleCount,d.DtMD_iCuttingAngle,d.DtMD_fBackOff,d.DtMD_iFireBending,d.DtMD_iPressed,d.DtMD_ISZhuanYong,CAST(s.Dts_Name AS UNSIGNED) as number_1,CAST(d.DtMD_sPartsID AS UNSIGNED) as number_2,
            case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn")
            ->where($where)
            ->order("number_1,bjbhn,DtMD_sPartsID,number_2 ASC")
            ->select();
        $data = [];
		foreach($detailList as $k=>$v){
			$v["DtMD_iUnitCount"] = round($v["DtMD_iUnitCount"],2);
			$data[] = $v->toArray();
		}
        return json(["code"=>1,"data"=>$data]);

    }

    public function rightTable(){
        $params = $this->request->post();
        $DCD_ID = $params["DCD_ID"]??"";
        if(!$DCD_ID) return json(["code"=>0,"msg"=>"有误，请重试"]);
        $field = [
            ["DtMD_sPartsID","text","readonly",""],
            ["DtMD_sStuff","text","readonly",""],
            ["DtMD_sMaterial","text","readonly",""],
            ["DtMD_sSpecification","text","readonly",""],
            ["DHS_Thick","text","readonly",""],
            ["DHS_Height","text","readonly",""],
            ["DHS_Count","text","",""],
            ["DtMD_fUnitWeight","text","",""],
            ["DHS_isM","text","",""],
            ["DHS_Length","text","",""],
            ["DHS_Memo","text","",""],
            ["DHS_ID","text","readonly","hidden"],
            ["DCD_ID","text","readonly","hidden"],
            ["DtMD_ID_PK","text","readonly","hidden"]
        ];
        $where = ["s.DCD_ID"=>$DCD_ID];
        $detailList = $this->singleModel->detailList($where);
        $dcdList = [];
        $tableField = '';
        foreach($detailList as $k=>$v){
            $dcdList[] = $v["DtMD_ID_PK"];
            $tableField .= '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger table_del"><i class="fa fa-trash"></i></a></td>';
            foreach($field as $vv){
                $tableField .= '<td '.$vv[3].'>'.build_input($vv[0],$vv[1],$v[$vv[0]],["class"=>"small_input",$vv[2]=>""]).'</td>';
            }
            $tableField .= "</tr>";
            // foreach($v as $kk=>$vv){

                // $type = "";
                // $visible = "";
                // if(in_array($kk,['DHS_Count','DHS_Length','DHS_Grade','DHS_isM'])) $type = "number";
                // if(in_array($kk,['DHS_Memo','DHS_ID','DCD_ID','DtMD_ID_PK'])) $type = "text";
                // if($type or $visible){
                //     $detailList[$k][$kk] = build_input($kk,$type,$vv,["class"=>"form-control"]);
                // }
            // }
        }
        return json(["code"=>1,"data"=>$tableField,"rightDcdList"=>$dcdList]);
    }

    public function saveDetail()
    {
        $params = json_decode(str_replace('&quot;','"',$this->request->post('data')), true);
        list($row,$tableToArr) = array_values($params);
        if (!$row or !$tableToArr) {
            $this->error(__('No Results were found'));
        }
        $tableRow = [];
        $count = count($tableToArr)/14;
        for($i=0;$i<$count;$i++){
            for($j=0;$j<14;$j++){
                $key = 14*$i+$j;
                $tableRow[$i][$tableToArr[$key]["name"]] = $tableToArr[$key]["value"];
            }
        }
        $list = $tableList = $dt_list = [];
        foreach($row as $v){
            $list[$v["name"]] = $v["value"];
        }
        if($list["DCD_Count"]==false) return json(["code"=>0,"msg"=>"组数不能为0"]);
        $list["Writer"] = $this->admin["nickname"];
        $list["WriteDate"] = date("Y-m-d H:i:s");
        foreach($tableRow as $k=>$v){
            if($v["DHS_Count"]==false) return json(["code"=>0,"msg"=>"单组件数不能为0"]);
            $name = $list["DCD_PartName"]."(-)".$v["DtMD_sPartsID"];
            $tableList[$name] = $v;
            $dt_list[$v["DtMD_ID_PK"]] = ["num"=>$v["DHS_Count"]*$list["DCD_Count"],"name"=>$v["DtMD_sPartsID"]];
        }
        if(in_array($list["DCD_PartName"].'(-)'.$list["DCD_PartNum"],array_keys($tableList))==FALSE) return json(["code"=>0,"msg"=>"电焊件号不存在于放样补件明细中。请重新输入!"]);
        $this->list = $list;
        $find_one = $this->detailModel->alias("d")
            ->join([$this->technology_ex."dhcooperateksingle"=>"s"],"d.DCD_ID=s.DCD_ID")
            ->join([$this->technology_ex."fytxkdetail"=>"dd"],"dd.DtMD_ID_PK = s.DtMD_ID_PK")
            ->where("d.DC_Num",$list["DC_Num"])
            ->where("d.DCD_ID","<>",$list["DCD_ID"])
            ->where(function ($query) {
                $query->where('d.DCD_PartNum', '=', $this->list["DCD_PartNum"])->whereOr('dd.DtMD_sPartsID', '=', $this->list["DCD_PartNum"]);
            })->find();
        if($find_one) return json(["code"=>0,"msg"=>"该组件号已经被使用作为组件号，或该件号在其他件号的电焊明细中，不能再次作为组件号。请重新输入!"]);
        //补充数量相关问题
        
        $fydetailList = $dhkdetailList = [];
        $dt_key = array_keys($dt_list);
        $fydetail = (new Fytxkdetail([],$this->technology_ex))->field("DtMD_iUnitCount,DtMD_ID_PK")->where("DtMD_ID_PK","IN",$dt_key)->select();
        foreach($fydetail as $v){
            $fydetailList[$v["DtMD_ID_PK"]] = $v["DtMD_iUnitCount"];
        }
        $where = [
            "d.DC_Num" => ["=",$list["DC_Num"]],
            "s.DtMD_ID_PK" => ["IN",$dt_key],
            "s.DCD_ID" => ["<>",$list["DCD_ID"]]
        ];
        $dhkdetail = $this->singleModel->alias("s")
            ->join([$this->technology_ex."dhcooperatekdetail"=>"d"],"s.DCD_ID = d.DCD_ID")
            ->field("sum(d.DCD_Count*s.DHS_Count) as sumCount,s.DtMD_ID_PK")
            ->where($where)
            ->group("s.DtMD_ID_PK")
            ->select();
        foreach($dhkdetail as $v){
            $dhkdetailList[$v["DtMD_ID_PK"]] = $v["sumCount"];
        }
        $msg = "";
        foreach($dt_list as $k=>$v){
            $diff_num = $v["num"]+@$dhkdetailList[$k]-@$fydetailList[$k];
            if($diff_num>0) $msg .= $v["name"]."的件数已经超出放样数据中的件数".$diff_num."件。";
        }
        if($msg != "") $msg .= "是否忽略问题?";
        else $msg = "success";
        return json(["code"=>1,"msg"=>$msg,"data"=>["list"=>$list,"tableList"=>json_encode($tableList)]]);
        
    }

    public function saveDetailSect()
    {
        $params = json_decode(str_replace('&quot;','"',$this->request->post('data')), true);
        $save_list = [];
        foreach($params as $v){
            $save_list[$v["DCD_ID"]] = [
                "DCD_ID" => $v["DCD_ID"],
                "DCD_Memo" => $v["DCD_Memo"],
                "DCD_Type" => $v["DCD_Type"]
            ];
        }
        $result = $this->detailModel->allowField(true)->saveAll($save_list);
        if($result) return json(["code"=>1,"msg"=>"保存成功！"]);
        else return json(["code"=>0,"msg"=>"保存失败！请刷新后重试！"]);
    }

    public function saveEditDetail()
    {
        $params = json_decode(str_replace('&quot;','"',$this->request->post('data')), true);
        if (!$params) {
            return json(["code"=>0,"msg"=>"保存失败！请刷新后重试！"]);
        }
        if (empty($params)) {
            return json(["code"=>0,"msg"=>"无须保存！"]);
        }
        $tableRow = [];
        $count = count($params)/5;
        for($i=0;$i<$count;$i++){
            for($j=0;$j<5;$j++){
                $key = 5*$i+$j;
                $tableRow[$i][$params[$key]["name"]] = $params[$key]["value"];
            }
        }
        $result = false;
        Db::startTrans();
        try {
            $result = $this->singleModel->allowField(true)->saveAll(array_values($tableRow));
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result) {
            return json(["code"=>1,"msg"=>"保存成功"]);
        } else {
            return json(["code"=>0,"msg"=>__('No rows were deleted')]);
        }
        
    }

    public function sureSaveDetail()
    {
        $params = $this->request->post();
        list($list,$tableList) = array_values($params);
        if(!$list or !$tableList) return json(["code"=>0,"msg"=>"有误请重试"]);
        $tableList = json_decode(str_replace('&quot;','"',$tableList), true);
        $sum = 0;
        // $dtm_model = new Fytxkdetail();
        foreach($tableList as $v){
            $sum += round($v["DHS_Count"]*$v["DtMD_fUnitWeight"],2);
        }
        Db::startTrans();
        try {
            if($list["DCD_ID"]=="") unset($list["DCD_ID"]);
            $list["DCD_SWeight"] = $sum;
            $result = $this->detailModel->allowField(true)->saveAll([0=>$list]);
            $DCD_ID = $list["DCD_ID"]??$result[0]["DCD_ID"];
            if($DCD_ID){
                
                $this->singleModel->where("DCD_ID",$DCD_ID)->delete();
                foreach($tableList as $k=>$v){
                    unset($tableList[$k]["DtMD_fUnitWeight"],$tableList[$k]["DHS_ID"]);
                    $tableList[$k]["DCD_ID"] = $DCD_ID;
                }
                $this->singleModel->allowField(true)->saveAll(array_values($tableList));
            }else{
                return json(["code"=>0,"msg"=>'保存失败请重试']);
            }
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if($result !== false){
            return json(["code"=>1,"msg"=>'保存成功',"dc_num"=>$result[0]["DC_Num"],"dcd"=>$result[0]["DCD_ID"]]);
        }else{
            return json(["code"=>0,"msg"=>"保存失败，请重试"]);
        }
        
    }

    public function delDetail($DCD_ID=NULL)
    {
        // $params = $this->request->post("num");
        if(!$DCD_ID) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);
        $dcd_id_list = explode(",",$DCD_ID);
        Db::startTrans();
        try {
            $this->detailModel->where("DCD_ID", "IN", $dcd_id_list)->delete();
            $this->singleModel->where("DCD_ID", "IN", $dcd_id_list)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        return json(["code"=>1,"msg"=>"删除成功","DCD_ID"=>$DCD_ID]);
    }

    public function selectTower(){
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\chain\lofting\Library([],$this->technology_ex))
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $year = date("Y");
                $find = $this->model->field("DC_Num")->where("DC_Num","like","DHK".$year.'%')->order("DC_Num DESC")->find();
                if($find) $DC_Num = "DHK".(substr($find["DC_Num"],3)+1);
                else $DC_Num = "DHK".$year."0001";
                $params["DC_Num"] = $DC_Num;
                $params["DC_EditDate"] = date("Y-m-d H:i:s");
                $params["DC_Editer"] = $this->admin["nickname"];
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('success',null,$DC_Num);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    public function typeList($DCD_ID = null)
    {
        $row = $this->detailModel->where("DCD_ID",$DCD_ID)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $result = $this->detailModel->where("DCD_ID",$DCD_ID)->update($params);
                if ($result != false) {
                    $this->success("成功",'',["DCD_Type"=>$params["DCD_Type"],"DCD_ID"=>$DCD_ID]);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = [
            ""=>"请选择",
            "主管焊件"=>"主管焊件",
            "支管 两头插板"=>"支管 两头插板",
            "支管 两头插板中间节点板"=>"支管 两头插板中间节点板",
            "支管 两头插板中间相贯管"=>"支管 两头插板中间相贯管",
            "支管 两头法兰"=>"支管 两头法兰",
            "支管 两头法兰中间节点板"=>"支管 两头法兰中间节点板",
            "支管 两头法兰中间相贯管"=>"支管 两头法兰中间相贯管",
            "支管 一头法兰一头插板"=>"支管 一头法兰一头插板",
            "支管 一头法兰 一头插板中间节点板"=>"支管 一头法兰 一头插板中间节点板",
            "支管 一头法兰一头插板中间相贯管"=>"支管 一头法兰一头插板中间相贯管",
            "扶手焊件"=>"扶手焊件",
            "爬梯焊件"=>"爬梯焊件",
            "联板角钢焊件"=>"联板角钢焊件",
            "其他焊件"=>"其他焊件",
        ];
        $this->view->assign("row",$row);
        $this->view->assign("list",$list);
        return $this->view->fetch();
    }

    public function memoList($DCD_ID = null)
    {
        $row = $this->detailModel->where("DCD_ID",$DCD_ID)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $result = $this->detailModel->where("DCD_ID",$DCD_ID)->update($params);
                if ($result != false) {
                    $this->success("成功",'',["DCD_Memo"=>$params["DCD_Memo"],"DCD_ID"=>$DCD_ID]);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = [
            "带脚钉",
            "两端焊",
            "组焊",
            "中间焊",
            "自焊",
            "开口焊",
            "焊塔脚",
            "假脚",
            "一级焊缝",
            "二级焊缝",
            "一端焊"
        ];
        $this->view->assign("row",$row);
        $this->view->assign("list",$list);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->select();

            $detailDel = $singleDel = [];
            $joinDetailList = $this->detailModel->field("DCD_ID")->where($pk, 'in', $ids)->select();
            foreach($joinDetailList as $v){
                $detailDel[] = $v["DCD_ID"];
            }
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                }
                $detailCount = $this->detailModel->where($pk, 'in', $ids)->delete();
                !empty($detailDel)?$singleCount = $this->singleModel->where("DCD_ID",'in',$detailDel)->delete():"";
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    protected function memo()
    {
        $list = ["带脚钉","组焊","自焊","焊塔脚","一级焊缝","二级焊缝","一端焊","二端焊","中间焊","开口焊"];
        return $list;
    }
}
