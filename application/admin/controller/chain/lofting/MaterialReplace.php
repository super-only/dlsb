<?php

namespace app\admin\controller\chain\lofting;

use app\admin\controller\Technology;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class MaterialReplace extends Technology
{
    
    /**
     * MaterialReplace模型对象
     * @var \app\admin\model\chain\lofting\MaterialReplace
     */
    protected $model = null;
    protected $noNeedLogin = ["delDetail"];

    public function _initialize()
    {
        parent::_initialize();
        $this->technology_ex = \think\Session::get('technology_ex');
        $this->technology_type = \think\Session::get('technology_type');
        $this->model = new \app\admin\model\chain\lofting\MaterialReplace;
        $this->detailModel = new \app\admin\model\chain\lofting\MaterialReplaceDetail;
        $deptList = [""=>"请选择"];
        $deptModel = (new \app\admin\model\jichu\jg\Deptdetail())
            ->field("DD_Name")
            ->where(["Valid"=>1])
            ->order(["ParentNum"=>"ASC","DD_Num"=>"ASC"])
            ->select();
        foreach($deptModel as $v){
            $deptList[$v["DD_Name"]] = $v["DD_Name"];
        }
        $this->view->assign("deptList",$deptList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("mr")
                ->join(["taskdetail"=>"td"],"td.TD_ID = mr.TD_ID")
                ->join(["task"=>"t"],"t.T_Num = td.T_Num")
                ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
                ->field("mr.MR_Num,mr.MR_Dept,t.T_Num,c.Customer_Name,t.t_project,td.TD_TypeName,mr.Writer,mr.WriterDate,mr.TD_ID")
                ->where("c.produce_type",$this->technology_type)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function edit($ids=null)
    {
        $row = $this->model->alias("mr")
            ->join(["taskdetail"=>"td"],"td.TD_ID = mr.TD_ID")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->field("mr.MR_Num,mr.MR_Dept,t.T_Num,c.Customer_Name,t.t_project,td.TD_TypeName,mr.Writer,mr.WriterDate,mr.TD_ID,mr.MR_Memo,'' as PT_Num,WriterMemo")
            ->where("mr.MR_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                foreach($paramsTable["MRD_ID"] as $k=>$v){
                    $sectSaveList[$k] = [
                        "MRD_ID" => $v,
                        "MRD_Memo" => $paramsTable["MRD_Memo"][$k]
                    ];
                    if($v==false) unset($sectSaveList[$k]);
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("MR_Num",$ids)->update($params);
                    if(!empty($sectSaveList)) $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $this->detailModel
            ->field("*,case IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when IM_Class='角钢' then 
            SUBSTR(REPLACE(OldType,'∠',''),1,locate('*',REPLACE(OldType,'∠',''))-1)*1000+
            SUBSTR(REPLACE(OldType,'∠',''),locate('*',REPLACE(OldType,'∠',''))+1,2)*1
            when (IM_Class='钢板' or IM_Class='法兰') then 
            REPLACE(OldType,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("MR_Num",$ids)->order("clsort ASC,OldLimber ASC,bh ASC")->select();
        $rows = [];
        $weight = 0;
        $pt_num = "";
        foreach($list as $k=>$v){
            // $v["AddWeight"] = round($v["AddWeight"],2);
            $rows[$k] = $v->toArray();
            // $weight += $v["AddWeight"];
            $pt_num = $v["PT_Num"];
        }
        if($list) $row["PT_Num"] = $pt_num;
        $row["weight"] = $weight;
        $tableField = $this->getDetailField();
        $this->view->assign("row", $row);
        $this->view->assign("tableField",$tableField);
        $this->view->assign("tableList",$rows);
        $this->assignconfig("tableField",$tableField);
        $this->assignconfig("ids", $ids);
        return $this->view->fetch();
    }

    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $count = 0;
            Db::startTrans();
            try {
                $count += $this->model->where("MR_Num",$ids)->delete();
                $count += $this->detailModel->where("MR_Num",$ids)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    //编辑table中某一个detail的删除
    public function delDetail()
    {
        $num = $this->request->post("num");
        if($num){
            Db::startTrans();
            try {
                $this->detailModel->where("MRD_ID",$num)->delete();
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>1,'msg'=>"成功！"]);
    }

    public function getDetailField(){
        $tableField = [
            ["MRD_ID","MRD_ID","readonly","hidden","save"],
            ["下达单号","PT_Num","readonly","",""],
            ["件号","OldPart","","","save"],
            ["材料名称","IM_Class","readonly","",""],
            // ["原材料","IM_Class","readonly","",""],
            ["原材质","OldLimber","readonly","",""],
            ["原规格","OldType","readonly","",""],
            ["原长度(mm)","OldLength","readonly","",""],
            ["原宽度(mm)","OldWidth","readonly","",""],
            ["原数量","OldCount","readonly","",""],
            ["代用材质","NewLimber","readonly","",""],
            ["代用材料","NewClass","readonly","",""],
            ["代用规格","NewType","readonly","",""],
            ["代用长度(mm)","NewLength","readonly","",""],
            ["代用宽度(mm)","NewWidth","readonly","",""],
            ["代用数量","NewCount","readonly","",""],
            ["计划重(kg)","OldWeight","readonly","",""],
            ["代用重(kg)","NewWeight","readonly","",""],
            // ["增重(kg)","AddWeight","readonly","",""],
            ["备注","MRD_Memo","","","save"]
        ];
        return $tableField;
    }

    public function print($ids=null)
    {
        $row = $this->model->alias("mr")
            ->join(["taskdetail"=>"td"],"td.TD_ID = mr.TD_ID")
            ->join(["task"=>"t"],"t.T_Num = td.T_Num")
            ->join(["compact"=>"c"],"c.C_Num = t.C_Num")
            ->field("mr.MR_Num,mr.MR_Dept,t.T_Num,c.Customer_Name,t.t_project,td.TD_TypeName,mr.Writer,mr.WriterDate,mr.TD_ID,mr.MR_Memo,'' as PT_Num,WriterMemo")
            ->where("mr.MR_Num",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        
        $list = $this->detailModel
            ->field("*,case IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
            when IM_Class='角钢' then 
            SUBSTR(REPLACE(OldType,'∠',''),1,locate('*',REPLACE(OldType,'∠',''))-1)*1000+
            SUBSTR(REPLACE(OldType,'∠',''),locate('*',REPLACE(OldType,'∠',''))+1,2)*1
            when (IM_Class='钢板' or IM_Class='法兰') then 
            REPLACE(OldType,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("MR_Num",$ids)->order("clsort ASC,OldLimber ASC,bh ASC")->select();
        $rows = [];
        $weight = 0;
        $pt_num = "";
        foreach($list as $k=>$v){
            // $v["AddWeight"] = round($v["AddWeight"],2);
            $rows[$k] = $v->toArray();
            // $weight += $v["AddWeight"];
            $pt_num = $v["PT_Num"];
        }
        if($list) $row["PT_Num"] = $pt_num;
        $row["weight"] = $weight;
        $row["WriterDate"] = date("Y-m-d", strtotime($row["WriterDate"]));
     
        $this->assignconfig("row",$row);
        $this->assignconfig("tableList",$rows);
        
        return $this->view->fetch();
    }


}
