<?php

namespace app\admin\controller\chain\machine;

use app\admin\model\chain\machine\WjStoreIn;
use app\admin\model\chain\machine\WjStoreOut;
use app\common\controller\Backend;

/**
 * 库存查询
 *
 * @icon fa fa-circle-o
 */
class ConStatements extends Backend
{
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $where = [
            "ParentNum" => ["=","01"],
            "WC_Name" => ["<>",""],
            "WC_Sort" => ["=","五金"]
        ];
        $this->warehouse_list = $this->wareclassList($where,1);
        $this->assignconfig("warehouse_list",$this->warehouse_list);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            if(!isset($filter["WC_Num"])) $this->error("请选择仓库！");
            $wc_name = $this->warehouse_list[$filter["WC_Num"]];
            $where_in = $where_out = ["w.Assessor" => ["<>",""]];
            $DJ_Date = explode(" - ",$filter["DJ_Date"]);
            $where_out["CK_Date"] = $where_in["RK_Date"] = ["between time",$DJ_Date];
            if(isset($filter["WC_Num"])) $where_out["WC_Num"] = $where_in["WC_Num"] = ["=",$filter["WC_Num"]];
            if(isset($filter["Department"])) $where_out["CK_TakeDept"] = $where_in["RK_Department"] = ["LIKE","%".$filter["Department"]."%"];
            if(isset($filter["Sort"])) $where_out["CK_CKSort"] = $where_in["RK_RKSort"] = ["=",$filter["Sort"]];
            if(isset($filter["IR_Num"])) $where_out["d.IR_Num"] = $where_in["d.IR_Num"] = ["=",$filter["IR_Num"]];
            $in_list = (new WjStoreIn())->alias("w")
                ->join(["wjstoreindetail"=>"d"],"w.RK_ID = d.RK_ID")
                ->join(["wjinventoryrecord"=>"wir"],"d.IR_Num = wir.IR_Num")
                ->join(["wjinventorysort"=>"wis"],"wir.sortNum = wis.IS_Num")
                ->field("RK_Num as DJ_Num,RK_Date as DJ_Date,'".$wc_name."' as WC_Num,RK_Department as Department,RK_RKSort as WJ_Sort,d.IR_Num,wir.IR_Name,wis.IS_Name as Sort,wir.IR_Spec,wir.IR_Unit,RKDe_Count,RKDe_NotaxPrice,RKDe_NotaxMoney,'' as CKDe_Count,'' as ckde_PriceNoTax,'' as ckde_MoneyNoTax,RKDe_Memo as memo")
                ->where($where_in)
                ->order("w.RK_Date,wis.IS_Num,d.IR_Num asc")
                ->select();
            $total_in_row = ["RKDe_Count"=>0,"RKDe_NotaxMoney"=>0];
            $in_total_list = [];
            foreach($in_list as $k=>$v){
                foreach(["RKDe_Count","RKDe_NotaxPrice","RKDe_NotaxMoney"] as $vv){
                    $v[$vv] = round($v[$vv],3);
                    if(isset($total_in_row[$vv])){
                        $total_in_row[$vv] += $v[$vv];
                    }
                    if(!$v[$vv]) $v[$vv] = "";
                }
                $in_total_list[$v["DJ_Date"]][$v["DJ_Num"]][$v["IR_Num"]][] = $v->toArray();
            }
            $out_list = (new WjStoreOut())->alias("w")
                ->join(["wjstoreoutdetail"=>"d"],"w.CK_ID = d.CK_ID")
                ->join(["wjinventoryrecord"=>"wir"],"d.IR_Num = wir.IR_Num")
                ->join(["wjinventorysort"=>"wis"],"wir.sortNum = wis.IS_Num")
                ->field("CK_Num as DJ_Num,CK_Date as DJ_Date,'".$wc_name."' as WC_Num,CK_TakeDept as Department,CK_CKSort as WJ_Sort,d.IR_Num,wir.IR_Name,wis.IS_Name as Sort,wir.IR_Spec,wir.IR_Unit,'' as RKDe_Count,'' as RKDe_NotaxPrice,'' as RKDe_NotaxMoney,CKDe_Count,ckde_PriceNoTax,ckde_MoneyNoTax,CKDe_Use as memo")
                ->where($where_out)
                ->order("w.CK_Date,wis.IS_Num,d.IR_Num asc")
                ->select();
            $total_out_row = ["CKDe_Count"=>0,"ckde_MoneyNoTax"=>0];
            foreach($out_list as $k=>$v){
                foreach(["CKDe_Count","ckde_PriceNoTax","ckde_MoneyNoTax"] as $vv){
                    $v[$vv] = round($v[$vv],3);
                    if(isset($total_out_row[$vv])){
                        $total_out_row[$vv] += $v[$vv];
                    }
                    if(!$v[$vv]) $v[$vv] = "";
                }
                $in_total_list[$v["DJ_Date"]][$v["DJ_Num"]][$v["IR_Num"]][] = $v->toArray();
            }
            $total_list = [];
            ksort($in_total_list);
            foreach($in_total_list as $v){
                ksort($v);
                foreach($v as $vv){
                    ksort($vv);
                    foreach($vv as $vvv){
                        ksort($vvv);
                        foreach($vvv as $vvvv)
                            $total_list[] = $vvvv;
                    }
                }
            }
            foreach($total_in_row as $k=>$v){
                $total_in_row[$k] = round($v,3);
            }
            foreach($total_out_row as $k=>$v){
                $total_out_row[$k] = round($v,3);
            }
            $total_list[] = ["IR_Num"=>"合计"]+$total_in_row+$total_out_row;

            $result = array("total" => count($total_list), "rows" => $total_list);

            return json($result);
        }
        $defaultTime = date("Y-m-1 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        return $this->view->fetch();
    }
}
