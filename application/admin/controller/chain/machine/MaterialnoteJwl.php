<?php

namespace app\admin\controller\chain\machine;

use app\admin\model\chain\machine\WjStoreInDetail;
use app\admin\model\jichu\wl\Vendor;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 机物料到货单
 *
 * @icon fa fa-circle-o
 */
class MaterialnoteJwl extends Backend
{
    
    /**
     * MaterialnoteJwl模型对象
     * @var \app\admin\model\chain\machine\MaterialnoteJwl
     */
    protected $model = null,$detailModel = null,$wjClassList,$admin,$vendorList,$classList,$classTree,$strandList,$shipplingList;
    protected $noNeedLogin = ["chooseMaterial"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\machine\MaterialnoteJwl;
        $this->detailModel = new \app\admin\model\chain\machine\MaterialGetNoticeJwl;
        // $this->relatedModel = new \app\admin\model\chain\material\MaterialGetRelated;
        $this->vendorList = $this->vendorNumList(1);
        $shipplingList = $this->shipplingList(1);
        $this->admin = \think\Session::get('admin');
        $this->shipplingList = $shipplingList;
        $this->view->assign("shippling",$shipplingList);
        list($classList,$classTree) = $this->vendorList();
        $this->classList = $classList;
        $this->classTree = $classTree;
        $this->strandList = [
            1 => "国网",
            2 => "南网",
            3 => "国标",
            4 => "其他"
        ];
        $this->view->assign("strandList",$this->strandList);
        $this->wjClassList = $this->wjInventClassList()[0];
        $this->assignconfig("wjClassList",$this->wjClassList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        // $vendorList = $this->vendorList;
        // $shipplingList = $this->shipplingList;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $ini_where = [];
            if(isset($filter["is_check"])){
                if($filter["is_check"]=="1") $ini_where["m.Auditor"] = ["=",""];
                if($filter["is_check"]=="2") $ini_where["m.Auditor"] = ["<>",""];
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("m")
                ->join(["vendor"=>"v"],"m.V_Num = v.V_Num","left")
                ->join(["shippingtype"=>"st"],"st.ST_Num = m.ST_Num","left")
                ->where($where)
                ->where($ini_where)
                ->order($sort, $order)
                ->paginate($limit);
            $row = [];
            foreach($list->items() as $k=>$v){
                $row[$k] = $v->toArray();
                // $row[$k]["V_Num"] = @$vendorList[$v["V_Num"]];
                // $row[$k]["ST_Num"] = @$shipplingList[$v["ST_Num"]];
                $row[$k]["is_check"] = $v["Auditor"]?"已审核":"未审核";
                // $row[$k]["WriteTime"] = date("Y-m-d",strtotime($v["WriteTime"]));
                // $row[$k]["AuditTime"] = date("Y-m-d",strtotime($v["AuditTime"]));
                // $row[$k]["ApproveTime"] = date("Y-m-d",strtotime($v["ApproveTime"]));
                // $row[$k]["MN_Date"] = date("Y-m-d",strtotime($v["MN_Date"]));
                // $row[$k]["ModifyTime"] = date("Y-m-d",strtotime($v["ModifyTime"]));
                // $row[$k]["mn_deliverdate"] = date("Y-m-d",strtotime($v["mn_deliverdate"]));
            }
            $result = array("total" => $list->total(), "rows" => $row);

            return json($result);
        }
        // $this->assignconfig("vendorList",$vendorList);
        // $this->assignconfig("shipplingList",$shipplingList);
        return $this->view->fetch();
    }

    public function requisitionDetail()
    {
        $ids = $this->request->post("ids");
        if(!$ids) return json(["code"=>0,"msg"=>"有误"]);
        $list = $this->detailModel->alias("ad")
            ->join(["wjinventoryrecord"=>"im"],"ad.IM_Num = im.IR_Num")
            ->field("ad.MGN_ID,ad.MN_Num,ad.IM_Num,im.IR_Name,im.IR_Spec,im.sortNum,ad.MGN_Count,ad.MGN_Memo,ad.mgn_price,ad.MGN_Destination,ad.mgn_testnum,ad.MGN_Maker,ad.mgn_DuiFangDian")
            ->where("ad.MN_Num",$ids)
            ->order("ad.IM_Num ASC")
            ->select();
        $sum_weight = $count = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            // $rows[$k]["MGN_Length"] = round($v['MGN_Length'],3);
            // $rows[$k]["MGN_Width"] = round($v['MGN_Width'],3);
            $rows[$k]["MGN_Count"] = round($v['MGN_Count'],2);
            // $rows[$k]["MGN_Weight"] = round($v['MGN_Weight'],2);
            $rows[$k]["mgn_price"] = round($v['mgn_price'],2);
            // $sum_weight += $v["MGN_Weight"];
            $count += $v["MGN_Count"];
        }
        $rows[] = ["MGN_ID"=>"合计","MGN_Weight"=>round($sum_weight,2),"MGN_Count"=>$count];
        return json(["code"=>1,"data"=>$rows]);
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                if(!$params["V_Num"]) $this->error('供应商必须选择！');
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $MN_Num = $params["MN_Num"];
                if($MN_Num){
                    $one = $this->model->where("MN_Num",$MN_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("ym");
                    $one = $this->model->field("MN_Num")->where("MN_Num","LIKE","DH".$month.'-%')->order("MN_Num DESC")->find();
                    if($one){
                        $num = substr($one["MN_Num"],7);
                        $MN_Num = 'DH'.$month.'-'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $MN_Num = 'DH'.$month.'-001';
                }
                $params["MN_Num"] = $MN_Num;
                // $htList = [];
                // $replaceList = json_decode($params["ht"],true);
                // foreach($replaceList as $k=>$v){
                //     $htList[$v] = [
                //         "MN_Num" => $params["MN_Num"],
                //         "pm_num" => $v
                //     ];
                // }
                // unset($params["ht"]);
                $sectSaveList = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk]["MN_Num"] = $params["MN_Num"];
                        if($k=="MGN_ID" and !$vv) continue;
                        // else if($k=="MGN_Length" or $k=="MGN_Width") $sectSaveList[$kk][$k] = $vv?$vv*1000:0;
                        // else if($k=="mgn_formuleweight") $sectSaveList[$kk][$k] = $vv?($vv/1000):($paramsTable["MGN_Weight"][$kk]/1000);
                        else if($vv=="") continue;
                        // else if($vv=="CTD_Weight") $sectSaveList[$kk]["CTD_GuoBanWeight"] = $vv;
                        else $sectSaveList[$kk][$k] = $vv;
                        
                    }
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model::create($params);
                    $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    // if(!empty($htList)) $htResult = $this->relatedModel->allowField(true)->saveAll($htList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success('成功！',null,$result["MN_ID"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        list($deptList,$deptTree) = $this->deptType(1);
        $purchaseList = $this->purchaseList(1);
        $tableField = $this->getTableField();
        list($limberList,$kj_limberList) = $this->getLimber();

        $vendor = (new Vendor())->field("V_Num,V_Name,V_ShortName")->where("V_ShortName","<>","")->select();
        $tableTr = "";
        foreach($vendor as $v){
            $tableTr .= "<tr><td>".$v["V_Num"]."</td><td>".$v["V_Name"]."</td><td>".$v["V_ShortName"]."</td></tr>";
        }

        $this->view->assign("vendorTable",$tableTr);
        $this->view->assign("row",$row);
        $this->view->assign("deptList",$deptList);
        $this->view->assign("purchaseList",$purchaseList);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->view->assign("limberList",$limberList);
        $this->assignconfig('limberList',$limberList);
        $this->assignconfig('kj_limberList',$kj_limberList);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->alias("m")
            ->field("m.*,v.V_Name")
            ->join(["vendor"=>"v"],"v.V_Num = m.V_Num")
            ->where("m.MN_ID",$ids)
            ->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        // $init_data = $this->relatedModel->where("MN_Num",$row["MN_Num"])->column("pm_num");
        // $row["ht"] = json_encode($init_data);
        $MN_Num = $row["MN_Num"];
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                // $si_one = $this->detailModel->alias("d")->join(["commisiontestdetail"=>"ctd"],"ctd.MGN_ID = d.MGN_ID")->where("d.MN_Num",$MN_Num)->find();
                // if($si_one) $this->error("已存在理化，不能进行修改！");

                $params = $this->preExcludeFields($params);
                
                $sectSaveList = [];
                foreach($paramsTable as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $sectSaveList[$kk]["MN_Num"] = $MN_Num;
                        if($k=="MGN_ID" and !$vv) continue;
                        else if($k=="MGN_Length" or $k=="MGN_Width") $sectSaveList[$kk][$k] = $vv?$vv*1000:0;
                        else if($k=="mgn_formuleweight") $sectSaveList[$kk][$k] = $vv?($vv/1000):($paramsTable["MGN_Weight"][$kk]/1000);
                        else if($vv=="") continue;
                        // else if($vv=="CTD_Weight") $sectSaveList[$kk]["CTD_GuoBanWeight"] = $vv;
                        else $sectSaveList[$kk][$k] = $vv;
                        
                    }
                }
                // $qgList = [];
                // $replaceList = json_decode($params["ht"],true);
                // foreach($replaceList as $k=>$v){
                //     $qgList[$v] = [
                //         "MN_Num" => $row["MN_Num"],
                //         "pm_num" => $v
                //     ];
                // }
                // unset($params["ht"]);
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->where("MN_ID",$ids)->update($params);
                    // $this->relatedModel->where("MN_Num",$row["MN_Num"])->delete();
                    // if(!empty($qgList)) $qgResult = $this->relatedModel->allowField(true)->saveAll($qgList);
                    if($row["Auditor"]=="") $this->detailModel->allowField(true)->saveAll($sectSaveList);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $this->detailModel->alias("mgn")
            ->join(["wjinventoryrecord"=>"im"],"mgn.IM_Num = im.IR_Num")
            ->field("mgn.MGN_ID,mgn.IM_Num,mgn.MGN_Count,mgn.MGN_Weight,mgn.mgn_price,mgn.MGN_Destination,mgn.mgn_testnum,mgn.mgn_pi,mgn.MGN_Maker,mgn.mgn_DuiFangDian,mgn.MGN_Memo,im.IR_Name,im.IR_Spec,im.sortNum,pm_id,way")
            ->where("mgn.MN_Num",$MN_Num)
            ->order("mgn.L_Name,MGN_Length ASC")
            ->select();
        $sum_weight = $count = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $rows[$k] = $v->toArray();
            // $rows[$k]["MGN_Length"] = round($v['MGN_Length'],3);
            // $rows[$k]["MGN_Width"] = round($v['MGN_Width'],3);
            $rows[$k]["MGN_Count"] = round($v['MGN_Count'],2);
            $rows[$k]["sortNum"] = $this->wjClassList[$v["sortNum"]];
            // $rows[$k]["MGN_Weight"] = round($v['MGN_Weight'],2);
            // $rows[$k]["mgn_formuleweight"] = round($v['mgn_formuleweight'],2);
            // $sum_weight += $v["MGN_Weight"];
            $count += $v["MGN_Count"];
        }
        $row["sum_weight"] = $sum_weight;
        $row["count"] = $count;
        $tableField = $this->getTableField();
        list($limberList,$kj_limberList) = $this->getLimber();
        // $this->assignconfig("init_data",$init_data);
        $this->view->assign("row",$row);
        $this->view->assign("list",$rows);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->view->assign("limberList",$limberList);
        $this->assignconfig('limberList',$limberList);
        $this->assignconfig('kj_limberList',$kj_limberList);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    // public function export($ids=null)
    // {
    //     $row = $this->model->get($ids);
    //     if (!$row) {
    //         $this->error(__('No Results were found'));
    //     }
    //     $title = $row["MN_Num"];

    //     $list = $this->detailModel->alias("mgn")
    //         ->join(["inventorymaterial"=>"im"],"mgn.IM_Num = im.IM_Num")
    //         ->field("mgn.MGN_ID,mgn.IM_Num,mgn.L_Name,round(mgn.MGN_Length/1000,3) as MGN_Length,round(mgn.MGN_Width/1000,3) as MGN_Width,mgn.MGN_Count,mgn.MGN_Weight,round(mgn.mgn_formuleweight*1000,2) as mgn_formuleweight,mgn.mgn_price,mgn.MGN_Destination,mgn.mgn_testnum,mgn.mgn_pi,mgn.MGN_Maker,mgn.mgn_DuiFangDian,mgn.MGN_Memo,im.IM_Class,im.IM_Spec,im.IM_PerWeight,case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,cast((case 
    //         when im.IM_Class='角钢' then 
    //         SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
    //         SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
    //         when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
    //         REPLACE(im.IM_Spec,'-','')
    //         else 0
    //         end) as UNSIGNED) bh")
    //         ->where("mgn.MN_Num",$title)
    //         ->order("clsort,mgn.L_Name,bh,MGN_Length ASC")
    //         ->select();
    //     foreach($list as $k=>$v){
    //         $list[$k]["ID"] = $k+1;
    //         $list[$k]["MGN_Length"] = round($v["MGN_Length"],2);
    //         $list[$k]["MGN_Width"] = round($v["MGN_Width"],2);
    //         $list[$k]["MGN_Count"] = round($v['MGN_Count'],2);
    //         $list[$k]["MGN_Weight"] = round($v['MGN_Weight'],2);
    //         $list[$k]["mgn_formuleweight"] = round($v['mgn_formuleweight'],2);
    //     }
    //     $tableField = $this->getTableField();
    //     $header = [['ID', 'ID']];
    //     foreach($tableField as $k=>$v){
    //         if($v[5]==""){
    //             $header[] = [$v[0],$v[1]];
    //         }
    //     }
    //     // $header = [
    //     //     ['ID', 'ID'],
    //     //     ['材料名称', 'IM_Class'],
    //     //     ['规格', 'IM_Spec'],
    //     //     ['材质', 'L_Name'],
    //     //     ['长度(m)', 'AD_Length'],
    //     //     ['宽度(m)', 'AD_Width'],
    //     //     ['数量', 'AD_Count'],
    //     //     ['重量(吨)', 'AD_Weight'],
    //     //     ['备注', 'AD_Memo']
    //     // ];

    //     return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
    
    // }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            $mn_one = $this->model->alias("m")->join(["materialgetnotice_jwl"=>"mgn"],"mgn.MN_Num = m.MN_Num")->where("mgn.MGN_ID",$num)->find();
            if($mn_one["Auditor"]) return json(["code"=>0,'msg'=>"已审核，不能进行删除！"]);
            $one = (new WjStoreInDetail())->where("DHID",$num)->find();
            if($one) return json(["code"=>0,'msg'=>"已存在入库，不能进行删除！"]);
            Db::startTrans();
            try {
                $this->detailModel->where("MGN_ID",$num)->delete();
                Db::commit();
                return json(["code"=>1,'msg'=>"删除成功"]);
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,'msg'=>$e->getMessage()]);
            }
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $one = $this->model->where("MN_ID",$ids)->find();
            if($one["Auditor"]) $this->error("已审核，不能进行删除！");
            $si_one = $this->detailModel->alias("d")->join(["wjstoreindetail"=>"ctd"],"ctd.DHID = d.MGN_ID")->where("d.MN_Num",$one["MN_Num"])->find();
            if($si_one) $this->error("已存在入库，不能进行删除！");
            $count = false;
            Db::startTrans();
            try {
                $count = $this->model->where("MN_ID",$ids)->delete();
                // foreach($list as $v){
                    // $task_detail_id[] = $v["AL_Num"];
                    // $v->update("");
                // }
                $count += $this->detailModel->where("MN_Num",$one["MN_Num"])->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function chooseVendor()
    {
        $imModel = new \app\admin\model\jichu\wl\Vendor;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $imModel
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["VC_Name"] = @$this->classList[$v["VC_Num"]];
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 读取分类树
     *
     * @internal
     */
    public function companytree()
    {
        $list = array_values($this->classTree);
        return json($list);
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["MGN_ID","MGN_ID","text","readonly","","hidden","MGN_ID"],
            ["IM_Num","IM_Num","text","readonly","","hidden","im_num"],
            ["pm_id","pm_id","text","readonly",0,"hidden","pm_id"],
            // ["way","way","text","readonly",0,"hidden","way"],
            ["材料名称","IR_Name","text","readonly","","","IR_Name"],
            ["材料规格","IR_Spec","text","readonly","","","IR_Spec"],
            ["材料类别","sortNum","text","readonly","","","sortNum"],
            // ["到货长度(m)","MGN_Length","text","","","","length"],
            // ["到货宽度(m)","MGN_Width","text","","","","width"],
            ["到货数量","MGN_Count","text","","","","count"],
            // ["比重","IM_PerWeight","text","readonly",0,"hidden","IM_PerWeight"],
            // ["实际到货重量(kg)","MGN_Weight","text","",0,"","weight"],
            // ["理论重量(kg)","mgn_formuleweight","text","readonly",0,"","weight"],
            ["到货单价(元/kg)","mgn_price","text","","",0,"price"],
            ["到货去向","MGN_Destination","text","","机物料仓库","","MGN_Destination"],
            // ["试验编号","mgn_testnum","text","","","","mgn_testnum"],
            // ["进货批次","mgn_pi","text","","","","mgn_pi"],
            // ["生产厂家","MGN_Maker","text","","","","MGN_Maker"],
            // ["堆放点","mgn_DuiFangDian","text","","","","mgn_DuiFangDian"],
            ["备注","MGN_Memo","text","","","","MGN_Memo"]
        ];
        return $list;
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        $admin = $this->admin["nickname"];
        $result = $this->model->where("MN_Num",$num)->update(['MN_Num' => $num,'Auditor'=>$admin,"AuditTime"=>date("Y-m-d H:i:s")]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"审核成功","content"=>"1"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        $one = $this->detailModel->alias("d")
            ->join(["wjstoreindetail"=>"wsid"],"wsid.DHID = d.MGN_ID")
            ->where("d.MN_Num",$num)->find();
        if($one) return json(["code"=>0,"msg"=>"已入库，弃审失败"]);
        $result = $this->model->where("MN_Num",$num)->update(['MN_Num' => $num,'Auditor'=>"","AuditTime"=>"0000-00-00 00:00:00"]);
        if ($result !== false) {
            return json(["code"=>1,"msg"=>"弃审成功","content"=>"0"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败"]);
        }
    }

    public function chooseMaterial($v_num='')
    {
        if(!$v_num) $this->error("请先选择供应商后，在选择到货信息");
        $iniWhere = [
            "V_Num" => ["=",$v_num]
        ];
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $buildSql = $this->model->alias("mn")
                ->join(["materialgetnotice_jwl"=>"mgn"],"mn.MN_Num=mgn.MN_Num")
                ->join(["wjinventoryrecord"=>"im"],"mgn.IM_Num = im.IR_Num")
                ->field("mn.MN_Num,mgn.MGN_ID AS DHID,im.IR_Num,im.sortNum,im.IR_Name,im.IR_Spec,im.IR_Unit,mgn.MGN_Count as RKDe_Count,round((mgn.mgn_price/(1+im.IR_TaxRate)),5) as RKDe_NotaxPrice,round((mgn.mgn_price/(1+im.IR_TaxRate)*MGN_Count),5) as RKDe_NotaxMoney,mgn.mgn_price as RKDe_Price,round((mgn.mgn_price*mgn.MGN_Count),5) as RKDe_Money,im.IR_TaxRate as RKDe_TaxRate,mn.Writer,mn.WriteTime")
                ->where("mn.Auditor","<>","")
                ->buildSql();

            $list = DB::Table($buildSql)->alias("a")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->assignconfig("v_num",$v_num);
        return $this->view->fetch();
    }

}
