<?php

namespace app\admin\controller\chain\machine;

use app\admin\model\chain\machine\WjInventoryRecord;
use app\admin\model\chain\machine\WjStore;
use app\admin\model\chain\machine\WjStoreIn;
use app\admin\model\chain\machine\WjStoreOut;
use app\admin\model\chain\machine\WjStoreOutDetail;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class WjStoreBalance extends Backend
{
    
    /**
     * WjStoreBalance模型对象
     * @var \app\admin\model\chain\machine\WjStoreBalance
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\machine\WjStoreBalance;
        list($this->classList,$this->classTree) = $this->wjStoreYj();
        $where = [
            "ParentNum" => ["=","01"],
            "WC_Name" => ["<>",""],
            "WC_Sort" => ["=","五金"]
        ];
        $warehouse_list = $this->wareclassList($where,1);
        $this->warehouse_list = $warehouse_list;
        $this->view->assign("warehouse_list",$warehouse_list);
        $this->assignconfig("warehouse_list",$warehouse_list);
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     /**
     * 读取分类树
     *
     * @internal
     */
    public function companytree($type = null)
    {
        $list = $this->classTree;
        return json(array_values($list));
    }

     /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            $this->treeSearch = $this->request->get("tree", '');
            if(!$this->treeSearch and empty($filter)){
                $result = array("total" => 0, "rows" => []);
                return json($result);
            }else{
                list($where, $sort, $order, $offset, $limit) = $this->buildparams();
                $list = $this->model->alias("m")
                    ->join(["wareclass"=>"wc"],"m.WC_Num = wc.WC_Num")
                    ->join(["wjinventoryrecord"=>"wir"],"m.IR_Num = wir.IR_Num")
                    ->join(["wjinventorysort"=>"wis"],"wir.sortNum = wis.IS_Num")
                    ->field("m.*,wc.WC_Name,wis.IS_Name,IR_Name,IR_Spec,IR_Unit");
                if($this->treeSearch){
                    $list = $list->where(function ($query) {
                        $query->where('BL_Num', '=', $this->treeSearch);
                    });
                }
                $list = $list->where($where)
                    ->order("m.WC_Num,wis.IS_Num,m.IR_Num asc")
                    ->select();
                $row = [];
                $total_list = [
                    "BL_LastCount" => 0,
                    "BL_LastMoney" => 0,
                    "BL_RKCount" => 0,
                    "BL_RKMoney" => 0,
                    "BL_CKCount" => 0,
                    "BL_CKMoney" => 0,
                    "BL_EndCount" => 0,
                    "BL_EndMoney" => 0
                ];
                foreach($list as $v){
                    foreach($total_list as $kk=>$vv){
                        $total_list[$kk] += $v[$kk];
                        $v[$kk] = round($v[$kk],2);
                    }
                    $v["BL_LastPrice"] = round($v["BL_LastPrice"],3);
                    $v["BL_RKPrice"] = round($v["BL_RKPrice"],3);
                    $v["BL_CKPrice"] = round($v["BL_CKPrice"],3);
                    $v["BL_EndPrice"] = round($v["BL_EndPrice"],3);
                    $row[] = $v->toArray();
                }
                $total_list["BL_EndCount"] = round($total_list["BL_LastCount"]+$total_list["BL_RKCount"]-$total_list["BL_CKCount"],2);
                $total_list["BL_EndMoney"] = round($total_list["BL_LastMoney"]+$total_list["BL_RKMoney"]-$total_list["BL_CKMoney"],2);
                foreach($total_list as $kk=>$vv){
                    $total_list[$kk] = round($vv,2);
                }
                $total_list["IS_Name"] = "合计";
                $row[] = $total_list;
                $result = array("total" => count($list), "rows" => $row);

                return json($result);
            }
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $start_time = "0000-00-00 00:00:00";
                $end_time = $params["BL_Date"]." 23:59:59";
                $last_one = $this->model->where("BL_Date",">=",$params["BL_Date"])->where("WC_Num",$params["WC_Num"])->order("BL_Date asc")->find();
                if($last_one) $this->error("无法生成");
                $next_one = $this->model->where("BL_Date","<",$params["BL_Date"])->where("WC_Num",$params["WC_Num"])->order("BL_Date desc")->find();
                if($next_one){
                    $start_time = date("Y-m-d 00:00:00",strtotime("+1 day",strtotime($next_one["BL_Date"])));
                    if($next_one["Assessor"]=="") $this->error("上个月结存未审核");
                }else $next_one = [];
                $one = $this->model->field("BL_Num")->order("BL_Num desc")->find();
                $month = date("Y");
                if($one){
                    $num = substr($one["BL_Num"],6);
                    $BL_Num = "YJ".$month.str_pad(++$num,4,0,STR_PAD_LEFT);
                }else $BL_Num = "YJ".$month.'0001';
                $nickname = $this->admin["nickname"];
                [$rows,$msg] = $this->saveEdit($BL_Num,$params,$nickname,$next_one,$start_time,$end_time);
                if($msg) $this->error($msg);
                $result = false;
                if(!empty($rows)){
                    Db::startTrans();
                    try {
                        $result = $this->model->allowField(true)->saveAll(array_values($rows));
                        
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                
                if ($result !== false) {
                    $this->success('成功！',null,$BL_Num);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->assignconfig("table_data",[]);
        $this->view->assign("row",$row);
        return $this->view->fetch();
    }

    public function edit($ids=null)
    {
        $row = $this->model->where("BL_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            if($row["Assessor"]) $this->error("该单已审核，无法修改");
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $start_time = "0000-00-00 00:00:00";
                $end_time = $params["BL_Date"]." 23:59:59";
                $next_one = $this->model->where("BL_Date","<",$params["BL_Date"])->where("WC_Num",$params["WC_Num"])->order("BL_Date desc")->find();
                if($next_one){
                    $start_time = date("Y-m-d 00:00:00",strtotime("+1 day",strtotime($next_one["BL_Date"])));
                    if($next_one["Assessor"]=="") $this->error("上个月结存未审核");
                }else $next_one = [];
                $nickname = $this->admin["nickname"];
                [$rows,$msg] = $this->saveEdit($ids,$params,$nickname,$next_one,$start_time,$end_time);
                if($msg) $this->error($msg);
                $result = false;
                if(!empty($rows)){
                    Db::startTrans();
                    try {
                        $this->model->where("BL_Num",$ids)->delete();
                        $result = $this->model->allowField(true)->saveAll(array_values($rows));
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $list = $this->model->alias("m")
            ->join(["wareclass"=>"wc"],"m.WC_Num = wc.WC_Num")
            ->join(["wjinventoryrecord"=>"wir"],"m.IR_Num = wir.IR_Num")
            ->join(["wjinventorysort"=>"wis"],"wir.sortNum = wis.IS_Num")
            ->field("m.*,wc.WC_Name,wis.IS_Name,IR_Name,IR_Spec,IR_Unit")
            ->where([
                "m.BL_Num" => ["=",$ids],
                "m.WC_Num" => ["=",$row["WC_Num"]]
            ])
            ->order("m.WC_Num,wis.IS_Num,m.IR_Num asc")
            ->select();

        $sum_list = ["BL_LastCount"=>0,"BL_LastMoney"=>0,"BL_RKCount"=>0,"BL_RKMoney"=>0,"BL_CKCount"=>0,"BL_CKMoney"=>0,"BL_EndCount"=>0,"BL_EndMoney"=>0];
        $end_list = [];
        foreach($list as $k=>$v){
            foreach($sum_list as $kk=>$vv){
                $sum_list[$kk] += $v[$kk];
                $v[$kk] = round($v[$kk],6);
            }
            foreach(["BL_LastPrice","BL_RKPrice","BL_CKPrice","BL_EndPrice"] as $vvv){
                $v[$vvv] = round($v[$vvv],6);
            }
            $end_list[] = $v->toArray();
        }
        $sum_list["BL_EndCount"] = $sum_list["BL_LastCount"]+$sum_list["BL_RKCount"]-$sum_list["BL_CKCount"];
        $sum_list["BL_EndMoney"] = $sum_list["BL_LastMoney"]+$sum_list["BL_RKMoney"]-$sum_list["BL_CKMoney"];
        foreach($sum_list as $kk=>$vv){
            $sum_list[$kk] = round($vv,2);
        }
        $end_list[] = ["IR_Name"=>"合计"]+$sum_list;
        $this->view->assign("row",$row);
        $this->assignconfig("table_data",$end_list);
        // $this->view->assign("list",$end_list);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function summary()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row");
            $params = $params?json_decode($params,true):[];
            if ($params) {
                $start_time = "0000-00-00 00:00:00";
                $end_time = $params["BL_Date"]." 23:59:59";
                $next_one = $this->model->where("BL_Date","<",$params["BL_Date"])->where("WC_Num",$params["WC_Num"])->order("BL_Date desc")->find();
                if($next_one){
                    $start_time = date("Y-m-d 00:00:00",strtotime("+1 day",strtotime($next_one["BL_Date"])));
                }
                $nickname = $this->admin["nickname"];
                $wj_num = (new WjInventoryRecord())->getMaterial();
                $WC_Name = $this->warehouse_list[$params["WC_Num"]];
                [$rows,$msg] = $this->saveEdit(0,$params,$nickname,$next_one,$start_time,$end_time);
                if($msg) $this->error($msg);
                foreach($rows as $k=>$v){
                    $rows[$k]["WC_Name"] = $WC_Name;
                    $wj_list = $wj_num[$v["IR_Num"]]??[];
                    $rows[$k] = array_merge($rows[$k],$wj_list);
                }
                return json(["code"=>1,"data"=>array_values($rows)]);
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return json(["code"=>0,"msg"=>"有误"]);
    }

    public function allDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);

        $row = $this->model->where("BL_Num",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"该月结信息已不存在，或者请稍后重试"]);
        if($row["Assessor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);
        $last_one = $this->model->where("BL_Date",">",$row["BL_Date"])->where("WC_Num",$row["WC_Num"])->order("BL_Date asc")->find();
        if($last_one) return json(["code"=>0,"msg"=>"删除失败"]);
        $result=0;
        Db::startTrans();
        try {
            $result += $this->model->where("BL_Num",$num)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result != false) {
            return json(["code"=>1,"msg"=>"删除成功"]);
        } else {
            return json(["code"=>0,"msg"=>"删除失败"]);
        }
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->model->where("BL_Num",$num)->find();
        if($row["Assessor"]) return json(["code"=>0,"msg"=>"已审核"]);
        $list = $this->model->where("BL_Num",$num)->select();
        
        $start_time = "0000-00-00 00:00:00";
        $end_time = date("Y-m-d",strtotime($row["BL_Date"]))." 23:59:59";
        $next_one = $this->model->where("BL_Date","<",$row["BL_Date"])->where("WC_Num",$row["WC_Num"])->order("BL_Date desc")->find();
        if($next_one) $start_time = date("Y-m-d 00:00:00",strtotime("+1 day",strtotime($next_one["BL_Date"])));

        $result = FALSE;
        $wjstore_model = new WjStore();
        $wjstoreout_model = new WjStoreOutDetail();
        $wj_num = (new WjInventoryRecord())->getMaterial();
        $out_list = $wjstoreout_model->alias("wsod")->join(["wjstoreout"=>"wso"],"wso.CK_ID = wsod.CK_ID")->field("CKDe_ID,IR_Num,CKDe_Count")->where([
            "CK_Date" => ["between time",[$start_time,$end_time]],
            "WC_Num" => ["=",$row["WC_Num"]]
        ])->select();
        if($list){
            Db::startTrans();
            try {
                $material_list = [];
                $result = $this->model->where("BL_Num",$num)->update(["Assessor"=>$this->admin["nickname"],"AssessDate"=>date("Y-m-d H:i:s")]);
                foreach($list as $v){
                    $wjstore_model->where([
                        "WC_Num"=> ["=",$v["WC_Num"]],
                        "IR_Num"=> ["=",$v["IR_Num"]]
                    ])->update([
                        "S_LastCount" => $v["BL_EndCount"],
                        "S_LastPrice" => $v["BL_EndPrice"],
                        "S_LastMoney" => $v["BL_EndMoney"],
                        "S_LastDate" => substr($v["BL_Date"],0,10)
                    ]);
                    $material_list[$v["IR_Num"]] = $v["BL_CKPrice"]?$v["BL_CKPrice"]:$v["BL_EndPrice"];
                }
                $update_list = [];
                foreach($out_list as $v){
                    if(isset($material_list[$v["IR_Num"]])){
                        $ckde_PriceNoTax = $material_list[$v["IR_Num"]];
                        $ckde_MoneyNoTax = $material_list[$v["IR_Num"]]*$v["CKDe_Count"];
                        $fax = $wj_num[$v["IR_Num"]]["IR_TaxRate"]??0;
                        $CKDe_Money = $ckde_MoneyNoTax*(1+$fax);
                        $CKDe_Price = $v["CKDe_Count"]?($CKDe_Money/$v["CKDe_Count"]):0;
                        $update_list[] = [
                            "CKDe_ID" => $v["CKDe_ID"],
                            "ckde_PriceNoTax" => $ckde_PriceNoTax,
                            "ckde_MoneyNoTax" => $ckde_MoneyNoTax,
                            "CKDe_Price" => $CKDe_Price,
                            "CKDe_Money" => $CKDe_Money
                        ];
                    }
                }
                // pri($update_list,1);
                $wjstoreout_model->allowField(true)->saveAll($update_list);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"弃审失败，请稍后重试"]);
        $row = $this->model->where("BL_Num",$num)->find();
        if(!$row["Assessor"]) return json(["code"=>0,"msg"=>"已弃审"]);
        $next_one = $this->model->where("BL_Date","<",$row["BL_Date"])->where("WC_Num",$row["WC_Num"])->order("BL_Date desc")->find();
        $list = [];
        if($next_one) $list = $this->model->where("BL_Num",$next_one["BL_Num"])->select();
        $result = FALSE;
        $wjstore_model = new WjStore();
        // if(!empty($list)){
            Db::startTrans();
            try {
                $result = $this->model->where("BL_Num",$num)->update(["Assessor"=>"","AssessDate"=>"0000-00-00 00:00:00"]);
                foreach($list as $v){
                    $wjstore_model->where([
                        "WC_Num"=> ["=",$v["WC_Num"]],
                        "IR_Num"=> ["=",$v["IR_Num"]]
                    ])->update([
                        "S_LastCount" => $v["BL_EndCount"],
                        "S_LastPrice" => $v["BL_EndPrice"],
                        "S_LastMoney" => $v["BL_EndMoney"],
                        "S_LastDate" => substr($v["BL_Date"],0,10)
                    ]);
                }
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        // }
        // pri($result,1);
        if ($result) {
            return json(["code"=>1,"msg"=>"弃审成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败！"]);
        }
    }

    protected function saveEdit($BL_Num,$params,$nickname,$next_one=[],$start_time,$end_time)
    {
        $base_row = [
            "WC_Num" => $params["WC_Num"],
            "BL_Num" => $BL_Num,
            "BL_Date" => $params["BL_Date"],
            "Writer" => $nickname,
            "BL_LastCount" => 0,
            "BL_LastPrice" => 0,
            "BL_LastMoney" => 0,
            "BL_RKCount" => 0,
            "BL_RKPrice" => 0,
            "BL_RKMoney" => 0,
            "BL_CKCount" => 0,
            "BL_CKPrice" => 0,
            "BL_CKMoney" => 0,
            "BL_EndCount" => 0,
            "BL_EndMoney" => 0,
            "BL_EndPrice" => 0,
        ];
        $ir_num_where = ["IR_Num"=>["=","50249"]];
        $rows = [];
        if(!empty($next_one)){
            $next_list = $this->model
                ->field("IR_Num,BL_EndCount as BL_LastCount,BL_EndPrice as BL_LastPrice,BL_EndMoney as BL_LastMoney,WC_Num")
                ->where("BL_Num",$next_one["BL_Num"])->order("IR_Num asc")->select();
            foreach($next_list as $v){
                $v["BL_LastCount"] = round($v["BL_LastCount"],3);
                $v["BL_LastPrice"] = round($v["BL_LastPrice"],6);
                // $v["BL_LastPrice"] = $v["BL_LastPrice"];
                $v["BL_LastMoney"] = round($v["BL_LastMoney"],3);
                $rows[$v["IR_Num"]] = array_merge($base_row,($v->toArray()));
            }
        }
        $in_list = (new WjStoreIn())->alias("w")
            ->join(["wjstoreindetail"=>"d"],"w.RK_ID = d.RK_ID")
            ->field("sum(RKDe_Count) as BL_RKCount,0 as BL_RKPrice,sum(RKDe_NotaxMoney) as BL_RKMoney,IR_Num,w.Assessor")
            ->where([
                "RK_Date" => ["between time",[$start_time,$end_time]],
                "WC_Num" => ["=",$params["WC_Num"]],
                // "Assessor" => ["<>",""]
            ])
            ->order("IR_Num asc")
            ->group("IR_Num")
            ->select();
        foreach($in_list as $v){
            if($v["Assessor"]==""){
                return [[],"入库存在未审核情况，请先审核该月入库单"];
            }
            unset($v["Assessor"]);
            $v["BL_RKCount"] = round($v["BL_RKCount"],3);
            $v["BL_RKPrice"] = $v["BL_RKCount"]?round($v["BL_RKMoney"]/$v["BL_RKCount"],6):0;
            $v["BL_RKMoney"] = round($v["BL_RKMoney"],3);
            if(isset($rows[$v["IR_Num"]])) $rows[$v["IR_Num"]] = array_merge($rows[$v["IR_Num"]],($v->toArray()));
            else $rows[$v["IR_Num"]] = array_merge($base_row,($v->toArray()));
        }
        // $price_list = (new WjStore())->getPrice(["WC_Num"=>$params["WC_Num"]]);
        $out_list = (new WjStoreOut())->alias("w")
            ->join(["wjstoreoutdetail"=>"d"],"w.CK_ID = d.CK_ID")
            // ->field("sum(CKDe_Count) as BL_CKCount,0 as BL_CKPrice,sum(CKDe_Money) as BL_CKMoney,IR_Num")
            ->field("sum(CKDe_Count) as BL_CKCount,0 as BL_CKPrice,0 as BL_CKMoney,IR_Num,w.Assessor")
            ->where([
                "CK_Date" => ["between time",[$start_time,$end_time]],
                "WC_Num" => ["=",$params["WC_Num"]],
                // "Assessor" => ["<>",""]
            ])
            ->order("IR_Num asc")
            ->group("IR_Num")
            ->select();
        foreach($out_list as $v){
            if($v["Assessor"]==""){
                return [[],"出库存在未审核情况，请先审核该月出库单"];
            }
            unset($v["Assessor"]);
            $v["BL_CKCount"] = round($v["BL_CKCount"],3);
            // $v["BL_CKPrice"] = $price_list[$v["IR_Num"]]??0;
            // $v["BL_CKMoney"] = round($v["BL_CKCount"]*$v["BL_CKPrice"],3);
            if(isset($rows[$v["IR_Num"]])) $rows[$v["IR_Num"]] = array_merge($rows[$v["IR_Num"]],($v->toArray()));
            else $rows[$v["IR_Num"]] = array_merge($base_row,($v->toArray()));
        }
        // $total_field = ["BL_LastCount","BL_LastPrice","BL_LastMoney","BL_RKCount","BL_RKPrice","BL_RKMoney","BL_RKCount"];
        foreach($rows as $k=>$v){
            // if($v["BL_RKCount"]==0 AND $v["BL_CKCount"]==0){
            //     $rows[$k]["BL_EndCount"] = $v["BL_EndCount"] = $v["BL_LastCount"];
            //     $rows[$k]["BL_EndMoney"] = $v["BL_EndMoney"] = $v["BL_LastPrice"];
            //     $rows[$k]["BL_EndPrice"] = $v["BL_EndPrice"] = $v["BL_LastMoney"];
            // }else{
            //     $BL_EndCount = $v["BL_LastCount"]+$v["BL_RKCount"]-$v["BL_CKCount"];
            //     $BL_CKPrice = ($v["BL_LastCount"]+$v["BL_RKCount"])==0?0:($v["BL_LastMoney"]+$v["BL_RKMoney"])/($v["BL_LastCount"]+$v["BL_RKCount"]);
            //     $BL_CKMoney = $v["BL_CKCount"]?$v["BL_CKCount"]*$BL_CKPrice:0;
            //     $BL_EndMoney = $v["BL_LastMoney"]+$v["BL_RKMoney"]-$v["BL_RKMoney"];
            //     $BL_EndPrice = $BL_EndCount==0?0:$BL_CKPrice;
            //     // $BL_CKPrice = $v["BL_CKCount"]?$BL_CKPrice:0;
            // }
            // $flag = 0;
            // foreach($total_field as $vv){
            //     $v[$vv] = round($v[$vv],2);
            //     if($v[$vv]!=0) $flag = 1;
            // }
            // if(!$flag){
            //     unset($rows[$k]);
            //     continue;
            // }
            $BL_EndCount = $v["BL_LastCount"]+$v["BL_RKCount"]-$v["BL_CKCount"];
            $BL_CKPrice = ($v["BL_LastCount"]+$v["BL_RKCount"])==0?0:($v["BL_LastMoney"]+$v["BL_RKMoney"])/($v["BL_LastCount"]+$v["BL_RKCount"]);
            $BL_CKMoney = $v["BL_CKCount"]?$v["BL_CKCount"]*$BL_CKPrice:0;
            $BL_EndMoney = $v["BL_LastMoney"]+$v["BL_RKMoney"]-$BL_CKMoney;
            $BL_EndPrice = $BL_EndCount==0?0:$BL_CKPrice;
            

            // $BL_EndMoney = $v["BL_LastMoney"]+$v["BL_RKMoney"]-$v["BL_CKMoney"];

            // $BL_EndMoney = $v["BL_LastMoney"]+$v["BL_RKMoney"]-$v["BL_CKMoney"];
            // $BL_EndPrice = $BL_EndCount?round($BL_EndMoney/$BL_EndCount,3):0;
            $rows[$k]["BL_EndCount"] = round($BL_EndCount,3);
            $rows[$k]["BL_EndMoney"] = round($BL_EndMoney,3);
            $rows[$k]["BL_EndPrice"] = round($BL_EndPrice,6);
            $rows[$k]["BL_CKPrice"] = round($BL_CKPrice,6);
            $rows[$k]["BL_CKMoney"] = round($BL_CKMoney,3);
        }
        return [$rows,""];
    }

    public function print($ids=null)
    {
        $row = $this->model->where("BL_Num",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        

        $list = $this->model->alias("m")
            ->join(["wareclass"=>"wc"],"m.WC_Num = wc.WC_Num")
            ->join(["wjinventoryrecord"=>"wir"],"m.IR_Num = wir.IR_Num")
            ->join(["wjinventorysort"=>"wis"],"wir.sortNum = wis.IS_Num")
            ->field("m.*,wc.WC_Name,wis.IS_Name,IR_Name,IR_Spec,IR_Unit")
            ->where([
                "m.BL_Num" => ["=",$ids],
                "m.WC_Num" => ["=",$row["WC_Num"]]
            ])
            ->order("m.WC_Num,wis.IS_Num,m.IR_Num asc")
            ->select();

        $sum_list = ["BL_LastCount"=>0,"BL_LastMoney"=>0,"BL_RKCount"=>0,"BL_RKMoney"=>0,"BL_CKCount"=>0,"BL_CKMoney"=>0,"BL_EndCount"=>0,"BL_EndMoney"=>0];
        $end_list = [];
        foreach($list as $k=>$v){
            foreach($sum_list as $kk=>$vv){
                $sum_list[$kk] += $v[$kk];
                $v[$kk] = round($v[$kk],3);
            }
            foreach(["BL_LastPrice","BL_RKPrice","BL_CKPrice","BL_EndPrice"] as $vvv){
                $v[$vvv] = round($v[$vvv],3);
            }
            $end_list[] = $v->toArray();
        }
        foreach($sum_list as $kk=>$vv){
            $sum_list[$kk] = round($vv,3);
        }
        $end_list[] = ["IR_Name"=>"合计"]+$sum_list;
        $row['PrintDate'] = date("Y-m-d");
        $row['BL_Date'] = date("Y-m-d",strtotime($row["BL_Date"]));
        $row['AssessDate'] = date("Y-m-d",strtotime($row["AssessDate"]));
        $row['WriteDate'] = date("Y-m-d",strtotime($row["WriteDate"])); 
        
        $this->assignconfig("row",$row);
        $this->assignconfig("list",$end_list);
        
        $this->assignconfig('ids',$ids);
        $this->assignconfig('wslist',$this->warehouse_list);
        return $this->view->fetch();
    }
}
