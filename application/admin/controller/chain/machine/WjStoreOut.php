<?php
namespace app\admin\controller\chain\machine;

use app\admin\model\chain\machine\WjInventoryRecord;
use app\admin\model\chain\machine\WjStore;
use app\admin\model\chain\machine\WjStoreBalance;
use app\admin\model\jichu\ch\WareClass;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 出库
 *
 * @icon fa fa-circle-o
 */
class WjStoreOut extends Backend
{
    
    /**
     * WjStoreOut模型对象
     * @var \app\admin\model\chain\machine\WjStoreOut
     */
    protected $model = null;
    protected $noNeedLogin = ["offerU"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\machine\WjStoreOut;
        $this->detailModel = new \app\admin\model\chain\machine\WjStoreOutDetail;
        $where = [
            "ParentNum" => ["=","01"],
            "WC_Name" => ["<>",""],
            "WC_Sort" => ["=","五金"]
        ];
        $warehouse_list = $this->wareclassList($where,1);
        $this->wareArr = $this->wareclassList($where,2);
        $this->warehouse_list = $warehouse_list;
        $this->admin = \think\Session::get('admin');
        $this->view->assign("warehouse_list",$this->warehouse_list);
        array_splice($warehouse_list,0,1);
        $this->assignconfig("warehouse_list",$warehouse_list);
        $this->type_list = ["正常出库"=>"正常出库","退货出库"=>"退货出库","调拨出库"=>"调拨出库","盘亏出库"=>"盘亏出库","委外出库"=>"委外出库","外售出库"=>"外售出库","借出"=>"借出","其他出库"=>"其他出库"];
        $this->view->assign("type_list",$this->type_list);
        $deptArr = [];
        $deptList = [""=>"请选择"];
        $deptModel = (new \app\admin\model\jichu\jg\Deptdetail())
            ->field("DD_Name,DD_Num")
            ->where(["Valid"=>1])
            ->order(["ParentNum"=>"ASC","DD_Num"=>"ASC"])
            ->select();
        foreach($deptModel as $v){
            $deptList[$v["DD_Name"]] = $v["DD_Name"];
            $deptArr[$v["DD_Name"]] = $v["DD_Num"];
        }
        $this->deptArr = $deptArr;
        $this->view->assign("deptList",$deptList);
        $this->assignconfig("deptList",$deptList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     /**
     * 查看
     */
    public function index()
    {
        [$sortList] = $this->wjInventClassList();
        $this->assignconfig("sortList",$sortList);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("wsi")
                    ->join(['wjstoreoutdetail'=>"wsid"],"wsid.CK_ID = wsi.CK_ID")
                    ->join(["wjinventoryrecord"=>"wir"],"wsid.IR_Num = wir.IR_Num")
                    ->field("wir.sortNum,wsi.CK_ID,CK_Num,CK_Date,wsid.IR_Num,CKDe_Count,ckde_PriceNoTax,ckde_MoneyNoTax,IR_Name,IR_Spec,trim(IR_Unit) as IR_Unit,wsi.CK_TakeDept")
                    ->where($where)
                    ->order($sort, $order)
                    ->order("CKDe_ID desc")
                    ->select();
            $data = [];
            $number = $price = $no_price = 0;
            foreach($list as $k=>$v){
                $data[$k] = $v->toArray();
                $data[$k]["CKDe_Count"] = round($v["CKDe_Count"],3);
                $data[$k]["ckde_PriceNoTax"] = round($v["ckde_PriceNoTax"],3);
                $data[$k]["ckde_MoneyNoTax"] = round($v["ckde_MoneyNoTax"],3);
                $number += $v["CKDe_Count"];
                $no_price += $v["ckde_MoneyNoTax"];
            }
            $data[] = ["IR_Name"=>"合计","CKDe_Count"=>round($number,3),"ckde_MoneyNoTax"=>round($no_price,2)];
            $result = array("total" => count($list), "rows" => $data);

            return json($result);
        }
        $defaultTime = date("Y-m-1 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        $row = [
            "writer" => $this->admin["nickname"],
            "DD_Name" => isset($this->admin["DD_Name"])?$this->admin["DD_Name"]:"",
            "time" => date("Y-m-d H:i:s")
        ];
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params["WC_Num"]) $this->error('仓库必选');
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $CK_Num = $params["CK_Num"];
                if($CK_Num){
                    $one = $this->model->where("CK_Num",$CK_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("Ymd");
                    $one = $this->model->field("CK_Num")->where("CK_Num","LIKE","CK".$month.'-J%')->order("CK_Num DESC")->find();
                    if($one){
                        $num = substr($one["CK_Num"],12);
                        $CK_Num = "CK".$month.'-J'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $CK_Num = "CK".$month.'-J001';
                }
                $params["CK_Num"] = $CK_Num;
                $params["Writer"] = $row["writer"];
                $msg = "";
                $sectSaveList = [];
                list($msg,$sectSaveList) = $this->saveEdit($paramsTable,$params["WC_Num"]);
                if($msg) $this->error($msg);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        
                        $result = $this->model::create($params);
                        foreach($sectSaveList as $k=>$v){
                            $sectSaveList[$k]["CK_ID"] = $result["CK_ID"];
                        }
                        $this->detailModel->allowField(true)->saveAll(array_values($sectSaveList));
                        
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                
                if ($result !== false) {
                    $this->success('成功！',null,$result["CK_ID"]);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        if ($this->request->isPost()) {
            if($row["Assessor"]) $this->error("该入库单已审核，无法修改");
            $params = $this->request->post("row/a");
            if(!$params["WC_Num"]) $this->error('仓库必选');
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $msg = "";
                $sectSaveList = [];
                list($msg,$sectSaveList) = $this->saveEdit($paramsTable,$params["WC_Num"],$ids);
                if($msg) $this->error($msg);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        $this->model->where("CK_ID",$ids)->update($params);
                        $result = $this->detailModel->allowField(true)->saveAll(array_values($sectSaveList));
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $list = $this->detailModel->alias("d")
            ->join(["wjinventoryrecord"=>"im"],"im.IR_Num = d.IR_Num","left")
            ->join(["wjinventorysort"=>"sort"],"im.sortNum = sort.IS_Num","left")
            ->join(["wjstore"=>"ws"],["ws.WC_Num = ".$row["WC_Num"],"d.IR_Num = ws.IR_Num"],"left")
            ->field("d.*,im.*,sort.IS_Name as sortNum,ifnull(ws.S_Count,0) as S_Count,ifnull(ws.S_Price,0) as S_Price,ifnull(ws.S_Money,0) as S_Money,'".$row["WC_Num"]."' as WC_Num")
            ->where("CK_ID",$ids)
            ->order("CKDe_ID")
            ->select();
        $count = $price = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $v["CKDe_Count"] = round($v["CKDe_Count"],3);
            $v["CKDe_Price"] = round($v["CKDe_Price"],3);
            $v["CKDe_Money"] = round($v["CKDe_Money"],3);
            $rows[$k] = $v->toArray();
            $count += $v["CKDe_Count"];
            $price += $v["CKDe_Price"];
        }
        $row["count"] = round($count,3);
        $row["price"] = round($price,3);
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("list",$rows);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function chooseMaterial($wc_num="")
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $this->treeSearch = $this->request->get("tree", '');
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $build = (new WjStore())->alias("ws")->join(["wjinventoryrecord"=>"wir"],"ws.IR_Num = wir.IR_Num");
            if($this->treeSearch){
                $build = $build->where(function ($query) {
                    $query->where('wir.sortNum', '=', $this->treeSearch);
                });
            }
            $list = $build
                ->field("ws.IR_Num,wir.IR_Name,wir.sortNum,wir.IR_Spec,wir.IR_Unit,ws.S_Count,ws.S_Price,ws.S_LastPrice,ws.S_Money,ws.WC_Num,IR_TaxRate")
                ->where("ws.WC_Num","=",$wc_num)
                ->where($where)
                ->order($sort, $order)
                ->paginate(100);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["sortNum"] = $this->classTree[$v["sortNum"]]["text"]??"";
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        $this->assignconfig("wc_num",$wc_num);
        return $this->view->fetch();
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            
            $in_detail = $this->model->alias("si")->join(["wjstoreoutdetail"=>"sid"],"sid.CK_ID = si.CK_ID")->where("sid.CKDe_ID",$num)->find();
            if(!$in_detail) return json(["code"=>0,"msg"=>"删除失败"]);
            if($in_detail["Assessor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);
            $count = $this->detailModel->where("CKDe_ID",$num)->delete();
            if($count) return json(["code"=>1,'msg'=>"删除成功"]);
            else return json(["code"=>0,"msg"=>"删除失败"]);
            
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function allDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);

        $row = $this->model->where("CK_ID",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"该条信息已不存在，或者请稍后重试"]);
        if($row["Assessor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);

        $result=0;
        Db::startTrans();
        try {
            $result += $this->model->where("CK_ID",$num)->delete();
            $result += $this->detailModel->where("CK_ID",$num)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result != false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->model->where("CK_ID",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"审核失败，该信息不存在，请稍后重试"]);
        if($row["Assessor"]) return json(["code"=>0,"msg"=>"已审核"]);
        $balance_model = new WjStoreBalance();
        $balance_one = $balance_model->where([
            "WC_Num" => ["=",$row["WC_Num"]],
            "BL_Date" => [">",$row["CK_Date"]]
        ])->find();
        if($balance_one) return json(["code"=>0,"msg"=>"该月月结已经审核，该入库单无法审核"]);
        $balance_no = $balance_model->where([
            "WC_Num" => ["=",$row["WC_Num"]],
            "BL_Date" => ["like", date("Y-m-",strtotime("-1 month",strtotime($row["CK_Date"])))."%"],
            "Assessor" => ["<>",""],
        ])->find();
        // if(!$balance_no) return json(["code"=>0,"msg"=>"上个月未月结或者月结未审核，该入库单无法审核"]);

        $list = $this->detailModel->where("CK_ID",$num)->select();
        $result = FALSE;
        $wjstore_model = new WjStore();
        if($list){
            $zl_list = $store_zl_list = $store_arr_add = $store_arr = [];
            foreach($list as $k=>$v){
                isset($zl_list[$v["IR_Num"]])?"":$zl_list[$v["IR_Num"]]=["CKDe_Count"=>0];
                $zl_list[$v["IR_Num"]]["CKDe_Count"] += $v["CKDe_Count"];
            }
            $store_one = $wjstore_model->where([
                "WC_Num" => ["=",$row["WC_Num"]],
                "IR_Num" => ["IN",array_keys($zl_list)]
            ])->order("S_LastDate desc")->group("IR_Num")->select();
            foreach($store_one as $k=>$v){
                $store_zl_list[$v["IR_Num"]] = $v->toArray();
            }
            foreach($zl_list as $k=>$v){
                if(isset($store_zl_list[$k]["S_ID"])){
                    $store_arr[$store_zl_list[$k]["S_ID"]] = $store_zl_list[$k];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"] -= $v["CKDe_Count"];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Price"] = $store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"]?round($store_arr[$store_zl_list[$k]["S_ID"]]["S_Money"]/$store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"],3):0;
                }else{
                    $store_arr_add[$k] = [
                        "WC_Num" => $row["WC_Num"],
                        "IR_Num" => $k,
                        "S_Count" => 0-$v["CKDe_Count"],
                        "S_Price" => 0,
                        "S_Money" => 0,
                    ];
                }
            }
            $store_arr = $store_arr + $store_arr_add;
            Db::startTrans();
            try {
                $result = $this->model->where("CK_ID",$num)->update(["Assessor"=>$this->admin["nickname"],"AssessDate"=>date("Y-m-d H:i:s")]);
                $wjstore_model->allowField(true)->saveAll($store_arr);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->model->where("CK_ID",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"审核失败，该信息不存在，请稍后重试"]);
        if(!$row["Assessor"]) return json(["code"=>0,"msg"=>"已弃审"]);
        $list = $this->detailModel->where("CK_ID",$num)->select();
        
        $balance_model = new WjStoreBalance();
        $balance_one = $balance_model->where([
            "WC_Num" => ["=",$row["WC_Num"]],
            "BL_Date" => [">=",$row["CK_Date"]]
        ])->find();
        if($balance_one) return json(["code"=>0,"msg"=>"该月月结已经审核，该入库单无法弃审"]);
        $result = FALSE;
        $wjstore_model = new WjStore();
        if($list){
            $zl_list = $store_zl_list = $store_arr_add = $store_arr = [];
            foreach($list as $k=>$v){
                isset($zl_list[$v["IR_Num"]])?"":$zl_list[$v["IR_Num"]]=["CKDe_Count"=>0];
                $zl_list[$v["IR_Num"]]["CKDe_Count"] += $v["CKDe_Count"];
            }
            $store_one = $wjstore_model->where([
                "WC_Num" => ["=",$row["WC_Num"]],
                "IR_Num" => ["IN",array_keys($zl_list)]
            ])->order("S_LastDate desc")->group("IR_Num")->select();
            foreach($store_one as $k=>$v){
                $store_zl_list[$v["IR_Num"]] = $v->toArray();
            }
            foreach($zl_list as $k=>$v){
                if(isset($store_zl_list[$k]["S_ID"])){
                    $store_arr[$store_zl_list[$k]["S_ID"]] = $store_zl_list[$k];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"] += $v["CKDe_Count"];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Price"] = $store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"]?round($store_arr[$store_zl_list[$k]["S_ID"]]["S_Money"]/$store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"],3):0;
                }else{
                    $store_arr_add[$k] = [
                        "WC_Num" => $row["WC_Num"],
                        "IR_Num" => $k,
                        "S_Count" => $v["CKDe_Count"],
                        "S_Price" => 0,
                        "S_Money" => 0,
                    ];
                }
            }
            $store_arr = $store_arr + $store_arr_add;
            Db::startTrans();
            try {
                $result = $this->model->where("CK_ID",$num)->update(["Assessor"=>"","AssessDate"=>"0000-00-00 00:00:00"]);
                $wjstore_model->allowField(true)->saveAll($store_arr);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result) {
            return json(["code"=>1,"msg"=>"弃审成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败！"]);
        }
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["ID","CKDe_ID","readonly","","hidden"],
            ["WC_Num","WC_Num","readonly","","hidden"],
            ["存货编码","IR_Num","readonly","",""],
            ["存货类别","sortNum","readonly","",""],
            ["存货名称","IR_Name","readonly","",""],
            ["规格","IR_Spec","readonly","",""],
            ["计量单位","IR_Unit","readonly","",""],
            ["领用数量","CKDe_Count","",1,""],
            ["含税单价","CKDe_Price","readonly",0,""],
            ["含税金额","CKDe_Money","readonly",0,""],
            ["不含税单价","ckde_PriceNoTax","readonly",0,""],
            ["不含税金额","ckde_MoneyNoTax","readonly",0,""],
            ["用途","CKDe_Use","","",""],
            ["库存数量","S_Count","readonly","",""],
            ["库存均价","S_Price","readonly","",""],
            ["库存金额","S_Money","readonly","",""]
        ];
        return $list;
    }

    protected function saveEdit($paramsTable=[], $wc_num = "", $rk_id=0)
    {
        $sectSaveList = [];$msg = "";
        foreach($paramsTable["CKDe_ID"] as $k=>$v){
            if($paramsTable["WC_Num"][$k] != $wc_num) continue;
            if(!$paramsTable["CKDe_Count"][$k]) $msg .="第".($k+1)."行缺少数量<br>";
            $sectSaveList[$k] = [
                "CK_ID" => $rk_id,
                "CKDe_ID" => $v,
                "IR_Num" => $paramsTable["IR_Num"][$k],
                "CKDe_Count" => $paramsTable["CKDe_Count"][$k],
                // "CKDe_Price" => $paramsTable["CKDe_Price"][$k],
                // "CKDe_Money" => $paramsTable["CKDe_Money"][$k],
                // "RKDe_TaxRate" => $paramsTable["RKDe_TaxRate"][$k],
                // "ckde_PriceNoTax" => $paramsTable["ckde_PriceNoTax"][$k],
                // "ckde_MoneyNoTax" => $paramsTable["ckde_MoneyNoTax"][$k],
                "CKDe_Use" => $paramsTable["CKDe_Use"][$k]
            ];
            if(!$v) unset($sectSaveList[$k]["CKDe_ID"]);
            if(!$rk_id) unset($sectSaveList[$k]["CK_ID"]);
        }
        return [$msg,$sectSaveList];
    }

    /**
     * 打印
     */
    public function print($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();

        $list = $this->detailModel->alias("d")
            ->join(["wjinventoryrecord"=>"im"],"im.IR_Num = d.IR_Num","left")
            ->join(["wjinventorysort"=>"sort"],"im.sortNum = sort.IS_Num","left")
            ->join(["wjstore"=>"ws"],["ws.WC_Num = ".$row["WC_Num"],"d.IR_Num = ws.IR_Num"],"left")
            ->field("d.*,im.*,sort.IS_Name as sortNum,ifnull(ws.S_Count,0) as S_Count,ifnull(ws.S_Price,0) as S_Price,ifnull(ws.S_Money,0) as S_Money,'".$row["WC_Num"]."' as WC_Num")
            ->where("CK_ID",$ids)
            ->order("CKDe_ID")
            ->select();
        $count = $price = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $v["CKDe_Count"] = round($v["CKDe_Count"],3);
            $v["CKDe_Price"] = round($v["CKDe_Price"],3);
            $v["CKDe_Money"] = round($v["CKDe_Money"],3);
            $rows[$k] = $v->toArray();
            $count += $v["CKDe_Count"];
            $price += $v["CKDe_Price"];
        }
        $row["count"] = round($count,3);
        $row["price"] = round($price,3);
        
        $this->assignconfig('row',$row);
        $this->assignconfig('list',$list);
        $this->assignconfig('wslist',$this->warehouse_list);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function offerU()
    {
        $id = $this->request->post("id");
        $row = $this->model->where("CK_ID",$id)->find();
        $res = [
            "code"=>0,
            "msg" => "有误",
            "data" => []
        ];
        if(!$row) return json($res);
        else if($row["Assessor"]==''){
            $res["msg"] = "请先审核";
            return json($res);
        }
        $cdepcode = $row["CK_TakeDept"];
        if(!$cdepcode){
            $res["msg"] = "不存在领用部门编号";
            return json($res);
        }
        $cwhcode = (new WareClass())->where("WC_Num","=",$row['WC_Num'])->value("WC_Name");
        if(!$cwhcode){
            $res["msg"] = "不存在仓库编号";
            return json($res);
        }
        $main = [
            "ccode" => $id,
            //采购类型编码
            "cdepname" => $cdepcode,
            //收发类别
            "crdcode" => "201",
            "cwhname" => $cwhcode,
            "ddate" => date("Y-m-d",strtotime($row["CK_Date"])),
            "cmaker" => $row['Writer'],
            "cmemo" => $row['CK_Memo'],
            "chandler" => $row['Assessor'],
            "dveridate" => date("Y-m-d",strtotime($row["AssessDate"])),
        ];
        $list = $this->detailModel
            ->where("CK_ID",$id)
            ->select();
        $details = [];
        foreach($list as $k=>$v){
            $details[] = [
                "cinvcode" => $v["IR_Num"],
                "iquantity" => 0,
                "cdefine22" => '',
                "cfree1" => '',
                "cfree2" => '',
                "cfree3" => '',
                "inum" => $v["CKDe_Count"]
            ];
        }
        $data = [
            "main" => $main,
            "details" => $details
        ];
        $url = "/PostRdrecord11/Add";
        $result = api_post_u($url,$data);
        if($result["code"]==0){
            $this->model->where("CK_ID",$id)->update(["offer"=>1]);
            $res["code"] = 1;
            $res["msg"] = "成功";
        }else $res["msg"] = $result["ErrorMsg"];
        return json($res);
    }
}
