<?php

namespace app\admin\controller\chain\machine;

use app\common\controller\Backend;

/**
 * 库存查询
 *
 * @icon fa fa-circle-o
 */
class WjStore extends Backend
{
    
    /**
     * WjStore模型对象
     * @var \app\admin\model\chain\machine\WjStore
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\machine\WjStore;
        $where = [
            "ParentNum" => ["=","01"],
            "WC_Name" => ["<>",""],
            "WC_Sort" => ["=","五金"]
        ];
        $warehouse_list = $this->wareclassList($where,1);
        $this->assignconfig("warehouse_list",$warehouse_list);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("m")
                ->join(["wareclass"=>"wc"],"m.WC_Num = wc.WC_Num")
                ->join(["wjinventoryrecord"=>"wir"],"m.IR_Num = wir.IR_Num")
                ->join(["wjinventorysort"=>"wis"],"wir.sortNum = wis.IS_Num")
                ->field("m.*,wc.WC_Name,wis.IS_Name,IR_Name,IR_Spec,IR_Unit,(m.S_Money/(1+wir.IR_TaxRate)) as S_No_Money")
                ->where($where)
                ->order("m.WC_Num,wis.IS_Num,m.IR_Num asc")
                ->select();
            $row = [];
            $count = $price = $noprice = $lastcount = $lastprice = 0;
            foreach($list as $v){
                $v["S_Count"] = round($v["S_Count"],3);
                $v["S_Money"] = round($v["S_Money"],3);
                $v["S_No_Money"] = round($v["S_No_Money"],2);
                $v["S_LastCount"] = round($v["S_LastCount"],3);
                $v["S_LastPrice"] = round($v["S_LastPrice"],3);
                $v["S_LastMoney"] = round($v["S_LastMoney"],3);
                $row[] = $v->toArray();
                $count += $v["S_Count"];
                $price += $v["S_Money"];
                $noprice += $v["S_No_Money"];
                $lastcount += $v["S_LastCount"];
                $lastprice += $v["S_LastMoney"];
            }
            $row[] = ["IS_Name"=>"合计","S_Count"=>round($count,3),"S_Money"=>round($price,3),"S_No_Money"=>round($noprice,2),"S_LastCount"=>round($lastcount,3),"S_LastMoney"=>round($lastprice,3)];
            $result = array("total" => count($list), "rows" => $row);

            return json($result);
        }
        return $this->view->fetch();
    }
}
