<?php

namespace app\admin\controller\chain\machine;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;


/**
 * 五金存货类别
 *
 * @icon fa fa-circle-o
 */
class WjInventorySort extends Backend
{
    
    /**
     * WjInventorySort模型对象
     * @var \app\admin\model\chain\machine\WjInventorySort
     */
    protected $model = null;
    protected $treeSearch;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\machine\WjInventorySort;
        $this->admin = \think\Session::get('admin');
        list($this->classList,$this->classTree) = $this->wjInventClassList();

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 读取分类树
     *
     * @internal
     */
    public function companytree($type = null)
    {
        $list = $this->classTree;
        return json(array_values($list));
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $this->treeSearch = $this->request->get("tree", '');
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $build = $this->model;
            if($this->treeSearch){
                $build = $build->where(function ($query) {
                    $query->where('IS_Num', 'like', $this->treeSearch.'%');

                });
            }
            $list = $build
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["ParentNum"] = $this->classTree[$v["ParentNum"]]["text"]??"";
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add($tree='')
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = $this->model->allowField(true)->save($params);
                if ($result != false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $class_list = [""=>"[全部]"]+$this->classList;
        $one = $this->model->where("ParentNum",$tree)->order("IS_Num DESC")->find();
        $default_is_num = $one?$tree.(str_pad((substr($one["IS_Num"],-2)+1),2,0,STR_PAD_LEFT)):$tree."01";
        $row = ["IS_Num"=>$default_is_num,"admin"=>$this->admin["nickname"],"time"=>date("Y-m-d H:i:s"),"tree"=>$tree];
        $this->view->assign("class_list",$class_list);
        $this->view->assign("row",$row);
        return $this->view->fetch();
    }

    public function stockTypeCode()
    {
        $num = $this->request->post("value");
        $one = $this->model->where("ParentNum",$num)->order("IS_Num DESC")->find();
        $default_is_num = $one?$num.(str_pad((substr($one["IS_Num"],-2)+1),2,0,STR_PAD_LEFT)):$num."01";
        return json(["code"=>1,"data"=>$default_is_num]);
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = $row->allowField(true)->save($params);
                if ($result != false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $class_list = [""=>"[全部]"]+$this->classList;
        $this->view->assign("class_list",$class_list);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
