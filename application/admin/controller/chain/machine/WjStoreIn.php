<?php

namespace app\admin\controller\chain\machine;

use app\admin\model\chain\machine\WjInventoryRecord;
use app\admin\model\chain\machine\WjInventorySort;
use app\admin\model\chain\machine\WjStore;
use app\admin\model\chain\machine\WjStoreBalance;
use app\admin\model\jichu\ch\WareClass;
use app\admin\model\jichu\wl\Vendor;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 入库
 *
 * @icon fa fa-circle-o
 */
class WjStoreIn extends Backend
{
    
    /**
     * WjStoreIn模型对象
     * @var \app\admin\model\chain\machine\WjStoreIn
     */
    protected $model = null;
    protected $noNeedLogin = ["offerU"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\machine\WjStoreIn;
        $this->detailModel = new \app\admin\model\chain\machine\WjStoreInDetail;
        $where = [
            "ParentNum" => ["=","01"],
            "WC_Name" => ["<>",""],
            "WC_Sort" => ["=","五金"]
        ];
        $warehouse_list = $this->wareclassList($where,1);
        $this->wareArr = $this->wareclassList($where,2);
        $this->warehouse_list = $warehouse_list;
        $this->sup_list = ["正常采购"=>"正常采购","缺料采购"=>"缺料采购","零星采购"=>"零星采购","外委加工"=>"外委加工"];
        $this->type_list = ["采购入库"=>"采购入库","退料入库"=>"退料入库","调拨入库"=>"调拨入库","盘盈入库"=>"盘盈入库","委内入库"=>"委内入库","冲红入库"=>"冲红入库","归还"=>"归还","其他入库"=>"其他入库"];
        $this->view->assign("sup_list",$this->sup_list);
        $this->view->assign("type_list",$this->type_list);
        $this->view->assign("warehouse_list",$this->warehouse_list);
        array_splice($warehouse_list,0,1);
        $this->assignconfig("warehouse_list",$warehouse_list);
        $this->admin = \think\Session::get('admin');
        list($this->classList,$this->classTree) = $this->wjInventClassList();
        // $this->assignconfig("classList",$this->classList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    

    /**
     * 查看
     */
    public function index()
    {
        $this->assignconfig("sortList",["03001"=>"油料类","03002"=>"劳保用品"]);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("wsi")
                    ->join(["vendor"=>"v"],"wsi.V_Num=v.V_Num","left")
                    ->join(['wjstoreindetail'=>"wsid"],"wsid.RK_ID = wsi.RK_ID")
                    ->join(["wjinventoryrecord"=>"wir"],"wsid.IR_Num = wir.IR_Num")
                    ->field("wir.sortNum,wsi.RK_ID,RK_Num,RK_Date,wsid.IR_Num,RKDe_Count,RKDe_Price,RKDe_Money,RKDe_NotaxPrice,RKDe_NotaxMoney,IR_Name,IR_Spec,trim(IR_Unit) as IR_Unit,v.V_Name")
                    ->where($where)
                    ->order($sort, $order)
                    ->order("RKDe_ID desc")
                    ->select();
            $data = [];
            $number = $price = $no_price = 0;
            foreach($list as $k=>$v){
                $data[$k] = $v->toArray();
                $data[$k]["RKDe_Count"] = round($v["RKDe_Count"],3);
                $data[$k]["RKDe_Price"] = round($v["RKDe_Price"],3);
                $data[$k]["RKDe_NotaxPrice"] = round($v["RKDe_NotaxPrice"],3);
                $data[$k]["RKDe_NotaxMoney"] = round($v["RKDe_NotaxMoney"],3);
                $number += $v["RKDe_Count"];
                $price += $v["RKDe_Money"];
                $no_price += $v["RKDe_NotaxMoney"];
            }
            $data[] = ["IR_Name"=>"合计","RKDe_Count"=>round($number,3),"RKDe_Money"=>round($price,3),"RKDe_NotaxMoney"=>round($no_price,3)];
            $result = array("total" => count($list), "rows" => $data);

            return json($result);
        }
        $defaultTime = date("Y-m-1 00:00:00").' - '.date("Y-m-d 23:59:59");
        $this->assignconfig("defaultTime",$defaultTime);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        $row = [
            "writer" => $this->admin["nickname"],
            "time" => date("Y-m-d H:i:s")
        ];
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");
            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $RK_Num = $params["RK_Num"];
                if($RK_Num){
                    $one = $this->model->where("RK_Num",$RK_Num)->find();
                    if($one) $this->error('单号不能重复，请重新填写或者自动生成');
                }else{
                    $month = date("Ymd");
                    $one = $this->model->field("RK_Num")->where("RK_Num","LIKE",'RK'.$month.'-J%')->order("RK_Num DESC")->find();
                    if($one){
                        $num = substr($one["RK_Num"],12);
                        $RK_Num = 'RK'.$month.'-J'.str_pad(++$num,3,0,STR_PAD_LEFT);
                    }else $RK_Num = 'RK'.$month.'-J001';
                }
                $params["RK_Num"] = $RK_Num;
                $params["Writer"] = $row["writer"];
                $msg = "";
                $sectSaveList = [];
                list($msg,$sectSaveList) = $this->saveEdit($paramsTable);
                if($msg) $this->error($msg);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        
                        $result = $this->model::create($params);
                        foreach($sectSaveList as $k=>$v){
                            $sectSaveList[$k]["RK_ID"] = $result["RK_ID"];
                        }
                        $this->detailModel->allowField(true)->saveAll(array_values($sectSaveList));
                        
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                
                if ($result !== false) {
                    $this->success('成功！',null,$result["RK_ID"]);
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->alias("rk")->join(["vendor"=>"v"],"rk.V_Num = v.V_Num","left")->where("RK_ID",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();
        if ($this->request->isPost()) {
            if($row["Assessor"]) $this->error("该入库单已审核，无法修改");
            $params = $this->request->post("row/a");
            $paramsTable = $this->request->post("table_row/a");

            // $ck_list = $this->model->alias("rk")
            //     ->join(["boltstoreoutsingle"=>"ck"],"ck.Out_Bsid = rk.Bsid")
            //     ->where("rk.In_Num",$row["In_Num"])
            //     ->find();
            // if($ck_list) $this->error("已存在出库，不能进行修改！");

            if ($params and $paramsTable) {
                $params = $this->preExcludeFields($params);
                $msg = "";
                $sectSaveList = [];
                list($msg,$sectSaveList) = $this->saveEdit($paramsTable,$ids);
                if($msg) $this->error($msg);
                $result = false;
                if(!empty($sectSaveList)){
                    Db::startTrans();
                    try {
                        $this->model->where("RK_ID",$ids)->update($params);
                        $result = $this->detailModel->allowField(true)->saveAll(array_values($sectSaveList));
                        Db::commit();
                    } catch (ValidateException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                }
                if ($result !== false) {
                    $this->success("保存成功！");
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }

        $list = $this->detailModel->alias("d")
            ->join(["wjinventoryrecord"=>"im"],"im.IR_Num = d.IR_Num","left")
            ->join(["wjinventorysort"=>"sort"],"im.sortNum = sort.IS_Num","left")
            ->field("d.*,im.*,sort.IS_Name as sortNum")
            ->where("RK_ID",$ids)
            ->order("RKDe_ID")
            ->select();
        $noprice = $count = $price = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $v["RKDe_Count"] = round($v["RKDe_Count"],3);
            $v["RKDe_Price"] = round($v["RKDe_Price"],3);
            $v["RKDe_Money"] = round($v["RKDe_Money"],3);
            $v["RKDe_TaxRate"] = round($v["RKDe_TaxRate"],3);
            $v["RKDe_NotaxPrice"] = round($v["RKDe_NotaxPrice"],3);
            $v["RKDe_NotaxMoney"] = round($v["RKDe_NotaxMoney"],3);
            $rows[$k] = $v->toArray();
            $noprice += $v["RKDe_NotaxMoney"];
            $count += $v["RKDe_Count"];
            $price += $v["RKDe_Money"];
        }
        $row["noprice"] = round($noprice,3);
        $row["count"] = round($count,3);
        $row["price"] = round($price,3);
        $tableField = $this->getTableField();
        $this->view->assign("row",$row);
        $this->view->assign("list",$rows);
        $this->view->assign("tableField",$tableField);
        $this->assignconfig('tableField',$tableField);
        $this->assignconfig('ids',$ids);
        return $this->view->fetch();
    }

    public function chooseMaterial($type=0)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $this->treeSearch = $this->request->get("tree", '');
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $build = new WjInventoryRecord();
            if($this->treeSearch){
                $build = $build->where(function ($query) {
                    $query->where('sortNum', '=', $this->treeSearch);

                });
            }
            $list = $build
                ->field("*,IR_TaxRate as RKDe_TaxRate")
                ->where($where)
                ->order($sort, $order)
                ->paginate(100);
            $rows = $list->items();
            foreach($rows as $k=>$v){
                $rows[$k]["sortNum"] = $this->classTree[$v["sortNum"]]["text"]??"";
            }
            $result = array("total" => $list->total(), "rows" => $rows);

            return json($result);
        }
        return $this->view->fetch();
    }

    public function delDetailContent()
    {
        $num = $this->request->post("num");
        if($num){
            
            $in_detail = $this->model->alias("si")->join(["wjstoreindetail"=>"sid"],"sid.RK_ID = si.RK_ID")->where("sid.RKDe_ID",$num)->find();
            if(!$in_detail) return json(["code"=>0,"msg"=>"删除失败"]);
            if($in_detail["Assessor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);
            $count = $this->detailModel->where("RKDe_ID",$num)->delete();
            if($count) return json(["code"=>1,'msg'=>"删除成功"]);
            else return json(["code"=>0,"msg"=>"删除失败"]);
            
        }
        return json(["code"=>0,'msg'=>"删除失败"]);
    }

    public function allDel()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"删除失败，请稍后重试"]);

        $row = $this->model->where("RK_ID",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"该条信息已不存在，或者请稍后重试"]);
        if($row["Assessor"]) return json(["code"=>0,"msg"=>"已审核，删除失败"]);

        $result=0;
        Db::startTrans();
        try {
            $result += $this->model->where("RK_ID",$num)->delete();
            $result += $this->detailModel->where("RK_ID",$num)->delete();
            Db::commit();
        } catch (ValidateException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (PDOException $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage()]);
        }
        if ($result != false) {
            $this->success("删除成功！");
        } else {
            $this->error("删除失败！");
        }
    }

    public function auditor()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->model->where("RK_ID",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"审核失败，该信息不存在，请稍后重试"]);
        if($row["Assessor"]) return json(["code"=>0,"msg"=>"已审核"]);
        $balance_model = new WjStoreBalance();
        $balance_one = $balance_model->where([
            "WC_Num" => ["=",$row["WC_Num"]],
            "BL_Date" => [">",$row["RK_Date"]]
        ])->find();
        if($balance_one) return json(["code"=>0,"msg"=>"该月月结已经审核，该入库单无法审核"]);
        $balance_no = $balance_model->where([
            "WC_Num" => ["=",$row["WC_Num"]],
            "BL_Date" => ["like", date("Y-m-",strtotime("-1 month",strtotime($row["RK_Date"])))."%"],
            "Assessor" => ["<>",""],
        ])->find();
        // if(!$balance_no) return json(["code"=>0,"msg"=>"上个月未月结或者月结未审核，该入库单无法审核"]);

        $list = $this->detailModel->where("RK_ID",$num)->select();
        $result = FALSE;
        $wjstore_model = new WjStore();
        if($list){
            $zl_list = $store_zl_list = $store_arr_add = $store_arr = [];
            foreach($list as $k=>$v){
                isset($zl_list[$v["IR_Num"]])?"":$zl_list[$v["IR_Num"]]=["RKDe_Count"=>0,"RKDe_Money"=>0];
                $zl_list[$v["IR_Num"]]["RKDe_Count"] += $v["RKDe_Count"];
                $zl_list[$v["IR_Num"]]["RKDe_Money"] += $v["RKDe_Money"];
            }
            $store_one = $wjstore_model->where([
                "WC_Num" => ["=",$row["WC_Num"]],
                "IR_Num" => ["IN",array_keys($zl_list)]
            ])->order("S_LastDate desc")->group("IR_Num")->select();
            foreach($store_one as $k=>$v){
                $store_zl_list[$v["IR_Num"]] = $v->toArray();
            }
            foreach($zl_list as $k=>$v){
                if(isset($store_zl_list[$k]["S_ID"])){
                    $store_arr[$store_zl_list[$k]["S_ID"]] = $store_zl_list[$k];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"] += $v["RKDe_Count"];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Money"] += $v["RKDe_Money"];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Price"] = $store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"]?round($store_arr[$store_zl_list[$k]["S_ID"]]["S_Money"]/$store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"],3):0;
                }else{
                    $store_arr_add[$k] = [
                        "WC_Num" => $row["WC_Num"],
                        "IR_Num" => $k,
                        "S_Count" => $v["RKDe_Count"],
                        "S_Price" => $v["RKDe_Count"]?round($v["RKDe_Money"]/$v["RKDe_Count"],3):0,
                        "S_Money" => $v["RKDe_Money"],
                    ];
                }
            }
            $store_arr = $store_arr + $store_arr_add;
            Db::startTrans();
            try {
                $result = $this->model->where("RK_ID",$num)->update(["Assessor"=>$this->admin["nickname"],"AssessDate"=>date("Y-m-d H:i:s")]);
                $wjstore_model->allowField(true)->saveAll($store_arr);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result) {
            return json(["code"=>1,"msg"=>"审核成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"审核失败！"]);
        }
    }

    public function giveUp()
    {
        $num = $this->request->post("num");
        if(!$num) return json(["code"=>0,"msg"=>"审核失败，请稍后重试"]);
        $row = $this->model->where("RK_ID",$num)->find();
        if(!$row) return json(["code"=>0,"msg"=>"审核失败，该信息不存在，请稍后重试"]);
        if(!$row["Assessor"]) return json(["code"=>0,"msg"=>"已弃审"]);
        $list = $this->detailModel->where("RK_ID",$num)->select();
        
        $balance_model = new WjStoreBalance();
        $balance_one = $balance_model->where([
            "WC_Num" => ["=",$row["WC_Num"]],
            "BL_Date" => [">=",$row["RK_Date"]]
        ])->find();
        if($balance_one) return json(["code"=>0,"msg"=>"该月月结已经审核，该入库单无法弃审"]);
        $result = FALSE;
        $wjstore_model = new WjStore();
        if($list){
            $zl_list = $store_zl_list = $store_arr_add = $store_arr = [];
            foreach($list as $k=>$v){
                isset($zl_list[$v["IR_Num"]])?"":$zl_list[$v["IR_Num"]]=["RKDe_Count"=>0,"RKDe_Money"=>0];
                $zl_list[$v["IR_Num"]]["RKDe_Count"] += $v["RKDe_Count"];
                $zl_list[$v["IR_Num"]]["RKDe_Money"] += $v["RKDe_Money"];
            }
            $store_one = $wjstore_model->where([
                "WC_Num" => ["=",$row["WC_Num"]],
                "IR_Num" => ["IN",array_keys($zl_list)]
            ])->order("S_LastDate desc")->group("IR_Num")->select();
            foreach($store_one as $k=>$v){
                $store_zl_list[$v["IR_Num"]] = $v->toArray();
            }
            foreach($zl_list as $k=>$v){
                if(isset($store_zl_list[$k]["S_ID"])){
                    $store_arr[$store_zl_list[$k]["S_ID"]] = $store_zl_list[$k];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"] -= $v["RKDe_Count"];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Money"] -= $v["RKDe_Money"];
                    $store_arr[$store_zl_list[$k]["S_ID"]]["S_Price"] = $store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"]?round($store_arr[$store_zl_list[$k]["S_ID"]]["S_Money"]/$store_arr[$store_zl_list[$k]["S_ID"]]["S_Count"],3):0;
                }else{
                    $store_arr_add[$k] = [
                        "WC_Num" => $row["WC_Num"],
                        "IR_Num" => $k,
                        "S_Count" => 0-$v["RKDe_Count"],
                        "S_Price" => $v["RKDe_Count"]?round($v["RKDe_Money"]/$v["RKDe_Count"],3):0,
                        "S_Money" => 0-$v["RKDe_Money"],
                    ];
                }
            }
            $store_arr = $store_arr + $store_arr_add;
            Db::startTrans();
            try {
                $result = $this->model->where("RK_ID",$num)->update(["Assessor"=>"","AssessDate"=>"0000-00-00 00:00:00"]);
                $wjstore_model->allowField(true)->saveAll($store_arr);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (PDOException $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            } catch (Exception $e) {
                Db::rollback();
                return json(["code"=>0,"msg"=>$e->getMessage()]);
            }
        }
        if ($result) {
            return json(["code"=>1,"msg"=>"弃审成功！"]);
        } else {
            return json(["code"=>0,"msg"=>"弃审失败！"]);
        }
    }

    //编辑table
    public function getTableField()
    {
        $list = [
            ["ID","RKDe_ID","readonly","","hidden"],
            ["存货编码","IR_Num","readonly","",""],
            ["存货类别","sortNum","readonly","",""],
            ["存货名称","IR_Name","readonly","",""],
            ["规格","IR_Spec","readonly","",""],
            ["计量单位","IR_Unit","readonly","",""],
            ["数量","RKDe_Count","",1,""],
            ["不含税单价","RKDe_NotaxPrice","",0,""],
            ["不含税金额","RKDe_NotaxMoney","",0,""],
            ["含税单价","RKDe_Price","",0,""],
            ["含税金额","RKDe_Money","readonly",0,""],
            ["税率","RKDe_TaxRate","readonly",0.17,""],
            ["其他说明","RKDe_Memo","","",""]
        ];
        return $list;
    }

    protected function saveEdit($paramsTable=[], $rk_id=0)
    {
        $sectSaveList = [];$msg = "";
        foreach($paramsTable["RKDe_ID"] as $k=>$v){
            if(!$paramsTable["RKDe_Count"][$k]) $msg .="第".($k+1)."行缺少数量<br>";
            $sectSaveList[$k] = [
                "RK_ID" => $rk_id,
                "RKDe_ID" => $v,
                "IR_Num" => $paramsTable["IR_Num"][$k],
                "RKDe_Count" => $paramsTable["RKDe_Count"][$k],
                "RKDe_Price" => $paramsTable["RKDe_Price"][$k],
                "RKDe_Money" => $paramsTable["RKDe_Money"][$k],
                "RKDe_TaxRate" => $paramsTable["RKDe_TaxRate"][$k],
                "RKDe_NotaxPrice" => $paramsTable["RKDe_NotaxPrice"][$k],
                "RKDe_NotaxMoney" => $paramsTable["RKDe_NotaxMoney"][$k],
                "RKDe_Memo" => $paramsTable["RKDe_Memo"][$k],
                "RKDe_RestCount" => $paramsTable["RKDe_Count"][$k],
            ];
            if(!$v) unset($sectSaveList[$k]["RKDe_ID"]);
            if(!$rk_id) unset($sectSaveList[$k]["RK_ID"]);
        }
        return [$msg,$sectSaveList];
    }

    /**
     * 打印
     */
    public function print($ids = null)
    {
        $row = $this->model->alias("rk")->join(["vendor"=>"v"],"rk.V_Num = v.V_Num")->where("RK_ID",$ids)->find();
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $row = $row->toArray();

        $list = $this->detailModel->alias("d")
            ->join(["wjinventoryrecord"=>"im"],"im.IR_Num = d.IR_Num","left")
            ->join(["wjinventorysort"=>"sort"],"im.sortNum = sort.IS_Num","left")
            ->field("d.*,im.*,sort.IS_Name as sortNum")
            ->where("RK_ID",$ids)
            ->order("RKDe_ID")
            ->select();
        $noprice = $count = $price = 0;
        $rows = [];
        foreach($list as $k=>$v){
            $v["RKDe_Count"] = round($v["RKDe_Count"],3);
            $v["RKDe_Price"] = round($v["RKDe_Price"],3);
            $v["RKDe_Money"] = round($v["RKDe_Money"],3);
            $v["RKDe_TaxRate"] = round($v["RKDe_TaxRate"],3);
            $v["RKDe_NotaxPrice"] = round($v["RKDe_NotaxPrice"],3);
            $v["RKDe_NotaxMoney"] = round($v["RKDe_NotaxMoney"],3);
            $rows[$k] = $v->toArray();
            $noprice += $v["RKDe_NotaxMoney"];
            $count += $v["RKDe_Count"];
            $price += $v["RKDe_Money"];
        }
        $row["noprice"] = round($noprice,3);
        $row["count"] = round($count,3);
        $row["price"] = round($price,3);

   
        $this->assignconfig('row',$row);
        $this->assignconfig('list',$list);
        $this->assignconfig('wslist',$this->warehouse_list);
        return $this->view->fetch();
    }

    public function offerU()
    {
        $id = $this->request->post("id");
        $row = $this->model->where("RK_ID",$id)->find();
        $res = [
            "code"=>0,
            "msg" => "有误",
            "data" => []
        ];
        if(!$row) return json($res);
        else if($row["Assessor"]==''){
            $res["msg"] = "请先审核";
            return json($res);
        }
        $cvencode = (new Vendor())->where("V_Num","=",$row['V_Num'])->value("V_Name");
        if(!$cvencode){
            $res["msg"] = "不存在供应商编号";
            return json($res);
        }
        $cwhcode = (new WareClass())->where("WC_Num","=",$row['WC_Num'])->value("WC_Name");
        if(!$cwhcode){
            $res["msg"] = "不存在仓库编号";
            return json($res);
        }
        $main = [
            "ccode" => $id,
            //采购类型编码
            "cptcode" => "1",
            //收发类别
            "crdcode" => '101',
            "cvenname" => $cvencode,
            "cwhname" => $cwhcode,
            "ddate" => date("Y-m-d",strtotime($row["RK_Date"])),
            "cmaker" => $row['Writer'],
            "cmemo" => $row['RK_Memo'],
            "chandler" => $row['RK_Memo'],
            "dveridate" => date("Y-m-d",strtotime($row["AssessDate"])),
        ];
        $list = $this->detailModel
            ->where("RK_ID",$id)
            ->select();
        $details = [];
        foreach($list as $k=>$v){
            $details[] = [
                "cinvcode" => $v["IR_Num"],
                "iquantity" => 0,
                "iunitcost" => $v["RKDe_NotaxPrice"],
                "cfree1" => '',
                "cfree2" => '',
                "cfree3" => '',
                "inum" => $v["RKDe_Count"]
            ];
        }
        $data = [
            "main" => $main,
            "details" => $details
        ];
        $url = "/PostRdrecord01/Add";
        $result = api_post_u($url,$data);
        if($result["code"]==0){
            $this->model->where("RK_ID",$id)->update(["offer"=>1]);
            $res["code"] = 1;
            $res["msg"] = "成功";
        }else $res["msg"] = $result["ErrorMsg"];
        return json($res);
    }
}
