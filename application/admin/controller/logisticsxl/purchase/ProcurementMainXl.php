<?php

namespace app\admin\controller\logisticsxl\purchase;

use app\admin\model\logisticsxl\material\MaterialGetNoticeXl;
use app\admin\model\logisticsxl\material\ApplyDetailsXl;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;
use jianyan\excel\Excel;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;

/**
 * 合同管理
 *
 * @icon fa fa-circle-o
 */
class ProcurementMainXl extends Backend
{
    
    /**
     * ProcurementMainXl模型对象
     * @var \app\admin\model\logisticsxl\purchase\ProcurementMainXl
     */
    protected $model = null,$admin='',$limberList = [],$wayList = [0=>"数量",1=>"重量"],$bzList = ["人民币"=>"人民币"],$procurementType = [0=>"原材料",1=>"机物料",2=>"紧固件",3=>"线缆"];
    protected $noNeedLogin = ["addMaterial","selectProcurement","selectProcurementList"];
    protected $relatedModel = null,$detailModel = null,$viewModel=null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\logisticsxl\purchase\ProcurementMainXl;
        $this->detailModel = new \app\admin\model\logisticsxl\purchase\ProcurementDetailXl;
        // $this->viewModel = new \app\admin\model\chain\purchase\DhProcurementView;
        $this->admin = \think\Session::get('admin');
        // $this->assignconfig("tableField",$tableField);
        //合同类型 币种 部门
        $purchaseList = $this->purchaseList(1);
        [$deptList] = $this->deptType(1);
        // $this->measurementList = $this->measurementList();
        $assign = [
            "tableField" => $this->getTableField(),
            "purchaseList" => $purchaseList,
            "deptList" => $deptList,
            "bzList" => $this->bzList,
            "wayList" => $this->wayList,
            "nickname" => $this->admin["username"],
            "procurement_type" => $this->procurementType,
            "vendorList" => $this->vendorNumList()
        ];
        $this->view->assign($assign);
        foreach($assign as $k=>$v){
            $this->assignconfig($k,$v);
        }
        [$limberList] = $this->getLimber();
        $this->limberList = $limberList;

    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     public function selectProcurement($v_num='',$type=0)
     {
         $iniWhere = [];
         if($v_num) $iniWhere["unit"] = ["=",$v_num];
         //type后续使用
         //设置过滤方法
         $this->request->filter(['strip_tags', 'trim']);
         if ($this->request->isAjax()) {
             //如果发送的来源是Selectpage，则转发到Selectpage
             if ($this->request->request('keyField')) {
                 return $this->selectpage();
             }
             list($where, $sort, $order, $offset, $limit) = $this->buildparams();
             $list = $this->model
                 ->where($where)
                 ->where($iniWhere)
                 ->order($sort, $order)
                 ->paginate($limit);
 
             $result = array("total" => $list->total(), "rows" => $list->items());
             return json($result);
         }
         $this->assignconfig("v_num",$v_num);
         $this->assignconfig("type",$type);
         return $this->view->fetch();
     }
 
     public function selectProcurementList($v_num='')
     {
         $iniWhere = [];
         if($v_num) $iniWhere["supplier"] = ["=",$v_num];
         // $iniWhere["procurement_type"] = ["=",$type];
         //type后续使用
         //设置过滤方法
         $this->request->filter(['strip_tags', 'trim']);
         if ($this->request->isAjax()) {
             //如果发送的来源是Selectpage，则转发到Selectpage
             if ($this->request->request('keyField')) {
                 return $this->selectpage();
             }
             list($where, $sort, $order, $offset, $limit) = $this->buildparams();
             $buildSql = $this->model->alias("pm")
                 ->join(["procurement_detail_xl"=>"pd"],"pm.pm_num=pd.pm_num")
                 ->join(["inventorymaterial"=>"im"],"pd.im_num=im.im_num")
                 ->field("`pm`.`pm_num` AS `pm_num`,
                 `pm`.`name` AS `name`,
                 `pm`.`type` AS `type`,
                 `pm`.`unit` AS `supplier`,
                 `pm`.`header` AS `header`,
                 `pm`.`sign_date` AS `sign_date`,
                 `pm`.`currency` AS `currency`,
                 `pm`.`rate` AS `rate`,
                 `pm`.`department` AS `department`,
                 `pm`.`salesman` AS `salesman`,
                 `pm`.`writer` AS `writer`,
                 `pm`.`writer_time` AS `writer_time`,
                 `pm`.`auditor` AS `auditor`,
                 `pm`.`auditor_time` AS `auditor_time`,
                 `pm`.`update_time` AS `update_time`,
                 `pm`.`procurement_type` AS `procurement_type`,
                 `pd`.`id` AS `pm_id`,
                 `pd`.`im_num` AS `im_num`,
                 `pd`.`AD_ID` AS `AD_ID`,
                 `pd`.`l_name` AS `l_name`,
                 `pd`.`length` AS `length`,
                 `pd`.`width` AS `width`,
                 `pd`.`unit` AS `unit`,
                 `pd`.`count` AS `count`,
                 `pd`.`weight` AS `weight`,
                 `pd`.`way` AS `way`,
                 `pd`.`amount` AS `amount`,
                 `pd`.`price` AS `price`,
                 `im`.`IM_Spec` AS `im_spec`,
                 `im`.`IM_PerWeight` AS `IM_PerWeight`,
                 `im`.`IM_Class` AS `im_class` ")
                 ->where("pm.auditor","<>","")
                 ->buildSql();
             $list = DB::TABLE($buildSql)->alias("a")
                 ->where($iniWhere)
                 ->order($sort,$order)
                 ->paginate($limit);
             $result = array("total" => $list->total(), "rows" => $list->items());
             return json($result);
         }
         $this->assignconfig("v_num",$v_num);
         // $this->assignconfig("type",$type);
         return $this->view->fetch();
     }
 
     /**
      * 添加
      */
     public function add()
     {
         if ($this->request->isPost()) {
             $params = $this->request->post("row/a");
             if(!$params) $this->error("主信息不能为空");
             $table = $this->request->post("table/a");
             if(!$table) $this->error("合同详情不能为空");
             // pri($params,$table,1);
             //pm_num HT202304-0001
             if(!$params["pm_num"]){
                 $year = "HTY".date("Ym")."-";
                 $lastOne = $this->model->where("pm_num","LIKE",$year."%")->order("pm_num desc")->value("pm_num");
                 if($lastOne) $lastOne = str_pad((substr($lastOne,-4)+1),4,0,STR_PAD_LEFT);
                 else $lastOne = "0001";
                 $params["pm_num"] = $year.$lastOne;
             }else{
                 $findOne = $this->model->where("pm_num","=",$params["pm_num"])->find();
                 if($findOne) $this->error("该编号已存在，请稍后再试");
             }
             $params["writer"] = $this->admin["nickname"];
             $params["writer_time"] = date("Y-m-d H:i:s");
             $saveTable = [];
             foreach($table["id"] as $k=>$v){
                 $way = $table["way"][$k];
                 // $count = $way?($table["weight"][$k]?$table["weight"][$k]:0):($table["count"][$k]?$table["count"][$k]:0);
                 // $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                 // $price = $count?round($amount/$count,5):0;
                 $saveTable[$k] = [
                     "id" => $v,
                     "pm_num" => $params["pm_num"],
                     "AD_ID" => $table["AD_ID"][$k],
                     "im_num" => $table["im_num"][$k],
                     "l_name" => $table["l_name"][$k],
                     "length" => $table["length"][$k]?$table["length"][$k]*1000:0,
                     "width" => $table["width"][$k]?$table["width"][$k]*1000:0,
                     "unit" => $table["unit"][$k],
                     "count" => $table["count"][$k]?$table["count"][$k]:0,
                     "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                     "way" => $way,
                     "price" => $table["price"][$k]?$table["price"][$k]:0,
                     "amount" => $table["amount"][$k]?$table["amount"][$k]:0,
                     "memo" => $table["memo"][$k],
                 ];
                 if(!$v) unset($saveTable[$k]["id"]);
             }
             $params["count"] = array_sum(array_column($saveTable,'count'));
             $params["weight"] = array_sum(array_column($saveTable,'weight'));
             $params["amount"] = array_sum(array_column($saveTable,'amount'));
             $result = false;
             Db::startTrans();
             try {
                 $result = $this->model->allowField(true)->save($params);
                 // if(!empty($qgList)) $qgResult = $this->relatedModel->allowField(true)->saveAll($qgList);
                 $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                 Db::commit();
             } catch (ValidateException $e) {
                 Db::rollback();
                 $this->error($e->getMessage());
             } catch (PDOException $e) {
                 Db::rollback();
                 $this->error($e->getMessage());
             } catch (Exception $e) {
                 Db::rollback();
                 $this->error($e->getMessage());
             }
             if ($result !== false) {
                 $this->success();
             } else {
                 $this->error(__('No rows were inserted'));
             }
         }
         // $this->assignconfig("init_data",[]);
         return $this->view->fetch();
     }
 
     /**
      * 编辑
      */
     public function edit($ids = null)
     {
         $row = $this->model->get($ids);
         if (!$row) {
             $this->error(__('No Results were found'));
         }
         $flag = 1;
         if($row["auditor"]=="") $flag = 0;
         $adminIds = $this->getDataLimitAdminIds();
         if (is_array($adminIds)) {
             if (!in_array($row[$this->dataLimitField], $adminIds)) {
                 $this->error(__('You have no permission'));
             }
         }
         $detailList = $this->detailModel->alias("d")
             ->join(["inventorymaterial"=>"im"],"d.im_num=im.IM_Num")
             ->field("d.id,d.im_num,im.IM_Class,im.IM_Spec,d.l_name,d.length,d.width,d.unit,d.count,d.weight,d.way,d.amount,d.price,d.memo,d.AD_ID")
             ->where("d.pm_num",$ids)
             ->select();
         $detailList = $detailList?collection($detailList)->toArray():[];
         $detailIdList = array_column($detailList,'id');
         // $detailIdList = $this->detailModel->where("pm_num",$ids)->column("id");
         $tbodyField = $this->_getTbodyField($detailList,1);
         if ($this->request->isPost()) {
             $params = $this->request->post("row/a");
             if(!$params) $this->error("主信息不能为空");
             $table = $this->request->post("table/a");
             if(!$table) $this->error("合同详情不能为空");
             $saveTable = [];
             foreach($table["id"] as $k=>$v){
                 $way = $table["way"][$k];
                 // $count = $way?($table["weight"][$k]?$table["weight"][$k]:0):($table["count"][$k]?$table["count"][$k]:0);
                 // $amount = $table["amount"][$k]?$table["amount"][$k]:0;
                 // $price = $count?round($amount/$count,5):0;
                 $saveTable[$k] = [
                     "id" => $v,
                     "pm_num" => $ids,
                     "AD_ID" => $table["AD_ID"][$k],
                     "im_num" => $table["im_num"][$k],
                     "l_name" => $table["l_name"][$k],
                     "length" => $table["length"][$k]?$table["length"][$k]*1000:0,
                     "width" => $table["width"][$k]?$table["width"][$k]*1000:0,
                     "unit" => $table["unit"][$k],
                     "count" => $table["count"][$k]?$table["count"][$k]:0,
                     "weight" => $table["weight"][$k]?$table["weight"][$k]:0,
                     "way" => $way,
                     "amount" => $table["amount"][$k]?$table["amount"][$k]:0,
                     "price" => $table["price"][$k]?$table["price"][$k]:0,
                     "memo" => $table["memo"][$k],
                 ];
                 if(!$v) unset($saveTable[$k]["id"]);
                 else if(in_array($v,$detailIdList)){
                     $wz = array_search($v,$detailIdList);
                     unset($detailIdList[$wz]);
                 }
             }
             $params["count"] = array_sum(array_column($saveTable,'count'));
             $params["weight"] = array_sum(array_column($saveTable,'weight'));
             $params["amount"] = array_sum(array_column($saveTable,'amount'));
             $result = false;
             Db::startTrans();
             try {
                 $result = $this->model->allowField(true)->save($params,["pm_num"=>$ids]);
                 // $this->relatedModel->where("pm_num",$ids)->delete();
                 // if(!empty($qgList)) $qgResult = $this->relatedModel->allowField(true)->saveAll($qgList);
                 if(!empty($detailIdList)) $this->detailModel->where("id","in",$detailIdList)->delete();
                 $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                 Db::commit();
             } catch (ValidateException $e) {
                 Db::rollback();
                 $this->error($e->getMessage());
             } catch (PDOException $e) {
                 Db::rollback();
                 $this->error($e->getMessage());
             } catch (Exception $e) {
                 Db::rollback();
                 $this->error($e->getMessage());
             }
             if ($result !== false) {
                 $this->success();
             } else {
                 $this->error(__('No rows were inserted'));
             }
         }
         $this->view->assign("tbodyField", $tbodyField);
         $this->view->assign("row", $row);
         // $this->assignconfig("init_data",$init_data);
         $this->assignconfig("ids",$ids);
         $this->view->assign("flag",$flag);
         $this->assignconfig("flag",$flag);
         return $this->view->fetch();
     }
 
     public function addMaterial($type=0)
     {
         $params = $this->request->post("data");
         $params = json_decode($params,true);
         // $tableField = $this->getTableField();
         $field = $this->_getTbodyField($params);
         return json(["code"=>1,"data"=>$field]);
     }
 
     protected function _getTbodyField($params=[],$type=0)
     {
         $field = "";
         foreach($params as $k=>$v){
             $field .= '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                     . '<td hidden=""><input type="text" class="small_input" readonly="" name="table[id][]" value="'.($type?$v["id"]:0).'"></td>'
                     . '<td><input type="text" class="small_input" readonly="" name="table[AD_ID][]" value="'.($type?$v["AD_ID"]:$v["AD_ID"]).'"></td>'
                     . '<td><input type="text" class="small_input" readonly="" name="table[im_num][]" value="'.($type?$v["im_num"]:$v["IM_Num"]).'"></td>'
                     . '<td><input type="text" class="small_input" readonly="" name="table[IM_Class][]" value="'.($type?$v["IM_Class"]:$v["IM_Class"]).'"></td>'
                     . '<td><input type="text" class="small_input" readonly="" name="table[IM_Spec][]" value="'.($type?$v["IM_Spec"]:$v["IM_Spec"]).'"></td>'
                     . '<td><input type="text" class="small_input" list="typelist" name="table[l_name][]" value="'.($type?$v["l_name"]:$v['L_Name']).'" placeholder="请选择"><datalist id="typelist">';
             foreach($this->limberList as $vvv){
                 $field .= '<option value="'.$vvv.'">'.$vvv.'</option>';
             }
             $field .= '</datalist></td>'
                     . '<td><input type="number" class="small_input" name="table[length][]" value="'.round((($type?$v["length"]/1000:$v["AD_Length"])),3).'"></td>'
                     . '<td><input type="number" class="small_input" name="table[width][]" value="'.round((($type?$v["width"]/1000:$v["AD_Width"])),3).'"></td>'
                     . '<td>';
             $field .= build_select('table[unit][]',$this->measurementList(),($type?$v["unit"]:$v["IM_Measurement"]),['class'=>'form-control selectpicker',"id"=>"c-unit","data-rule"=>"required"]);
             $field .= '</td>'
                     . '<td><input type="number" class="small_input" name="table[count][]" value="'.round(($type?$v["count"]:$v["sy_count"]),3).'"></td>'
                     . '<td><input type="number" class="small_input" name="table[weight][]" value="'.round(($type?$v["weight"]:$v["sy_weight"]),3).'"></td><td>'
                     . build_select('table[way][]',$this->wayList,($type?$v["way"]:1),['class'=>'form-control selectpicker',"id"=>"c-way","data-rule"=>"required"])
                     . '</td><td><input type="number" class="small_input" name="table[price][]" value="'.round(($type?$v["price"]:''),5).'"></td>'
                     . '<td><input type="number" class="small_input" name="table[amount][]" value="'.round(($type?$v["amount"]:''),5).'"></td>'
                     . '<td><input type="text" class="small_input" name="table[memo][]" value="'.($type?$v["memo"]:'').'"></td></tr>';
         }
         return $field;
     }
 
     public function download($pm_num='')
     {
         $row = $this->model->get($pm_num);
         if (!$row) {
             $this->error(__('No Results were found'));
         }
         $title = "合同".$pm_num;
         $list = $this->detailModel->alias("pd")
             ->join(["inventorymaterial"=>"im"],"pd.IM_Num = im.IM_Num")
             ->field("
             pd.pm_num,pd.id,pd.AD_ID,pd.im_num,im.IM_Class,im.IM_Spec,pd.L_Name,(pd.length/1000) as length,(pd.width/1000) as width,pd.unit,pd.count,pd.weight,pd.way,pd.price,pd.memo,
             case im.IM_Class when '钢板' then 0 when '角钢' then 1 else 2 end clsort,
             cast((case 
             when im.IM_Class='角钢' then 
             SUBSTR(REPLACE(im.IM_Spec,'∠',''),1,locate('*',REPLACE(im.IM_Spec,'∠',''))-1)*1000+
             SUBSTR(REPLACE(im.IM_Spec,'∠',''),locate('*',REPLACE(im.IM_Spec,'∠',''))+1,2)*1
             when (im.IM_Class='钢板' or im.IM_Class='法兰') then 
             REPLACE(im.IM_Spec,'-','')
             else 0
             end) as UNSIGNED) bh")
             ->where("pd.pm_num",$pm_num)
             ->order("clsort,pd.L_Name,bh,pd.length ASC")
             ->select();
         foreach($list as &$v){
             $v["length"] = round($v['length'],3);
             $v["width"] = round($v['width'],3);
             $v["count"] = round($v['count'],3);
             $v["weight"] = round($v['weight'],3);
             $v["price"] = round($v['price'],5);
             $v["price"] = $v["price"]?$v["price"]:'';
             $v["way"] = $this->wayList[$v["way"]];
         }
         $header = [
             ['合同号', 'pm_num'],
             ['合同ID', 'id'],
             ['请购ID', 'AD_ID'],
             ['材料编号', 'im_num'],
             ['材料名称', 'IM_Class'],
             ['规格', 'IM_Spec'],
             ['材质', 'L_Name'],
             ['长度(m)', 'length'],
             ['宽度(m)', 'width'],
             ['单位', 'unit'],
             ['数量', 'count'],
             ['重量(kg)', 'weight'],
             ['单价计算方式', 'way'],
             ['单价', 'price'],
             ['备注', 'memo']
         ];
 
         return Excel::exportData($list, $header, $title .'-清单-'. date('Ymd'));
     }
 
     /**
      * 导入
      */
     public function import()
     {
         $file = $this->request->request('file');
         if (!$file) {
             $this->error(__('Parameter %s can not be empty', 'file'));
         }
         $filePath = ROOT_PATH . DS . 'public' . DS . $file;
         if (!is_file($filePath)) {
             $this->error(__('No results were found'));
         }
         //实例化reader
         $ext = pathinfo($filePath, PATHINFO_EXTENSION);
         if (!in_array($ext, ['csv', 'xls', 'xlsx'])) {
             $this->error(__('Unknown data format'));
         }
         if ($ext === 'csv') {
             $file = fopen($filePath, 'r');
             $filePath = tempnam(sys_get_temp_dir(), 'import_csv');
             $fp = fopen($filePath, "w");
             $n = 0;
             while ($line = fgets($file)) {
                 $line = rtrim($line, "\n\r\0");
                 $encoding = mb_detect_encoding($line, ['utf-8', 'gbk', 'latin1', 'big5']);
                 if ($encoding != 'utf-8') {
                     $line = mb_convert_encoding($line, 'utf-8', $encoding);
                 }
                 if ($n == 0 || preg_match('/^".*"$/', $line)) {
                     fwrite($fp, $line . "\n");
                 } else {
                     fwrite($fp, '"' . str_replace(['"', ','], ['""', '","'], $line) . "\"\n");
                 }
                 $n++;
             }
             fclose($file) || fclose($fp);
 
             $reader = new Csv();
         } elseif ($ext === 'xls') {
             $reader = new Xls();
         } else {
             $reader = new Xlsx();
         }
 
         //导入文件首行类型,默认是注释,如果需要使用字段名称请使用name
         // $importHeadType = isset($this->importHeadType) ? $this->importHeadType : 'comment';
 
         // $table = $this->model->getQuery()->getTable();
         // $database = \think\Config::get('database.database');
         $fieldArr = [
             "合同号" => "pm_num",
             "合同ID" => "id",
             "数量" => "count",
             "重量(kg)" => "weight",
             "单价计算方式" => "way",
             "单价" => "price",
             "备注" => "memo"
         ];
         // $list = db()->query("SELECT COLUMN_NAME,COLUMN_COMMENT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ? AND TABLE_SCHEMA = ?", [$table, $database]);
         // foreach ($list as $k => $v) {
         //     if ($importHeadType == 'comment') {
         //         $fieldArr[$v['COLUMN_COMMENT']] = $v['COLUMN_NAME'];
         //     } else {
         //         $fieldArr[$v['COLUMN_NAME']] = $v['COLUMN_NAME'];
         //     }
         // }
         //加载文件
         $insert = [];
         try {
             if (!$PHPExcel = $reader->load($filePath)) {
                 $this->error(__('Unknown data format'));
             }
             $currentSheet = $PHPExcel->getSheet(0);  //读取文件中的第一个工作表
             $allColumn = $currentSheet->getHighestDataColumn(); //取得最大的列号
             $allRow = $currentSheet->getHighestRow(); //取得一共有多少行
             $maxColumnNumber = Coordinate::columnIndexFromString($allColumn);
             $fields = [];
             for ($currentRow = 1; $currentRow <= 1; $currentRow++) {
                 for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                     $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                     $fields[] = $val;
                 }
             }
 
             for ($currentRow = 2; $currentRow <= $allRow; $currentRow++) {
                 $values = [];
                 for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                     $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                     $values[] = is_null($val) ? '' : $val;
                 }
                 $row = [];
                 $temp = array_combine($fields, $values);
                 foreach ($temp as $k => $v) {
                     if (isset($fieldArr[$k]) && $k !== '') {
                         $row[$fieldArr[$k]] = $v;
                     }
                 }
                 if ($row) {
                     $insert[] = $row;
                 }
             }
         } catch (Exception $exception) {
             $this->error($exception->getMessage());
         }
         if (!$insert) {
             $this->error(__('No rows were updated'));
         }
         try {
             $key = [];
             foreach($insert as $k=>$v){
                 $key[$v["id"]] = $v["pm_num"];
                 if($v["way"]=="重量") $insert[$k]["amount"] = $v["weight"]*$v["price"];
                 else if($v["way"]=="数量") $insert[$k]["amount"] = $v["count"]*$v["price"];
                 else throw new Exception(($k+1)."行单价计算方式有误，请重新填写后上传");
                 unset($insert[$k]["way"]);
             }
             $getExistAuditor = $this->model->alias("pm")
                 ->join(["procurement_detail_xl"=>"pd"],"pm.pm_num=pd.pm_num")
                 ->where("pd.id","IN",array_keys($key))
                 ->where("pm.auditor","<>","")
                 ->column("pd.id");
             if(!empty($getExistAuditor)) throw new Exception("合同ID为".(implode(",",$getExistAuditor))."已审核无法修改子信息");
             $result = $this->detailModel->allowField(true)->saveAll($insert);
 
             $sum_list = $this->model->alias("pm")
                 ->join(["procurement_detail_xl"=>"pd"],"pm.pm_num=pd.pm_num")
                 ->where("pm.pm_num","IN",$key)
                 ->group("pm.pm_num")
                 ->column("pm.pm_num,sum(pd.count) as count,sum(pd.weight) as weight,sum(pd.amount) as amount");
             $this->model->allowField(true)->saveAll($sum_list);
         } catch (PDOException $exception) {
             $msg = $exception->getMessage();
             // if (preg_match("/.+Integrity constraint violation: 1062 Duplicate entry '(.+)' for key '(.+)'/is", $msg, $matches)) {
             //     $msg = "导入失败，包含【{$matches[1]}】的记录已存在";
             // };
             $this->error($msg);
         } catch (Exception $e) {
             $this->error($e->getMessage());
         }
 
         $this->success();
     }
 
     /**
      * 审核 
      * 核对采购数量是否超过请购数量
      * 核对是否存在无材料编号和无金额的情况
      */
     public function checkAuditor()
     {
         $ids = $this->request->post("ids");
         $row  = $this->model->get($ids);
         if(!$row) $this->error("不存在该合同编号，审核失败");
         $list = $this->detailModel->where("pm_num",$ids)->select();
         $list = $list?collection($list)->toArray():[];
         if(empty($list)) $this->error("不存在合同明细，请先添加合同明细后再进行审核操作");
         foreach($list as $k=>$v){
             if(!$v["im_num"] or round($v["amount"],5)==0) $this->error("存在缺少材料编号或者没有金额的，请检查");
         }
         $applyIdList = array_column($list,'AD_ID');
         $msgList = $this->compact($ids,$applyIdList)[0];
         if(!empty($msgList)) $this->success("请购ID为".implode($msgList)."的采购数量或者重量已超过请购需要量;");
         else $this->success();
     }
 
     /**
      * 请购和采购比较
      * type 0 超过
      * type 1 大于等于
      */
     protected function compact($ids,$applyIdList,$type=0)
     {
         // 请购数量
         $appNews = (new ApplyDetailsXl())->where("AD_ID","IN",$applyIdList)->column("AD_ID,AD_Count AS count,AD_Weight as weight");
         // 采购数量
         $prodetailNews = $this->model->alias("pm")
             ->join(["procurement_detail"=>"pd"],"pm.pm_num=pd.pm_num")
             ->where("pd.AD_ID","IN",$applyIdList)
             ->where(function ($query) use ($ids) {
                 $query->where('pm.auditor',"<>","")->whereor('pm.pm_num', $ids);
             })
             ->group("AD_ID")->column("pd.AD_ID,sum(pd.count) as count,sum(pd.weight) as weight");
         $msgList = [];
         foreach($prodetailNews as $k=>$v){
             $needCount = isset($appNews[$k])?round($appNews[$k]["count"],5):0;
             $needWeight = isset($appNews[$k])?round($appNews[$k]["weight"],5):0;
             $v["count"] = round($v["count"],5);
             $v["weight"] = round($v["weight"],5);
             if($type){
                 if($v["count"]>=$needCount or $v["weight"]>=$needWeight) $msgList[$k] = $k;
             }else{
                 if($v["count"]>$needCount or $v["weight"]>$needWeight) $msgList[$k] = $k;
             }
             
         }
         return [$msgList,$prodetailNews];
     }
 
     public function checkGive()
     {
         $ids = $this->request->post("ids");
         $row  = $this->model->get($ids);
         if(!$row) $this->error("不存在该合同编号，审核失败");
         $list = $this->detailModel->where("pm_num",$ids)->select();
         $list = $list?collection($list)->toArray():[];
         if(empty($list)) $this->error("不存在合同明细，请先添加合同明细后再进行审核操作");
         $applyIdList = array_column($list,'id');
         $one = (new MaterialGetNoticeXl())->where("pm_id","in",$applyIdList)->find();
         if($one) $this->error("已存在部分到货，无法弃审");
         else $this->success();
     }
 
     /**
      * 删除
      * 需要核对修改 如果已到货 不可删除
      */
     public function del($ids = "")
     {
         if (!$this->request->isPost()) {
             $this->error(__("Invalid parameters"));
         }
         $ids = $ids ? $ids : $this->request->post("ids");
         if ($ids) {
             $pk = $this->model->getPk();
             $adminIds = $this->getDataLimitAdminIds();
             if (is_array($adminIds))
              {
                 $this->model->where($this->dataLimitField, 'in', $adminIds);
             }
             $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->select();
             $newIdList = [];
             $count = 0;
             Db::startTrans();
             try {
                 foreach ($list as $k => $v) {
                     $count += $v->delete();
                     $newIdList[$v[$pk]] = $v[$pk];
                 }
                 $this->detailModel->where($pk,"IN",$newIdList)->delete();
                 // $this->relatedModel->where($pk,"IN",$newIdList)->delete();
                 Db::commit();
             } catch (PDOException $e) {
                 Db::rollback();
                 $this->error($e->getMessage());
             } catch (Exception $e) {
                 Db::rollback();
                 $this->error($e->getMessage());
             }
             if ($count) {
                 $this->success();
             } else {
                 $this->error(__('No rows were deleted'));
             }
         }
         $this->error(__('Parameter %s can not be empty', 'ids'));
     }
 
     /**
      * 审核
      * 审核通过同步修改请购已购信息
      * 重量和数量 有大于等于的 即已购入
      */
     public function auditor()
     {
         $ids = $this->request->post("ids");
         $row  = $this->model->get($ids);
         if(!$row) $this->error("不存在该合同编号，审核失败");
         $list = $this->detailModel->where("pm_num",$ids)->select();
         $list = $list?collection($list)->toArray():[];
         $applyIdList = array_column($list,'AD_ID');
         [$msgList,$prodetailNews] = $this->compact($ids,$applyIdList,1);
         $saveDetail = [];
         foreach($prodetailNews as $k=>$v){
             $saveDetail[$k] = [
                 "AD_ID" => $k,
                 "AD_BuyCount" => $v["count"],
                 "AD_BuyWeight" => $v["weight"],
                 "AD_BuyDate" => $row["writer_time"],
                 "ad_cp_check" => 0
             ];
             if(isset($msgList[$k])) $saveDetail[$k]["ad_cp_check"] = 1;
         }
         Db::startTrans();
         try {
             (new ApplyDetailsXl())->allowField(true)->saveAll($saveDetail);
             $result = $this->model->where("pm_num",$ids)->update([
                 "auditor" => $this->admin["username"],
                 "auditor_time" => date("Y-m-d H:i:s")
             ]);
             Db::commit();
         } catch (PDOException $e) {
             Db::rollback();
             $this->error($e->getMessage());
         } catch (Exception $e) {
             Db::rollback();
             $this->error($e->getMessage());
         }
         
         if($result) $this->success("审核成功");
         else $this->error("审核失败，请稍后再试");
     }
 
     /**
      * 弃审
      * 
      */
     public function giveUp($ids = "")
     {
         $ids = $this->request->post("ids");
         $row  = $this->model->get($ids);
         if(!$row) $this->error("不存在该合同编号，审核失败");
         $list = $this->detailModel->where("pm_num",$ids)->select();
         $list = $list?collection($list)->toArray():[];
         $applyIdList = array_column($list,'AD_ID');
         [$msgList,$prodetailNews] = $this->compact($ids,$applyIdList,1);
         $saveDetail = [];
         foreach($prodetailNews as $k=>$v){
             $saveDetail[$k] = [
                 "AD_ID" => $k,
                 "AD_BuyCount" => $v["count"],
                 "AD_BuyWeight" => $v["weight"],
                 "AD_BuyDate" => $row["writer_time"],
                 "ad_cp_check" => 0
             ];
             if(isset($msgList[$k])) $saveDetail[$k]["ad_cp_check"] = 1;
         }
         Db::startTrans();
         try {
             (new ApplyDetailsXl())->allowField(true)->saveAll($saveDetail);
             $result = $this->model->where("pm_num",$ids)->update([
                 "auditor" => '',
                 "auditor_time" => "0000-00-00 00:00:00"
             ]);
             Db::commit();
         } catch (PDOException $e) {
             Db::rollback();
             $this->error($e->getMessage());
         } catch (Exception $e) {
             Db::rollback();
             $this->error($e->getMessage());
         }
         if($result) $this->success("弃审成功");
         else $this->error("弃审失败，请稍后再试");
     }
     
     //编辑table
     public function getTableField()
     {
         $list = [
             ["id","id","text","","readonly",0,"hidden"],
             ["请购ID","AD_ID","text","","readonly","",""],
             ["材料编号","im_num","text","","readonly","",""],
             ["材料名称","IM_Class","text","","readonly","",""],
             ["规格","IM_Spec","text","","readonly","",""],
             ["材质","l_name","text","","","",""],
             ["长度(m)","length","number","","","",""],
             ["宽度(m)","width","number","","","",""],
             ["单位","unit","select","","","",""],
             ["数量","count","number","","","",""],
             // ["比重","IM_PerWeight","text","","readonly",0,"hidden"],
             ["重量","weight","number","","","",""],
             ["单价计算","way","select","","","",""],
             ["单价","price","number","","","",""],
             ["金额","amount","number","","","",""],
             ["备注","memo","text","","","",""]
         ];
         return $list;
     }
}
