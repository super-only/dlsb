<?php

namespace app\admin\controller\logisticsxl\purchase;

use app\admin\model\logisticsxl\purchase\InvoiceMainXl;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 付款申请单
 *
 * @icon fa fa-circle-o
 */
class PaymentAppMainXl extends Backend
{
    
    /**
     * PaymentAppMainXl模型对象
     * @var \app\admin\model\logisticsxl\purchase\PaymentAppMainXl
     */
    protected $model = null,$detailModel = null, $payModel = null, $viewModel = null, $admin='',$limberList = [],$bzList = ["人民币"=>"人民币"],$businessType = [0=>"普通采购",1=>"盘点"];
    protected $noNeedLogin = ["selectStorein","addMaterial"];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\logisticsxl\purchase\PaymentAppMainXl;
        $this->detailModel = new \app\admin\model\logisticsxl\purchase\PaymentAppDetailXl;
        $this->payModel = new \app\admin\model\logisticsxl\purchase\PayListXl();
        // $this->model = new \app\admin\model\logistics\invoice\PayStatusView;
        $this->admin = \think\Session::get('admin');
        $statusList = [
            "未支付" => "未支付",
            "部分支付" => "部分支付",
            "已支付" => "已支付"
        ];
        [$deptList] = $this->deptType(1);
        $assign = [
            // "tableField" => $this->getTableField(),
            "bzList" => $this->bzList,
            "businessType" => $this->businessType,
            "deptList" => $deptList,
            "nickname" => $this->admin["username"],
            "vendorList" => $this->vendorNumList(),
            "balanceMode" => $this->_getBalanceModel(),
            "statusList" => $statusList
        ];
        $this->view->assign($assign);
        foreach($assign as $k=>$v){
            $this->assignconfig($k,$v);
        }

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
     /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            // $table = $this->request->post("table/a");
            // if(!$table) $this->error("发票详情不能为空");
            // $exList = [0=>"Y",1=>"W",2=>"J",3=>"X"];
            // $ex = $exList[$params["type"]];
            //pm_num HT202304-0001
            $year = "FK".date("Ym")."-";
            $lastOne = $this->model->where("pay_app_num","LIKE",$year."%")->order("pay_app_num desc")->value("pay_app_num");
            if($lastOne) $lastOne = str_pad((substr($lastOne,-4)+1),4,0,STR_PAD_LEFT);
            else $lastOne = "0001";
            $params["pay_app_num"] = $year.$lastOne;
            if(!$params["supplier"]) $this->error("供应商不能为空");
            // $params["rate"] = $params["rate"]?$params["rate"]:1;
            $params["writer"] = $this->admin["username"];
            $params["writer_time"] = date("Y-m-d H:i:s");
            // $saveTable = [];
            // foreach($table["id"] as $k=>$v){
            //     if(!$table["amount"][$k]) $this->error("金额不能为0");
            //     $saveTable[$k] = [
            //         "id" => $v,
            //         "pay_app_num" => $params["pay_app_num"],
            //         "department" => $params["department"],
            //         "salesman" => $params["salesman"],
            //         "amount" => $table["amount"][$k],
            //         "pm_num" => $table["pm_num"][$k],
            //         "invoice_num" => $table["invoice_num"][$k],
            //         "memo" => $table["memo"][$k]
            //     ];
            //     if(!$v) unset($saveTable[$k]["id"]);
            // }
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params);
                // $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        $this->assignconfig("init_data",[]);
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $flag = 1;
        if($row["auditor"]=="") $flag = 0;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        // $detailList = $this->detailModel
            // ->where("pay_app_num",$ids)
            // ->select();
        // $detailList = $detailList?collection($detailList)->toArray():[];
        // $detailIdList = [];
        // foreach($detailList as $k=>$v){
            // $detailIdList[$v["id"]] = $v["id"];
        // }
        // [$tbodyField,$tableSidList] = $this->_getTbodyField($detailList,$this->getTableField());
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if(!$params) $this->error("主信息不能为空");
            if(!$params["pay_time"]) unset($params["pay_time"]);
            // $table = $this->request->post("table/a");
            // if(!$table) $this->error("付款申请详情不能为空");
            // $saveTable = [];
            // foreach($table["id"] as $k=>$v){
            //     if(!$table["amount"][$k]) $this->error("金额不能为0");
            //     $saveTable[$k] = [
            //         "id" => $v,
            //         "pay_app_num" => $ids,
            //         "department" => $params["department"],
            //         "salesman" => $params["salesman"],
            //         "amount" => $table["amount"][$k],
            //         "pm_num" => $table["pm_num"][$k],
            //         "invoice_num" => $table["invoice_num"][$k],
            //         "memo" => $table["memo"][$k]
            //     ];
            //     if(!$v) unset($saveTable[$k]["id"]);
            //     else if(in_array($v,$detailIdList)!=-1){
            //         $wz = in_array($v,$detailIdList);
            //         unset($detailIdList[$wz]);
            //     }
            // }

            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->allowField(true)->save($params,["pay_app_num"=>$ids]);
                // if(!empty($detailIdList)) $this->detailModel->where("id","in",$detailIdList)->delete();
                // $detailResult = $this->detailModel->allowField(true)->saveAll($saveTable);
                Db::commit();
            } catch (ValidateException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success();
            } else {
                $this->error(__('No rows were inserted'));
            }
        }
        // $this->assignconfig("tableSidList",$tableSidList);
        // $this->view->assign("tbodyField", $tbodyField);
        $this->view->assign("row", $row);
        // $this->assignconfig("init_data",$init_data);
        $this->assignconfig("ids",$ids);
        $this->view->assign("flag",$flag);
        $this->assignconfig("flag",$flag);
        return $this->view->fetch();
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->select();
            $newIdList = [];
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                    $newIdList[$v[$pk]] = $v[$pk];
                }
                $this->detailModel->where($pk,"IN",$newIdList)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
    
    /**
     * 审核
     */
    public function auditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk,"in",$ids)->where("auditor","=","")->where("pay_amount","<>",0)->order("writer_time asc")->column("pay_app_num,date,type,supplier,department,salesman,amount,pay_amount,method,pay_time");
            // pri($list,1);
            if(!$list) $this->error("没有需要审核的付款信息");
            // $list = $list?collection($list)->toArray():[];
            $payAppNumList = [];
            $groupList = $supplierGroup = [];
            foreach($list as $k=>$v){
                if(!$v["type"]) $groupList[$v["supplier"]][$v["pay_app_num"]] = $v["pay_amount"];
                else $payAppNumList[$v["pay_app_num"]] = $v["pay_app_num"];
            }
            $invoiceModel = new InvoiceMainXl();
            $supplierList = $invoiceModel
                ->where("supplier","IN",array_keys($groupList))
                ->where("auditor","<>","")
                ->where("status","<>",2)
                ->order("fp_date asc")->select();
            foreach($supplierList as $k=>$v){
                $supplierGroup[$v["supplier"]][$v["invoice_num"]] = [
                    "invoice_num" => $v["invoice_num"],
                    //初始需支付金额
                    "ini_amount" => $v["amount"]-$v["pay_amount"],
                    //扣减后仍需要支付金额
                    // "update_amount" => $v["amount"]-$v["pay_amount"],
                    //初始已支付金额
                    // "ini_pay_amount" => $v["pay_amount"],
                    //支付后的支付金额
                    "pay_amount" => $v["pay_amount"],
                    "status" => $v["status"]
                ];
            }
            // pri($groupList,$supplierGroup,1);
            $invoiceList = $detailList = [];
            foreach($groupList as $k=>$v){
                foreach($v as $kk=>$vv){
                    $iniSumAmount = array_sum(array_column($supplierGroup[$k],'ini_amount'));
                    // 判断支付金额是否大于需支付金额 大于不审核了
                    if($vv>$iniSumAmount) continue;
                    else{
                        $payAppNumList[$kk] = $kk;
                        $syAmount = $vv;
                        foreach($supplierGroup[$k] as $sk=>$sv){
                            if($syAmount<=0) break;
                            if($sv["ini_amount"]<=0) continue;
                            // 已有金额＜支付金额
                            if($syAmount<$sv["ini_amount"]){
                                $supplierGroup[$k][$sk]["ini_amount"] = $sv["ini_amount"]-$vv;
                                $supplierGroup[$k][$sk]["pay_amount"] = $sv["pay_amount"]+$vv;
                                $supplierGroup[$k][$sk]["status"] = 1;
                                $syAmount=0;
                            }else{
                                $supplierGroup[$k][$sk]["ini_amount"] = 0;
                                $supplierGroup[$k][$sk]["pay_amount"] = $sv["pay_amount"]+$vv;
                                $supplierGroup[$k][$sk]["status"] = $supplierGroup[$k][$sk]["ini_amount"]?1:2;
                                $syAmount -= $sv["ini_amount"];
                            }
                            $detailList[] = [
                                "pay_app_num" => $kk,
                                "amount" => $vv,
                                "invoice_num" => $sk,
                                "pay_time" => $list[$kk]["pay_time"]
                            ];
                        }
                    }
                }
            }
            if(empty($payAppNumList)) $this->error("没有达到审核条件的付款单");
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $payAppNumList)->where("auditor","=","")->update([
                    "auditor" => $this->admin["username"],
                    "auditor_time" => date("Y-m-d H:i:s")
                ]);
                foreach($supplierGroup as $k=>$v){
                    foreach($v as $kk=>$vv){
                        $invoiceList[$kk] = $vv;
                    }
                }
                $this->detailModel->allowField(true)->saveAll($detailList);
                $invoiceModel->allowField(true)->saveAll($invoiceList);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("审核失败");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function giveUp($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk,"in",$ids)->where("auditor","<>","")->order("writer_time asc")->select();
            if(!$list) $this->error("没有需要弃审的付款信息");
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","<>","")->update([
                    "auditor" => '',
                    "auditor_time" => "0000-00-00 00:00:00"
                ]);
                $detailList = $this->detailModel->where($pk,"in",$ids)->group("invoice_num")->column("invoice_num,sum(amount) as amount");
                if(!empty($detailList)){
                    $invoiceModel = new InvoiceMainXl();
                    $invoiceList = $invoiceModel->where("invoice_num","IN",array_keys($detailList))->column("invoice_num,pay_amount,status");
                    foreach($detailList as $k=>$v){
                        $invoiceList[$k]["pay_amount"] -= $v;
                        $invoiceList[$k]["status"] = $invoiceList[$k]["pay_amount"]?1:0;
                    }
                    $this->detailModel->where($pk,"in",$ids)->delete();
                    $invoiceModel->allowField(true)->saveAll($invoiceList);
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

}
