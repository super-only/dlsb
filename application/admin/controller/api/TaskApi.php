<?php
//正式
namespace app\admin\controller\api;

use app\admin\model\chain\lofting\Dtmaterial;
use app\admin\model\quality\process\ProTestDetailBz;
use app\common\controller\Backend;
use think\Db;
use Exception;
use think\exception\PDOException;

/**
 * 后台首页
 * @internal
 */
class TaskApi extends Backend
{
    const SUPPLIER_CODE = '1000014615';
    protected $noNeedLogin = "*";

    protected $return_msg = [
        "status" => 1,
        "message" => "失败",
        "supplierCode" => self::SUPPLIER_CODE,
        "data" => []
    ];

    public function _initialize()
    {
        parent::_initialize();

        //移除HTML标签
        // $this->request->filter('trim,strip_tags,htmlspecialchars');
    }

    // 原材料检验数据信息直传接口
    public function materialTestApi()
    {
        // $return_data = $this->return_msg;
        // return json_encode($return_data);
        $amount = $this->request->get("amount");
        $return_data = $this->return_msg;
        if(!$amount) return json_encode($return_data);
        $MaterialTestViewModel = new \app\admin\model\api\MaterialTestView();
        $LuSumViewModel = new \app\admin\model\api\LuSumView();
        $materialTestDetailModel = new \app\admin\model\quality\experiment\MaterialTestDetail();
        $list = $MaterialTestViewModel->where([
            "allow" => ["=",1],
            "is_upload" => ["=",0]
            ])->limit($amount)->select();
        if(!$list) return json_encode($return_data);
        $data = $keys = [];
        $matflag = $this->_getMatflagType();
        Db::startTrans();
        try {
            foreach($list as $v){
                $luPiNum = 0;
                if($v["LuPiHao"]) $luPiNum = $LuSumViewModel->where("LuPiHao",$v["LuPiHao"])->value("weight");
                $luPiNum = $luPiNum?round($luPiNum/1000,2):0;
                $keys[$v["MTD_ID"]] = $v["MTD_ID"];
                if(!isset($matflag[$v["IM_Class"]])) continue;
                $data[$v["MTD_ID"]] = [
                    "YCLJYZC_ID" => md5($v["MTD_ID"]),
                    "SUPPLIER_CODE" => "1000014615",
                    "INFO_TYPE_CODE" => "T1010",
                    "MATFLG" => $matflag[$v["IM_Class"]],
                    "MATMATERIAL" => $v["L_Name"],
                    "MATSPEC" => $v["IM_Spec"],
                    "LENGTH" => "".$v["MGN_Length"],
                    "BATCH" => $v["LuPiHao"],
                    "MAT_RECHECK_NO" => $v["LuPiHao"],
                    //材料标准
                    "MAT_STANDARD" => "4",
                    "MATERIAL_STANDARD" => $v["MT_Standard"],
                    //材料生产日期
                    "MAT_PROD_DATE" => date("Y-m-d",strtotime($v["MTD_ChDate"])),
                    //到料日期
                    "MAT_ARRIVAL_DATE" => date("Y-m-d",strtotime($v["MN_Date"])),
                    "SUPPLIER_NAME" => $v["MT_Mader"],
                    "YCLCGBOM_ID" => '\\',
                    //本批采购数量
                    "MAT_QUANTITY" => "".round($v["CTD_Weight"],2),
                    //该炉批号累计采购数量
                    "MAT_BATCH_QUANTITY" => "".$luPiNum,
                    "C_PVALUE" => "".$v["MTD_C"],
                    "SI_PVALUE" => "".$v["MTD_Si"],
                    "MN_PVALUE" => "".$v["MTD_Mn"],
                    "P_PVALUE" => "".$v["MTD_P"],
                    "S_PVALUE" => "".$v["MTD_S"],
                    "V_PVALUE" => "".$v["MTD_V"],
                    "NB_PVALUE" => "".$v["MTD_Nb"],
                    "TI_PVALUE" => "".$v["MTD_Ti"],
                    "CZSL_PVALUE" => $v["MTD_Czsl"],
                    "KLQD_PVALUE" => "".$v["MTD_Rm"],
                    "QFQD_PVALUE" => "".$v["MTD_Rel"],
                    "DHSCL_PVALUE" => "".$v["MTD_Percent"],
                    "CJJC_PVALUE" => $v["MTD_Cjf"].','.$v["MTD_Cjs"].','.$v["MTD_Cjt"],
                    "CJJC_TEMPERATURE" => "".$v["MTD_Temperature"],
                    "WQSY_PVALUE" => $v["MTD_Wq"],
                    "ZCWG_PVALUE" => $v["MTD_Wg"],
                    "ACQ_TIME" => date("Y-m-d H:i:s"),
                    "DETECTION_CHEM_TIME" => $v["MT_WriteDate"],
                    "DETECTION_CHEM_USER" => $v["MT_Writer"],
                    "DETECTION_MECH_TIME" => $v["MT_WriteDate"],
                    "DETECTION_MECH_USER" => $v["MT_Writer"],
                    "DETECTION_APPE_TIME" => $v["MT_WriteDate"],
                    "DETECTION_APPE_USER" => $v["MT_Writer"],
                    "EXAMINE_USER" => $v["MT_Writer"]

                ];
            }
            $result = $materialTestDetailModel->where("MTD_ID","IN",$keys)->update(["is_upload"=>1]);
            if($result) Db::commit();
            else{
                Db::rollback();                    
                throw new Exception("失败");
            }
        } catch (PDOException | Exception $e) {
            Db::rollback();
            $return_data["message"] = $e->getMessage();
            return json_encode($return_data);
        }
        $return_data["status"]=0;
        $return_data["message"] = "成功！";
        $return_data["data"] = array_values($data);
        return json_encode($return_data);
    }

    //原材料检验采集数据信息接口  补充紧固件和辅助材料的
    public function collectApi()
    {
        
        $amount = $this->request->get("amount");
        $return_data = $this->return_msg;

        // return json_encode($return_data);

        if(!$amount) return json_encode($return_data);
        $ProMaterialTestDetailBzModel = new \app\admin\model\chain\material\ProMaterialTestDetailBz;
        $list = $ProMaterialTestDetailBzModel->alias("mtdb")
            ->join(["product_work_view"=>"pwv"],"pwv.PT_Num=mtdb.PT_Num")
            ->field("mtdb.*,pwv.poNo,pwv.T_Num,pwv.poItemId,pwv.poItemNo,pwv.TD_ID")
            ->where([
            "mtdb.upload" => ["=",0],
            ])->limit($amount)->select();
        if(!$list) return json_encode($return_data);
        $data = [];
        Db::startTrans();
        try {
            foreach($list as $v){
                $data[$v["ID"]] = [
                    "ORDER_NO"=> $v["poNo"],
                    "SUPPLIER_CODE"=> self::SUPPLIER_CODE,
                    "PROD_ORDER_NO"=> $v["T_Num"],
                    "PO_ITEM_ID"=> $v["poItemId"],
                    "PROD_WORK_ORDER"=> $v["PT_Num"],
                    "INFO_TYPE_CODE"=> 'T1001',
                    "TRACE_CODE"=> self::SUPPLIER_CODE.$v["poItemNo"]."0000".(str_pad($v["TD_ID"],16,0,STR_PAD_LEFT))."0000",
                    "MATFLG"=>$v["MATFLG"],
                    "MATMATERIAL"=> $v["MATMATERIAL"],
                    "MATSPEC"=> $v["MATSPEC"],
                    "INSPSTD"=> $v["INSPSTD"],
                    "ITEM_DETAIL"=> $v["ITEM_DETAIL"],
                    "RULE_CODE"=> $v["RULE_CODE"],
                    "BATCH"=> "".$v["BATCH"],
                    "MAT_RECHECK_NO"=>"".$v["MAT_RECHECK_NO"],
                    "PROD_VALUE"=> "".$v["PROD_VALUE"],
                    "ACQ_TIME"=> date("Y-m-d H:i:s"),
                    "DETECTION_TIME"=> $v["DETECTION_TIME"],
                    "DETECTION_USER"=> $v["DETECTION_USER"],
                    "EXAMINE_USER" => $v["DETECTION_USER"]
                ];
            }
            $result = $ProMaterialTestDetailBzModel->where("ID","IN",array_keys($data))->update(["upload"=>1]);
            if($result) Db::commit();
            else{
                Db::rollback();                    
                throw new Exception("失败");
            }
        } catch (PDOException | Exception $e) {
            Db::rollback();
            $return_data["message"] = $e->getMessage();
            return json_encode($return_data);
        }
        $return_data["status"]=0;
        $return_data["message"] = "成功！";
        $return_data["data"] = array_values($data);
        return json_encode($return_data);
    }

    // 生产/试验过程采集数据信息接口
    public function proTestApi()
    {
        $amount = $this->request->get("amount");
        $return_data = $this->return_msg;
        // return json_encode($return_data);

        if(!$amount) return json_encode($return_data);
        $ProTestDetailBzModel = new \app\admin\model\quality\process\ProTestDetailBz();
        $list = $ProTestDetailBzModel->alias("ptdb")
            ->join(["product_work_view"=>"ptv"],"ptdb.PT_Num = ptv.PT_Num")
            ->where([
                "ptdb.UPLOAD" => ["=",0],
            ])
            ->limit(0,$amount)->select();
        if(!$list) return json_encode($return_data);
        $data = [];
        Db::startTrans();
        try {
            foreach($list as $v){
                $data[$v["ID"]] = [
                    "ORDER_NO" => $v["poNo"],
                    "SUPPLIER_CODE"=> self::SUPPLIER_CODE,
                    "PO_ITEM_ID"=> $v["poItemId"],
                    "PROD_ORDER_NO"=> $v["T_Num"],
                    "PROD_WORK_ORDER" => $v["PT_Num"],
                    "INFO_TYPE_CODE" => "T1002",
                    "TRACE_CODE"=> self::SUPPLIER_CODE.$v["poItemNo"]."0000".(str_pad($v["TD_ID"],16,0,STR_PAD_LEFT))."0000",
                    "PART_CODE" => $v["PART_CODE"],
                    "MATMATERIAL" => $v["MATMATERIAL"],
                    "MATSPEC" => $v["MATSPEC"],
                    "SAMPLING_RATE" => $v["rate"],
                    "RULE_CODE" => $v["RULE_CODE"],
                    "PROD_VALUE" => $v["value"],
                    "ACQ_TIME" => date("Y-m-d H:i:s"),
                    "DETECTION_TIME" => $v["time"],
                    "DETECTION_USER" => "王金淼",
                    "EXAMINE_USER" => "王金淼",
                ];
            }
            $result = $ProTestDetailBzModel->where("ID","IN",array_keys($data))->update(["UPLOAD"=>1]);
            if($result) Db::commit();
            else{
                Db::rollback();                    
                throw new Exception("失败");
            }
        } catch (PDOException | Exception $e) {
            Db::rollback();
            $return_data["message"] = $e->getMessage();
            return json_encode($return_data);
        }
        $return_data["status"]=0;
        $return_data["message"] = "成功！";
        $return_data["data"] = array_values($data);
        return json_encode($return_data);
    }

    // 成品入库管理信息（新建、更新）接口
    public function proApiTest()
    {
        // $return_data = $this->return_msg;
        // return json_encode($return_data);
        $amount = $this->request->get("amount");
        $return_data = $this->return_msg;
        return json_encode($return_data);
        if(!$amount) return json_encode($return_data);
        $sectConfigDetailModel = new \app\admin\model\chain\sale\Sectconfigdetail();
        $productWorkViewModel = new \app\admin\model\eipapi\api\ProductWorkView();
        $list = $productWorkViewModel->alias("pwv")
            ->join(["taskheight"=>"th"],"pwv.TD_ID=th.TD_ID")
            ->JOIN(["sectconfigdetail"=>"scd"],"th.TH_ID=scd.TH_ID")
            ->FIELD("pwv.PT_Num,concat(pwv.PT_Num,',',pwv.materialsDescription,',',scd.SCD_TPNum) as CCPGL_ID,pwv.poNo,pwv.t_project,pwv.materialsDescription,scd.SCD_TPNum,pwv.TD_ID,scd.SCD_ID,scd.SCD_Weight,pwv.planFinishDate,pwv.T_Num,scd.cp_upload")
            ->WHERE(["scd.cp_upload"=>["=",1]])
            ->LIMIT(0,$amount)
            ->SELECT();
        $data = [];
        // $iniPack = [
        //     "PROD_ORDER_NO"=> "",
        //     "PROD_WORK_ORDER"=> "",
        //     "TRACE_CODE"=> "",
        //     "PACKAGE_NAME"=> "",
        //     "WEIGHT"=> "",
        //     "FN_BAOZHUANGJH_DATE"=> "0000-00-00",
        //     "FN_BAOZHUANG_DATE"=> "0000-00-00",
        //     "FN_FAHUOJH_DATE"=> "0000-00-00",
        //     "FN_FAHUO_DATE"=> "0000-00-00",
        // ];
        foreach($list as $k=>$v){
            $data[$v["SCD_ID"]] = [
                "CCPGL_ID"=>md5($v["CCPGL_ID"]),
                "SUPPLIER_CODE"=> self::SUPPLIER_CODE,
                "ORDER_NO"=>$v["poNo"],
                "PRJNAME"=>$v["t_project"],
                "TOWER_TYPE"=> $v["materialsDescription"],
                "TOWER_NO"=> $v["SCD_TPNum"],
                "INFO_TYPE_CODE"=> "T1008",
                "TRACE_CODE"=> self::SUPPLIER_CODE.$v["poNo"]."0000".(str_pad($v["TD_ID"],16,0,STR_PAD_LEFT))."0000",
                "WEIGHT"=>"".round($v["SCD_Weight"]/1000,2),
                "CONTRACT_DELIVERY_DATE"=>date("Y-m-d",strtotime($v["planFinishDate"])),
                "FN_BAOZH"=>"0",
                "FN_BAOZH_DATE"=> "0000-00-00",
                "FN_RUKU"=>"0",
                "FN_RUKU_DATE"=>"0000-00-00",
                "FN_FHJH_DATE"=>"0000-00-00",
                "FN_FH"=>"0",
                "FN_FH_DATE"=>"0000-00-00",
                "REMARK"=> "无备注",
                "ACQ_TIME" => date("Y-m-d H:i:s"),
                "PACKAGELIST"=> [],
                "T_Num"=>$v["T_Num"],
                "PT_Num"=>$v["PT_Num"],
            ];
        }
        $bzList = (new \app\admin\model\chain\pack\Pack())->alias("p")
            ->join(["packdetail"=>"pd"],"pd.P_ID=p.P_ID")
            ->field("p.AuditorDate,pd.PD_Name,p.SCD_ID,pd.PD_sumWeight")
            ->where("p.SCD_ID","IN",array_keys($data))->where("p.Auditor","<>","")->select();
        foreach($bzList as $k=>$v){
            $data[$v["SCD_ID"]]["FN_BAOZH"] = "100";
            $data[$v["SCD_ID"]]["FN_BAOZH_DATE"] = date("Y-m-d",strtotime($v["AuditorDate"]));
            // $data[$v["SCD_ID"]][$v["PD_Name"]] = $iniPack;
            $data[$v["SCD_ID"]]["PACKAGELIST"][$v["PD_Name"]] = [
                "PROD_ORDER_NO"=> $data[$v["SCD_ID"]]["T_Num"],
                "PROD_WORK_ORDER"=> $data[$v["SCD_ID"]]["PT_Num"],
                "TRACE_CODE"=> $data[$v["SCD_ID"]]["TRACE_CODE"],
                "PACKAGE_NAME"=> $v["PD_Name"],
                "WEIGHT"=> $v["PD_sumWeight"],
                "FN_BAOZHUANGJH_DATE"=> date("Y-m-d",strtotime($v["AuditorDate"])),
                "FN_BAOZHUANG_DATE"=> date("Y-m-d",strtotime($v["AuditorDate"])),
                "FN_FAHUOJH_DATE"=> "0000-00-00",
                "FN_FAHUO_DATE"=> "0000-00-00"
            ];
        }
        
        $rkList = (new \app\admin\model\chain\pack\ProduceUnitIn())->alias("pui")
            ->join(["produceunitmain"=>"puim"],"puim.PUM_Num=pui.PUM_Num")
            ->FIELD("pui.SCD_ID,puim.AuditorDate")
            ->where("pui.SCD_ID","IN",array_keys($data))->where("puim.Auditor","<>","")->select();
        foreach($rkList as $k=>$v){
            $data[$v["SCD_ID"]]["FN_RUKU"] = "100";
            $data[$v["SCD_ID"]]["FN_RUKU_DATE"] = date("Y-m-d",strtotime($v["AuditorDate"]));
        }

        $rkList = (new \app\admin\model\chain\pack\SaleInvoiceSingle())->alias("sis")
            ->join(["saleinvoice"=>"si"],"sis.SI_Num=si.SI_Num")
            ->FIELD("sis.SIS_ScdId,si.Writedate")
            ->where("sis.SIS_ScdId","IN",array_keys($data))->select();
        foreach($rkList as $k=>$v){
            $data[$v["SIS_ScdId"]]["FN_FHJH_DATE"] = date("Y-m-d",strtotime($v["Writedate"]));
        }

        $fyList = (new \app\admin\model\chain\pack\BsoCount())->alias("bc")
            ->join(["saleinvoicedetail"=>"sid"],"sid.BSO_ID=bc.BSO_ID")
            ->JOIN(["saleinvoicemain"=>"sim"],"sim.SIM_Num=sid.SI_Num")
            ->field("bc.SCD_ID,bc.PD_Name,sim.WriterDate")
            ->where("bc.SCD_ID","IN",array_keys($data))->order("sim.WriterDate")->select();
        foreach($fyList as $k=>$v){
            $data[$v["SCD_ID"]]["FN_FH_DATE"] = date("Y-m-d",strtotime($v["WriterDate"]));
            $data[$v["SCD_ID"]]["PACKAGELIST"][$v["PD_Name"]]["FN_FAHUO_DATE"] = date("Y-m-d",strtotime($v["WriterDate"]));
        }
        foreach($data as $k=>$v){
            if(empty($v["PACKAGELIST"])) unset($data[$k]);
            else{
                $finish = $count = $rate = 0;
                unset($data[$k]["T_Num"],$data[$k]["PT_Num"]);
                foreach($v["PACKAGELIST"] as $kk=>$vv){
                    $count++;
                    if($vv["FN_FAHUO_DATE"]!="0000-00-00") $finish++;
                    $data[$k]["PACKAGELIST"][$kk]["FN_FAHUOJH_DATE"] = $v["FN_FHJH_DATE"];
                }
                if($count==1) $data[$k]["PACKAGELIST"][$kk]["WEIGHT"] = $data[$k]["WEIGHT"];
                $rate = round($finish/$count*100,0);
                $data[$k]["FN_FH"] = "".$rate;
                $data[$k]["PACKAGELIST"] = json_encode(array_values($data[$k]["PACKAGELIST"]));
            }
        }
        Db::startTrans();
        try {
            $result = $sectConfigDetailModel->where("SCD_ID","IN",array_keys($data))->update(["cp_upload"=>2]);
            if($result) Db::commit();
            else{
                Db::rollback();                    
                throw new Exception("失败");
            }
        } catch (PDOException | Exception $e) {
            Db::rollback();
            $return_data["message"] = $e->getMessage();
            return json_encode($return_data);
        }
        $return_data["status"]=0;
        $return_data["message"] = "成功！";
        $return_data["data"] = array_values($data);
        return json_encode($return_data);
    }

    public function proApi()
    {
        // $return_data = $this->return_msg;
        // return json_encode($return_data);
        $amount = $this->request->get("amount");
        $return_data = $this->return_msg;
        if(!$amount) return json_encode($return_data);
        $sectConfigDetailModel = new \app\admin\model\chain\sale\Sectconfigdetail();
        $productWorkViewModel = new \app\admin\model\eipapi\api\ProductWorkView();
        $list = $productWorkViewModel->alias("pwv")
            ->join(["taskheight"=>"th"],"pwv.TD_ID=th.TD_ID")
            ->JOIN(["sectconfigdetail"=>"scd"],"th.TH_ID=scd.TH_ID")
            ->FIELD("pwv.PT_Num,concat(pwv.PT_Num,',',pwv.materialsDescription,',',scd.SCD_TPNum) as CCPGL_ID,pwv.poNo,pwv.t_project,pwv.materialsDescription,scd.SCD_TPNum,pwv.TD_ID,scd.SCD_ID,scd.SCD_Weight,pwv.planFinishDate,pwv.T_Num,scd.cp_upload,poItemNo")
            ->WHERE(["scd.cp_upload"=>["=",1]])
            ->LIMIT(0,$amount)
            ->SELECT();
        // pri(collection($list)->toArray(),1);
        $data = [];
        $scdList = [];
        // $iniPack = [
        //     "PROD_ORDER_NO"=> "",
        //     "PROD_WORK_ORDER"=> "",
        //     "TRACE_CODE"=> "",
        //     "PACKAGE_NAME"=> "",
        //     "WEIGHT"=> "",
        //     "FN_BAOZHUANGJH_DATE"=> "0000-00-00",
        //     "FN_BAOZHUANG_DATE"=> "0000-00-00",
        //     "FN_FAHUOJH_DATE"=> "0000-00-00",
        //     "FN_FAHUO_DATE"=> "0000-00-00",
        // ];
        foreach($list as $k=>$v){
            $scdList[$v["SCD_ID"]][] = $v["PT_Num"];
            $data[$v["PT_Num"]][$v["SCD_ID"]] = [
                "CCPGL_ID"=>md5($v["CCPGL_ID"]),
                "SUPPLIER_CODE"=> self::SUPPLIER_CODE,
                "ORDER_NO"=>$v["poNo"],
                "PRJNAME"=>$v["t_project"],
                "TOWER_TYPE"=> $v["materialsDescription"],
                "TOWER_NO"=> $v["SCD_TPNum"],
                "INFO_TYPE_CODE"=> "T1008",
                "TRACE_CODE"=> self::SUPPLIER_CODE.$v["poItemNo"]."0000".(str_pad($v["TD_ID"],16,0,STR_PAD_LEFT))."0000",
                "WEIGHT"=>"".round($v["SCD_Weight"]/1000,2),
                "CONTRACT_DELIVERY_DATE"=>date("Y-m-d",strtotime($v["planFinishDate"])),
                "FN_BAOZH"=>"0",
                "FN_BAOZH_DATE"=> "0000-00-00",
                "FN_RUKU"=>"0",
                "FN_RUKU_DATE"=>"0000-00-00",
                "FN_FHJH_DATE"=>"0000-00-00",
                "FN_FH"=>"0",
                "FN_FH_DATE"=>"0000-00-00",
                "REMARK"=> "无备注",
                "ACQ_TIME" => date("Y-m-d H:i:s"),
                "PACKAGELIST"=> [],
                "T_Num"=>$v["T_Num"],
                "PT_Num"=>$v["PT_Num"],
            ];
        }
        $bzList = (new \app\admin\model\chain\pack\Pack())->alias("p")
            ->join(["packdetail"=>"pd"],"pd.P_ID=p.P_ID")
            ->field("p.AuditorDate,pd.PD_Name,p.SCD_ID,pd.PD_sumWeight")
            ->where("p.SCD_ID","IN",array_keys($scdList))->where("p.Auditor","<>","")->select();
        foreach($bzList as $k=>$v){
            foreach($scdList[$v["SCD_ID"]] as $slk=>$slv){
                $data[$slv][$v["SCD_ID"]]["FN_BAOZH"] = "100";
                $data[$slv][$v["SCD_ID"]]["FN_BAOZH_DATE"] = date("Y-m-d",strtotime($v["AuditorDate"]));
                // $data[$v["SCD_ID"]][$v["PD_Name"]] = $iniPack;
                $data[$slv][$v["SCD_ID"]]["PACKAGELIST"][$v["PD_Name"]] = [
                    "PROD_ORDER_NO"=> $data[$slv][$v["SCD_ID"]]["T_Num"],
                    "PROD_WORK_ORDER"=> $data[$slv][$v["SCD_ID"]]["PT_Num"],
                    "TRACE_CODE"=> $data[$slv][$v["SCD_ID"]]["TRACE_CODE"],
                    "PACKAGE_NAME"=> $v["PD_Name"],
                    "WEIGHT"=> $v["PD_sumWeight"],
                    "FN_BAOZHUANGJH_DATE"=> date("Y-m-d",strtotime($v["AuditorDate"])),
                    "FN_BAOZHUANG_DATE"=> date("Y-m-d",strtotime($v["AuditorDate"])),
                    "FN_FAHUOJH_DATE"=> "0000-00-00",
                    "FN_FAHUO_DATE"=> "0000-00-00"
                ];
            }
        }
        
        $rkList = (new \app\admin\model\chain\pack\ProduceUnitIn())->alias("pui")
            ->join(["produceunitmain"=>"puim"],"puim.PUM_Num=pui.PUM_Num")
            ->FIELD("pui.SCD_ID,puim.AuditorDate")
            ->where("pui.SCD_ID","IN",array_keys($scdList))->where("puim.Auditor","<>","")->select();
        foreach($rkList as $k=>$v){
            foreach($scdList[$v["SCD_ID"]] as $slk=>$slv){
                $data[$slv][$v["SCD_ID"]]["FN_RUKU"] = "100";
                $data[$slv][$v["SCD_ID"]]["FN_RUKU_DATE"] = date("Y-m-d",strtotime($v["AuditorDate"]));
            }
        }

        $rkList = (new \app\admin\model\chain\pack\SaleInvoiceSingle())->alias("sis")
            ->join(["saleinvoice"=>"si"],"sis.SI_Num=si.SI_Num")
            ->FIELD("sis.SIS_ScdId,si.Writedate")
            ->where("sis.SIS_ScdId","IN",array_keys($scdList))->select();
        foreach($rkList as $k=>$v){
            foreach($scdList[$v["SIS_ScdId"]] as $slk=>$slv){
                $data[$slv][$v["SIS_ScdId"]]["FN_FHJH_DATE"] = date("Y-m-d",strtotime($v["Writedate"]));
            }
        }

        $fyList = (new \app\admin\model\chain\pack\BsoCount())->alias("bc")
            ->join(["saleinvoicedetail"=>"sid"],"sid.BSO_ID=bc.BSO_ID")
            ->JOIN(["saleinvoicemain"=>"sim"],"sim.SIM_Num=sid.SI_Num")
            ->field("bc.SCD_ID,bc.PD_Name,sim.WriterDate")
            ->where("bc.SCD_ID","IN",array_keys($scdList))->order("sim.WriterDate")->select();
        foreach($fyList as $k=>$v){
            foreach($scdList[$v["SCD_ID"]] as $slk=>$slv){
                $data[$slv][$v["SCD_ID"]]["FN_FH_DATE"] = date("Y-m-d",strtotime($v["WriterDate"]));
                $data[$slv][$v["SCD_ID"]]["PACKAGELIST"][$v["PD_Name"]]["FN_FAHUO_DATE"] = date("Y-m-d",strtotime($v["WriterDate"]));
            }
        }
        $endData = [];
        // foreach($data as $k=>$v){
        //     foreach($scdList as $slk=>$slv){
        //         if(empty($v["PACKAGELIST"])) unset($data[$slv][$k]);
        //         else{
        //             $finish = $count = $rate = 0;
        //             unset($data[$slv][$k]["T_Num"],$data[$slv][$k]["PT_Num"]);
        //             foreach($v["PACKAGELIST"] as $kk=>$vv){
        //                 $count++;
        //                 if($vv["FN_FAHUO_DATE"]!="0000-00-00") $finish++;
        //                 $data[$slv][$k]["PACKAGELIST"][$kk]["FN_FAHUOJH_DATE"] = $v["FN_FHJH_DATE"];
        //             }
        //             $rate = round($finish/$count*100,0);
        //             $data[$slv][$k]["FN_FH"] = "".$rate;
        //             $data[$slv][$k]["PACKAGELIST"] = json_encode(array_values($data[$slv][$k]["PACKAGELIST"]));
        //         }
        //         $endData[] = $data[$slv][$k];
        //     }
        // }

        foreach($data as $k=>$v){
            foreach($v as $slk=>$slv){
                if(empty($slv["PACKAGELIST"])) unset($data[$k][$slk]);
                else{
                    $finish = $count = $rate = 0;
                    unset($data[$k][$slk]["T_Num"],$data[$k][$slk]["PT_Num"]);
                    foreach($slv["PACKAGELIST"] as $kk=>$vv){
                        $count++;
                        if($vv["FN_FAHUO_DATE"]!="0000-00-00") $finish++;
                        $data[$k][$slk]["PACKAGELIST"][$kk]["FN_FAHUOJH_DATE"] = $slv["FN_FHJH_DATE"];
                    }
                    $rate = round($finish/$count*100,0);
                    $data[$k][$slk]["FN_FH"] = "".$rate;
                    $data[$k][$slk]["PACKAGELIST"] = json_encode(array_values($data[$k][$slk]["PACKAGELIST"]));
                    $endData[] = $data[$k][$slk];
                }
            }
        }
        Db::startTrans();
        try {
            $result = $sectConfigDetailModel->where("SCD_ID","IN",array_keys($scdList))->update(["cp_upload"=>2]);
            if($result) Db::commit();
            else{
                Db::rollback();                    
                throw new Exception("失败");
            }
        } catch (PDOException | Exception $e) {
            Db::rollback();
            $return_data["message"] = $e->getMessage();
            return json_encode($return_data);
        }
        $return_data["status"]=0;
        $return_data["message"] = "成功！";
        $return_data["data"] = array_values($endData);
        return json_encode($return_data);
    }

    //原材料/生产/试验过程采集附件信息接口 未好 补充紧固件和辅助材料的文件
    public function enclosureApi()
    {
        $return_data = $this->return_msg;
        // return json_encode($return_data);
        $amount = $this->request->get("amount");
        // $amount = 5;
        if(!$amount) return json_encode($return_data);
        $fjmodel = new \app\admin\model\eipapi\passive\FjMaterialTestBz();
        $list = $fjmodel->alias("ptdb")
        ->join(["product_work_view"=>"ptv"],"ptdb.PT_Num = ptv.PT_Num")
        ->where([
            "ptdb.upload" => ["=",0],
        ])
        ->limit(0,$amount)->select();
        if(!$list) return json_encode($return_data);
        $data = $noFile = [];
        Db::startTrans();
        try {
            foreach($list as $v){
                $file = ROOT_PATH . DS . 'public' . DS . 'zlfiles' . DS . $v["fileContent"];
                if(is_file($file)){
                    $img = base64_encode(file_get_contents($file));
                    $data[$v["ID"]] = [
                        "PROD_WORK_ORDER"=> $v["PT_Num"],
                        "BELONG_CODE"=> $v["BELONGTO_CODE"],
                        "INFO_TYPE_CODE"=> "T1003",
                        "PART_CODE"=> $v["PART_CODE"]?$v["PART_CODE"]:"无",
                        // "BELONGTO_ID" => $v["BELONGTO_ID"]?$v["BELONGTO_ID"]:"无",
                        "BATCH" => $v["BATCH"]?$v["BATCH"]:"无",
                        "MAT_RECHECK_NO"=> $v["MAT_RECHECK_NO"]?$v["MAT_RECHECK_NO"]:"无",
                        "FILE_TYPE"=>$v["FILE_TYPE"],
                        "FILE_NO"=> $v["FILE_NO"]?$v["FILE_NO"]:"无",
                        "FILE_DATA"=> [
                            "filename"=> $v["filename"],
                            "fileContent"=> $img
                        ]
                    ];
                }else $noFile[$v["ID"]] = $v["ID"];
            }
            $result = $fjmodel->where("ID","IN",array_keys($data))->update(["upload"=>1]);
            $fjResult = $fjmodel->where("ID","IN",array_keys($noFile))->update(["upload"=>2]);
            if($result or $fjResult) Db::commit();
            else{
                Db::rollback();                    
                throw new Exception("失败");
            }
        } catch (PDOException | Exception $e) {
            Db::rollback();
            $return_data["message"] = $e->getMessage();
            return json_encode($return_data);
        }
        $return_data["status"]=0;
        $return_data["message"] = "成功！";
        $return_data["data"] = array_values($data);
        return json_encode($return_data);
    }

    //镀锌温度信息采集接口
    // public function galTemApi()
    // {
    //     $amount = $this->request->get("amount");
    //     $return_data = $this->return_msg;
        
    //     // return json_encode($return_data);

    //     if(!$amount) return json_encode($return_data);
    //     $productProcessModel = (new \app\admin\model\quality\process\ProProcessMain());
    //     $dxIdList = $productProcessModel->where([
    //         "dx_upload" => ["=",1]
    //     ])->limit($amount)->column("id");
    //     $list = $productProcessModel->alias("ppm")
    //         ->join(["product_work_view"=>"pwv"],"ppm.PT_Num=pwv.PT_Num")
    //         ->join(["galvanized_true"=>"gt"],"gt.ppm_id=ppm.id")
    //         ->join(["dev_duxin"=>"dd"],"gt.temp_id=dd.id")
    //         ->field("ppm.id,pwv.*,dd.sj,dd.wd")
    //         ->where("ppm.id","IN",$dxIdList)
    //         ->order("dd.sj asc")
    //         ->select();
    //     if(!$list) return json_encode($return_data);
    //     $data = [];
    //     Db::startTrans();
    //     try {
    //         foreach($list as $v){
    //             if((substr($v["PT_Num"],0,4)+0)>=2211){
    //                 isset($data[$v["id"]])?"":$data[$v["id"]] = [
    //                     "ORDER_NO"=> $v["poNo"],
    //                     "SUPPLIER_CODE"=> self::SUPPLIER_CODE,
    //                     "PROD_ORDER_NO"=> $v["T_Num"],
    //                     "PO_ITEM_ID"=> $v["poItemId"],
    //                     "PROD_WORK_ORDER"=> $v["PT_Num"],
    //                     "INFO_TYPE_CODE"=> 'T1007',
    //                     "GALVANIZED_BATCH_NO" => $v["PT_Num"],
    //                     "WAVE_FORM" => [
    //                         "TIME" => [],
    //                         "TEMPERATURES" => []
    //                     ]
    //                 ];
    //                 $data[$v["id"]]["WAVE_FORM"]["TIME"][] = $v["sj"];
    //                 $data[$v["id"]]["WAVE_FORM"]["TEMPERATURES"][] = $v["wd"];
    //             }
    //         }
    //         $diff = array_diff($dxIdList,array_keys($data));
    //         if(!empty($diff)){
    //             $diffList = $productProcessModel->alias("ppm")
    //                 ->join(["product_work_view"=>"pwv"],"ppm.PT_Num=pwv.PT_Num")
    //                 ->join(["pack"=>"p"],"p.TD_ID=pwv.TD_ID")
    //                 ->field("ppm.*,pwv.*,p.WriteDate")
    //                 ->where("ppm.id","IN",$diff)
    //                 ->group("ppm.PT_Num")
    //                 ->select();
    //             foreach($diffList as $k=>$v){
    //                 // $new_ids = substr($v["PT_Num"],0,4)+0;
    //                 // if($new_ids>=2211){
    //                     $data[$v["id"]] = [
    //                         "ORDER_NO"=> $v["poNo"],
    //                         "SUPPLIER_CODE"=> self::SUPPLIER_CODE,
    //                         "PROD_ORDER_NO"=> $v["T_Num"],
    //                         "PO_ITEM_ID"=> $v["poItemId"],
    //                         "PROD_WORK_ORDER"=> $v["PT_Num"],
    //                         "INFO_TYPE_CODE"=> 'T1007',
    //                         "GALVANIZED_BATCH_NO" => $v["PT_Num"],
    //                         "WAVE_FORM" => [
    //                             "TIME" => [date("Y-m-d H:i:s",strtotime($v["WriteDate"])-14000),date("Y-m-d H:i:s",strtotime($v["WriteDate"])-12000),date("Y-m-d H:i:s",strtotime($v["WriteDate"])-10000),date("Y-m-d H:i:s",strtotime($v["WriteDate"])-8000),date("Y-m-d H:i:s",strtotime($v["WriteDate"])-6000),date("Y-m-d H:i:s",strtotime($v["WriteDate"])-4000)],
    //                             "TEMPERATURES" => [436.1,435.9,436.1,436.1,436,436.1]
    //                         ]
    //                     ];
    //                 // }
    //             }
    //         }
    //         foreach($data as $k=>$v){
    //             $data[$k]["WAVE_FORM"] = json_encode($v["WAVE_FORM"]);
    //         }
    //         $result = $productProcessModel->where("id","IN",array_keys($data))->update(["dx_upload"=>2]);
    //         if($result) Db::commit();
    //         else{
    //             Db::rollback();                    
    //             throw new Exception("失败");
    //         }
    //     } catch (PDOException | Exception $e) {
    //         Db::rollback();
    //         $return_data["message"] = $e->getMessage();
    //         return json_encode($return_data);
    //     }
    //     $return_data["status"]=0;
    //     $return_data["message"] = "成功！";
    //     $return_data["data"] = array_values($data);
    //     return json_encode($return_data);
    // }

    //镀锌温度信息采集接口
    public function galTemApi()
    {
        $amount = $this->request->get("amount");
        $return_data = $this->return_msg;
        // return json_encode($return_data);
        if(!$amount) return json_encode($return_data);
        $productProcessModel = (new \app\admin\model\quality\process\ProProcessMain());
        $dxIdList = $productProcessModel->where([
            "dx_upload" => ["=",1]
        ])->limit($amount)->column("id,PT_Num");
        $data = [];
        Db::startTrans();
        try {

            $list = $productProcessModel->alias("ppm")
                ->join(["product_work_view"=>"pwv"],"ppm.PT_Num=pwv.PT_Num")
                ->join(["galvanized_true"=>"gt"],"gt.ppm_id=ppm.id")
                ->field("ppm.id,pwv.*,gt.time,gt.temp")
                ->where("ppm.id","IN",array_keys($dxIdList))
                ->order("gt.time asc")
                ->select();
            foreach($list as $v){
                isset($data[$v["id"]])?"":$data[$v["id"]] = [
                    "ORDER_NO"=> $v["poNo"],
                    "SUPPLIER_CODE"=> self::SUPPLIER_CODE,
                    "PROD_ORDER_NO"=> $v["T_Num"],
                    "PO_ITEM_ID"=> $v["poItemId"],
                    "PROD_WORK_ORDER"=> $v["PT_Num"],
                    "INFO_TYPE_CODE"=> 'T1007',
                    "GALVANIZED_BATCH_NO" => $v["PT_Num"],
                    "WAVE_FORM" => [
                        "TIME" => [],
                        "TEMPERATURES" => []
                    ]
                ];
                $data[$v["id"]]["WAVE_FORM"]["TIME"][] = $v["time"];
                $data[$v["id"]]["WAVE_FORM"]["TEMPERATURES"][] = $v["temp"];
            }
            foreach($data as $k=>$v){
                $data[$k]["WAVE_FORM"] = json_encode($v["WAVE_FORM"]);
            }
            $result = $productProcessModel->where("id","IN",array_keys($data))->update(["dx_upload"=>2]);
            if($result) Db::commit();
            else{
                Db::rollback();                    
                throw new Exception("失败");
            }
        } catch (PDOException | Exception $e) {
            Db::rollback();
            $return_data["message"] = $e->getMessage();
            return json_encode($return_data);
        }
        $return_data["status"]=0;
        $return_data["message"] = "成功！";
        $return_data["data"] = array_values($data);
        return json_encode($return_data);
    }

    //采购订单综合信息维护（新建、更新）接口 未好 暂时不用动
    public function orderApi()
    {  $return_data = $this->return_msg;
        return json_encode($return_data);
        $str = '{
            "status": 0,
            "message": "",
            "supplierCode": "20140",
            "data": [{
                "CGDDZH_ID": "a31e030432bb3b00a99233dfcbbaa52c",
                "SUPPLIER_CODE": "1000014615",
                "ORDER_NO": "2210241257",
                "PRJNAME": "绍兴电力设备有限公司虚拟合同",
                "PO_ITEM_ID":"6727980335480523744",
                "INFO_TYPE_CODE": "T1009",
                "VOLTAGE_LVL": "220",
                "TOWERTYPES_AMOUNT": "2",
                "TOWERS_AMOUNT": "4",
                "SP_TOWERS_AMOUNT": "4",
                "FN_TOWERS_AMOUNT": "0",
                "TOTAL_WEIGHT": "4.32",
                "SP_TOWERS_WEIGHT": "4.32",
                "FN_TOWERS_WEIGHT": "2",
                "FN_LOFTING": "100",
                "FN_LOFTING_DATE": "2022-11-05",
                "FN_JGBL": "100",
                "FN_JGBL_DATE": "2022-11-05",
                "FN_GBBL": "100",
                "FN_GBBL_DATE": "2022-11-05",
                "FN_GGBL": "100",
                "FN_GGBL_DATE": "2022-11-05",
                "FN_FLBL": "100",
                "FN_FLBL_DATE": "2022-11-05",
                "FN_LSDH": "100",
                "FN_LSDH_DATE": "2022-11-05",
                "FN_BCP": "50",
                "FN_BCP_DATE": "2022-10-24",
                "FN_DUXIN": "50",
                "FN_DUXIN_DATE": "2022-10-24",
                "FN_BAOZH": "50",
                "FN_BAOZH_DATE": "2022-10-24",
                "FN_FAHUO": "50",
                "FN_FAHUO_DATE": "2022-10-24",
                "REMARK": "无备注",
                "ACQ_TIME": "2022-11-09 10:03:01"
            }
        ]
        }';
        return $str;
    }

    //质量追溯信息数据采集接口 未好
    public function qualityApi()
    {   $return_data = $this->return_msg;
        return json_encode($return_data);


        $amount = $this->request->get("amount");
        $return_data = $this->return_msg;
        if(!$amount) return json_encode($return_data);
        $ProMaterialTestDetailBzModel = new \app\admin\model\chain\material\ProMaterialTestDetailBz;
        $list = $ProMaterialTestDetailBzModel->alias("mtdb")
            ->join(["product_work_view"=>"pwv"],"pwv.PT_Num=mtdb.PT_Num")
            ->field("mtdb.*,pwv.poNo,pwv.T_Num,pwv.poItemId,pwv.poItemNo,pwv.TD_ID")
            ->where([
            "mtdb.zs_upload" => ["=",0],
            "mtdb.MATFLG" => ["IN",['01001','01002']]
            ])->limit($amount)->group("BATCH")->select();
        if(!$list) return json_encode($return_data);
        $gdList = [];
        foreach($list as $v){
            $gdList[$v["TD_ID"]] = $v["TD_ID"];
        }
        $dtList = (new Dtmaterial())->alias("d")
            ->join(["dtsect"=>"ds"],"d.DtM_iID_PK=ds.DtM_iID_FK")
            ->join(["dtmaterialdetial"=>"dmd"],"ds.DtS_ID_PK=dmd.DtMD_iSectID_FK")
            ->where("d.TD_ID","IN",$gdList)
            ->group("DtMD_sStuff,DtMD_sMaterial")
            ->column("concat(DtMD_sMaterial,',',DtMD_sSpecification) as key_value,group_concat(DtMD_sPartsID) as part");
        $data = [];
        foreach($list as $v){
            $data[$v["ID"]] = [
                "ORDER_NO"=> $v["poNo"],
                "SUPPLIER_CODE"=> self::SUPPLIER_CODE,
                "PROD_ORDER_NO"=> $v["T_Num"],
                "PO_ITEM_ID"=> $v["poItemId"],
                "INFO_TYPE_CODE"=>"T1006",
                "MATFLG"=> $v["MATFLG"],
                "MATMATERIAL"=> $v["MATMATERIAL"],
                "MATSPEC"=> $v["MATSPEC"],
                "BATCH"=> $v["BATCH"],
                "MAT_RECHECK_NO" => $v["MAT_RECHECK_NO"],
                "MAT_QUANTITY" => "10",
                "MAT_SIZE" => "10000",
                "MAT_FACTORY_NAME" => "钢铁有限公司",
                "PART_NUMBER"=> isset($dtList[$v["MATMATERIAL"].','.$v["MATSPEC"]])?$dtList[$v["MATMATERIAL"].','.$v["MATSPEC"]]:'无',
                "TOWER_TYPE"=>"35C-1-DJ3",
                "TOWER_NO"=>"G1#",
                "TRACE_CODE"=> self::SUPPLIER_CODE.$v["poItemNo"]."0000".(str_pad($v["TD_ID"],16,0,STR_PAD_LEFT))."0000",
                "ACQ_TIME"=> "2022-12-09 12:23:12"
            ];
        }
        Db::startTrans();
        try {
            $result = $ProMaterialTestDetailBzModel->where("ID","IN",array_keys($data))->update(["zs_upload"=>1]);
            if($result) Db::commit();
            else{
                Db::rollback();                    
                throw new Exception("失败");
            }
        } catch (PDOException | Exception $e) {
            Db::rollback();
            $return_data["message"] = $e->getMessage();
            return json_encode($return_data);
        }
        $return_data["status"]=0;
        $return_data["message"] = "成功！";
        $return_data["data"] = array_values($data);
        return json_encode($return_data);

    }
}