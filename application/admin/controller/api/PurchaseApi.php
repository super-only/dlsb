<?php
namespace app\admin\controller\api;

use app\admin\model\eipapi\api\CgPurchase;
use app\common\controller\Backend;

set_time_limit(0);
ignore_user_abort(true); 
/**
 * 后台首页
 * @internal
 */
class PurchaseApi extends Backend
{

    protected $noNeedLogin = "*";
    // protected $noNeedRight = ['index', 'logout'];
    // protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new CgPurchase();
        //移除HTML标签
        // $this->request->filter('trim,strip_tags,htmlspecialchars');
    }


    public function index_function()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $url = self::WG_URL;
            $params = [
                "proxyUrl" => "https://eip.sgcc.com.cn/cmc-si/api/supplier-send",
                "header" => [
                    "orisys"=>0,
                    "transno"=>date("YmdHis")."000<".mt_rand(100001,999999).">",
                    "operatetype"=>"QUERY"
                ],
                "result" => [
                    "purchaserHqCode"=>"SGCC",
                    "supplierCode"=>"1000014615",
                    "supplierName"=>"绍兴电力设备有限公司",
                    "pageSize" => 100,
                    "pageNum" => 1,
                    "dataSource"=>"0"
                ]
            ];
            $limit = $this->request->get("limit/d", 100);
            $offset = $this->request->get("offset/d", 0);
            $page = $limit ? intval($offset / $limit) + 1 : 1;
            if ($this->request->has("page")) {
                $page = $this->request->get("page/d", 1);
            }
            $params["result"]["pageNum"] = $page;
            $params["result"]["pageSize"] = $limit;
            $list = api_post($url,$params);
            if($list["status"]!="10000000"){
                $msg = $list["pageInfo"];
                $result = array("total" => $msg["total"], "rows" => $list["data"]);
                return json($result);
            }else $this->error($list["message"]);
        }
        return $this->view->fetch();
    }

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->where("subclassCode","60001")
                ->order($sort, $order)
                // ->select(false);
                ->paginate($limit);
            // pri($list,1);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }
    
    
    public function purchaseApiCopy($pageNum=0)
    {
        if($pageNum==0){
            $count = $this->model->value("count(poItemId) as count");
            $pageNum = ceil($count/100)-1;
        }
        $startTime = $this->model->order("sellerSignTime desc")->value("sellerSignTime");
        $existList = $this->model->column("poItemId,poItemId as value");
        $url = self::WG_URL;
        $params = [
            "proxyUrl" => "https://eip.sgcc.com.cn/cmc-si/api/supplier-send",
            "header" => [
                "orisys"=>0,
                "transno"=>date("YmdHis")."000<".mt_rand(100001,999999).">",
                "operatetype"=>"QUERY"
            ],
            "result" => [
                "purchaserHqCode"=>"SGCC",
                "supplierCode"=>"1000014615",
                "supplierName"=>"绍兴电力设备有限公司",
                "pageSize" => 100,
                "pageNum" => $pageNum,
                // "conValidEndTime"=>date("Y-m-d"),
                "dataSource"=>"0"
            ]
        ];
        // $startTime?$params["result"]["conValidStartTime"] = date("Y-m-d",strtotime("+1 day",strtotime($startTime))):'';
        $result = api_post($url,$params);
        // pri($result,1);
        if($result["status"]=="00000"){
            $msg = $result["pageInfo"];
            $data = $result["data"];
            if(empty($data)){
                sleep(90);
                $this->purchaseApiCopy($pageNum);
            }
            $list = [];
            foreach($data as $k=>$v){
                $list[$v["poItemId"]] = $v;
            }
            $endList = array_diff_key($list,$existList);
            // $this->model->where("1=1")->delete();
            if(!empty($data)) $result = $this->model->allowField(true)->saveAll($endList,false);
            // if($result){
                $cs = ceil($msg["total"]/100);
                if($pageNum<$cs){
                    sleep(90);
                    $this->purchaseApiCopy(++$pageNum);
                }
                else pri($pageNum,1);
            // }else{
            //     sleep(120);
            //     $this->purchaseApiCopy($pageNum);
            // }
        }else return json(["code"=>0,"msg"=>"失败"]);


        //pri($result,1);
        // if($result["status"]!="10000000"){
        //     $msg = $result["pageInfo"];
        //     $data = $result["data"];
        //     $i_arr = [];
        //     if($msg["total"]>50){
        //         $cs = ceil($msg["total"]/50);
        //         for($i=2;$i<=$cs;$i++){
        //             $params["result"]["pageNum"] = $i;
        //             sleep(120);
        //             $result = api_post($url,$params);
        //             $data = array_merge_recursive($data,$result["data"]??[]);
        //         }
        //     }
        //     foreach($data as $v){
        //         $i_arr[$v["poItemId"]] = $v;
        //     }
        //     $this->model->where("1=1")->delete();
        //     if(!empty($data)) $result = $this->model->allowField(true)->saveAll($data,false);
        //     $this->success("成功");
        // }else $this->error($result["message"]);
    }

    public function orderNewsApi($param_url="supplier-so",$operatetype= "ADD",$data = [])
    {
        $return_result = ["code"=>0,"msg"=>"失败"];
        if(empty($data)) return $return_result;
        $url = self::WG_URL;
        $params = [
            "proxyUrl" => "https://eip.sgcc.com.cn/cmc-si/api/".$param_url,
            "header" => [
                "orisys"=>0,
                "transno"=>date("YmdHis")."000<".mt_rand(100001,999999).">",
                "operatetype"=>$operatetype
            ],
            "result" => $data
        ];
        $result = api_post($url,$params);
        if($result["status"]!="10000000") $return_result["code"]=1;
        // if($result["status"]=="00000" or $result["status"]=="20006") $return_result["code"]=1;
        $return_result["msg"] = $result["message"];
        return $return_result;
    }

}
