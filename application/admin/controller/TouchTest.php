<?php

namespace app\admin\controller;

use PDO;
use think\Controller;




/**
 * 套料
 */
class TouchTest
{
    // protected $mateList = [
    //     ["317","Q355B","∠90*7",2674,2],
    //     ["431","Q355B","∠90*7",3924,2],
    //     ["432","Q355B","∠90*7",3924,2],
    //     ["438","Q355B","∠90*7",2904,2],
    //     ["439","Q355B","∠90*7",2904,2],
    //     ["440","Q355B","∠90*7",2296,2],
    //     ["441","Q355B","∠90*7",2296,2],
    //     ["442","Q355B","∠90*7",1529,2],
    //     ["445","Q355B","∠90*7",1274,2],
    //     ["554","Q355B","∠90*7",3435,2],
    //     ["555","Q355B","∠90*7",3434,2],
    //     ["639","Q355B","∠90*7",2828,8],
    //     ["646","Q355B","∠90*7",2529,4],
    //     ["859","Q355B","∠90*7",2295,4],
    //     ["860","Q355B","∠90*7",2295,4],
    //     ["1211","Q355B","∠90*7",2612,16],
    //     ["1409","Q355B","∠90*7",4007,1],
    //     ["1410","Q355B","∠90*7",4007,1],
    //     ["1926","Q355B","∠90*7",4353,1],
    //     ["1927","Q355B","∠90*7",4353,1],
    //     ["2031","Q355B","∠90*7",4007,4],
    //     ["2032","Q355B","∠90*7",4007,4],
    //     ["2033","Q355B","∠90*7",4556,4],
    //     ["2034","Q355B","∠90*7",4556,4]
    // ];
    // protected $kcList = [
    //     ["Q355B","∠90*7",12000,188],
    //     ["Q355B","∠90*7",10000,188],
    //     ["Q355B","∠90*7",9000,101],
    //     ["Q355B","∠90*7",8000,330],
    //     ["Q355B","∠90*7",7000,246],
    // ];

    protected function shAndWbData($x,$y)
    {
        $sh = $wb = 0;
        if($x>=40 and $x<=45) $sh=15;
        if($x>=50 and $x<=63) $wb=100;
        if($x>=70 and $x<=80){
            $sh = 15;
            $wb = 100;
        }
        if($x>=90 and $x<=100 and $y<=8) $wb=100;
        if($x>=90 and $x<=180 and $y<=12){
            $sh = 25;
            $wb = 100;
        }
        if($x>=125 and $x<=200 and $y>12) $sh = 5;
        if($x>=220 and $x<=250) $sh=5;
        return [$sh,$wb];
    }

    //刀数 刀+1
    //刀数损耗 单刀损耗*刀数
    //部件使用长度 部件长度*数量
    //余量 单根长度-刀数损耗-部件使用长度
    //损耗 单根长度-部件使用长度
    //利用率(%) 损耗/总长*100
    //损耗率(%) 100-利用率
    public function main()
    {
        $spec = '∠90*7';
        preg_match_all("/\d+\.?\d*/",$spec,$matches);
        $x = $matches[0][0]?((float)$matches[0][0]):0;
        $y = $matches[0][1]?((float)$matches[0][1]):0;
        [$ds,$wb] = $this->shAndWbData($x,$y);
        // $kcList = [
        //     [7000,246],
        //     [8000,330],
        //     [9000,101],
        //     [10000,188],
        //     [12000,188]
        // ];
        // $kcList = arraySort($kcList, 0, SORT_DESC);

        $kcList = [
            7000 => 246,
            8000 => 330,
            9000 => 101,
            10000 => 188,
            12000 => 188
        ];

        $mateList = [
            ["317",2674,2],
            ["431",3924,2],
            ["432",3924,2],
            ["438",2904,2],
            ["439",2904,2],
            ["440",2296,2],
            ["441",2296,2],
            ["442",1529,2],
            ["445",1274,2],
            ["554",3435,2],
            ["555",3434,2],
            ["639",2828,8],
            ["646",2529,4],
            ["859",2295,4],
            ["860",2295,4],
            ["1211",2612,16],
            ["1409",4007,1],
            ["1410",4007,1],
            ["1926",4353,1],
            ["1927",4353,1],
            ["2031",4007,4],
            ["2032",4007,4],
            ["2033",4556,4],
            ["2034",4556,4]

            // ["1201",4956,1],
            // ["1202",4956,1],
            // ["1201T",4956,1],
            // ["1202T",4956,1],

            // ["50-3101",10692,1],
            // ["3202",10182,1],
            // ["3201T",10182,1],
            // ["3202",10182,1],
            // ["3202T",10182,1],
            // ["3201",10182,1],
            // ["1302",7736,1],
            // ["1301T",7736,1],
            // ["1302T",7736,1],
            // ["1301",7736,1],
            // ["50-2801T",7634,2],
            // ["50-2401",3558,1],
            // ["3301",2944,1],
            // ["3302",2944,1],
            // ["3301T",2944,1],
            // ["3302T",2944,1]
        ];
        $mateList = arraySort($mateList, 1, SORT_DESC);
        // $partList[] = [
        //     "mate" => $v[0].'/'.$v[1]."*".$i,
        //     "part" => [$k,$i],
        //     "sjds" => $sjds,
        //     "dssh" => $dssh,
        //     "bjsycd" => $bjsycd,
        //     "length" => $length,
        //     "nextClosest" => $nextClosest,
        //     "sh" => $sh,
        //     "lyv" => $lyv
        // ];
        $partList = [];
        while(!empty($mateList) and !empty($kcList)){
            krsort($kcList);
            $mateList = arraySort($mateList, 1, SORT_DESC);
            $mateList = array_values($mateList);
            $bcList = $this->_getPart($mateList,$kcList,$ds,$wb);
            // pri($bcList,1);
            if(!empty($bcList)){
                $partList[] = $bcList;
                foreach($bcList["part"] as $pv){
                    $mateList[$pv[0]][2] -= $pv[1];
                    if($mateList[$pv[0]][2]==0) unset($mateList[$pv[0]]);
                }
                $kcList[$bcList['nextClosest']]--;
                if($kcList[$bcList['nextClosest']]==0) unset($kcList[$bcList['nextClosest']]);
            }
        }
        pri($partList,1);
        
        // pri($mateList,1);
        // foreach($kcList as $k=>$v){
        //     [$kcList[$k][4],$kcList[$k][5]] = $this->shAndWbData($v[1]);
        // }
        // foreach($mateList as $){

        // }
    }

    protected function _getPart($mateList,$kcList,$ds,$wb)
    {
        if(empty($mateList) or empty($kcList)) return [];
        // foreach($mateList as $k=>$v){
            $k = 0;
            $v = $mateList[0];
            $cbList = [];
            if(!empty($cbList) and $cbList["lyv"]>97) return $cbList;
            for($i=$v[2];$i>0;$i--){
                $sjds = ($i+1);
                $dssh = $sjds*$ds;
                $bjsycd = $v[1]*$i;
                $length = $bjsycd+$dssh+$wb;
                $nextClosest = $this->getClosest($length,array_keys($kcList));
                if($nextClosest){
                    $sh = $nextClosest-$bjsycd;
                    $shv = round($sh/$nextClosest*100,2);
                    $lyv = 100-$shv;
                    $list = [
                        "mate" => $v[0].'/'.$v[1]."*".$i,
                        "part" => [[$k,$i]],
                        "sjds" => $sjds,
                        "dssh" => $dssh,
                        "bjsycd" => $bjsycd,
                        "length" => $length,
                        "nextClosest" => $nextClosest,
                        "sh" => $sh,
                        "shv" =>$shv,
                        "lyv" => $lyv
                    ];
                    if(empty($cbList)){
                        $cbList = $list;
                    }else if($lyv>$cbList["lyv"]){
                        $cbList = $list;
                    }
                    if($cbList["lyv"]>=97) return $cbList;
                    else{
                        foreach($mateList as $sk=>$sv){
                            if($sk==$k) continue;
                            for($si=$sv[2];$si>0;$si--){
                                $sjds = 1+$i+$si;
                                $dssh = $sjds*$ds;
                                $bjsycd = $v[1]*$i+$sv[1]*$si;
                                $length = $bjsycd+$dssh+$wb;
                                $nextClosest = $this->getClosest($length,array_keys($kcList));
                                if($nextClosest){
                                    $sh = $nextClosest-$bjsycd;
                                    $shv = round($sh/$nextClosest*100,2);
                                    $lyv = 100-$shv;
                                    $list = [
                                        "mate" => $v[0].'/'.$v[1]."*".$i."+".$sv[0].'/'.$sv[1]."*".$si,
                                        "part" => [[$k,$i],[$sk,$si]],
                                        "sjds" => $sjds,
                                        "dssh" => $dssh,
                                        "bjsycd" => $bjsycd,
                                        "length" => $length,
                                        "nextClosest" => $nextClosest,
                                        "sh" => $sh,
                                        "shv" =>$shv,
                                        "lyv" => $lyv
                                    ];
                                    if(empty($cbList)){
                                        $cbList = $list;
                                    }else if($lyv>$cbList["lyv"]){
                                        $cbList = $list;
                                    }
                                    if($cbList["lyv"]>=97) return $cbList;
                                    else{
                                        foreach($mateList as $tk=>$tv){
                                            if($tk==$k or $tk==$sk) continue;
                                            for($ti=$tv[2];$ti>0;$ti--){
                                                $sjds = 1+$i+$si+$ti;
                                                $dssh = $sjds*$ds;
                                                $bjsycd = $v[1]*$i+$sv[1]*$si+$tv[1]*$ti;
                                                $length = $bjsycd+$dssh+$wb;
                                                $nextClosest = $this->getClosest($length,array_keys($kcList));
                                                if($nextClosest){
                                                    $sh = $nextClosest-$bjsycd;
                                                    $shv = round($sh/$nextClosest*100,2);
                                                    $lyv = 100-$shv;
                                                    $list = [
                                                        "mate" => $v[0].'/'.$v[1]."*".$i."+".$sv[0].'/'.$sv[1]."*".$si."+".$tv[0].'/'.$tv[1]."*".$ti,
                                                        "part" => [[$k,$i],[$sk,$si],[$tk,$ti]],
                                                        "sjds" => $sjds,
                                                        "dssh" => $dssh,
                                                        "bjsycd" => $bjsycd,
                                                        "length" => $length,
                                                        "nextClosest" => $nextClosest,
                                                        "sh" => $sh,
                                                        "shv" =>$shv,
                                                        "lyv" => $lyv
                                                    ];
                                                    if(empty($cbList)){
                                                        $cbList = $list;
                                                    }else if($lyv>$cbList["lyv"]){
                                                        $cbList = $list;
                                                    }
                                                    if($cbList["lyv"]>=97) return $cbList;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $cbList;
        // }
    }

    public function getClosest($search, $arr) {
        $closest = 0;
        foreach ($arr as $item) {
           if ((!$closest || abs($search - $closest) > abs($item - $search)) and (($item - $search)>0)){
              $closest = $item;
           }
        }
        return $closest;
     }

    public function getPartList($mateList=[],$kcList=[],$ds=0,$wb=0)
    {
        $list = [];
        $flag = 1;
        if(empty($mateList) or empty($kcList)) return [];
        $count = count($mateList);
        $k=0;
        $flag = 1;
        $cnt = $forcnt = 0;
        while($k<$count){
            $v = $mateList[$k];
            $cnt++;
            pri('cnt:'.$cnt,$v,'');
            for($i=$v[2];$i>0;$i--){
                $sjds = ($i+1);
                $dssh = $sjds*$ds;
                $bjsycd = $v[1]*$i;
                $length = $bjsycd+$dssh+$wb;
                $nextClosest = $this->getClosest($length,array_keys($kcList));
                pri('forcnt:'.$i.";nextClosest:".$nextClosest,'');
                if($nextClosest){
                    $sh = $nextClosest-$bjsycd;
                    $shv = round($sh/$nextClosest*100,2);
                    $lyv = 100-$shv;
                    pri('lyv:'.$lyv,'');
                    if($lyv>=97){
                        $list = [
                            "mate" => $v[0].'/'.$v[1]."*".$i,
                            "part" => [[$k,$i]],
                            "sjds" => $sjds,
                            "dssh" => $dssh,
                            "bjsycd" => $bjsycd,
                            "length" => $length,
                            "nextClosest" => $nextClosest,
                            "sh" => $sh,
                            "lyv" => $lyv
                        ];
                        pri($list,'');
                        foreach($list["part"] as $pv){
                            $mateList[$pv[0]][2] -= $pv[1];
                            if($mateList[$pv[0]][2]==0) unset($mateList[$pv[0]]);
                        }
                        $kcList[$list['nextClosest']]--;
                        if($kcList[$list['nextClosest']]==0) unset($kcList[$list['nextClosest']]);
                        // $flag = 0;
                        continue;
                    }
                }
            }
            $k++;
            // $flag = 1;
        }die;
        pri($list,$mateList,$kcList,1);
        return [$list,$mateList,$kcList];
    }

    // public function getPartList($mateList=[],$kcList=[],$ds=0,$wb=0)
    // {
    //     $partList = [];
    //     $flag = 1;
    //     if(empty($mateList) or empty($kcList)) return [];
    //     while($flag){
    //         $count = count($mateList);
    //         foreach($mateList as $k=>$v){
    //             for($i=$v[2];$i>0;$i--){
    //                 $sjds = ($i+1);
    //                 $dssh = $sjds*$ds;
    //                 $bjsycd = $v[1]*$i;
    //                 $length = $bjsycd+$dssh+$wb;
    //                 $nextClosest = $this->getClosest($length,array_keys($kcList));
    //                 if($nextClosest){
    //                     $sh = $nextClosest-$bjsycd;
    //                     $lyv = round($sh/$nextClosest*100,2);
    //                     if($lyv>=0.97){
    //                         $list = [
    //                             "mate" => $v[0].'/'.$v[1]."*".$i,
    //                             "part" => [[$k,$i]],
    //                             "sjds" => $sjds,
    //                             "dssh" => $dssh,
    //                             "bjsycd" => $bjsycd,
    //                             "length" => $length,
    //                             "nextClosest" => $nextClosest,
    //                             "sh" => $sh,
    //                             "lyv" => $lyv
    //                         ];
    //                         foreach($list["part"] as $pv){
    //                             $mateList[$pv[0]][2] -= $pv[1];
    //                             if($mateList[$pv[0]][2]==0) unset($mateList[$pv[0]]);
    //                         }
    //                         $kcList[$list['nextClosest']]--;
    //                         if($kcList[$list['nextClosest']]==0) unset($kcList[$list['nextClosest']]);
    //                         $flag=0;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     die;
    //     return [$list,$mateList,$kcList];
    // }
    
    // protected $mateList=[];
    // protected $ck=[];
    // protected $oldsl=[];
    // protected $flag=0;
    // protected $newcz="";
    // protected $newgg="";
    // protected $cnt=0;
    // protected $newlyl=0;
    // protected $zsl=0;
    // protected $jl=[];
    // protected $res=[];
    // // public function __construct(
    // //     //材料数组[部件号,材质,规格,长度,数量]
    // //     $mateList = [],
    // //     //该材料的库存量
    // //     $ck = []
    // // )
    // // {
        
    // //     $this->mateList = $this->arraySort($mateList, 3);
    // //     $this->ck = $ck;
    // // }
    
    // public function main($mateList,$ck)
    // {
    //     $this->oldsl = $this->jl = $this->res = [];
    //     $this->flag = $this->cnt = $this->newlyl = $this->zsl = 0;
    //     $this->newcz = $this->newgg = "";
    //     $this->ck = $ck;
    //     $this->mateList = $this->arraySort($mateList, 3);
    //     for($i = 0 ; $i < count($this->mateList) ; $i++) {
    //         $this->oldsl[$i] = $this->mateList[$i][4];
    //     }
    //     //BUG首位减不到0
    //     // $this->mateList[0][4]++;
    //     // $this->oldsl[0]++;

    //     $this->newcz = $this->mateList[0][1];
    //     $this->newgg = $this->mateList[0][2];
    //     $this->zsl = count($this->mateList);
    // // }

    // // public function main()
    // // {
        

    //     for($i = 1 ; $i <= 1000 , $this->flag == 0; $i++){
    //         $this->newlyl = 0.98;
    //         $this->flag = 1;
    //         while($this->flag == 1) {
    //             $this->pd(0,0,1,0,0,-1);
    //             $this->newlyl-=0.01;
    //             if($this->newlyl<0.10){
    //                 break;
    //             } 
    //         }
    //         // if(flag == 1) {
    //         //     cout<<"无法匹配到结果"<<endl; 
    //         //     cout << "部件号 材质 规格 长度 数量" << endl;
    //         //     for(int i = 0 ; i < zsl ; i++) {
    //         //         if(newgg == cs[i].gg && newcz == cs[i].cz)
    //         //             cout << cs[i].bjh << " " << cs[i].cz << " " << cs[i].gg << " " << cs[i].cd << " " << cs[i].sl << " " << endl;
    //         //     }
    //         // }
    //     }
    //     // pri($this->res,$this->mateList,1);
    //     return $this->res;
    // }
    // /**
	//  * 具体套料
	//  * @param  $x 当前位置
	//  * @param  $y 当前值
	//  * @param  $ans 刀片数量
    //  * @param  $pdqg 判断有没有取过
	//  * @param  $pdsl 判断取的数量有没有超过总量
	//  * @param  $tp 特判200的料 
	//  * @return 
	//  */
    // public function pd($x,$y,$ans,$pdqg,$pdsl,$tp)
    // {
        
    //     if ($x >= $this->zsl || max($this->ck) <= $y || $pdqg > 4 || !$this->flag || $pdsl > $this->mateList[$x][3]){
    //         // var_dump($this->res);
    //         return false;
    //     }
    //     $sxczValue = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[6];
    //     if($sxczValue != -1){// && shl < 1.00 && yl >= wbsy(newgg)
    //         $this->flag = 0;
    //         //超过数量进行回退 
    //         for($i = 1;$i < $ans;$i++){//可能浪费时间，但是影响不大 
    //             $this->mateList[$this->weizhi($this->jl[$i]["zhi"])][4]--;
    //             //治标不治本，尝试新开一个变量（判断当前和之前是否一样） 
    //             if($this->mateList[$this->weizhi($this->jl[$i]["zhi"])][4]==0) {
    //                 for($j=1;$j<=$i;$j++){
    //                     $this->mateList[$this->weizhi($this->jl[$i]["zhi"])][4]++;
    //                 }
    //                 $this->flag = 1;
    //                 break;
    //             }
    //         }
    //         if($this->flag==1) return false;
    //         // pri($this->oldsl,$this->mateList[$i][3],1);
    //         // $fl = 1; 
    //         for($i = 0 ;$i <$this->zsl ;$i++) {
    //             if($this->oldsl[$i] != $this->mateList[$i][4]) {
    //                 // if(!$fl) {
    //                 //     $this->res[$this->cnt]['ppjg'] .= " + ";
    //                 // }
    //                 // $this->res[$this->cnt]['ppjg'] = $this->res[$this->cnt]['ppjg'] + " / " + $this->mateList[$i][3] + " * " + ($this->oldsl[$i] - $this->mateList[$i][4]);
    //                 // cs[i].bjh << " / " << cs[i].cd << " * " << (oldsl[i] - cs[i].sl)
    //                 // pri($this->mateList[$i][3],1);
    //                 if(!isset($this->res[$this->cnt]['ppjg'])) $this->res[$this->cnt]['ppjg'] = "";
    //                 $this->res[$this->cnt]['ppjg'] .= '+'.$this->mateList[$i][0] . "/" . $this->mateList[$i][3] . "*" . ($this->oldsl[$i] - $this->mateList[$i][4]);
    //                 // $fl = 0;
    //                 $this->oldsl[$i] = $this->mateList[$i][4];
    //             }
    //         }
    //         //存储数组 
    //         $this->res[$this->cnt]['cz'] = $this->newcz;
    //         $this->res[$this->cnt]['gg'] = $this->newgg;
    //         $this->res[$this->cnt]['cd'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[6];
    //         $this->res[$this->cnt]['ppjg'] = trim($this->res[$this->cnt]['ppjg'],"+");
    //         $this->res[$this->cnt]['dssh'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[0];
    //         $this->res[$this->cnt]['sycd'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[2];
    //         $this->res[$this->cnt]['yl'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[3];
    //         $this->res[$this->cnt]['sh'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[4];
    //         $this->res[$this->cnt++]['shl'] = $this->sxcz($ans,$y,$this->newgg,$this->newlyl)[5];
    //         // pri($this->res,1);
    //         // //结果展示 
    //         // printf("选择的材质%d\n",sxcz(ans,y,newgg,newlyl).ggcz);
    //         // printf("刀数损耗%d\n",dssh);
    //         // printf("使用长度%d\n",sycd);
    //         // printf("余量%d\n",yl);
    //         // printf("损耗%d\n",sh);
    //         // printf("损耗率%.5f\n",shl);
    //         // printf("利用率%.5f\n",lyl); 
    //         // $this->res[] = [

    //         // ];
    //     }
    //     if($this->mateList[$x][4] > $pdsl && $this->mateList[$x][2] == $this->newgg && $this->mateList[$x][1] == $this->newcz && ($tp == -1 || $this->mateList[$x][3] > 1500 && $tp == 2 || $this->mateList[$x][3] <= 1500 && $tp == 1)) {//newcd > y + $this->mateList[x].cd && 
    //         $this->jl[$ans]["zhi"] = $this->mateList[$x][3];
    //         if($this->pdtp($this->mateList[$x][2]) >= 200 && $this->pdtp($this->mateList[$x][2]) <= 300)
    //             $tp = ($this->mateList[$x][3] > 1500) ? 2 : 1;
    //         // pri($this->jl,$ans,$this->mateList,$x,1);
    //         if($this->jl[$ans-1]["zhi"]??0 != $this->mateList[$x][3]){
    //             $this->pd($x,$y+$this->mateList[$x][3],$ans+1,$pdqg+1,$pdsl+1,$tp);
    //         } else {
    //             $this->pd($x,$y+$this->mateList[$x][3],$ans+1,$pdqg,$pdsl+1,$tp);
    //         }
    //     }
    //     $this->pd($x+1,$y,$ans,$pdqg,0,$tp);
    // }

    
    // //判断规格计算切割损耗 
    // public function qgsh($mp="")
    // {
    //     preg_match_all("/\d+\.?\d*/",$mp,$matches);
    //     $x = $matches[0][0]?((float)$matches[0][0]):0;
    //     $y = $matches[0][1]?((float)$matches[0][1]):0;
    //     if($x >= 40 && $x <= 45){
    //         return 15;
    //     }
    //     if($x >= 50 && $x <= 63){
    //         return 0;
    //     }
    //     if($x >= 70 && $x <= 80){
    //         return 15;
    //     }
    //     if($x >= 90 && $x <= 100 && $y <= 8){
    //         return 0;
    //     }
    //     if($x >= 90 && $x <= 180 && $y <= 12){
    //         return 25;
    //     }
    //     if($x >= 125 && $x <= 200 && $y > 12){
    //         return 5;
    //     }
    //     if($x >= 200 && $x <= 250){
    //         return 5;
    //     }
    //     return 0;
    // }

    // //判断规格返回相应的尾部剩余
    // public function wbsy($mp="")
    // {
    //     preg_match_all("/\d+\.?\d*/",$mp,$matches);
    //     $x = $matches[0][0]??0;
    //     $y = $matches[0][1]??0;
    //     if($x >= 50 && $x <= 63){
    //         return 100;
    //     }
    //     if($x >= 70 && $x <= 80){
    //         return 100;
    //     }
    //     if($x >= 90 && $x <= 100 && $y <= 8){
    //         return 100;
    //     }
    //     if($x >= 90 && $x <= 180 && $y <= 12){
    //         return 100;
    //     }
    //     return 0;
    // }

    // //截取规格的角度 
    // public function pdtp($mp="")
    // {
    //     preg_match_all("/\d+\.?\d*/",$mp,$matches);
    //     $x = $matches[0][0]??0;
    //     return $x;
    // }

    // //查询当前值的位置
    // public function weizhi($x)
    // {
    //     for($i = 0;$i < $this->zsl;$i++) {
    //         if($x == $this->mateList[$i][3] && $this->mateList[$i][4] > 0) {
    //             return $i;
    //         }
    //     }
    //     return 0;
    // }
    

    // /**
	//  * 筛选材质 
	//  * @param  $ans 
	//  * @param  $y
    //  * @param  $pdgg
	//  * @param  $pdlyl
	//  * @return 
	//  */
    // public function sxcz($ans,$y,$pdgg='',$pdlyl)
    // {
    //     $dssh = $ans * $this->qgsh($pdgg);
    //     $sycd = $dssh + $y;
        
    //     for($i = 0; $i < count($this->ck) ; $i++) {
    //         $yl = $this->ck[$i] - $sycd;
    //         $sh = $dssh + $yl;
    //         $shl = $sh * 1.0 / $this->ck[$i] * 1.0;
    //         $lyl = 1.0 - $shl;
    //         if($lyl > $pdlyl && $shl < 1.0 && $yl >= $this->wbsy($pdgg)) {
    //             return [$dssh,$sycd,$yl,$sh,$shl,$lyl,$this->ck[$i]];
    //         }
    //     }
    //     return [$dssh,$sycd,$yl,$sh,$shl,$lyl,-1];
    // }

}
