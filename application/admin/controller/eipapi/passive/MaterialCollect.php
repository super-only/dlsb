<?php

namespace app\admin\controller\eipapi\passive;

use app\admin\model\chain\material\CommisionTestDetail;
use app\admin\model\eipapi\passive\FjMaterialTestBz;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use Exception;
use ZipArchive;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class MaterialCollect extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\MaterialCollect
     */
    protected $model = null;
    protected $noNeedLogin = "*";
    protected $fiUpload = [
        0 => "未允许上传",
        1 => "允许上传"
    ];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\eipapi\api\ProductWorkView();
        $this->assignconfig("fiUploadList",$this->fiUpload);
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->field("poNo as ORDER_NO,T_Num as PROD_ORDER_NO,poItemId as PO_ITEM_ID,PT_Num as PROD_WORK_ORDER,cl_upload")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function allowUpload($ids=null)
    {
        $params = $this->model->where("PT_Num",$ids)->where("cl_upload",0)->find();
        if(!$params) return json(["code"=>0,"msg"=>"不存在或者已上传，失败"]);
        // $orderParams = $this->model->where("PT_Num",$ids)->where("eipUpload","1")->find();
        // if(!$orderParams) return json(["code"=>0,"msg"=>"请优先上传工单"]);
        $list = (new \app\admin\model\eipapi\passive\MaterialCollectView())
            ->field("PT_Num,L_Name as MATMATERIAL,IM_Spec as MATSPEC,MT_Standard as INSPSTD,CONCAT(MTD_Temperature,',',MTD_Cjf,',',MTD_Cjs,',',MTD_Cjt) AS MTD_Cj,LuPiHao,MTD_C,MTD_Si,MTD_Mn,MTD_P,MTD_S,MTD_V,MTD_Nb,MTD_Ti,MTD_Cr,MTD_Czsl,MTD_Rm,MTD_Rel,MTD_Percent,MTD_Wq,MTD_Wg,MTD_Mader,mn_deliverdate,IM_Class,MT_Auditor,MT_AuditDate,'绍兴电力设备有限公司' as dw,CTD_QualityNo,bh as MATFLG,MTD_Conclusion as jl")
            ->where("PT_Num",$ids)
            ->select();
        // pri($list,1);
        
        $data = $this->defaultMaterialData($ids);
        $fjList = $this->defaultFjData($ids);
        $matelg = [
            "角钢" => "01001",
            "钢板" => "01002",
            "圆钢" => "01003",
            "钢管" => "01004",
            "紧固件" => "01005",
            "辅助材料" => "01007",
        ];
        $ini = [
            '01001' => [
                ["MTD_C" , "JGTG01" , "C", 0],
                ["MTD_Si" , "JGTG02" , "Si", 0],
                ["MTD_Mn" , "JGTG03" , "Mn", 0],
                ["MTD_P" , "JGTG04" , "P", 0],
                ["MTD_S" , "JGTG05" , "S", 0],
                ["MTD_V" , "JGTG06" , "V", 0],
                ["MTD_Nb" , "JGTG07" , "Nb", 0],
                ["MTD_Ti" , "JGTG08" , "Ti", 0],
                ["MTD_Cr" , "JGTG68" , "Cr", 0],
                ["MTD_Czsl" , "JGTG32" , "层状撕裂检验", 0],
                ["MTD_Rm" , "JGTG09" , "抗拉强度", 0],
                ["MTD_Rel" , "JGTG10" , "屈服强度", 0],
                ["MTD_Percent" , "JGTG11" , "断后伸长率", 0],
                ["MTD_Cj" , "JGTG12" , "冲击检测", 0],
                ["MTD_Wq" , "JGTG13" , "弯曲试验", 0],
                ["MTD_Wg" , "JGTG14" , "尺寸外观", 0],
                ["MTD_Mader" , "JGTG65" , "生产厂家", 0],
                ["mn_deliverdate" , "JGTG66" , "原材料出厂日期", 0]
            ],
            '01002' => [
                ["MTD_C" , "JGTG01" , "C", 0],
                ["MTD_Si" , "JGTG02" , "Si", 0],
                ["MTD_Mn" , "JGTG03" , "Mn", 0],
                ["MTD_P" , "JGTG04" , "P", 0],
                ["MTD_S" , "JGTG05" , "S", 0],
                ["MTD_V" , "JGTG06" , "V", 0],
                ["MTD_Nb" , "JGTG07" , "Nb", 0],
                ["MTD_Ti" , "JGTG08" , "Ti", 0],
                ["MTD_Cr" , "JGTG68" , "Cr", 0],
                ["MTD_Czsl" , "JGTG32" , "层状撕裂检验", 0],
                ["MTD_Rm" , "JGTG09" , "抗拉强度", 0],
                ["MTD_Rel" , "JGTG10" , "屈服强度", 0],
                ["MTD_Percent" , "JGTG11" , "断后伸长率", 0],
                ["MTD_Cj" , "JGTG12" , "冲击检测", 0],
                ["MTD_Wq" , "JGTG13" , "弯曲试验", 0],
                ["MTD_Wg" , "JGTG14" , "尺寸外观", 0],
                ["MTD_Mader" , "JGTG65" , "生产厂家", 0],
                ["mn_deliverdate" , "JGTG66" , "原材料出厂日期", 0]
            ],
            '01005' => [
                ["LuPiHao" , "JGTG33" , "抽检单号", 0],
                ["MTD_Mader" , "JGTG34" , "生产厂家", 0],
                ["IM_Class" , "JGTG35" , "品名", 0],
                ["MATMATERIAL" , "JGTG36" , "等级", 0],
                ["MATSPEC" , "JGTG37" , "规格", 0],
                ["mn_deliverdate" , "JGTG67" , "出厂日期", 0],
                ["dw" , "JGTG38" , "检测单位", 0],
                ["jl" , "JGTG15" , "检测结论", 0],
            ],
            '01007' => [
                ["type" , "JGTG39" , "辅材类型", 0],
                ["MTD_Mader" , "JGTG40" , "生产厂家", 0],
                ["MATMATERIAL" , "JGTG41" , "材质", 0],
                ["MATSPEC" , "JGTG42" , "规格", 0],
                ["dw" , "JGTG43" , "检测单位", 0],
                ["jl" , "JGTG44" , "检测结论", 0],
            ]
        ];
        $delList = [];
        foreach($list as $k=>$v){
            $list[$k] = $v->toArray();
            // $v["MATFLG"] = $list[$k]["MATFLG"] = in_array($v["IM_Class"],array_keys($matelg))?$matelg[$v["IM_Class"]]:"01007";
            if($list[$k]["MATFLG"]=="01007") $v["type"] = $list[$k]["type"] = $v["IM_Class"];
            else $v["type"] = $list[$k]["type"] = "";
            if(($list[$k]["MATFLG"]=="01001" or $list[$k]["MATFLG"]=="01002") and $v["MTD_C"]<=0) continue;
            // $ini_data = in_array($v["IM_Class"],array_keys($matelg))?$ini["jg"]:$ini["fl"];
            foreach($ini[$v["MATFLG"]] as $kk=>$vv){
                if($vv[1] == "JGTG12" and $v[$vv[0]] == ",0,0,0") continue;
                if(!$v[$vv[0]]) continue;
                $data[] = [
                    "PT_Num" => $ids,
                    "MATFLG" => $v["MATFLG"],
                    "MATMATERIAL" => $v["MATMATERIAL"],
                    "MATSPEC" => $v["MATSPEC"],
                    "INSPSTD" => $v["INSPSTD"],
                    "BATCH" => $v["LuPiHao"],
                    "MAT_RECHECK_NO" => $v["LuPiHao"],
                    "ITEM_DETAIL" => $vv[2],
                    "RULE_CODE" => $vv[1],
                    "PROD_VALUE" => "".$v[$vv[0]],
                    "DETECTION_TIME" => $v["MT_AuditDate"],
                    "DETECTION_USER" => $v["MT_Auditor"],
                    "TYPE" => ""
                ];
            }
            if(in_array($v["MATFLG"],["01001","01002"])){
                if(!$v["CTD_QualityNo"]) return json(["code"=>0,"msg"=>$v["LuPiHao"]."该炉批号缺少质量证明书号，请及时补充后重新上传"]);
                $fjList[] = [
                    "PT_Num" => $ids,
                    "BELONGTO_CODE" => "JGTS01",
                    "BELONGTO_ID" => "无",
                    "BATCH" => $v["LuPiHao"],
                    "MAT_RECHECK_NO" => $v["LuPiHao"],
                    "FILE_NO" => "无",
                    "FILE_TYPE"=> "1",
                    "filename" => "化学检测报告.pdf",
                    "fileContent" => "rcfjbg/".$v["LuPiHao"].'.pdf',
                ];
                $fjList[] = [
                    "PT_Num" => $ids,
                    "BELONGTO_CODE" => "JGTS05",
                    "BELONGTO_ID" => "无",
                    "BATCH" => $v["LuPiHao"],
                    "MAT_RECHECK_NO" => $v["LuPiHao"],
                    "FILE_NO" => $v["LuPiHao"],
                    "FILE_TYPE"=> "3",
                    "filename" => "入厂复检报告.pdf",
                    "fileContent" => "rcfjbg/".$v["LuPiHao"].'.pdf',
                ];
                $fjList[] = [
                    "PT_Num" => $ids,
                    "BELONGTO_CODE" => "JGTS49",
                    "BELONGTO_ID" => "无",
                    "BATCH" => $v["LuPiHao"],
                    "MAT_RECHECK_NO" => $v["LuPiHao"],
                    "FILE_NO" => $v["CTD_QualityNo"],
                    "FILE_TYPE"=> "2",
                    "filename" => "出厂质量证明书.pdf",
                    "fileContent" => "cczlzms/".$v["CTD_QualityNo"].'.pdf',
                ];
            }else if($v["MATFLG"]=="01005"){
                $fjList[] = [
                    "PT_Num" => $ids,
                    "BELONGTO_CODE" => "JGTS18",
                    "BELONGTO_ID" => "无",
                    "BATCH" => $v["LuPiHao"],
                    "MAT_RECHECK_NO" => $v["LuPiHao"],
                    "FILE_NO" => $v["LuPiHao"],
                    "FILE_TYPE"=> "2",
                    "filename" => "出厂质量证明书.pdf",
                    "fileContent" => "cczlzms/".$v["CTD_QualityNo"].'.pdf',
                ];
                $fjList[] = [
                    "PT_Num" => $ids,
                    "BELONGTO_CODE" => "JGTS06",
                    "BELONGTO_ID" => "无",
                    "BATCH" => $v["LuPiHao"],
                    "MAT_RECHECK_NO" => $v["LuPiHao"],
                    "FILE_NO" => "无",
                    "FILE_TYPE"=> "1",
                    "filename" => "检测.pdf",
                    "fileContent" => "zjbg/".$v["LuPiHao"].'.pdf',
                ];
            }else if($v["MATFLG"]=="01007"){
                $fjList[] = [
                    "PT_Num" => $ids,
                    "BELONGTO_CODE" => "JGTS24",
                    "BELONGTO_ID" => "无",
                    "BATCH" => $v["LuPiHao"],
                    "MAT_RECHECK_NO" => $v["LuPiHao"],
                    "FILE_NO" => $v["LuPiHao"],
                    "FILE_TYPE"=> "2",
                    "filename" => "出厂质量证明书.pdf",
                    "fileContent" => "cczlzms/".$v["CTD_QualityNo"].'.pdf',
                ];
                $fjList[] = [
                    "PT_Num" => $ids,
                    "BELONGTO_CODE" => "JGTS28",
                    "BELONGTO_ID" => "无",
                    "BATCH" => $v["LuPiHao"],
                    "MAT_RECHECK_NO" => $v["LuPiHao"],
                    "FILE_NO" => "无",
                    "FILE_TYPE"=> "1",
                    "filename" => "检测.pdf",
                    "fileContent" => "zjbg/".$v["LuPiHao"].'.pdf',
                ];
            }
        }
        $fjModel = new \app\admin\model\eipapi\passive\FjMaterialTestBz();
        $bzModel = new \app\admin\model\chain\material\ProMaterialTestDetailBz();
        $result = false;
        Db::startTrans();
        try {
            (new \app\admin\model\chain\lofting\ProduceTask([],$params["sect_field"]))->where("PT_Num",$ids)->update(["cl_upload"=>1]);
            $result = $bzModel->allowField(true)->saveAll($data);
            $fjModel->allowField(true)->saveAll($fjList);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $this->success("允许上传成功");
        } else {
            $this->error("允许上传失败");
        }
    }

    public function detail($ids=null)
    {
        $list = (new \app\admin\model\eipapi\passive\MaterialCollectView())
            ->field("PT_Num,L_Name as MATMATERIAL,IM_Spec as MATSPEC,MT_Standard as INSPSTD,CONCAT(MTD_Temperature,',',MTD_Cjf,',',MTD_Cjs,',',MTD_Cjt) AS MTD_Cj,LuPiHao,MTD_C,MTD_Si,MTD_Mn,MTD_P,MTD_S,MTD_V,MTD_Nb,MTD_Ti,MTD_Cr,MTD_Czsl,MTD_Rm,MTD_Rel,MTD_Percent,MTD_Wq,MTD_Wg,MTD_Mader,mn_deliverdate,IM_Class,bh as MATFLG,MTD_Conclusion")
            ->where("PT_Num",$ids)
            ->select();
        $data = [];
        $matelg = [
            "角钢" => "01001",
            "钢板" => "01002",
            "圆钢" => "01003",
            "钢管" => "01004",
            "紧固件" => "01005",
            "辅助材料" => "01007",
        ];
        foreach($list as $k=>$v){
            if(($v["MATFLG"]=="01001" or $v["MATFLG"]=="01002") and $v["MTD_C"]<=0) continue;
            $data[$k] = $v->toArray();
            if($data[$k]["MATFLG"]=="01007") $data[$k]["type"] = $v["IM_Class"];
            else $data[$k]["type"] = "";
        }
        $this->assignconfig("list",array_values($data));
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function partImport($ids=null)
    {
        if ($this->request->isPost()) {
            $filename = $this->request->post("filename");
            if(!$filename) $this->error("不存在文件名，请稍后重试");
            $zip = new ZipArchive;
            // $zip->open(ROOT_PATH . 'public' .$filename,\ZipArchive::CREATE);
            // 是否存在压缩包
            if ($zip->open(ROOT_PATH . 'public' .$filename, \ZipArchive::CREATE) === true){
                //压缩包解压到指定位置
                $zip->extractTo(ROOT_PATH . 'public' . DS .'zlfiles' . DS .'rcfjbg');
                $name = $zip->getNameIndex(0);
                $name = substr($name,0,strpos($name, '/'));
                $zip->close();
                $oldfieldname = ROOT_PATH . 'public' . DS .'zlfiles' . DS .'rcfjbg' .DS.$name;
                $newfieldname = ROOT_PATH . 'public' . DS .'zlfiles' . DS .'rcfjbg';
                // pri($oldfieldname,$newfieldname,1);
                if(!is_dir($newfieldname)){
                    mkdir($newfieldname,0777);
                }
                clearstatcache($newfieldname);
                $fileList = scandir($oldfieldname);
                $saveData = [];
                foreach($fileList as $v){
                    if(substr($v,-4)=='.pdf'){
                        // $partName = substr($v,0,-4);
                        $saveData[$v] = $v;
                        if(file_exists($newfieldname.DS.$v)) unlink($newfieldname.DS.$v);
                        rename($oldfieldname.DS.$v,$newfieldname.DS.$v);
                    }else if($v=="." or $v=="..") continue;
                    if(is_file($oldfieldname.DS.$v)) unlink($oldfieldname.DS.$v);
                }
                rmdir($oldfieldname);
                
            }
            $zjbg = ROOT_PATH . 'public' . DS .'zlfiles' . DS .'zjbg';
            foreach($saveData as $v){
                $source = $newfieldname . DS .$v;
                $dest = $zjbg . DS .$v;
                copy($source,$dest);
            }
            $this->success();
        }
        return $this->view->fetch();
    }

    

    protected function defaultMaterialData($PT_Num='')
    {
        $data = [];
        if(!$PT_Num) return $data;
        $defaultField = [
            "MATFLG",
            "MATMATERIAL",
            "MATSPEC",
            "INSPSTD",
            "BATCH",
            "MAT_RECHECK_NO",
            "ITEM_DETAIL",
            "RULE_CODE",
            "PROD_VALUE",
            "DETECTION_TIME",
            "DETECTION_USER"
        ];
        $defaultData = [
            ["01005", "8.8","M24*105","GB/T700-2006","L220829003","L220829003","检测结论","JGTG15","合格","2022-01-31 9:26:11","阮观祥"],
            ["01005","8.8","M24*105","GB/T700-2006","L220829003","L220829003","检测单位","JGTG38","绍兴电力设备有限公司","2022-01-31 9:26:12","阮观祥"],
            ["01005","8.8","M24*105","GB/T700-2006","L220829003","L220829003","出厂日期","JGTG67","2022-01-10 9:21:00","2022-01-31 9:26:13","阮观祥"],
            ["01005","8.8","M24*105","GB/T700-2006","L220829003","L220829003","规格","JGTG37","M24*105","2022-01-31 9:26:14","阮观祥"],
            ["01005","8.8","M24*105","GB/T700-2006","L220829003","L220829003","等级","JGTG36","8.8","2022-01-31 9:26:15","阮观祥"],
            ["01005","8.8","M24*105","GB/T700-2006","L220829003","L220829003","品名","JGTG35","热镀锌防卸螺栓","2022-01-31 9:26:16","阮观祥"],
            ["01005","8.8","M24*105","GB/T700-2006","L220829003","L220829003","生产厂家","JGTG34","华佳","2022-01-31 9:26:17","阮观祥"],
            ["01005","8.8","M24*105","GB/T700-2006","L220829003","L220829003","抽检单号","JGTG33","L220829003","2022-01-31 9:26:18","阮观祥"],
            ["01005","8.8","M24*95","GB/T700-2006","L220807003","L220807003","检测结论","JGTG15","合格","2022-01-31 9:26:19","阮观祥"],
            ["01005","8.8","M24*95","GB/T700-2006","L220807003","L220807003","检测单位","JGTG38","绍兴电力设备有限公司","2022-01-31 9:26:20","阮观祥"],
            ["01005","8.8","M24*95","GB/T700-2006","L220807003","L220807003","出厂日期","JGTG67","2022-01-10 9:21:00","2022-01-31 9:26:21","阮观祥"],
            ["01005","8.8","M24*95","GB/T700-2006","L220807003","L220807003","规格","JGTG37","M24*95","2022-01-31 9:26:22","阮观祥"],
            ["01005","8.8","M24*95","GB/T700-2006","L220807003","L220807003","等级","JGTG36","8.8","2022-01-31 9:26:23","阮观祥"],
            ["01005","8.8","M24*95","GB/T700-2006","L220807003","L220807003","品名","JGTG35","热镀锌防卸螺栓","2022-01-31 9:26:24","阮观祥"],
            ["01005","8.8","M24*95","GB/T700-2006","L220807003","L220807003","生产厂家","JGTG34","华佳","2022-01-31 9:26:25","阮观祥"],
            ["01005","8.8","M24*95","GB/T700-2006","L220807003","L220807003","抽检单号","JGTG33","L220807003","2022-01-31 9:26:26","阮观祥"],
            ["01005","8.8","M24*85","GB/T700-2006","L220805003","L220805003","检测结论","JGTG15","合格","2022-01-31 9:26:27","阮观祥"],
            ["01005","8.8","M24*85","GB/T700-2006","L220805003","L220805003","检测单位","JGTG38","绍兴电力设备有限公司","2022-01-31 9:26:28","阮观祥"],
            ["01005","8.8","M24*85","GB/T700-2006","L220805003","L220805003","出厂日期","JGTG67","2022-01-10 9:21:00","2022-01-31 9:26:29","阮观祥"],
            ["01005","8.8","M24*85","GB/T700-2006","L220805003","L220805003","规格","JGTG37","M24*85","2022-01-31 9:26:30","阮观祥"],
            ["01005","8.8","M24*85","GB/T700-2006","L220805003","L220805003","等级","JGTG36","8.8","2022-01-31 9:26:31","阮观祥"],
            ["01005","8.8","M24*85","GB/T700-2006","L220805003","L220805003","品名","JGTG35","热镀锌防卸螺栓","2022-01-31 9:26:32","阮观祥"],
            ["01005","8.8","M24*85","GB/T700-2006","L220805003","L220805003","生产厂家","JGTG34","华佳","2022-01-31 9:26:33","阮观祥"],
            ["01005","8.8","M24*85","GB/T700-2006","L220805003","L220805003","抽检单号","JGTG33","L220805003","2022-01-31 9:26:34","阮观祥"],
            ["01005","8.8","M24*65","GB/T700-2006","L220805004","L220805004","检测结论","JGTG15","合格","2022-01-31 9:26:27","阮观祥"],
            ["01005","8.8","M24*65","GB/T700-2006","L220805004","L220805004","检测单位","JGTG38","绍兴电力设备有限公司","2022-01-31 9:26:28","阮观祥"],
            ["01005","8.8","M24*65","GB/T700-2006","L220805004","L220805004","出厂日期","JGTG67","2022-01-10 9:21:00","2022-01-31 9:26:29","阮观祥"],
            ["01005","8.8","M24*65","GB/T700-2006","L220805004","L220805004","规格","JGTG37","M24*65","2022-01-31 9:26:30","阮观祥"],
            ["01005","8.8","M24*65","GB/T700-2006","L220805004","L220805004","等级","JGTG36","8.8","2022-01-31 9:26:31","阮观祥"],
            ["01005","8.8","M24*65","GB/T700-2006","L220805004","L220805004","品名","JGTG35","普通单帽螺栓","2022-01-31 9:26:32","阮观祥"],
            ["01005","8.8","M24*65","GB/T700-2006","L220805004","L220805004","生产厂家","JGTG34","华佳","2022-01-31 9:26:33","阮观祥"],
            ["01005","8.8","M24*65","GB/T700-2006","L220805004","L220805004","抽检单号","JGTG33","L220805004","2022-01-31 9:26:34","阮观祥"],
            ["01005","8.8","M24*70","GB/T700-2006","L220805005","L220805005","检测结论","JGTG15","合格","2022-01-31 9:26:27","阮观祥"],
            ["01005","8.8","M24*70","GB/T700-2006","L220805005","L220805005","检测单位","JGTG38","绍兴电力设备有限公司","2022-01-31 9:26:28","阮观祥"],
            ["01005","8.8","M24*70","GB/T700-2006","L220805005","L220805005","出厂日期","JGTG67","2022-01-10 9:21:00","2022-01-31 9:26:29","阮观祥"],
            ["01005","8.8","M24*70","GB/T700-2006","L220805005","L220805005","规格","JGTG37","M24*70","2022-01-31 9:26:30","阮观祥"],
            ["01005","8.8","M24*70","GB/T700-2006","L220805005","L220805005","等级","JGTG36","8.8","2022-01-31 9:26:31","阮观祥"],
            ["01005","8.8","M24*70","GB/T700-2006","L220805005","L220805005","品名","JGTG35","普通单帽螺栓","2022-01-31 9:26:32","阮观祥"],
            ["01005","8.8","M24*70","GB/T700-2006","L220805005","L220805005","生产厂家","JGTG34","华佳","2022-01-31 9:26:33","阮观祥"],
            ["01005","8.8","M24*70","GB/T700-2006","L220805005","L220805005","抽检单号","JGTG33","L220805005","2022-01-31 9:26:34","阮观祥"],
            ["01005","8.8","M24*130","GB/T700-2006","L220805006","L220805006","检测结论","JGTG15","合格","2022-01-31 9:26:27","阮观祥"],
            ["01005","8.8","M24*130","GB/T700-2006","L220805006","L220805006","检测单位","JGTG38","绍兴电力设备有限公司","2022-01-31 9:26:28","阮观祥"],
            ["01005","8.8","M24*130","GB/T700-2006","L220805006","L220805006","出厂日期","JGTG67","2022-01-10 9:21:00","2022-01-31 9:26:29","阮观祥"],
            ["01005","8.8","M24*130","GB/T700-2006","L220805006","L220805006","规格","JGTG37","M24*130","2022-01-31 9:26:30","阮观祥"],
            ["01005","8.8","M24*130","GB/T700-2006","L220805006","L220805006","等级","JGTG36","8.8","2022-01-31 9:26:31","阮观祥"],
            ["01005","8.8","M24*130","GB/T700-2006","L220805006","L220805006","品名","JGTG35","普通单帽螺栓","2022-01-31 9:26:32","阮观祥"],
            ["01005","8.8","M24*130","GB/T700-2006","L220805006","L220805006","生产厂家","JGTG34","华佳","2022-01-31 9:26:33","阮观祥"],
            ["01005","8.8","M24*130","GB/T700-2006","L220805006","L220805006","抽检单号","JGTG33","L220805006","2022-01-31 9:26:34","阮观祥"],
            ["01007","焊丝","SJ-50-1.2","GB/T470-2008","HS23113","HS23113","检测结论","JGTG44","合格","2022-01-31 9:26:35","阮观祥"],
            ["01007","焊丝","SJ-50-1.2","GB/T470-2008","HS23113","HS23113","检测单位","JGTG43","绍兴电力设备有限公司","2022-01-31 9:26:36","阮观祥"],
            ["01007","焊丝","SJ-50-1.2","GB/T470-2008","HS23113","HS23113","规格","JGTG42","SJ-50-1.2","2022-01-31 9:26:37","阮观祥"],
            ["01007","焊丝","SJ-50-1.2","GB/T470-2008","HS23113","HS23113","材质","JGTG41","焊丝","2022-01-31 9:26:38","阮观祥"],
            ["01007","焊丝","SJ-50-1.2","GB/T470-2008","HS23113","HS23113","生产厂家","JGTG40","上海大西洋","2022-01-31 9:26:39","阮观祥"],
            ["01007","焊丝","SJ-50-1.2","GB/T470-2008","HS23113","HS23113","辅材类型","JGTG39","焊丝","2022-01-31 9:26:40","阮观祥"],
            ["01007","锌锭","锌锭","GB/T470-2008","DLYL-JJ2223","DLYL-JJ2223","检测结论","JGTG44","合格","2022-01-31 9:26:41","阮观祥"],
            ["01007","锌锭","锌锭","GB/T470-2008","DLYL-JJ2223","DLYL-JJ2223","检测单位","JGTG43","绍兴电力设备有限公司","2022-01-31 9:26:42","阮观祥"],
            ["01007","锌锭","锌锭","GB/T470-2008","DLYL-JJ2223","DLYL-JJ2223","规格","JGTG42","锌锭","2022-01-31 9:26:43","阮观祥"],
            ["01007","锌锭","锌锭","GB/T470-2008","DLYL-JJ2223","DLYL-JJ2223","材质","JGTG41","锌锭","2022-01-31 9:26:44","阮观祥"],
            ["01007","锌锭","锌锭","GB/T470-2008","DLYL-JJ2223","DLYL-JJ2223","生产厂家","JGTG40","陕西东岭","2022-01-31 9:26:45","阮观祥"],
            ["01007","锌锭","锌锭","GB/T470-2008","DLYL-JJ2223","DLYL-JJ2223","辅材类型","JGTG39","锌锭","2022-01-31 9:26:46","阮观祥"]
        ];
        foreach($defaultData as $k=>$v){
            $data[$k]["PT_Num"] = $PT_Num;
            foreach($defaultField as $kk=>$vv){
                $data[$k][$vv] = $v[$kk];
            }
        }
        return $data;
    }

    protected function defaultFjData($PT_Num='')
    {
        $data = [];
        if(!$PT_Num) return $data;
        $defaultField = [
            "BELONGTO_CODE",
            "BATCH",
            "MAT_RECHECK_NO",
            "FILE_NO",
            "FILE_TYPE",
            "filename",
            "fileContent"
        ];
        $defaultData = [
            ["JGTS06","L220829003","L220829003","无","1","检测.pdf","zjbg/L220829003.pdf"],
            ["JGTS18","L220829003","L220829003","L220829003","2","出厂质量证明书.pdf","cczlzms/L220829003.pdf"],
            ["JGTS06","L220805003","L220805003","无","1","检测.pdf","zjbg/L220805003.pdf"],
            ["JGTS18","L220805003","L220805003","L220805003","2","出厂质量证明书.pdf","cczlzms/L220805003.pdf"],
            ["JGTS06","L220807003","L220807003","无","1","检测.pdf","zjbg/L220807003.pdf"],
            ["JGTS18","L220807003","L220807003","L220807003","2","出厂质量证明书.pdf","cczlzms/L220807003.pdf"],
            ["JGTS06","L220807003","L220807003","无","1","检测.pdf","zjbg/L220807003.pdf"],
            ["JGTS18","L220805004","L220805004","L220805004","2","出厂质量证明书.pdf","cczlzms/L220805004.pdf"],
            ["JGTS06","L220805004","L220805004","无","1","检测.pdf","zjbg/L220805004.pdf"],
            ["JGTS18","L220805005","L220805005","L220805005","2","出厂质量证明书.pdf","cczlzms/L220805005.pdf"],
            ["JGTS06","L220805005","L220805005","无","1","检测.pdf","zjbg/L220805005.pdf"],
            ["JGTS18","L220805006","L220805006","L220805006","2","出厂质量证明书.pdf","cczlzms/L220805006.pdf"],
            ["JGTS06","L220805006","L220805006","无","1","检测.pdf","zjbg/L220805006.pdf"],
            ["JGTS28","HS23113","HS23113","无","1","检测.pdf","zjbg/HS23113.pdf"],
            ["JGTS24","HS23113","HS23113","HS23113","2","出厂质量证明书.pdf","cczlzms/HS23113.pdf"],
            ["JGTS28","DLYL-JJ2223","DLYL-JJ2223","无","1","检测.pdf","zjbg/DLYL-JJ2223.pdf"],
            ["JGTS24","DLYL-JJ2223","DLYL-JJ2223","DLYL-JJ2223","2","出厂质量证明书.pdf","cczlzms/DLYL-JJ2223.pdf"]
        ];
        foreach($defaultData as $k=>$v){
            foreach($defaultField as $kk=>$vv){
                $data[$k][$vv] = $v[$kk];
            }
            $data[$k]["PT_Num"] = $PT_Num;
        }
        return $data;
    }

    public function updateMaterialNews()
    {
        return false;
        $materialTestDetailModel = new \app\admin\model\quality\experiment\MaterialTestDetail();
        $list = $materialTestDetailModel->alias("mtd")
            ->join(["inventorymaterial"=>"im"],"mtd.IM_Num=im.IM_Num")
            ->field("mtd.*")
            ->where([
                // "CT_Num" => [">","SJ22"],
                "CT_Num" => ["<","SJ2210"],
                "IM_Class" => ["IN",["角钢","板件"]]
            ])->select();
        $list = $list?collection($list)->toArray():[];
        //各钢板角钢有值的最新一个
        $existNews = DB::QUERY("select *,CONCAT(IM_Num,',',L_Name) AS new_name from materialtestdetail as t where t.MTD_ID IN (select substring_index(GROUP_CONCAT(MTD_ID order by MTD_ID DESC),',',1) from materialtestdetail where CT_Num>='SJ2210' and MTD_C>0 GROUP BY CONCAT(IM_Num,',',L_Name))");
        $existNewsList = [];
        $field = [
            "MTD_C","MTD_Si","MTD_Mn","MTD_P","MTD_S","MTD_V","MTD_Nb","MTD_Ti","MTD_Cr","MTD_Czsl","MTD_Rm","MTD_Rel","MTD_Percent","MTD_Temperature","MTD_Cjf","MTD_Cjs","MTD_Cjt","MTD_Wq","MTD_Wg","MTD_Conclusion","Memo"
        ];
        foreach($existNews as $v){
            foreach($field as $vv){
                $existNewsList[$v["new_name"]][$vv] = $v[$vv];
            }
        }

        foreach($list as $k=>$v){
            if(isset($existNewsList[$v["IM_Num"].','.$v["L_Name"]])){
                $key = $v["IM_Num"].','.$v["L_Name"];
                foreach($field as $vv){
                    $list[$k][$vv] = $existNewsList[$key][$vv];
                }
            }
        }
        $result = $materialTestDetailModel->allowField(true)->saveAll($list);
    }

    public function updateMaterialLoss()
    {
        return false;
        $materialTestDetailModel = new \app\admin\model\quality\experiment\MaterialTestDetail();
        $list = $materialTestDetailModel->alias("mtd")
            ->join(["inventorymaterial"=>"im"],"mtd.IM_Num=im.IM_Num")
            ->field("mtd.*,im.IM_Spec")
            ->where([
                // "CT_Num" => [">","SJ22"],
                "MTD_C" => ["<=","0"],
                "IM_Class" => ["IN",["角钢","钢板"]]
            ])->select();
        $list = $list?collection($list)->toArray():[];
        $spec = $specList = [];
        foreach($list as $k=>$v){
            $spec[$v["IM_Spec"]] = $v["IM_Spec"];
        }
        $existNews = DB::QUERY("select substring_index(GROUP_CONCAT(MTD_ID order by MTD_ID DESC),',',1),im.IM_Spec,mtd.IM_Num,MTD_C,MTD_Si,MTD_Mn,MTD_P,MTD_S,MTD_V,MTD_Nb,MTD_Ti,MTD_Cr,MTD_Czsl,MTD_Rm,MTD_Rel,MTD_Percent,MTD_Temperature,MTD_Cjf,MTD_Cjs,MTD_Cjt,MTD_Wq,MTD_Wg,CTD_Weight from materialtestdetail mtd inner join inventorymaterial im on mtd.IM_Num=im.IM_Num where CT_Num>='SJ2210' and MTD_C>0 AND im.IM_Spec in ('".implode("','",$spec)."') GROUP BY IM_Spec");
        foreach($existNews as $k=>$v){
            $specList[$v["IM_Spec"]] = $v;
        }
        $field = ["MTD_C","MTD_Si","MTD_Mn","MTD_P","MTD_S","MTD_V","MTD_Nb","MTD_Ti","MTD_Cr","MTD_Czsl","MTD_Rm","MTD_Rel","MTD_Percent","MTD_Temperature","MTD_Cjf","MTD_Cjs","MTD_Cjt","MTD_Wq","MTD_Wg"];
        foreach($list as $k=>$v){
            if(isset($specList[$v["IM_Spec"]])){
                foreach($field as $vv){
                    $list[$k][$vv] = $specList[$v["IM_Spec"]][$vv];
                }
            }
        }
        $result = $materialTestDetailModel->allowField(true)->saveAll($list);
    }

    public function updateCommision()
    {
        $arr = [];
        $list = DB::Query("select substring_index(GROUP_CONCAT(CTD_ID order by CTD_ID DESC),',',1),CT_Num,CTD_Spec,L_Name,CTD_QualityNo from commisiontestdetail where CTD_QualityNo<>'' GROUP BY CONCAT(CTD_Spec,L_Name)");
        foreach($list as $k=>$v){
            $arr[$v["CTD_Spec"]][$v["L_Name"]] = $v["CTD_QualityNo"];
        }
        $noList = DB::QUERY("select CTD_ID,CTD_Spec,L_Name from commisiontestdetail where CTD_QualityNo=''");
        $saveList = [];
        foreach($noList as $k=>$v){
            if(isset($arr[$v["CTD_Spec"]])){
                if(isset($arr[$v["CTD_Spec"]][$v["L_Name"]])){
                    $saveList[$v["CTD_ID"]] = [
                        "CTD_ID" => $v["CTD_ID"],
                        "CTD_QualityNo" => $arr[$v["CTD_Spec"]][$v["L_Name"]]
                    ];
                }else $saveList[$v["CTD_ID"]] = [
                    "CTD_ID" => $v["CTD_ID"],
                    "CTD_QualityNo" => current($arr[$v["CTD_Spec"]])
                ];
            }
        }
        $result = (new \app\admin\model\chain\material\CommisionTestDetail())->allowField(true)->saveAll($saveList);
        PRI(count($result),1);
    }

    public function updateNoC()
    {
        $bzModel = new FjMaterialTestBz();
        $list = $bzModel->where([
            "BELONGTO_CODE" => ["=","JGTS49"],
            "upload" => ["=",2]
        ])->order("ID desc")->limit(0,100)->select();
        $commisionTestDetailModel = new CommisionTestDetail();
        $noList = $ctdList = [];
        foreach($list as $k=>$v){
            $file = ROOT_PATH . DS . 'public' . DS . 'zlfiles' . DS . $v["fileContent"];
            if(!is_file($file)){
                $find = $commisionTestDetailModel->where([
                    "CTD_QualityNo" => ["=",$v["FILE_NO"]],
                    "LuPiHao" => ["=",$v["BATCH"]]
                ])->find();
                if($find){
                    $alikeFind = $commisionTestDetailModel->where([
                        "CTD_Spec" => ["=",$find["CTD_Spec"]],
                        "L_Name" => ["=",$find["L_Name"]]
                    ])->order("CTD_ID DESC")->select();
                    if($alikeFind){
                        foreach($alikeFind as $kk=>$vv){
                            $alikeFile = ROOT_PATH . DS . 'public' . DS . 'zlfiles' . DS . 'cczlzms' . DS . $vv["CTD_QualityNo"].".pdf";
                            if(is_file($alikeFile)){
                                $ctdList[] = [
                                    "CTD_ID" => $find["CTD_ID"],
                                    "CTD_QualityNo" => $vv["CTD_QualityNo"]
                                ];
                                $noList[] = [
                                    "ID" => $v["ID"],
                                    "BATCH" => $v["BATCH"],
                                    "FILE_NO" => $vv["CTD_QualityNo"],
                                    "fileContent" => "cczlzms/".$vv["CTD_QualityNo"].".pdf",
                                    "upload" => 0
                                ];
                                break;
                            }
                        }
                    }

                }
            }
        }
        $result = $commisionTestDetailModel->saveAll($ctdList);
        $bzresult = $bzModel->saveAll($noList);
        pri(count($result),count($bzresult),1);
    }
}
