<?php

namespace app\admin\controller\eipapi\passive;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class ProductProcess extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\ProductProcess
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\quality\process\ProProcessMain();
        $this->viewModel = new \app\admin\model\eipapi\api\ProductWorkView();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("ppm")
                ->join(["product_work_view"=>"pwv"],"ppm.PT_Num=pwv.PT_Num")
                ->field("ppm.id,pwv.poNo as ORDER_NO,pwv.T_Num as PROD_ORDER_NO,pwv.poItemId as PO_ITEM_ID,pwv.PT_Num as PROD_WORK_ORDER,ppm.writer_time as DETECTION_TIME,ppm.writer as DETECTION_USER,ppm.auditor as EXAMINE_USER,ppm.upload,'' as url")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $base_url = ROOT_PATH . 'public' . DS .'zlfiles' .DS. 'sjzjbg' .DS;
            foreach($list as $k=>$v){
                $file_url = $base_url.$v["PROD_WORK_ORDER"].".pdf";
                if(file_exists($file_url)) $list[$k]["url"] = DS .'zlfiles' .DS. 'sjzjbg' .DS. $v["PROD_WORK_ORDER"].".pdf";
            }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function allowUpload($ids=null)
    {
        $params = $this->model->where("id",$ids)->where("upload",0)->find();
        if(!$params) return json(["code"=>0,"msg"=>"不存在或者已上传，失败"]);
        $exist = $this->viewModel->where("PT_Num",$params["PT_Num"])->where("cl_upload",1)->find();
        if(!$exist) return json(["code"=>0,"msg"=>"原材料未上传，请先上传原材料"]);
        // $orderParams = $this->model->where("PT_Num",$ids)->where("eipUpload","1")->find();
        // if(!$orderParams) return json(["code"=>0,"msg"=>"请优先上传工单"]);
        $list = (new \app\admin\model\quality\process\ProProcessDetail())
            ->field("count,type,creat_time,CONCAT(hg_count,',',cj_count) as hg_count,(case when kong=1 then '钻孔' when kong=0 then '冲孔' else '' end) as kong,hole_number,(case when bending=1 then '冷弯' when bending=2 then '热弯' else '' end) as bending,(case when fire=1 then '一级' when fire=2 then '二级' when fire=3 then '三级' else '' end) as fire,concat(processor,',',certificate) as processor,factory,gal_time,parts,material,specification,sum_weight,late_weight")
            ->where("m_id",$ids)->select();
        if(!$list) return json(["code"=>0,"msg"=>"失败"]);
        $ini = [
            ["count" , "JGTG45" , "xl"],
            ["type" , "JGTG69" , "xl"],
            ["creat_time" , "JGTG46" , "xl"],
            ["hg_count" , "JGTG47" , "xl"],
            ["count" , "JGTG63" , "zk"],
            ["kong" , "JGTG48" , "zk"],
            ["hole_number" , "JGTG49" , "zk"],
            ["hg_count" , "JGTG50" , "zk"],
            ["count" , "JGTG64" , "zw"],
            ["bending" , "JGTG51" , "zw"],
            ["hg_count" , "JGTG52" , "zw"],
            ["fire" , "JGTG53" , "hj"],
            ["processor" , "JGTG54" , "hj"],
            ["hg_count" , "JGTG55" , "hj"],
            // ["sum_weight" , "JGTG57" , "dx"],
            // ["late_weight" , "JGTG58" , "dx"],
            // ["factory" , "JGTG59" , "dx"],
            // ["gal_time" , "JGTG60" , "dx"]
        ];
        $data = [];
        $fjList = [];
        $fjModel = new \app\admin\model\eipapi\passive\FjMaterialTestBz();
        $sum_count = (new \app\admin\model\chain\lofting\ProduceTaskDetail())->where("PT_Num",$params["PT_Num"])->count("PTD_ID");
        $xl_count = $zk_count = $zw_count = $hj_count = $dx_count = 0;
        $sum_weight = $late_weight = [];
        $part_num = ["xl"=>[],"zk"=>[],"zw"=>[],"hj"=>[],"dx"=>[]];
        foreach($list as $v){
            $part_num["xl"][] = $part_num["zk"][] = $part_num["dx"][] = $v["parts"];
            $sum_weight[] = $v["sum_weight"];
            $late_weight[] = $v["late_weight"];
            $xl_count++;
            $zk_count++;
            $dx_count++;
            if($v["fire"]){
                $hj_count++;
                $part_num["hj"][] = $v["parts"];
            }
            if($v["bending"]){
                $zw_count++;
                $part_num["zw"][] = $v["parts"];
            }
            foreach($ini as $iv){
                if(!$v["fire"] and $iv[2]=="hj") continue;
                if(!$v["bending"] and $iv[2]=="zw") continue;
                $data[] = [
                    "PT_Num" => $params["PT_Num"],
                    "FIELD" => $iv[0],
                    "RULE_CODE" => $iv[1],
                    "UPLOAD" => 0,
                    "type" => 0,
                    "workmanship" => $iv[2],
                    "value" => $v[$iv[0]],
                    "rate" => 0,
                    "time" => $params["writer_time"],
                    "PART_CODE" => $v["parts"],
                    "MATMATERIAL" => $v["material"],
                    "MATSPEC" => $v["specification"]
                ];
            }
        }
        $dxList = [
            ["sum_weight" , "JGTG57", array_sum($sum_weight)],
            ["late_weight" , "JGTG58", array_sum($late_weight)],
            ["factory" , "JGTG59", "绍兴电力设备有限公司"],
            ["gal_time" , "JGTG60", $params["writer_time"]],
        ];
        foreach($dxList as $k=>$v){
            $data[] = [
                "PT_Num" => $params["PT_Num"],
                "FIELD" => $v[0],
                "RULE_CODE" => $v[1],
                "UPLOAD" => 0,
                "type" => 0,
                "workmanship" => 'dx',
                "value" => $v[2],
                "rate" => 0,
                "time" => $params["writer_time"],
                "PART_CODE" => $params["PT_Num"],
                "MATMATERIAL" => "",
                "MATSPEC" => ""
            ];
        }
        
        $xl = $zk = $dx = round($xl_count/$sum_count*100);
        $zw = round($zw_count/$sum_count*100);
        $hj = round($hj_count/$sum_count*100);
        $rateList = ["xl"=>$xl,"zk"=>$zk,"hj"=>$hj,"dx"=>$dx,"zw"=>$zw];
        foreach($data as $k=>$v){
            $data[$k]["rate"] = $rateList[$v["workmanship"]];
        }
        foreach(["xl"=>"JGTS31","zk"=>"JGTS34","zw"=>"JGTS36","hj"=>"JGTS39"] as $k=>$v){
            if(!empty($part_num[$k])){
                $fjList[] = [
                    "PT_Num" => $params["PT_Num"],
                    "BELONGTO_CODE" => $v,
                    "PART_CODE" => implode(",",$part_num[$k]),
                    "BELONGTO_ID" => "无",
                    "BATCH" => "无",
                    "MAT_RECHECK_NO" => "无",
                    "FILE_NO" => "无",
                    "FILE_TYPE"=> "1",
                    "filename" => "质检报告.pdf",
                    "fileContent" => "sjzjbg/".$params["PT_Num"].'.pdf',
                ];
            }
            
        }
        $result = false;
        Db::startTrans();
        try {
            $this->model->where("id",$ids)->update(["upload"=>1]);
            $result = (new \app\admin\model\quality\process\ProTestDetailBz())->saveAll($data);
            $fjModel->allowField(true)->saveAll($fjList);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $this->success("允许上传成功");
        } else {
            $this->error("允许上传失败");
        }
    }

    public function detail($ids=null)
    {
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\quality\process\ProProcessDetail())
                ->field("id,DtMD_ID_PK,sect,parts,stuff,material,specification,length,width,count as xl_count,type as xl_type,creat_time as xl_creat_time,CONCAT(hg_count,',',cj_count) AS xl_hg_count,count as zk_count,(case when kong='1' then '钻孔' else '冲孔' end) as zk_kong,sum_hole as zk_sum_hole,CONCAT(hg_count,',',cj_count) AS zk_hg_count,(case when bending<>'0' then count else '' end) as zw_count,(case bending when '0' then '' when 1 then '冷弯' else '热弯' end) as zw_bending,(case when bending<>'0' then CONCAT(hg_count,',',cj_count) else '' end) as zw_hg_count,(case fire when '0' then '' when 1 then '一级' when 2 then '二级' else '三级' end) as hj_fire,(case when fire='0' then '' else CONCAT(processor,',',certificate) end) as hj_processor,(case when fire<>'0' then CONCAT(hg_count,',',cj_count) else '' end) as hj_hg_count,sum_weight as dx_sum_weight,late_weight as dx_late_weight,factory as dx_factory,gal_time as dx_gal_time")
                ->where("m_id",$ids)
                ->where($where)
                ->order($sort, $order)
                ->select();
            $result = array("total" => count($list), "rows" => $list);

            return json($result);
        }
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

}
