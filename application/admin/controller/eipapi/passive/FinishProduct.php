<?php

namespace app\admin\controller\eipapi\passive;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class FinishProduct extends Backend
{
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\FinishProduct
     */
    protected $model = null;
    protected $noNeedLogin = "*";
    protected $status = [
        "0" => "未打包",
        "1" => "已打包未入库",
        //bso 有scd_id的
        "2" => "已入库",
        //bso 有count为0不为0的都有
        "3" => "已出库未发运",
        //bso count全为0
        "4" => "已发运"
    ];
    protected $fiUpload = [
        0 => "未允许上传",
        1 => "允许上传",
        2 => "已上传允许更新"
    ];

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\eipapi\api\ProductWorkView();
        $this->assignconfig("statusList",$this->status);
        $this->assignconfig("fiUploadList",$this->fiUpload);
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $data = [];
            $list = $this->model->alias("fp")
                ->join(["taskheight"=>"th"],"fp.TD_ID=th.TD_ID")
                ->JOIN(["sectconfigdetail"=>"scd"],"scd.TH_ID=th.TH_ID")
                ->FIELD("poNo as ORDER_NO,T_Num as PROD_ORDER_NO,poItemId as PO_ITEM_ID,PT_Num as PROD_WORK_ORDER,scd.cp_upload,0 as `status`")
                ->where($where)
                ->ORDER("scd.cp_upload")
                ->order($sort, $order)
                ->GROUP("fp.PT_Num")
                ->paginate($limit);
            $ptNumList = [];
            foreach($list as $v){
                $data[$v["PROD_WORK_ORDER"]] = $v->toArray();
                $ptNumList[$v["PROD_WORK_ORDER"]] = $v["PROD_WORK_ORDER"];
            }
            $scdPtNumList = (new \app\admin\model\chain\lofting\UnionProduceTaskFlatView())->where("PT_Num","in",$ptNumList)->group("SCD_ID")->SELECT();
            $scdPtNumArr = $packScdList = $ckScdList = $rkScdList = $fyScdList = [];
            foreach($scdPtNumList as $v){
                $scdPtNumArr[$v["SCD_ID"]] = $v["PT_Num"];
            }
            //先这样 晚点修改
            if(!empty($scdPtNumArr)){
                $packScdList = (new \app\admin\model\chain\pack\Pack())->where("SCD_ID","IN",array_keys($scdPtNumArr))->where("Auditor","<>","")->order("Auditor asc")->group("SCD_ID")->column("SCD_ID");
                foreach($packScdList as $v){
                    if(isset($data[$scdPtNumArr[$v]])){
                        $data[$scdPtNumArr[$v]]["status"] = 1;
                    }
                }
            }
            if(!empty($packScdList)){
                $rkScdList = (new \app\admin\model\chain\pack\ProduceUnitIn())->where("SCD_ID","IN",$packScdList)->group("SCD_ID")->column("SCD_ID");
                foreach($rkScdList as $v){
                    if(isset($data[$scdPtNumArr[$v]])){
                        $data[$scdPtNumArr[$v]]["status"] = 2;
                    }
                }
                $ckScdList = (new \app\admin\model\chain\pack\SaleInvoiceSingle())->where("SIS_ScdId","IN",$packScdList)->group("SIS_ScdId")->column("SIS_ScdId");
                foreach($ckScdList as $v){
                    if(isset($data[$scdPtNumArr[$v]])){
                        $data[$scdPtNumArr[$v]]["status"] = 3;
                    }
                }
            }
            if(!empty($ckScdList)){
                $fyScdList = (new \app\admin\model\chain\pack\BsoCount())->where("SCD_ID","IN",$ckScdList)->where("BSO_Count","<>",0)->group("SCD_ID")->column("SCD_ID");
                $diff = array_diff($ckScdList,$fyScdList);
                foreach($diff as $v){
                    if(isset($data[$scdPtNumArr[$v]])){
                        $data[$scdPtNumArr[$v]]["status"] = 4;
                    }
                }
            }
            $result = array("total" => $list->total(), "rows" => array_values($data));

            return json($result);
        }
        return $this->view->fetch();
    }

    public function allowUpload($ids = null)
    {
        // $orderParams = $this->model->where("PT_Num",$ids)->where("eipUpload","1")->find();
        // if(!$orderParams) return json(["code"=>0,"msg"=>"请优先上传工单"]);
        // $result = new \app\admin\model\chain\sale\Sectconfigdetail();
        $idList = explode(",",$ids);
        if(empty($idList)) $this->error("允许上传失败");
        $result = (new \app\admin\model\chain\lofting\UnionProduceTaskView())->alias("pt")
            ->JOIN(["taskheight"=>"th"],"pt.TD_ID=th.TD_ID")
            ->JOIN(["sectconfigdetail"=>"scd"],"scd.TH_ID=th.TH_ID")
            ->WHERE("pt.PT_Num","IN",$idList)
            ->UPDATE(["scd.cp_upload"=>1]);
        if ($result) {
            $this->success("允许上传成功");
        } else {
            $this->error("允许上传失败");
        }
    }
}
