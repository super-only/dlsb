<?php

namespace app\admin\controller\eipapi\passive;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class BomTxt extends Backend
{
    
    const SUPPLIER_CODE = '1000014615';
    protected $noNeedLogin = "*";
    protected $return_msg = [
        "status" => 1,
        "message" => "失败",
        "supplierCode" => self::SUPPLIER_CODE,
        "data" => []
    ];
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\BomTxt
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\eipapi\api\ProductWorkView();

    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->field("poNo as ORDER_NO,T_Num as PROD_ORDER_NO,poItemId as PO_ITEM_ID,PT_Num as PROD_WORK_ORDER,poItemNo")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function export($ids = null)
    {
        $return_data = $this->return_msg;
        $params = $this->model->where("PT_Num",$ids)->find();
        if(!$params) return json(["code"=>0,"data"=>json_encode($return_data)]);
        $produceTaskDetailModel = new \app\admin\model\chain\lofting\ProduceTaskDetail([],$params['sect_field']);
        $list = $produceTaskDetailModel->alias("d")
            ->join([$params['sect_field']."dtmaterialdetial"=>'dd'],"d.DtMD_ID_PK = dd.DtMD_ID_PK")
            ->join([$params['sect_field']."dtsect"=>"ds"],"dd.DtMD_iSectID_FK = ds.DtS_ID_PK")
            ->field("CAST(d.PT_Sect AS UNSIGNED) AS number_1,d.PT_Sect,CAST(dd.DtMD_sPartsID AS UNSIGNED) AS number_2,dd.DtMD_sPartsID,dd.DtMD_sStuff,dd.DtMD_sMaterial,dd.DtMD_sSpecification,dd.DtMD_iLength,dd.DtMD_fWidth,ifnull(dd.DtMD_sType,'') as DtMD_sType,ifnull(dd.type,'') as type,ifnull(dd.DtMD_iTorch,0) as DtMD_iTorch,d.PTD_Count,dd.DtMD_iUnitHoleCount,(dd.DtMD_iUnitHoleCount*PTD_Count) as SumCount,round(dd.DtMD_fUnitWeight,1) as DtMD_fUnitWeight,round(dd.DtMD_fUnitWeight*PTD_Count,1) as SumWeight,dd.DtMD_sRemark,d.IF_Shi,dd.DtMD_iWelding,d.PTD_ID,
            case when (length(DtMD_sPartsID)-length(replace(DtMD_sPartsID,'-','')))>1 then 
            SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),1,LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))-1)*10000+cast(SUBSTR(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1),LOCATE('-',SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1))+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=3 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,1)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1+1) as signed)
            when LOCATE('-',DtMD_sPartsID)>0 and LENGTH(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed))=4 then SUBSTR(DtMD_sPartsID,1,LOCATE('-',DtMD_sPartsID)-1)*10000000+SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),1,2)*10000+cast(SUBSTR(cast(SUBSTR(DtMD_sPartsID,LOCATE('-',DtMD_sPartsID)+1) as signed),2+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=3 then SUBSTR(cast(DtMD_sPartsID as signed),1,1)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),1+1) as signed)
            when LENGTH(cast(DtMD_sPartsID as signed))=4 then SUBSTR(cast(DtMD_sPartsID as signed),1,2)*10000000+cast(SUBSTR(cast(DtMD_sPartsID as signed),2+1) as signed)
            else 0 end 	bjbhn,
            cast((case 
            when dtmd_sstuff='角钢' then 
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),1,locate('*',REPLACE(DtMD_sSpecification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(DtMD_sSpecification,'∠',''),locate('*',REPLACE(DtMD_sSpecification,'∠',''))+1,2)*1
            when (dtmd_sstuff='钢板' or dtmd_sstuff='钢管') then 
            REPLACE(DtMD_sSpecification,'-','')
            else 0
            end) as UNSIGNED) bh,d.DtMD_ID_PK,ds.DtS_Name,dd.DtMD_ZuanKong,dd.DtMD_iBackGouging,dd.DtMD_fBackOff,dd.DtMD_iFireBending,dd.DtMD_iCuttingAngle,dd.DtMD_KaiHeJiao,dd.DtMD_DaBian")
            ->where([
                "d.PT_Num" => ["=",$ids]
            ])
            ->order("number_1,bjbhn,number_2,dd.DtMD_sPartsID asc")
            ->select();
        if(!$list) return json(["code"=>0,"data"=>json_encode($return_data)]);
        $data = [];
        $typeList = [
            "主材" => "1",
            "接头件" => "2",
            "连接板" => "3",
            "腹材" => "4",
            "焊接件" => "5",
            "杆体" => "6",
            "横担" => "7",
            "其他" => "9"
        ];
        $weldList = [
            0=>"无",
            1=>"一级",
            2=>"二级",
            3=>"三级"
        ];
        foreach($list as $v){
            $data[$v["PTD_ID"]] = [
                "JSJGBOM_ID" => md5($params["TD_ID"].'-'.$v["DtMD_sPartsID"]),
                "SUPPLIER_CODE" => "1000014615",
                "ORDER_NO" => $params["poNo"],
                "PO_ITEM_ID" => $params["poItemId"],
                "INFO_TYPE_CODE" => "T1011",
                "PROD_ORDER_NO" => $params["T_Num"],
                "TOWER_TYPE" => $params["materialsDescription"],
                "PROD_WORK_ORDER" => $params["PT_Num"],
                "TRACE_CODE"=> self::SUPPLIER_CODE.$params["poItemNo"]."0000".(str_pad($params["TD_ID"],16,0,STR_PAD_LEFT))."0000",
                "PROCEDURE_CODE" => "1",
                "SEGMENT_SN" => $v["DtS_Name"],
                "PART_CODE" => $v["DtMD_sPartsID"],
                "PART_TYPE" => $typeList[$v["type"]]??"9",
                "MAT_NAME" => $v["DtMD_sStuff"],
                "MATSPEC" => $v["DtMD_sSpecification"],
                "MATMATERIAL" => $v["DtMD_sMaterial"],
                "LENGTH" => "".($v["DtMD_iLength"]??0),
                "WIDTH" => "".($v["DtMD_fWidth"]??0),
                "THICKNESS" => "".($v["DtMD_iTorch"]??0),
                "SINGLE_COUNT" => "".($v["SumCount"]??0),
                "WEIGHT" => "".($v["DtMD_fUnitWeight"]??0),
                "HOLES" => "".($v["DtMD_iUnitHoleCount"]??0),
                "PUNCHING_HOLES" => "".($v["DtMD_ZuanKong"]?0:1),
                "DRILLING_HOLES" => "".($v["DtMD_ZuanKong"]?1:0),
                "CLEAR_ROOT" => "".($v["DtMD_iBackGouging"]?1:0),
                "SHOVEL_BACK" => "".($v["DtMD_fBackOff"]?1:0),
                "COLD_BENDING" => "".($v["DtMD_iFireBending"]==1?1:0),
                "FIRE_BENDING" => "".($v["DtMD_iFireBending"]==2?1:0),
                "CORNER_CUTTING" => "".($v["DtMD_iCuttingAngle"]?1:0),
                "MERGE_ANGLE" => "".($v["DtMD_KaiHeJiao"]==2?1:0),
                "OPERATING_ANGLE" => "".($v["DtMD_KaiHeJiao"]==1?1:0),
                "SQUASH" => "".($v["DtMD_DaBian"]?1:0),
                "WELDED" => "".($v["DtMD_iWelding"]?1:0),
                "WELDED_GRADE" => "".($v["DtMD_iWelding"]?$weldList[$v["DtMD_iWelding"]]:0),
                "REMARK" => isset($typeList[$v["type"]])?"":"构件类型：".$v["type"],
                "ACQ_TIME" => date("Y-m-d H:i:s"),
            ];
        }
        if(!empty($data)){
            $return_data["status"]=0;
            $return_data["message"] = "成功！";
            $return_data["data"] = array_values($data);
            return json(["code"=>1,"data"=>json_encode($return_data),"pt_num"=>$params["poNo"]."-".$ids]);
        }
        return json(["code"=>0,"data"=>json_encode($return_data)]);
        
    }
}
