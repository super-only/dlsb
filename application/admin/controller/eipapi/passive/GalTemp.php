<?php

namespace app\admin\controller\eipapi\passive;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class GalTemp extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\GalTemp
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\quality\process\ProProcessMain();
        $this->assignconfig("status",[0=>"未允许",1=>"已允许未上传",2=>"已上传"]);

    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("ppm")
                ->join(["product_work_view"=>"pwv"],"ppm.PT_Num=pwv.PT_Num")
                ->field("ppm.id,pwv.poNo as ORDER_NO,pwv.T_Num as PROD_ORDER_NO,pwv.poItemId as PO_ITEM_ID,pwv.PT_Num as PROD_WORK_ORDER,ppm.writer_time as DETECTION_TIME,ppm.writer as DETECTION_USER,ppm.auditor as EXAMINE_USER,ppm.dx_upload")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function allowUpload($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) return json(["code"=>0,"msg"=>__('No Results were found')]);
        $productProcessModel = (new \app\admin\model\quality\process\ProProcessMain());
        $galModel = (new \app\admin\model\quality\process\GalvanizedTrue());
        $gal_row = $galModel->where("ppm_id",$ids)->order("time asc")->select();
        if(!$gal_row){
            $gal_row = $galModel->saveRange($row);
        }
        if(!$gal_row) return json(["code"=>0,"msg"=>'请稍后再试']);
        $result = $productProcessModel->where("id",$ids)->update(["dx_upload"=>1]);
        if ($result !== false) {
            $this->success("允许上传成功");
        } else {
            $this->error("允许上传失败");
        }
    }

    public function detail($ids=null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $galModel = (new \app\admin\model\quality\process\GalvanizedTrue());
        $gal_row = $galModel->where("ppm_id",$ids)->order("time asc")->select();
        if(!$gal_row){
            $gal_row = $galModel->saveRange($row);
        }
        $list = $gal_row?collection($gal_row)->toArray():[];
        $this->assignconfig("list",$list);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

}
