<?php

namespace app\admin\controller\eipapi\passive;

use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class TestProcess extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\TestProcess
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\quality\process\TestProcessMain();

    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("ppm")
                ->join(["product_work_view"=>"pwv"],"ppm.PT_Num=pwv.PT_Num")
                ->field("ppm.id,pwv.poNo as ORDER_NO,pwv.T_Num as PROD_ORDER_NO,pwv.poItemId as PO_ITEM_ID,pwv.PT_Num as PROD_WORK_ORDER,ppm.writer_time as DETECTION_TIME,ppm.writer as DETECTION_USER,ppm.auditor as EXAMINE_USER,ppm.upload,'' as url")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $base_url = ROOT_PATH . 'public' . DS .'zlfiles' .DS. 'sygczjbg' .DS;
            foreach($list as $k=>$v){
                $file_url = $base_url.$v["PROD_WORK_ORDER"].".pdf";
                if(file_exists($file_url)) $list[$k]["url"] = DS .'zlfiles' .DS. 'sygczjbg' .DS. $v["PROD_WORK_ORDER"].".pdf";
            }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function allowUpload($ids=null)
    {
        $params = $this->model->where("id",$ids)->where("upload",0)->find();
        if(!$params) return json(["code"=>0,"msg"=>"不存在或者已上传，失败"]);
        $exist = (new \app\admin\model\quality\process\ProProcessMain())->where("PT_Num",$params["PT_Num"])->where("upload",1)->find();
        if(!$exist) return json(["code"=>0,"msg"=>"生产过程未上传，请先上传生产过程"]);

        // $orderParams = $this->model->where("PT_Num",$ids)->where("eipUpload","1")->find();
        // if(!$orderParams) return json(["code"=>0,"msg"=>"请优先上传工单"]);
        $list = (new \app\admin\model\quality\process\TestProcessDetail())
            ->field("(case when fire='1' then '一级' when fire='2' then '二级' when fire='3' then '三级' else '' end) as fire,concat(processor,',',certificate) as processor,ave_thick_flag,ave_thick,min_thick_flag,min_thick,adhesion,uniformity,con_por,place_rate,main_control,parts,material,specification,concat(cj_count,',',hg_count) as hg_count")
            ->where("m_id",$ids)->where("fire","<",3)->select();
        if(!$list) return json(["code"=>0,"msg"=>"失败"]);
        $ini = [
            // ["fire" , "JGTG61" , "hf"],
            // ["processor" , "JGTG62" , "hf"],
            // ["ave_thick_flag" , "JGTG23" , "dx"],
            // ["ave_thick" , "JGTG24" , "dx"],
            // ["min_thick_flag" , "JGTG25" , "dx"],
            // ["min_thick" , "JGTG26" , "dx"],
            ["adhesion" , "JGTG27" , "dx"],
            ["uniformity" , "JGTG28" , "dx"],
            ["con_por" , "JGTG29" , "szz"],
            ["place_rate" , "JGTG30" , "szz"],
            ["main_control" , "JGTG31" , "szz"]
        ];
        $data = $ini_data = [];
        $sum_count = (new \app\admin\model\chain\lofting\ProduceTaskDetail())->where("PT_Num",$params["PT_Num"])->count("PTD_ID");
        $hf_count = $dx_count = $szz_count = 0;
        $part_num = ["hf"=>[],"dx"=>[],"szz"=>[]];
        $fjList = [];
        $fjModel = new \app\admin\model\eipapi\passive\FjMaterialTestBz();
        foreach($list as $v){
            $ini_data = $ini;
            $dx_count++;
            $szz_count++;
            $part_num["dx"][] = $part_num["szz"][] = $v["parts"];
            if($v["fire"]){
                $part_num["hf"][] = $v["parts"];
                $hf_count++;
                if($v["fire"]=='一级') $ini_data[] = ["hg_count" , "JGTG21" , "hf"];
                else if($v["fire"]=='二级') $ini_data[] = ["hg_count" , "JGTG22" , "hf"];
                $ini_data[] = ["fire" , "JGTG61" , "hf"];
                $ini_data[] = ["processor" , "JGTG62" , "hf"];
            }
            if($v["ave_thick_flag"]) $ini_data[] = ["ave_thick" , "JGTG23" , "dx"];
            else $ini_data[] = ["ave_thick" , "JGTG24" , "dx"];

            if($v["min_thick_flag"]) $ini_data[] = ["min_thick" , "JGTG25" , "dx"];
            else $ini_data[] = ["min_thick" , "JGTG26" , "dx"];
            foreach($ini_data as $iv){
                if(!$v["fire"] and $iv[2]=="hf") continue;
                $data[] = [
                    "PT_Num" => $params["PT_Num"],
                    "FIELD" => $iv[0],
                    "RULE_CODE" => $iv[1],
                    "UPLOAD" => 0,
                    "type" => 1,
                    "workmanship" => $iv[2],
                    "value" => $v[$iv[0]],
                    "rate" => 0,
                    "time" => $params["writer_time"],
                    "PART_CODE" => $v["parts"],
                    "MATMATERIAL" => $v["material"],
                    "MATSPEC" => $v["specification"]
                ];
            }
        }
        $dx = $szz = round($dx_count/$sum_count*100);
        $hf = round($hf_count/$sum_count*100);
        $rateList = ["dx"=>$dx,"szz"=>$szz,"hf"=>$hf];
        foreach($data as $k=>$v){
            $data[$k]["rate"] = $rateList[$v["workmanship"]];
        }
        foreach(["hf"=>"JGTS08","dx"=>"JGTS12","szz"=>"JGTS15"] as $k=>$v){
            if(!empty($part_num[$k])){
                $fjList[] = [
                    "PT_Num" => $params["PT_Num"],
                    "BELONGTO_CODE" => $v,
                    "PART_CODE" => implode(",",$part_num[$k]),
                    "BELONGTO_ID" => "无",
                    "BATCH" => "无",
                    "MAT_RECHECK_NO" => "无",
                    "FILE_NO" => "无",
                    "FILE_TYPE"=> "1",
                    "filename" => "质检报告.pdf",
                    "fileContent" => "sygczjbg/".$params["PT_Num"].'.pdf',
                ];
            }
            
        }
        $result = false;
        Db::startTrans();
        try {
            $this->model->where("id",$ids)->update(["upload"=>1]);
            $result = (new \app\admin\model\quality\process\ProTestDetailBz())->saveAll($data);
            $fjModel->allowField(true)->saveAll($fjList);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $this->success("允许上传成功");
        } else {
            $this->error("允许上传失败");
        }
    }

    public function detail($ids=null)
    {
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = (new \app\admin\model\quality\process\TestProcessDetail())
                ->field("id,DtMD_ID_PK,sect,parts,stuff,material,specification,length,(case fire when '0' then '' when '1' then '一级' when '2' then '二级' else '三级' end) as fire,(case when fire='0' then '' else CONCAT(cj_count,',',hg_count) end) as hg_count,(case when fire='0' then '' else CONCAT(processor,',',certificate) end) as processor,ave_thick,min_thick,adhesion,uniformity,con_por,place_rate,main_control")
                ->where("m_id",$ids)
                ->where($where)
                ->order($sort, $order)
                ->select();
            $result = array("total" => count($list), "rows" => $list);

            return json($result);
        }
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

}
