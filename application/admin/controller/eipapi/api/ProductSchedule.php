<?php

namespace app\admin\controller\eipapi\api;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class ProductSchedule extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\ProductSchedule
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\chain\sequence\Sequence;
        $this->detailModel = new \app\admin\model\chain\sequence\SequenceDetail;
        $this->ipoStatus = [
            "1" => "创建",
            "2" => "原材料检验",
            "3" => "生产中",
            "4" => "出厂试验",
            "5" => "包装入库"
        ];
        $this->assignconfig("ipoStatus",$this->ipoStatus);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("m")
                ->join(["product_work_view"=>"pwv"],"m.PT_Num=pwv.PT_Num")
                ->field("m.*,pwv.planFinishDate as dueDate,pwv.poItemId,m.eipUpload")
                ->where($where)
                ->order("m.eipUpload asc")
                ->order($sort, $order)
                ->paginate($limit);
            $data = [];
            foreach($list as $k=>$v){
                $data[$v["PT_Num"]] = $v->toArray();
                $diff_seconds = strtotime($v["PlanFinishTime"])-strtotime($v["PlanStartTime"]);
                $diff_days = $diff_seconds/86400;
                $data[$v["PT_Num"]]["planPeriod"] = $list[$k]["planPeriod"] = $diff_days;
            }
            $pcSchedulingModel = new \app\admin\model\chain\material\PcScheduling();
            $timeList = $pcSchedulingModel->alias("ps")
                ->join(["pc_scheduling_sect"=>"pss"],"ps.ID=pss.MID")
                ->join(["pc_feedback"=>"pfc"],"pfc.ZID=pss.ID","LEFT")
                ->field("PtNum,ifnull(FinishedTime,'0000-00-00 00:00:00') as FinishedTime")
                ->where("PtNum","IN",array_keys($data))
                ->ORDER("PtNum,FinishedTime desc")
                ->select();
            $timeArr = [];
            foreach($timeList as $v){
                !isset($timeArr[$v["PtNum"]])?$timeArr[$v["PtNum"]] = [
                    "ActStartTime" => "0000-00-00 00:00:00",
                    "ActFinishTime" => "0000-00-00 00:00:00",
                ]:"";
                if($v["FinishedTime"]!="0000-00-00 00:00:00") $timeArr[$v["PtNum"]]["ActStartTime"] = $v["FinishedTime"];
                else $timeArr[$v["PtNum"]]["ActFinishTime"] = $v["FinishedTime"];
            }
            foreach($timeArr as $k=>$v){
                $data[$k]["ActStartTime"] = $v["ActStartTime"];
                $data[$k]["ActFinishTime"] = $v["ActFinishTime"];
            }
            // $startTimeList = $pcSchedulingModel->where("PTNum","IN",array_keys($data))->order("writer_time asc")->group("PTNum")->column("PTNum,writer_time");
            // $finishTimeList = $pcSchedulingModel->where("PTNum","IN",array_keys($data))->order("auditor_time desc")->group("PTNum")->column("PTNum,auditor_time");
            // foreach($startTimeList as $k=>$v){
            //     $data[$k]["actualStartDate"] = $v=="0000-00-00 00:00:00"?$data[$k]["actualStartDate"]:$v;
            // }
            // foreach($finishTimeList as $k=>$v){
            //     $data[$k]["actualFinishDate"] = $v=="0000-00-00 00:00:00"?$data[$k]["actualFinishDate"]:$v;
            // }
            $result = array("total" => $list->total(), "rows" => array_values($data));

            return json($result);
        }
        return $this->view->fetch();
    }

    // public function updateFinish($ids=null)
    // {
    //     $pcSchedulingModel = new \app\admin\model\chain\material\PcScheduling();
    //     $find = $pcSchedulingModel->field("case when Num=nrecord then 1 else 0 end count")->where("PC_Num",$ids)->having("count=0")->find();
    //     if($find) return json(["code"=>0,"msg"=>"有未完工的，更新失败"]);
    //     $result = $pcSchedulingModel->where("PC_Num",$ids)->update(["actualFinishDate"=>date("Y-m-d H:i:s")]);
    //     if($result) return json(["code"=>1,"msg"=>"更新成功"]);
    //     else return json(["code"=>0,"msg"=>"更新失败"]);
    // }

    public function detail($ids=null)
    {
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            $ptNum = $this->model
            ->where("sNum",$ids)->value("PT_Num");
            // $pcSchedulingModel = new \app\admin\model\chain\material\PcScheduling();
            // $startTimeList = $pcSchedulingModel->where("PTNum",$ids)->order("writer_time asc")->group("PTNum")->column("PTNum,writer_time");
            // $finishTimeList = $pcSchedulingModel->where("PTNum",$ids)->order("auditor_time desc")->group("PTNum")->column("PTNum,auditor_time");
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->detailModel
                ->where("sNum",$ids)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);


                $pcSchedulingModel = new \app\admin\model\chain\material\PcScheduling();
                $timeList = $pcSchedulingModel->alias("ps")
                    ->join(["pc_scheduling_sect"=>"pss"],"ps.ID=pss.MID")
                    ->join(["pc_feedback_copy"=>"pfc"],"pfc.ZID=pss.ID","LEFT")
                    ->field("PtNum,ifnull(FinishedTime,'0000-00-00 00:00:00') as FinishedTime")
                    ->where("PtNum",$ptNum)
                    ->ORDER("PtNum,FinishedTime desc")
                    ->select();
                $timeArr = [];
                foreach($timeList as $v){
                    !isset($timeArr[$v["PtNum"]])?$timeArr[$v["PtNum"]] = [
                        "ActStartTime" => "0000-00-00 00:00:00",
                        "ActFinishTime" => "0000-00-00 00:00:00",
                    ]:"";
                    if($v["FinishedTime"]!="0000-00-00 00:00:00") $timeArr[$v["PtNum"]]["ActStartTime"] = $v["FinishedTime"];
                    else $timeArr[$v["PtNum"]]["ActFinishTime"] = $v["FinishedTime"];
                }
                // foreach($timeArr as $k=>$v){
                //     $data[$k]["actualStartDate"] = $v["actualStartDate"];
                //     $data[$k]["actualFinishDate"] = $v["actualFinishDate"];
                // }
                
            foreach($list as $k=>$v){
                if($v["orderId"]=="TTGX01"){
                    $list[$k]["ActStartTime"] = $timeArr[$ptNum]["ActStartTime"]??$list[$k]["ActStartTime"];
                    $list[$k]["ActFinishTime"] = $timeArr[$ptNum]["ActFinishTime"]??$list[$k]["ActFinishTime"];
                }
                
            }
           
            $result = array("total" => $list->total(), "rows" => $list->items());
            return json($result);
        }
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function eipUpload($ids=null)
    {
        $params = $this->model->alias("m")
            ->join(["product_work_view"=>"pwv"],"m.PT_Num=pwv.PT_Num")
            ->field("m.eipUpload,m.PT_Num,m.sNum,m.PlanStartTime as planStartDate,m.PlanFinishTime as planFinishDate,m.writer,m.writer_time,m.ActStartTime as actualStartDate,m.ActFinishTime as actualFinishDate,pwv.planFinishDate as dueDate,pwv.poItemId,m.eipUpload,pwv.materialsDescription,pwv.PT_sumWeight")
            ->where("m.sNum",$ids)
            ->find();
        if (!$params) {
            $this->error(__('No Results were found'));
        }
        // $pcSchedulingModel = new \app\admin\model\chain\material\PcScheduling();
        // $startTimeList = $pcSchedulingModel->where("PTNum",$ids)->order("writer_time asc")->group("PTNum")->column("PTNum,writer_time");
        // $finishTimeList = $pcSchedulingModel->where("PTNum",$ids)->order("auditor_time desc")->group("PTNum")->column("PTNum,auditor_time");
        $list = $this->detailModel->field("sdNum,
        sNum,
        orderId,
        PlanStartTime as planStartDate,
        PlanFinishTime as planFinishDate,
        writer,
        ActStartTime as actualStartDate,
        ActFinishTime as actualFinishDate")->where("sNum",$ids)->select();
        $ptNum = $this->model
            ->where("sNum",$ids)->value("PT_Num");
        $pcSchedulingModel = new \app\admin\model\chain\material\PcScheduling();
        $timeList = $pcSchedulingModel->alias("ps")
            ->join(["pc_scheduling_sect"=>"pss"],"ps.ID=pss.MID")
            ->join(["pc_feedback_copy"=>"pfc"],"pfc.ZID=pss.ID","LEFT")
            ->field("PtNum,ifnull(FinishedTime,'0000-00-00 00:00:00') as FinishedTime")
            ->where("PtNum",$ptNum)
            ->ORDER("PtNum,FinishedTime desc")
            ->select();
        $timeArr = [];
        foreach($timeList as $v){
            !isset($timeArr[$v["PtNum"]])?$timeArr[$v["PtNum"]] = [
                "actualStartDate" => "0000-00-00 00:00:00",
                "actualFinishDate" => "0000-00-00 00:00:00",
            ]:"";
            if($v["FinishedTime"]!="0000-00-00 00:00:00") $timeArr[$v["PtNum"]]["actualStartDate"] = $v["FinishedTime"];
            else $timeArr[$v["PtNum"]]["actualFinishDate"] = $v["FinishedTime"];
        }
        foreach($list as $k=>$v){
            if($v["orderId"]=="TTGX01"){
                $list[$k]["actualStartDate"] = isset($timeArr[$ptNum])?$timeArr[$ptNum]["actualStartDate"]:$list[$k]["actualStartDate"];
                $list[$k]["actualFinishDate"] = isset($timeArr[$ptNum])?$timeArr[$ptNum]["actualFinishDate"]:$list[$k]["actualFinishDate"];
            }
            
        }
        $params["actualStartDate"] = isset($timeArr[$ptNum])?$timeArr[$ptNum]["actualStartDate"]:$params["actualStartDate"];
        $params["actualFinishDate"] = isset($timeArr[$ptNum])?$timeArr[$ptNum]["actualFinishDate"]:$params["actualFinishDate"];
        // $params["actualFinishDate"] = (isset($finishTimeList[$ids]) and $finishTimeList[$ids]!="0000-00-00 00:00:00")?$finishTimeList[$ids]:$params["actualFinishDate"];
        $param_url = "supplier-production-schedule";
        $row = $this->_getDetail($params,$list);
        $operatetype = "ADD";
        if($params["eipUpload"]) $operatetype = 'UPDATE';
        $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
        $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$row);
        if($api_result["code"]==1) $this->model->where("sNum",$ids)->update(["eipUpload"=>1]);
        else $saveData[$ids] = $api_result["msg"];
        $msg = "上传成功";
        if(!empty($saveData)) {
            $msg = "";
            foreach($saveData as $k=>$v){
                $msg .= $k.$v.";";
            }
            $json_result["msg"] = $msg;
        }else $json_result = ["code"=>1,"msg"=>$msg];
        return json($json_result);
    }

    protected function _getDetail($params,$list)
    {
        $diff_seconds = strtotime($params["planFinishDate"])-strtotime($params["planStartDate"]);
        $diff_days = $diff_seconds/86400;
        $row = [
            "purchaserHqCode" => "SGCC",
            "supplierCode" => "1000014615",
            "supplierName" => "绍兴电力设备有限公司",
            "poItemId" => $params["poItemId"],
            "scheduleCode" => $params["sNum"],
            "planPeriod" => $diff_days,
            "dueDate" => $params["dueDate"]=="0000-00-00 00:00:00"?"0000-00-00":date("Y-m-d",strtotime($params["dueDate"])),
            "planStartDate" => $params["planStartDate"]=="0000-00-00 00:00:00"?"0000-00-00":date("Y-m-d",strtotime($params["planStartDate"])),
            "actualStartDate" => $params["actualStartDate"]=="0000-00-00 00:00:00"?"0000-00-00":date("Y-m-d",strtotime($params["actualStartDate"])),
            "planFinishDate" => $params["planFinishDate"]=="0000-00-00 00:00:00"?"0000-00-00":date("Y-m-d",strtotime($params["planFinishDate"])),
            "actualFinishDate" => $params["actualFinishDate"]=="0000-00-00 00:00:00"?"0000-00-00":date("Y-m-d",strtotime($params["actualFinishDate"])),
            "categoryCode" => "60",
            "subclassCode" => "60001",
            "dataSource" => 0,
            "dataSourceCreateTime"=>date("Y-m-d H:i:s"),
            "woList" => [
                [
                    "batchCode" => $params["PT_Num"],
                    "scheduleWo" => $params["PT_Num"],
                    "matCode" => $params["materialsDescription"],
                    "matDesc" => $params["materialsDescription"],
                    "scheduleAmount" => $params["PT_sumWeight"],
                    "scheduleUnit" => 'kg',
                    "processList" => []
                ],
            ]
        ];
        foreach(["dueDate","planStartDate","actualStartDate","planFinishDate","actualFinishDate"] as $v){
            if($row[$v]=="0000-00-00") unset($row[$v]);
        }
        $orderList = $this->_orderList();
        foreach($list as $v){
            $ini_data = [
                "processCode" => $v["orderId"],
                "processName" => $orderList[$v["orderId"]],
                "planStartDate" => $v["planStartDate"]=="0000-00-00 00:00:00"?"0000-00-00":date("Y-m-d",strtotime($v["planStartDate"])),
                "actualStartDate" => $v["actualStartDate"]=="0000-00-00 00:00:00"?"0000-00-00":date("Y-m-d",strtotime($v["actualStartDate"])),
                "planFinishDate" => $v["planFinishDate"]=="0000-00-00 00:00:00"?"0000-00-00":date("Y-m-d",strtotime($v["planFinishDate"])),
                "actualFinishDate" => $v["actualFinishDate"]=="0000-00-00 00:00:00"?"0000-00-00":date("Y-m-d",strtotime($v["actualFinishDate"])),
            ];
            foreach(["planStartDate","actualStartDate","planFinishDate","actualFinishDate"] as $vv){
                if($ini_data[$vv]=="0000-00-00") unset($ini_data[$vv]);
            }
            $row["woList"][0]["processList"][] = $ini_data;
        }
        return $row;
    }
}
