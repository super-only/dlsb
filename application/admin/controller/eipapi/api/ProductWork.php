<?php

namespace app\admin\controller\eipapi\api;

use app\admin\model\chain\material\StoreOutDetail;
use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class ProductWork extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\ProductWork
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\eipapi\api\ProductWorkView;
        $this->ipoStatus = [
            "1" => "创建",
            "2" => "原材料检验",
            "3" => "生产中",
            "4" => "出厂试验",
            "5" => "包装入库"
        ];
        $this->assignconfig("ipoStatus",$this->ipoStatus);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $data = [];
            foreach($list as $v){
                $data[$v["PT_Num"]] = $v->toArray();
                // $data[$v["PT_Num"]]["actualStartDate"] = strtotime($v["planStartDate"])<strtotime("2022-10-01")?$v["planStartDate"]:$v["actualStartDate"];
            }
            $pieceDetailModel = new \app\admin\model\chain\lofting\PieceDetail();
            $actualStartDateList = $pieceDetailModel->alias("pd")
            ->JOIN(["piece_main"=>"pm"],"pm.id=pd.piece_id")
            ->where("pd.PT_Num","IN",array_keys($data))
            ->group("pd.PT_Num")
            ->column("(SUBSTRING_INDEX(GROUP_CONCAT(pd.PT_Num order by pm.record_time),',',1)) as PT_Num,pm.record_time");
            foreach($actualStartDateList as $k=>$v){
                $data[$k]["actualStartDate"] = $v;
                if($data[$k]["actualFinishDate"]!="0000-00-00 00:00:00") $data[$k]["actualStartDate"] = strtotime($data[$k]["actualStartDate"])>strtotime($data[$k]["actualFinishDate"])?date("Y-m-d H:i:s",strtotime("+5 day",strtotime($data[$k]["planStartDate"]))):$data[$k]["actualStartDate"];
            }

            $storeOutList = (new StoreOutDetail())->alias("sod")
                ->join(["storeout"=>"so"],"sod.SO_Num=so.SO_Num")
                ->where("sod.PT_Num","IN",array_keys($data))
                ->group("sod.PT_Num")
                ->column("(SUBSTRING_INDEX(GROUP_CONCAT(sod.PT_Num order by so.SO_TakeDate desc),',',1)) as PT_Num,so.SO_TakeDate");
            foreach($storeOutList as $k=>$v){
                if(isset($data[$k]) and $data[$k]["actualStartDate"]=="0000-00-00 00:00:00"){
                    $data[$k]["actualStartDate"] = $v;
                    if($data[$k]["actualFinishDate"]!="0000-00-00 00:00:00") $data[$k]["actualStartDate"] = strtotime($data[$k]["actualStartDate"])>strtotime($data[$k]["actualFinishDate"])?date("Y-m-d H:i:s",strtotime("+5 day",strtotime($data[$k]["planStartDate"]))):$data[$k]["actualStartDate"];
                }
            }

            $result = array("total" => $list->total(), "rows" => array_values($data));

            return json($result);
        }
        return $this->view->fetch();
    }

    public function updateFinish($ids=null)
    {
        $produceTaskModel = new \app\admin\model\chain\lofting\UnionProduceTaskView();
        $row = $produceTaskModel->get($ids);
        if(!$row) return json(["code"=>0,"msg"=>"没有找到相关信息"]);
        $date = $produceTaskModel->alias("pt")
            ->join(["bsocount"=>"b"],"pt.TD_ID=b.TD_ID")
            ->join(["saleinvoicedetail"=>"sid"],"b.BSO_ID=sid.BSO_ID")
            ->join(["saleinvoicemain"=>"sim"],"sid.SI_Num=sim.SIM_Num")
            ->where("pt.PT_Num",$ids)
            ->order("sim.SIM_Date desc")
            ->value("ifnull(sim.SIM_Date,'0000-00-00 00:00:00')");
        
        $result = $row->save(["actualFinishDate"=>$date]);
        if($result) return json(["code"=>1,"msg"=>"修改成功"]);
        else return json(["code"=>0,"msg"=>"修改失败"]);
    }

    public function eipUpload($ids=null)
    {
        $params = $this->model->field("T_Num,PT_Num,materialsDescription,amount,planStartDate,planFinishDate,actualStartDate,actualFinishDate,eipUpload,weight,sect_field")->where("PT_Num",$ids)->find();
        if (!$params) {
            $this->error(__('No Results were found'));
        }
        // $orderParams = $this->model->field("T_Num,eipUpload")->where("T_Num",$params["T_Num"])->where("eipUpload",1)->find();
        // if(!$orderParams) $this->error("请先提交对应生产订单");

        $pieceDetailModel = new \app\admin\model\chain\lofting\PieceDetail();
        $actualStartDate = $pieceDetailModel->alias("pd")
            ->JOIN(["piece_main"=>"pm"],"pm.id=pd.piece_id")
            ->where("pd.PT_Num",$params["PT_Num"])
            ->order("pm.record_time")
            ->value("ifnull(pm.record_time,'')");
        $params["actualStartDate"] = $actualStartDate?$actualStartDate:'0000-00-00 00:00:00';
        $storeOutDate = (new StoreOutDetail())->alias("sod")
            ->join(["storeout"=>"so"],"sod.SO_Num=so.SO_Num")
            ->where("sod.PT_Num",$params["PT_Num"])
            ->order("so.SO_TakeDate desc")
            ->value("ifnull(so.SO_TakeDate,'')");
        ($storeOutDate and $params["actualStartDate"]=="0000-00-00 00:00:00")?$params["actualStartDate"] = $storeOutDate:"";
        $params["actualFinishDate"] = $params["actualFinishDate"]??'0000-00-00 00:00:00';
        $params["actualStartDate"] = strtotime($params["actualStartDate"])>strtotime($params["actualFinishDate"])?date("Y-m-d H:i:s",strtotime("+5 day",strtotime($params["planStartDate"]))):$params["actualStartDate"];
        $row = $this->_getDetail($params);
        // pri($row,1);
        $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
        $param_url = "Eipsupplier-wo";
        $operatetype = "ADD";
        if($params["eipUpload"]) $operatetype = 'UPDATE';
        $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$row);
        if($api_result["code"]==1){
            (new \app\admin\model\chain\lofting\ProduceTask([],$params["sect_field"]))->where("PT_Num",$ids)->update(["eipUpload"=>1]);
            $json_result = ["code"=>1,"msg"=>"上传成功"];
        }else{
            $json_result = ["code"=>0,"msg"=>$api_result["msg"]];
        }
        return json($json_result);
    }

    protected function _getDetail($params)
    {
        $row = [
            "purchaserHqCode" => "SGCC",
            "ipoNo" => $params["T_Num"],
            "supplierCode" => "1000014615",
            "supplierName" => "绍兴电力设备有限公司",
            "woNo" => $params["PT_Num"],
            "categoryCode" => "60",
            "subclassCode" => "60001",
            "materialsCode" => '铁塔',
            "materialsDescription" => $params["materialsDescription"],
            "amount" => "".round($params["weight"]/1000,2),
            "unit" => "吨",
            "planStartDate" => date("Y-m-d",strtotime($params["planStartDate"])),
            "planFinishDate" => date("Y-m-d",strtotime($params["planFinishDate"])),
            // "actualStartDate" => date("Y-m-d",strtotime($params["actualStartDate"])),
            "woStatus" => 1,
            "dataSource" => 0,
            "dataSourceCreateTime"=>date("Y-m-d H:i:s")
        ];
        if($params["actualStartDate"]!='0000-00-00 00:00:00'){
            $row["actualStartDate"] = date("Y-m-d",strtotime($params["actualStartDate"]));
            $row["woStatus"] = 3;
        }
        if($params["actualFinishDate"]!='0000-00-00 00:00:00'){
            $row["actualFinishDate"] = date("Y-m-d",strtotime($params["actualFinishDate"]));
            $row["woStatus"] = 5;
        }
        return $row;
    }

}
