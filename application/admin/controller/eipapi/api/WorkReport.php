<?php

namespace app\admin\controller\eipapi\api;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class WorkReport extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\WorkReport
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\eipapi\api\WorkReportView;
        $this->orderArr = [
            "TTGX01" => "下料",
            "TTGX02" => "制孔",
            "TTGX03" => "制管",
            "TTGX04" => "装配/组对/组装",
            "TTGX05" => "焊接",
            "TTGX06" => "镀锌",
            "TTGX07" => "制弯"
        ];
        $this->assignconfig("orderArr",$this->orderArr);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order("eipUpload asc")
                ->order($sort, $order)
                ->paginate($limit);
            foreach($list as &$v){
                $v["planFinishDate"] = date("Y-m-d H:i:s",strtotime("+5 day",strtotime($v["planStartDate"])));
            }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function eipUpload($ids=null)
    {
        $params = $this->model->field("ipoNo,insideNo,device_name,productBatchNo,order,woNo,create_time,auditor_time,buyerProvince,eipUpload,planStartDate,record_time")->where("id",$ids)->find();
        $params["planFinishDate"] = date("Y-m-d H:i:s",strtotime("+5 day",strtotime($params["planStartDate"])));
        if (!$params) {
            $this->error(__('No Results were found'));
        }
        $row = $this->_getDetail($params);
        $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
        $param_url = "supplier-process-work";
        $operatetype = "ADD";
        if($params["eipUpload"]) $operatetype = 'UPDATE';
        $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$row);
        if($api_result["code"]==1){
            (new \app\admin\model\chain\lofting\PieceMain())->where("id",$ids)->update(["eipUpload"=>1]);
            $json_result = ["code"=>1,"msg"=>"上传成功"];
        }else{
            $json_result = ["code"=>0,"msg"=>$api_result["msg"]];
        }
        return json($json_result);
    }

    protected function _getDetail($params)
    {
        $row = [
            "purchaserHqCode" => "SGCC",
            "supplierCode" => "1000014615",
            "ipoNo" => $params["ipoNo"],
            "insideNo" => $params["insideNo"],
            "deviceNo" => $params["device_name"],
            "productBatchNo" => $params["productBatchNo"],
            "processName" => $this->orderArr[$params["order"]],
            "categoryCode" => "60",
            "subclassCode" => "60001",
            "processCode" => $params["order"],
            "woNo" => $params["woNo"],
            "planStartDate" => $params["planStartDate"],
            "actualStartDate" => $params["record_time"],
            "planFinishDate" => $params["planFinishDate"],
            "actualFinishDate" => $params["auditor_time"],
            "dataSource" => 0,
            "dataSourceCreateTime"=>date("Y-m-d H:i:s"),
            "buyerProvince" => $params["buyerProvince"]
        ];
        return $row;
    }
}
