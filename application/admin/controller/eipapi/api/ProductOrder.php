<?php

namespace app\admin\controller\eipapi\api;

use app\admin\model\chain\material\StoreOutDetail;
use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class ProductOrder extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\ProductOrder
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\eipapi\api\ProductOrderView;
        $this->ipoStatus = [
            "1" => "创建",
            "2" => "原材料检验",
            "3" => "生产中",
            "4" => "出厂试验",
            "5" => "包装入库"
        ];
        $this->assignconfig("ipoStatus",$this->ipoStatus);

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $data = [];
            foreach($list as $v){
                $data[$v["T_Num"]] = $v->toArray();
                // $data[$v["T_Num"]]["actualStartDate"] = strtotime($v["planStartDate"])<strtotime("2022-10-01")?$v["planStartDate"]:$v["actualStartDate"];
            }
            $taskModel = (new \app\admin\model\chain\sale\TaskDetail());
            $actualStartDateList = $taskModel->alias("td")
            ->join(["union_producetask_view"=>"pt"],"td.TD_ID=pt.TD_ID")
            ->JOIN(["piece_detail"=>"pd"],"pd.pt_num=pt.PT_Num")
            ->JOIN(["piece_main"=>"pm"],"pm.id=pd.piece_id")
            ->where("td.T_Num","IN",array_keys($data))
            ->group("td.T_Num")
            ->column("(SUBSTRING_INDEX(GROUP_CONCAT(td.T_Num order by pm.record_time),',',1)) as T_Num,pm.record_time");
            foreach($actualStartDateList as $k=>$v){
                $data[$k]["actualStartDate"] = $v;
            }

            $storeOutList = (new StoreOutDetail())->alias("sod")
                ->join(["storeout"=>"so"],"sod.SO_Num=so.SO_Num")
                ->where("sod.T_Num","IN",array_keys($data))
                ->group("sod.T_Num")
                ->column("(SUBSTRING_INDEX(GROUP_CONCAT(sod.T_Num order by so.SO_TakeDate desc),',',1)) as T_Num,so.SO_TakeDate");
            foreach($storeOutList as $k=>$v){
                if(isset($data[$k]) and $data[$k]["actualStartDate"]=="0000-00-00 00:00:00"){
                    $data[$k]["actualStartDate"] = $v;
                }
            }

            $result = array("total" => $list->total(), "rows" => array_values($data));

            return json($result);
        }
        return $this->view->fetch();
    }

    public function updateFinish($ids=null)
    {
        $taskModel = new \app\admin\model\chain\sale\Task();
        $row = $taskModel->get($ids);
        if(!$row) return json(["code"=>0,"msg"=>"没有找到相关信息"]);
        $taskModel = (new \app\admin\model\chain\sale\TaskDetail());
        $date = $taskModel->alias("td")
            ->join(["bsocount"=>"b"],"td.TD_ID=b.TD_ID")
            ->join(["saleinvoicedetail"=>"sid"],"b.BSO_ID=sid.BSO_ID")
            ->join(["saleinvoicemain"=>"sim"],"sid.SI_Num=sim.SIM_Num")
            ->where("td.T_Num",$ids)
            ->order("sim.SIM_Date desc")
            ->value("ifnull(sim.SIM_Date,'0000-00-00 00:00:00')");

        $result = $row->save(["actualFinishDate"=>$date]);
        if($result) return json(["code"=>1,"msg"=>"修改成功"]);
        else return json(["code"=>0,"msg"=>"修改失败,无信息更新"]);
    }

    public function eipUpload($ids=null)
    {
        $params = $this->model->field("T_Num,poItemId,materialsDesc,amount,planStartDate,planFinishDate,actualStartDate,actualFinishDate,eipUpload")->where("T_Num",$ids)->find();
        if (!$params) {
            $this->error(__('No Results were found'));
        }
        $taskModel = (new \app\admin\model\chain\sale\TaskDetail());
        $buildSqlTD = $taskModel->alias("td")
            ->join(["mergtypename"=>"mtn"],"td.TD_ID=mtn.TD_ID","LEFT")
            ->WHERE("td.T_Num",$params["T_Num"])
            ->column("(case when ifnull(mTD_ID,'')='' then td.TD_ID else mtn.mTD_ID end) as TD_ID");
        if(!empty($buildSqlTD)){
            $buildTnum = $taskModel->where("TD_ID","IN",$buildSqlTD)->column("T_Num");
            if($params["T_Num"]=="NO.20220033") $buildTnum[] = "NO.20220032";
            // $actualStartDate = $taskModel->alias("td")
            //     ->join(["producetask"=>"pt"],"td.TD_ID=pt.TD_ID")
            //     ->JOIN(["piece_detail"=>"pd"],"pd.pt_num=pt.PT_Num")
            //     ->JOIN(["piece_main"=>"pm"],"pm.id=pd.piece_id")
            //     ->where("td.TD_ID","IN",$buildSqlTD)
            //     ->order("pm.record_time")
            //     ->value("ifnull(pm.record_time,'')");
            $actualStartDate = (new \app\admin\model\chain\lofting\UnionProduceTaskView())->alias("pt")
                ->JOIN(["piece_detail"=>"pd"],"pd.pt_num=pt.PT_Num")
                ->JOIN(["piece_main"=>"pm"],"pm.id=pd.piece_id")
                ->where("pt.TD_ID","IN",$buildSqlTD)
                ->where("pm.record_time","<>","0000-00-00 00:00:00")
                ->order("pm.record_time asc")
                ->value("ifnull(pm.record_time,'')");
            $params["actualStartDate"] = $actualStartDate?$actualStartDate:'0000-00-00 00:00:00';
            $storeOutDate = (new StoreOutDetail())->alias("sod")
                ->join(["storeout"=>"so"],"sod.SO_Num=so.SO_Num")
                ->where("sod.T_Num","IN",$buildTnum)
                ->order("so.SO_TakeDate desc")
                ->value("ifnull(so.SO_TakeDate,'')");
            ($storeOutDate and $params["actualStartDate"]=="0000-00-00 00:00:00")?$params["actualStartDate"] = $storeOutDate:"";

            $params["actualFinishDate"] = $params["actualFinishDate"]??'0000-00-00 00:00:00';
            $row = $this->_getDetail($params);
            $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
            $param_url = "supplier-ipo";
            $operatetype = "ADD";
            if($params["eipUpload"]) $operatetype = 'UPDATE';
            $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$row);
            if($api_result["code"]==1){
                (new \app\admin\model\chain\sale\Task())->where("T_Num",$ids)->update(["eipUpload"=>1]);
                $json_result = ["code"=>1,"msg"=>"上传成功"];
            }else{
                $json_result = ["code"=>0,"msg"=>$api_result["msg"]];
            }
        }else{
            $json_result = ["code"=>0,"msg"=>"请重试"];
        }
        return json($json_result);
    }

    protected function _getDetail($params)
    {
        $row = [
            "purchaserHqCode"=>"SGCC",
            "ipoType"=>0,
            "supplierCode"=>"1000014615",
            "supplierName"=>"绍兴电力设备有限公司",
            "ipoNo"=>$params["T_Num"],
            "categoryCode"=>"60",
            "subclassCode"=>"60001",
            "poItemId"=>$params["poItemId"],
            "dataType"=>1,
            "materialsCode"=>"铁塔",
            "materialsName"=>"铁塔",
            "materialsUnit"=>"基",
            "materialsDesc"=>$params["materialsDesc"],
            "amount"=>$params["amount"],
            "unit"=>"基",
            "planStartDate"=>date("Y-m-d",strtotime($params["planStartDate"])),
            "planFinishDate"=>strtotime($params["planStartDate"])<strtotime($params["planFinishDate"])?date("Y-m-d",strtotime($params["planFinishDate"])):date("Y-m-d",strtotime("+6 month",strtotime($params["planStartDate"]))),
            // "actualStartDate"=>date("Y-m-d",strtotime($params["actualStartDate"])),
            "ipoStatus" => 1,
            "dataSource"=>0,
            "dataSourceCreateTime"=>date("Y-m-d H:i:s")
        ];
        if($params["actualStartDate"]!='0000-00-00 00:00:00'){
            $row["actualStartDate"] = date("Y-m-d",strtotime($params["actualStartDate"]));
            $row["ipoStatus"] = 3;
        }
        if($params["actualFinishDate"]!='0000-00-00 00:00:00'){
            $row["actualFinishDate"] = date("Y-m-d",strtotime($params["actualFinishDate"]));
            $row["ipoStatus"] = 5;
        }
        return $row;
    }
}
