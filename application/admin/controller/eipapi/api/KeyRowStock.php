<?php

namespace app\admin\controller\eipapi\api;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class KeyRowStock extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\KeyRowStock
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\eipapi\api\KeyRowStockView;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order("eipUpload asc")
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function eipUpload($ids=null)
    {
        $params = $this->model->field("IM_Class,SID_RestWeight,TD_Pressure,SOD_Length,L_Name,IM_Spec,SID_TestResult,eipUpload")->where("id",$ids)->find();
        if (!$params) {
            $this->error(__('No Results were found'));
        }
        $row = $this->_getDetail($params);
        $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
        $param_url = "supplier-process-work";
        $operatetype = "ADD";
        if($params["eipUpload"]) $operatetype = 'UPDATE';
        $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$row);
        if($api_result["code"]==1){
            (new \app\admin\model\chain\lofting\PieceMain())->where("id",$ids)->update(["eipUpload"=>1]);
            $json_result = ["code"=>1,"msg"=>"上传成功"];
        }else{
            $json_result = ["code"=>0,"msg"=>$api_result["msg"]];
        }
        return json($json_result);
    }

    protected function _getDetail($params)
    {
        $row = [
            "purchaserHqCode" => "SGCC",
            "supplierCode" => "1000014615",
            "categoryCode" => "60",
            "subclassCode" => "60001",
            "matName" => $params["IM_Class"],
            "matCode" => $params["IM_Class"]=="角钢"?"MAT_735138587162443776":"MAT_735138886191153152",
            "matNum" => "".round($params["SID_RestWeight"]/1000,2),
            // "matNum" => "0",
            "matUnit" => "吨",
            "matVoltageLevel" => '/',
            "matProdAddr" => $params["SID_TestResult"],
            "storeCity" => '浙江省绍兴市',
            "matSupplierName" => $params["V_Name"],
            "speModels" => $params["IM_Class"]=="角钢"?$params["L_Name"].','.$params["IM_Spec"].",".round($params["SID_Length"],2):($params["L_Name"].','.$params["IM_Spec"].",".round($params["SID_Length"],2)."*".round($params["SID_Width"],2)),
            "putStorageTime" => date("Y-m-d",strtotime($params["SI_WriteDate"]))
        ];
        return $row;
    }

    public function changeStock()
    {
        $keyStockViewModel = new \app\admin\model\api\KeyStockView();
        $keyStockBackModel = new \app\admin\model\api\KeyStockBack();
        // $msgList[] = ["SID_ID"=>"test","msg"=>"ok"];
        // $keyStockBackModel->allowField(true)->saveAll($msgList);
        // return 1;
        $storeInDetailModel = new \app\admin\model\chain\material\StoreInDetail();
        $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
        $param_url = "eipmatinventory";
        $keyList = $keyStockViewModel->where("status",'<>',2)->order("SID_ID ASC")->select();
        $msgList = [];$statusList = [];
        foreach($keyList as $k=>$v){
            $operatetype = "ADD";
            if($v["status"]) $operatetype = "UPDATE";
            $row = $this->_getDetail($v);
            $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$row);
            if($api_result["code"]==1){
                $msgList[] = ["SID_ID"=>$v["SID_ID"],"msg"=>$api_result["msg"]];
                $statusList[] = ["SID_ID"=>$v["SID_ID"],"status"=>$v["SID_RestWeight"]>0?1:2];
            }else{
                $msgList[] = ["SID_ID"=>$v["SID_ID"],"msg"=>$api_result["msg"]];
            }
        }
        if(!empty($statusList)) $storeInDetailModel->allowField(true)->saveAll($statusList);
        if(!empty($msgList)) $keyStockBackModel->allowField(true)->saveAll($msgList);
        die;
    }
}
