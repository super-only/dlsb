<?php

namespace app\admin\controller\eipapi\api;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class SalesOrder extends Backend
{
    
    /**
     * CgPurchase模型对象
     * @var \app\admin\model\eipapi\api\SalesOrder
     */
    protected $model = null;
    protected $noNeedLogin = "*";

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\eipapi\api\SalesOrderView;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function detail($ids=null)
    {
        $compactList = $this->model->field("C_Num,Customer_Name,poItemId,eipUpload")->where("C_Num",$ids)->find();
        if (!$compactList) {
            $this->error(__('No Results were found'));
        }
        $row = $this->_getDetail($compactList);
        $this->view->assign("row",$row);
        $this->assignconfig("list",$row["itemList"]);
        return $this->view->fetch();
    }

    public function eipUpload($ids=null)
    {
        $compactList = $this->model->field("C_Num,Customer_Name,poItemId,eipUpload")->where("C_Num",$ids)->find();
        if (!$compactList) {
            $this->error(__('No Results were found'));
        }
        $row = $this->_getDetail($compactList);
        $purchaseApiControl = new \app\admin\controller\api\PurchaseApi();
        $saveData = [];
        $param_url = "supplier-so";
        $operatetype = "ADD";
        if($compactList["eipUpload"]) $operatetype = 'UPDATE';
        $api_result = $purchaseApiControl->orderNewsApi($param_url,$operatetype,$row);
        if($api_result["code"]==1){
            (new \app\admin\model\chain\sale\Compact())->where("C_Num",$ids)->update(["eipUpload"=>1]);
            $json_result = ["code"=>1,"msg"=>"上传成功"];
        }else{
            $json_result = ["code"=>0,"msg"=>$api_result["msg"]];
        }
        return json($json_result);
    }

    protected function _getDetail($compactList)
    {
        $row = [
            "purchaserHqCode" => "SGCC",
            "soNo" => $compactList["C_Num"],
            "supplierCode" => "1000014615",
            "buyerName" => $compactList["Customer_Name"],
            "categoryCode" => "60",
            "dataSource" => "0",
            "dataSourceCreateTime" => date("Y-m-d H:i:s"),
            "itemList" => []
        ];
        $list = $this->model->alias('c')
            ->join(["salegt"=>"s"],"c.C_Num=s.C_Num")
            ->join(["sectconfigdetail"=>"scd"],"scd.SGT_ID=s.SGT_ID")
            ->field("SCD_ID,TD_TypeName,SCD_Unit,SCD_Count")
            ->where("c.C_Num",$compactList["C_Num"])
            ->select();
        foreach($list as $k=>$v){
            $row["itemList"][] = [
                "categoryCode" => "60",
                "subclassCode" => "60001",
                "soItemNo" => $v["SCD_ID"],
                "poItemId" => $compactList["poItemId"],
                "productCode" => $v["TD_TypeName"],
                "productName" => $v["TD_TypeName"],
                "productUnit" => $v["SCD_Unit"],
                "productAmount" => "".$v["SCD_Count"],
            ];
        }
        return $row;
    }
}
