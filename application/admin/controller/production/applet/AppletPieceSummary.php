<?php

namespace app\admin\controller\production\applet;

use app\admin\model\chain\lofting\UnionProduceTaskView;
use app\applet\model\piece\AppletDetailView;
use app\applet\model\piece\AppletPieceDetail;
use app\common\controller\Backend;
use think\Db;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class AppletPieceSummary extends Backend
{
    
    /**
     * AppletPieceSummary模型对象
     * @var \app\admin\model\production\applet\AppletPieceSummary
     */
    protected $model = null;
    protected $orderCon,$machineCon,$operatorModel;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\production\applet\AppletPieceSummary;
        $this->operatorModel = new \app\admin\model\production\applet\AppletPieceSummaryOperator;
        $this->orderCon = (new \app\applet\model\piece\AppletOrder())->getOrder();
        $this->machineCon = (new \app\applet\model\piece\AppletMachine())->getMachine();
        $this->view->assign("orderCon",$this->orderCon);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 查看
     */
    public function index()
    {
        $orderList = $machineList = [];
        foreach($this->orderCon as $v){
            $orderList[$v["order_id"]] = $v["name"];
        }
        foreach($this->machineCon as $v){
            $machineList[$v["machine_id"]] = $v["name"];
        }
        $this->assignconfig("orderList",$orderList);
        $this->assignconfig("machineList",$machineList);
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model->alias("m")
                ->join(["applet_machine"=>"ao"],"m.machine_id=ao.machine_id")
                ->field("m.*,ao.order_id")
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $dayId = array_column(collection($list->items())->toArray(),'day_id');
            $workList = (new AppletPieceDetail())->where("day_id","in",$dayId)->group("day_id")->column("day_id,round(sum(amount),2) as amount");
            foreach($list as &$v){
                $v["work_amount"] = $workList[$v["day_id"]]??0;
                $v["amount"] = ($workList[$v["day_id"]]??0)+$v["zg_amount"];
            }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function add()
    {
        $orderList = ["" => "请选择"];
        foreach($this->orderCon as $v){
            $orderList[$v["order_id"]] = $v["name"];
        }
        $yestoday = date("Y-m-d",strtotime("-1 day",time()));
        $this->view->assign("yestoday",$yestoday);
        $this->view->assign("orderList",$orderList);
        return $this->view->fetch();
    }

    public function edit($ids=null)
    {
        $row = $this->model->get($ids);
        if(!$row) $this->error(__('No Results were found'));
        $row["orderName"] = $this->orderCon[$this->machineCon[$row["machine_id"]]["order_id"]]["name"];
        $row["machineName"] = $this->machineCon[$row["machine_id"]]["name"];
        $flag = true;
        if($row["auditor"]) $flag = false;
        $operatorList = $this->operatorModel->where("day_id",$ids)->column("*,round(work_amount+zg_amount,2) as sum_amount");
        $list = (new AppletDetailView())->where("day_id",$ids)->order("writer_time asc")->column("*");
        $ptNumList = array_column($list,'PT_Num');
        $ptNumContent = (new UnionProduceTaskView())->alias("tp")
            ->join(["taskdetail"=>"td"],"tp.TD_ID=td.TD_ID")
            ->where("PT_Num","IN",$ptNumList)
            ->group("PT_Num")
            ->column("PT_Num,PT_Number,td.TD_Pressure");
        foreach($list as &$v){
            $v["pressure"] = isset($ptNumContent[$v["PT_Num"]])?trim(strtoupper($ptNumContent[$v["PT_Num"]]["TD_Pressure"]),"KV"):0;
            $v["tynumer"] = isset($ptNumContent[$v["PT_Num"]])?$ptNumContent[$v["PT_Num"]]["PT_Number"]:'';
            $v["operator"] = trim($v["operator"],",");
        }
        $this->view->assign("row",$row);
        $this->view->assign("flag",$flag);
        $this->assignconfig("flag",$flag);
        $this->assignconfig("peopleTable",array_values($operatorList));
        $this->assignconfig("detailTable",array_values($list));
        return $this->view->fetch();
    }


    public function getFollowMachine()
    {
        $orderId = $this->request->post("order_id");
        $machineList = (new \app\applet\model\piece\AppletMachine())->getMachine([
            "am.order_id" => ["=",$orderId]
        ]);
        $machineData = array_combine(array_keys($machineList),array_column($machineList,'name'));
        return json($machineData);

    }

    public function generateContent()
    {
        [$id,$day,$machine_id,$zg_amount] = array_values($this->request->post());
        if(!$day) return json(["code"=>0,"msg"=>"请选择结算日期","data"=>[]]);
        if(!$machine_id) return json(["code"=>0,"msg"=>"请选择设备","data"=>[]]);
        if($id){
            $mainOne = $this->model->get($id);
            if(!$mainOne) return json(["code"=>0,"msg"=>"不存在，请重新添加","data"=>[]]);
            else if($mainOne["auditor"]) return json(["code"=>0,"msg"=>"已审核，无法修改","data"=>[]]);
        }
        $mainExistOne = $this->model->where("day","=",$day)->find();
        if($mainExistOne and $mainExistOne['id']!=$id) return json(["code"=>0,"msg"=>"该日期已经做结算，请打开对应内容进行编辑","data"=>[]]);

        $betweenTime = [
            "writer_time" => ["between time",[$day." 00:00:00",$day." 23:59:59"]],
            "machine_id" => ["=",$machine_id]
        ];
        $list = (new \app\applet\model\piece\AppletDetailView())->where($betweenTime)->order("writer_time asc")->select();
        $data = $operator = [];
        $ptNumList = array_column($list,'PT_Num');
        $ptNumContent = (new UnionProduceTaskView())->alias("tp")
            ->join(["taskdetail"=>"td"],"tp.TD_ID=td.TD_ID")
            ->where("PT_Num","IN",$ptNumList)
            ->group("PT_Num")
            ->column("PT_Num,PT_Number,td.TD_Pressure");
        foreach($list as $k=>$v){
            $data[$k] = $v->toArray();
            $pressure = isset($ptNumContent[$v["PT_Num"]])?trim(strtoupper($ptNumContent[$v["PT_Num"]]["TD_Pressure"]),"KV"):0;
            $tynumber = isset($ptNumContent[$v["PT_Num"]])?$ptNumContent[$v["PT_Num"]]["PT_Number"]:0;
            $data[$k]["pressure"] = $pressure;
            $data[$k]["tynumer"] = $tynumber;
            $data[$k]["operator"] = trim($v["operator"],',');
            $content = [
                "pressure" => $pressure,
                "tynumber" => $tynumber,
                "stuff" => $v["stuff"],
                "spec" => $v["spec"],
                "torch" => $v["torch"],
                "machine_id" => $v["machine_id"],
                "order_id" => $v["order_id"],
                "weight" => $v["weight"],
                "hole" => $v["hole"],
                "meter" => $v["meter"],
                "price_type" => $v["price_type"],
            ];
            [$data[$k]["price"],$data[$k]["amount"]] = $this->sumPriceCalcul($content);
            $operatorList = explode(",",trim($v["operator"],','));
            foreach($operatorList as $vv){
                isset($operator[$vv])?"":$operator[$vv]=[];
                $operator[$vv][] = $data[$k]["amount"]/count($operatorList);
            }
        }
        $operatorSum = [];
        foreach($operator as $k=>$v){
            $work_ave_amount = round(array_sum($v),3);
            $zg_ave_amount = round($zg_amount/count($operator),3);
            $operatorSum[$k] = [
                "operator" => $k,
                "hours" => 8,
                "work_amount" => $work_ave_amount,
                "zg_amount" => $zg_ave_amount,
                "sum_amount" => round($work_ave_amount+$zg_ave_amount,2)
            ];
        }
        return json([
            "code" => 1,
            "msg" => "成功",
            "data" => [
                "peopleData" => array_values($operatorSum),
                "detailData" => array_values($data)
            ]
        ]);

    }

    public function save()
    {
        [$id,$day,$machine_id,$people,$data] = array_values($this->request->post());
        if($id){
            $mainOne = $this->model->get($id);
            if(!$mainOne) return json(["code"=>0,"msg"=>"不存在，请重新添加","data"=>[]]);
            else if($mainOne["auditor"]) return json(["code"=>0,"msg"=>"已审核，无法修改","data"=>[]]);
        }

        if(!$day) return json(["code"=>0,"msg"=>"请选择结算日期","data"=>[]]);
        if(!$machine_id) return json(["code"=>0,"msg"=>"请选择设备","data"=>[]]);
        if($id){
            $mainOne = $this->model->get($id);
            if(!$mainOne) return json(["code"=>0,"msg"=>"不存在，请重新添加","data"=>[]]);
            else if($mainOne["auditor"]) return json(["code"=>0,"msg"=>"已审核，无法修改","data"=>[]]);
            else if($mainOne["day"]!=$day) return json(["code"=>0,"msg"=>"修改无法修改日期，请删除原信息后重试","data"=>[]]);
        }
        $mainExistOne = $this->model->where("day","=",$day)->find();
        if($mainExistOne and $mainExistOne["id"]!=$id) return json(["code"=>0,"msg"=>"该日期已经做结算，请打开对应内容进行编辑","data"=>[]]);
        if(!$people) return json(["code"=>0,"msg"=>"操作人员不能为空","data"=>[]]);
        $peopleList = json_decode($people,true);
        if(empty($peopleList)) return json(["code"=>0,"msg"=>"操作人员不能为空","data"=>[]]);
        $dataList = json_decode($data,true);
        Db::startTrans();
        try {
            $zg_amount = array_sum(array_column($peopleList,'zg_amount'));
            $mainRow = [
                // "day_id" => $id,
                "day" => $day,
                "zg_amount" => $zg_amount,
                "writer" => $this->view->admin["username"],
                "machine_id" => $machine_id
            ];
            if(!$id) $id = $this->model->insertGetId($mainRow);
            else $this->model->where("day_id","=",$id)->update($mainRow);
            
            $existList = $this->operatorModel->where("day_id","=",$id)->column("operator,id");
            $operatorList = [];
            foreach($peopleList as $k=>$v){
                if(!round($v["hours"],2) and !round($v["zg_amount"],2) and !round($v["work_amount"],2)) continue;
                $operatorList[$k] = [
                    "day_id" => $id,
                    "operator" => $v["operator"],
                    "hours" => $v["hours"],
                    "zg_amount" => $v["zg_amount"],
                    "work_amount" => $v["work_amount"],
                ];
                if(isset($existList[$v["operator"]])){
                    $operatorList[$k]["id"] = $existList[$v["operator"]];
                    unset($existList[$k]);
                }
            }
            if(!empty($existList)) $this->operatorModel->where("id","in",array_values($existList))->delete();
            $this->operatorModel->allowField(true)->saveAll($operatorList);
            if(!empty($dataList)){
                foreach($dataList as $k=>$v){
                    $dataList[$k]["day_id"] = $id;
                }
                (new AppletPieceDetail())->allowField(true)->saveAll($dataList);
            }
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return json(["code"=>0,"msg"=>$e->getMessage(),"data"=>[]]);
        }
        return json(["code"=>1,"msg"=>"成功","data"=>["id"=>$id]]);
    }

    // 审核 弃审 删除
    public function auditor($ids=null)
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = 'day_id';
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->select();
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->save([
                        "auditor" => $this->view->admin["username"],
                        "auditor_time" => date("Y-m-d H:i:s")
                    ]);
                }
                $result = (new AppletPieceDetail())->where($pk,"in",$ids)->update([
                    "auditor" => $this->view->admin["username"],
                    "auditor_time" => date("Y-m-d H:i:s")
                ]);
                Db::commit();
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
    }

    public function giveUp($ids=null)
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = 'day_id';
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","<>","")->select();
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->save([
                        "auditor" => '',
                        "auditor_time" => "0000-00-00 00:00:00"
                    ]);
                }
                $result = (new AppletPieceDetail())->where($pk,"in",$ids)->update([
                    "auditor" => '',
                    "auditor_time" => "0000-00-00 00:00:00"
                ]);
                Db::commit();
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
    }

    // 删除
    public function del($ids=null)
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->select();
            $count = 0;
            Db::startTrans();
            try {
                foreach ($list as $k => $v) {
                    $count += $v->delete();
                }
                $this->operatorModel->where($pk,"in",$ids)->delete();
                $result = (new AppletPieceDetail())->where($pk,"in",$ids)->update([
                    "price" => 0,
                    "amount" => 0,
                    "day_id" => 0
                ]);
                Db::commit();
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    protected function sumPriceCalcul($content=[])
    {
        $price_type_list = [0=>"weight",1=>"hole",2=>"meter"];
        $zk = 1;
        $price = $amount = 0;
        if(empty($content)) return [
            "price" => $price,
            "amount" => $amount
        ];
        $specification = 0;
        $fh = mb_substr($content["spec"],0,1);
        if($fh=="∠"){
            preg_match_all("/\d+\.?\d*/",$content["spec"],$matches);
            $x = $matches[0][0]?((float)$matches[0][0]):0;
            $y = $matches[0][1]?((float)$matches[0][1]):0;
            $specification = $x*1000+$y;
        }else if($fh=="-") $specification = abs($content["spec"]);
        else return [
            "price" => $price,
            "amount" => $amount
        ];
        // 电焊9-18 等离子5 切角3 刨床6 火曲7 落料1 制孔2 钢印9
        if($content["order_id"]==9){
            if($fh=="∠"){
                if($content["torch"]>=16){
                    $price = 0.038;
                    if(($content["pressure"]=="220" and $content["tynumber"]>=2) or ($content["pressure"]>=500 and $content["tynumber"]>=2)) $price=0.024;
                }else{
                    $price = 0.06;
                    if(($content["pressure"]=="220" and $content["tynumber"]>=2) or ($content["pressure"]>=500 and $content["tynumber"]>=2)) $price=0.035;
                }
            }else $price =  $specification>=30?0.06:0.1;
        }else if($content["order_id"]==12) $price = 0.1;
        else if($content["order_id"]==13) $price = $content["pressure"]>=500?0.053:0.085;
        else if($content["order_id"]==14) $price = $specification>=20?0.08:($specification<=14?0.135:0.097);
        else if($content["order_id"]==15){
            if($content["pressure"]<=110) $price = $specification>=30?0.205:0.36;
            else if($content["pressure"]>=220) $price = 0.185;
        }else if($content["order_id"]==16) $price = $specification>=20?0.2:($specification<=14?0.285:0.201);
        else if($content["order_id"]==17) $price = 0.105;
        else if($content["order_id"]==18) $price = $specification>=200000?0.055:($specification<=160000?0.125:0.0895);
        else if($content["order_id"]==5) $price = $specification>=14?0.63:($specification<=8?0.4:0.54);
        else if($content["order_id"]==3){
            if($specification>=40003 and $specification<=45004){
                if($content["tynumber"]<=2) $price=0.040;//+38;
                else $price=0.032;//+38;
            }else if($specification>=50004 and $specification<81000){
                if($content["tynumber"]<=2) $price=0.020;//+38;
                else $price=0.018;//+38;
            }else if($specification>=90006 and $specification<=140010){
                if($content["tynumber"]<=2) $price=0.017;//+38;
                else $price=0.015;//+38;
            }else if($specification>140010){
                if($content["tynumber"]<=2) $price=0.013;//+38;
                else $price=0.011;//+38;
            }
        }else if($content["order_id"]==6){
            if($specification<110000){
                $price = 0.183;
                if($content["pressure"]>=500 or $content["tynumber"]>=3) $price = 0.134;
            }else if($specification>=110000 and $specification<141000){
                $price = 0.139;
                if($content["pressure"]>=500 or $content["tynumber"]>=3) $price = 0.098;
            }else if($specification>=160000 and $specification<181000){
                $price = 0.113;
                if($content["pressure"]>=500 or $content["tynumber"]>=3) $price = 0.079;
            }else if($specification>=200000 and $specification<221000){
                $price = 0.101;
                if($content["pressure"]>=500 or $content["tynumber"]>=3) $price = 0.071;
            }else if($specification>=250000 and $specification<251000){
                $price = 0.090;
                if($content["pressure"]>=500 or $content["tynumber"]>=3) $price = 0.064;
            }else if($specification>=251000) $price=0.053;
        }else if($content["order_id"]==7){
            if($content["stuff"]=="角钢"){
                if($content["tynumber"]<=2) $price=0.041;
                else $price=0.036;
            }else{
                if($content["torch"]<12){
                    if($content["tynumber"]<=2) $price = 0.044;
                    else $price = 0.036;
                }else if($content["torch"]<=14){
                    if($content["tynumber"]<=2) $price = 0.044;
                    else if($content["pressure"]<500) $price = 0.036;
                    else $price = 0.030;
                }else if($content["torch"]<=18){
                    if($content["tynumber"]<=2) $price = 0.044;
                    else if($content["pressure"]<500) $price = 0.036;
                    else $price = 0.026;
                }else{
                    if($content["tynumber"]<=2) $price = 0.044;
                    else if($content["pressure"]<500) $price = 0.036;
                    else $price = 0.024;
                }
                if($content["pressure"]<220 and $content["tynumber"]>=5) $zk=0.95;
                if(($content["pressure"]>=220 and $content["tynumber"]>=4) or ($content["pressure"]>=500 and $content["tynumber"]>=3)) $zk=0.92;
            }
        }else if($content["order_id"]==1){
            if($content["machine_id"]==1){
                if($specification>=40003 and $specification<=45005){
                    if($content["tynumber"]<=2) $price = 0.057;
                    else $price = 0.046;
                }else if($specification>=50004 and $specification<=50005){
                    if($content["tynumber"]<=2) $price = 0.050;
                    else $price = 0.040;
                }elseif($specification>=56004){
                    if($content["tynumber"]<=2) $price = 0.035;
                    else $price = 0.032;
                }
                if($content["pressure"]<220 and $content["tynumber"]>=5) $zk=0.95;
                if(($content["pressure"]>=220 and $content["tynumber"]>=4) or ($content["pressure"]>=500 and $content["tynumber"]>=3)) $zk=0.92;
            }else if(in_array($content["machine_id"],[2,3,4,5,6])){
                if($specification<56000){
                    if($content["tynumber"]<=2) $price=0.100;
                    else $price=0.075;
                }else if($specification<64000){
                    if($content["tynumber"]<=2) $price=0.067;
                    else $price=0.061;
                }else if($specification>=70000 and $specification<76000){
                    if($content["tynumber"]<=2) $price=0.057;
                    else $price=0.052;
                }else if($specification>=80000 and $specification<91000){
                    if($content["tynumber"]<=2) $price=0.041;
                    else if($content["pressure"]==220) $price=0.034;
                    // else if($content["pressure"]<500) $amount=34;
                    else $price=0.032;
                }else if($specification>=100000 and $specification<126000){
                    if($content["tynumber"]<=2) $price=0.038;
                    else if($content["pressure"]==220) $price=0.032;
                    // else if($content["pressure"]<500) $amount=32;
                    else $price=0.030;
                }else if($specification>=140000 and $specification<141000){
                    if($content["tynumber"]<=2) $price=0.036;
                    else if($content["pressure"]==220) $price=0.0305;
                    // else if($content["pressure"]<500) $amount=30.5;
                    else $price=0.029;
                }else if($specification>=160000 and $specification<181000){
                    if($content["tynumber"]<=2) $price=0.034;
                    else if($content["pressure"]==220) $price=0.0265;
                    // else if($content["pressure"]<500) $amount=26.5;
                    else $price=0.023;
                }
                if($content["pressure"]<220 and $content["tynumber"]>=5) $zk=0.95;
                if($content["pressure"]>=220 and $content["tynumber"]>=4) $zk=0.92;
                if(($content["pressure"]>=500 and $content["tynumber"]>=3) and $specification>=90000) $zk=0.9;
            }else if($content["machine_id"]==30){
                if($content["torch"]>=2 and $content["torch"]<=5){
                    if($content["tynumber"]<=2) $price = 0.144;
                    else $price = 0.136;
                }else if($content["torch"]==6){
                    if($content["tynumber"]<=2) $price = 0.130;
                    else $price = 0.121;
                }else if($content["torch"]==8){
                    if($content["tynumber"]<=2) $price = 0.100;
                    else $price = 0.090;
                }else if($content["torch"]>=10 and $content["torch"]<=12){
                    if($content["tynumber"]<=2) $price = 0.083;
                    else $price = 0.066;
                }else if($content["torch"]>=14 and $content["torch"]<=16){
                    if($content["tynumber"]<=2) $price = 0.065;
                    else $price = 0.058;
                }
                if($content["pressure"]<220 and $content["tynumber"]>=5) $zk=0.95;
                if(($content["pressure"]>=220 and $content["tynumber"]>=4) or ($content["pressure"]>=500 and $content["tynumber"]>=3)) $zk=0.92;
            }else if(in_array($content["machine_id"],[31,32])){
                if($content["torch"]<=8) $price = 0.4;
                else if($content["torch"]<=12) $price = 0.54;
                else $price = 0.63;
            }
        }else if($content["order_id"]==2){
            if($content["machine_id"]==8){
                if($specification>=40003 and $specification<=45005){
                    if($content["tynumber"]<=2) $price = 0.115;
                    else $price = 0.086;
                }else if($specification>=50004 and $specification<=50005){
                    if($content["tynumber"]<=2) $price = 0.100;
                    else $price = 0.075;
                }elseif($specification>=56004){
                    if($content["tynumber"]<=2) $price = 0.077;
                    else $price = 0.060;
                }
                if($content["pressure"]<220 and $content["tynumber"]>=5) $zk=0.95;
                if(($content["pressure"]>=220 and $content["tynumber"]>=4) or ($content["pressure"]>=500 and $content["tynumber"]>=3)) $zk=0.92;
            }else if(in_array($content["machine_id"],[7,26])){
                if($content["torch"]<=16){
                    $price = 0.19;
                    if($content["tynumber"]>=3){
                        if($content["pressure"]>=800) $price = 0.2;
                        elseif($content["pressure"]>=1000) $price = 0.21;
                    }
                }else if($content["torch"]==18){
                    $price = 0.21;
                    if($content["tynumber"]>=3){
                        if($content["pressure"]>=800) $price = 0.22;
                        elseif($content["pressure"]>=1000) $price = 0.23;
                    }
                }else if($content["torch"]==20){
                    $price = 0.22;
                    if($content["tynumber"]>=3){
                        if($content["pressure"]>=800) $price = 0.23;
                        elseif($content["pressure"]>=1000) $price = 0.24;
                    }
                }else if($content["torch"]==22){
                    $price = 0.23;
                    if($content["tynumber"]>=3){
                        if($content["pressure"]>=800) $price = 0.24;
                        elseif($content["pressure"]>=1000) $price = 0.25;
                    }
                }else if($content["torch"]==24){
                    $price = 0.24;
                    if($content["tynumber"]>=3){
                        if($content["pressure"]>=800) $price = 0.25;
                        elseif($content["pressure"]>=1000) $price = 0.26;
                    }
                }else if($content["torch"]>=26 and $content["torch"]<=28){
                    $price = 0.25;
                    if($content["tynumber"]>=3){
                        if($content["pressure"]>=800) $price = 0.26;
                        elseif($content["pressure"]>=1000) $price = 0.27;
                    }
                }else if($content["torch"]==30){
                    $price = 0.26;
                    if($content["tynumber"]>=3){
                        if($content["pressure"]>=800) $price = 0.27;
                        elseif($content["pressure"]>=1000) $price = 0.29;
                    }
                }else if($content["torch"]>=32){
                    $price = 0.30;
                    if($content["tynumber"]>=3){
                        if($content["pressure"]>=800) $price = 0.32;
                        elseif($content["pressure"]>=1000) $price = 0.33;
                    }
                }
            }else if(in_array($content["machine_id"],[9,27,28])){
                if($content["torch"]<=6){
                    if($content["tynumber"]<=2) $price=0.170;//+38;
                    else $price=0.158;//+38;
                }else if($content["torch"]==8){
                    if($content["tynumber"]<=2) $price=0.089;//+38;
                    else $price=0.081;//+38;
                }else if($content["torch"]>=10){
                    if($content["tynumber"]<=2) $price=0.064;//+17;
                    else $price=0.056;//+17;
                }
            }else if($content["machine_id"]==33){
                if($content["torch"]<=12){
                    if($content["tynumber"]<=2) $price=0.155;//+38;
                    else $price=0.150;//+38;
                }else if($content["torch"]<=16){
                    if($content["tynumber"]<=2) $price=0.125;//+38;
                    else $price=0.122;//+38;
                }else if($content["torch"]<=24){
                    if($content["tynumber"]<=2) $price=0.104;//+17;
                    else $price=0.102;//+17;
                }else if($content["torch"]<=32){
                    if($content["tynumber"]<=2) $price=0.092;//+17;
                    else $price=0.089;//+17;
                }else if($content["torch"]<=50){
                    if($content["tynumber"]<=2) $price=0.063;//+17;
                    else $price=0.059;//+17;
                }else if($content["torch"]<=70) $price=0.082;
                else $price=0.093;
            }
        }else if($content["order_id"]==9) $price = $content["torch"]<10?0.038:0.035;
        $price = $price*$zk;
        $amount = $price*$content[$price_type_list[$content["price_type"]]];
        return [$price,$amount];
    }
}
