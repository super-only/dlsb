<?php

namespace app\admin\controller\production\applet;

use app\admin\model\chain\lofting\UnionProduceTaskView;
use app\common\controller\Backend;
use think\Db;
use Exception;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class AppletPieceList extends Backend
{
    
    /**
     * AppletPieceList
     * @var \app\admin\model\production\applet\AppletPieceList
     */
    protected $model = null;
    protected $viewModel;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\applet\model\piece\AppletPieceDetail;
        $this->viewModel = new \app\applet\model\piece\AppletDetailView;
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    public function index()
    {
        $this->model = $this->viewModel;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            $ptNumList = array_column(collection($list->items())->toArray(),'PT_Num');
            $ptNumContent = (new UnionProduceTaskView())->alias("tp")
                ->join(["taskdetail"=>"td"],"tp.TD_ID=td.TD_ID")
                ->where("PT_Num","IN",$ptNumList)
                ->group("PT_Num")
                ->column("PT_Num,PT_Number,td.TD_Pressure");
            foreach($list as $k=>$v){
                $list[$k]["pressure"] = isset($ptNumContent[$v["PT_Num"]])?trim(strtoupper($ptNumContent[$v["PT_Num"]]["TD_Pressure"]),"KV"):0;
                $list[$k]["tynumer"] = isset($ptNumContent[$v["PT_Num"]])?$ptNumContent[$v["PT_Num"]]["PT_Number"]:'';
                $list[$k]["operator"] = trim($v["operator"],",");
            }
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }
    
}
