<?php

namespace app\admin\controller\production\piece;

use app\admin\model\chain\lofting\PieceMain;
use app\admin\model\chain\sale\TaskDetail;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 焊接组计件情况
 *
 * @icon fa fa-circle-o
 */
class WeldingMain extends Backend
{
    
    /**
     * WeldingMain模型对象
     * @var \app\admin\model\production\piece\WeldingMain
     */
    protected $model = null,$admin = null;
    protected $noNeedLogin = ["getMachineUser",'getPieceMain'];
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\production\piece\WeldingMain;
        $this->admin = \think\Session::get('admin');
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * 添加
     */
    public function add()
    {
        $this->assignconfig("init_data",[]);
        if ($this->request->isPost()) {
            $params = $this->request->post("main/a");
            $row = $this->request->post("row/a");
            if ($params and $row) {
                $params = $this->preExcludeFields($params);
                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $resultParams = [];
                foreach($row["id_pt_num"] as $k=>$v){
                    $id_pt_num = explode("_",$v);
                    $resultParams[] = [
                        "record_time" => $params["record_time"],
                        "piece_main_id" => $id_pt_num[0],
                        "pt_num" => $id_pt_num[1],
                        "gas_up" => $row["gas_up"][$k]?$row["gas_up"][$k]:0,
                        "gas_down" => $row["gas_down"][$k]?$row["gas_down"][$k]:0,
                        "gas_plate_range" => $row["gas_plate_range"][$k]?$row["gas_plate_range"][$k]:0,
                        "gas_plate_up" => $row["gas_plate_up"][$k]?$row["gas_plate_up"][$k]:0,
                        "gas_steel" => $row["gas_steel"][$k]?$row["gas_steel"][$k]:0,
                        "team_special" => $row["team_special"][$k]?$row["team_special"][$k]:0,
                        "team_foot" => $row["team_foot"][$k]?$row["team_foot"][$k]:0,
                        "team_plate_down" => $row["team_plate_down"][$k]?$row["team_plate_down"][$k]:0,
                        "team_plate_up" => $row["team_plate_up"][$k]?$row["team_plate_up"][$k]:0,
                        "team_iron" => $row["team_iron"][$k]?$row["team_iron"][$k]:0,
                        "team_foot_up" => $row["team_foot_up"][$k]?$row["team_foot_up"][$k]:0,
                        "weld_floor_down" => $row["gas_steel"][$k]?$row["gas_steel"][$k]:0,
                        "weld_floor_up" => $row["team_special"][$k]?$row["team_special"][$k]:0,
                        "weld_floor" => $row["team_foot"][$k]?$row["team_foot"][$k]:0,
                        "weld_cross_low" => $row["team_plate_down"][$k]?$row["team_plate_down"][$k]:0,
                        "weld_cross_medium" => $row["team_plate_up"][$k]?$row["team_plate_up"][$k]:0,
                        "weld_corss_long" => $row["team_iron"][$k]?$row["team_iron"][$k]:0,
                        "weld_anchor_bolt" => $row["team_foot_up"][$k]?$row["team_foot_up"][$k]:0,
                        "weld_bolt_medium" => $row["gas_steel"][$k]?$row["gas_steel"][$k]:0,
                        "weld_bolt_up" => $row["team_special"][$k]?$row["team_special"][$k]:0,
                        "weld_plate_down" => $row["team_foot"][$k]?$row["team_foot"][$k]:0,
                        "weld_plate_up" => $row["team_plate_down"][$k]?$row["team_plate_down"][$k]:0,
                        "weld_special" => $row["team_plate_up"][$k]?$row["team_plate_up"][$k]:0,
                        "weld_strengthen_down" => $row["team_iron"][$k]?$row["team_iron"][$k]:0,
                        "weld_strengthen_up" => $row["team_foot_up"][$k]?$row["team_foot_up"][$k]:0,
                        "writer" => $this->admin["username"],
                    ];
                }
                $result = false;
                Db::startTrans();
                try {
                    $result = $this->model->allowField(true)->saveAll($resultParams);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $tower_name = (new TaskDetail())->alias("td")->join(["producetask"=>"pt"],"pt.TD_ID=td.TD_ID")->where("pt.PT_Num",$row["pt_num"])->value("TD_TypeName");
        $row["tower_name"] = $tower_name;
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        $field = [
            "gas_up",
            "gas_down",
            "gas_plate_range",
            "gas_plate_up",
            "gas_steel",
            "gas_steel_special",
            "team_special",
            "team_foot",
            "team_plate_down",
            "team_plate_up",
            "team_iron",
            "team_foot_up",
            "weld_floor_down",
            "weld_floor_up",
            "weld_floor",
            "weld_cross_low",
            "weld_cross_medium",
            "weld_corss_long",
            "weld_anchor_bolt",
            "weld_bolt_medium",
            "weld_bolt_up",
            "weld_plate_down",
            "weld_plate_up",
            "weld_special",
            "weld_strengthen_down",
            "weld_strengthen_up",
        ];
        foreach($field as $v){
            $row[$v] = round($row[$v],5);
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    foreach($params as $k=>$v){
                        if(!$v) unset($params[$k]);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $pieceMainModel = new PieceMain();
        $list = $pieceMainModel->alias("pm")
            ->join(["piece_detail"=>"pd"],"pm.id=pd.piece_id")
            ->join(["piece_content"=>"pc"],"pd.piece_content_id=pc.id")
            ->where([
                "pm.id" => ["=",$row["piece_main_id"]],
                "pd.pt_num" => ["=",$row["pt_num"]],
                "pd.pid" => ["=",0],
                "pm.device" => ["=",8]
            ])->field("pm.id,pd.pt_num,pd.sum_weight,pd.hole_number,pc.stuff,pc.specification,pc.material,case stuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->order("pd.pt_num,clsort,pc.stuff,pc.material,bh,specification")
            ->select();
        $list = $list?collection($list)->toArray():[];
        foreach($list as $k=>$v){
            $list[$k]["idfield"] = $k;
        }
        $this->assignconfig("list",$list);
        $this->view->assign("row", $row);
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    /**
     * 审核
     */
    public function auditor($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","=","")->update([
                    "auditor" => $this->admin["username"],
                    "auditor_time" => date("Y-m-d H:i:s")
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    /**
     * 审核
     */
    public function giveUp($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $pk = $this->model->getPk();
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }

            $count = 0;
            Db::startTrans();
            try {
                $count = $this->model->where($pk, 'in', $ids)->where("auditor","<>","")->update([
                    "auditor" => '',
                    "auditor_time" => "0000-00-00 00:00:00"
                ]);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error("未更新行");
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function getMachineUser()
    {
        $record_time = $this->request->post("record_time");
        if(!$record_time) return json(["code"=>0,"msg"=>"无时间","data"=>[]]);
        $start = date("Y-m-d 00:00:00",strtotime($record_time));
        $end = date("Y-m-d 23:59:59",strtotime($record_time));
        $machineUserList = (new PieceMain())->alias("pm")
            ->join(["piece_operator"=>"pd"],"pm.id=pd.piece_main_id")
            ->where("pd.operator","<>","")
            ->where("pm.device",8)
            ->where("pm.record_time","between",[$start,$end])
            ->column("pd.operator");
        return json(["code"=>1,"msg"=>"成功","data"=>$machineUserList]);
    }

    public function getPieceMain()
    {
        $record_time = $this->request->post("record_time");
        if(!$record_time) return json(["code"=>0,"msg"=>"请选择时间","data"=>[]]);
        $record_start = date("Y-m-d 00:00:00",strtotime($record_time));
        $record_end = date("Y-m-d 23:59:59",strtotime($record_time));
        $pieceMainModel = new PieceMain();
        $list = $pieceMainModel->alias("pm")
            ->join(["piece_detail"=>"pd"],"pm.id=pd.piece_id")
            ->join(["piece_content"=>"pc"],"pd.piece_content_id=pc.id")
            ->where([
                "pm.record_time" => ["between",[$record_start,$record_end]],
                "pd.pid" => ["=",0],
                "pm.device" => ["=",8]
            ])->field("pm.id,pd.pt_num,pd.sum_weight,pd.hole_number,pc.stuff,pc.specification,pc.material,case stuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->order("pd.pt_num,clsort,pc.stuff,pc.material,bh,specification")
            ->select();
        $data = $idList = $ptNumList = [];
        foreach($list as $v){
            $idList[$v["pt_num"]] = $v["id"];
            $v["key"] = $v["id"].'_'.$v["pt_num"];
            $data[$v["key"]][] = $v->toArray();
        }
        foreach($data as $k=>$v){
            foreach($v as $kk=>$vv){
                $data[$k][$kk]["idfield"] = $kk;
            }
        }
        $ptNumList = (new TaskDetail())->alias("td")
            ->join(["producetask"=>"pt"],"td.TD_ID=pt.TD_ID")
            ->where("pt.PT_Num","in",array_keys($idList))
            ->column("pt.PT_Num,td.TD_TypeName");
        if(empty($data)) return json(["code"=>0,"msg"=>"无计件信息","data"=>[]]);
        return json(["code"=>1,"msg"=>"成功","data"=>["idList"=>array_unique(array_values($idList)),"data"=>$data,"ptNumList"=>$ptNumList]]);
    }
}
