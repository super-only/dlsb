<?php

namespace app\admin\controller\production\piece;

use app\admin\model\jichu\jg\EmployeeWagesDetail;
use app\admin\model\jichu\jg\EmployeeWagesMain;
use app\admin\model\production\piece\PieceZgAmountView;
use app\common\controller\Backend;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use Exception;

/**
 * 计件工资
 *
 * @icon fa fa-circle-o
 */
class PieceSummary extends Backend
{
    
    /**
     * PieceSummary模型对象
     * @var \app\admin\model\production\piece\PieceSummary
     */
    protected $model = null,$viewModel=null,$detailModel=null,$amountModel=null,$admin = null;
    protected $typeList = [0=>"铁件组",1=>"焊接组"];
    protected $noNeedLogin = ["summaryTest","detailSummary"];

    public function _initialize()
    {
        parent::_initialize();
        $this->admin = \think\Session::get('admin');
        $this->model = new \app\admin\model\production\piece\PieceSummary;
        $this->viewModel = new \app\admin\model\production\piece\PieceAverageView;
        $this->detailModel = new \app\admin\model\production\piece\PieceSummaryDetail;
        $this->amountModel = new \app\admin\model\production\piece\PieceSummaryAmount;
        $this->view->assign("typeList",$this->typeList);
        $this->assignconfig("typeList",$this->typeList);
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    
    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                // list($code,$msg) = $this->model->judRepete($params["DtM_sTypeName"],0);
                // if(!$code) $this->error($msg);
                if(!$params["record_time"]) $this->error("结算时间必填");
                $j_num = date("Ym",strtotime($params["record_time"]));
                $one = $this->model->where(["j_num"=>["=",$j_num],"type"=>["=",$params["type"]]])->value("id");
                if($one) $this->success('成功！',null,$one);
                $params["j_num"] = $j_num;
                $params["record_time"] = date("Y-m-d H:i:s",strtotime($params["record_time"]));
                $params["writer"] = $this->admin["nickname"];
                $result = $this->model::create($params);
                if ($result) {
                    $this->success('成功！',null,$result["id"]);
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            if($row["auditor"]) $this->error("已审核无法修改");
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $defaultKey = $this->keyList();
                    foreach($defaultKey as $v){
                        if(isset($params[$v]) && $params[$v]==1) $this->model->where($v,1)->update([$v => 0]);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $list = $this->detailModel->where("j_id",$ids)->order("operator")->select();
        $list = $list?collection($list)->toArray():[];
        $this->view->assign("row", $row);
        $this->assignconfig("tableList",$list);
        $this->assignconfig("ids",$ids);
        $this->view->assign("flag",$row["auditor"]?false:true);
        return $this->view->fetch();
    }

    public function summary($peoNum='')
    {
        $ids = $this->request->post("ids");
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row["auditor"]) $this->error("已审核无法修改");
        $start_time = date("Y-m-d 00:00:00",strtotime($row["record_time"]));
        $end_time = date("Y-m-d 23:59:59",strtotime("-1 day",strtotime("+1 month",strtotime($row["record_time"]))));
        //焊接组的
        $detailWhere = [
            "record_time" => ["between",[$start_time,$end_time]]
        ];
        // 1.电焊审核 2.计件审核 3.等离子/钢印审核
        // 电焊model
        $weldingMainModel = new \app\admin\model\production\piece\WeldingMain;
        $noAuditorList = $weldingMainModel->where($detailWhere)->where("auditor","=","")->column("id");
        $noAuditorList = $noAuditorList?collection($noAuditorList)->toArray():[];
        if(!empty($noAuditorList)) $this->error("ID为".implode(",",$noAuditorList)."的电焊未审核，请先审核");
        //计件model
        $pieceMainModel = new \app\admin\model\chain\lofting\PieceMain();
        $noAuditorList = $pieceMainModel->where($detailWhere)->where("auditor","=","")->column("id");
        if(!empty($noAuditorList)) $this->error("ID为".implode(",",$noAuditorList)."的计件未审核，请先审核");
        // 等离子/钢印model
        $pieceOtherMainModel = new \app\admin\model\chain\lofting\PieceOtherMain();
        $noAuditorList = $pieceOtherMainModel->where($detailWhere)->where("auditor","=","")->column("id");
        if(!empty($noAuditorList)) $this->error("ID为".implode(",",$noAuditorList)."的等离子/钢印未审核，请先审核");

        // 今日每个机器工作个人工作小时以及总小时 所有的人
        $timeList = DB::QUERY("select a.device,a.record_time,a.operator,sum(a.hours) as hours from 
        (select pm.id,cast(pm.device as UNSIGNED) as device,left(pm.record_time,10) as record_time,po.operator,po.hours from piece_main pm inner join piece_operator po on pm.id=po.piece_main_id where (record_time BETWEEN '".$start_time."' and '".$end_time."')
        union
        select pm.id,cast(pm.device as UNSIGNED) as device,left(pm.record_time,10) as record_time,po.operator,po.hours from piece_other_main pm inner join piece_other_operator po on pm.id=po.piece_main_id where (record_time BETWEEN '".$start_time."' and '".$end_time."') and pm.device=15) a 
         group by a.record_time,a.device,a.operator order by a.record_time,a.device,a.operator");
        $timePeoList = $peoList = [];
        foreach($timeList as $k=>$v){
            $peoList[$v["operator"]] = [
                "operator" => $v["operator"],
                "j_id" => $ids
            ];
            $timePeoList[$v["record_time"]][$v["device"]]["gr"][$v["operator"]] = $v["hours"];
        }
        if(empty($peoList)) $this->error("本月工作人员的没有计件信息，请稍后再试");
        foreach($timePeoList as $k=>$v){
            foreach($v as $kk=>$vv){
                $timePeoList[$k][$kk]["hj"] = array_sum(array_values($vv["gr"]));
            }
        }
        $detailArr = [];
        // 杂工工资 包括
        $zgList = DB::QUERY("select record_time,device,sum(zg) as zg from (
            select left(record_time,10) as record_time,cast(device as UNSIGNED) as device,(case when device=12 then zg_hours else zg_amount end) as zg from piece_main where (record_time BETWEEN '".$start_time."' and '".$end_time."')
            union 
            select left(record_time,10) as record_time,cast(device as UNSIGNED) as device,(case when device=12 then zg_hours else zg_amount end) as zg from piece_other_main where (record_time BETWEEN '".$start_time."' and '".$end_time."') and device=15
            ) as a group by record_time,device order by record_time,device");
        $zgDeviceList = [];
        foreach($zgList as $k=>$v){
            // if($v["zg"]>0){
                $zgDeviceList[$v["record_time"]][$v["device"]] = [
                    "device" => 0,
                    "workmanship" => 0,
                    "stuff" => "杂工",
                    "specification" => "杂工",
                    "material" => "杂工",
                    "pt_number" => '',
                    "pressure" => '',
                    "n_value" => $v["zg"],
                    "n_type" => 1,
                    "price" => 1,
                    "amount" => 1*$v["zg"],
                    "pt_num" => '',
                    "jj_time" => $v["record_time"]
                ];
            // }
        }
        // 获取所有pt_num的数量和电压
        $ptNewArr = DB::QUERY("select pt.pt_num,ptd.PT_Number,td.TD_Pressure from (select a.pt_num from (
            select pd.pt_num from piece_main pm inner join piece_detail pd on pm.id=pd.piece_id where (record_time BETWEEN '".$start_time."' and '".$end_time."')
            union
            select pd.pt_num from piece_other_main pm inner join piece_other_detail pd on pm.id=pd.piece_id where (record_time BETWEEN '".$start_time."' and '".$end_time."')
            ) as a group by a.pt_num) as pt inner join producetask ptd on pt.pt_num=ptd.PT_Num inner join taskdetail td on ptd.TD_ID=td.TD_ID ");
        $ptNewList = [];
        foreach($ptNewArr as $v){
            $ptNewList[$v["pt_num"]] = $v;
        }

        // 今日工资 和除板件外杂工工资
        // 1.电焊device=8
        $piecePriceModel = new \app\admin\model\production\piece\PiecePrice();
        $piecePriceArr = $piecePriceModel->order("id")->column("field,price");
        $arr = $this->weldArr;
        $list = $weldingMainModel->where($detailWhere)->select();
        foreach($list as $k=>$v){
            $jj_time = substr($v["record_time"],0,10);
            $pt_num = $v["pt_num"];
            foreach($arr as $ak=>$av){
                if($v[$ak]>0){
                    $detailArr[$jj_time][8][] = [
                        "device" => 8,
                        "workmanship" => 2,
                        "stuff" => "电焊",
                        "specification" => $av[0],
                        "material" => $av[1],
                        "pt_number" => isset($ptNewList[$pt_num])?$ptNewList[$pt_num]["PT_Number"]:0,
                        "pressure" => isset($ptNewList[$pt_num])?trim(strtoupper($ptNewList[$pt_num]["TD_Pressure"]),"KV"):0,
                        "n_value" => $v[$ak],
                        "price" => isset($piecePriceArr[$ak])?$piecePriceArr[$ak]:0,
                        "amount" => isset($piecePriceArr[$ak])?$v[$ak]*$piecePriceArr[$ak]:0,
                        "n_type" => 0,
                        "pt_num" => $pt_num,
                        "jj_time" => $jj_time
                    ];
                }
            }
        }
        // 2.所有 除打钢印和等离子
        $pieceList = $pieceMainModel->alias("pm")
            ->join(["piece_detail"=>"pd"],"pm.id=pd.piece_id")
            ->join(["piece_content"=>"pc"],"pd.piece_content_id=pc.id")
            ->field("left(pm.record_time,10) as record_time,cast(pm.device as unsigned) as device,pd.pt_num,( CASE WHEN (pd.hole_number = 0 ) THEN pd.sum_weight ELSE 0 END ) AS sum_weight,( CASE WHEN (pd.hole_number = 0 ) THEN 0 ELSE 1 END ) AS type,pc.stuff,pc.specification,pc.material,pc.workmanship")
            ->where([
                "pm.record_time" => ["between",[$start_time,$end_time]],
                "pd.pid" => ["=",0],
                "pm.device" => ["<>",8]
            ])
            ->order("record_time,device")
            ->select();
        foreach($pieceList AS $k=>$v){
            $pt_num = $v["pt_num"];
            $content = [
                "device" => $v["device"],
                "workmanship" => $v["workmanship"],
                "stuff" => $v["stuff"],
                "specification" => $v["specification"],
                "material" => $v["material"],
                "pt_number" => isset($ptNewList[$pt_num])?$ptNewList[$pt_num]["PT_Number"]:0,
                "pt_num" => $v["pt_num"],
                "pressure" => isset($ptNewList[$pt_num])?trim(strtoupper($ptNewList[$pt_num]["TD_Pressure"]),"KV"):0,
                "n_type" => $v["type"],
                "n_value" => $v["sum_weight"],
                'price' => 0,
                "amount" => 0,
                "jj_time" => $v["record_time"]
            ];
            [$amount,$price] = $this->sumGongZiTest($content);
            $content["amount"] = $amount;
            $content["price"] = $price;
            $detailArr[$v["record_time"]][$v["device"]][] = $content;
        }
        //3.打钢印和等离子
        $otherList = $pieceOtherMainModel->alias("pom")
            ->join(["piece_other_detail"=>"pod"],"pom.id=pod.piece_id")
            ->field("left(pom.record_time,10) as record_time,cast(pom.device as unsigned) as device,pod.pt_num,pod.stuff,pod.specification,pod.material,pod.workmanship,pod.sum_weight")
            ->where([
                "pom.record_time" => ["between",[$start_time,$end_time]]
            ])->order("record_time,device")
            ->select();
        foreach($otherList AS $k=>$v){
            $pt_num = $v["pt_num"];
            $content = [
                "device" => $v["device"],
                "workmanship" => $v["workmanship"],
                "stuff" => $v["stuff"],
                "specification" => $v["specification"],
                "material" => $v["material"],
                "pt_number" => isset($ptNewList[$pt_num])?$ptNewList[$pt_num]["PT_Number"]:0,
                "pt_num" => $v["pt_num"],
                "pressure" => isset($ptNewList[$pt_num])?trim(strtoupper($ptNewList[$pt_num]["TD_Pressure"]),"KV"):0,
                "n_value" => $v["sum_weight"],
                "n_type" => $v['device']==15?1:0,
                'price' => 0,
                "amount" => 0,
                "jj_time" => $v["record_time"]
            ];
            [$amount,$price] = $this->sumGongZiTest($content);
            $content["amount"] = $amount;
            $content["price"] = $price;
            $detailArr[$v["record_time"]][$v["device"]][] = $content;
        }
        $endArr = [];
        $result = false;
        Db::startTrans();
        try {
            $existIdList = $this->detailModel->where("j_id",$ids)->column("id");
            $this->amountModel->where("d_id","IN",$existIdList)->delete();
            $this->detailModel->where("j_id",$ids)->delete();

            $saveDetail = $this->detailModel->allowField(true)->saveAll($peoList);
            $operatorReturn = []; 
            foreach($saveDetail as $v){
                $operatorReturn[$v["operator"]] = $v["id"];
            }
            foreach($detailArr as $k=>$v){
                foreach($v as $kk=>$vv){
                    foreach($vv as $kkk=>$vvv){
                        foreach($timePeoList[$k][$kk]["gr"] as $ok=>$ov){
                            $endCount = $vvv;
                            if($endCount["n_value"]<=0) continue;
                            $d_id = $operatorReturn[$ok];
                            $value = ($timePeoList[$k][$kk]["hj"]>0)?round($endCount["n_value"]/$timePeoList[$k][$kk]["hj"]*$ov,5):0;
                            $endCount["n_value"] = $value;
                            $endCount["amount"] = ($endCount["device"]==8 or $endCount["n_type"]==1)?round($endCount["price"]*$value,5):round($endCount["price"]/1000*$value,5);
                            $endCount['d_id'] = $d_id;
                            $endArr[] = $endCount;
                        }
                    }
                    $zgPartPrice = 1;
                    $todayPartSummary = array_sum(array_column($vv,'amount'));
                    if($kk==12){
                        $zgPartPrice = ($timePeoList[$k][$kk]["hj"]>0)?round($todayPartSummary/$timePeoList[$k][$kk]["hj"],5):0;
                    }
                    foreach($timePeoList[$k][$kk]["gr"] as $ok=>$ov){
                        $endCount = $zgDeviceList[$k][$kk];
                        if($endCount["n_value"]<=0) continue;
                        $d_id = $operatorReturn[$ok];
                        $endCount["amount"] = $endCount["n_type"]?round($endCount["n_value"]*$zgPartPrice,5):round($endCount["n_value"]/1000*$zgPartPrice,5);;
                        $endCount['d_id'] = $d_id;
                        $endArr[] = $endCount;
                    }
                    
                }
            }
            $saveAmount = $this->amountModel->allowField(true)->saveAll($endArr);
            $sumSaveArr = $this->amountModel->field("d_id as id,sum(n_value) as weight,sum(amount) as amount,sum(amount) as total_amount")->where("d_id","IN",$operatorReturn)->group("d_id")->select();
            $sumSave = $sumSaveArr?collection($sumSaveArr)->toArray():[];
            if(!empty($sumSave)){
                $result = $this->detailModel->allowField(true)->saveAll($sumSave);
                Db::commit();
            }
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $returnList = $this->detailModel->where("j_id",$ids)->order("operator")->select();
            $returnList = $returnList?collection($returnList)->toArray():[];
            $this->success("成功",null,$returnList);
        } else {
            $this->error(__('Parameter %s can not be empty', ''));
        }
    }
    protected $weldArr = [
        'gas_up'               => ["气割",'钢管Ф159以上(吨)'],
        'gas_down'             => ["气割",'钢管Ф159以下(吨)'],
        'gas_plate_range'      => ["气割",'钢板(法兰板) 14mm-30mm (吨)'],
        'gas_plate_up'         => ["气割",'钢板(法兰板) 30mm及以上 (吨)'],
        'gas_steel'            => ["气割",'角钢 厚度16以上气割 (吨)'],
        'team_special'         => ["组对",'特殊角钢组对 (吨)'],
        'team_foot'            => ["组对",'塔脚(吨)'],
        'team_plate_down'      => ["组对",'板件组对 20mm以下(吨)'],
        'team_plate_up'        => ["组对",'板件组对 20mm及以上(吨)'],
        'team_iron'            => ["组对",'铁附件(吨)'],
        'team_foot_up'         => ["组对",'500kv以上塔脚(吨)'],
        'weld_floor_down'      => ["电焊",'110KV及以下塔脚 30mm底板以下 (只)'],
        'weld_floor_up'        => ["电焊",'110KV及以下塔脚 30mm底板及以上(吨)'],
        'weld_floor'           => ["电焊",'220KV及以上塔脚(吨)'],
        'weld_cross_low'       => ["电焊",'横担 长度900(支)'],
        'weld_cross_medium'    => ["电焊",'横担 长度1500(支)'],
        'weld_corss_long'      => ["电焊",'横担 长度1900(支)'],
        'weld_anchor_bolt'     => ["电焊",'地脚螺栓每组焊接(吨)'],
        'weld_bolt_medium'     => ["电焊",'地脚螺栓底板加强筋焊接 M30-M36(支)'],
        'weld_bolt_up'         => ["电焊",'地脚螺栓底板加强筋焊接 M42以上(支)'],
        'weld_plate_down'      => ["电焊",'板件焊接 20mm以下(吨)'],
        'weld_plate_up'        => ["电焊",'板件焊接 20mm及以上(吨)'],
        'weld_special'         => ["电焊",'铁附件、特殊角钢(吨)'],
        'weld_strengthen_down' => ["电焊",'角钢焊(板件)加强角 200以下(吨)'],
        'weld_strengthen_up'   => ["电焊",'角钢焊(板件)加强角 200及以上(吨)']
    ];

    public function summaryTest()
    {
        $ids = $this->request->post("ids");
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row["auditor"]) $this->error("已审核无法修改");
        $start_time = date("Y-m-d 00:00:00",strtotime($row["record_time"]));
        $end_time = date("Y-m-d 23:59:59",strtotime("-1 day",strtotime("+1 month",strtotime($row["record_time"]))));
        //焊接组的
        $detailWhere = [
            "record_time" => ["between",[$start_time,$end_time]]
        ];
        // if($row["type"]==1){

        // 工序：电焊2 机器电焊8
        $weldingMainModel = new \app\admin\model\production\piece\WeldingMain;
        $noAuditorList = $weldingMainModel->where($detailWhere)->where("auditor","=","")->column("id");
        $noAuditorList = $noAuditorList?collection($noAuditorList)->toArray():[];
        if(!empty($noAuditorList)) $this->error("ID为".implode(",",$noAuditorList)."的电焊未审核，请先审核");
        $list = $weldingMainModel->where($detailWhere)->select();
        $peoplePrice = $ptList = $peopleSave = [];
        $arr = [
        'gas_up'               => ["气割",'钢管Ф159以上(吨)'],
        'gas_down'             => ["气割",'钢管Ф159以下(吨)'],
        'gas_plate_range'      => ["气割",'钢板(法兰板) 14mm-30mm (吨)'],
        'gas_plate_up'         => ["气割",'钢板(法兰板) 30mm及以上 (吨)'],
        'gas_steel'            => ["气割",'角钢 厚度16以上气割 (吨)'],
        'team_special'         => ["组对",'特殊角钢组对 (吨)'],
        'team_foot'            => ["组对",'塔脚(吨)'],
        'team_plate_down'      => ["组对",'板件组对 20mm以下(吨)'],
        'team_plate_up'        => ["组对",'板件组对 20mm及以上(吨)'],
        'team_iron'            => ["组对",'铁附件(吨)'],
        'team_foot_up'         => ["组对",'500kv以上塔脚(吨)'],
        'weld_floor_down'      => ["电焊",'110KV及以下塔脚 30mm底板以下 (只)'],
        'weld_floor_up'        => ["电焊",'110KV及以下塔脚 30mm底板及以上(吨)'],
        'weld_floor'           => ["电焊",'220KV及以上塔脚(吨)'],
        'weld_cross_low'       => ["电焊",'横担 长度900(支)'],
        'weld_cross_medium'    => ["电焊",'横担 长度1500(支)'],
        'weld_corss_long'      => ["电焊",'横担 长度1900(支)'],
        'weld_anchor_bolt'     => ["电焊",'地脚螺栓每组焊接(吨)'],
        'weld_bolt_medium'     => ["电焊",'地脚螺栓底板加强筋焊接 M30-M36(支)'],
        'weld_bolt_up'         => ["电焊",'地脚螺栓底板加强筋焊接 M42以上(支)'],
        'weld_plate_down'      => ["电焊",'板件焊接 20mm以下(吨)'],
        'weld_plate_up'        => ["电焊",'板件焊接 20mm及以上(吨)'],
        'weld_special'         => ["电焊",'铁附件、特殊角钢(吨)'],
        'weld_strengthen_down' => ["电焊",'角钢焊(板件)加强角 200以下(吨)'],
        'weld_strengthen_up'   => ["电焊",'角钢焊(板件)加强角 200及以上(吨)']];
        foreach($list as $k=>$v){
            $ptList[$v["pt_num"]] = $v["pt_num"];
            $machineUser = explode(",",$v["machine_user"]);
            foreach($machineUser as $mk=>$mv){
                $peopleSave[$mv] = [
                    "operator" => $mv,
                    "j_id" => $ids
                ];
                foreach($arr as $ak=>$av){
                    $peoplePrice[$mv][$v["record_time"]][$v["pt_num"]][$ak][] = round($v[$ak]/count($machineUser),5);
                }
            }
        }
        $produceTaskModel = new \app\admin\model\chain\lofting\ProduceTask();
        $taskNewsList = $produceTaskModel->alias("pt")->join(["taskdetail"=>"td"],"pt.TD_ID=td.TD_ID")->where("pt.PT_Num","IN",$ptList)->column("pt.PT_Num,td.TD_Pressure,td.TD_TypeName,pt.PT_Number");
        $piecePriceModel = new \app\admin\model\production\piece\PiecePrice();
        $piecePriceArr = $piecePriceModel->where("type",1)->order("id")->select();
        $taskNewsArr = [];
        foreach($taskNewsList as $k=>$v){
            $key = $v["PT_Num"];
            $taskNewsArr[$key] = $v;
            $taskNewsArr[$key]["TD_Pressure"] = trim(strtoupper($v["TD_Pressure"]),"KV");
            $taskNewsArr[$key]["price"] = [];
            foreach($piecePriceArr as $pv){
                if($pv["field"]=="gas_steel" and $pv["choose_type"]==1){
                    if(($v["TD_Pressure"]>="220" and $v["TD_Pressure"]<="500") and $v["PT_Number"]>2) $taskNewsArr[$key]["price"][$pv["field"]] = $pv["price"];
                }else $taskNewsArr[$key]["price"][$pv["field"]] = $pv["price"];
                // $taskNewsArr[$key]["price"][$pv["field"]] = $pv["price"];
            }
        }
        //判断是否还有没审核的
        $pieceMainModel = new \app\admin\model\chain\lofting\PieceMain();
        // $weldingMainModel = new \app\admin\model\production\piece\WeldingMain();
        // $detailWhere = [
        //     "record_time" => ["between",[$row["record_time"],datetime(strtotime("+1 month",strtotime($row["record_time"])),'Y-m-d H:i:s')]]
        // ];
        $noAuditorList = $pieceMainModel->where($detailWhere)->where("auditor","=","")->column("id");
        if(!empty($noAuditorList)) $this->error("ID为".implode(",",$noAuditorList)."的计件未审核，请先审核");
        // $noAuditorWeldingList = $weldingMainModel->where($detailWhere)->where("auditor","=","")->column("id");
        // if(!empty($noAuditorWeldingList)) $this->error("存在电焊计件未审核的情况，请先审核");
        // if($row['type']==1) $detailWhere["workmanship"] = ["=",2];
        // else $detailWhere["workmanship"] = ["<>",2];
        $otherPriceViewModel = new \app\admin\model\jichu\jg\OtherPriceView();
        $otherPriceModel = new \app\admin\model\jichu\jg\OtherPrice();
        $one = $otherPriceModel->where($detailWhere)->where("auditor","=","")->find();
        if($one) $this->error("该月份其他工资未审核的情况，请先审核");

        $operatorList = [];
        $operatorArr = $pieceMainModel->alias("pm")->join(["piece_operator"=>"po"],"pm.id=po.piece_main_id")->field("po.operator,".$ids." as 'j_id'")->where($detailWhere)->group("po.operator")->select();
        foreach($operatorArr as $k=>$v){
            $operatorList[$v["operator"]] = $v->toArray();
        }
        $otherPriceList = $otherPriceViewModel->where($detailWhere)->select();
        foreach($otherPriceList AS $k=>$v){
            isset($operatorList[$v["name"]])?"":$operatorList[$v["name"]] = [
                "operator" => $v["name"],
                "j_id" => $ids
            ];
        }
        if(empty($operatorList)) $this->error("本月工作人员的没有计件信息，请稍后再试");
        $result = false;
        Db::startTrans();
        try {
            $existIdList = $this->detailModel->where("j_id",$ids)->column("id");
            $this->amountModel->where("d_id","IN",$existIdList)->delete();
            $this->detailModel->where("j_id",$ids)->delete();

            $saveDetail = $this->detailModel->allowField(true)->saveAll($operatorList);
            $operatorReturn = []; 
            foreach($saveDetail as $v){
                $operatorReturn[$v["operator"]] = $v["id"];
            }
            //计件工资
            $detailList = $this->viewModel->where($detailWhere)->select();
            $detailArr = [];
            foreach($detailList as $k=>$v){
                $d_id = $operatorReturn[$v["operator"]];
                $sumList[$d_id] = $sumList[$d_id]??["weightList"=>[],"holeList"=>[],"amountList"=>[]];
                foreach(["sum_weight","hole_number"] as $kk=>$vv){
                    if($v[$vv]>0){
                        $content = [
                            "d_id" => $d_id,
                            "device" => $v["device"],
                            "workmanship" => $v["workmanship"],
                            "stuff" => $v["stuff"],
                            "specification" => $v["specification"],
                            "material" => $v["material"],
                            "pt_number" => $v["PT_Number"],
                            "pt_num" => $v["pt_num"],
                            "pressure" => trim(strtoupper($v["TD_Pressure"]),"KV"),
                            "n_type" => $kk,
                            "n_value" => $v[$vv],
                            'price' => 0,
                            "jj_time" => $v["record_time"]
                            // "amount" => 100
                        ];
                        // pri($content,'');
                        [$amount,$price] = $this->sumGongZiTest($content);
                        $content["amount"] = $amount;
                        $content["price"] = $price;
                        $detailArr[] = $content;
                        // if($kk==0) $sumList[$d_id]["weightList"][] = $v[$vv];
                        // else if($kk==1) $sumList[$d_id]["holeList"][] = $v[$vv];
                        // $sumList[$d_id]["amountList"][] = $amount;
                    }
                }
            }
            //焊接工资
            foreach($peoplePrice as $k=>$tv){
                $d_id = $operatorReturn[$k];
                foreach($tv as $tk=>$v){
                    foreach($v as $kk=>$vv){
                        foreach($vv as $kkk=>$vvv){
                            if(array_sum($vvv)>0){
                                $detailArr[] = [
                                    "d_id" => $d_id,
                                    "device" => 8,
                                    "workmanship" => 2,
                                    "stuff" => "电焊",
                                    "specification" => $arr[$kkk][0],
                                    "material" => $arr[$kkk][1],
                                    "pt_number" => $taskNewsArr[$kk]?$taskNewsArr[$kk]["PT_Number"]:0,
                                    "pressure" => $taskNewsArr[$kk]?$taskNewsArr[$kk]["TD_Pressure"]:0,
                                    // "n_type" => $kk,
                                    "n_value" => array_sum($vvv),
                                    "amount" => $taskNewsArr[$kk]?array_sum($vvv)*$taskNewsArr[$kk]["price"][$kkk]:0,
                                    "pt_num" => $kk,
                                    "jj_time" => $tk
                                ];
                            }
                        }
                    }
                }
            }
            //杂工工资
            $zgList = (new PieceZgAmountView())->where($detailWhere)->where("amount","<>",0)->select();
            foreach($zgList as $k=>$v){
                $d_id = $operatorReturn[$v["operator"]];
                $detailArr[] = [
                    "d_id" => $d_id,
                    "device" => 0,
                    "workmanship" => 0,
                    "stuff" => "杂工",
                    "specification" => "杂工",
                    "material" => "杂工",
                    "pt_number" => 0,
                    "pressure" => 0,
                    // "n_type" => $kk,
                    "n_value" => 0,
                    "amount" => $v["amount"],
                    "pt_num" => '',
                    "jj_time" => $v["record_time"]
                ];
            }

            //其他工资
            foreach($otherPriceList AS $k=>$v){
                $d_id = $operatorReturn[$v["name"]];
                $detailArr[] = [
                    "d_id" => $d_id,
                    "device" => 0,
                    "workmanship" => 0,
                    "stuff" => "其他",
                    "specification" => "其他",
                    "material" => "其他",
                    "pt_number" => 0,
                    "pressure" => 0,
                    // "n_type" => $kk,
                    "n_value" => 0,
                    "amount" => $v["ave_amount"],
                    "pt_num" => '',
                    "jj_time" => $v["record_time"]
                ];
            }

            $saveAmount = $this->amountModel->allowField(true)->saveAll($detailArr);
            $sumSaveArr = $this->amountModel->field("d_id as id,sum(n_value) as weight,sum(amount) as amount,sum(amount) as total_amount")->where("d_id","IN",$operatorReturn)->group("d_id")->select();
            $sumSave = $sumSaveArr?collection($sumSaveArr)->toArray():[];
            if(!empty($sumSave)){
                $result = $this->detailModel->allowField(true)->saveAll($sumSave);
                Db::commit();
            }
            $result = $this->detailModel->allowField(true)->saveAll($sumSave);
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($result !== false) {
            $returnList = $this->detailModel->where("j_id",$ids)->order("operator")->select();
            $returnList = $returnList?collection($returnList)->toArray():[];
            $this->success("成功",null,$returnList);
        } else {
            $this->error(__('Parameter %s can not be empty', ''));
        }
        
    }

    public function sumGongZiTest($content = [])
    {
        $price = 0;
        if(empty($content)) return $price;
        $zk = 1;
        $amount = 0;
        //0 冲孔 1钻孔 暂时不用
        $holetype = 0;
        $torch = 0;
        $specification = 0;
        if($content["stuff"]=="角钢"){
            preg_match_all("/\d+\.?\d*/",$content["specification"],$matches);
            $x = $matches[0][0]?((float)$matches[0][0]):0;
            $torch = $y = $matches[0][1]?((float)$matches[0][1]):0;
            $specification = $x*1000+$y;
        }else $torch = $specification = abs($content["specification"]);
        if(substr($content["material"],0,4)=="Q235") $holetype = $torch<=14?0:1;
        elseif(substr($content["material"],0,4)=="Q355") $holetype = $torch<=12?0:1;
        elseif(substr($content["material"],0,4)=="Q420") $holetype = $torch<=10?0:1;
        
        //落料 冲孔 钻孔 剪板 板件数控 切角火曲 刨床 数控和数控钻孔 等离子割板
        if($content["device"]==1){
            if($specification>=40003 and $specification<=45005){
                if($content["pt_number"]<=2) $amount = 57;
                else $amount = 46;
            }else if($specification>=50004 and $specification<=50005){
                if($content["pt_number"]<=2) $amount = 50;
                else $amount = 40;
            }elseif($specification>=56004){
                if($content["pt_number"]<=2) $amount = 35;
                else $amount = 32;
            }
            if($content["pressure"]<220 and $content["pt_number"]>=5) $zk=0.95;
            if(($content["pressure"]>=220 and $content["pt_number"]>=4) or ($content["pressure"]>=500 and $content["pt_number"]>=3)) $zk=0.92;
            $price = $content["n_value"]/1000*$amount*$zk;
        }else if($content["device"]==4){
            if($specification>=40003 and $specification<=45005){
                if($content["pt_number"]<=2) $amount = 115;
                else $amount = 86;
            }else if($specification>=50004 and $specification<=50005){
                if($content["pt_number"]<=2) $amount = 100;
                else $amount = 75;
            }elseif($specification>=56004){
                if($content["pt_number"]<=2) $amount = 77;
                else $amount = 60;
            }
            if($content["pressure"]<220 and $content["pt_number"]>=5) $zk=0.95;
            if(($content["pressure"]>=220 and $content["pt_number"]>=4) or ($content["pressure"]>=500 and $content["pt_number"]>=3)) $zk=0.92;
            $price = $content["n_value"]/1000*$amount*$zk;
        }else if(in_array($content["device"],[6,7])){
            if($torch<=16){
                $amount = 0.19;
                if($content["pt_number"]>=3){
                    if($content["pressure"]>=800) $amount = 0.2;
                    elseif($content["pressure"]>=1000) $amount = 0.21;
                }
            }else if($torch==18){
                $amount = 0.21;
                if($content["pt_number"]>=3){
                    if($content["pressure"]>=800) $amount = 0.22;
                    elseif($content["pressure"]>=1000) $amount = 0.23;
                }
            }else if($torch==20){
                $amount = 0.22;
                if($content["pt_number"]>=3){
                    if($content["pressure"]>=800) $amount = 0.23;
                    elseif($content["pressure"]>=1000) $amount = 0.24;
                }
            }else if($torch==22){
                $amount = 0.23;
                if($content["pt_number"]>=3){
                    if($content["pressure"]>=800) $amount = 0.24;
                    elseif($content["pressure"]>=1000) $amount = 0.25;
                }
            }else if($torch==24){
                $amount = 0.24;
                if($content["pt_number"]>=3){
                    if($content["pressure"]>=800) $amount = 0.25;
                    elseif($content["pressure"]>=1000) $amount = 0.26;
                }
            }else if($torch>=26 and $torch<=28){
                $amount = 0.25;
                if($content["pt_number"]>=3){
                    if($content["pressure"]>=800) $amount = 0.26;
                    elseif($content["pressure"]>=1000) $amount = 0.27;
                }
            }else if($torch==30){
                $amount = 0.26;
                if($content["pt_number"]>=3){
                    if($content["pressure"]>=800) $amount = 0.27;
                    elseif($content["pressure"]>=1000) $amount = 0.29;
                }
            }else if($torch>=32){
                $amount = 0.30;
                if($content["pt_number"]>=3){
                    if($content["pressure"]>=800) $amount = 0.32;
                    elseif($content["pressure"]>=1000) $amount = 0.33;
                }
            }
            $price = $content["n_value"]*$amount;
        }else if($content["device"]==3){
            if($torch>=2 and $torch<=5){
                if($content["pt_number"]<=2) $amount = 144;
                else $amount = 136;
            }else if($torch==6){
                if($content["pt_number"]<=2) $amount = 130;
                else $amount = 121;
            }else if($torch==8){
                if($content["pt_number"]<=2) $amount = 100;
                else $amount = 90;
            }else if($torch>=10 and $torch<=12){
                if($content["pt_number"]<=2) $amount = 83;
                else $amount = 66;
            }else if($torch>=14 and $torch<=16){
                if($content["pt_number"]<=2) $amount = 65;
                else $amount = 58;
            }
            if($content["pressure"]<220 and $content["pt_number"]>=5) $zk=0.95;
            if(($content["pressure"]>=220 and $content["pt_number"]>=4) or ($content["pressure"]>=500 and $content["pt_number"]>=3)) $zk=0.92;
            $price = $content["n_value"]/1000*$amount*$zk;
        }else if($content["device"]==12){
            if($content["workmanship"]==16){
                if($torch<=8) $amount = 38;
                else $amount = 17;
            }else{
                if($torch>=5 and $torch<=6){
                    if($content["pt_number"]<=2) $amount=170;//+38;
                    else $amount=158;//+38;
                }else if($torch==8){
                    if($content["pt_number"]<=2) $amount=89;//+38;
                    else $amount=81;//+38;
                }else if($torch>=10){
                    if($content["pt_number"]<=2) $amount=64;//+17;
                    else $amount=56;//+17;
                }
            }
            $price = $content["n_value"]/1000*$amount;
        }else if($content["device"]==5){
            if($content["workmanship"] == 4){
                if($specification>=40003 and $specification<=45004){
                    if($content["pt_number"]<=2) $amount=40;//+38;
                    else $amount=32;//+38;
                }else if($specification>=50004 and $specification<81000){
                    if($content["pt_number"]<=2) $amount=20;//+38;
                    else $amount=18;//+38;
                }else if($specification>=90006 and $specification<=140010){
                    if($content["pt_number"]<=2) $amount=17;//+38;
                    else $amount=15;//+38;
                }else if($specification>140010){
                    if($content["pt_number"]<=2) $amount=13;//+38;
                    else $amount=11;//+38;
                }
            }else if($content["workmanship"] == 11){
                if($content["stuff"]=="角钢"){
                    if($content["pt_number"]<=2) $amount=41;
                    else $amount=36;
                }else{
                    if($torch<12){
                        if($content["pt_number"]<=2) $amount = 44;
                        else $amount = 36;
                    }else if($torch<=14){
                        if($content["pt_number"]<=2) $amount = 44;
                        else if($content["pressure"]<500) $amount = 36;
                        else $amount = 30;
                    }else if($torch<=18){
                        if($content["pt_number"]<=2) $amount = 44;
                        else if($content["pressure"]<500) $amount = 36;
                        else $amount = 26;
                    }else{
                        if($content["pt_number"]<=2) $amount = 44;
                        else if($content["pressure"]<500) $amount = 36;
                        else $amount = 24;
                    }
                    if($content["pressure"]<220 and $content["pt_number"]>=5) $zk=0.95;
                    if(($content["pressure"]>=220 and $content["pt_number"]>=4) or ($content["pressure"]>=500 and $content["pt_number"]>=3)) $zk=0.92;
                }
            }
            $price = $content["n_value"]/1000*$amount*$zk;
        }else if($content["device"]==11){
            if($specification<110000){
                $amount = 183;
                if($content["pressure"]>=500 or $content["pt_number"]>=3) $amount = 134;
            }else if($specification>=110000 and $specification<141000){
                $amount = 139;
                if($content["pressure"]>=500 or $content["pt_number"]>=3) $amount = 98;
            }else if($specification>=160000 and $specification<181000){
                $amount = 113;
                if($content["pressure"]>=500 or $content["pt_number"]>=3) $amount = 79;
            }else if($specification>=200000 and $specification<221000){
                $amount = 101;
                if($content["pressure"]>=500 or $content["pt_number"]>=3) $amount = 71;
            }else if($specification>=250000 and $specification<251000){
                $amount = 90;
                if($content["pressure"]>=500 or $content["pt_number"]>=3) $amount = 64;
            }else if($specification>=251000) $amount=53;
            $price = $content["n_value"]/1000*$amount;
        }else if(in_array($content["device"],[2,9,10,13,14])){
            if($content["workmanship"]==16){
                if($torch<=8) $amount = 38;
                else $amount = 17;
                $price = $content["n_value"]/1000*$amount;
            }else{
                if($content["n_type"]){
                    if($torch<=16){
                        $amount = 0.19;
                        if($content["pt_number"]>=3){
                            if($content["pressure"]>=800) $amount = 0.2;
                            elseif($content["pressure"]>=1000) $amount = 0.21;
                        }
                    }else if($torch==18){
                        $amount = 0.21;
                        if($content["pt_number"]>=3){
                            if($content["pressure"]>=800) $amount = 0.22;
                            elseif($content["pressure"]>=1000) $amount = 0.23;
                        }
                    }else if($torch==20){
                        $amount = 0.22;
                        if($content["pt_number"]>=3){
                            if($content["pressure"]>=800) $amount = 0.23;
                            elseif($content["pressure"]>=1000) $amount = 0.24;
                        }
                    }else if($torch==22){
                        $amount = 0.23;
                        if($content["pt_number"]>=3){
                            if($content["pressure"]>=800) $amount = 0.24;
                            elseif($content["pressure"]>=1000) $amount = 0.25;
                        }
                    }else if($torch==24){
                        $amount = 0.24;
                        if($content["pt_number"]>=3){
                            if($content["pressure"]>=800) $amount = 0.25;
                            elseif($content["pressure"]>=1000) $amount = 0.26;
                        }
                    }else if($torch>=26 and $torch<=28){
                        $amount = 0.25;
                        if($content["pt_number"]>=3){
                            if($content["pressure"]>=800) $amount = 0.26;
                            elseif($content["pressure"]>=1000) $amount = 0.27;
                        }
                    }else if($torch==30){
                        $amount = 0.26;
                        if($content["pt_number"]>=3){
                            if($content["pressure"]>=800) $amount = 0.27;
                            elseif($content["pressure"]>=1000) $amount = 0.29;
                        }
                    }else if($torch>=32){
                        $amount = 0.30;
                        if($content["pt_number"]>=3){
                            if($content["pressure"]>=800) $amount = 0.32;
                            elseif($content["pressure"]>=1000) $amount = 0.33;
                        }
                    }
                    $price = $content["n_value"]*$amount;
                }else{
                    if($specification<56000){
                        if($content["pt_number"]<=2) $amount=100;
                        else $amount=75;
                    }else if($specification<64000){
                        if($content["pt_number"]<=2) $amount=67;
                        else $amount=61;
                    }else if($specification>=70000 and $specification<76000){
                        if($content["pt_number"]<=2) $amount=57;
                        else $amount=52;
                    }else if($specification>=80000 and $specification<91000){
                        if($content["pt_number"]<=2) $amount=41;
                        else if($content["pressure"]==220) $amount=34;
                        // else if($content["pressure"]<500) $amount=34;
                        else $amount=32;
                    }else if($specification>=100000 and $specification<126000){
                        if($content["pt_number"]<=2) $amount=38;
                        else if($content["pressure"]==220) $amount=32;
                        // else if($content["pressure"]<500) $amount=32;
                        else $amount=30;
                    }else if($specification>=140000 and $specification<141000){
                        if($content["pt_number"]<=2) $amount=36;
                        else if($content["pressure"]==220) $amount=30.5;
                        // else if($content["pressure"]<500) $amount=30.5;
                        else $amount=29;
                    }else if($specification>=160000 and $specification<181000){
                        if($content["pt_number"]<=2) $amount=34;
                        else if($content["pressure"]==220) $amount=26.5;
                        // else if($content["pressure"]<500) $amount=26.5;
                        else $amount=23;
                    }
                    if($content["pressure"]<220 and $content["pt_number"]>=5) $zk=0.95;
                    if($content["pressure"]>=220 and $content["pt_number"]>=4) $zk=0.92;
                    if(($content["pressure"]>=500 and $content["pt_number"]>=3) and $specification>=90000) $zk=0.9;
                    $price = $content["n_value"]/1000*$amount*$zk;
                }
            }
        }else if($content["device"]==15){
            if($torch<=8) $amount = 0.4;
            else if($torch<=12) $amount = 0.54;
            else $amount = 0.63;
            $price = $content["n_value"]*$amount*$zk;
        }
        $dj = $amount*$zk;
        return [$price,$dj];
    }

    public function saveDetail()
    {
        $data = $this->request->post("data");
        $data = $data?json_decode($data,true):[];
        if(empty($data)) $this->error("无法保存，请稍后再试");
        $saveList = [];
        foreach($data as $v){
            $saveList[] = [
                "id" => $v["id"],
                "total_amount" => $v["total_amount"],
                "remark" => trim($v["remark"],"-")
            ];
        }
        $result = $this->detailModel->allowField(true)->saveAll($saveList);
        if($result) $this->success();
        else $this->error(__('No rows were updated'));
    }

    public function detail($d_id = null)
    {
        $row = $this->detailModel->get($d_id);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->amountModel
             ->field("*,case stuff when '角钢' then 0 when '钢板' then 1 else 2 end clsort,cast((case 
            when stuff='角钢' then 
            SUBSTR(REPLACE(specification,'∠',''),1,locate('*',REPLACE(specification,'∠',''))-1)*1000+
            SUBSTR(REPLACE(specification,'∠',''),locate('*',REPLACE(specification,'∠',''))+1,2)*1
            when (stuff='钢板' or stuff='钢管') then 
            REPLACE(specification,'-','')
            else 0
            end) as UNSIGNED) bh")
            ->where("d_id",$d_id)
            ->where($where)
            ->order("jj_time,device,workmanship,pressure,pt_number,clsort,stuff,bh,specification,material,n_type")
            ->select();
            $list = $list?collection($list)->toArray():[];
            foreach($list as $k=>&$v){
                $v["index"] = $k;
            }
            $sum_amount = array_sum(array_column($list,'amount'));
            $list[] = [
                "stuff" => "汇总",
                "specification" => "汇总",
                "amount" => round($sum_amount,5)
            ];
            $result = array("total" => count($list), "rows" => $list, "extend"=> [
                "amount" => $row["amount"],
                "total_amount" => $row["total_amount"]
            ]);

            return json($result);
        }
        
        // $list = $list?collection($list)->toArray():[];
        $machineList = $this->machineList(["type"=>["=",2]])[0];
        $machineList[0] = "杂工";
        $orderList = [0=>"杂工",1=>"普通",2=>"电焊",3=>"弯曲",4=>"切角",5=>"铲背",6=>"清根",7=>"打扁",8=>"开合角",9=>"钻孔",10=>'割豁',11=>"制弯打扁开合角"];
        $ntypeList = ["0"=>"重量","1"=>"孔数"];
        $this->view->assign("row",$row);
        // $this->assignconfig("list",$list);
        $this->assignconfig("machineList",$machineList);
        $this->assignconfig("orderList",$orderList);
        $this->assignconfig("ntypeList",$ntypeList);
        $this->assignconfig("d_id",$d_id);
        return $this->view->fetch();
    }

    public function detailSummary($ids = null)
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->amountModel
                ->field("left(jj_time,10) as jj_time,round(sum(amount),5) as amount")
                ->where($where)
                ->where("d_id",$ids)
                ->order($sort, $order)
                ->group("left(jj_time,10)")
                ->select();

            $result = array("total" => count($list), "rows" => $list?collection($list)->toArray():[]);

            return json($result);
        }
        $this->assignconfig("ids",$ids);
        return $this->view->fetch();
    }

    public function savePeopleDetail()
    {
        [$id,$field,$value] = array_values($this->request->post());
        if($field!='price') $this->error("有误，请稍后再试");
        $row = $this->amountModel->alias("a")
            ->join(["piece_summary_detail"=>"psd"],"a.d_id=psd.id")
            ->join(["piece_summary"=>"psm"],"psd.j_id=psm.id")
            ->field("psm.auditor,a.*")
            ->where("a.id",$id)
            ->find();
        if(!$row) $this->error("未找到该信息");
        else if($row["auditor"]) $this->error("已审核，无法修改");
        $saveData = [
            "id" => $id,
            "price" => $value,
            "amount" => ($row["device"]==8 or $row["n_type"]==1)?round($row["n_value"]*$value,5):round($row["n_value"]*$value/1000,5)
        ];
        $result = false;
        Db::startTrans();
        try {
            $result = $this->amountModel->where("id",$id)->update($saveData);
            if($result){
                $list = $this->amountModel->field("d_id as id,sum(n_value) as weight,sum(amount) as amount,sum(amount) as total_amount")->where([
                    "d_id" => $row["d_id"]
                ])->select();
                $list = $list?collection($list)->toArray():[];
                $next_result = $this->detailModel->allowField(true)->saveAll($list);
                Db::commit();
            }
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        $this->success("保存成功");
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $adminIds = $this->getDataLimitAdminIds();
            if (is_array($adminIds))
             {
                $this->model->where($this->dataLimitField, 'in', $adminIds);
            }
            $list = $this->model->where("id", 'in', $ids)->where("auditor","=","")->column("id");
            $count = 0;
            Db::startTrans();
            try {
                $dIdList = $this->detailModel->where("j_id","in",$list)->column("id");
                $count += $this->amountModel->where("d_id","IN",$dIdList)->delete();
                $count += $this->detailModel->where("j_id","IN",$list)->delete();
                $count += $this->model->where("id","IN",$list)->delete();
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($count) {
                $this->success();
            } else {
                $this->error(__('No rows were deleted'));
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }

    public function auditor()
    {
        $ids = $this->request->post("ids");
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if($row["auditor"]) $this->error("已审核，无须重复审核");
        $result = false;
        Db::startTrans();
        try {
            $result = $this->model->where("id",$ids)->update([
                "auditor" => $this->admin["nickname"],
                "auditor_time" => date("Y-m-d H:i:s")
            ]);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if($result) $this->success();
        else $this->error("请稍后重试");
    }

    public function create()
    {
        $ids = $this->request->post("ids");
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $employeeWagesMainModel = new EmployeeWagesMain();
        $employeeWagesDetailModel = new EmployeeWagesDetail();
        $main = $employeeWagesMainModel->where([
            "cm_num" => $row["j_num"],
            "type" => '仓库'
        ])->find();
        if($main) $this->error("已生成该月的仓库工资明细");
        $result = false;
        Db::startTrans();
        try {
            $main = $detail = [];
            $wagesList = $this->detailModel->where("j_id","=",$ids)->group("operator")->column("operator,sum(total_amount) as total_amount");
            if(empty($wagesList)) throw new Exception("缺少员工明细");
            $main = [
                "cm_num" => $row["j_num"],
                "type" => '仓库',
                'sj_day' => 0,
                "writer" => $this->admin["username"]
            ];
            $mainResult = $employeeWagesMainModel->insertGetId($main);
            foreach($wagesList as $k=>$v){
                $detail[] = [
                    "m_id" => $mainResult,
                    "name" => $k,
                    "cm_day" => 0,
                    "jb_wages" => 0,
                    "wages" => $v,
                    "base_wages" => $v
                ];
            }
            $result = $employeeWagesDetailModel->allowField(true)->saveAll($detail);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if($result) $this->success();
        else $this->error("请稍后重试");
    }

    public function giveUp()
    {
        $ids = $this->request->post("ids");
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if(!$row["auditor"]) $this->error("未审核，无须弃审");
        $employeeWagesMainModel = new EmployeeWagesMain();
        $main = $employeeWagesMainModel->where([
            "cm_num" => $row["j_num"],
            "type" => '仓库'
        ])->find();
        if($main) $this->error("已生成该月的仓库工资明细");
        $result = $this->model->where("id",$ids)->update([
            "auditor" => '',
            "auditor_time" => "0000-00-00 00:00:00"
        ]);
        if($result) $this->success();
        else $this->error("请稍后重试");
    }

}
