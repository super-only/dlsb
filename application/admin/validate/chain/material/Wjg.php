<?php

namespace app\admin\validate\chain\material;

use think\Validate;

class Wjg extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        "PT_Num|生产下达单号" => ['require'],
        // "IM_Num|材料编码" => ['require'],
        "material|材料名称" => ['require'],
        "IM_Spec|规格" => ['require'],
        "L_Name|材质" => ['require'],
        "length|长度" => ['require'],
        "width|宽度" => ['require',"number"],
        "quantity|数量" => ['require',"number"],
        "weight|重量" => ['require',"number"],
        "cc_date|原材料出厂日期" => ['require',"date"],
        "fh_date|发货日期" => ['require',"date"],
        "dh_date|到货时间" => ['require',"date"],
        "lh_date|申请理化时间" => ['require',"date"],
        "rk_date|入库时间" => ['require',"date"],
        "ck_date|出库日期" => ['require',"date"],
        "v_num|供应商" => ['require'],
        // "result|结论" => ['require'],
        "CTD_QualityNo|质保书号" => ["require"],
        "MT_Standard|检验标准" => ['require'],
        "LuPiHao|炉/批号" => ['require'],
        "MTD_C|C" => ['require',"number"],
        "MTD_Si|Si" => ['require',"number"],
        "MTD_Mn|Mn" => ['require',"number"],
        "MTD_P|P" => ['require',"number"],
        "MTD_S|S" => ['require',"number"],
        "MTD_V|V" => ['require',"number"],
        "MTD_Nb|Nb" => ['require',"number"],
        "MTD_Ti|Ti" => ['require',"number"],
        "MTD_Cr|Cr" => ['require',"number"],
        // "MTD_Czsl|层状撕裂检验" => ['require'],
        "MTD_Rm|抗拉强度" => ['require',"number"],
        "MTD_Rel|屈服强度" => ['require',"number"],
        "MTD_Percent|断后伸长率" => ['require',"number"],
        "MTD_Temperature|温度" => ['require',"number"],
        "MTD_Cjf|冲击实测值1" => ['require',"number"],
        "MTD_Cjs|冲击实测值2" => ['require',"number"],
        "MTD_Cjt|冲击实测值3" => ['require',"number"],
        // "MTD_Wq|弯曲试验" => ['require'],
        // "MTD_Wg|尺寸外观" => ['require'],
        "MTD_Mader|生产厂家" => ['require'],
        "mn_deliverdate|原材料出厂日期" => ['require',"date"],
        "MT_AuditDate|检测时间" => ['require',"date"],
        "MT_Auditor|检测人员" => ['require']
    ];
    /**
     * 提示消息
     */
    protected $message = [
    ];
    /**
     * 验证场景
     */
    protected $scene = [
        'add'  => [],
        'edit' => [],
    ];
    
}
