<?php

return [
    'Mn_num'         => '到货单号',
    'Mn_id'          => 'ID',
    'Mn_company'     => '外协外购单位',
    'Mn_charger'     => '部门主管',
    'Mn_operater'    => '购置经手人',
    'Valid'          => '有效值 默认0',
    'V_num'          => '供应商id',
    'Writer'         => '制表人',
    'Writetime'      => '制表时间',
    'Auditor'        => '审核人',
    'Audittime'      => '审核时间',
    'Approve'        => '批准人',
    'Approvetime'    => '批准时间',
    'St_num'         => '运输方式',
    'Mn_date'        => '到货日期',
    'Modifyer'       => '修改人',
    'Modifytime'     => '修改时间',
    'Mn_delivernum'  => '送货单号',
    'Mn_license'     => '车牌号',
    'Mn_deliverdate' => '发货日期',
    'Ht_id'          => '合同ID'
];
