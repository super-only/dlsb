<?php

return [
    'Sid_id'         => 'ID',
    'Si_otherid'     => '入库编码',
    'Mgn_id'         => '材料仓库管理id',
    'Ct_num'         => '理化申请id',
    'Im_num'         => '材料id',
    'L_name'         => '材质',
    'Sid_length'     => '长度(m)',
    'Sid_width'      => '宽度(m)',
    'Sid_count'      => '入库数量',
    'Sid_weight'     => '过磅重量(kg)',
    'SID_FactWeight'      => '入库重量(kg)',
    'SID_RestCount'     => '剩余数量(kg)',
    'SID_RestWeight'     => '剩余重量(kg)',
    'Sid_testresult' => '生产厂家',
    'Lupihao'        => '炉号',
    'Pihao'          => '批号',
    'Sid_planprice'  => '单价(元/吨)',
    'V_num'          => '供应商id',
    'Sid_money'      => '金额',
    'Sid_frate'      => '税率',
    'Sid_notaxprice' => '不含税单价',
    'Sid_notaxmoney' => '不含税金额'
];
