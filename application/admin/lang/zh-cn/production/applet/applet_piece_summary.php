<?php

return [
    'Day_id'       => 'ID',
    'Day'          => '日期',
    'machine_id'   => '设备',
    'Zg_amount'    => '杂工工资',
    'Writer'       => '制单人',
    'Writer_time'  => '制单时间',
    'Auditor'      => '审核人',
    'Auditor_time' => '审核时间'
];
