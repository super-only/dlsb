<?php

return [
    'Pay_app_num'  => '申请付款单号',
    'Date'         => '日期',
    'Type'         => '类型',
    'Supplier'     => '供应商',
    'Department'   => '部门',
    'Salesman'     => '业务员',
    'Rate'         => '汇率(%)',
    'Method'       => '结算方式',
    'Currency'     => '币种',
    'Bank'         => '供应商银行',
    'Account'      => '供应商账号',
    'Url'          => '附件信息',
    'Remark'       => '备注',
    'Writer'       => '制单人',
    'Writer_time'  => '制单时间',
    'Auditor'      => '审核人',
    'Auditor_time' => '制单时间',
    'Update_time'  => '修改时间'
];
