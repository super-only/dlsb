<?php

return [
    'Pt_num'       => '生产下达单号',
    'Writer'       => '制单人',
    'Writer_time'  => '制单时间',
    'Auditor'      => '审核人',
    'Auditor_time' => '审核时间'
];
