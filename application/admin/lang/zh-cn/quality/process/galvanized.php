<?php

return [
    'G_id'        => '镀锌批次ID',
    'Ppm_id'      => '生产试验过程id',
    'Pt_num'      => '生产下达单号',
    'Writer'      => '制单人',
    'Writer_time' => '制单时间'
];
