<?php

return [
    'Invoice_num'      => '发票编号',
    'Invoice_no'       => '发票号',
    'Business_type'    => '业务类型',
    'Type'             => '发票类型',
    'Kp_date'          => '开票日期',
    'Supplier'         => '供应商',
    'Procurement_type' => '采购类型',
    'Fax'              => '税率(%)',
    'Department'       => '部门',
    'Salesman'         => '业务员',
    'Currency'         => '币种',
    'Rate'             => '汇率(%)',
    'Fp_date'          => '发票日期',
    'Url'              => '附件信息',
    'Remark'           => '备注',
    'Writer'           => '制单人',
    'Writer_time'      => '制单时间',
    'Auditor'          => '审核人',
    'Auditor_time'     => '制单时间',
    'Update_time'      => '修改时间'
];
