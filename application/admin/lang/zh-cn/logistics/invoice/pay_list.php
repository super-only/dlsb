<?php

return [
    'Pay_num'      => '支付单号',
    'Pay_app_num'  => '支付申请单号',
    'Date'         => '日期',
    'Amount'       => '支付金额',
    'Currency'     => '人民币',
    'Rate'         => '汇率(%)',
    'Account'      => '供应商账号',
    'Bill_number'  => '票据号',
    'Url'          => '附件信息',
    'Remark'       => '备注',
    'Writer'       => '制单人',
    'Writer_time'  => '制单时间',
    'Auditor'      => '审核人',
    'Auditor_time' => '制单时间',
    'Update_time'  => '修改时间'
];
