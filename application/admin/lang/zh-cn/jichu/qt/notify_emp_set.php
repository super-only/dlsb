<?php

return [
    'N_id'        => 'ID',
    'N_type'      => '联系单方案',
    'E_num'       => '员工编号',
    'N_writer'    => '设置人(制单人)',
    'N_writedate' => '设置时间(制单时间)'
];
