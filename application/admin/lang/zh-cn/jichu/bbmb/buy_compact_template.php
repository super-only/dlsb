<?php

return [
    'Bct_num'     => '模板编码',
    'Writer'      => '制单人',
    'Writetime'   => '制单时间',
    'Modifyer'    => '修改人',
    'Modifytime'  => '修改时间',
    'Auditor'     => '审核人',
    'Audittime'   => '审核时间',
    'Approve'     => '批准人',
    'Approvetime' => '批准时间',
    'V_num'       => '供应商 vendor表V_Num',
    'Bct_type'    => '模板类别'
];
