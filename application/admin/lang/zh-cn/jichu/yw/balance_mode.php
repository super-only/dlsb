<?php

return [
    'Bm_num'     => '结算方式编码',
    'Bm_name'    => '结算方式名称',
    'Bm_valid'   => '有效',
    'Bm_default' => '是否默认',
    'Bm_days'    => '天数'
];
