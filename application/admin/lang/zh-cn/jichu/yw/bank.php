<?php

return [
    'Bn_id'   => 'ID',
    'Bn_bank' => '开户行',
    'Bn_name' => '账户名',
    'Bn_num'  => '账号',
    'Bn_memo' => '备注',
    'Valid'   => '是否有效 0无效1有效'
];
