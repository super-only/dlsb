<?php

return [
    'Pt_num'        => '采购类型编码',
    'Pt_name'       => '采购类型名称',
    'Pt_ifdefault'  => '是否默认',
    'Valid'         => '有效',
    'Pt_enterclass' => '前缀编码'
];
