<?php

return [
    'Id'      => '设备编码',
    'Name'    => '设备名称',
    'Default' => '是否默认',
    'Valid'   => '是否有效'
];
