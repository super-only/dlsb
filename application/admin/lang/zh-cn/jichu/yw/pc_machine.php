<?php

return [
    'Id'          => '设备编码',
    'Name'        => '设备名称',
    'Valid'       => '是否有效',
    'Writer'      => '登记者',
    'Writer_time' => '登记时间'
];
