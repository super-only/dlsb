<?php

return [
    'St_num'      => '发运类型编码',
    'St_name'     => '发运类型名称',
    'Valid'       => '有效',
    'St_selected' => '是否默认'
];
