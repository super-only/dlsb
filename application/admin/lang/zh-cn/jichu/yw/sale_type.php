<?php

return [
    'St_num'       => '销售类型编码',
    'St_name'      => '销售类型名称',
    'St_ifdefault' => '是否默认',
    'Valid'        => '有效',
    'is_default'     => '是',
    'no_default'     => '否',
    
];
