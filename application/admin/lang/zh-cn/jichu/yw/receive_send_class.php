<?php

return [
    'Rsid'       => 'ID',
    'Rsc_num'    => '收发类型编码',
    'Rsc_rstype' => '出入库类别',
    'Rsc_name'   => '类别名称',
    'Valid'      => '是否有效',
    'Rsc_flag'   => '产品类别 '
];
