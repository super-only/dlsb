<?php

return [
    'Dd_num'          => '部门编码',
    'Dd_name'         => '部门名称',
    'Dd_resperson'    => '部门负责人',
    'Dd_tel'          => '部门电话',
    'Dd_address'      => '地址',
    'Dd_email'        => 'Email',
    'Dd_memo'         => '传真',
    'Parentnum'       => '所属部门',
    'Valid'           => '是否有效',
    'Dd_sort'         => '排序标志',
    'Dd_resprisontel' => '部门负责人电话',
    'Dd_telinner'     => '部门内线电话'
];
