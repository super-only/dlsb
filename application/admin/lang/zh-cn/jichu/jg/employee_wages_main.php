<?php

return [
    'Cm_num'       => '工资编号',
    'Type'         => '类型',
    'Update_time'  => '修改时间',
    'Writer'       => '创建者',
    'Writer_time'  => '创建时间',
    'Auditor'      => '审核人',
    'Auditor_time' => '审核时间'
];
