<?php

return [
    'Factoryname'        => '*企业名称(中文)',
    'Id'                 => 'ID',
    'Factoryenglishname' => '*企业名称(英文)',
    'Shortname'          => '简称(中文)',
    'Shortenglishname'   => '简称(英文)',
    'Fartificalperson'   => '法人代表',
    'Fsignnum'           => '企业注册号',
    'Ftelephone'         => '电话号码',
    'Ffax'               => '传真',
    'Fpostalcode'        => '邮政编码',
    'Fe_mail'            => 'E_mail',
    'Faddress'           => '地址(中文)',
    'Flinkman'           => '联系人',
    'FMemo'              => '备注',
    'FLogo'              => '企业Logo(厂标)',
    'Fenaddress'         => '地址(英文)',
    'Symbol'             => 'symbol',
    'FdeptStructure'     => '企业部门结构图(上传)',
    'Faddressenglish'    => 'FAddressEnglish'
];
