<?php

return [
    'Id'           => 'ID',
    'Record_time'  => '登记时间',
    'Writer_time'  => '制单时间',
    'Writer'       => '制单人',
    'Auditor_time' => '审核时间',
    'Auditor'      => '审核人',
    'Peoples'      => '人员',
    'Amount'       => '金额',
    'Remark'       => '备注'
];
