<?php

return [
    'Cm_num'       => '考勤编号',
    'Name'         => '员工名称',
    'Date'         => '日期',
    'Check'        => '考勤情况',
    'Writer'       => '制单人',
    'Writer_time'  => '制单时间',
    'Auditor'      => '审核人',
    'Auditor_time' => '审核时间'
];
