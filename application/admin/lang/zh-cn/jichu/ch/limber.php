<?php

return [
    'L_name'     => '材质名称',
    'L_valid'    => '有效',
    'L_type'     => '类别',
    'L_id'       => 'ID',
    'L_supernum' => '快捷码'
];
