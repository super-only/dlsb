<?php

return [
    'Wc_num'          => '仓库编码',
    'Wc_name'         => '仓库名称',
    'Wc_address'      => '仓库地址',
    'Wc_tel'          => '仓库电话',
    'Wc_dd_resperson' => '仓库负责人',
    'Valid'           => '有效',
    'Parentnum'       => '所属仓库',
    'Wc_sort'         => '仓库类别',
    'Wc_storeprefix'  => '前缀编码'
];
