<?php

return [
    'Bd_materialname' => '材料名称',
    'Bd_limber'       => '等级',
    'Bd_type'         => '规格',
    'Bd_lenth'        => '无扣长',
    'Bd_other'        => '另附规格',
    'Fastinput'       => '代号',
    'Bd_perweight'    => '比重(kg)',
    'Bd_default'      => '是否默认',
    'Bd_sort'         => '排序',
    'Bf_plan'         => '方案'
];
