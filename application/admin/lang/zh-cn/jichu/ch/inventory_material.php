<?php

return [
    'type'           => '类别',
    'Im_num'         => '存货编码',
    'Im_name'        => '存货一级类名称',
    'Im_spec'        => '规格',
    'Im_class'       => '材料名称',
    'Im_perweight'   => '比重',
    'Parentnum'      => '父编码',
    'Im_sign'        => '符号',
    'Im_max'         => '最高库存',
    'Im_min'         => '最低库存',
    'Valid'          => '有效',
    'Im_measurement' => '计量单位',
    'Im_class'       => '材料名称',
    'Id'             => 'ID',
    'Kjm'            => '快捷码',
    'Ifimcommision'  => '允许不做理化申请而直接到货入库',
    'Price'          => '计划单位(元/吨)',
    'Im_count'       => '螺栓套数(套/袋)',
    'Im_bd_other'    => '另付规格',
    'Im_length'      => '无长扣',
    'Ifsuanbycount'  => '按数量金额计算',
    'Im_rax'         => '税率',
    'Im_day'         => '库龄(天)'
];
