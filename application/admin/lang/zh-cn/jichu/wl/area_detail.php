<?php

return [
    'Ad_num'     => '地区编码',
    'Ad_name'    => '地区名称',
    'Ad_freight' => '地区运费底价(元)',
    'Parentnum'  => '所属地区',
    'Valid'      => '有效'
];
