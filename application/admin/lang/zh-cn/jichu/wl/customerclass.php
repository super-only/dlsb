<?php

return [
    'Cc_num'  => '客户类型编码',
    'Cc_name' => '客户类型名称',
    'Valid'   => '有效',
    'Cc_memo' => '备注'
];
