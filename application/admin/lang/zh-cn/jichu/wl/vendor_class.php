<?php

return [
    'Vc_num'  => '供应商类型编码',
    'Vc_name' => '供应商类型名称',
    'Valid'   => '有效',
    'Vc_memo' => '备注'
];
