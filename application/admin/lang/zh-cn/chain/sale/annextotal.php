<?php

return [
    'Id'             => 'ID',
    'Date'           => '日期',
    'Pc_num'         => '工程编号',
    'Pc_projectname' => '工程名称',
    'Specifications' => '规格 1铁附件 2地脚螺栓 3钢管杆 4钢管塔',
    'Count'          => '数量',
    'Unit'           => '单位',
    'C_name'         => '客户名称',
    'Amount'         => '总金额',
    'Kp_date'        => '开票日期',
    'Remark'         => '备注',
    'Price'          => '单价',
    'Jg_count'       => '加工数量',
    'Jg_amount'      => '开票金额',
    'Jg_date'        => '开票日期',
    'Writer'         => '制表人',
    'Writer_time'    => '制表时间'
];
