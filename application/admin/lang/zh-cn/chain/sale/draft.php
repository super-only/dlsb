<?php

return [
    'Id'           => 'ID',
    'Type'         => '类型',
    'Project_name' => '工程名称',
    'Writer'       => '制表人',
    'Writer_time'  => '制表日期',
    'Auditor'      => '审核人',
    'Auditor_time' => '审核日期',
    'Remark'       => '备注'
];
