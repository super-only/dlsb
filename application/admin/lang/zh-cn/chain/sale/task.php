<?php

return [
    'T_num'          => '任务单号',
    'C_num'          => '合同号',
    'T_sort'         => '类别',
    'T_date'         => '要求交货时间',
    'T_memo'         => '备注',
    'Auditor'        => '审核人',
    'Writer'         => '制单人',
    'T_writerdate'   => '制单时间',
    'T_auditordate'  => '审核时间',
    'T_company'      => '下发分公司',
    'T_sumweight'    => '总重',
    'T_logtime'      => '塔体交货时间',
    'T_basetime'     => '基础交货时间',
    'T_project'      => '工程名称',
    'T_shortproject' => '工程简称'
];
