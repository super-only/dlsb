<?php

return [
    'Zid'          => '派工单子表ID',
    'Machinename'  => '设备名称',
    'Qty'          => '数量',
    'Finishedtime' => '完工时间',
    'Pdid'         => '操作员'
];
