<?php

return [
    'Snum'           => '排产ID',
    'Pt_num'         => '生产任务下达号',
    'Choose_order'   => '选择工序',
    'Planstarttime'  => '计划开始时间',
    'Planfinishtime' => '计划结束时间',
    'Writer'         => '制单人',
    'Writer_time'    => '制单时间',
    'Actstarttime'   => '实际开始时间',
    'Actfinishtime'  => '实际结束时间'
];
