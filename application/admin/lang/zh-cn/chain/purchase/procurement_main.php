<?php

return [
    'Pm_num'       => '合同编码',
    'Name'         => '合同名称',
    'Type'         => '合同类型',
    'Unit'         => '对方单位',
    'Header'       => '对方负责人',
    'Sign_date'    => '合同签订日期',
    'Procurement_type' => '采购类型',
    'Currency'     => '币种',
    'Rate'         => '汇率(%)',
    'Department'   => '部门',
    'Salesman'     => '业务员',
    'Amount'       => '合同总金额',
    'Count'        => '合同总数量',
    'Url'          => '附件信息',
    'Remark'       => '合同备注',
    'Writer'       => '制单人',
    'Writer_time'  => '制单时间',
    'Auditor'      => '审核人',
    'Auditor_time' => '制单时间',
    'Update_time'  => '修改时间'
];
