<?php

return [
    'Al_num'          => '请购单号',
    'Al_arrivedate'   => '购回日期',
    'Valid'           => '1删除 默认0',
    'Dd_num'          => '申请部门',
    'Al_applypepo'    => '申请人',
    'Al_writedate'    => '请购日期',
    'Al_purchasetype' => '采购类型',
    'Al_writer'       => '制单人',
    'Al_writerdate'   => '制单时间',
    'Modifyer'        => '修改人',
    'Modifytime'      => '修改时间'
];
