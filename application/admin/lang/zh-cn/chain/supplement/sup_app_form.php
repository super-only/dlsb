<?php

return [
    'Trnm_num'         => '单据号',
    'T_num'            => '任务单号',
    'Trnm_writedate'   => '制单时间',
    'Trnm_writer'      => '制单人',
    'Trnm_auditor'     => '审核人',
    'Trnm_auditordate' => '审核时间',
    'Trnm_sort'        => '成品补件(工地部件)',
    'Trnm_ifquick'     => '是否急件',
    'Trnm_ifmath'      => '是否参与结算',
    'Td_id'            => 'TD_ID',
    'Trnm_memo'        => '备注',
    'Trnm_bnum'        => '工程补件单号'
];
