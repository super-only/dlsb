<?php

return [
    'Yme_num'      => '月结单号',
    'Yme_year'     => '年份',
    'Yme_month'    => '月份',
    'Yme_datefrom' => '开始日期',
    'Yme_dateto'   => '终止日期',
    'Writer'       => '制单人',
    'Writerdate'   => '制单时间',
    'Auditor'      => '审核人',
    'Auditordate'  => '审核时间',
    'Yme_memo'     => '备注',
    'Subcorp'      => '分公司'
];
