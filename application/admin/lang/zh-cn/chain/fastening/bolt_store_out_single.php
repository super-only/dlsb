<?php

return [
    'Bsout_id'       => 'ID',
    'V_num'          => '供应商',
    'Out_num'        => '出库编号',
    'Im_num'         => '材料编号',
    'Im_lname'       => '等级',
    'Im_length'      => '无扣长',
    'Outcount'       => '数量',
    'Outweight'      => '重量',
    'Outsort'        => '类别',
    'Outsinglememo'  => '备注',
    'Outnaxprice'    => '不含税单价',
    'Outnaxsumprice' => '不含税总价',
    'Out_bsid'       => '出库ID'
];
