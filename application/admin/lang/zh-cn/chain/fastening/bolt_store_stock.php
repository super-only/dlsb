<?php

return [
    'Bss_id'          => 'ID',
    'Im_num'          => '材料编号',
    'Im_lname'        => '级别',
    'Im_length'       => '无扣',
    'Bss_count'       => '数量',
    'Bss_weight'      => '重量',
    'Bss_place'       => '所在仓库',
    'Bss_sort'        => '类别',
    'V_num'           => '供应商',
    'Bss_naxprice'    => '不含税单价',
    'Bss_naxsummoney' => '不含税总价'
];
