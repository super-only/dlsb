<?php

return [
    'Bsid'          => 'ID',
    'V_num'         => '供应商',
    'Im_num'        => '材料编号',
    'Im_lname'      => '级别',
    'Incount'       => '数量',
    'Inweight'      => '重量',
    'Bsmemo'        => '备注',
    'Inprice'       => '含税单价',
    'Insumprice'    => '含税总价',
    'In_num'        => '入库编号',
    'Insort'        => '类别',
    'Innaxprice'    => '不含税单件',
    'Innaxsumprice' => '不含税总价',
    'Naxrate'       => '税率'
];
