<?php

return [
    'Btxk_id'     => 'ID',
    'Writer'      => '制表人',
    'Writedate'   => '制表时间',
    'Btxk_memo'   => '备注',
    'Td_typename' => '塔型',
    'Td_pressure' => '电压等级',

    'T_company'        => '所属公司',
    'T_Num'            => '任务单号',
    'Dtm_type'         => '塔类型',
    'Dtm_sproject'     => '工程名称',
    'Dtm_ssortproject' => '工程简写',
    'Dtm_stypename'    => '塔型名称',
    'Dtm_spressure'    => '电压等级',
    'Dtm_spicturenum'  => '图号',
    'DtM_sAuditor'     => '审核人',
    'DtM_dTime'        => '审核时间',
    'CustomerName'     => '客户名称',

    'C_Num'          => '合同号',
    'PC_Num'         => '工程编号',
    'T_Sort'         => '产品类别',
    'T_Company'      => '分公司',
    'T_WriterDate'   => '日期',
    't_project'      => '工程名称',
    'TD_TypeName'    => '塔型',
    'TD_TypeNameGY'  => '打钢印',
    'TD_Pressure'    => '电压等级',
    'TD_Count'       => '总基数',
    'state'          => '下达情况',
    'Customer_Name'  => '客户名称',
    'SCD_Part'       => '公共段',
    'SCD_SpPart'     => '专用段'
];
