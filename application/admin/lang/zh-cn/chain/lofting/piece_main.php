<?php

return [
    'Id'          => 'ID',
    'Create_time' => '创建时间',
    'Update_time' => '更新时间',
    'Record_time' => '登记日期',
    'Operator'    => '操作者',
    'Device'      => '设备号',
    'Toggle sub menu'      => '点击切换子菜单'
];
