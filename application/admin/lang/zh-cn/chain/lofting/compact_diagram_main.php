<?php

return [
    'Cdm_num'      => '编码',
    'Td_id'        => 'TD_ID',
    'Cdm_cproject' => '工程名称',
    'Cdm_typename' => '塔型',
    'Cdm_pressure' => '电压',
    'Writer'       => '制表人',
    'Writedate'    => '制表时间',
    'Auditor'      => '审核人',
    'Auditedate'   => '审核时间',
    'Remark'       => '备注',
    'Td_type'      => '塔类型'
];
