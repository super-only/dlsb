<?php

return [
    'Cdbm_num'  => '单据号',
    'Cdm_num'   => '图纸数据id',
    'Cdbm_mark' => '批次',
    'Writer'    => '制表人',
    'Writedate' => '制表日期',
    'Auditor'   => '审核人',
    'Auditdate' => '审核日期',
    'Td_id'     => '图纸数据关联td_id',
    'Scdcount'  => '基数'
];
