<?php

return [
    'Id'           => 'ID',
    'Create_time'  => '创建时间',
    'Update_time'  => '更新时间',
    'Record_time'  => '登记日期',
    'Operator'     => '操作者',
    'Device'       => '设备号',
    'Writer'       => '制单人',
    'Auditor'      => '审核人',
    'Auditor_time' => '审核时间',
    'Order'        => '工序',
    'Zg_amount'    => '杂工工资',
    'Zg_hours'     => '杂工小时'
];
