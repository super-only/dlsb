<?php

return [
    'Mr_num'     => '代料单号',
    'Td_id'      => 'TD_ID',
    'Mr_memo'    => '备注',
    'Writer'     => '制表人',
    'Writerdate' => '制表时间',
    'Writermemo' => '制表人意见',
    'Mr_dept'    => '申请部门'
];
