<?php

return [
    'C_Num'          => '合同号',
    'PC_Num'         => '工程编号',
    'T_Num'          => '任务单号',
    'T_Sort'         => '产品类别',
    'T_Company'      => '分公司',
    'T_WriterDate'   => '日期',
    't_project'      => '工程名称',
    'TD_TypeName'    => '塔型',
    'TD_TypeNameGY'  => '打钢印',
    'TD_Pressure'    => '电压等级',
    'TD_Count'       => '总基数',
    'state'          => '下达情况',
    'Customer_Name'  => '客户名称',
    'SCD_Part'       => '公共段',
    'SCD_SpPart'     => '专用段',

    'TH_Height'      => '呼高',
    'SCD_TPNum'      => '杆塔号'
];
