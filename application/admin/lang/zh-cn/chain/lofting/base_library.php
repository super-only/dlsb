<?php

return [
    'Dtm_iid_pk'    => 'DtM_iID_PK',
    'Dtm_stypename' => '塔型',
    'Dtm_spressure' => '电压等级',
    'Dtm_sauditor'  => '审核人',
    'Dtm_dtime'     => '审核时间',
    'Dtm_sremark'   => '备注'
];
