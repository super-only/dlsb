<?php

return [
    'DC_Num'                => '编号',
    'DC_Editdate'           => '制单时间',
    'DC_Editer'             => '制单人',
    'DC_AuditorDate'        => '审核时间',
    'T_Company'             => '分公司',
    'DC_Memo'               => '备注',

    'T_Num'                 => '任务单号',
    'T_Sort'                => '产品类型',
    'DtM_sProject'          => '工程名称',
    

    'DtM_sTypeName'         => '塔型',
    // 'T_Sort'                => '打钢印',
    'DtM_sPressure'         => '电压等级',

    'CustomerName'          => '客户名称',
    'C_Num'                 => '合同号',

    'C_SaleType'            => '销售类型',
    'PC_Num'                => '工程编号',
    'DtM_sSortProject'      => '工程简称',
    'TD_Count'              => '总数',
    'P_Num'                 => '线路名称',

    'DCD_ID'                => '编号',
    'DCD_PartNum'           => '电焊部件号',
    'DCD_PartName'          => '段名',
    'DCD_Count'             => '组数',
    'DCD_SWeight'           => '单件重量(kg)/(自动计算)',
    'DCD_Type'              => '类别',
    'DCD_Memo'              => '备注',
    'Writer'                => '制表人',
    'WriteDate'             => '制表时间',
    'Auditor'               => '审核人',
    'AuditDate'             => '审核时间',

    'DtMD_sPartsID'         => '零部件号',
    'DtMD_sStuff'           => '名称',
    'DtMD_sMaterial'        => '材质',
    'DtMD_sSpecification'   => '规格',
    'DHS_Thick'             => '长度(mm)',
    'DHS_Height'            => '宽度(mm)',
    'DtMD_fUnitWeight'      => '零件单件重量(kg)',
    'DHS_Count'             => '零件件数',
    'DHS_Length'            => '焊缝长度(mm)',
    'DHS_Grade'             => '焊缝等级',
    'DHS_isM'               => '主件标识',
    'DHS_Memo'              => '备注',

    'chooseDuan'            => '选择段名',
    'ifspecial'             => '是否特殊件号',

    'DtS_Name'              => '段号',
    'DtMD_sPartsID'         => '部件号',
    'DtMD_iUnitCount'       => '数量',
    'DtMD_sRemark'          => '备注',
    'DtMD_sStuff'           => '材料',
    'DtMD_sMaterial'        => '材质',
    'DtMD_sSpecification'   => '规格',
    'DtMD_iLength'          => '长度(mm)',
    'DtMD_fWidth'           => '宽度(mm)',
    'DtMD_iWelding'         => '是否电焊',
    'DtMD_fUnitWeight'      => '单件重量(kg)',
    'DtMD_iUnitHoleCount'   => '单件孔数',
    'DtMD_iCuttingAngle'    => '切角',
    'DtMD_fBackOff'         => '铲背',
    'DtMD_iFireBending'     => '制弯',
    'DtMD_iPressed'         => '压制',
    'DtMD_ISZhuanYong'      => '专用',

];
