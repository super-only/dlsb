<?php

return [
    'L_name'         => '材质',
    'Spec_down'      => '规格小(包含)',
    'Spec_up'        => '规格大(包含)',
    'Thickness_down' => '厚度大(包含)',
    'Thickness_up'   => '厚度小(包含)',
    'Loss'           => '损耗',
    'Tail'           => '尾部',
    'Order'          => '排序'
];
