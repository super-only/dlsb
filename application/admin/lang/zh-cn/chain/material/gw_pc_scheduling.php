<?php

return [
    'G_pc_num'         => '排产计划编码',
    'Pt_num'           => '生产任务下达单',
    'Planstartdate'    => '计划开始日期',
    'Planfinishdate'   => '计划完成日期',
    'Write_time'       => '制单时间',
    'Writer'           => '制单人',
    'Actualstartdate'  => '实际开始日期',
    'Actualfinishdate' => '实际完成日期'
];
