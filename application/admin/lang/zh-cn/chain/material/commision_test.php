<?php

return [
    'Ct_num'         => '委试号',
    'Ct_date'        => '委托日期',
    'Ct_takedate'    => '取件日期',
    'Ct_shape'       => '试件形式',
    'Ct_standard'    => '执行标准',
    'V_num'          => '供应商',
    'Ct_memo'        => '备注',
    'Ct_acceptpepo'  => '接受试验经办人',
    'Ct_requestpepo' => '委托单位经办人',
    'Writer'         => '制表人',
    'Writetime'      => '制表时间',
    'Auditor'        => '审核人',
    'Audittime'      => '审核时间',
    'Ct_type'        => '试验类型'
];
