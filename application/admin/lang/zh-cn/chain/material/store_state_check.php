<?php

return [
    'Ssc_num'       => '盘点单号',
    'Si_otherid'    => '入库编号',
    'Ssc_flag'      => '盘点类型',
    'Ssc_date'      => '盘点日期',
    'Ssc_warehouse' => '所在仓库',
    'Writer'        => '制表人',
    'Writedate'     => '制表时间',
    'Auditor'       => '审核人',
    'Audidate'      => '审核时间',
    'Ssc_memo'      => '备注'
];
