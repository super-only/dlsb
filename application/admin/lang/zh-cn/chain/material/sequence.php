<?php

return [
    'Pc_id'        => '排产编号',
    'Pt_num'       => '生产任务下达',
    'Td_id'        => '塔信息',
    'Writer'       => '制单人',
    'Writer_time'  => '制单时间',
    'Auditor'      => '审核人',
    'Auditor_time' => '审核时间'
];
