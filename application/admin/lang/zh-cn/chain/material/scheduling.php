<?php

return [
    'Id'           => '套料编号',
    'Pt_num'       => '生产任务下达',
    'Warehouse'    => '仓库',
    'Td_id'        => '塔型',
    'Writer'       => '制单人',
    'Writer_time'  => '制单时间',
    'Auditor'      => '审核人',
    'Auditor_time' => '审核时间',
    'Pc_flag'      => '是否排产',

];
