<?php

return [
    'Wr_num'       => '领料单号',
    'Wr_date'      => '领料日期',
    'Wr_warehouse' => '所在仓库',
    'V_name'       => '单位名称',
    'Wr_lu'        => '炉号',
    'Wr_pi'        => '批号',
    'Wr_buydate'   => '进货日期',
    'Wr_buypi'     => '进货批次',
    'Wr_zb'        => '质保编号',
    'Wr_hy'        => '化验号',
    'Wr_person'    => '领料人',
    'Wr_dept'      => '领料班组',
    'Wr_memo'      => '备注',
    'Writer'       => '制单人',
    'Writedate'    => '制单日期',
    'Auditor'      => '审核',
    'Audidate'     => '审核日期',
    'Td_typename'  => '塔型',
    'Wr_send'      => '已发料',
    'Pt_num'       => '下达单号'
];
