<?php

return [
    'Im_num'       => '材料编号',
    'Im_spec'      => '规格',
    'L_name'       => '材质',
    'Ss_length'    => '长度',
    'Ss_width'     => '宽度',
    'Ss_count'     => '库存数量',
    'Ss_weight'    => '库存重量(kg)',
    'Ss_sumprice'  => '账面金额',
    'Ss_minstore'  => '最高库存',
    'Ss_maxstore'  => '最低库存',
    'Ss_warehouse' => '所在仓库',
    'Sf_count'     => '账面数量',
    'Sf_weight'    => '账面重量',
    'Asf_count'    => '配料后数量',
    'Asf_weight'   => '配料后重量(kg)'
];
