<?php

return [
    'S_id'        => '库存ID',
    'Wc_num'      => '供应商ID',
    'Ir_num'      => '材料ID',
    'S_count'     => '库存数量',
    'S_price'     => '含税金额',
    'S_money'     => '不含税金额',
    'S_lastcount' => '上期结存数量',
    'S_lastprice' => '上期结存均价',
    'S_lastmoney' => '上期结存金额',
    'S_lastdate'  => '上期结存日期'
];
