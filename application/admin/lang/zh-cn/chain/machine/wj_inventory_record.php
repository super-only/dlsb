<?php

return [
    'Ir_num'        => '存货编码',
    'Ir_name'       => '存货名称',
    'Ir_spec'       => '规格',
    'Sortnum'       => '所属类别',
    'Ir_unit'       => '计量单位',
    'Ir_taxrate'    => '税率',
    'Ir_symbol'     => '符号',
    'Ir_priceplan'  => '入库参考价',
    'Ir_proportion' => '比重',
    'Ir_lowamount'  => '最低库存',
    'Ir_highamount' => '最高库存',
    'Ir_ifcheck'    => '需要质检',
    'Writer'        => '制单人',
    'Writedate'     => '制单时间',
    'Valid'         => '有效'
];
