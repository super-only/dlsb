<?php

return [
    'Ck_id'       => '出库ID',
    'Ck_num'      => '出库编号',
    'Ck_date'     => '出库日期',
    'Wc_num'      => '仓库',
    'Ck_takeman'  => '领用人',
    'Ck_takedept' => '领用部门',
    'Ck_banzu'    => '班组',
    'Ck_cksort'   => '出库类别',
    'Ck_project'  => '工程',
    'Ck_memo'     => '备注',
    'Writer'      => '制单人',
    'Writedate'   => '制单时间',
    'Assessor'    => '审核人',
    'Assessdate'  => '审核时间'
];
