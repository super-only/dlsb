<?php

return [
    'Is_num'    => '编号',
    'Is_name'   => '类别名称',
    'Parentnum' => '所属类别',
    'Writer'    => '制表人',
    'Writedate' => '制表时间'
];
