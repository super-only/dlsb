<?php

return [
    'Bl_id'        => 'ID',
    'Bl_num'       => '结存单号',
    'Bl_date'      => '结存日期',
    'Writer'       => '制单人',
    'Writedate'    => '制单日期',
    'Assessor'     => '审核人',
    'Assessdate'   => '审核日期',
    'Ir_num'       => '存货编码',
    'Bl_lastcount' => '上期结存数量',
    'Bl_lastprice' => '上期结存单价(元)',
    'Bl_lastmoney' => '上期结存金额(元)',
    'Bl_rkcount'   => '本期入库数量',
    'Bl_rkprice'   => '本期入库单价(元)',
    'Bl_rkmoney'   => '本期入库金额(元)',
    'Bl_ckcount'   => '本期出库数量',
    'Bl_ckprice'   => '本期出库单价(元)',
    'Bl_ckmoney'   => '本期出库金额(元)',
    'Bl_endcount'  => '本期结存数量',
    'Bl_endprice'  => '本期结存单价(元)',
    'Bl_endmoney'  => '本期结存金额(元)',
    'Wc_num'       => '仓库'
];
