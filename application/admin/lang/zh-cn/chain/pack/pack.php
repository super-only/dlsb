<?php

return [
    'P_id'        => '编号',
    'Scd_id'      => '杆塔号',
    'P_count'     => '配段数量',
    'P_memo'      => '备注',
    'Writer'      => '制单人',
    'Auditor'     => '审核人',
    'Writedate'   => '制单时间',
    'Auditordate' => '审核时间',
    'P_flag'      => '0:公共段 1:专用段 2:混合打'
];
