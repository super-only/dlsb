<?php

return [
    'Pum_num'     => '进仓单号',
    'Pum_date'    => '进仓日期',
    'Writer'      => '制单人',
    'Writedate'   => '制单时间',
    'Auditor'     => '审核人',
    'Auditordate' => '审核时间',
    'Td_id'       => 'TD_ID',
    'Pum_memo'    => '备注',
    'Pu_point'    => '存放点',
    'Pu_lb'       => '入库类别'
];
