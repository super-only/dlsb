<?php

return [
    'Si_num'        => '发货单号',
    'C_num'         => '合同号',
    'Si_date'       => '发货日期',
    'Writer'        => '制单人',
    'Auditor'       => '审核人',
    'Writedate'     => '制单时间',
    'Auditordate'   => '审核时间',
    'Si_way'        => '运输方式',
    'Si_address'    => '发往地址',
    'Si_customer'   => '客户单位',
    'Si_saleman'    => '业务员',
    'Si_phone'      => '联系电话',
    'Si_contractor' => '联系人',
    'T_company'     => '分公司',
    'Si_memo'       => '备注',
    'Arrivedate'    => '到货时间',
    'Si_project'    => '工程名称',
    'Ifbj'          => '是否补件',
    'Dxunit'        => '出库类别'
];
