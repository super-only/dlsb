<?php
/**
 * 登录接口页面
 */
namespace app\applet\controller;

use app\common\controller\Base;
use think\Validate;
use think\Db;
use Exception;

class Piece extends Base
{
    protected $noNeedLogin = ['getOrder','getMachine',"getEmployee","getTowerEx"];
    protected $noNeedRight = ["*"];
    protected $wayList = [0=>"weight",1=>"hole",2=>"meter"];
    protected $layout = '';
    protected $operatorModel = null,$viewModel = null,$orderCon,$machineCon;
    public function _initialize()
    {
        parent::_initialize();
        //移除HTML标签
        $this->request->filter('trim,strip_tags,htmlspecialchars');
        $this->model = new \app\applet\model\piece\AppletPieceDetail();
        $this->viewModel = new \app\applet\model\piece\AppletDetailView();
        $this->operatorModel = new \app\applet\model\piece\AppletPieceOperate();
        $this->orderCon = (new \app\applet\model\piece\AppletOrder())->getOrder();
        $this->machineCon = (new \app\applet\model\piece\AppletMachine())->getMachine();
    }

    /**
     * 获取工序接口
     */
    public function getOrder()
    {
        $orderCon = $this->orderCon;
        $gxList = $this->_getChild($orderCon,[]);
        return response_success("成功",[
            'gxList' => $gxList
        ]);
    }

    /**
     * 获取机器接口
     * order_id 工序
     */
    public function getMachine()
    {
        $this->request->filter(['strip_tags', 'trim']);
        $params = $this->request->post();
        if(($params["order_id"]??'')==false) return response_error("请选择工序");
        $where = [];
        $where["am.order_id"] = ["=",$params["order_id"]];
        $machineCon = (new \app\applet\model\piece\AppletMachine())->getMachine($where);
        $sbList = [];
        foreach($machineCon as $v){
            $sbList[] = [
                "value" => $v["machine_id"],
                "label" => $v["name"],
            ];
        }
        return response_success("成功",[
            'sbList' => $sbList
        ]);
    }

    /**
     * 获取塔型接口
     * 其实是固定的 这个到时候看
     */
    public function getTowerEx()
    {
        return response_success("成功",[
            'exList' => [
                [
                    "value" => "",
                    "label" => "角钢塔"
                ],[
                    "value" => "tfj_",
                    "label" => "铁附件"
                ],[
                    "value" => "ggg_",
                    "label" => "钢管杆"
                ],[
                    "value" => "ggt_",
                    "label" => "钢管塔"
                ]
            ]
        ]);
    }

    /**
     * 获取二维码信息接口 未完成
     */
    public function qrNews()
    {
        $this->request->filter(['strip_tags', 'trim']);
        $params = $this->request->post();
        if($params){
            $validate = new Validate([
                //A5卡片ID
                'PTD_ID'  => 'require',
                'machine_id'   => 'require'
            ], [
                'PTD_ID.require' => "二维码信息缺失",
                "machine_id.require" => "设备必选"
            ]);
            $res = $validate->check($params);
            if($res !== true) return response_error($validate->getError());
            // 生产任务下达单号
            [$PTD_ID,$machine_id,$ex] = [$params["PTD_ID"],$params["machine_id"],$params["ex"]??''];
            $machineCon = (new \app\applet\model\piece\AppletMachine())->getMachine(["machine_id"=>["=",$machine_id]]);
            if(!isset($machineCon[$machine_id])) return response_error("设备有误，请重新选择");
            $result = $this->getRowDetail($PTD_ID,$ex,$machineCon[$machine_id]);
            if($result["code"]==0) return response_error($result["msg"]);
            else return response_success("成功",["tzdh"=>$result["data"]]);
        }
        return response_error("有误，请联系技术人员");
    }

    protected function getRowDetail($PTD_ID='',$ex='',$machineList=[],$findOne=[])
    {
        $result = [
            "code" => 0,
            "msg" => '',
            "data" => []
        ];
        $piece_id = !empty($findOne)?$findOne["id"]:0;
        $producetaskModel = new \app\admin\model\chain\lofting\ProduceTask([],$ex);
        $detail = $producetaskModel->alias("pt")
            ->join([$ex."producetaskdetail"=>"ptd"],"ptd.PT_Num=pt.PT_Num")
            ->join([$ex."dtmaterialdetial"=>"dd"],"ptd.DtMD_ID_PK=dd.DtMD_ID_PK")
            ->join(["taskdetail"=>"td"],"td.TD_ID = pt.TD_ID")
            ->where([
                "PTD_ID" => ["=",$PTD_ID]
            ])
            ->field(
                "ptd.PTD_ID,
                pt.PT_Num,
                ptd.IF_Shi,
                ptd.DtMD_ID_PK,
                ptd.PTD_Count,
                dd.DtMD_sPartsID,
                dd.DtMD_sStuff,
                dd.DtMD_sMaterial,
                dd.DtMD_sSpecification,
                dd.DtMD_iLength,
                dd.DtMD_fWidth,
                dd.DtMD_iTorch,
                (case when dd.DtMD_sStuff='角钢' then ptd.PTD_sumWeight else 0 end) as base_weight,
                (case when dd.DtMD_sStuff='钢板' then ptd.PTD_sumWeight else 0 end) as jb_weight,
                (ptd.PTD_sumWeight) as gy_weight,
                dd.DtMD_iUnitCount,
                dd.DtMD_iUnitHoleCount,
                (dd.DtMD_iUnitHoleCount*PTD_Count) as base_hole,
                (case when dd.DtMD_iWelding>0 then ptd.PTD_sumWeight else 0 end) as qg_weight,
                (case when dd.DtMD_iWelding>0 then ptd.PTD_sumWeight else 0 end) as zd_weight,
                (case when dd.DtMD_iWelding>0 then ptd.PTD_sumWeight else 0 end) as dh_weight,
                (case when dd.DtMD_fBackOff>0 then ptd.PTD_sumWeight else 0 end)+(case when dd.DtMD_iBackGouging>0 then ptd.PTD_sumWeight else 0 end) as bc_weight,
                (case when dd.DtMD_iCuttingAngle>0 then ptd.PTD_sumWeight else 0 end) as qj_weight,
                (case when dd.DtMD_DaBian>0 then ptd.PTD_sumWeight else 0 end)+(case when dd.DtMD_KaiHeJiao>0 then ptd.PTD_sumWeight else 0 end)+(case when dd.DtMD_iFireBending>0 then ptd.PTD_sumWeight else 0 end) as hq_weight,
                dd.DtMD_GeHuo,
                dd.DtMD_ZuanKong,
                dd.DtMD_sRemark,
                pt.C_ProjectName,
                pt.PT_Memo,
                pt.PT_DHmemo,
                td.TD_TypeName,
                td.TD_Pressure")
            ->find();
        if(!$detail){
            $result["msg"] = "需要重新扫描该部件的最新A5卡片";
            return $result;
        }
        $type = $piece_id?$findOne["type"]:$machineList["way"];
        $field = $piece_id?$findOne["field"]:$machineList["field"];
        $row = [
            // 塔前缀
            "ex" => $ex,
            //唯一ID 无法更改
            "PTD_ID" => $detail["PTD_ID"],
            //DtMD_ID_PK 无法更改
            "DtMD_ID_PK" => $detail["DtMD_ID_PK"],
            //生产下达单号 无法更改
            "PT_Num" => $detail["PT_Num"],
            //工程 无法更改
            "project" => $detail["C_ProjectName"],
            //电压  无法更改
            "pressure" => $detail["TD_Pressure"],
            //塔型  无法更改
            "typename" => $detail["TD_TypeName"],
            //套用塔型 无法更改
            "tytower" => $detail["PT_DHmemo"],
            //零件编码 无法更改
            "parts" => $detail["DtMD_sPartsID"],
            //名称 无法更改
            "stuff" => $detail["DtMD_sStuff"],
            //材质 无法更改
            "lname" => $detail["DtMD_sMaterial"],
            //规格 无法更改
            "spec" => $detail["DtMD_sSpecification"],
            //厚度 无法更改
            "torch" => $detail["DtMD_iTorch"],
            //工序备注 无法更改
            "remark" => $detail["DtMD_sRemark"],
            //记录方式 无法更改 0产量(kg)/1数量(个)/2米数(m)
            "type" => $type,
            //field 无法更改
            "field" => $field,
            //总数 无法更改
            "sumCount" => null,
            //可报重量/孔数 无法更改
            "count" => null,
            //本次报的量
            "value" => 0,

            //自用
            "base_weight" => $machineList["machine_id"]==1?$detail["base_weight"]*2:$detail["base_weight"],
            "jb_weight" => $detail["jb_weight"],
            "gy_weight" => $detail["gy_weight"],
            "base_hole" => $detail["base_hole"],
            "qg_weight" => $detail["qg_weight"],
            "zd_weight" => $detail["zd_weight"],
            "dh_weight" => $detail["dh_weight"],
            "bc_weight" => $detail["bc_weight"],
            "qj_weight" => $detail["qj_weight"],
            "hq_weight" => $detail["hq_weight"],
            "price_type" => $type,
        ];
    
        if($type!=2){
            $row["sumCount"] = ($detail[$field]??0);
            $way = $this->wayList;
            $where = [
                "PTD_ID" => ["=",$PTD_ID],
                "ex" => ["=",$ex],
                "field" => ["=",$field]
            ];
            if($piece_id) $where["id"] = ["<>",$piece_id];
            $partValue = $this->model->where($where)->field("sum(weight) as weight,sum(hole) as hole")->find();
            $row["b_hole"] = $partValue["hole"]??0;
            $row["b_weight"] = $partValue["weight"]??0;
            $syHoleCount = ($detail[$field]??0)-($partValue?$partValue[$way[$type]]:0);
            $row["count"] = $syHoleCount>0?$syHoleCount:0;
        }
        $result["code"] = 1;
        $result["data"] = $row;
        return $result;
    }

    public function savePiece()
    {
        $this->request->filter(['strip_tags', 'trim']);
        $params = $this->request->post();
        if($params){
            $validate = new Validate([
                //A5卡片ID
                'PTD_ID'  => 'require',
                'value'   => 'require',
                'zyry' => 'require',
            ], [
                'PTD_ID.require' => "二维码信息缺失",
                "value.require" => "产量必填",
                "zyry.require" => "作业人员必填",
            ]);
            $res = $validate->check($params);
            if($res !== true) return response_error($validate->getError());
            // 生产任务下达单号
            [$PTD_ID,$value,$machine_id,$zyry,$bz,$ex,$id,$writer_time] = [$params["PTD_ID"],$params["value"],$params["machine_id"]??0,$params["zyry"],$params["bz"]??'',$params["ex"]??'',$params["id"]??0,$params["writer_time"]??''];
            if($id){
                $findOne = $this->model->where("id",$id)->find();
                if(!$findOne) return response_error("未找到该信息，请稍后重试");
                if($findOne["auditor"]) return response_error("已审核，无法修改");
                else if($findOne["day_id"]) return response_error("已在审核中，无法修改");
                $findOne = $findOne->toArray();
                $machine_id = $findOne["machine_id"];
                $writer_time = $writer_time?$writer_time:$findOne["writer_time"];
            }else $findOne=[];
            $machineCon = (new \app\applet\model\piece\AppletMachine())->getMachine(["machine_id"=>["=",$machine_id]]);
            if(!isset($machineCon[$machine_id])) return response_error("设备有误，请重新选择");
            $result = $this->getRowDetail($PTD_ID,$ex,$machineCon[$machine_id],$findOne);
            if($result["code"]==0) return response_error($result["msg"]);
            if($machineCon[$machine_id]["way"]!=2){
                $row = $result["data"];
                if($value>$row["count"]) return response_error("上报量{$value}大于可报数量{$row['count']}，请重新填写");
                // 如果是产量 如果是孔数
                if($machineCon[$machine_id]["way"]==0) $row["weight"] = $value;
                else{
                    $row["hole"] = $value;
                    //算重量 dz = (总重量-已报重量)/(总孔数-已报孔数)
                    $dz = ($row["gy_weight"]-$row["b_weight"])>0?(($row["gy_weight"]-$row["b_weight"])/($row["sumCount"]-$row["b_hole"])):0;
                    $row["weight"] = $dz*$value;
                } 
            }else $row["meter"] = $value;
            $row["writer"] = $this->_user;
            $row["writer_time"] = $writer_time?$writer_time:date("Y-m-d H:i:s");
            $row["order_id"] = $machineCon[$machine_id]["order_id"];
            $row["machine_id"] = $machine_id;
            $row["memo"] = $bz;
            $result = false;
            Db::startTrans();
            try {
                if($id) $result = $this->model->allowField(true)->save($row,["id"=>$id]);
                else{
                    $result = $this->model->allowField(true)->save($row);
                    $id = $this->model->id;
                }
                $operaterList = [];
                foreach($zyry as $v){
                    if(!$v) continue;
                    $operaterList[$v] = [
                        "d_id" => $id,
                        "operate_id" => $v
                    ];
                }
                if(!empty($operaterList)){
                    $exList = $this->operatorModel->where("d_id",$id)->column("operate_id");
                    if($exList!=array_column($operaterList,'operate_id')){
                        $this->operatorModel->where("d_id",$id)->delete();
                        $oresult = $this->operatorModel->allowField(true)->saveAll($operaterList);
                    }
                }else throw new Exception("请选择操作员工");
                Db::commit();
            }catch (\Exception $e){
                return response_error($e->getMessage());
                Db::rollback();
            }
            return response_success("成功",["id"=>$id]);
        }
        return response_error("有误，请联系技术人员");
    }

    public function editPiece()
    {
        $this->request->filter(['strip_tags', 'trim']);
        $params = $this->request->post();
        if($params){
            $id = $params["id"]??0;
            $row = $this->viewModel->where("id",$id)->find();
            if(!$row) return response_error("未找到该信息，请稍后重试");
            // $machineCon = (new \app\applet\model\piece\AppletMachine())->getMachine(["machine_id"=>["=",$row["machine_id"]]]);
            // if(!isset($machineCon[$row["machine_id"]])) return response_error("设备有误，请重新选择");
            $row = $row->toArray();
            $result = $this->getRowDetail($row["PTD_ID"],$row["ex"],$this->machineCon[$row["machine_id"]],$row);
            if($result["code"]==0) return response_error($result["msg"]);
            else{
                $data = $result["data"];
                $data["order_id"] = $this->orderCon[$row["order_id"]]["name"];
                $data["machine_id"] = $this->machineCon[$row["machine_id"]]["name"];
                $data["operator"] = trim($row["operator"],",");
                $data["memo"] = $row["memo"];
                $data["writer"] = $row["writer"];
                $data["writer_time"] = $row["writer_time"];
                $way = $this->wayList;
                $data["value"] = $row[$way[$row["type"]]];
                return response_success("成功",["editList"=>$data]);
            }
        }
        return response_error("有误，请联系技术人员");
    }

    public function delPiece()
    {
        $this->request->filter(['strip_tags', 'trim']);
        $params = $this->request->post();
        if($params){
            $id = $params["id"]??0;
            $row = $this->model->where("id",$id)->find();
            if(!$row) return response_error("未找到该信息，请稍后重试");
            if($row["day_id"]) return response_error("已结算，无法删除");
            Db::startTrans();
            try {
                $result = $row->delete();
                if($result) $this->operatorModel->where("d_id",$id)->delete();
                Db::commit();
            }catch (\Exception $e){
                Db::rollback();
                return response_error($e->getMessage());
            }
            return response_success("成功");
        }
        return response_error("有误，请联系技术人员");
    }

    /**
     * 本人查看
     * 
     */
    public function pieceList()
    {
        //$this->admin信息 暂时无 token会验证时谁 暂空 只可以看自己的
        $this->request->filter(['strip_tags', 'trim']);
        $params = $this->request->post();
        if($params){
            [
                $time,
                $limit,
                $page
            ] = [
                $params["time"]??'',
                $params["limit"]??10000,
                $params["page"]??1
            ];
            $limit = $limit?$limit:10000;
            $page = $page?$page:1;
            $time = $params["time"]??"";
            $user = $this->_user;
            $orderCon = $this->orderCon;
            $machineCon = $this->machineCon;
            $where = [
                "flag" => ["=",0]
            ];
            $time?$where["writer_time"] = ["between time",explode(" ~ ",$time)]:"";
            $list = $this->viewModel
                ->where($where)->where(function($query) use ($user){
                    $query->where(["writer"=>["=",$user]])->whereor(["operator"=>["LIKE","%,".$user.",%"]]);
                })->order("writer_time desc")->limit(($page-1)*$limit,$limit)
                ->select();
            $list = $list?collection($list)->toArray():[];
            foreach($list as $k=>$v){
                $list[$k]["order_id"] = $orderCon[$v["order_id"]]["name"];
                $list[$k]["machine_id"] = $machineCon[$v["machine_id"]]["name"];
                $list[$k]["operator"] = trim($v["operator"],",");
            }
            $total = $this->viewModel->where($where)->where(function($query) use ($user){
                    $query->where(["writer"=>["=",$user]])->whereor(["operator"=>["LIKE","%,".$user.",%"]]);
                })->value("count(id) as count");
            $data = [
                "data" => $list,
                "total" => $total,
                "sum_weight" => array_sum(array_column($list,'weight')),
                "sum_hole" => array_sum(array_column($list,'hole')),
                "sum_meter" => array_sum(array_column($list,'meter'))
            ];
            return response_success("成功",$data);
        }
        return response_error("有误，请联系技术人员");
    } 

    

    /**
     * 获取员工
     * name 姓名
     * cardid 工号
     */
    public function getEmployee()
    {
        $this->request->filter(['strip_tags', 'trim']);
        $params = $this->request->post();
        if($params){
            [$name,$cardid,$limit,$page] = [$params["name"]??'',$params["cardid"]??'',$params["limit"]??10000,$params["page"]??1];
            $limit = $limit?$limit:10000;
            $page = $page?$page:1;
            $where = [
                "DD_Num"=>["=","0209"]
            ];
            if($name) $where["E_Name"] = ["LIKE", "%".$name."%"];
            if($cardid) $where["E_Num"] = ["LIKE", "%".$cardid."%"];
            $peopleList = (new \app\admin\model\jichu\jg\Employee())->where($where)->order("E_Num",$cardid)->column("E_PerNum as value,E_PerNum as lable,'' as checked");
            foreach($peopleList as &$v){
                $v["checked"] = false;
            }
            $peopleList = array_slice($peopleList,(($page-1)*$limit),$limit);
            return response_success("成功",[
                'zyryList' => array_values($peopleList)
            ]);
        }
        return response_error("有误，请联系技术人员");
    }

    /**
     * 获取近期某个设备的操作人员
     */
    public function getRecently()
    {
        $this->request->filter(['strip_tags', 'trim']);
        $params = $this->request->post();
        if($params){
            $user = $this->_user;
            $machine_id = $params["machine_id"]??'';
            $where = [
                // "writer_time" => ["> time",date("Y-m-d",strtotime("-1 month"))],
                "writer" => ["=",$user]
            ];
            if($machine_id) $where["machine_id"] = ["=", $machine_id];
            $getDetailId = (new \app\applet\model\piece\AppletPieceDetail())->where($where)->order("writer_time desc")->value("id");
            if(!$getDetailId) return response_success("成功",[
                'zyryList' => []
            ]);
            $peopleList = (new \app\applet\model\piece\AppletPieceOperate())
                ->where(["d_id"=>["=",$getDetailId]])->column("operate_id as value,operate_id as lable,'' as checked");
            foreach($peopleList as &$v){
                $v["checked"] = false;
            }
            return response_success("成功",[
                'zyryList' => array_values($peopleList)
            ]);
        }
        return response_error("有误，请联系技术人员");
    }

    protected function _getChild($orderCon=[],$gxList=[])
    {
        foreach($orderCon as $k=>$v){
            if($v["pid"] == 0) $gxList[] = [
                "value" => $v["order_id"],
                "label" => $v["name"],
                "children" => []
            ];
            else{
                $gxList = $this->_getSecond($v,$gxList);
            }
        }
        return $gxList;
    }

    protected function _getSecond($ini,$nextList)
    {
        foreach($nextList as $k=>$v){
            if($v["value"]==$ini["pid"]){
                $nextList[$k]["children"][] = [
                    "value" => $ini["order_id"],
                    "label" => $ini["name"],
                    "children" => []
                ];
            }else if(!empty($nextList["children"])){
                $nextList[$k] = $this->_getSecond($ini,$nextList["children"]);
            }
        }
        return $nextList;
    }
}