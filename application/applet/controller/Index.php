<?php
/**
 * 登录接口页面
 */
namespace app\applet\controller;

// use app\admin\model\Admin;

use app\admin\model\jichu\jg\Employee;
use app\common\controller\Base;
use fast\Random;
use PDO;
use think\Config;
use think\Validate;

class Index extends Base
{
    protected $noNeedLogin = ['login'];
    protected $noNeedRight = ["*"];
    protected $layout = '';
    protected $logined = false; //登录状态

    public function _initialize()
    {
        parent::_initialize();
        //移除HTML标签
        $this->request->filter('trim,strip_tags,htmlspecialchars');
    }

    public function index()
    {
        echo "hello world";
    }

    /**
     * 登录
     * username 姓名 可以废弃
     * password 密码
     * cardid 员工号
     */
    public function login()
    {   
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        $params = $this->request->post();
        if($params){
            
            $validate = new Validate([
                // 'cardid'  => 'require',
                'username'  => 'require',
                'password'   => 'require|min:6|max:18' 
            ], [
                // 'cardid.require' => "工号必填",
                'username.require' => "用户名必填",
                "password.require" => "密码必填",
                "password.min" => "密码不能小于6位",
                "password.max" => "密码不能大于18位"
            ]);
            $res = $validate->check($params);
            if($res !== true) return response_error($validate->getError());
            $where = [
                "E_PerNum" => ["=",$params["username"]],
                // "cardid" => ["=",$params["cardid"]]
            ];
            $admin = (new Employee())->where($where)->find();
            if(!$admin) return response_error("员工信息错误");
            if($admin["E_LeaveTime"]<=date("Y-m-d H:i:s",time())) return response_error("员工状态无法登录");
            if (Config::get('fastadmin.login_failure_retry') && $admin->loginfailure >= 10 && time() - $admin->updatetime < 86400) return response_error("请在一天后重新尝试登录");
            if ($admin->password != md5(md5($params["password"]) . $admin->salt)) {
                $admin->loginfailure++;
                $admin->save();
                return response_error('密码错误');
            }
            $admin->loginfailure = 0;
            $admin->logintime = time();
            $admin->loginip = request()->ip();
            $token = Random::uuid();
            $admin->token = $token;
            $admin->save();
            return response_success("登录成功",[
                'username' => $admin->E_PerNum,
                "token" => $token
            ]);
        }
        return response_error("有误，请联系技术人员");
    }

    public function logout()
    {
        $token = $_SERVER['HTTP_TOKEN'];
        if(!$token) return response_success("已退出登录，请刷新后重试");
        $admin = Employee::get($this->_user);
        if ($admin) {
            $admin->token = '';
            $admin->save();
        }
        $this->logined = false; //重置登录状态
        return response_success("已退出登录");
    }

    public function createSalt()
    {
        die;
        $list = (new Employee())->select();
        foreach($list as $k=>$v)
        {
            $newSalt = substr(md5(uniqid(true)), 0, 6);
            $adminPassword = $v["E_Mobile"]?trim($v["E_Mobile"]):"123456";
            $newPassword = md5(md5($adminPassword) . $newSalt);
            $list[$k]["salt"] = $newSalt;
            $list[$k]["password"] = $newPassword;
        }
        $list = $list?collection($list)->toArray():[];
        $result = (new Employee())->allowField(true)->saveAll($list);
        pri(count($result),1);
    }
}