<?php

use app\common\model\Category;
use fast\Form;
use fast\Tree;
use think\Db;
use think\Response;
use fast\Http;
use think\Env;

if (!function_exists('build_select')) {

    /**
     * 生成下拉列表
     * @param string $name
     * @param mixed  $options
     * @param mixed  $selected
     * @param mixed  $attr
     * @return string
     */
    function build_select($name, $options, $selected = [], $attr = [],$type=0)
    {
        $options = is_array($options) ? $options : explode(',', $options);
        $selected = is_array($selected) ? $selected : explode(',', $selected);
        return Form::select($name, $options, $selected, $attr,$type);
    }
}

if (!function_exists('build_radios')) {

    /**
     * 生成单选按钮组
     * @param string $name
     * @param array  $list
     * @param mixed  $selected
     * @return string
     */
    function build_radios($name, $list = [], $selected = null)
    {
        $html = [];
        $selected = is_null($selected) ? key($list) : $selected;
        $selected = is_array($selected) ? $selected : explode(',', $selected);
        foreach ($list as $k => $v) {
            $html[] = sprintf(Form::label("{$name}-{$k}", "%s {$v}"), Form::radio($name, $k, in_array($k, $selected), ['id' => "{$name}-{$k}"]));
        }
        return '<div class="radio">' . implode(' ', $html) . '</div>';
    }
}

if (!function_exists('build_checkboxs')) {

    /**
     * 生成复选按钮组
     * @param string $name
     * @param array  $list
     * @param mixed  $selected
     * @return string
     */
    function build_checkboxs($name, $list = [], $selected = null)
    {
        $html = [];
        $selected = is_null($selected) ? [] : $selected;
        $selected = is_array($selected) ? $selected : explode(',', $selected);
        foreach ($list as $k => $v) {
            $html[] = sprintf(Form::label("{$name}-{$k}", "%s {$v}"), Form::checkbox($name, $k, in_array($k, $selected), ['id' => "{$name}-{$k}"]));
        }
        return '<div class="checkbox">' . implode(' ', $html) . '</div>';
    }
}


if (!function_exists('build_category_select')) {

    /**
     * 生成分类下拉列表框
     * @param string $name
     * @param string $type
     * @param mixed  $selected
     * @param array  $attr
     * @param array  $header
     * @return string
     */
    function build_category_select($name, $type, $selected = null, $attr = [], $header = [])
    {
        $tree = Tree::instance();
        $tree->init(Category::getCategoryArray($type), 'pid');
        $categorylist = $tree->getTreeList($tree->getTreeArray(0), 'name');
        $categorydata = $header ? $header : [];
        foreach ($categorylist as $k => $v) {
            $categorydata[$v['id']] = $v['name'];
        }
        $attr = array_merge(['id' => "c-{$name}", 'class' => 'form-control selectpicker'], $attr);
        return build_select($name, $categorydata, $selected, $attr);
    }
}

if (!function_exists('build_toolbar')) {

    /**
     * 生成表格操作按钮栏
     * @param array $btns 按钮组
     * @param array $attr 按钮属性值
     * @return string
     */
    function build_toolbar($btns = null, $attr = [])
    {
        $auth = \app\admin\library\Auth::instance();
        $controller = str_replace('.', '/', strtolower(think\Request::instance()->controller()));
        $btns = $btns ? $btns : ['refresh', 'add', 'edit', 'del', 'import'];
        $btns = is_array($btns) ? $btns : explode(',', $btns);
        $index = array_search('delete', $btns);
        if ($index !== false) {
            $btns[$index] = 'del';
        }
        $btnAttr = [
            'refresh' => ['javascript:;', 'btn btn-primary btn-refresh', 'fa fa-refresh', '', __('Refresh')],
            'add'     => ['javascript:;', 'btn btn-success btn-add', 'fa fa-plus', __('Add'), __('Add')],
            'edit'    => ['javascript:;', 'btn btn-success btn-edit btn-disabled disabled', 'fa fa-pencil', __('Edit'), __('Edit')],
            'del'     => ['javascript:;', 'btn btn-danger btn-del btn-disabled disabled', 'fa fa-trash', __('Delete'), __('Delete')],
            'import'  => ['javascript:;', 'btn btn-info btn-import', 'fa fa-upload', __('Import'), __('Import')],
        ];
        $btnAttr = array_merge($btnAttr, $attr);
        $html = [];
        foreach ($btns as $k => $v) {
            //如果未定义或没有权限
            if (!isset($btnAttr[$v]) || ($v !== 'refresh' && !$auth->check("{$controller}/{$v}"))) {
                continue;
            }
            list($href, $class, $icon, $text, $title) = $btnAttr[$v];
            //$extend = $v == 'import' ? 'id="btn-import-file" data-url="ajax/upload" data-mimetype="csv,xls,xlsx" data-multiple="false"' : '';
            //$html[] = '<a href="' . $href . '" class="' . $class . '" title="' . $title . '" ' . $extend . '><i class="' . $icon . '"></i> ' . $text . '</a>';
            if ($v == 'import') {
                $template = str_replace('/', '_', $controller);
                $download = '';
                if (file_exists("./template/{$template}.xlsx")) {
                    $download .= "<li><a href=\"/template/{$template}.xlsx\" target=\"_blank\">XLSX模版</a></li>";
                }
                if (file_exists("./template/{$template}.xls")) {
                    $download .= "<li><a href=\"/template/{$template}.xls\" target=\"_blank\">XLS模版</a></li>";
                }
                if (file_exists("./template/{$template}.csv")) {
                    $download .= empty($download) ? '' : "<li class=\"divider\"></li>";
                    $download .= "<li><a href=\"/template/{$template}.csv\" target=\"_blank\">CSV模版</a></li>";
                }
                $download .= empty($download) ? '' : "\n                            ";
                if (!empty($download)) {
                    $html[] = <<<EOT
                        <div class="btn-group">
                            <button type="button" href="{$href}" class="btn btn-info btn-import" title="{$title}" id="btn-import-file" data-url="ajax/upload" data-mimetype="csv,xls,xlsx" data-multiple="false"><i class="{$icon}"></i> {$text}</button>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" title="下载批量导入模版">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">{$download}</ul>
                        </div>
EOT;
                } else {
                    $html[] = '<a href="' . $href . '" class="' . $class . '" title="' . $title . '" id="btn-import-file" data-url="ajax/upload" data-mimetype="csv,xls,xlsx" data-multiple="false"><i class="' . $icon . '"></i> ' . $text . '</a>';
                }
            } else {
                $html[] = '<a href="' . $href . '" class="' . $class . '" title="' . $title . '"><i class="' . $icon . '"></i> ' . $text . '</a>';
            }
        }
        return implode(' ', $html);
    }
}

if (!function_exists('build_heading')) {

    /**
     * 生成页面Heading
     *
     * @param string $path 指定的path
     * @return string
     */
    function build_heading($path = null, $container = true)
    {
        $title = $content = '';
        if (is_null($path)) {
            $action = request()->action();
            $controller = str_replace('.', '/', request()->controller());
            $path = strtolower($controller . ($action && $action != 'index' ? '/' . $action : ''));
        }
        // 根据当前的URI自动匹配父节点的标题和备注
        $data = Db::name('auth_rule')->where('name', $path)->field('title,remark')->find();
        if ($data) {
            $title = __($data['title']);
            $content = __($data['remark']);
        }
        if (!$content) {
            return '';
        }
        $result = '<div class="panel-lead"><em>' . $title . '</em>' . $content . '</div>';
        if ($container) {
            $result = '<div class="panel-heading">' . $result . '</div>';
        }
        return $result;
    }
}

if (!function_exists('build_suffix_image')) {
    /**
     * 生成文件后缀图片
     * @param string $suffix 后缀
     * @param null   $background
     * @return string
     */
    function build_suffix_image($suffix, $background = null)
    {
        $suffix = mb_substr(strtoupper($suffix), 0, 4);
        $total = unpack('L', hash('adler32', $suffix, true))[1];
        $hue = $total % 360;
        list($r, $g, $b) = hsv2rgb($hue / 360, 0.3, 0.9);

        $background = $background ? $background : "rgb({$r},{$g},{$b})";

        $icon = <<<EOT
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
            <path style="fill:#E2E5E7;" d="M128,0c-17.6,0-32,14.4-32,32v448c0,17.6,14.4,32,32,32h320c17.6,0,32-14.4,32-32V128L352,0H128z"/>
            <path style="fill:#B0B7BD;" d="M384,128h96L352,0v96C352,113.6,366.4,128,384,128z"/>
            <polygon style="fill:#CAD1D8;" points="480,224 384,128 480,128 "/>
            <path style="fill:{$background};" d="M416,416c0,8.8-7.2,16-16,16H48c-8.8,0-16-7.2-16-16V256c0-8.8,7.2-16,16-16h352c8.8,0,16,7.2,16,16 V416z"/>
            <path style="fill:#CAD1D8;" d="M400,432H96v16h304c8.8,0,16-7.2,16-16v-16C416,424.8,408.8,432,400,432z"/>
            <g><text><tspan x="220" y="380" font-size="124" font-family="Verdana, Helvetica, Arial, sans-serif" fill="white" text-anchor="middle">{$suffix}</tspan></text></g>
        </svg>
EOT;
        return $icon;
    }
}

if(!function_exists('pri')){
    function pri() {
        $args=func_get_args();
        foreach ($args as $v){
            if($v){
                echo "<pre>";
                print_r($v);
                echo "</pre>";
            }
        }
        if($args[count($args)-1]) exit();
    }
}

if(!function_exists('compareArrays')){
    function compareArrays($a, $b) {
        return $a <=> $b;
    }
}

if(!function_exists('getWeek')){
    function getWeek($num) {
        $week = '';
        switch ($num)
        {
            case 1: $week = '一';break;
            case 2: $week = '二';break;
            case 3: $week = '三';break;
            case 4: $week = '四';break;
            case 5: $week = '五';break;
            case 6: $week = '六';break;
            case 7: $week = '日';break;
            default: break;
        }
        return $week;
    }
}

if(!function_exists('shAndWbData')){
    //判断规格计算切割损耗 
    function shAndWbData($x,$y,$lname)
    {
        $nestingLossModel = new \app\admin\model\chain\material\NestingLoss();
        $nestingLossList = $nestingLossModel->getLossData();
        $sh = $wb = 0;
        foreach($nestingLossList as $k=>$v){
            if(($lname==$v["l_name"] or $v["l_name"]=='') and ($x>=$v["spec_down"] and $x<=$v["spec_up"]) and ($y>=$v["thickness_down"] and $y<=$v["thickness_up"])){
                $sh = $v["loss"];
                $wb = $v["tail"];
            }
        }
        return [$sh,$wb];
    }
}

    /**
	 * 二维数组某一项排序
	 * @param  $array
	 * @param  $keys
	 * @param  $sort
	 * @return 
	 */
if(!function_exists('arraySort')){
	function arraySort($array, $keys, $sort = SORT_DESC) {
	    $keysValue = [];
	    foreach ($array as $k => $v) {
	        $keysValue[$k] = $v[$keys];
	    }
	    array_multisort($keysValue, $sort, $array);
	    return $array;
	}
}

if(!function_exists('checkZeroes')){
    function checkZeroes($input)
    {
        return !preg_match('/^[0.,]*$/', $input);
    }
}

if(!function_exists('splitArray')){
    function splitArray($arr,$n){
        if(empty($arr)) return [];
        for ($i = 0; $i <= $n; $i++) {
            $res[] = array_slice($arr, $i * $n, $n);
        }
        return $res;
    }
}

if(!function_exists('diffDay')){
    function diffDay($diff=0) {
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24)  / (30*60*60*24));
        $days = floor(($diff - $years * 365*60*60*24 -  $months*30*60*60*24)/ (60*60*24)); 
        return $years.'年'.$months.'月'.$days.'日';
    }
}

if(!function_exists('objectToArray')){
    function objectToArray($object){
        if(empty($object)) return [];
        $arr = [];
        foreach($object as $v){
            $arr[] = $v->toArray();
        }
        return $arr;
    }
}

if(!function_exists('listToTable')){
    function listToTable($fieldList=[],$list = []){
        if(empty($fieldList)) return '';
        $field = '<thead><tr><th width="80">下达单</th>';
        $field .= '<th width="40">'.implode('</th><th width="40">',$fieldList).'</th></tr></thead><tbody>';

        foreach($list as $k=>$v){
            $field .= '<tr><td>'.$k.'</td>';
            foreach($fieldList as $vv){
                $field .= '<td>'.($v[$vv]??0).'</td>';
            }
            $field .= '</tr>';
        }
        $field .= '</tbody>';
        return $field;
    }
}

if (!function_exists('build_input')) {

    /**
     * 生成input
     * @param string $name
     * @param mixed  $type
     * @param mixed  $value
     * @param mixed  $attr
     * @return string
     */
    function build_input($name, $type, $value = '', $attr = [])
    {

        $field = '<input name="'.$name.'" type="'.$type.'" value="'.$value.'"';
        if(!empty($attr)){
            foreach($attr as $k=>$v){
                if($v=='') $field .= ' '.$k;
                else $field .= ' '.$k.'="'.$v.'"';
            }
        }
        $field .= '>';
        return $field;
    }
}

if (!function_exists('api_get')) {
    //发送 http post
    function api_get($url, $param)
    {
        // $param['token'] = gettoken();
        // $url=env('api.baseurl').$url;
        $res = Http::get($url, $param, [CURLOPT_HTTPHEADER => ['Content-Type: application/json']]);
        $resjson = json_decode($res, true);
        return $resjson;
    }
}

if (!function_exists('api_post')) {
    //发送 http post
    function api_post($url, $param)
    {
        $param = json_encode($param);
        $res = Http::post($url, $param, [CURLOPT_HTTPHEADER => ['Content-Type: application/json']]);
        $resjson = json_decode($res, true);
        if(!$resjson) return ["data" => [],"status" => "10000000","message"=>"空数据"];
        else return $resjson["resultValue"];
    }
}

if (!function_exists('api_post_u')) {
    //发送 http post
    function api_post_u($url, $param)
    {
        $param = json_encode($param);
        $token = "YWFhYmJiY2Nj";
        $baseUrl = "http://192.168.2.87:8081/api";
        $url = $baseUrl.$url."?token=".$token;
        $res = Http::post($url, $param, [CURLOPT_HTTPHEADER => ['Content-Type: application/json']]);
        $resjson = json_decode($res, true);
        return $resjson;
    }
}

/**
 * 成功响应
 * @param string $msg 提示信息
 * @param array $data 返回数据
 * @param int $code 状态码
 * @return string
 */
function response_success($msg = '', $data = [], $code = 1): void
{
    $temp = [
        'msg' => ($msg)? $msg: 'success',
        'code' => $code,
        'data' => $data
    ];
    echo json_encode($temp, JSON_UNESCAPED_UNICODE);die;
}

/**
 * 失败响应
 * @param string $msg 提示信息
 * @param array $data 返回数据
 * @param int $code 状态码
 * @return string
 */
function response_error($msg = '', $data = [], $code = 0): void
{
    $temp = [
        'msg' => ($msg)? $msg: 'error',
        'code' => $code,
        'data' => $data
    ];
    echo json_encode($temp, JSON_UNESCAPED_UNICODE);die;
}