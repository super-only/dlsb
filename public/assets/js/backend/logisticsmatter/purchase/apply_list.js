define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logisticsmatter/purchase/apply_list/index' + location.search,
                    auditor_url: 'logisticsmatter/purchase/apply_list/auditor',
                    giveup_url: 'logisticsmatter/purchase/apply_list/giveUp',
                    table: 'applylist_jwl',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'AD_ID',
                sortName: 'AD_ID',
                search: false,
                showToggle: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ad_cp_check', title: '是否采购', searchList: Config.status, formatter: Table.api.formatter.status},
                        {field: 'AL_Num', title: '请购编号', operate: 'LIKE'},
                        {field: 'AD_ID', title: '请购ID', operate: 'LIKE'},
                        {field: 'IR_Num', title: '存货编码', operate: 'LIKE'},
                        {field: 'IS_Name', title: '存货类别', operate: 'LIKE'},
                        {field: 'IR_Name', title: '存货名称', operate: 'LIKE'},
                        {field: 'IR_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'IR_Unit', title: '计量单位', operate:false},
                        {field: 'AD_Count', title: '请购数量', operate: false},
                        {field: 'AD_BuyCount', title: '已购数量', operate: false},
                        {field: 'sy_count', title: '剩余数量', operate: false},
                        // {field: 'AD_Memo', title: '备注'},
                        // {field: 'AL_Writer', title: '请购人', operate: 'LIKE'},
                        // {field: 'AL_WriterDate', title: '请购时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});