define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'api/purchase_api/index' + location.search,
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'poItemId',
                pageSize: 20,
                pageList: [10, 20, 25, 50, 100],
                singleSelect: true,
                sortName: 'sellerSignTime',
                sortOrder: 'DESC',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'poNo', title: '采购订单编码', operate: "LIKE"},
                        {field: 'poItemId', title: '采购订单行项目id', operate: "LIKE"},
                        {field: 'poItemNo', title: '采购订单行项目号', operate: "LIKE"},
                        {field: 'materialDesc', title: '采购方物料描述', operate: "LIKE"},
                        {field: 'conName', title: '合同名称', operate: "LIKE"},
                        {field: 'conCode', title: '合同编号', operate: "LIKE"},
                        {field: 'subclassCode', title: '种类编码', operate: "LIKE"},
                        {field: 'subclassName', title: '种类名称', operate: "LIKE"},
                        {field: 'amount', title: '采购数量', operate: "LIKE"},
                        {field: 'sellerSignTime', title: '合同签订日期', formatter: function (value) {
                            return value.substring(0,10);
                        },operate: false},
                        // {field: 'measUnit', title: '单位', operate:false},
                        {field: 'modifyTime', title: '更新时间', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var formData=table.bootstrapTable('getSelections');
                if(formData.length == 0) {
                    layer.msg("请选择！");
                }else Fast.api.close(formData[0]);
            });
        }
    };
    return Controller;
});