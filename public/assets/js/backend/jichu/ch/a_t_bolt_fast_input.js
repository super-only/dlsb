define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/a_t_bolt_fast_input/index' + location.search,
                    add_url: 'jichu/ch/a_t_bolt_fast_input/add',
                    edit_url: 'jichu/ch/a_t_bolt_fast_input/edit',
                    del_url: 'jichu/ch/a_t_bolt_fast_input/del',
                    multi_url: 'jichu/ch/a_t_bolt_fast_input/multi',
                    import_url: 'jichu/ch/a_t_bolt_fast_input/import',
                    table: 'A_tBoltFastInput',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'BF_ID',
                sortName: 'BF_ID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'BF_ID', title: __('Bf_id')},
                        {field: 'BD_sFlag', title: __('Bd_sflag'), formatter: Table.api.formatter.flag},
                        {field: 'BD_MaterialName', title: __('Bd_materialname'), operate: 'LIKE'},
                        {field: 'BD_Limber', title: __('Bd_limber'), operate: 'LIKE'},
                        {field: 'BD_Type', title: __('Bd_type'), operate: 'LIKE'},
                        {field: 'BD_Lenth', title: __('Bd_lenth'), operate:'BETWEEN'},
                        {field: 'BD_Other', title: __('Bd_other'), operate: 'LIKE'},
                        {field: 'FastInPut', title: __('Fastinput'), operate: 'LIKE'},
                        {field: 'BD_PerWeight', title: __('Bd_perweight'), operate:'BETWEEN'},
                        {field: 'BD_Default', title: __('Bd_default')},
                        {field: 'BD_Sort', title: __('Bd_sort')},
                        {field: 'BF_Plan', title: __('Bf_plan'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});