define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/limber/index' + location.search,
                    add_url: 'jichu/ch/limber/add',
                    edit_url: 'jichu/ch/limber/edit',
                    del_url: 'jichu/ch/limber/del',
                    multi_url: 'jichu/ch/limber/multi',
                    import_url: 'jichu/ch/limber/import',
                    table: 'Limber',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'L_ID',
                sortName: 'L_ID',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'L_ID', title: __('L_id')},
                        {field: 'L_Name', title: __('L_name'), operate: 'LIKE'},
                        {field: 'L_Type', title: __('L_type'), operate: 'LIKE'},
                        {field: 'L_SuperNum', title: __('L_supernum'), operate: 'LIKE'},
                        {field: 'L_Valid', title: __('L_valid'),operate: false,table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});