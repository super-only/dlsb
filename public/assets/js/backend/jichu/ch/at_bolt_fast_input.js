define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/at_bolt_fast_input/index' + location.search,
                    add_url: 'jichu/ch/at_bolt_fast_input/add',
                    edit_url: 'jichu/ch/at_bolt_fast_input/edit',
                    del_url: 'jichu/ch/at_bolt_fast_input/del',
                    multi_url: 'jichu/ch/at_bolt_fast_input/multi',
                    import_url: 'jichu/ch/at_bolt_fast_input/import',
                    table: 'a_tboltfastinput',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                height: document.body.clientHeight-100,
                pk: 'BF_ID',
                sortName: 'BF_ID',
                sortOrder: 'DESC',
                search: false,
                onlyInfoPagination:true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ID', title: '序号', formatter: function(value, row, index){return ++index;}},
                        {field: 'BF_ID', title: __('Bf_id'), visible: false, operate: false},
                        {field: 'BD_sFlag', title: __('Bd_sflag'), visible: false, operate: false},
                        {field: 'BD_MaterialName', title: __('Bd_materialname'), operate: 'LIKE'},
                        {field: 'BD_Limber', title: __('Bd_limber'), operate: 'LIKE'},
                        {field: 'BD_Type', title: __('Bd_type'), operate: 'LIKE'},
                        {field: 'BD_Lenth', title: __('Bd_lenth'), operate:'BETWEEN'},
                        {field: 'BD_Other', title: __('Bd_other'), operate: 'LIKE'},
                        {field: 'FastInPut', title: __('Fastinput'), operate: 'LIKE'},
                        {field: 'BD_PerWeight', title: __('Bd_perweight'), operate:'BETWEEN'},
                        {field: 'BD_Default', title: __('Bd_default'), operate: false, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'BD_Sort', title: __('Bd_sort')},
                        {field: 'BF_Plan', title: __('Bf_plan'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on("click", "#supplier", function () {
                var url = "jichu/ch/vendor_j_g_msg/chooseSupplier";
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["90%","100%"],
                    callback:function(value){
                        $("#c-V_Num").val(value.V_Num);
                        $("#c-V_Name").val(value.V_Name);
                    }
                };
                Fast.api.open(url,"供应商选择",options);
            });

            $(document).on("click", "#choose", function () {
                var msg = "是否删除原有数据？";
                var VNum = $("#c-V_Num").val();
                if(VNum==''){
                    alert("请先选择供应商!");
                    return false;
                } 
                var index = layer.confirm(msg, {
                    btn: ['是', '否'],
                }, function(data) {
                    $.ajax({
                        url: 'jichu/ch/at_bolt_fast_input/dealLs',
                        type: 'post',
                        dataType: 'json',
                        data: {num: VNum},
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                layer.msg(ret.msg);
                                if (ret.code === 1) {
                                    window.location.reload();
                                }
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            });

        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});