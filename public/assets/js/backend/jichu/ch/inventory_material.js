const { options } = require("toastr");
define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    var Controller = {
        index: function () {
            //修改顶部弹窗大小
            $(".btn-add").data("area",["80%","80%"]);
            $(".btn-edit").data("area",["80%","80%"]);

            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/ch/inventory_material/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/inventory_material/index' + location.search,
                    add_url: 'jichu/ch/inventory_material/add',
                    edit_url: 'jichu/ch/inventory_material/edit',
                    del_url: 'jichu/ch/inventory_material/del',
                    multi_url: 'jichu/ch/inventory_material/multi',
                    import_url: 'jichu/ch/inventory_material/import',
                    table: 'InventoryMaterial',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                height: document.body.clientHeight-50,
                pk: 'ID',
                // sortName: 'IM_Num',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'IM_Num', title: __('Im_num'), operate: 'LIKE'},
                        {field: 'KJM', title: __('Kjm'), operate: 'LIKE'},
                        {field: 'type', title: __('type'),operate: false},
                        {field: 'IM_Class', title: __('Im_class'), operate: 'LIKE'},
                        {field: 'IM_Spec', title: __('Im_spec'), operate: 'LIKE'},
                        {field: 'IM_PerWeight', title: __('Im_perweight'), operate:'BETWEEN'},
                        {field: 'IM_Sign', title: __('Im_sign'), operate: 'LIKE'},
                        {field: 'IM_Measurement', title: __('Im_measurement'), operate: 'LIKE'},
                        {field: 'IM_Min', title: __('Im_min')},
                        {field: 'IM_Max', title: __('Im_max')},
                        {field: 'Valid', title: __('Valid'), table: table, formatter: Table.api.formatter.toggle},
                        {field: 'price', title: __('Price')},
                        {field: 'IM_BD_Other', title: __('Im_bd_other'), operate: 'LIKE'},
                        {field: 'IM_Length', title: __('Im_length'), operate: 'LIKE'},
                        {field: 'IM_Rax', title: __('Im_rax'), operate:'BETWEEN'},
                        {field: 'IM_Day', title: __('Im_day'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table',function(){
                $(".btn-editone").data("area",["80%","80%"]);
            });
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.queryParams = function(op){
                    op.tree = dd_num;
                    return op;
                }
                table.bootstrapTable('refresh',{})
                Table.api.init({
                    extend: {
                        index_url: 'jichu/ch/inventory_material/index' + location.search,
                        add_url: 'jichu/ch/inventory_material/add?tree='+dd_num,
                        edit_url: 'jichu/ch/inventory_material/edit',
                        del_url: 'jichu/ch/inventory_material/del',
                        multi_url: 'jichu/ch/inventory_material/multi',
                        import_url: 'jichu/ch/inventory_material/import',
                        table: 'InventoryMaterial',
                    }
                });
                
            }).jstree();
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        selecttree: function () {
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/ch/inventory_material/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                Fast.api.close(dd_num);
                
            }).jstree();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on("click", "#selectParent", function () {
                    var url = "jichu/ch/inventory_material/selectTree";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["20%","80%"],
                        callback:function(value){
                            $.ajax({
                                url: "jichu/ch/inventory_material/stockTypeCode",
                                type: 'post',
                                data: {value: value},
                                dataType: 'json',
                                success: function (ret) {
                                    if (ret.hasOwnProperty("code")) {
                                        var data = ret.hasOwnProperty("data") && ret.data != "" ? ret.data : "";
                                        if (ret.code === 1) {
                                            $("input[name='row[IM_Num]']").val(data["IM_Num"]);
                                            $("#c-type").val(data["type"]);
                                            $("input[name='row[ParentNum]']").val(data["ParentNum"]);
                                            $("input[name='row[IM_Class]']").val(data["IM_Class"]);
                                        }
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            // CallBackFun(value);//在回调函数里可以调用你的业务代码实现前端的各种逻辑和效果
                        }
                    };
                    Fast.api.open(url,"产品分类选择",options);
                });
            }
        }
    };
    return Controller;
});