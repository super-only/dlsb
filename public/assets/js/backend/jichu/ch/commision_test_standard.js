define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/commision_test_standard/index' + location.search,
                    add_url: 'jichu/ch/commision_test_standard/add',
                    edit_url: 'jichu/ch/commision_test_standard/edit',
                    del_url: 'jichu/ch/commision_test_standard/del',
                    multi_url: 'jichu/ch/commision_test_standard/multi',
                    import_url: 'jichu/ch/commision_test_standard/import',
                    table: 'CommisionTestStandard',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'id', title: __('Id')},
                        {field: 'CT_Standard', title: __('Ct_standard'), operate: 'LIKE'},
                        {field: 'valid', title: __('Valid'), table: table, formatter: Table.api.formatter.toggle},
                        {field: 'ct_default', title: __('Ct_default'), table: table, formatter: Table.api.formatter.toggle},
                        {field: 'ifCommision', title: '是否试验标准', table: table, formatter: Table.api.formatter.toggle},
                        {field: 'ifWm', title: '是否外貌标准', table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});