define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/measurement/index' + location.search,
                    add_url: 'jichu/ch/measurement/add',
                    edit_url: 'jichu/ch/measurement/edit',
                    del_url: 'jichu/ch/measurement/del',
                    multi_url: 'jichu/ch/measurement/multi',
                    import_url: 'jichu/ch/measurement/import',
                    table: 'Measurement',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'M_ID',
                sortName: 'M_ID',
                sortOrder: 'M_ID',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'M_ID', title: __('M_id')},
                        {field: 'M_Name', title: __('M_name'), operate: 'LIKE'},
                        {field: 'Valid', title: __('Valid'),operate: false, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});