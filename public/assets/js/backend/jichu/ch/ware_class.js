const { options } = require("toastr");

define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    var Controller = {
        index: function () {

            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/ch/ware_class/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/ware_class/index' + location.search,
                    add_url: 'jichu/ch/ware_class/add',
                    edit_url: 'jichu/ch/ware_class/edit',
                    del_url: 'jichu/ch/ware_class/del',
                    multi_url: 'jichu/ch/ware_class/multi',
                    import_url: 'jichu/ch/ware_class/import',
                    table: 'WareClass',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'WC_Num',
                sortName: 'WC_Num',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'WC_Num', title: __('Wc_num'), operate: 'LIKE'},
                        {field: 'WC_Name', title: __('Wc_name'), operate: 'LIKE'},
                        {field: 'WC_Tel', title: __('Wc_tel'), operate: 'LIKE'},
                        {field: 'WC_DD_ResPerson', title: __('Wc_dd_resperson'), operate: 'LIKE'},
                        {field: 'WC_Address', title: __('Wc_address'), operate: 'LIKE'},
                        {field: 'ParentNum', title: __('Parentnum'),operate: false},
                        {field: 'Valid', title: __('Valid'),operate: false,table: table, formatter: Table.api.formatter.toggle},
                        {field: 'WC_Sort', title: __('Wc_sort'),operate: false},
                        {field: 'wc_storeprefix', title: __('Wc_storeprefix'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.queryParams = function(op){
                    op.tree = dd_num;
                    return op;
                }
                table.bootstrapTable('refresh',{})
                Table.api.init({
                    extend: {
                        add_url: 'jichu/ch/ware_class/add?tree='+dd_num,
                    }
                });
                
            }).jstree();
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on("click", "#is_type", function () {
                    $.ajax({
                        url: "jichu/ch/ware_class/stockTypeCode",
                        type: 'post',
                        dataType: 'json',
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var data = ret.hasOwnProperty("data") && ret.data != "" ? ret.data : "";
                                if (ret.code === 1) {
                                    $("input[name='row[DD_Num]']").val(ret.data["DD_Num"]);
                                    $("input[name='row[ParentNum]']").val("");
                                    $("#c-ParentNum_Text").val("");
                                }
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });
            }
        }
    };
    return Controller;
});