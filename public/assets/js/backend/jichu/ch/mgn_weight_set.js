define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            Controller.api.bindevent();
            $(document).on("click", "#c-way", function () {
                if($(this).is(":checked")){
                    $("#c-sc_percent").attr("readonly",false)//将input元素设置为readonly
                    $("#c-mgn_weight").attr("readonly",true)//去除input元素的readonly属性
                }else{
                    $("#c-sc_percent").attr("readonly",true)//将input元素设置为readonly
                    $("#c-mgn_weight").attr("readonly",false)//去除input元素的readonly属性
                }
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});