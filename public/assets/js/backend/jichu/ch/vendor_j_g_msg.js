const { options } = require("toastr");
define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    imList = typeof imList=="undefined"?[]:imList;
    var treeNum = '';
    var Controller = {
        index: function () {
            //修改顶部弹窗大小
            $(".btn-add").data("area",["90%","100%"]);
            $(".btn-edit").data("area",["90%","100%"]);
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/ch/vendor_j_g_msg/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/vendor_j_g_msg/index' + location.search,
                    add_url: 'jichu/ch/vendor_j_g_msg/add',
                    multi_url: 'jichu/ch/vendor_j_g_msg/multi',
                    import_url: 'jichu/ch/vendor_j_g_msg/import',
                    table: 'VendorJGMsg',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'VJG_ID',
                sortName: 'IM_Num',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'IM_Num', title: __('Im_num'), operate: 'LIKE'},
                        {field: 'IM_Class', title: __('Im_class'), operate: false},
                        {field: 'IM_Spec', title: __('Im_spec'), operate: false},
                        {field: 'VJG_Length', title: __('Vjg_length'), operate:'BETWEEN'},
                        {field: 'VJG_PerWeight', title: __('Vjg_perweight'), operate:'BETWEEN'},
                        {field: 'VJG_BlackPerWeight', title: __('Vjg_blackperweight'), operate:'BETWEEN'},
                        
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                treeNum = dd_num;
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'V_Num': dd_num}),
                        op: JSON.stringify({'V_Num': '='}),
                        tree:dd_num
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
                
            }).jstree();
            $(document).on("click", ".btn-edit", function () {
                var url = "jichu/ch/vendor_j_g_msg/edit/ids/"+treeNum;
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["90%","100%"],
                    callback:function(value){
                        window.location.reload();
                    }
                };
                Fast.api.open(url,"编辑",options);
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        choosesupplier: function () {
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/ch/vendor_j_g_msg/vendorTree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/vendor_j_g_msg/chooseSupplier' + location.search,
                    table: 'vendor',
                }
            });

            var table = $("#table");

            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'V_Num',
                sortName: 'V_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'V_Num', title: __('V_num'), operate: 'LIKE'},
                        {field: 'V_Name', title: __('V_name'), operate: 'LIKE'},
                        {field: 'V_ShortName', title: __('V_shortname'), operate: 'LIKE'},
                        {field: 'VC_Num', title: __('Vc_num'), operate: false},
                        {field: 'AD_Name', title: __('Ad_name'), operate: false},
                        {field: 'V_C_Contact1', title: __('V_c_contact1'), operate: 'LIKE'},
                        {field: 'V_C_Tel', title: __('V_c_tel'), operate: 'LIKE'},
                        {field: 'V_C_Fax', title: __('V_c_fax'), operate: 'LIKE'},
                        {field: 'V_C_SalesMan', title: __('V_c_salesman'), operate: 'LIKE'},
                        {field: 'V_C_Dept', title: __('V_c_dept'), operate: false},
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'VC_Num': dd_num}),
                        op: JSON.stringify({'VC_Num': '='}),
                        tree:dd_num
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
                
            }).jstree();
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                Fast.api.close(tableContent[0]);
                
            });
        },
        chooseim: function () {
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/ch/inventory_material/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/ch/vendor_j_g_msg/chooseIm' + location.search,
                    table: 'InventoryMaterial',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'ID',
                sortName: 'IM_Num',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'IM_Num', title: __('Im_num'), operate: 'LIKE'},
                        {field: 'KJM', title: __('Kjm'), operate: 'LIKE'},
                        {field: 'type', title: __('type'),operate: false},
                        {field: 'IM_Class', title: __('Im_class'), operate: 'LIKE'},
                        {field: 'IM_Spec', title: __('Im_spec'), operate: 'LIKE'},
                        {field: 'IM_PerWeight', title: __('Im_perweight'), operate:'BETWEEN'},
                        {field: 'IM_Sign', title: __('Im_sign'), operate: 'LIKE'},
                        {field: 'IM_Measurement', title: __('Im_measurement'), operate: 'LIKE'},
                        {field: 'IM_Min', title: __('Im_min')},
                        {field: 'IM_Max', title: __('Im_max')},
                        {field: 'Valid', title: __('Valid'), table: table, formatter: Table.api.formatter.toggle},
                        {field: 'price', title: __('Price')},
                        {field: 'IM_BD_Other', title: __('Im_bd_other'), operate: 'LIKE'},
                        {field: 'IM_Length', title: __('Im_length'), operate: 'LIKE'},
                        {field: 'IM_Rax', title: __('Im_rax'), operate:'BETWEEN'},
                        {field: 'IM_Day', title: __('Im_day'),operate: false},
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'IM_Num': dd_num}),
                        op: JSON.stringify({'IM_Num': 'LIKE %'}),
                        tree:dd_num
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
                
            }).jstree();
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                Fast.api.close(tableContent);
                
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on("click", "#supplier", function () {
                    var url = "jichu/ch/vendor_j_g_msg/chooseSupplier";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["90%","100%"],
                        callback:function(value){
                            $.ajax({
                                url: 'jichu/ch/vendor_j_g_msg/isExistVendor',
                                type: 'post',
                                dataType: 'json',
                                data: {num: value.V_Num},
                                success: function (ret) {
                                    console.log(value);
                                    if(ret.code==1){
                                        $("#c-V_Num").val(value.V_Num);
                                        $("#c-V_Name").val(value.V_Name);
                                    }else{
                                        layer.msg(ret.msg);
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }
                    };
                    Fast.api.open(url,"供应商选择",options);
                });
                $(document).on("click", "#choose", function () {
                    var url = "jichu/ch/vendor_j_g_msg/chooseIM";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["90%","100%"],
                        callback:function(value){
                            var addHtml = '';
                            for(key in value){
                                if($.inArray(value[key]["IM_Num"],imList)==-1){
                                    addHtml +='<tr><td style="width:90px"><input type="button" class="table_del" value="删除" readonly></td>';
                                    addHtml += '<td>'+value[key]["IM_Num"]+'<input type="hidden" name="table_row[IM_Num][]" value="'+value[key]["IM_Num"]+'"></td>';
                                    addHtml += '<td>'+value[key]["IM_Class"]+'</td>';
                                    addHtml += '<td>'+value[key]["IM_Spec"]+'</td>';
                                    addHtml += '<td><input type="number" name="table_row[VJG_Length][]" value="0"></td>';
                                    addHtml += '<td><input type="number" name="table_row[VJG_PerWeight][]" value="0"></td>';
                                    addHtml += '<td><input type="number" name="table_row[VJG_BlackPerWeight][]" value="0"></td></tr>';
                                    imList.push(value[key]["IM_Num"]);
                                }
                            }
                            $("#tbdeal").append(addHtml);
                        }
                    };
                    Fast.api.open(url,"紧固件选择",options);
                });
                $(document).on('click', ".table_del", function(e){
                    var index = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td').eq(1).find('input').val().trim();
                    $( e.target ).closest("tr").remove();
                    imList = $.grep(imList, function( n ) {
                        return ( n != num );
                    });
                });
                
            }
        }
    };
    return Controller;
});