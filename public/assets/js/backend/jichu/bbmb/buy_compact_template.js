define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/bbmb/buy_compact_template/index' + location.search,
                    add_url: 'jichu/bbmb/buy_compact_template/add',
                    edit_url: 'jichu/bbmb/buy_compact_template/edit',
                    del_url: 'jichu/bbmb/buy_compact_template/del',
                    multi_url: 'jichu/bbmb/buy_compact_template/multi',
                    import_url: 'jichu/bbmb/buy_compact_template/import',
                    table: 'BuyCompactTemplate',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'BCT_Num',
                sortName: 'BCT_Num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'BCT_Num', title: __('Bct_num'), operate: 'LIKE'},
                        {field: 'BCT_1', title: __('Bct_1'), operate: 'LIKE'},
                        {field: 'BCT_2', title: __('Bct_2'), operate: 'LIKE'},
                        {field: 'BCT_3', title: __('Bct_3'), operate: 'LIKE'},
                        {field: 'BCT_4', title: __('Bct_4'), operate: 'LIKE'},
                        {field: 'BCT_5', title: __('Bct_5'), operate: 'LIKE'},
                        {field: 'BCT_6', title: __('Bct_6'), operate: 'LIKE'},
                        {field: 'BCT_7', title: __('Bct_7'), operate: 'LIKE'},
                        {field: 'BCT_8', title: __('Bct_8'), operate: 'LIKE'},
                        {field: 'BCT_9', title: __('Bct_9'), operate: 'LIKE'},
                        {field: 'BCT_10', title: __('Bct_10'), operate: 'LIKE'},
                        {field: 'BCT_11', title: __('Bct_11'), operate: 'LIKE'},
                        {field: 'BCT_12', title: __('Bct_12'), operate: 'LIKE'},
                        {field: 'BCT_13', title: __('Bct_13'), operate: 'LIKE'},
                        {field: 'BCT_14', title: __('Bct_14'), operate: 'LIKE'},
                        {field: 'BCT_15', title: __('Bct_15'), operate: 'LIKE'},
                        {field: 'BCT_16', title: __('Bct_16'), operate: 'LIKE'},
                        {field: 'BCT_17', title: __('Bct_17'), operate: 'LIKE'},
                        {field: 'BCT_18', title: __('Bct_18'), operate: 'LIKE'},
                        {field: 'BCT_19', title: __('Bct_19'), operate: 'LIKE'},
                        {field: 'BCT_20', title: __('Bct_20'), operate: 'LIKE'},
                        {field: 'Writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'WriteTime', title: __('Writetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Modifyer', title: __('Modifyer'), operate: 'LIKE'},
                        {field: 'ModifyTime', title: __('Modifytime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'AuditTime', title: __('Audittime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Approve', title: __('Approve'), operate: 'LIKE'},
                        {field: 'ApproveTime', title: __('Approvetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'V_Num', title: __('V_num'), operate: 'LIKE'},
                        {field: 'bct_type', title: __('Bct_type'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});