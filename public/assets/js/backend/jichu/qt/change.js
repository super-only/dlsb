define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jquerybase64'], function ($, undefined, Backend, Table, Form) {
    // var ids = tdId;
    var Controller = {
        index: function () {
            Form.api.bindevent($("form[role=form]"));
            $(document).on('click', "#fachoose-excel", function(e){
                var file = $("#c-avatar").val();
                if(file == false){
                    layer.confirm('请重试！');
                    return false;
                }
                file = $.base64.encode(file);
                window.open('/admin.php/jichu/qt/change/export/file/'+file);//+encodeURIComponent(file)
            });
        }
    };
    return Controller;
});