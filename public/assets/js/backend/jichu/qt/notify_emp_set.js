define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/qt/notify_emp_set/index' + location.search,
                    add_url: 'jichu/qt/notify_emp_set/add',
                    edit_url: 'jichu/qt/notify_emp_set/edit',
                    del_url: 'jichu/qt/notify_emp_set/del',
                    multi_url: 'jichu/qt/notify_emp_set/multi',
                    import_url: 'jichu/qt/notify_emp_set/import',
                    table: 'NotifyEmpSet',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'N_ID',
                sortName: 'N_ID',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'N_ID', title: __('N_id')},
                        {field: 'N_Type', title: __('N_type'), operate: 'LIKE'},
                        {field: 'E_Num', title: __('E_num'), operate: 'LIKE'},
                        {field: 'N_Writer', title: __('N_writer'), operate: 'LIKE'},
                        {field: 'N_WriteDate', title: __('N_writedate'), operate:'RANK', addclass:'datetimerange', autocomplete:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});