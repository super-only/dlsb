const { options } = require("toastr");

define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    var Controller = {
        index: function () {

            $('#treeview').jstree({
                'core' : {
                    'data' : {
                        'url' : 'jichu/jg/deptdetail/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                },
                
            });

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/jg/deptdetail/index' + location.search,
                    add_url: 'jichu/jg/deptdetail/add',
                    edit_url: 'jichu/jg/deptdetail/edit',
                    del_url: 'jichu/jg/deptdetail/del',
                    multi_url: 'jichu/jg/deptdetail/multi',
                    import_url: 'jichu/jg/deptdetail/import',
                    table: 'deptdetail',
                }
            });

            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'DD_Num',
                sortName: 'DD_Num',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DD_Num', title: __('Dd_num'), operate: false},
                        {field: 'DD_Name', title: __('Dd_name'), operate: 'LIKE'},
                        {field: 'DD_ResPerson', title: __('Dd_resperson'), operate: 'LIKE'},
                        {field: 'DD_ResPrisonTel', title: __('Dd_resprisontel'), operate: false},
                        {field: 'ParentNum', title: __('Parentnum'), operate: false},
                        {field: 'DD_Tel', title: __('Dd_tel'), operate: false},
                        {field: 'DD_TelInner', title: __('Dd_telinner'), operate: false},
                        {field: 'DD_Address', title: __('Dd_address'), operate: 'LIKE'},
                        {field: 'DD_Email', title: __('Dd_email'), operate: false},
                        {field: 'DD_Memo', title: __('Dd_memo'), operate: 'LIKE'},
                        // {field: 'dd_sort', title: __('Dd_sort')},
                        {field: 'Valid', title: __('Valid'), operate: false, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'DD_Num': dd_num}),
                        op: JSON.stringify({'DD_Num': 'LIKE %'})
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
            }).jstree();
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on("change", "select[name='row[ParentNum]']", function () {
                    var num = $(this).find("option:selected").val();
                    // console.log(num);
                    $.ajax({
                        url: "jichu/jg/deptdetail/stockTypeCode",
                        data: {num: num},
                        type: 'post',
                        dataType: 'json',
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                if (ret.code === 1) {
                                    $("input[name='row[DD_Num]']").val(ret.data["DD_Num"]);
                                }
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });
            },
            
        }
    };
    return Controller;
});