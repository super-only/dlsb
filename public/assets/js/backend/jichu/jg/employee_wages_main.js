define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/jg/employee_wages_main/index' + location.search,
                    add_url: 'jichu/jg/employee_wages_main/add',
                    edit_url: 'jichu/jg/employee_wages_main/edit',
                    del_url: 'jichu/jg/employee_wages_main/del',
                    auditor_url: 'jichu/jg/employee_wages_main/auditor',
                    giveup_url: 'jichu/jg/employee_wages_main/giveUp',
                    multi_url: 'jichu/jg/employee_wages_main/multi',
                    import_url: 'jichu/jg/employee_wages_main/import',
                    table: 'employee_wages_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'cm_num', title: __('Cm_num'), operate: 'LIKE'},
                        {field: 'type', title: __('Type'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,
                        buttons: [
                            {
                                name: 'table_secd',
                                text: "表",
                                title: "表",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-table',
                                url: 'jichu/jg/employee_wages_main/tableSecd',
                                extend: 'data-area=["100%","100%"]'
                        }]}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                window.location.reload();
                return false;
            }, null, function () {
                let content = table.bootstrapTable('getData');
                if (content.length > 0) {
                    $("input[name='row[check]']").val(JSON.stringify(content));
                }
                return true;
            });
            var idList = Config.idList;
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                height: document.body.clientHeight-100,
                columns: [
                    [
                        {checkbox: true, field: "checkbox",rowspan:2},
                        {field: 'ids', title: '序号',rowspan:2,align:'center',formatter: function (value, row, index) {
                            return ++index;
                        }},
                        {field: 'name', title: '姓名',rowspan:2,align:'center'},
                        {field: 'cm_day', title: '出勤',rowspan:2,align:'center'},
                        {field: 'other_stickers', title: '其他补贴',rowspan:2,align:'center',visible: false},
                        {field: 'communication_charges', title: '通讯费',rowspan:2,align:'center',visible: false},
                        {field: 'base', title: '基本',align:'center'},  
                        {field: 'overtime', title: '加班',align:'center'},  
                        {field: 'bt', title: '补贴',colspan:6,align:'center'},
                        {field: 'kk', title: '扣款',colspan:4,align:'center'},
                        {field: 'sj_money', title: '实发工资',rowspan:2,align:'center'},
                        {field: 'debit_card', title: '借记卡用户',rowspan:2,align:'center'},
                    ],
                    // [
                    //     {field: 'cm_day', title: '出勤',rowspan:2,align:'center'}, 
                    //     {field: 'base', title: '基本',align:'center'}, 
                    //     {field: 'overtime', title: '加班',align:'center'}, 
                    //     {field: 'bt', title: '补贴',colspan:6,align:'center'}, 
                    //     {field: 'kk', title: '扣款',colspan:4,align:'center'}, 
                    // ],
                    [
                        {field: 'wages', title: '工资',align:'center'},
                        {field: 'jb_wages', title: '值班',align:'center'},
                        {field: 'post_subsidies', title: '岗位补贴',align:'center'},
                        {field: 'car_stickers', title: '车贴',align:'center'},
                        {field: 'nutrition_subsidies', title: '营养补贴',align:'center'},
                        {field: 'night_meal_allowance', title: '夜餐补贴',align:'center'},
                        {field: 'safety_award', title: '安全奖',align:'center'},
                        {field: 'other_base', title: '其它',align:'center'},
                        {field: 'meals', title: '餐费',align:'center'},
                        {field: 'other_deduction', title: '其它',align:'center'},
                        {field: 'social_insurance', title: '社保金',align:'center'}, 
                        {field: 'personal_income_tax', title: '个所税',align:'center'}
                    ]
                ],
                onClickCell: function(field, value, row, $element) {
                    if($.inArray(field,["checkbox","ids","name","cm_day"]) === -1){
                        $element.attr('contenteditable', true);
                        $element.attr('style', "display:block");
                        $element.blur(function() {
                            let index = $element.parent().data('index');
                            let tdValue = $element.html();
                            let yl_money = parseFloat(row.wages)+parseFloat(row.jb_wages)+parseFloat(row.post_subsidies)+parseFloat(row.car_stickers)+parseFloat(row.nutrition_subsidies)+parseFloat(row.night_meal_allowance)+parseFloat(row.safety_award)+parseFloat(row.other_stickers)-parseFloat(row.meals)-(row.other_deduction)-parseFloat(row.social_insurance)-parseFloat(row.personal_income_tax);
                            if(field!="debit_card"){
                                tdValue = (tdValue=="<br>" || !tdValue)?0:tdValue;
                                if($.inArray(field,["meals","other_deduction","social_insurance","personal_income_tax"]) === -1) yl_money = yl_money-parseFloat(row[field])+parseFloat(tdValue);
                                else yl_money = yl_money+parseFloat(row[field])-parseFloat(tdValue);
                            }else tdValue = (tdValue=="<br>" || !tdValue)?'':tdValue;
                            saveData(index, field, tdValue);
                            saveData(index, "sj_money", yl_money);
                        })
                    }
                }
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            function saveData(index, field, value) {
                table.bootstrapTable('updateCell', {
                    index: index,
                    field: field,
                    value: value
                });
                return false;
            }
            $(document).on("click", "#auditor", function () {
                if(Config.flag) return false;
                check('审核之后无法修改，确定审核？',"jichu/jg/employee_wages_main/auditor",Config.ids);
            });
            $(document).on("click", "#giveup", function () {
                if(!Config.flag) return false;
                check('确定弃审？',"jichu/jg/employee_wages_main/giveUp",Config.ids);
                
            });
            function check(msg,url,num){
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                Layer.msg(ret.msg);
                                if (ret.code === 1) {
                                    window.location.reload();
                                }
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            };
            $(document).on("click", "#del", function (e) {
                if(!Config.ids) return false;
                var content = table.bootstrapTable('getData');
                if(content.length == 0) return false;
                var position = table.bootstrapTable('getScrollPosition');
                var i=0;
                var newcontent = [];
                $.each(content,function(index,row){
                    if(row.checkbox == undefined){
                        i++;
                        row["ids"] = i;
                        newcontent.push(row);
                    }else if($.inArray(row.name,idList)!==-1){
                        idList.splice($.inArray(row.name,idList),1);
                    }
                })
                table.bootstrapTable('load',newcontent);
                table.bootstrapTable('scrollTo',position);
            });

            $(document).on("click", "#add", function (e) {
                if(!Config.ids) return false;
                var content = table.bootstrapTable('getData');
                if(content.length == 0) return false;
                var position = table.bootstrapTable('getScrollPosition');
                var i=0;
                var newcontent = [];
                $.each(content,function(index,row){
                    if(row.checkbox == undefined){
                        i++;
                        row["ids"] = i;
                        newcontent.push(row);
                    }else if($.inArray(row.name,idList)!==-1){
                        idList.splice($.inArray(row.name,idList),1);
                    }
                })
                table.bootstrapTable('load',newcontent);
                table.bootstrapTable('scrollTo',position);
            });

            // $(document).on('click', "#summary", function () {
            //     // var cmNum = $("#c-cm_num").val();
            //     Layer.confirm("汇总会覆盖未保存信息，确定已保存当前信息？", function () {
            //         Fast.api.ajax({
            //             url: 'jichu/jg/employee_wages_main/summary',
            //             type: "post",
            //             async: false,
            //             data: {ids: Config.ids},
            //         }, function (data, ret) {
            //             table.bootstrapTable('load',data);
            //         }, function () {
            //             Layer.closeAll();
            //         });
            //     });
            //     return false;
            // });
        },
        tablesecd: function () {
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                window.location.reload();
                return false;
            }, null, function () {
                let content = table.bootstrapTable('getData');
                if (content.length > 0) {
                    $("input[name='row[check]']").val(JSON.stringify(content));
                }
                return true;
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                height: document.body.clientHeight-100,
                columns: [
                    [
                        {checkbox: true, field: "checkbox",rowspan:2},
                        {field: 'ids', title: '序号',rowspan:2,align:'center',formatter: function (value, row, index) {
                            return ++index;
                        }},
                        {field: 'name', title: '姓名',rowspan:2,align:'center'},
                        {field: 'cm_day', title: '出勤',rowspan:2,align:'center'},
                        {field: 'base', title: '基本',align:'center'},  
                        {field: 'overtime', title: '加班',align:'center'},  
                        {field: 'other_base', title: '其它',rowspan:2,align:'center', visible:false},
                        {field: 'communication_charges', title: '通讯费',rowspan:2,align:'center'},
                        {field: 'bt', title: '补贴',colspan:6,align:'center'},
                        {field: 'kk', title: '扣款',colspan:4,align:'center'},
                        {field: 'sj_money', title: '实发工资',rowspan:2,align:'center'},
                        {field: 'debit_card', title: '借记卡用户',rowspan:2,align:'center'},
                    ],
                    // [
                    //     {field: 'cm_day', title: '出勤',rowspan:2,align:'center'}, 
                    //     {field: 'base', title: '基本',align:'center'}, 
                    //     {field: 'overtime', title: '加班',align:'center'}, 
                    //     {field: 'bt', title: '补贴',colspan:6,align:'center'}, 
                    //     {field: 'kk', title: '扣款',colspan:4,align:'center'}, 
                    // ],
                    [
                        {field: 'base_wages', title: '工资',align:'center'},
                        {field: 'zb_wages', title: '值班',align:'center'},
                        {field: 'post_subsidies', title: '岗位补贴',align:'center'},
                        {field: 'car_stickers', title: '车贴',align:'center'},
                        {field: 'nutrition_subsidies', title: '营养补贴',align:'center'},
                        {field: 'night_meal_allowance', title: '夜餐补贴',align:'center'},
                        {field: 'safety_award', title: '安全奖',align:'center'},
                        {field: 'other_stickers', title: '其它',align:'center'},
                        {field: 'meals', title: '餐费',align:'center'},
                        {field: 'other_deduction', title: '其它',align:'center'},
                        {field: 'social_insurance', title: '社保金',align:'center'}, 
                        {field: 'personal_income_tax', title: '个所税',align:'center'}
                    ]
                ],
                onClickCell: function(field, value, row, $element) {
                    if($.inArray(field,["base_wages","communication_charges","other_stickers"]) !== -1){
                        $element.attr('contenteditable', true);
                        $element.attr('style', "display:block");
                        $element.blur(function() {
                            let index = $element.parent().data('index');
                            let tdValue = $element.html();
                            tdValue = (tdValue=="<br>" || !tdValue)?0:tdValue;
                            let jb_money = parseFloat(row.sj_money)-parseFloat(row.post_subsidies)-parseFloat(row.car_stickers)-parseFloat(row.nutrition_subsidies)-parseFloat(row.night_meal_allowance)-parseFloat(row.safety_award)-parseFloat(row.other_base)+parseFloat(row.meals)+(row.other_deduction)+parseFloat(row.social_insurance)-parseFloat(row.personal_income_tax)-parseFloat(row.base_wages)-parseFloat(row.communication_charges)-parseFloat(row.other_stickers)+parseFloat(row[field])-parseFloat(tdValue);
                            saveData(index, field, tdValue);
                            saveData(index, "zb_wages", jb_money);
                        })
                    }
                }
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            function saveData(index, field, value) {
                table.bootstrapTable('updateCell', {
                    index: index,
                    field: field,
                    value: value
                });
                return false;
            }
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                
            }
        }
    };
    return Controller;
});