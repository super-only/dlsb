const { options } = require("toastr");

define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    var Controller = {
        index: function () {
            //修改顶部弹窗大小
            $(".btn-add").data("area",["90%","100%"]);
            $(".btn-edit").data("area",["90%","100%"]);
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/jg/deptdetail/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });


            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/jg/employee/index' + location.search,
                    add_url: 'jichu/jg/employee/add',
                    edit_url: 'jichu/jg/employee/edit',
                    del_url: 'jichu/jg/employee/del',
                    multi_url: 'jichu/jg/employee/multi',
                    // import_url: 'jichu/jg/employee/import',
                    table: 'employee',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'E_PerNum',
                sortName: 'E_Num',
                sortOrder: 'ASC',
                fixedColumns: true,
                fixedNumber: 4,
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'Valid', title: __('Valid'), 
                            searchList: {"0": "离职", "1": "在职", "2": "开除", "3": "辞职"},
                            formatter: function(value) {
                                var valid = ["离职","在职","开除","辞职"];
                                return valid[value];
                            }
                        },
                        {field: 'E_Num', title: __('E_num'), operate: 'LIKE'},
                        {field: 'E_Name', title: __('E_name'), operate: 'LIKE'},
                        {field: 'py', title: '姓名拼音首字母', operate: false},
                        {field: 'E_Sex', title: __('E_sex'), 
                            searchList: {"0": "女", "1": "男"},
                            formatter: function(value) {
                                var sex = ["女","男"];
                                return sex[value];
                            }
                        },
                        {field: 'E_IDCard', title: __('E_idcard'), operate: false},
                        {field: 'DD_Num', title: __('Dd_num'), searchList: Config.company, formatter: function(value){
                            return Config.company[value];
                        }},
                        {field: 'E_Postitle', title: __('E_postitle'), operate: false},
                        {field: 'e_EmpType', title: __('E_emptype'), operate: false},
                        // {field: 'year', title: '工龄', operate: false},
                        {field: 'E_EnterTime', title: __('E_entertime'), operate:'RANK', addclass:'datetimerange', autocomplete:false},
                        {field: 'e_workdate', title: __('E_workdate'), operate:false, addclass:'datetimerange', autocomplete:false},
                        {field: 'E_Certificate', title: __('E_certificate'), operate: false},
                        {field: 'E_Birthday', title: __('E_birthday'), operate:'RANK', addclass:'datetimerange', autocomplete:false},
                        {field: 'E_Mobile', title: __('E_mobile'), operate: false},
                        {field: 'E_Tel', title: __('E_tel'), operate: false},
                        {field: 'E_Address', title: __('E_address'), operate: false},
                        {field: 'E_Formal', title: __('E_formal'), operate:false, addclass:'datetimerange', autocomplete:false},
                        {field: 'E_Nation', title: __('E_nation'), operate: false},
                        {field: 'E_NativePlace', title: __('E_nativeplace'), operate: false},
                        {field: 'E_Education', title: __('E_education'), operate: false},
                        {field: 'e_Degree', title: __('E_degree'), operate: false},
                        {field: 'E_Specialized', title: __('E_specialized'), operate: false},
                        {field: 'E_College', title: __('E_college'), operate: false},
                        {field: 'e_PostCode', title: __('E_postcode'), operate: false},
                        {field: 'E_IfMarry', title: __('E_ifmarry'), operate: false},
                        {field: 'E_HKtype', title: __('E_hktype'), operate: false},
                        {field: 'E_WorkValue', title: __('E_workvalue'), operate: false},
                        {field: 'E_FileNum', title: __('E_filenum'), operate: false},
                        {field: 'e_BanZu', title: __('E_banzu'), operate: false},
                        {field: 'E_LeaveTime', title: __('E_leavetime'), operate:false, addclass:'datetimerange', autocomplete:false},
                        {field: 'E_Memo', title: __('E_memo'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });


            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table',function(){
                $(".btn-editone").data("area",["90%","100%"]);
            });
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'DD_Num': dd_num}),
                        op: JSON.stringify({'DD_Num': 'LIKE %'}),
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
                
            }).jstree();
            $(document).on('click', ".import", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["60%","40%"],
                };
				Fast.api.open('jichu/jg/employee/importType',"导入",options);
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        selectcrif: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/jg/employee/selectCrif/field/'+Config.field + location.search,
                    table: 'employee',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'E_Num',
                sortName: 'E_Num',
                sortOrder: 'ASC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'Valid', title: __('Valid'), 
                            searchList: {"0": "离职", "1": "在职", "2": "开除", "3": "辞职"},
                            formatter: function(value) {
                                var valid = ["离职","在职","开除","辞职"];
                                return valid[value];
                            }
                        },
                        {field: 'E_Num', title: __('E_num'), operate: 'LIKE'},
                        {field: 'E_Name', title: __('E_name'), operate: 'LIKE'},
                        {field: 'E_Sex', title: __('E_sex'), 
                            searchList: {"0": "女", "1": "男"},
                            formatter: function(value) {
                                var sex = ["女","男"];
                                return sex[value];
                            }
                        },
                        {field: 'E_hjCerif', title: '电焊证书', operate: false},
                        {field: 'E_jcCerif', title: '检测证书', operate: false},
                        {field: 'DD_Num', title: __('Dd_num'), operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var formData=table.bootstrapTable('getAllSelections');
                Fast.api.close(formData[0]);
                
            });
        },
        importtype: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                // $(document).on("change", "select[name='row[DD_Num]']", function () {
                //     var num = $(this).find("option:selected").val();
                //     $.ajax({
                //         url: "jichu/jg/employee/companyType",
                //         type: 'post',
                //         dataType: 'json',
                //         data: {num: num},
                //         success: function (ret) {
                //             if (ret.hasOwnProperty("code")) {
                //                 if (ret.code === 1) {
                //                     $("#c-E_Num").val(ret.data["E_Num"]);
                //                 }
                //             }
                //         }, error: function (e) {
                //             Backend.api.toastr.error(e.message);
                //         }
                //     });
                // });
            }
        }
    };
    return Controller;
});