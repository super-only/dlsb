define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    //读取选中的条目
    $.jstree.core.prototype.get_all_checked = function (full) {
        var obj = this.get_selected(), i, j;
        for (i = 0, j = obj.length; i < j; i++) {
            obj = obj.concat(this.get_node(obj[i]).parents);
        }
        obj = $.grep(obj, function (v, i, a) {
            return v != '#';
        });
        obj = obj.filter(function (itm, i, a) {
            return i == a.indexOf(itm);
        });
        return full ? $.map(obj, $.proxy(function (i) {
            return this.get_node(i);
        }, this)) : obj;
    };
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/jg/check/index' + location.search,
                    add_url: 'jichu/jg/check/add',
                    edit_url: 'jichu/jg/check/edit',
                    del_url: 'jichu/jg/check/del',
                    multi_url: 'jichu/jg/check/multi',
                    import_url: 'jichu/jg/check/import',
                    table: 'check',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'idfield',
                idField: 'idfield',
                uniqueId: 'idfield',
                sortName: 'cm_num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'idfield', title: '唯一标志', visible: false},
                        {field: 'cm_num', title: __('Cm_num')},
                        {field: 'type', title: __('Type')},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate: false},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Form.api.bindevent($("form[role=form]"), null, null, function () {
                if ($("#treeview").length > 0) {
                    var r = $("#treeview").jstree("get_all_checked");
                    $("input[name='row[rules]']").val(r.join(','));
                }
                return true;
            });
            $('#treeview').jstree({
                'core': {
                    'data': Config.treeList
                },
                "plugins" : [ "checkbox"]
            });
            $(document).on("click", "#checkall", function () {
                $("#treeview").jstree($(this).prop("checked") ? "check_all" : "uncheck_all");
            });
            $(document).on("click", "#expandall", function () {
                $("#treeview").jstree($(this).prop("checked") ? "open_all" : "close_all");
            });
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                window.location.reload();
                return false;
            }, null, function () {
                let content = table.bootstrapTable('getData');
                if (content.length > 0) {
                    $("input[name='row[check]']").val(JSON.stringify(content));
                }
                return true;
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.detail,
                height: document.body.clientHeight-100,
                columns: Config.columns,
                onClickCell: function(field, value, row, $element) {
                    if(field.substring(0,4)=="day-"){
                        $element.attr('contenteditable', true);
                        $element.attr('style', "display:block");
                        $element.blur(function() {
                            let index = $element.parent().data('index');
                            let tdValue = $element.html();
                            // var cq=0,bj=0;
                            // $.each(row,function(rindex,rrow){
                            //     if(rindex.substring(0,4)=="day-"){
                            //         if($.trim(rrow)=="/") cq++;
                            //         if($.trim(rrow)=="半") cq += 0.5;
                            //         if($.trim(rrow)=="⊕") bj++;
                            //     }
                            // })
                            saveData(index, field, tdValue);
                            // saveData(index, 'attendance', cq);
                            // saveData(index, 'sick', bj);
                            // saveData(index, 'total', bj+cq);
                        })
                    }
                }
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            function saveData(index, field, value) {
                table.bootstrapTable('updateCell', {
                    index: index,
                    field: field,
                    value: value
                });
                return false;
            }
            $(document).on("click", "#del", function (e) {
                if(Config.flag) return false;
                var content = table.bootstrapTable('getData');
                if(content.length == 0) return false;
                var position = table.bootstrapTable('getScrollPosition');
                var i=0;
                var newcontent = [];
                $.each(content,function(index,row){
                    if(row.checkbox == undefined){
                        i++;
                        row["id"] = i;
                        newcontent.push(row);
                    }
                })
                table.bootstrapTable('load',newcontent);
                table.bootstrapTable('scrollTo',position);
            });
            $(document).on("click", "#add", function (e) {
                if(Config.flag) return false;
                var content = table.bootstrapTable('getData');
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["70%","100%"],
                    callback:function(value){
                        let weekList = value.weekList;
                        let daysList = value.daysList;
                        let r = value.r;
                        $.each(content,function(ci,cr){
                            if($.inArray(cr.name,r)!=-1) r.splice($.inArray(cr.name,r),1);
                        })
                        let count = content.length;
                        $.each(r,function(ri,rr){
                            if($.isNumeric(rr)===false){
                                var oneData = {
                                    "id": count,
                                    "ids": 0,
                                    "name": rr
                                };
                                $.each(Config.daysWeekList,function(di,dr){
                                    if($.inArray(di,daysList)!=-1) oneData[("day-"+di)] = "√";
                                    else if($.inArray(dr,weekList)!=-1) oneData[("day-"+di)] = "√";
                                    else oneData[("day-"+di)] = "/";
                                });
                                content.push(oneData);
                                count++;
                            }
                        })
                        table.bootstrapTable('load',content);
                        table.bootstrapTable('scrollTo','bottom');
                    }
                };
                Fast.api.open('jichu/jg/check/addEditCheck/ids/'+Config.ids,"添加",options);
            });
            $(document).on("click", "#edit", function (e) {
                if(Config.flag) return false;
                var content = table.bootstrapTable('getData');
                if(content.length == 0) return false;
                var position = table.bootstrapTable('getScrollPosition');
                var openContent = `<form class="form-horizontal"><div class="form-group"><p>选择符号:</p>`+Config.fhList+`</div><div class="form-group"><p>选择日期:</p>`+Config.daysList+`</div></form>`;
                layer.open({
                    title: '修改',
                    area: ['50%', '60%'],
                    btnAlign: 'c',
                    closeBtn:'1',//右上角的关闭
                    content: openContent,
                    btn:['确认','取消'],
                    yes: function (index, layero) {
                        let fhList = $('select[name="fhList"]').val();
                        let gx_day = $('select[name="gx_day[]"]').val();
                        $.each(content,function(cindex,crow){
                            if(crow.checkbox==true){
                                $.each(gx_day,function(gindex,grow){
                                    if(crow["day-"+grow] != undefined) content[cindex]["day-"+grow] = fhList;
                                })
                            }
                        })
                        table.bootstrapTable('load',content);
                        layer.close(index);
                    },
                    no:function(index){
                        layer.close(index);
                        return false;//点击按钮按钮不想让弹层关闭就返回false
                    }
                    
                });
                table.bootstrapTable('scrollTo',position);
            });
            $(document).on("click", "#auditor", function () {
                if(Config.flag) return false;
                check('审核之后无法修改，确定审核？',"jichu/jg/check/auditor",Config.ids);
            });
            $(document).on("click", "#giveup", function () {
                if(!Config.flag) return false;
                check('确定弃审？',"jichu/jg/check/giveUp",Config.ids);
                
            });
            function check(msg,url,num){
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                Layer.msg(ret.msg);
                                if (ret.code === 1) {
                                    window.location.reload();
                                }
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
            $(document).on("click", "#createMoney", function (e) {
                if(!Config.flag) return false;
                Fast.api.ajax({
                    url: "jichu/jg/check/createMoney",
                    type: "post",
                    data: {ids: Config.ids},
                }, function (data) {
                    console.log(data);
                });
            });
            $(document).on("click", "#export", function () {
                window.open('/admin.php/jichu/jg/check/export/ids/'+Config.ids);
            });
            // parent.window.$(".layui-layer-iframe").find(".layui-layer-close").on('click',function () {    
            //     console.log(1);    
            //     parent.window.$("#table").bootstrapTable("refresh",{silent: true});
            // }); 
        },
        addeditcheck: function () {
            Controller.api.bindevent();
            $('#treeview').jstree({
                'core': {
                    'data': Config.treeList
                },
                "plugins" : [ "checkbox"]
            });
            $(document).on("click", "#checkall", function () {
                $("#treeview").jstree($(this).prop("checked") ? "check_all" : "uncheck_all");
            });
            $(document).on("click", "#expandall", function () {
                $("#treeview").jstree($(this).prop("checked") ? "open_all" : "close_all");
            });

            $(document).on("click", "#add", function () {
                var weekList = $("select[name='weekList[]']").val();
                var daysList = $("select[name='daysList[]']").val();
                var r = $("#treeview").jstree("get_all_checked");
                if(r.length == 0) return false;
                Fast.api.close({'weekList':weekList,'daysList':daysList,'r':r});
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});