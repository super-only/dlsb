define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/bank/index' + location.search,
                    add_url: 'jichu/yw/bank/add',
                    edit_url: 'jichu/yw/bank/edit',
                    del_url: 'jichu/yw/bank/del',
                    multi_url: 'jichu/yw/bank/multi',
                    import_url: 'jichu/yw/bank/import',
                    table: 'Bank',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'BN_ID',
                sortName: 'BN_ID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'BN_ID', title: __('Bn_id')},
                        {field: 'BN_Bank', title: __('Bn_bank'), operate: 'LIKE'},
                        {field: 'BN_Name', title: __('Bn_name'), operate: 'LIKE'},
                        {field: 'BN_Num', title: __('Bn_num'), operate: 'LIKE'},
                        {field: 'BN_Memo', title: __('Bn_memo'), operate: 'LIKE'},
                        {field: 'Valid', title: __('Valid')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});