define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    proList = typeof proList=="undefined"?[]:proList;
    stockList = typeof stockList=="undefined"?[]:stockList;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/receive_send_class/index' + location.search,
                    add_url: 'jichu/yw/receive_send_class/add',
                    edit_url: 'jichu/yw/receive_send_class/edit',
                    del_url: 'jichu/yw/receive_send_class/del',
                    multi_url: 'jichu/yw/receive_send_class/multi',
                    import_url: 'jichu/yw/receive_send_class/import',
                    table: 'ReceiveSendClass',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortOrder: 'ASC',
                pk: 'RSID',
                sortName: 'RSC_RSType',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'RSID', title:"ID", operate: false},
                        {field: 'RSC_Flag', title: __('Rsc_flag'), searchList: proList},
                        {field: 'RSC_Num', title: __('Rsc_num'), operate: 'LIKE'},
                        {field: 'RSC_Name', title: __('Rsc_name'), operate: 'LIKE'},
                        {field: 'RSC_RSType', title: __('Rsc_rstype'), searchList: stockList},
                        {field: 'Valid', title: __('Valid'), searchList: {"0": "无效", "1": "有效"}, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on("change", "input[name='row[RSC_Flag]']", function () {
                    getCode();
                });
                $(document).on("change", "input[name='row[RSC_RSType]']", function () {
                    getCode();
                });
                function getCode(){
                    var productType = $("input[name='row[RSC_Flag]']:checked").val();
                    var stockType = $("input[name='row[RSC_RSType]']:checked").val();
                    $.ajax({
                        url: "jichu/yw/receive_send_class/stockTypeCode",
                        type: 'post',
                        dataType: 'json',
                        data: {productType: productType, stockType:stockType},
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var data = ret.hasOwnProperty("data") && ret.data != "" ? ret.data : "";
                                if (ret.code === 1) {
                                    $("input[name='row[RSC_Num]']").val(ret.data["defaultCode"]);
                                }
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});