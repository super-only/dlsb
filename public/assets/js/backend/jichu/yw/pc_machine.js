define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $(".btn-add").data("area",["50%","50%"]);
            $(".btn-edit").data("area",["50%","50%"]);
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/pc_machine/index' + location.search,
                    add_url: 'jichu/yw/pc_machine/add',
                    edit_url: 'jichu/yw/pc_machine/edit',
                    del_url: 'jichu/yw/pc_machine/del',
                    multi_url: 'jichu/yw/pc_machine/multi',
                    import_url: 'jichu/yw/pc_machine/import',
                    table: 'pc_machine',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'valid', title: __('Valid'), searchList: {"0": "无效", "1": "有效"}, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table',function(){
                $(".btn-editone").data("area",["50%","50%"]);
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});