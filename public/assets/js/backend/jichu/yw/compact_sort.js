define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/compact_sort/index' + location.search,
                    add_url: 'jichu/yw/compact_sort/add',
                    edit_url: 'jichu/yw/compact_sort/edit',
                    del_url: 'jichu/yw/compact_sort/del',
                    multi_url: 'jichu/yw/compact_sort/multi',
                    import_url: 'jichu/yw/compact_sort/import',
                    table: 'CompactSort',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortOrder: 'ASC',
                pk: 'CS_ID',
                sortName: 'CS_ID',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CS_ID', title: __('Cs_id')},
                        {field: 'CS_Name', title: __('Cs_name'), operate: 'LIKE'},
                        {field: 'CS_Pressure', title: __('Cs_pressure'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});