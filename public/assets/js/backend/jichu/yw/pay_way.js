define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/pay_way/index' + location.search,
                    add_url: 'jichu/yw/pay_way/add',
                    edit_url: 'jichu/yw/pay_way/edit',
                    del_url: 'jichu/yw/pay_way/del',
                    multi_url: 'jichu/yw/pay_way/multi',
                    import_url: 'jichu/yw/pay_way/import',
                    table: 'PayWay',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortOrder: 'ASC',
                pageSize: 50,
                pk: 'PW_Num',
                sortName: 'PW_Num',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PW_Num', title: __('Pw_num'), operate: 'LIKE'},
                        {field: 'PW_WayName', title: __('Pw_wayname'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});