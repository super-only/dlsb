define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/purchase_type/index' + location.search,
                    add_url: 'jichu/yw/purchase_type/add',
                    edit_url: 'jichu/yw/purchase_type/edit',
                    del_url: 'jichu/yw/purchase_type/del',
                    multi_url: 'jichu/yw/purchase_type/multi',
                    import_url: 'jichu/yw/purchase_type/import',
                    table: 'PurchaseType',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortOrder: 'ASC',
                pk: 'PT_Num',
                sortName: 'PT_Num',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PT_Num', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'PT_Name', title: __('Pt_name'), operate: 'LIKE'},
                        {field: 'PT_EnterClass', title: __('Pt_enterclass'), operate: 'LIKE'},
                        {field: 'PT_IFDefault', title: __('Pt_ifdefault'), operate: false, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'Valid', title: __('Valid'), searchList: {"0": "无效", "1": "有效"}, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});