define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/balance_mode/index' + location.search,
                    add_url: 'jichu/yw/balance_mode/add',
                    edit_url: 'jichu/yw/balance_mode/edit',
                    del_url: 'jichu/yw/balance_mode/del',
                    multi_url: 'jichu/yw/balance_mode/multi',
                    import_url: 'jichu/yw/balance_mode/import',
                    table: 'BalanceMode',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortOrder: 'ASC',
                pageSize: 50,
                pk: 'BM_Num',
                sortName: 'BM_Num',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'BM_Num', title: __('Bm_num'), operate: 'LIKE'},
                        {field: 'BM_Name', title: __('Bm_name'), operate: 'LIKE'},
                        {field: 'bm_days', title: __('Bm_days'), operate: false},
                        {field: 'bm_default', title: __('Bm_default'),  operate: false, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'BM_Valid', title: __('Bm_valid'), searchList: {"0": "无效", "1": "有效"}, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});