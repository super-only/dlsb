define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/shipping_type/index' + location.search,
                    add_url: 'jichu/yw/shipping_type/add',
                    edit_url: 'jichu/yw/shipping_type/edit',
                    del_url: 'jichu/yw/shipping_type/del',
                    multi_url: 'jichu/yw/shipping_type/multi',
                    import_url: 'jichu/yw/shipping_type/import',
                    table: 'ShippingType',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortOrder: 'ASC',
                pk: 'ST_Num',
                sortName: 'ST_Num',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ST_Num', title: __('St_num'), operate: 'LIKE'},
                        {field: 'ST_Name', title: __('St_name'), operate: 'LIKE'},
                        {field: 'Valid', title: __('Valid'), searchList: {"0": "无效", "1": "有效"},table: table, formatter: Table.api.formatter.toggle},
                        {field: 'st_selected', title: __('St_selected'), operate: false,table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});