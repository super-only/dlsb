define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $(".btn-add").data("area",["50%","50%"]);
            $(".btn-edit").data("area",["50%","50%"]);
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/device_model/index' + location.search,
                    add_url: 'jichu/yw/device_model/add',
                    edit_url: 'jichu/yw/device_model/edit',
                    del_url: 'jichu/yw/device_model/del',
                    multi_url: 'jichu/yw/device_model/multi',
                    import_url: 'jichu/yw/device_model/import',
                    table: 'devicemodel',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'type', title: __('type'), operate: 'in', formatter: Table.api.formatter.label, searchList: Config.categoryList},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'detail', title: '设备型号', operate: 'LIKE'},
                        {field: 'valid', title: __('Valid'), searchList: {"0": "无效", "1": "有效"}, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'status', title: '开机状态', searchList: {"0": "关机", "1": "开机"}, table: table, formatter: Table.api.formatter.status},
                        {field: 'work', title: '工作状态', searchList: {"0": "休息中", "1": "工作中"}, table: table, formatter: Table.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,buttons: [
                            {
                                name: 'detail',
                                text: "详情",
                                title: "普通下料详情",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'jichu/yw/device_model/detail/type/{row.detail}',
                                visible: function(value){
                                    return value.detail?true:false;
                                }
                            },
                         ]}
                    ]
                ]
            });

            // 绑定过滤事件
            $('.filter-type ul li a', table.closest(".panel-intro")).on('click', function (e) {
                $(this).closest("ul").find("li").removeClass("active");
                $(this).closest("li").addClass("active");
                var field = 'mimetype';
                var value = $(this).data("value") || '';
                var object = $("[name='" + field + "']", table.closest(".bootstrap-table").find(".commonsearch-table"));
                if (object.prop('tagName') == "SELECT") {
                    $("option[value='" + value + "']", object).prop("selected", true);
                } else {
                    object.val(value);
                }
                table.trigger("uncheckbox");
                table.bootstrapTable('refresh', {pageNumber: 1});
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table',function(){
                $(".btn-editone").data("area",["50%","50%"]);
            });
            
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        detail: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/yw/device_model/detail/type/'+Config.type+'/ids/'+Config.ids + location.search,
                }
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: Config.sortName,
                columns: [
                    Config.columns
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});