const { options } = require("toastr");
define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    var Controller = {
        index: function () {

            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/wl/area_detail/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/wl/area_detail/index' + location.search,
                    add_url: 'jichu/wl/area_detail/add',
                    edit_url: 'jichu/wl/area_detail/edit',
                    del_url: 'jichu/wl/area_detail/del',
                    multi_url: 'jichu/wl/area_detail/multi',
                    import_url: 'jichu/wl/area_detail/import',
                    table: 'AreaDetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'AD_Num',
                sortName: 'AD_Num',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'AD_Num', title: __('Ad_num'), operate: 'LIKE'},
                        {field: 'AD_Name', title: __('Ad_name'), operate: 'LIKE'},
                        {field: 'AD_Freight', title: __('Ad_freight'), operate:'BETWEEN'},
                        {field: 'ParentNum', title: __('Parentnum'), operate: false},
                        {field: 'Valid', title: __('Valid'), searchList: {"0": "无效", "1": "有效"}, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'AD_Num': dd_num}),
                        op: JSON.stringify({'AD_Num': 'LIKE %'})
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
                
            }).jstree();
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on("change", "select[name='row[ParentNum]']", function () {
                    var num = $(this).find("option:selected").val();
                    $.ajax({
                        url: "jichu/wl/area_detail/stockTypeCode",
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                if (ret.code === 1) {
                                    $("#c-AD_Num").val(ret.data["AD_Num"]);
                                }
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });
            }
        }
    };
    return Controller;
});