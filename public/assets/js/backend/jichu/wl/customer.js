define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    classList = typeof classList=="undefined"?[]:classList;
    addressList = typeof addressList=="undefined"?[]:addressList;
    deptList = typeof deptList=="undefined"?[]:deptList;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/wl/customer/index' + location.search,
                    add_url: 'jichu/wl/customer/add',
                    edit_url: 'jichu/wl/customer/edit',
                    del_url: 'jichu/wl/customer/del',
                    multi_url: 'jichu/wl/customer/multi',
                    import_url: 'jichu/wl/customer/import',
                    table: 'customer',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'C_Num',
                sortName: 'C_Num',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'C_Num', title: __('C_num'), operate: 'LIKE'},
                        {field: 'C_Name', title: __('C_name'), operate: 'LIKE'},
                        {field: 'C_ShortName', title: __('C_shortname'), operate: 'LIKE'},
                        {field: 'CC_Num', title: __('Cc_num'), searchList: classList},
                        {field: 'AD_Name', title: __('Ad_name'), searchList: addressList},
                        {field: 'C_C_SalesMan', title: __('C_c_salesman'), operate: 'LIKE'},
                        {field: 'C_C_Dept', title: __('C_c_dept'), searchList: deptList},
                        {field: 'Valid', title: __('Valid'), searchList: {"0": "无效", "1": "有效"}, table: table, formatter: Table.api.formatter.toggle},
                        // {field: 'C_HeadNum', title: __('C_headnum'), operate: 'LIKE'},
                        // {field: 'C_TicketNum', title: __('C_ticketnum'), operate: 'LIKE'},
                        // {field: 'C_TaxNum', title: __('C_taxnum'), operate: 'LIKE'},
                        // {field: 'C_LegalMan', title: __('C_legalman'), operate: 'LIKE'},
                        // {field: 'C_Bank', title: __('C_bank'), operate: 'LIKE'},
                        // {field: 'C_Account', title: __('C_account'), operate: 'LIKE'},
                        // {field: 'C_C_Dept', title: __('C_c_dept'), operate: 'LIKE'},
                        // {field: 'C_C_Fax', title: __('C_c_fax'), operate: 'LIKE'},
                        // {field: 'C_C_Tel', title: __('C_c_tel'), operate: 'LIKE'},
                        // {field: 'C_C_Contacter1', title: __('C_c_contacter1'), operate: 'LIKE'},
                        // {field: 'C_C_Phone1', title: __('C_c_phone1'), operate: 'LIKE'},
                        // {field: 'C_C_Contacter2', title: __('C_c_contacter2'), operate: 'LIKE'},
                        // {field: 'C_C_Phone2', title: __('C_c_phone2'), operate: 'LIKE'},
                        // {field: 'C_C_Address', title: __('C_c_address'), operate: 'LIKE'},
                        // {field: 'C_C_GoodAddress', title: __('C_c_goodaddress'), operate: 'LIKE'},
                        // {field: 'C_C_GoodStore', title: __('C_c_goodstore'), operate: 'LIKE'},
                        // {field: 'C_C_Email', title: __('C_c_email'), operate: 'LIKE'},
                        // {field: 'C_C_MailCode', title: __('C_c_mailcode'), operate: 'LIKE'},
                        // {field: 'C_Memo', title: __('C_memo'), operate: 'LIKE'},
                        // {field: 'C_Writer', title: __('C_writer'), operate: 'LIKE'},
                        // {field: 'C_Updater', title: __('C_updater'), operate: 'LIKE'},
                        // {field: 'C_EditDate', title: __('C_editdate')},
                        // {field: 'Valid', title: __('Valid')},
                        // {field: 'C_StartDate', title: __('C_startdate')},
                        // {field: 'C_EndDate', title: __('C_enddate')},
                        // {field: 'AD_Name', title: __('Ad_name'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on("change", "select[name='row[CC_Num]']", function () {
                    var num = $(this).find("option:selected").val();
                    if(num){
                        $.ajax({
                            url: "jichu/wl/Customer/customerType",
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    var data = ret.hasOwnProperty("data") && ret.data != "" ? ret.data : "";
                                    if (ret.code === 1) {
                                        $("#c-C_Num").val(ret.data["C_Num"]);
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }else{
                        $("#c-C_Num").val("");
                    }
                });
            }
        }
    };
    return Controller;
});