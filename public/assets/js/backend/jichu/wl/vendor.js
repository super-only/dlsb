define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    classList = typeof classList=="undefined"?[]:classList;
    addressList = typeof addressList=="undefined"?[]:addressList;
    deptList = typeof deptList=="undefined"?[]:deptList;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/wl/vendor/index' + location.search,
                    add_url: 'jichu/wl/vendor/add',
                    edit_url: 'jichu/wl/vendor/edit',
                    del_url: 'jichu/wl/vendor/del',
                    multi_url: 'jichu/wl/vendor/multi',
                    import_url: 'jichu/wl/vendor/import',
                    table: 'Vendor',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'V_Num',
                sortName: 'V_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'V_Num', title: __('V_num'), operate: 'LIKE'},
                        {field: 'V_Name', title: __('V_name'), operate: 'LIKE'},
                        {field: 'V_ShortName', title: __('V_shortname'), operate: 'LIKE'},
                        {field: 'VC_Num', title: __('Vc_num'),  operate: false},
                        {field: 'vc.VC_Num', title: __('Vc_num'), searchList: classList, visible: false},
                        {field: 'AD_Name', title: __('Ad_name'), searchList: addressList},
                        {field: 'V_C_Contact1', title: __('V_c_contact1'), operate: 'LIKE'},
                        {field: 'V_C_Tel', title: __('V_c_tel'), operate: 'LIKE'},
                        {field: 'V_C_Fax', title: __('V_c_fax'), operate: 'LIKE'},
                        // {field: 'V_StartTime', title: __('V_starttime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'V_EndTime', title: __('V_endtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'V_C_SalesMan', title: __('V_c_salesman'), operate: 'LIKE'},
                        {field: 'V_C_Dept', title: __('V_c_dept'), searchList: deptList},
                        {field: 'Valid', title: __('Valid'), searchList: {"0": "无效", "1": "有效"},table: table, formatter: Table.api.formatter.toggle},
                        

                        // {field: 'V_HeadNum', title: __('V_headnum'), operate: 'LIKE'},
                        // {field: 'V_LegalMan', title: __('V_legalman'), operate: 'LIKE'},
                        // {field: 'V_Bank', title: __('V_bank'), operate: 'LIKE'},
                        // {field: 'V_Account', title: __('V_account'), operate: 'LIKE'},
                        // {field: 'V_TaxNum', title: __('V_taxnum'), operate: 'LIKE'},
                        // {field: 'V_EnollMoney', title: __('V_enollmoney'), operate:'BETWEEN'},
                        // {field: 'V_C_SalesMan', title: __('V_c_salesman'), operate: 'LIKE'},
                        // {field: 'V_C_Fax', title: __('V_c_fax'), operate: 'LIKE'},
                        // {field: 'V_C_Tel', title: __('V_c_tel'), operate: 'LIKE'},
                        // {field: 'V_C_Contact1', title: __('V_c_contact1'), operate: 'LIKE'},
                        // {field: 'V_C_Phone1', title: __('V_c_phone1'), operate: 'LIKE'},
                        // {field: 'V_C_Contact2', title: __('V_c_contact2'), operate: 'LIKE'},
                        // {field: 'V_C_Phone2', title: __('V_c_phone2'), operate: 'LIKE'},
                        // {field: 'V_C_MailCode', title: __('V_c_mailcode'), operate: 'LIKE'},
                        // {field: 'V_C_Email', title: __('V_c_email'), operate: 'LIKE'},
                        // {field: 'V_Writer', title: __('V_writer'), operate: 'LIKE'},
                        // {field: 'V_Updater', title: __('V_updater'), operate: 'LIKE'},
                        // {field: 'V_EditDate', title: __('V_editdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'V_C_Address', title: __('V_c_address'), operate: 'LIKE'},
                        // {field: 'V_StartTime', title: __('V_starttime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'V_EndTime', title: __('V_endtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'V_Memo', title: __('V_memo'), operate: 'LIKE'},
                        // {field: 'V_SL_Start', title: __('V_sl_start'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'V_SL_End', title: __('V_sl_end'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'V_SL_certificate', title: __('V_sl_certificate'), operate: 'LIKE'},
                        // {field: 'V_SPE_Start', title: __('V_spe_start'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'V_SPE_End', title: __('V_spe_end'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'V_SPE_certificate', title: __('V_spe_certificate'), operate: 'LIKE'},
                        // {field: 'V_SPY_Start', title: __('V_spy_start'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'V_SPY_End', title: __('V_spy_end'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        // {field: 'Valid', title: __('Valid')},
                        // {field: 'mader', title: __('Mader')},
                        // {field: 'V_EName', title: __('V_ename'), operate: 'LIKE'},
                        // {field: 'V_SuperNum', title: __('V_supernum'), operate: 'LIKE'},
                        // {field: 'V_Scope', title: __('V_scope'), operate: 'LIKE'},
                        // {field: 'V_Sort', title: __('V_sort')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on("change", "select[name='row[VC_Num]']", function () {
                    var num = $(this).find("option:selected").val();
                    if(num){
                        $.ajax({
                            url: "jichu/wl/Vendor/customerType",
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    if (ret.code === 1) {
                                        $("#c-V_Num").val(ret.data["V_Num"]);
                                    }else{
                                        $("#c-V_Num").val("");
                                        layer.msg(ret.msg);
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }else $("#c-V_Num").val("");
                    
                });
            }
        }
    };
    return Controller;
});