define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jichu/wl/customerclass/index' + location.search,
                    add_url: 'jichu/wl/customerclass/add',
                    edit_url: 'jichu/wl/customerclass/edit',
                    del_url: 'jichu/wl/customerclass/del',
                    multi_url: 'jichu/wl/customerclass/multi',
                    import_url: 'jichu/wl/customerclass/import',
                    table: 'customerclass',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'CC_Num',
                sortName: 'CC_Num',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CC_Num', title: __('Cc_num'), operate: 'LIKE'},
                        {field: 'CC_Name', title: __('Cc_name'), operate: 'LIKE'},
                        {field: 'Valid', title: __('Valid'), searchList: {"0": "无效", "1": "有效"}, table: table, formatter: Table.api.formatter.toggle},
                        {field: 'CC_Memo', title: __('Cc_memo'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                $(document).on('click', "input[name='row[Valid]']", function () {
                    var name = $("input[name='row[name]']");
                    var ismenu = $(this).val() == 1;
                    name.prop("placeholder", ismenu ? name.data("placeholder-menu") : name.data("placeholder-node"));
                    $('div[data-type="menu"]').toggleClass("hidden", !ismenu);
                });
                $("input[name='row[Valid]']:checked").trigger("click");
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});