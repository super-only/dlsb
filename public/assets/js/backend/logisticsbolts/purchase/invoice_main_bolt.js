define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    $.arrayIntersect = function(a, b)
    {
        return $.merge($.grep(a, function(i)
            {
                return $.inArray(i, b) == -1;
            }) , $.grep(b, function(i)
            {
                return $.inArray(i, a) == -1;
            })
        );
    };
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logisticsbolts/purchase/invoice_main_bolt/index' + location.search,
                    add_url: 'logisticsbolts/purchase/invoice_main_bolt/add',
                    edit_url: 'logisticsbolts/purchase/invoice_main_bolt/edit',
                    del_url: 'logisticsbolts/purchase/invoice_main_bolt/del',
                    auditor_url: 'logisticsbolts/purchase/invoice_main_bolt/auditor',
                    giveup_url: 'logisticsbolts/purchase/invoice_main_bolt/giveUp',
                    multi_url: 'logisticsbolts/purchase/invoice_main_bolt/multi',
                    import_url: 'logisticsbolts/purchase/invoice_main_bolt/import',
                    table: 'invoice_main_bolt',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'invoice_num',
                sortName: 'writer_time',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'status', title: '支付状态',searchList: Config.statusList, formatter:function(value){
                            return Config.statusList[value];
                        }},
                        {field: 'amount', title: '需支付', operate: false},
                        {field: 'pay_amount', title: '已支付', operate: false},
                        {field: 'invoice_num', title: __('Invoice_num'), operate: 'LIKE'},
                        {field: 'invoice_no', title: __('Invoice_no'), operate: 'LIKE'},
                        {field: 'business_type', title: __('Business_type'),searchList: Config.businessType, formatter:function(value){
                            return Config.businessType[value];
                        }},
                        {field: 'type', title: __('Type'),searchList: Config.invioceList, formatter:function(value){
                            return Config.invioceList[value];
                        }},
                        {field: 'kp_date', title: __('Kp_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'supplier', title: __('Supplier'),searchList: Config.vendorList, formatter:function(value){
                            return Config.vendorList[value];
                        }},
                        {field: 'procurement_type', title: __('Procurement_type'),searchList: Config.procurementType, formatter:function(value){
                            return Config.procurementType[value];
                        }},
                        {field: 'fax', title: __('Fax'), operate:'BETWEEN'},
                        {field: 'department', title: __('Department'),searchList: Config.deptList, formatter:function(value){
                            return Config.deptList[value];
                        }},
                        {field: 'salesman', title: __('Salesman'), operate: 'LIKE'},
                        {field: 'currency', title: __('Currency'),searchList: Config.bzList, formatter:function(value){
                            return Config.bzList[value];
                        }},
                        {field: 'rate', title: __('Rate'), operate:'BETWEEN'},
                        {field: 'fp_date', title: __('Fp_date')},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        // {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        selectstorein: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logisticsbolts/purchase/invoice_main_bolt/selectStorein/v_num/'+Config.v_num + location.search,
                }
            });

            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'Bsid',
                sortName: 'Bsid',
                search: false,
                // clickToSelect: true,
                // singleSelect: true,
                showToggle: false,
                // onlyInfoPagination: true,
                height: document.body.clientHeight,
                // showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'Bsid', title: "入库ID", operate: false},
                        {field: 'In_Num', title: '入库编号', operate: "LIKE"},
                        {field: 'InDate', title: '入库日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'InPlace', title: '所在仓库', operate:false},
                        {field: 'IM_Num', title: '材料编码', operate: 'LIKE'},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'IM_LName', title: '材料材质', operate: 'LIKE'},
                        {field: 'IM_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'IM_Length', title: '无扣长', operate: 'LIKE'},
                        {field: 'IM_Measurement', title: '单位', operate: '='},
                        {field: 'InCount', title: '数量', operate:false},
                        {field: 'InWeight', title: '重量', operate:false},
                        {field: 'InNaxPrice', title: '单价', operate:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent=table.bootstrapTable('getAllSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                let table_height = document.body.clientHeight-300-$(".part_one").height();
                $(".table-responsive").height(table_height);
                // var tableSidList = Config.tableSidList??[];
                $('#c-procurement_type').change(function(){
                    $("#tbshow").html("");
                });
                $(document).on('click', ".overtext", function(e){
                    let field = $(this).data("table");
                    //c-unit c-salesman c-qg
                    if(field == "c-supplier"){
                        let v_num = $("#c-supplier_num").val();
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                let change_v_num = value.V_Num;
                                if(v_num!='' && v_num!=change_v_num){
                                    var index = layer.confirm("修改供应商会导致表格内容清空，确定修改吗？", {
                                        btn: ['确定', '取消'],
                                    }, function(data) {
                                        $("#c-supplier_num").val(change_v_num);
                                        $("#c-supplier").val(value.V_Name);
                                        $("#c-amount").val(0);
                                        $("#tbshow").html("");
                                        layer.close(index);
                                    })
                                }else{
                                    $("#c-supplier_num").val(change_v_num);
                                    $("#c-supplier").val(value.V_Name);
                                }
                            }
                        };
                        Fast.api.open('chain/material/material_note/chooseVendor',"对方单位选择",options);
                    }
                    if(field == "c-salesman"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                $("#c-salesman").val(value.E_Name);
                            }
                        };
                        Fast.api.open("chain/sale/project_cate_log/selectSaleMan","业务员选择",options);
                    }
                });
                $(document).on('click', "#syncMaterial", function(e){
                    let v_num = $("#c-supplier_num").val();
                    if(!v_num){
                        layer.msg("请选择供应商");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            let existValue = [];
                            let endValue = [];
                            $.each($('input[name="table[sid_id][]"]'),function(s_index,s_v){
                                existValue.push($(s_v).val());
                            });
                            $.each(value,function(v_index,v_e){
                                if($.inArray(v_e["SID_ID"]+'',existValue)===-1){
                                    endValue.push(v_e);
                                }
                            });
                            $.ajax({
                                url: 'logisticsbolts/purchase/invoice_main_bolt/addMaterial',
                                type: 'post',
                                dataType: 'json',
                                data: {data:JSON.stringify(endValue)},
                                success: function (ret) {
                                    var content = '';
                                    if(ret.code==1){
                                        content = ret.data["field"];
                                    }
                                    $("#tbshow").append(content);
                                    $.each($("#tbshow").find("tr"),function(t_index,t_value){
                                        jisuan(t_index);
                                    })
                                    Controller.api.getSum();
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            })
                        }
                    };
                    Fast.api.open('logisticsbolts/purchase/invoice_main_bolt/selectStorein/v_num/'+v_num,"选择材料",options);
                });
                $(document).on('keyup','td input[name="table[price][]"]',function () {
                    let index = $(this).parents('tr').index();
                    jisuan(index);
                    Controller.api.getSum();
                })
                $(document).on('change','td select[name="table[way][]"]',function () {
                    let index = $(this).parents('tr').index();
                    jisuan(index);
                    Controller.api.getSum();
                })
                $(document).on('keyup','#c-fax',function () {
                    $.each($("#tbshow").find("tr"),function(t_index,t_value){
                        jisuan(t_index);
                    })
                    Controller.api.getSum();
                    
                })
                function jisuan(index){
                    let rate = $("#c-fax").val();
                    rate = rate?(parseFloat)(rate)*0.01:0;
                    // let index = object.parents('tr').index();
                    let body = $("#tbshow").find("tr").eq(index);
                    let price = body.find('td input[name="table[price][]"]').val();
                    var way = body.find('td select[name="table[way][]"]').val();
                    if(way==1) var count = body.find('td input[name="table[weight][]"]').val().trim();
                    else var count = body.find('td input[name="table[count][]"]').val().trim();
                    price = price==''?0:(parseFloat)(price);
                    count = count==''?0:(parseFloat)(count);

                    // 金额 单价*数量 税额 金额*税 价税合计 金额+税额 含税单价 价税合计/数量
                    let amount = (parseFloat)(price*count).toFixed(5);
                    let fax_amount = (parseFloat)(amount*rate).toFixed(5);
                    let sum_amount = ((parseFloat)(amount)+(parseFloat)(fax_amount)).toFixed(5);
                    let fax_price = (parseFloat)(count?sum_amount/count:0).toFixed(5);
                    body.find('td input[name="table[fax_price][]"]').val(fax_price);
                    body.find('td input[name="table[fax_amount][]"]').val(fax_amount);
                    body.find('td input[name="table[sum_amount][]"]').val(sum_amount);
                    body.find('td input[name="table[amount][]"]').val(amount);
                }
                $(document).on('click', ".del", function(e){
                    $( e.target ).closest("tr").remove();
                    Controller.api.getSum();
                });
                $(document).on("click", "#auditor", function () {
                    if(Config.flag) return false;
                    check('审核之后无法修改，确定审核？',"logisticsbolts/purchase/invoice_main_bolt/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    if(!Config.flag) return false;
                    check('确定弃审？',"logisticsbolts/purchase/invoice_main_bolt/giveUp",Config.ids);
                    
                });
                function check(msg,url,num){
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {ids: num},
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    Layer.msg(ret.msg);
                                    if (ret.code === 1) {
                                        window.location.reload();
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                };
            },
            getSum: function() {
                var map_value = 0;
                $('#tbshow tr').each(function(te,tv) {
                    ini_value = $(tv).find('td input[name="table[sum_amount][]"]').val();
                    map_value += isNaN(parseFloat(ini_value))?0:parseFloat(ini_value); 
                });
                $("#c-amount").val(map_value);
            }
        }
    };
    return Controller;
});