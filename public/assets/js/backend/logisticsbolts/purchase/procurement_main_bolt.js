define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logisticsbolts/purchase/procurement_main_bolt/index' + location.search,
                    add_url: 'logisticsbolts/purchase/procurement_main_bolt/add',
                    edit_url: 'logisticsbolts/purchase/procurement_main_bolt/edit',
                    del_url: 'logisticsbolts/purchase/procurement_main_bolt/del',
                    // auditor_url: 'logisticsbolts/purchase/procurement_main_bolt/auditor',
                    // giveup_url: 'logisticsbolts/purchase/procurement_main_bolt/giveUp',
                    multi_url: 'logisticsbolts/purchase/procurement_main_bolt/multi',
                    import_url: 'logisticsbolts/purchase/procurement_main_bolt/import',
                    table: 'procurement_main_bolt',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pm_num',
                sortName: 'writer_time',
                columns: [
                    [
                        {checkbox: true},
                        {
                            field: 'down',
                            title: '导出',
                            operate: false,
                            formatter: Controller.api.formatter.subnode
                        },
                        {field: 'pm_num', title: __('Pm_num'), operate: 'LIKE'},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'type', title: __('Type'), searchList: Config.purchaseList, formatter: function(value){
                            return Config.purchaseList[value]??value;
                        }},
                        {field: 'unit', title: __('Unit'), searchList: Config.vendorList, formatter: function(value){
                            return Config.vendorList[value]??value;
                        }},
                        {field: 'header', title: __('Header'), operate: 'LIKE'},
                        {field: 'sign_date', title: __('Sign_date')},
                        {field: 'currency', title: __('Currency'),searchList: Config.bzList, formatter:function(value){
                            return Config.bzList[value];
                        }},
                        {field: 'rate', title: __('Rate'), operate:'BETWEEN'},
                        {field: 'department', title: __('Department'), searchList: Config.deptList, formatter: function(value){
                            return Config.deptList[value]??value;
                        }},
                        {field: 'salesman', title: __('Salesman'), operate: 'LIKE'},
                        {field: 'amount', title: __('Amount'), operate:'BETWEEN'},
                        {field: 'count', title: __('Count'), operate:'BETWEEN'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        selectprocurementlist: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logisticsbolts/purchase/procurement_main_bolt/selectProcurementList/v_num/'+Config.v_num+ location.search,
                    table: 'procurement_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pm_id',
                sortName: 'writer_time',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pm_num', title: __('Pm_num'), operate: 'LIKE'},
                        {field: 'name', title: '合同名称', operate: 'LIKE'},
                        {field: 'im_num', title: '材料编号', operate: 'LIKE'},
                        {field: 'im_class', title: '材料名称', operate: 'LIKE'},
                        {field: 'im_spec', title: '材料规格', operate: 'LIKE'},
                        {field: 'l_name', title:'材质', operate: 'LIKE'},
                        // {field: 'IM_PerWeight', title: '比重', operate: false},
                        {field: 'length', title: '无扣长', operate: false},
                        {field: 'unit', title: '单位', operate: false},
                        {field: 'count', title: '数量', operate: false},
                        {field: 'weight', title: '重量', operate: false},
                        {field: 'way', title: '计算价格方式', operate: false,formatter:function(value){
                            return value==1?'按重量':'按数量';
                        }},
                        {field: 'price', title: '单价', operate: false},
                        {field: 'writer', title: __('Writer'), operate: false},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var tableContent=table.bootstrapTable('getAllSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                let table_height = document.body.clientHeight-300-$(".part_one").height();
                $(".table-responsive").height(table_height);
                $(document).on('click', "#syncMaterial", function(e){
                    // let type = $("#c-procurement_type").val();
                    // console.log(type);
                    // if(type===''){
                        // layer.msg("必须选择采购类型");
                        // return false;
                    // }
                    // var v_num = $("#c-unit_num").val();
                    // if(!v_num){
                    //     layer.msg("请先选择对方单位！");
                    //     return false;
                    // }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            let existValue = [];
                            let endValue = [];
                            $.each($('input[name="table[AD_ID][]"]'),function(s_index,s_v){
                                existValue.push($(s_v).val());
                            });
                            $.each(value,function(v_index,v_e){
                                if($.inArray(v_e["AD_ID"]+'',existValue)===-1){
                                    endValue.push(v_e);
                                }
                            });
                            $.ajax({
                                url: 'logisticsbolts/purchase/procurement_main_bolt/addMaterial',
                                type: 'post',
                                dataType: 'json',
                                data: {data:JSON.stringify(endValue)},
                                success: function (ret) {
                                    var content = '';
                                    if(ret.code==1){
                                        content = ret.data;
                                    }
                                    $("#tbshow").append(content);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            })
                        }
                    };
                    var url = 'chain/fastening/applylist_jgj/syncRequisition';
                    Fast.api.open(url,"选择材料",options);
                });
                $(document).on('click', ".overtext", function(e){
                    let field = $(this).data("table");
                    //c-unit c-salesman c-qg
                    if(field == "c-unit"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                $("#c-unit_num").val(value.V_Num);
                                $("#c-unit").val(value.V_Name);
                            }
                        };
                        Fast.api.open('chain/material/material_note/chooseVendor',"对方单位选择",options);
                    }
                    if(field == "c-salesman"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                $("#c-salesman").val(value.E_Name);
                            }
                        };
                        Fast.api.open("chain/sale/project_cate_log/selectSaleMan","业务员选择",options);
                    }
                });
                $(document).on('click', ".del", function(e){
                    $( e.target ).closest("tr").remove();
                    Controller.api.getSum();
                });
                $('#tbshow').on('keyup','td input[name="table[count][]"],td input[name="table[weight][]"],td input[name="table[price][]"]',function () {
                    Controller.api.getAmount($(this));
                    Controller.api.getSum();
                });
                $('#tbshow').on('change','td select[name="table[way][]"]',function () {
                    Controller.api.getAmount($(this));
                    Controller.api.getSum();
                });
                $(document).on("click", "#auditor", function () {
                    if(Config.flag) return false;
                    check('审核之后无法修改，确定继续审核？',"logisticsbolts/purchase/procurement_main_bolt/checkAuditor","logisticsbolts/purchase/procurement_main_bolt/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    if(!Config.flag) return false;
                    check('确定弃审？',"logisticsbolts/purchase/procurement_main_bolt/checkGive","logisticsbolts/purchase/procurement_main_bolt/giveUp",Config.ids);
                    
                });
                function check(msg,check_url,url,num){
                    $.ajax({
                        url: check_url,
                        type: 'post',
                        dataType: 'json',
                        data: {ids: num},
                        success: function (ret) {
                            if(ret.code == 0) layer.msg(ret.msg);
                            else{
                                let ret_msg = ret.msg+msg;
                                var index = layer.confirm(ret_msg, {
                                    btn: ['确定', '取消'],
                                }, function(data) {
                                    Fast.api.ajax({
                                        url: url,
                                        type: 'post',
                                        dataType: 'json',
                                        data: {ids: num},
                                    },function (data, ret) {
                                        if(ret.code == 1) window.location.reload();
                                        else layer.msg(ret.msg);
                                    });
                                    layer.close(index);
                                })
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                };
            },
            getAmount: function (object) {
                var index = object.parents('tr').index();
                var way = object.parents("tbody").find("tr").eq(index).find('td select[name="table[way][]"]').val();
                if(way==1) var count = object.parents('tbody').find('tr').eq(index).find('td input[name="table[weight][]"]').val().trim();
                else var count = object.parents('tbody').find('tr').eq(index).find('td input[name="table[count][]"]').val().trim();
                var price = object.parents('tbody').find('tr').eq(index).find('td input[name="table[price][]"]').val().trim();
                var amount = 0;
                count==''?count=0:'';
                amount==''?amount=0:'';
                count = parseFloat(count);
                price = parseFloat(price);
                amount = (price*count).toFixed(5);
                object.parents('tbody').find('tr').eq(index).find('td input[name="table[amount][]"]').val(amount);
            },
            //0 是返回三个数， 1是金钱 2是数量 3是重量
            getSum: function(field=[1,2,3]) {
                var map = {1:"amount", 2: "count", 3: "weight"};
                var map_value = {"amount":0, "count":0, "weight":0};
                var ini_value = 0;
                $('#tbshow tr').each(function(te,tv) {
                    $.each(map_value,function(key,value){
                        ini_value = $(tv).find('td input[name="table['+key+'][]"]').val();
                        map_value[key] += isNaN(parseFloat(ini_value))?0:parseFloat(ini_value); 
                    });
                });
                for(var i=0;i<=field.length;i++){
                    $("#c-"+map[i]).val((parseFloat)(map_value[map[i]]).toFixed(5));
                }
            },
            formatter: {
                subnode: function (value, row, index) {
                    return '<a href="procurement_main_bolt/download/pm_num/' + row["pm_num"] + '" data-toggle="tooltip" title="导出" data-id="' + row["pm_num"] + '" class="btn btn-xs btn-success btn-download-sub" target="_blank"><i class="fa fa-cloud-download"></i></a>';
                }
            }
        }
    };
    return Controller;
});