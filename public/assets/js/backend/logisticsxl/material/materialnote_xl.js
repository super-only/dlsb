define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree','input-tag'], function ($, undefined, Backend, Table, Form, undefined, InputTag) {

    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: [],
                height: 250,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'MGN_ID', title: '到货明细ID'},
                        {field: 'IM_Class', title: '材料名称'},
                        {field: 'IM_Spec', title: '规格'},
                        {field: 'L_Name', title: '材质'},
                        {field: 'MGN_Length', title: '到货长度(m)'},
                        {field: 'MGN_Width', title: '到货宽度(m)'},
                        {field: 'MGN_Count', title: '实际到货数量'},
                        {field: 'MGN_Weight', title: '实际到货重量(kg)'},
                        {field: 'mgn_price', title: '到货单件(元/kg)'},
                        {field: 'MGN_Destination', title: '到货去向'},
                        {field: 'mgn_testnum', title: '试验编号'},
                        {field: 'MGN_Maker', title: '生产厂家'},
                        {field: 'MGN_Memo', title: '备注'},
                        {field: 'mgn_DuiFangDian', title: '堆放点'}
                    ]
                ]
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logisticsxl/material/materialnote_xl/index' + location.search,
                    add_url: 'logisticsxl/material/materialnote_xl/add',
                    edit_url: 'logisticsxl/material/materialnote_xl/edit',
                    del_url: 'logisticsxl/material/materialnote_xl/del',
                    multi_url: 'logisticsxl/material/materialnote_xl/multi',
                    import_url: 'logisticsxl/material/materialnote_xl/import',
                    table: 'materialnote',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'MN_ID',
                sortName: 'WriteTime',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                // showExport: false,
                height: 500,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'MN_Num', title: __('Mn_num'), operate: 'LIKE'},
                        {field: 'MN_ID', title: __('Mn_id'), operate: false,visible:false},
                        // {field: 'V_Num', title: __('V_num'), operate: false},
                        {field: 'V_Name', title: __('V_num'), operate: 'LIKE'},
                        {field: 'MN_Date', title: __('Mn_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'ST_Name', title: __('St_num'), operate: 'LIKE'},
                        {field: 'mn_delivernum', title: __('Mn_delivernum'), operate: 'LIKE'},
                        {field: 'mn_license', title: __('Mn_license'), operate: 'LIKE'},
                        {field: 'Writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'WriteTime', title: __('Writetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'MN_Company', title: __('Mn_company'), operate: 'LIKE'},
                        {field: 'MN_Charger', title: __('Mn_charger'), operate: 'LIKE'},
                        {field: 'MN_Operater', title: __('Mn_operater'), operate: false},
                        {field: 'is_check', title: '状态', searchList:{1:"未审核",2:"已审核"}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var MN_Num = $element.MN_Num;
                if(MN_Num == '') return false;
                $.ajax({
                    url: 'logisticsxl/material/materialnote_xl/requisitionDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:MN_Num},
                    success: function (ret) {
                        var content = [];
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
        },
        add: function () {
            Controller.api.bindevent("add");
            $('#V_Name').focus(function(){
                $(".box").show();
            });
            $('#V_Name').blur(function(e){
                $(document).on('click', "#table-1 tbody tr", function(e){
                    var vendor_id = $(this).find("td").eq(0)[0].innerText;
                    var vendor_name = $(this).find("td").eq(1)[0].innerText;
                    $("#V_Num").val(vendor_id);
                    $("#V_Name").val(vendor_name);
                    $(".box").hide();
                });
            })
            $("#V_Name").on('keyup', function () {
                var table1 = $("#table-1");
                var input = $(this);
                Backend.search(table1, input);
                
            })
            
            $(document).on('click', "#table-1 tbody tr", function(e){
                var vendor_id = $(this).find("td").eq(0)[0].innerText;
                var vendor_name = $(this).find("td").eq(1)[0].innerText;
                $("#V_Num").val(vendor_id);
                $("#V_Name").val(vendor_name);
                $(".box").hide();
            });

        },
        edit: function () {
            Controller.api.bindevent("field");
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        layer.msg(ret.msg);
                        window.location.reload();
                    }
                    return false;
                });
                // var init_data = Config.init_data??[];
                // var tagObj1 = InputTag.render({
                //     elem: '.c-ht',
                //     data: init_data,//初始值
                //     number: init_data,
                //     onNumberChange: function (data, value, type) {
                //         $('#c-ht').val(JSON.stringify(data));
                //     }
                // });
                // $(document).on('click', ".overtext", function(e){
                //     var options = {
                //         shadeClose: false,
                //         shade: [0.3, '#393D49'],
                //         area: ["100%","100%"],
                //         callback:function(value){
                //             if(value.length==0) return false;
                //             $.each(value,function(index,e){
                //                 if($.inArray(e.pm_num, init_data)===-1){
                //                     init_data.push(e.pm_num);
                //                 }
                //             });
                //             $('#c-ht').val(JSON.stringify(init_data));
                //             tagObj1.clearData();
                //             tagObj1.init({
                //                 elem: '.c-ht',
                //                 data: init_data,//初始值
                //                 number: init_data
                //             });
                //         }
                //     };
                //     Fast.api.open('chain/purchase/procurement_main/selectProcurement',"合同选择",options);
                // });
                $(document).on("click", "#export", function () {
                    window.open('/admin.php/logisticsxl/material/materialnote_xl/export/ids/' + Config.ids);
                });
                $(document).on('click', "#chooseVendor", function(e){
                    let v_num = $("#V_Num").val();
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            if(v_num && v_num!=value.V_Num){
                                $("#tbshow").html('');
                            }
                            $("#V_Num").val(value.V_Num);
                            $("#V_Name").val(value.V_Name);
                        }
                    };
                    Fast.api.open('chain/material/material_note/chooseVendor',"供应商选择",options);
                });
                $(document).on('click', "#chooseMaterial", function(e){
                    let v_num = $("#V_Num").val();
                    if(!v_num){
                        layer.msg("请先选择供应商");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var tableField = Config.tableField;
                            var limber = Config.limberList;
                            var content = "";
                            $.each(value,function(v_index,v_e){
                                content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                $.each(tableField,function(t_index,e){
                                    let input_value = '';
                                    if(e[1]=="MGN_Length" || e[1]=="MGN_Width"){
                                        input_value = (v_e[e[6]]/1000).toFixed(3);
                                    }else input_value = (typeof(v_e[e[6]]) == 'undefined'?e[4]:v_e[e[6]])
                                    content += '<td '+e[5]+'><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+input_value+'"></td>';
                                });
                                content += "</tr>";
                            });
                            $("#tbshow").append(content);
                        }
                    };
                    Fast.api.open('logisticsxl/purchase/procurement_main_xl/selectProcurementList/v_num/'+v_num,"选择材料",options);
                    // Fast.api.open('chain/purchase/requisition/chooseMaterial',"选择材料",options);
                })

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(1).find('input').val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'logisticsxl/material/materialnote_xl/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                });

                $('#tbshow').on('keyup','td input[name="table_row[MGN_Length][]"]',function () {
                    get_weight($(this));
                })
                $('#tbshow').on('keyup','td input[name="table_row[MGN_Width][]"]',function () {
                    get_weight($(this));
                })
                $('#tbshow').on('keyup','td input[name="table_row[MGN_Count][]"]',function () {
                    get_weight($(this));
                })
                function get_weight(content){
                    var length = content.parents('tr').find("input[name='table_row[MGN_Length][]']").val().trim();
                    var width = content.parents('tr').find("input[name='table_row[MGN_Width][]']").val().trim();
                    var number = content.parents('tr').find("input[name='table_row[MGN_Count][]']").val().trim();
                    var bz = content.parents('tr').find("input[name='table_row[IM_PerWeight][]']").val().trim();
                    var IM_Class = content.parents('tr').find("input[name='table_row[IM_Class][]']").val().trim();
                    var IM_Spec = content.parents('tr').find("input[name='table_row[IM_Spec][]']").val().trim();
                    length = length==''?0:parseFloat(length);
                    width = width==''?0:parseFloat(width);
                    number = number==''?0:parseInt(number);
                    bz = bz==''?0:parseFloat(bz);
                    var weight=0;
                    if(IM_Class == '角钢'){
                        weight = (length*bz*number).toFixed(2);
                    }else if(IM_Class == "槽钢"){
                        weight = (length*bz*number).toFixed(2);
                    }else if(IM_Class == "钢板"){
                        weight = (length*width*Math.abs(IM_Spec)*bz*number).toFixed(2);
                    }else if(IM_Class == "钢管" || IM_Class == "法兰"){
                        var rep = /\d+\.?\d+/g;
                        var bj = rep.exec(IM_Spec);
                        var big_r = typeof(bj[0]) == 'undefined'?0:(bj[0]/2*0.001);
                        var small_r = typeof(bj[1]) == 'undefined'?0:(bj[1]/2*0.001);
                        weight = (length*3.14159*(big_r*big_r-small_r*small_r)*7850*number).toFixed(2);
                    }else if(IM_Class == "圆钢"){
                        var rep = /\d+\.?\d+/g;
                        var bj = rep.exec(IM_Spec);
                        var big_r = typeof(bj[0]) == 'undefined'?0:bj[0];
                        weight = (big_r*big_r*length*0.00617).toFixed(2);
                    }else if(IM_Class == "格栅板"){
                        // var rou = {"G255/40/100W":32.1,"G253/40/100W":21.3};
                        var rou_one = IM_Spec=="G255/40/100W"?32.1:21.3;
                        weight = round(rou_one * length*width,4);
                    }
                    content.parents('tr').find("input[name='table_row[MGN_Weight][]']").val(weight);
                    content.parents('tr').find("input[name='table_row[mgn_formuleweight][]']").val(weight);
                }

                var kj_limberList = Config.kj_limberList;
                $('#tbshow').on('keyup','td input[name="table_row[L_Name][]"]',function () {
                    var text_count = $(this).parents('tr').find("input[name='table_row[L_Name][]']").val().trim();
                    if(kj_limberList[text_count] != undefined ) $(this).parents('tr').find("input[name='table_row[L_Name][]']").val(kj_limberList[text_count])
                })

                $(document).on("click", "#author", function () {
                    var num = $("#MN_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"logisticsxl/material/materialnote_xl/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#MN_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"logisticsxl/material/materialnote_xl/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
            }
        }
    };
    return Controller;
});