define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'rkddata'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logisticsxl/material/store_in_xl/index' + location.search,
                    add_url: 'logisticsxl/material/store_in_xl/add',
                    edit_url: 'logisticsxl/material/store_in_xl/edit',
                    multi_url: 'logisticsxl/material/store_in_xl/multi',
                    table: 'storeindetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SI_OtherID',
                sortName: 'SID_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                // showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'offer', title: '是否传U8', searchList:{0:"否",1:"是"}},
                        {field: 'SID_ID', title: __('Sid_id'), operate: false, visible: false},
                        {field: 'SI_OtherID', title: 'SI_OtherID', operate: false, visible: false},
                        {field: 'SI_InDate', title: '入库日期', defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'SI_WareHouse', title: '所在仓库', operate: 'LIKE'},
                        {field: 'IM_Num', title: '存货编码', operate: false},
                        // {field: 'PC_ProjectName', title: '工程名称', operate: 'LIKE'},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'sid.L_Name', title: __('L_name'), operate: 'LIKE',visible: false},
                        {field: 'L_Name', title: __('L_name'), operate: false},
                        {field: 'IM_Spec', title: '规格', operate: '='},
                        {field: 'SID_Weight', title: __('Sid_weight'), operate:false},
                        {field: 'SID_Length', title: __('Sid_length'), operate:'=1000'},
                        {field: 'SID_Width', title: __('Sid_width'), operate:false},
                        {field: 'SID_Count', title: __('Sid_count'), operate:false},
                        {field: 'SID_FactWeight', title: __('Sid_factweight'), operate:false},
                        // {field: 'SID_PlanPrice', title: __('Sid_planprice'), operate:false},
                        // {field: 'SID_NoTaxPrice', title: __('Sid_notaxprice'), operate:false},
                        {field: 'SID_RestCount', title: __('Sid_restcount'), operate:false},
                        {field: 'SID_RestWeight', title: __('Sid_restweight'), operate:false},
                        {field: 'SID_TestResult', title: __('Sid_testresult'), operate: 'LIKE'},
                        // {field: 'mgn_DuiFangDian', title: '堆放点', operate: 'LIKE'},
                        {field: 'LuPiHao', title: __('Lupihao'), operate: 'LIKE'},
                        {field: 'PiHao', title: __('Pihao'), operate: false},
                        {field: 'V_Name', title: '供应商', operate: 'LIKE'},
                        {field: 'RSC_ID', title: '入库方式', searchList: Config.rsclist},
                        {field: 'is_check', title: '状态', searchList:{1:"未审核",2:"已审核"}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            // $("#rkdprint").click(function () {
            //     window.top.Fast.api.open('logisticsxl/material/store_in_xl/rkdPrint/ids/' + Config.ids, '原材料入库', {
            //         area: ["100%", "100%"]
            //     });
            // });
            Controller.api.bindevent("edit");
        },
        selectphysic: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logisticsxl/material/store_in_xl/selectPhysic' + location.search,
                    table: 'commisiontestdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'CTD_ID',
                sortName: 'CTD_ID',
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'MGN_ID', title: "MGN_ID", operate: false, visible: false},
                        {field: 'MGN_ID2', title: "MGN_ID2", operate: false, visible: false},
                        {field: 'MGN_Destination', title: "所属仓库", operate: false},
                        {field: 'MN_Date', title: '到货日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'CTD_testnum', title: '实验编号', operate: 'LIKE'},
                        {field: 'V_Name', title: '供应商', operate:'LIKE'},
                        {field: 'LuPiHao', title: '炉号', operate: 'LIKE'},
                        {field: 'PiHao', title: '批号', operate: 'LIKE'},
                        {field: 'CTD_Pi', title: '进货批次', operate: 'LIKE'},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'CTD_Spec', title: '规格', operate: '='},
                        {field: 'L_Name', title: '材质', operate: false},
                        {field: 'ss.L_Name', title: '材质', operate: 'LIKE', visible: false},
                        {field: 'MGN_Length', title: '长度(mm)', operate: false},
                        {field: 'MGN_Width', title: '宽度(mm)', operate: false},
                        {field: 'CTD_FactCount', title: '数量', operate: false},
                        {field: 'CTD_Weight', title: '重量(kg)', operate: false},
                        {field: 'sy_count', title: '剩余数量', operate: false},
                        {field: 'sy_weight', title: '剩余重量', operate: false},
                        // {field: 'mgn_price', title: '到货价', operate: false},
                        {field: 'CTD_UnqualityCount', title: '外观不合格数量', operate: false},
                        {field: 'CTD_Result', title: '理化试验结果', operate: false},
                        {field: 'MN_Num', title: '供通号', operate: false},
                        {field: 'CT_Num', title: '委试号', operate: 'LIKE'},
                        {field: 'MGN_Maker', title: '生产厂家', operate: false},
                        {field: 'mgn_DuiFangDian', title: '堆放点', operate: 'LIKE'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        selectarrival: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logisticsxl/material/store_in_xl/selectArrival' + location.search,
                    table: 'materialgetnotice',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'MGN_ID',
                sortName: 'MGN_ID2',
                sortOrder: 'DESC',
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'MGN_Destination', title: "所属仓库", operate: false},
                        {field: 'MN_Date', title: '到货日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'V_Name', title: '供应商', operate:'LIKE'},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'IM_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'L_Name', title: '材质', operate: false},
                        {field: 'MGN_Length', title: '长度(mm)', operate: false},
                        {field: 'MGN_Width', title: '宽度(mm)', operate: false},
                        {field: 'MGN_Count', title: '数量', operate: false},
                        {field: 'MGN_Weight', title: '重量(kg)', operate: false},
                        {field: 'SID_Count', title: '剩余数量', operate: false},
                        {field: 'SID_Weight', title: '剩余重量', operate: false},
                        {field: 'MN_Num', title: '供通号', operate: false},
                        {field: 'SID_TestResult', title: '生产厂家', operate: false},
                        {field: 'mgn_DuiFangDian', title: '堆放点', operate: 'LIKE'},
                        {field: 'SID_testnum', title: '实验编号', operate: 'LIKE'},
                        {field: 'MGN_ID', title: "MGN_ID", operate: false, visible: false},
                        {field: 'MGN_ID2', title: "MGN_ID2", operate: false, visible: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        rkdprint: function() {
            
            let list=Config.list;
            console.log(list);
            let mainInfos = Config.mainInfos;
            //list[i]['SID_TestResult'],//生产厂家
            
            let tb_tmp = [];
            let countSum = 0;
            let weightSum = 0;
            for(let i=0;i<list.length;i++){
                countSum += list[i]['SID_Count'];
                weightSum += list[i]['SID_Weight']*100;
                let tmp={
                    'ch':list[i]['IM_Class'], //存货编码
                    'cz':list[i]['L_Name'],
                    'gg':list[i]['IM_Spec'],//长宽(m)
                    'cd':Number(list[i]['SID_Length']).toFixed(1),//数量
                    'sl':list[i]['SID_Count'],//重量
                    'zl':list[i]['SID_Weight'],
                    'lh':list[i]['LuPiHao'],//炉号
                };
                tb_tmp.push(tmp)
            }

            
            console.log('tb_tmp',tb_tmp);
            let tmp2={
                'ch':'合计：', //存货编码
                'cz':'',
                'gg':'',
                'sl':countSum,
                'zl':weightSum/100,
                'cd':'',
                'lh':'',
            };
            
            tb_tmp.push(tmp2);
            

            let data_tmp={
                rkdh:mainInfos['SI_OtherID'],
                szck:mainInfos['SI_WareHouse'],
                rklb:mainInfos['RSC_ID'],
                rkrq:mainInfos['SI_InDate'],
                gys:list.length? list[0]['V_Name']: '',
                bz:mainInfos['SI_Memo'],
                zd:mainInfos['SI_Writer'],
                sccj:list.length>0? list[0]['SID_TestResult']: '',
                gcmc: mainInfos['PC_ProjectName'],
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){
                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: rkddata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            pageto('#p_mx1',0);
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                
                $(document).on('click', "#allDel", function(env){
                    var num = Config.ids;
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'logisticsxl/material/store_in_xl/allDel',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        parent.location.reload();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        layer.msg("删除失败");
                    }
                })

                $(document).on('click', "#chooseArrival", function(env){
                    var ware = $("select[name='row[SI_WareHouse]'] option:selected").text();
                    if(ware!="请选择"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                var tableField = Config.tableField;
                                var content = "";
                                $.each(value,function(v_index,v_e){
                                    content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                    $.each(tableField,function(t_index,e){
                                        if(e[1]=='SID_Length' || e[1]=='SID_Width') content += '<td '+e[5]+'><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+(v_e[e[1]]/1000).toFixed(3)+'"></td>';
                                        else content += '<td '+e[5]+'><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?e[4]:v_e[e[1]])+'"></td>';
                                    });
                                    content += "</tr>";
                                });
                                $("#tbshow").append(content);
                            }
                        };
                        Fast.api.open('logisticsxl/material/store_in_xl/selectArrival',"原材料到货选择",options);
                    }else layer.msg("必须选择所在仓库！");
                    
                })

                $('#tbshow').on('keyup','td input[name="table_row[SID_Count][]"]',function () {
                    var bz = $(this).parents('tr').find("input[name='table_row[dz][]']").val().trim();
                    bz = bz==''?0:parseFloat(bz);
                    var number = $(this).parents('tr').find("input[name='table_row[SID_Count][]']").val().trim();
                    number = number==''?0:parseInt(number);
                    var weight =0;
                    weight = (bz*number).toFixed(0);
                    $(this).parents('tr').find("input[name='table_row[SID_Weight][]']").val(weight);
                })

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(1).find('input').val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'logisticsxl/material/store_in_xl/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                });

                $(document).on("click", "#author", function () {
                    var num = $("#c-SI_OtherID").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"logisticsxl/material/store_in_xl/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#c-SI_OtherID").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"logisticsxl/material/store_in_xl/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }

            }
        }
    };
    return Controller;
});