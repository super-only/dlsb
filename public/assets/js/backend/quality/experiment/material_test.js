define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: [],
                height: document.body.clientHeight-600,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: '', formatter: function(value,row,index){return ++index;}},
                        {field: 'MTD_TestNo', title: '试验编号'},
                        {field: 'IM_Class', title: '试样名称'},
                        {field: 'CT_Num', title: '委托单编码'},
                        {field: 'CTD_Pi', title: '进货批次'},
                        {field: 'IM_Spec', title: '规格'},
                        {field: 'L_Name', title: '材质'},
                        {field: 'MGN_Length', title: '长度(m)'},
                        {field: 'LuPiHao', title: '炉号'},
                        {field: 'PiHao', title: '批号'},
                        {field: 'MTD_C', title: 'C(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(2):0;}},
                        {field: 'MTD_Si', title: 'Si(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(2):0;}},
                        {field: 'MTD_Mn', title: 'Mn(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(2):0;}},
                        {field: 'MTD_P', title: 'P(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_S', title: 'S(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_V', title: 'V(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_Nb', title: 'Nb(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_Ti', title: 'Ti(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_Cr', title: 'Cr(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_Czsl', title: '层状撕裂'},
                        {field: 'MTD_Rm', title: '抗拉强度(MPa)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Rel', title: '屈服强度(MPa)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Percent', title: '伸长率(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Temperature', title: '冲击温度(℃)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Cjf', title: '冲击1', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Cjs', title: '冲击2', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Cjt', title: '冲击3', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Cj_ave', title: '冲击均值', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(1):0;}},
                        {field: 'MTD_Wq', title: '弯曲试验'},
                        {field: 'MTD_Wg', title: '尺寸外观'},
                        {field: 'MTD_ChDate', title: '原材料出厂日期'},
                        {field: 'MTD_Conclusion', title: '结论'},
                    ]
                ]
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/experiment/material_test/index' + location.search,
                    add_url: 'quality/experiment/material_test/add',
                    edit_url: 'quality/experiment/material_test/edit',
                    del_url: 'quality/experiment/material_test/del',
                    multi_url: 'quality/experiment/material_test/multi',
                    import_url: 'quality/experiment/material_test/import',
                    table: 'materialtest',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'MT_ID',
                sortName: 'MT_WriteDate',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                height: 500,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'MT_ID', title: __('Mt_id'), operate: false, visible: false},
                        {field: 'MT_Num', title: __('Mt_num'), operate: 'LIKE'},
                        {field: 'DD_Name', title: __('Dd_name'), operate: 'LIKE'},
                        {field: 'MT_Mader', title: __('Mt_mader'), operate: 'LIKE'},
                        {field: 'MT_Writer', title: __('Mt_writer'), operate: 'LIKE'},
                        {field: 'MT_WriteDate', title: __('Mt_writedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'MT_Auditor', title: __('Mt_auditor'), operate: 'LIKE'},
                        {field: 'MT_AuditDate', title: __('Mt_auditdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'is_check', title: '状态', searchList:{1:"未审核",2:"已审核"}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var MT_Num = $element.MT_Num;
                if(MT_Num == '') return false;
                $.ajax({
                    url: 'quality/experiment/material_test/requisitionDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:MT_Num},
                    success: function (ret) {
                        var content = [];
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            if(!Config.flag) $("input").attr("readonly","readonly");
            
            $(document).on('click', "#allowApi", function(e){
                $.ajax({
                    url: 'quality/experiment/material_test/allowApi',
                    type: 'post',
                    async: false,
                    dataType: 'json',
                    data: {num: Config.ids},
                    success: function (ret) {
                        Layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            })
        },
        choosesupplier: function () {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: [],
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CTD_ID', title: 'CTD_ID', operate: 'LIKE',visible: false},
                        {field: 'CT_Num', title: '委试号', operate: 'LIKE'},
                        {field: 'CTD_testnum', title: '试验编号', operate: 'LIKE'},
                        {field: 'Memo', title: '备注'},
                        {field: 'CT_Date', title: '申请时间', operate: 'LIKE'},
                        {field: 'V_Num', title: '供应商名称', operate: 'LIKE',visible: false},
                        {field: 'V_Name', title: '供应商名称', operate: 'LIKE'},
                        // {field: 'MTD_Mader', title: '生产厂家', operate: 'LIKE'},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'CTD_Pi', title: '进货批次', operate: 'LIKE'},
                        {field: 'L_Name', title: '材质', operate: 'LIKE'},
                        {field: 'CTD_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'MGN_Length', title: '长度(mm)', operate: 'LIKE'},
                        {field: 'CTD_Count', title: '试验数量', operate: 'LIKE'},
                        {field: 'LuPiHao', title: '炉号', operate: 'LIKE'},
                        {field: 'PiHao', title: '批号', operate: 'LIKE'},
                        {field: 'QualityNum', title: '质量证明书号', operate: 'LIKE'},
                        {field: 'CTD_Weight', title: '重量', operate: 'LIKE'},
                        {field: 'CTD_UnqualityWeight', title: '不合格重量', operate: 'LIKE'},
                        {field: 'MN_Date', title: '到货时间', operate: 'LIKE'},
                        {field: 'CT_Standard', title: '测定标准', operate: 'LIKE'},
                        {field: 'CTD_Project', title: '试验项目', operate: 'LIKE'},
                        {field: 'MGN_Destination', title: '所在仓库', operate: 'LIKE'},
                        {field: 'Writer', title: '申请人', operate: 'LIKE'},
                        {field: 'CTD_Memo', title: '说明', operate: 'LIKE'}
                    ]
                ]
            });

            getSupplierMaterial(Config.v_name);

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on('click', "#sureSearch", function(env){
                var formContent = $("#edit-form").serializeArray();
                var arrcopy = {};
                $.each(formContent,function(index,e){
                    arrcopy[e.name] = e.value;
                });
                $('.panel-heading ul').find("li:eq(0)>a").trigger("click");
                getSupplierMaterial(Config.v_name,arrcopy);
            })
            function getSupplierMaterial(v_name="",data={}){
                $.ajax({
                    url: 'quality/experiment/material_test/chooseSupplier/v_name/'+v_name,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    async: false,
                    success: function (ret) {
                        var content = [];
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        table.bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            }
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        choosewarranty: function (){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/experiment/material_test/chooseWarranty/luhao/'+Config.luhao + location.search,
                    table: 'recheck',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'writer',
                singleSelect: true,
                // queryParamsType: 'limit',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false},
                        {field: 'LuPiHao', title: __('Lupihao'), operate: 'LIKE', defaultValue:Config.luhao},
                        {field: 'MTD_C', title: __('Mtd_c'), operate:false},
                        {field: 'MTD_Si', title: __('Mtd_si'), operate:false},
                        {field: 'MTD_Mn', title: __('Mtd_mn'), operate:false},
                        {field: 'MTD_P', title: __('Mtd_p'), operate:false},
                        {field: 'MTD_S', title: __('Mtd_s'), operate:false},
                        {field: 'MTD_V', title: __('Mtd_v'), operate:false},
                        {field: 'MTD_Nb', title: __('Mtd_nb'), operate:false},
                        {field: 'MTD_Ti', title: __('Mtd_ti'), operate:false},
                        {field: 'MTD_Cr', title: __('Mtd_cr'), operate:false},
                        {field: 'MTD_Czsl', title: __('Mtd_czsl'), operate: false},
                        {field: 'MTD_Rm', title: __('Mtd_rm'), operate:false},
                        {field: 'MTD_Rel', title: __('Mtd_rel'), operate:false},
                        {field: 'MTD_Percent', title: __('Mtd_percent'), operate:false},
                        {field: 'MTD_Temperature', title: __('Mtd_temperature'), operate:false},
                        {field: 'MTD_Cjf', title: __('Mtd_cjf'), operate:false},
                        {field: 'MTD_Cjs', title: __('Mtd_cjs'), operate:false},
                        {field: 'MTD_Cjt', title: __('Mtd_cjt'), operate:false},
                        {field: 'MTD_Cj_ave', title: '冲击平均', operate:false, formatter: function (value,row,index) {
                            return ((parseFloat(row["MTD_Cjf"])+parseFloat(row["MTD_Cjs"])+parseFloat(row["MTD_Cjt"]))/3).toFixed(3);
                        }},
                        {field: 'MTD_Wq', title: '弯曲试验', operate: false},
                        {field: 'MTD_Wg', title: '尺寸外观', operate: false},
                        {field: 'MTD_Conclusion', title: '结论', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        layer.msg(ret.msg);
                        window.location.reload();
                    }
                    return false;
                });
                $("form table").parent("div").height(document.body.clientHeight-200);
                $(document).on("click", "#export", function () {
                    window.open('/admin.php/quality/experiment/material_test/export/ids/' + Config.ids);
                });
                $(document).on('click', "#chooseManu", function(env){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#MT_MaderTwo").val(value.V_ShortName);
                        }
                    };
                    Fast.api.open('chain/material/material_note/chooseVendor',"生产厂家选择",options);
                })
                $(document).on('click', "#chooseSupplier", function(env){
                    var MT_Mader = $("#MT_Mader").val();
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var tableField = Config.tableField;
                            // var content = "";
                            var supplierName = (typeof(value[0]["V_Name"]) == 'undefined'?"":value[0]["V_Name"]);
                            var CT_Standard = (typeof(value[0]["CT_Standard"]) == 'undefined'?"":value[0]["CT_Standard"]);
                            $.each(value,function(v_index,v_e){
                                if(v_e['V_Name'] != supplierName){
                                    layer.msg("供应商有误！");
                                    return false;
                                }
                                var content = '<tr data-lname="'+v_e['L_Name']+'" data-lupihao="'+v_e['LuPiHao']+'"><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a><a href="javascript:;" class="btn btn-xs btn-info copy"><i class="fa fa-eercast"></i></a></td>';
                                $.each(tableField,function(t_index,e){
                                    if(e[2]=="select") content += '<td '+e[5]+'><select class="form-control small_input" name="table_row['+e[1]+'][]"><option value="合格">合格</option><option value="不合格">不合格</option></select></td>';
                                    else if(e[2]=="datetimerange") content += '<td '+e[5]+'><input data-date-format="YYYY-MM-DD HH:mm:ss" class="small_input datetimepicker" data-use-current="true" name="table_row['+e[1]+'][]" type="text" value="'+Config.time+'"></td>';
                                    else content += '<td '+e[5]+'><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?e[4]:v_e[e[1]])+'"></td>';
                                });
                                content += "</tr>";
                                
                                if($('tr[data-lname="'+v_e['L_Name']+'"][data-lupihao="'+v_e['LuPiHao']+'"]').length != 0) $('tr[data-lname="'+v_e['L_Name']+'"][data-lupihao="'+v_e['LuPiHao']+'"]').last().after(content);
                                else $("#tbshow").append(content);
                            });
                            // $("#tbshow").append(content);
                            $("#MT_Mader").val(supplierName);
                            // $("#MT_Standard").selectpicker("val",CT_Standard);
                            // $("#MT_Standard").selectpicker("val",CT_Standard);
                            // $("#MT_Standard").find("option[value='"+CT_Standard+"']").attr("selected",true);
                            // $("#QualityNum").find("option[value='"+CT_Standard+"']").attr("selected",true);
                        }
                    };
                    Fast.api.open('quality/experiment/material_test/chooseSupplier/v_name/'+MT_Mader,"理化试验申请单选择框",options);
                })
                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(1).find('input').val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'quality/experiment/material_test/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                });
                

                $(document).on('click', ".copy", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var this_content = $(this).parents('tbody').find('tr').eq(indexChoose)

                    var luhao = this_content.find('td input[name="table_row[LuPiHao][]"]').val();
                    var pihao = this_content.find('td input[name="table_row[PiHao][]"]').val();
                    if(luhao){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                for(let i in value){
                                    if(i.substring(0,4)=="MTD_"){
                                        if(this_content.find('td input[name="table_row['+i+'][]"]').length==1) this_content.find('td input[name="table_row['+i+'][]"]').val(value[i]);
                                        if(this_content.find('td select[name="table_row['+i+'][]"]').length==1) this_content.find('td select[name="table_row['+i+'][]"]').val(value[i]);
                                    }
                                }
                            }
                        };
                        Fast.api.open('quality/experiment/material_test/chooseWarranty/luhao/'+luhao,"选择复检数据",options);

                        // $.ajax({
                        //     url: 'quality/experiment/material_test/copyField',
                        //     type: 'post',
                        //     dataType: 'json',
                        //     data: {luhao: luhao,pihao: pihao},
                        //     success: function (ret) {
                        //         Layer.msg(ret.msg);
                        //         if(ret.code==1){
                        //             let obj = ret.data;
                        //             for(let i in obj){
                        //                 console.log(obj[i]);
                        //                 if(i.substring(0,4)=="MTD_"){
                        //                     if(this_content.find('td input[name="table_row['+i+'][]"]').length==1) this_content.find('td input[name="table_row['+i+'][]"]').val(obj[i]);
                        //                     if(this_content.find('td select[name="table_row['+i+'][]"]').length==1) this_content.find('td select[name="table_row['+i+'][]"]').val(obj[i]);
                        //                 }
                        //             }
                        //         }
                                
                        //     }, error: function (e) {
                        //         Backend.api.toastr.error(e.message);
                        //     }
                        // });
                    }else layer.msg("请填写炉号");
                });
                
                $(document).on("click", "#author", function () {
                    var num = $("#MT_ID").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"quality/experiment/material_test/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#MT_ID").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"quality/experiment/material_test/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
            }
        }
    };
    return Controller;
});