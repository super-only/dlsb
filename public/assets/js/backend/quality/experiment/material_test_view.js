define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/experiment/material_test_view/index' + location.search,
                    table: 'material_test_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'MTD_ID',
                sortName: 'MT_WriteDate',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {field: 'id', title: '', operate: false, formatter: function(value,row,index){return ++index;}},
                        {field: 'MTD_TestNo', title: '试验编号'},
                        {field: 'IM_Class', title: '试样名称'},
                        {field: 'CT_Num', title: '委托单编码'},
                        {field: 'MTD_Mader', title: '生产厂家'},
                        {field: 'CTD_QualityNo', title: '质保书编号'},
                        {field: 'CTD_QualityNo_Url', title: '质保书URL',operate: false,formatter: Table.api.formatter.url},
                        {field: 'IM_Spec', title: '规格'},
                        {field: 'L_Name', title: '材质'},
                        {field: 'MGN_Length', title: '长度'},
                        {field: 'LuPiHao', title: '炉号'},
                        {field: 'PiHao', title: '批号'},
                        {field: 'MTD_C', title: 'C(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(2):0;}},
                        {field: 'MTD_Si', title: 'Si(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(2):0;}},
                        {field: 'MTD_Mn', title: 'Mn(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(2):0;}},
                        {field: 'MTD_P', title: 'P(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_S', title: 'S(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_V', title: 'V(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_Nb', title: 'Nb(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_Ti', title: 'Ti(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_Cr', title: 'Cr(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(3):0;}},
                        {field: 'MTD_Czsl', title: '层状撕裂'},
                        {field: 'MTD_Rm', title: '抗拉强度(MPa)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Rel', title: '屈服强度(MPa)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Percent', title: '伸长率(%)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Temperature', title: '冲击温度(℃)', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Cjf', title: '冲击1', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Cjs', title: '冲击2', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Cjt', title: '冲击3', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(0):0;}},
                        {field: 'MTD_Cj_ave', title: '冲击均值', formatter: function(value,row,index){return value?(parseFloat)(value).toFixed(1):0;}},
                        {field: 'MTD_Wq', title: '弯曲试验'},
                        {field: 'MTD_Wg', title: '尺寸外观'},
                        {field: 'MTD_ChDate', title: '原材料出厂日期'},
                        {field: 'MTD_Conclusion', title: '结论'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});