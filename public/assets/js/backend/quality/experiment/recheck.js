define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/experiment/recheck/index' + location.search,
                    add_url: 'quality/experiment/recheck/add',
                    edit_url: 'quality/experiment/recheck/edit',
                    del_url: 'quality/experiment/recheck/del',
                    multi_url: 'quality/experiment/recheck/multi',
                    import_url: 'quality/experiment/recheck/import',
                    table: 'recheck',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false},
                        {field: 'LuPiHao', title: __('Lupihao'), operate: 'LIKE'},
                        // {field: 'PiHao', title: __('Pihao'), operate: 'LIKE'},
                        {field: 'MTD_C', title: __('Mtd_c'), operate:false},
                        {field: 'MTD_Si', title: __('Mtd_si'), operate:false},
                        {field: 'MTD_Mn', title: __('Mtd_mn'), operate:false},
                        {field: 'MTD_P', title: __('Mtd_p'), operate:false},
                        {field: 'MTD_S', title: __('Mtd_s'), operate:false},
                        {field: 'MTD_V', title: __('Mtd_v'), operate:false},
                        {field: 'MTD_Nb', title: __('Mtd_nb'), operate:false},
                        {field: 'MTD_Ti', title: __('Mtd_ti'), operate:false},
                        {field: 'MTD_Cr', title: __('Mtd_cr'), operate:false},
                        {field: 'MTD_Czsl', title: __('Mtd_czsl'), operate: false},
                        {field: 'MTD_Rm', title: __('Mtd_rm'), operate:false},
                        {field: 'MTD_Rel', title: __('Mtd_rel'), operate:false},
                        {field: 'MTD_Percent', title: __('Mtd_percent'), operate:false},
                        {field: 'MTD_Temperature', title: __('Mtd_temperature'), operate:false},
                        {field: 'MTD_Cjf', title: __('Mtd_cjf'), operate:false},
                        {field: 'MTD_Cjs', title: __('Mtd_cjs'), operate:false},
                        {field: 'MTD_Cjt', title: __('Mtd_cjt'), operate:false},
                        {field: 'MTD_Cj_ave', title: '冲击平均', operate:false, formatter: function (value,row,index) {
                            return ((parseFloat(row["MTD_Cjf"])+parseFloat(row["MTD_Cjs"])+parseFloat(row["MTD_Cjt"]))/3).toFixed(1);
                        }},
                        {field: 'MTD_Wq', title: '弯曲试验', operate: false},
                        {field: 'MTD_Wg', title: '尺寸外观', operate: false},
                        {field: 'MTD_Conclusion', title: '结论', operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            if(!Config.flag) $("input").attr("readonly","readonly");
            $(document).on("click", "#author", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"quality/experiment/recheck/auditor",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('确定弃审？',"quality/experiment/recheck/giveUp",num);
                
            });
            function check(msg,url,num){
                // console.log(url,msg,num);return false;
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                window.location.reload();
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        choosegp: function (){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/experiment/recheck/chooseGp' + location.search,
                    table: 'dev_guangpu',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'IDATETIME',
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),formatter: function(value,row,index){
                            return ++index;
                        }},
                        {field: 'FileName', title: '文件名', operate: 'LIKE'},
                        {field: 'IDATETIME', title: '时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'INAME', title: '名称', operate: 'LIKE'},
                        {field: 'cz', title: '材质', operate: false},
                        {field: 'gg', title: '规格', operate: false},
                        {field: 'lph', title: '炉批号', operate: false},
                        {field: 'sccj', title: '生产厂家', operate: false},
                        {field: 'IFIXNAME', title: '类型标量', operate: 'LIKE'},
                        {field: 'C', title: __('Mtd_c'), operate:false},
                        {field: 'Si', title: __('Mtd_si'), operate:false},
                        {field: 'Mn', title: __('Mtd_mn'), operate:false},
                        {field: 'P', title: __('Mtd_p'), operate:false},
                        {field: 'S', title: __('Mtd_s'), operate:false},
                        {field: 'V', title: __('Mtd_v'), operate:false},
                        {field: 'Nb', title: __('Mtd_nb'), operate:false},
                        {field: 'Ti', title: __('Mtd_ti'), operate:false},
                        {field: 'Cr', title: __('Mtd_cr'), operate:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        chooseqd: function (){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/experiment/recheck/chooseQd' + location.search,
                    table: 'dev_lali',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'IDATE',
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),formatter: function(value,row,index){
                            return ++index;
                        }},
                        {field: 'Num', title: '编号', operate: 'LIKE'},
                        {field: 'IBATCH', title: '名称', operate: 'LIKE'},
                        {field: 'CAIZHI', title: '材质', operate: 'LIKE'},
                        {field: 'GUIGE', title: '规格', operate: 'LIKE'},
                        {field: 'IDATE', title: '时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'SQFQD', title: '上屈服强度', operate:false},
                        {field: 'KLQD', title: '抗拉强度', operate:false},
                        {field: 'LASHENLV', title: '拉伸率', operate:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        choosecj: function (){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/experiment/recheck/chooseCj' + location.search,
                    table: 'dev_lali',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'Date',
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),formatter: function(value,row,index){
                            return ++index;
                        }},
                        {field: 'BATCH', title: '名称', operate: 'LIKE'},
                        {field: 'Temperature', title: '温度', operate: false},
                        {field: 'Cjf', title: '冲击1', operate: false},
                        {field: 'Cjs', title: '冲击2', operate: false},
                        {field: 'Cjt', title: '冲击3', operate: false},
                        {field: 'Date', title: '时间', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        choosemain: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/experiment/recheck/chooseMain' + location.search,
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'CTD_QYDate',
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),formatter: function(value,row,index){
                            return ++index;
                        }},
                        {field: 'LuPiHao', title: '炉批号', operate: 'LIKE'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "edit"){
                        window.location.reload();
                    }
                    
                });
                $(document).on('click', "#gp", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var field = ['C','Si','Mn','P','S','V','Nb','Ti','Cr'];
                            field.forEach(function(i,index){
                                $("#c-MTD_"+i).val(value[i]);
                            })
                        }
                    };
                    Fast.api.open('quality/experiment/recheck/chooseGp',"光谱信息选择",options);
                });
                $(document).on('click', "#qd", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#c-MTD_Rm").val(value["SQFQD"]);
                            $("#c-MTD_Rel").val(value["KLQD"]);
                            $("#c-MTD_Percent").val(value["LASHENLV"]);
                        }
                    };
                    Fast.api.open('quality/experiment/recheck/chooseQd',"拉力信息选择",options);
                });
                $(document).on('click', "#cj", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var field = ['Temperature','Cjf','Cjs','Cjt'];
                            field.forEach(function(i,index){
                                $("#c-MTD_"+i).val(value[i]);
                            })
                        }
                    };
                    Fast.api.open('quality/experiment/recheck/chooseCj',"冲击信息选择",options);
                });
                $(document).on('click', "#main_news", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["40%","100%"],
                        callback:function(value){
                            $("#c-LuPiHao").val(value.LuPiHao);
                        }
                    };
                    Fast.api.open('quality/experiment/recheck/chooseMain',"选择炉批号",options);
                });
            }
        }
    };
    return Controller;
});