define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'scsygcdata'], function ($, undefined, Backend, Table, Form) {
    function col_index(div_content = "#tbshow"){
        var col = 0,dh=0,zw=0,zk=0;
        $(div_content).find("tr").each(function () {
            col++;
            $(this).children('td').eq(0).text(col);
            if($(this).children('td').find("input[name='table_row[fire][]']").val() != 0) dh++;
            if($(this).children('td').find("input[name='table_row[bending][]']").val() != 0) zw++;
            if($(this).children('td').find("input[name='table_row[kong][]']").val() != 0) zk++;
        });
        var sum = col*2+dh+zw+zk;
        $("#part_c").text(col);
        $("#part_m").text(sum);
        $("#dh_c").text(dh);
        $("#zw_c").text(zw);
        $("#zk_c").text(zk);
    }
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/process/pro_process_main/index' + location.search,
                    add_url: 'quality/process/pro_process_main/add',
                    edit_url: 'quality/process/pro_process_main/edit',
                    del_url: 'quality/process/pro_process_main/del',
                    multi_url: 'quality/process/pro_process_main/multi',
                    import_url: 'quality/process/pro_process_main/import',
                    table: 'pro_process_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false},
                        {field: 'PT_Num', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: false},
                        {field: 'auditor_time', title: __('Auditor_time'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            if(!Config.flag) $("input").attr("readonly","readonly");
            col_index();
            $(document).on('click', "#settingSpec", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["60%","70%"]
                };
                Fast.api.open('quality/process/pro_process_main/settingSpec',"设置镀锌后重量系数",options);
            })
            $(document).on("click", "#updateWg", function () {
                $.ajax({
                    url: 'quality/process/pro_process_main/allowApi',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {num: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#galvanized", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["90%","90%"]
                };
                Fast.api.open('quality/process/galvanized/index/ids/'+Config.ids,"镀锌温度",options);
            });
            let list = Config.list;
            let tb_tmp = [];
            for (const key in list) {
                if (Object.hasOwnProperty.call(list, key)) {
                    const ele = list[key];
                    tb_tmp.push({
                        parts: ele['parts'],
                        stuff: ele['stuff'],
                        material: ele['material'],
                        specification: ele['specification'],
                        length: ele['length'],
                        xl_count: ele['xl_count'],
                        xl_type: ele['xl_type'],
                        xl_creat_time: ele['xl_creat_time'].substring(0,10),
                        xl_hg_count: ele['xl_hg_count'],
                        zk_count: ele['zk_count'],
                        zk_kong: ele['zk_kong'],
                        zk_sum_hole: ele['zk_sum_hole'],
                        zw_count: ele['zw_count'],
                        zw_bending: ele['zw_bending'],
                        hj_fire: ele['hj_fire'],
                        hj_processor: ele['hj_processor'],
                        dx_sum_weight: ele['dx_sum_weight'],
                        dx_late_weight: ele['dx_late_weight'],
                        dx_gal_time: ele['dx_gal_time'].substring(0,10)
                    })
                }
            }          
            
            let data_tmp={
                tb:tb_tmp
            };
            $(document).on("click", "#toPdf", function () {
                hiprint.init();
                //初始化模板
                let htemp =  new hiprint.PrintTemplate({template: scsygcdata});
                htemp.toPdf(data_tmp,Config.row["PT_Num"]);
            });

            $(document).on("click", "#toSave", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["500px","300px"],
                };
                Fast.api.open('quality/process/pro_process_main/partImport/ids/'+Config.ids,"上传pdf",options);
            });
        },
        partimport: function() {
            Form.api.bindevent($("form[role=form]"));
        },
        choosedetail: function () {
            var pt_num = Config.pt_num;
            var detailField = Config.getTableField;
            var show = $("#show");
            var rightTable = $("#rightTable");
            var height = document.body.clientHeight/2;
            var topDtMd = [];
            show.parents("div").css("height",height);
            // 初始化表格
            rightTable.bootstrapTable({
                data:[],
                height: height,
                rowStyle: function (row, index) {
                    var style = {};
                    if(row.flag){
                        style = {css:{'background':'#87CEEB'}};
                    }
                    return style;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pt_num', title: '生产下达单号'},
                        {field: 'DtMD_ID_PK', title: 'DtMD_ID_PK', visible: false},
                        {field: 'sect', title: '段名'},
                        {field: 'parts', title: '部件号',visible: false},
                        {field: 'parts_Input', title: '部件号'},
                        {field: 'stuff', title: '材料'},
                        {field: 'material', title: '材质'},
                        {field: 'specification', title: '规格'},
                        {field: 'length', title: '长度(mm)'},
                        {field: 'width', title: '宽度(mm)'},
                        {field: 'type', title: '类型',sortable:true},
                        {field: 'count', title: '数量'},
                        {field: 'weight', title: '单重'},
                        {field: 'sum_weight', title: '总重'},
                        {field: 'hole_number', title: '单件孔数'},
                        {field: 'sum_hole', title: '总孔数'},
                        {field: 'fire', title: '焊接',sortable:true},
                        {field: 'bending', title: '制弯',sortable:true},
                        {field: 'kong', title: '制孔',sortable:true},
                    ]
                ]
            });

            ajax_copy_td({PT_Sect:0,DtMD_sSpecification:0});
            $(document).on('click', "#searchMaterial", function(e){
                var search = {};
                var PT_Sect =  $("#PT_Sect").val();
                var DtMD_sSpecification =  $("#DtMD_sSpecification").val();
                search.PT_Sect = PT_Sect;
                search.DtMD_sSpecification = DtMD_sSpecification;
                ajax_copy_td(search);
            });

            $(document).on('click', "#sure", function(e){
                var tableObject = $("#detail-form").serializeArray();
                var content;
                var tableArray = [];
                var num = detailField[0].length;
                var count = tableObject.length/num;
                for(var j=0;j<count;j++){
                    var first_array = {};
                    
                    for(var i=0;i<num;i++){
                        var js = j*num+i;
                        content = tableObject[js];
                        first_array[content.name]=content.value;
                    }
                    tableArray.push(first_array);
                }
                
                if(tableArray.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableArray);
            });
            function ajax_copy_td(search){
                $.ajax({
                    url: "quality/process/pro_process_main/chooseDetail/pt_num/"+pt_num,
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: search,
                    success: function (ret) {
                        var tableHtml = [];
                        if(ret.code==1) tableHtml = ret.data;
                        rightTable.bootstrapTable('load',tableHtml);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            $(document).on('keyup', "#daihao", function(e){
                var val = $("#daihao").val();
                rightTable.bootstrapTable("uncheckAll");
                if(val) $('#rightTable tbody tr').hide().find("td input[value^='" +(val) + "']").parents("tr").show();
                else $('#rightTable tbody tr').show();
                var chooseId = $("#rightTable tbody tr:visible:first").data("index");
                rightTable.bootstrapTable("check",chooseId);
                if(e.keyCode == 13){
                    var content = rightTable.bootstrapTable("getAllSelections");
                    if(content.length==0){
                        layer.msg("未选中！")
                        return false;
                    }
                    show_append(content);
                    // show.bootstrapTable("append",content);
                    $('#rightTable tbody tr').show();
                    rightTable.bootstrapTable("uncheckAll");
                    rightTable.bootstrapTable("check",0);
                    $("#daihao").val("");
                }
            });
            $(document).on('click', ".del", function(e){
                $( e.target ).closest("tr").remove();
            });

            $(document).on('keyup', "#rightTable", function(event){
                if (event.keyCode == 13) {
                    var content = rightTable.bootstrapTable("getAllSelections");
                    if(content.length==0){
                        layer.msg("未选中！")
                        return false;
                    }
                    rightTable.bootstrapTable("uncheckAll");
                    show_append(content);
                    // show.bootstrapTable("append",content);
                }
            });
            function show_append(content){
                var tableContent = "";
                if(content.length != 0){
                    $.each(content,function(index_c,e_c){
                        if($.inArray(e_c["DtMD_ID_PK"],topDtMd)==-1){
                            topDtMd.push(e_c["DtMD_ID_PK"]);
                            tableContent += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                            $.each(detailField[0],function(index_d,e_d){
                                tableContent += '<td '+e_d[5]+'><input type="text" class="small_input" '+e_d[3]+' name="'+e_d[1]+'" value="'+(e_c[e_d[1]]==undefined?'':e_c[e_d[1]])+'"></td>';
                            });
                            tableContent += '</tr>';
                        }
                    });
                }
                $("#left_tbshow").append(tableContent);
                var left_height = $("#show").height();
                $("#de_height").scrollTop(left_height);
            }
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                // parent.window.$(".layui-layer-iframe").find(".layui-layer-close").on('click',function () {        
                //     parent.window.reload(true);
                // }); 
                var getTableField = Config.getTableField;
                var dtmdList = Config.dtmdList;
                $("form table").parent("div").height(document.body.clientHeight-150);
                $(document).on('click', "#selectPtNum", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback: function (value){
                            $("#c-PT_Num").val(value.PT_Num);
                        }
                    };
                    Fast.api.open('chain/material/nesting/xddh',"选择下达单号",options);
                })

                $(document).on('click', '#chooseMaterial', function (e){
                    var ids = Config.ids;
                    if(!ids){
                        layer.msg("请先保存主信息");
                        return false;
                    }
                    var pt_num = $("#c-PT_Num").val();
                    if(!pt_num){
                        layer.msg("请选择生产单号");
                        return false;
                    }else{
                        var url = "quality/process/pro_process_main/chooseDetail/pt_num/"+pt_num;
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                if(value.length == 0) return false;
                                var tableHtml = "";
                                $.each(value,function(index,e){
                                    if($.inArray(e["DtMD_ID_PK"],dtmdList)==-1){
                                        dtmdList.push(e["DtMD_ID_PK"]);
                                        tableHtml += '<tr><td></td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                        $.each(getTableField[1],function(tindex,te){
                                            if(te[2]=="button"){
                                                tableHtml += '<td '+te[5]+'>';
                                                if(e["fire"]!=0) tableHtml += '<a href="javascript:;" class="btn btn-xs btn-info peo"><i class="fa fa-user"></i></a>';
                                                tableHtml += '</td>';
                                            }else if(te[2]=="time"){
                                                tableHtml += '<td '+te[5]+'><input class="small_input datetimepicker" '+te[3]+' data-date-format="YYYY-MM-DD HH:mm:ss" data-use-current="true" name="table_row['+te[1]+'][]" type="text" value="'+ te[6]+'"></input></td>';
                                            }else if(te[2] == "fire"){
                                                tableHtml += '<td '+te[5]+'>';
                                                if(e["fire"]==0) tableHtml += '<input class="small_input" type="text" readonly placeholder="/" name="table_row['+te[1]+'][]" value="">';
                                                else tableHtml += '<input type="text" class="small_input" '+te[3]+' name="table_row['+te[1]+'][]" value="'+(e[te[4]]==undefined?'':e[te[4]])+'">';
                                                tableHtml += '</td>';
                                            }else{
                                                tableHtml += '<td '+te[5]+'><input type="text" class="small_input" '+te[3]+' name="table_row['+te[1]+'][]" value="'+(e[te[4]]==undefined?te[6]:e[te[4]])+'"></td>';
                                            }
                                        })
                                        tableHtml += '</tr>';
                                    }
                                    
                                });
                                $("#tbshow").append(tableHtml);
                                col_index();
                                $(".datetimepicker").datetimepicker();
                            }
                        };
                        Fast.api.open(url,"选择明细",options);
                    }
                })

                $(document).on('click', ".peo", function(e){
                    var process_input = $(this).parents('tr').find("input[name='table_row[processor][]']");
                    var pro_ce_input = $(this).parents('tr').find("input[name='table_row[certificate][]']");
                    var url = "jichu/jg/employee/selectCrif/field/E_hjCerif";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            process_input.val(value.E_Name);
                            process_input.text(value.E_Name);
                            pro_ce_input.val(value.E_hjCerif);
                            pro_ce_input.text(value.E_hjCerif);
                        }
                    };
                    Fast.api.open(url,"选择员工",options);
                })

                $(document).on('click', ".del", function(e){
                    // var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tr').find("input[name='table_row[id][]']").val();
                    var dtmId = $(this).parents('tr').find("input[name='table_row[DtMD_ID_PK][]']").val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'quality/process/pro_process_main/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {"num": num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                        col_index();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                        col_index();
                    }
                    dtmdList = $.grep(dtmdList, function( n ) {
                        return ( n != dtmId );
                    });
                });

                $(document).on("click", "#author", function () {
                    var num = Config.ids;
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"quality/process/pro_process_main/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = Config.ids;
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"quality/process/pro_process_main/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }

            }
        }
    };
    return Controller;
});