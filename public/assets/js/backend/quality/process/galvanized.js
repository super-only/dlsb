define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/process/galvanized/index/ids/' + Config.ppm_id,
                    del_url: 'quality/process/galvanized/del',
                    table: 'galvanized_true',
                }
            });

            var table = $("#table");
            //修改顶部弹窗大小
            $(".btn-add").data("area",["80%","80%"]);
            $(".btn-edit").data("area",["80%","80%"]);
            table.on('post-body.bs.table',function(){
                $(".btn-editone").data("area",["80%","80%"]);
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'time',
                sortOrder: 'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', operate: false, formatter: function (value,row,index) { return ++index;}},
                        // {field: 'temp_id', title: '温度序号', operate: false, visible: false},
                        {field: 'temp', title: '温度', operate: false},
                        {field: 'time', title: '时间', operate: 'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on('click', ".btn-iniadd", function(e){
                var url = "quality/process/galvanized/selectTemp";
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        let detailContent = [];
                        let detailObject = {};
                        $.each(value,function(index,e){
                            detailObject = {
                                "id": 0,
                                "temp": e["wd"],
                                "time": e["sj"]
                            };
                            detailContent.push(detailObject);
                        });
                        table.bootstrapTable('append',detailContent);
                    }
                };
                Fast.api.open(url,"选择镀锌温度",options);
            })
            $(document).on('click', ".btn-save", function(e){
                var url = "quality/process/galvanized/add/ppm_id/"+ Config.ppm_id;
                var data = table.bootstrapTable('getSelections');
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    data: {data:data},
                    success: function (ret) {
                        if (ret.code == 1) {
                            window.location.reload();
                        }else{
                            Layer.msg(ret.msg);
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            })
        },
        add: function () {
            Controller.api.bindevent();
        },
        selecttemp: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'quality/process/galvanized/selectTemp' + location.search,
                    table: 'dev_duxin',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'sj',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', operate: false},
                        {field: 'wd', title: '温度', operate: false},
                        {field: 'sj', title: '时间', operate: 'LIKE'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".btn-sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});