define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logistics/invoice/pay_list/index' + location.search,
                    // add_url: 'logistics/invoice/pay_list/add',
                    // edit_url: 'logistics/invoice/pay_list/edit',
                    // del_url: 'logistics/invoice/pay_list/del',
                    // multi_url: 'logistics/invoice/pay_list/multi',
                    // import_url: 'logistics/invoice/pay_list/import',
                    table: 'pay_list',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pay_num',
                sortName: 'pay_num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pay_num', title: __('Pay_num'), operate: 'LIKE'},
                        {field: 'pay_app_num', title: __('Pay_app_num'), operate: 'LIKE'},
                        {field: 'date', title: __('Date')},
                        {field: 'amount', title: __('Amount'), operate: 'LIKE'},
                        {field: 'currency', title: __('Currency'), operate: 'LIKE'},
                        {field: 'rate', title: __('Rate'), operate:'BETWEEN'},
                        {field: 'account', title: __('Account'), operate: 'LIKE'},
                        {field: 'bill_number', title: __('Bill_number'), operate: 'LIKE'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        // {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        selectpaylist: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logistics/invoice/pay_list/selectPayList/pay_app_num/'+Config.pay_app_num + location.search,
                    add_url: 'logistics/invoice/pay_list/add/pay_app_num/'+Config.pay_app_num,
                    edit_url: 'logistics/invoice/pay_list/edit',
                    del_url: 'logistics/invoice/pay_list/del',
                    // multi_url: 'logistics/invoice/pay_list/multi',
                    // import_url: 'logistics/invoice/pay_list/import',
                    table: 'pay_list',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pay_num',
                sortName: 'pay_num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pay_num', title: __('Pay_num'), operate: 'LIKE'},
                        {field: 'pay_app_num', title: __('Pay_app_num'), operate: 'LIKE'},
                        {field: 'date', title: __('Date')},
                        {field: 'amount', title: __('Amount'), operate: 'LIKE'},
                        {field: 'currency', title: __('Currency'), operate: 'LIKE'},
                        {field: 'rate', title: __('Rate'), operate:'BETWEEN'},
                        {field: 'account', title: __('Account'), operate: 'LIKE'},
                        {field: 'bill_number', title: __('Bill_number'), operate: 'LIKE'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});