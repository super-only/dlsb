define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    $.arrayIntersect = function(a, b)
    {
        return $.merge($.grep(a, function(i)
            {
                return $.inArray(i, b) == -1;
            }) , $.grep(b, function(i)
            {
                return $.inArray(i, a) == -1;
            })
        );
    };
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logistics/invoice/invoice_main/index' + location.search,
                    add_url: 'logistics/invoice/invoice_main/add',
                    edit_url: 'logistics/invoice/invoice_main/edit',
                    del_url: 'logistics/invoice/invoice_main/del',
                    auditor_url: 'logistics/invoice/invoice_main/auditor',
                    giveup_url: 'logistics/invoice/invoice_main/giveUp',
                    multi_url: 'logistics/invoice/invoice_main/multi',
                    import_url: 'logistics/invoice/invoice_main/import',
                    table: 'invoice_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'invoice_num',
                sortName: 'writer_time',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'invoice_num', title: __('Invoice_num'), operate: 'LIKE'},
                        {field: 'invoice_no', title: __('Invoice_no'), operate: 'LIKE'},
                        {field: 'business_type', title: __('Business_type'),searchList: Config.businessType, formatter:function(value){
                            return Config.businessType[value];
                        }},
                        {field: 'type', title: __('Type'),searchList: Config.invioceList, formatter:function(value){
                            return Config.invioceList[value];
                        }},
                        {field: 'kp_date', title: __('Kp_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'supplier', title: __('Supplier'),searchList: Config.vendorList, formatter:function(value){
                            return Config.vendorList[value];
                        }},
                        {field: 'procurement_type', title: __('Procurement_type')},
                        {field: 'fax', title: __('Fax'), operate:'BETWEEN'},
                        {field: 'department', title: __('Department'),searchList: Config.deptList, formatter:function(value){
                            return Config.deptList[value];
                        }},
                        {field: 'salesman', title: __('Salesman'), operate: 'LIKE'},
                        {field: 'currency', title: __('Currency'),searchList: Config.bzList, formatter:function(value){
                            return Config.bzList[value];
                        }},
                        {field: 'rate', title: __('Rate'), operate:'BETWEEN'},
                        {field: 'fp_date', title: __('Fp_date')},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        // {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        selectstorein: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logistics/invoice/invoice_main/selectStorein/v_num/'+Config.v_num+'/type/'+Config.type + location.search,
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SID_ID',
                sortName: 'SID_ID',
                search: false,
                // clickToSelect: true,
                // singleSelect: true,
                showToggle: false,
                // onlyInfoPagination: true,
                height: document.body.clientHeight,
                // showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'SID_ID', title: "SID_ID", operate: false},
                        {field: 'SI_OtherID', title: '入库编号', operate: "LIKE"},
                        {field: 'SI_InDate', title: '入库日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'SI_WareHouse', title: '所在仓库', operate: 'LIKE'},
                        {field: 'IM_Num', title: '存货编码', operate: false},
                        {field: 'PC_ProjectName', title: '工程名称', operate: 'LIKE'},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'sid.L_Name', title: '材质', operate: 'LIKE',visible: false},
                        {field: 'L_Name', title: '材质', operate: false},
                        {field: 'IM_Spec', title: '规格', operate: '='},
                        {field: 'IM_Measurement', title: '单位', operate: '='},
                        {field: 'SID_Weight', title: '重量', operate:false},
                        {field: 'SID_Length', title: '长度', operate:'=1000'},
                        {field: 'SID_Width', title: '宽度', operate:false},
                        {field: 'SID_Count', title: '数量', operate:false},
                        // {field: 'SID_FactWeight', title: '', operate:false},
                        // // {field: 'SID_PlanPrice', title: __('Sid_planprice'), operate:false},
                        // // {field: 'SID_NoTaxPrice', title: __('Sid_notaxprice'), operate:false},
                        // {field: 'SID_RestCount', title: __('Sid_restcount'), operate:false},
                        // {field: 'SID_RestWeight', title: __('Sid_restweight'), operate:false},
                        // {field: 'SID_TestResult', title: __('Sid_testresult'), operate: 'LIKE'},
                        // {field: 'mgn_DuiFangDian', title: '堆放点', operate: 'LIKE'},
                        {field: 'LuPiHao', title: "炉批号", operate: 'LIKE'},
                        // {field: 'PiHao', title: __('Pihao'), operate: false},
                        {field: 'V_Name', title: '供应商', operate: 'LIKE'},
                        {field: 'RSC_ID', title: '入库方式', searchList: Config.rsclist}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent=table.bootstrapTable('getAllSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        selectinvoice: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logistics/invoice/invoice_main/selectinvoice/v_num/'+Config.v_num+'/type/' +Config.type + location.search,
                    table: 'invoice_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'invoice_num',
                sortName: 'writer_time',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'invoice_num', title: __('Invoice_num'), operate: 'LIKE'},
                        {field: 'invoice_no', title: __('Invoice_no'), operate: 'LIKE'},
                        {field: 'business_type', title: __('Business_type'),searchList: Config.businessType, formatter:function(value){
                            return Config.businessType[value];
                        }},
                        {field: 'type', title: __('Type'),searchList: Config.invioceList, formatter:function(value){
                            return Config.invioceList[value];
                        }},
                        {field: 'kp_date', title: __('Kp_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'supplier', title: __('Supplier'),searchList: Config.vendorList, formatter:function(value){
                            return Config.vendorList[value];
                        }},
                        {field: 'procurement_type', title: __('Procurement_type')},
                        {field: 'fax', title: __('Fax'), operate:'BETWEEN'},
                        {field: 'department', title: __('Department'),searchList: Config.deptList, formatter:function(value){
                            return Config.deptList[value];
                        }},
                        {field: 'salesman', title: __('Salesman'), operate: 'LIKE'},
                        {field: 'currency', title: __('Currency'), operate: 'LIKE'},
                        {field: 'amount', title: '金额',operate: false},
                        {field: 'rate', title: __('Rate'), operate:'BETWEEN'},
                        {field: 'fp_date', title: __('Fp_date')},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent=table.bootstrapTable('getAllSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                let table_height = document.body.clientHeight-300-$(".part_one").height();
                $(".table-responsive").height(table_height);
                // var tableSidList = Config.tableSidList??[];
                $('#c-procurement_type').change(function(){
                    $("#tbshow").html("");
                });
                $(document).on('click', ".overtext", function(e){
                    let field = $(this).data("table");
                    //c-unit c-salesman c-qg
                    if(field == "c-supplier"){
                        let v_num = $("#c-supplier_num").val();
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                let change_v_num = value.V_Num;
                                if(v_num!='' && v_num!=change_v_num){
                                    var index = layer.confirm("修改供应商会导致表格内容清空，确定修改吗？", {
                                        btn: ['确定', '取消'],
                                    }, function(data) {
                                        $("#c-supplier_num").val(change_v_num);
                                        $("#c-supplier").val(value.V_Name);
                                        layer.close(index);
                                    })
                                }else{
                                    $("#c-supplier_num").val(change_v_num);
                                    $("#c-supplier").val(value.V_Name);
                                }
                            }
                        };
                        Fast.api.open('chain/material/material_note/chooseVendor',"对方单位选择",options);
                    }
                    if(field == "c-salesman"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                $("#c-salesman").val(value.E_Name);
                            }
                        };
                        Fast.api.open("chain/sale/project_cate_log/selectSaleMan","业务员选择",options);
                    }
                });
                $(document).on('click', "#syncMaterial", function(e){
                    let v_num = $("#c-supplier_num").val();
                    let type = $("#c-procurement_type").val();
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            let existValue = [];
                            let endValue = [];
                            $.each($('input[name="table[sid_id][]"]'),function(s_index,s_v){
                                existValue.push($(s_v).val());
                            });
                            $.each(value,function(v_index,v_e){
                                if($.inArray(v_e["SID_ID"]+'',existValue)===-1){
                                    endValue.push(v_e);
                                }
                            });
                            $.ajax({
                                url: 'logistics/invoice/invoice_main/addMaterial',
                                type: 'post',
                                dataType: 'json',
                                data: {data:JSON.stringify(endValue)},
                                success: function (ret) {
                                    var content = '';
                                    if(ret.code==1){
                                        content = ret.data["field"];
                                    }
                                    $("#tbshow").append(content);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            })
                        }
                    };
                    Fast.api.open('logistics/invoice/invoice_main/selectStorein/v_num/'+v_num+'/type/'+type,"选择材料",options);
                });
                $(document).on('keyup','td input[name="table[amount][]"]',function () {
                    let index = $(this).parents('tr').index();
                    jisuan(index);
                })
                $(document).on('change','td select[name="table[way][]"]',function () {
                    let index = $(this).parents('tr').index();
                    jisuan(index);
                })
                $(document).on('keyup','#c-fax',function () {
                    $.each($("#tbshow").find("tr"),function(t_index,t_value){
                        jisuan(t_index);
                    })
                    
                })
                function jisuan(index){
                    let rate = $("#c-fax").val();
                    rate = rate?(parseFloat)(rate)*0.01:0;
                    // let index = object.parents('tr').index();
                    let body = $("#tbshow").find("tr").eq(index);
                    let money = body.find('td input[name="table[amount][]"]').val();
                    var way = body.find('td select[name="table[way][]"]').val();
                    if(way==1) var count = body.find('td input[name="table[weight][]"]').val().trim();
                    else var count = body.find('td input[name="table[count][]"]').val().trim();
                    money = money==''?0:(parseFloat)(money);
                    count = count==''?0:(parseFloat)(count);
                    let fax_money = (parseFloat)(money*rate).toFixed(2);
                    let sum_money = (parseFloat)(money)+(parseFloat)(fax_money);
                    let price = (parseFloat)(count?money/count:0).toFixed(5);
                    let fax_price = (parseFloat)(count?sum_money/count:0).toFixed(5);
                    body.find('td input[name="table[price][]"]').val(price);
                    body.find('td input[name="table[fax_price][]"]').val(fax_price);
                    body.find('td input[name="table[fax_amount][]"]').val(fax_money);
                    body.find('td input[name="table[sum_amount][]"]').val(sum_money);
                }
                $(document).on('click', ".del", function(e){
                    $( e.target ).closest("tr").remove();
                });
                $(document).on("click", "#auditor", function () {
                    if(Config.flag) return false;
                    check('审核之后无法修改，确定审核？',"logistics/invoice/invoice_main/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    if(!Config.flag) return false;
                    check('确定弃审？',"logistics/invoice/invoice_main/giveUp",Config.ids);
                    
                });
                function check(msg,url,num){
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {ids: num},
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    Layer.msg(ret.msg);
                                    if (ret.code === 1) {
                                        window.location.reload();
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                };
            }
        }
    };
    return Controller;
});