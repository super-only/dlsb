define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logistics/invoice/pay_list/index' + location.search,
                    // add_url: 'logistics/invoice/pay_list/add',
                    // edit_url: 'logistics/invoice/pay_list/edit',
                    // del_url: 'logistics/invoice/pay_list/del',
                    // multi_url: 'logistics/invoice/pay_list/multi',
                    // import_url: 'logistics/invoice/pay_list/import',
                    table: 'pay_list_wj',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'supplier',
                sortName: 'supplier',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ID', title: '序号', operate: false, formatter:function(value,row,index){
                            return ++index;
                        }},
                        {field: 'supplier', title: "供应商",searchList: Config.vendorList, formatter:function(value){
                            return Config.vendorList[value];
                        }},
                        {field: 'need_pay', title: '需要支付金额', operate: false },
                        {field: 'pay', title: '已付金额', operate: false },
                        {field: 'sy_pay', title: '剩余未付金额', operate: false },
                        {
                            field: 'buttons',
                            // width: "120px",
                            title: '已付详情',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    text: '详情',
                                    title: '详情',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'logistics/invoice/pay_list/detail'
                                }
                            ],
                            formatter: Table.api.formatter.buttons
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logistics/invoice/pay_list/detail' + location.search,
                    table: 'pay_list_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'pay_time',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ID', title: '序号', operate: false, formatter:function(value,row,index){
                            return ++index;
                        }},
                        {field: 'supplier', title: '供应商', operate: false, formatter:function(value){
                            return Config.vendorList[value];
                        }},
                        {field: 'pay_app_num', title: '申请付款单号', operate: 'LIKE'},
                        // {field: 'invoice_num', title: '发票编号', operate: 'LIKE'},
                        // {field: 'invoice_no', title: '发票号', operate: 'LIKE'},
                        {field: 'pay_amount', title: '已支付金额', operate: false},
                        {field: 'date', title: '支付时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});