define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'logistics/invoice/payment_app_main/index' + location.search,
                    add_url: 'logistics/invoice/payment_app_main/add',
                    edit_url: 'logistics/invoice/payment_app_main/edit',
                    del_url: 'logistics/invoice/payment_app_main/del',
                    auditor_url: 'logistics/invoice/payment_app_main/auditor',
                    giveup_url: 'logistics/invoice/payment_app_main/giveUp',
                    multi_url: 'logistics/invoice/payment_app_main/multi',
                    import_url: 'logistics/invoice/payment_app_main/import',
                    table: 'payment_app_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pay_app_num',
                sortName: 'pay_app_num',
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'status', title: '支付状态', searchList: Config.statusList},
                        {field: 'pay_app_num', title: __('Pay_app_num'), operate: 'LIKE'},
                        {field: 'date', title: __('Date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'amount', title: '申请支付金额', operate: false},
                        {field: 'pay_amount', title: '实际金额', operate: false},
                        {field: 'type', title: __('Type'),searchList: Config.businessType, formatter:function(value){
                            return Config.businessType[value];
                        }},
                        {field: 'supplier', title: __('Supplier'),searchList: Config.vendorList, formatter:function(value){
                            return Config.vendorList[value];
                        }},
                        {field: 'department', title: __('Department'),searchList: Config.deptList, formatter:function(value){
                            return Config.deptList[value];
                        }},
                        {field: 'salesman', title: __('Salesman'), operate: 'LIKE'},
                        {field: 'method', title: __('Method'),searchList: Config.balanceMode, formatter:function(value){
                            return Config.balanceMode[value];
                        }},
                        {field: 'currency', title: __('Currency'),searchList: Config.bzList, formatter:function(value){
                            return Config.bzList[value];
                        }},
                        {field: 'bank', title: __('Bank'), operate: 'LIKE'},
                        {field: 'account', title: __('Account'), operate: 'LIKE'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on('click', ".overtext", function(e){
                    let field = $(this).data("table");
                    //c-unit c-salesman c-qg
                    if(field == "c-supplier"){
                        let v_num = $("#c-supplier_num").val();
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                let change_v_num = value.V_Num;
                                if(v_num!='' && v_num!=change_v_num){
                                    var index = layer.confirm("修改供应商会导致表格内容清空，确定修改吗？", {
                                        btn: ['确定', '取消'],
                                    }, function(data) {
                                        $("#c-supplier_num").val(change_v_num);
                                        $("#c-supplier").val(value.V_Name);
                                        layer.close(index);
                                    })
                                }else{
                                    $("#c-supplier_num").val(change_v_num);
                                    $("#c-supplier").val(value.V_Name);
                                }
                            }
                        };
                        Fast.api.open('chain/material/material_note/chooseVendor',"供应商选择",options);
                    }
                    if(field == "c-salesman"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                $("#c-salesman").val(value.E_Name);
                            }
                        };
                        Fast.api.open("chain/sale/project_cate_log/selectSaleMan","业务员选择",options);
                    }
                });
                $(document).on('click', "#syncCom,#syncStore", function(e){
                    let v_num = $("#c-supplier_num").val();
                    if(!v_num){
                        layer.msg("请先选择供应商");
                        return false;
                    }
                    let idName = $(this).attr("id");
                    let url = '';
                    if(idName=="syncCom") url = 'chain/purchase/procurement_main/selectprocurement/v_num/'+v_num+'/type/0';
                    else url = 'logistics/invoice/invoice_main/selectinvoice/v_num/'+v_num+'/type/0';
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            let existValue = [];
                            let endValue = [];
                            let keyString = '';
                            let keyEndString = '';
                            $.each($('#tbshow'),function(s_index,s_v){
                                keyString = $(s_v).find("input[name='table[pm_num][]']").val()+$(s_v).find("input[name='table[invoice_num][]']").val();
                                existValue.push(keyString);
                            });
                            $.each(value,function(v_index,v_e){
                                keyEndString = (v_e["pm_num"]??'')+(v_e["invoice_num"]??'');
                                if($.inArray(keyEndString,existValue)===-1){
                                    endValue.push(v_e);
                                }
                            });
                            $.ajax({
                                url: 'logistics/invoice/payment_app_main/addMaterial',
                                type: 'post',
                                dataType: 'json',
                                data: {data:JSON.stringify(endValue)},
                                success: function (ret) {
                                    var content = '';
                                    if(ret.code==1){
                                        content = ret.data["field"];
                                    }
                                    $("#tbshow").append(content);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            })
                        }
                    };
                    Fast.api.open(url,"选择材料",options);
                });
                $(document).on('click', "#table_add", function(){
                    $.ajax({
                        url: 'logistics/invoice/payment_app_main/addMaterial',
                        type: 'post',
                        dataType: 'json',
                        data: {data:JSON.stringify([{amount:0}])},
                        success: function (ret) {
                            var content = '';
                            if(ret.code==1){
                                content = ret.data["field"];
                            }
                            $("#tbshow").append(content);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    })
                    $("#tbshow").append(field_append);
                });
                $(document).on('click', ".del", function(e){
                    $( e.target ).closest("tr").remove();
                });
                $(document).on("click", "#auditor", function () {
                    if(Config.flag) return false;
                    check('审核之后无法修改，确定审核？',"logistics/invoice/payment_app_main/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    if(!Config.flag) return false;
                    check('确定弃审？',"logistics/invoice/payment_app_main/giveUp",Config.ids);
                    
                });
                function check(msg,url,num){
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {ids: num},
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    Layer.msg(ret.msg);
                                    if (ret.code === 1) {
                                        window.location.reload();
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                };
            }
        }
    };
    return Controller;
});