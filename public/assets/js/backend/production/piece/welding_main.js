define(['jquery', 'bootstrap', 'backend', 'table', 'form','input-tag'], function ($, undefined, Backend, Table, Form, InputTag) {
    function sumOperate(id='')
    {
        var sum_list = {"sum_weight":0,"hole_number":0};
        var list = $('#left_'+id+'_table').bootstrapTable('getSelections');
        $.each(list, function (index,value){
            $.each(sum_list, function (sum_index,sum_value){
                sum_list[sum_index]+=(parseFloat)(value[sum_index]);
            })  
        })
        let string = "重量(吨)："+(sum_list.sum_weight/1000).toFixed(5)+" 孔数："+(sum_list.hole_number).toFixed(2);
        $("#"+id+" p").text(string);
    }
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'production/piece/welding_main/index' + location.search,
                    add_url: 'production/piece/welding_main/add',
                    edit_url: 'production/piece/welding_main/edit',
                    del_url: 'production/piece/welding_main/del',
                    auditor_url: 'production/piece/welding_main/auditor',
                    giveup_url: 'production/piece/welding_main/giveUp',
                    multi_url: 'production/piece/welding_main/multi',
                    import_url: 'production/piece/welding_main/import',
                    table: 'welding_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'record_time', title: __('Record_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'pt_num', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            
            var jt_left_table;
            var left_table_height = document.body.clientHeight-270;
            // var weld_tpl = 
            // $.get("common/weld.html", function(data) {
            //     // var template = $(data);
            //     console.log(data);
            //   });
            // return false;
            var keyList = Config.keyList??[];
            var init_data = Config.init_data??[];
            var tagObj1 = InputTag.render({
                elem: '.c-piece_main_id',
                data: init_data,//初始值
                number: init_data,
                onNumberChange: function (data, value, type) {
                    $('#c-piece_main_id').val(JSON.stringify(data));
                }
            });
            $("#update_weld").on("click",function(){
                var record_time = $("#c-record_time").val();
                if(!record_time) return false;
                tagObj1.clearData();
                $("#tab_tower ul").html('');
                $("#myTabContent").html('');
                Fast.api.ajax({
                    url: "production/piece/welding_main/getPieceMain",
                    type: "post",
                    async: false,
                    data: {record_time: record_time},
                }, function (data, ret) {
                    if(ret.code==1){
                        tagObj1.clearData();
                        $('#c-piece_main_id').val(JSON.stringify(data.idList));
                        tagObj1.init({
                            elem: '.c-piece_main_id',
                            data: data.idList,//初始值
                            number: data.idList
                        });
                        $.each(data.data,function(di,dv){
                            if($.inArray(di,keyList)===-1){
                                keyList.push(di);
                                $("#tab_tower ul").append('<li class=""><a href="#'+di+'" data-toggle="tab">'+di+'<i class="close-tab fa fa-remove"></i></a></li>');
                                var table_content = '<div class="tab-pane fade" id="'+di+'">'
                                            + '<div class="form-group col-xs-12 col-sm-4 leftPart">'
                                            + '<div class="form-group col-xs-12 col-sm-12 sum_weight_hole">'
                                            + '<h3>'+data.ptNumList[di.substring(di.indexOf('_')+1)]+'</h3>'
                                            + '<p>重量(吨)：0 孔数：0</p>'
                                            +'</div><div class="form-group col-xs-12 col-sm-12">'
                                            + '<table id="left_'+di+'_table" class="table table-striped table-bordered table-hover table-nowrap" width="100%"></table>'
                                            + '</div></div>'
                                            + '<div class="form-group col-xs-12 col-sm-8 rightPart">'
                                            + '<input id="c-id_pt_num" class="form-control" name="row[id_pt_num][]" type="hidden" value="'+di+'">'
                                            + '<fieldset>'
                                                + '<legend>气割</legend>'
                                                + '<div class="form-group col-xs-12 col-sm-12">'
                                                    + '<label class="control-label col-xs-12 col-sm-2">钢管Ф159以上(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        +'<input id="c-gas_up" min="0" class="form-control" name="row[gas_up][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">钢管Ф159以下(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-gas_down" min="0" class="form-control" name="row[gas_down][]" type="number">'
                                                    + '</div>'
                                                + '</div>'
                                                + '<div class="form-group col-xs-12 col-sm-12">'
                                                    + '<label class="control-label col-xs-12 col-sm-2">钢板(法兰板) 14mm-30mm (吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-gas_plate_range" min="0" class="form-control" name="row[gas_plate_range][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">钢板(法兰板) 30mm及以上 (吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-gas_plate_up" min="0" class="form-control" name="row[gas_plate_up][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">钢板(法兰板) 30mm及以上 (吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-gas_steel" min="0" class="form-control" name="row[gas_steel][]" type="number">'
                                                    + '</div>'
                                                + '</div>'
                                            + '</fieldset>'
                                            + '<fieldset>'
                                                + '<legend>组对</legend>'
                                                + '<div class="form-group col-xs-12 col-sm-12">'
                                                    + '<label class="control-label col-xs-12 col-sm-2">特殊角钢组对 (吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-team_special" min="0" class="form-control" name="row[team_special][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">塔脚(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-team_foot" min="0" class="form-control" name="row[team_foot][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">板件组对 20mm以下(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-team_plate_down" min="0" class="form-control" name="row[team_plate_down][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">板件组对 20mm及以上(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-team_plate_up" min="0" class="form-control" name="row[team_plate_up][]" type="number">'
                                                    + '</div>'
                                                + '</div>'
                                                + '<div class="form-group col-xs-12 col-sm-12">'
                                                    + '<label class="control-label col-xs-12 col-sm-2">铁附件(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-team_iron" min="0" class="form-control" name="row[team_iron][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">500kv以上塔脚(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-team_foot_up" min="0" class="form-control" name="row[team_foot_up][]" type="number">'
                                                    + '</div>'
                                                + '</div>'
                                            + '</fieldset>'
                                            + '<fieldset>'
                                                + '<legend>电焊</legend>'
                                                + '<div class="form-group col-xs-12 col-sm-12">'
                                                    + '<label class="control-label col-xs-12 col-sm-2">110KV及以下塔脚 30mm底板以下 (只):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_floor_down" min="0" class="form-control" name="row[weld_floor_down][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">110KV及以下塔脚 30mm底板及以上(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_floor_up" min="0" class="form-control" name="row[weld_floor_up][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">220KV及以上塔脚(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_floor" min="0" class="form-control" name="row[weld_floor][]" type="number">'
                                                    + '</div>'
                                                + '</div>'
                                                + '<div class="form-group col-xs-12 col-sm-12">'
                                                    + '<label class="control-label col-xs-12 col-sm-2">横担 长度900(支):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_cross_low" min="0" class="form-control" name="row[weld_cross_low][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">横担 长度1500(支):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_cross_medium" min="0" class="form-control" name="row[weld_cross_medium][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">横担 长度1900(支):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_corss_long" min="0" class="form-control" name="row[weld_corss_long][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">地脚螺栓每组焊接(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_anchor_bolt" min="0" class="form-control" name="row[weld_anchor_bolt][]" type="number">'
                                                    + '</div>'
                                                + '</div>'
                                                + '<div class="form-group col-xs-12 col-sm-12">'
                                                    + '<label class="control-label col-xs-12 col-sm-2">地脚螺栓底板加强筋焊接 M30-M36(支):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_bolt_medium" min="0" class="form-control" name="row[weld_bolt_medium][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">地脚螺栓底板加强筋焊接 M42以上(支):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_bolt_up" min="0" class="form-control" name="row[weld_bolt_up][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">板件焊接 20mm以下(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_plate_down" min="0" class="form-control" name="row[weld_plate_down][]" type="number">'
                                                    + '</div>'
                                                + '</div>'
                                                + '<div class="form-group col-xs-12 col-sm-12">'
                                                    + '<label class="control-label col-xs-12 col-sm-2">板件焊接 20mm及以上(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_plate_up" min="0" class="form-control" name="row[weld_plate_up][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">铁附件、特殊角钢(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_special" min="0" class="form-control" name="row[weld_special][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">角钢焊(板件)加强角 200以下(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_strengthen_down" min="0" class="form-control" name="row[weld_strengthen_down][]" type="number">'
                                                    + '</div>'
                                                    + '<label class="control-label col-xs-12 col-sm-2">角钢焊(板件)加强角 200及以上(吨):</label>'
                                                    + '<div class="col-xs-12 col-sm-1">'
                                                        + '<input id="c-weld_strengthen_up" min="0" class="form-control" name="row[weld_strengthen_up][]" type="number">'
                                                    + '</div>'
                                                + '</div>'
                                            + '</fieldset></div></div>';
                                $("#myTabContent").append(table_content);
                                jt_left_table = $('#left_'+di+'_table');
                                // 初始化表格
                                jt_left_table.bootstrapTable({
                                    data:dv,
                                    // uniqueId: "id",
                                    height: left_table_height,
                                    columns: [
                                        [
                                            {checkbox: true},
                                            {field: 'pt_num', title: '生产下达单号'},
                                            {field: 'stuff', title: '材料'},
                                            {field: 'specification', title: '规格'},
                                            {field: 'material', title: '材质'},
                                            {field: 'sum_weight', title: '重量'},
                                            {field: 'hole_number', title: '孔数'}
                                        ]
                                    ]
                                });
                                // 为表格绑定事件
                                Table.api.bindevent(jt_left_table);
                                $('a[href="#'+di+'"]').trigger("click");

                                jt_left_table.on('check.bs.table', function (e, data) {
                                    sumOperate(di);
                                });
                                jt_left_table.on('uncheck.bs.table', function (e, data) {
                                    sumOperate(di);
                                });
                                jt_left_table.on('check-all.bs.table', function (e, data) {
                                    sumOperate(di);
                                });
                                jt_left_table.on('uncheck-all.bs.table', function (e, data) {
                                    sumOperate(di);
                                });
                                jt_left_table.on('click-row.bs.table', function (row, $element) {
                                    var index = $element.idfield;
                                    if($element[0] === true) jt_left_table.bootstrapTable('uncheck', index);
                                    else jt_left_table.bootstrapTable('check', index);
                                    sumOperate(di);
                                });
                            }
                        });
                    }
                })
            });
        },
        edit: function () {
            Controller.api.bindevent();
            var left_table_height = document.body.clientHeight-270;
            var table = $('#left_table_table');
            // 初始化表格
            table.bootstrapTable({
                data:Config.list,
                // uniqueId: "id",
                height: left_table_height,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pt_num', title: '生产下达单号'},
                        {field: 'stuff', title: '材料'},
                        {field: 'specification', title: '规格'},
                        {field: 'material', title: '材质'},
                        {field: 'sum_weight', title: '重量'},
                        {field: 'hole_number', title: '孔数'}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);

            table.on('check.bs.table', function (e, data) {
                sumOperate('table');
            });
            table.on('uncheck.bs.table', function (e, data) {
                sumOperate('table');
            });
            table.on('check-all.bs.table', function (e, data) {
                sumOperate('table');
            });
            table.on('uncheck-all.bs.table', function (e, data) {
                sumOperate('table');
            });

            table.on('click-row.bs.table', function (row, $element) {
                var index = $element.idfield;
                if($element[0] === true) table.bootstrapTable('uncheck', index);
                else table.bootstrapTable('check', index);
                sumOperate('table');
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});