define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'production/piece/piece_summary/index' + location.search,
                    add_url: 'production/piece/piece_summary/add',
                    edit_url: 'production/piece/piece_summary/edit',
                    del_url: 'production/piece/piece_summary/del',
                    multi_url: 'production/piece/piece_summary/multi',
                    import_url: 'production/piece/piece_summary/import',
                    table: 'piece_summary',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false},
                        {field: 'j_num', title: __('J_num')},
                        {field: 'record_time', title: __('Record_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: function(value,row,index){return value.substring(0,7);}},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'remark', title: __('Remark'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.tableList,
                height: document.body.clientHeight-100,
                idField: "id",
                uniqueId: "id",
                columns: [
                    [
                        {field: 'id', title: __('Id'), visible: false},
                        {field: 'detail', title: "详情", table: table, events: Table.api.events.operate, formatter: Table.api.formatter.buttons,
                        buttons:[
                            {
                                name: '详情',
                                text: '',
                                title: '详情',
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'production/piece/piece_summary/detail/d_id/{id}',
                                callback: function (data) {
                                    let news = table.bootstrapTable('getRowByUniqueId',data.id);
                                    news["amount"] = data.amount;
                                    news["total_amount"] = data.total_amount;
                                    table.bootstrapTable('updateRow', {
                                        index: news["index"],
                                        row: news
                                    });
                                },
                            }
                        ]},
                        {field: 'operator', title: "操作人员"},
                        {field: 'weight', title: "总计量"},
                        {field: 'hole', title: "总孔数"},
                        {field: 'amount', title: "理论总工资"},
                        {field: 'total_amount', title: "实际总工资"},
                        {field: 'remark', title: "备注", formatter: function(value){ return value?value:"-";}}
                    ]
                ],
                onClickCell: function(field, value, row, $element) {
                    if(field=="total_amount" || field=="remark"){
                        $element.attr('contenteditable', true);
                        $element.blur(function() {
                            let index = $element.parent().data('index');
                            let tdValue = $element.html();
                            saveData(index, field, tdValue);
                        })
                    }
                }
                // onPostBody: function(){
                //     let page_value = localStorage.getItem("piece_page")??0;
                //     page_value = (parseFloat)(page_value);
                //     console.log(1);
                //     if(page_value) table.bootstrapTable("scrollTo",123);
                // }
            });
            function saveData(index, field, value) {
                table.bootstrapTable('updateCell', {
                    index: index,
                    field: field,
                    value: value
                });
                return false;
            }

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on('click', "#stick", function(){
                Fast.api.ajax({
                    url: "production/piece/piece_summary/summary",
                    type: "post",
                    async: false,
                    data: {ids: Config.ids},
                }, function (data, ret) {
                    table.bootstrapTable('load', data);
                });
            });
            $(document).on('click', "#del", function(){
                Fast.api.ajax({
                    url: "production/piece/piece_summary/del",
                    type: "post",
                    async: false,
                    data: {ids: Config.ids},
                }, function (data, ret) {
                    Fast.api.close();
                });
            });
            $(document).on('click', "#author", function(){
                Fast.api.ajax({
                    url: "production/piece/piece_summary/auditor",
                    type: "post",
                    async: false,
                    data: {ids: Config.ids},
                }, function (data, ret) {
                    window.location.reload();
                });
            });
            $(document).on('click', "#create", function(){
                Fast.api.ajax({
                    url: "production/piece/piece_summary/create",
                    type: "post",
                    async: false,
                    data: {ids: Config.ids},
                });
            });
            $(document).on('click', "#give", function(){
                Fast.api.ajax({
                    url: "production/piece/piece_summary/giveUp",
                    type: "post",
                    async: false,
                    data: {ids: Config.ids},
                }, function (data, ret) {
                    window.location.reload();
                });
            });
            // parent.window.$(".layui-layer-iframe").find(".layui-layer-close").on('click',function () {        
            //     localStorage.removeItem('piece_page');
            // });
        },
        detail: function (){
            // Controller.api.bindevent("detail");
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'production/piece/piece_summary/detail/d_id/'+Config.d_id + location.search,
                }
            });
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                height: document.body.clientHeight-100,
                pagination: true,
                onlyInfoPagination: true,
                onLoadSuccess: function (data) {
                    $(".update_div").find("div:eq(2)").find("span").text((data['extend'].amount));
                    $(".update_div").find("div:eq(3)").find("span").text((data['extend'].total_amount));
                },
                columns: [
                    [
                        {field: 'id', title: "ID", operate:false},
                        {field: 'jj_time', title: "时间", operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'device', title: "机器",searchList: Config.machineList, formatter: function(value){ return Config.machineList[value]??'';}},
                        {field: 'workmanship', title: "工序",searchList: Config.orderList, formatter: function(value){ return Config.orderList[value]??'';}},
                        {field: 'stuff', title: "材料", operate:false},
                        {field: 'specification', title: "规格", operate:'='},
                        {field: 'material', title: "材质", operate:false},
                        {field: 'pt_num', title: "生产下达单号", operate:'='},
                        {field: 'pt_number', title: "基数", operate:false},
                        {field: 'pressure', title: "电压", operate:false, formatter: function(value){ return value?value+"KV":"";}},
                        // {field: 'n_type', title: "类型", formatter: function(value){ return Config.ntypeList[value];}},
                        {field: 'n_value', title: "量", operate:false},
                        {field: 'price', title: "单价", operate:false},
                        {field: 'amount', title: "工资", operate:false}
                    ]
                ],
                onClickCell: function(field, value, row, $element) {
                    if(field=='price'){
                        $element.attr('contenteditable', true);
                        $element.attr('style', "display:block");
                        $element.blur(function() {
                            let index = row.id;
                            let tdValue = $element.html();
                            if(row.price != tdValue && index) saveData(index, field, tdValue);
                        })
                    }
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on('click', "#summary", function(){
                let operator_name = $("form").find("div:eq(0)").find("div:eq(0)").text();
                var url = "production/piece/piece_summary/detailSummary/ids/"+Config.d_id;
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["90%","90%"]
                };
                Fast.api.open(url,operator_name+"工资汇总",options);
            });
            function saveData(index, field, value) {
                $.ajax({
                    url: "production/piece/piece_summary/savePeopleDetail",
                    type: 'post',
                    data: {
                        id: index,
                        field: field,
                        value: value
                    },
                    dataType: 'json',
                    async: false,
                    success: function (ret) {
                        table.bootstrapTable('refresh',{silent: true});
                        
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
                return false;
            }
            parent.window.$(".layui-layer-iframe").find(".layui-layer-close").on('click',function () {    
                let amount = $(".update_div").find("div:eq(2)").find("span").text();
                let total_amount = $(".update_div").find("div:eq(3)").find("span").text();
                Fast.api.close({amount:amount,total_amount:total_amount,id:Config.d_id});
            }); 
        },
        detailsummary: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'production/piece/piece_summary/detailSummary/ids/'+Config.ids + location.search,
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'jj_time',
                sortOrder: 'asc',
                pagination: true,
                onlyInfoPagination: true,
                height: document.body.clientHeight-20,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'jj_time', title: '时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: function(value,row,index){return value.substring(0,10);}},
                        {field: 'amount', title: '金额', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        Backend.api.close();
                        parent.Backend.api.open('production/piece/piece_summary/edit/ids/'+data, '编辑');
                    }else{
                        window.location.reload();
                    }
                    return false;
                },null,function(success,error){
                    var this_new = this;
                    if(field=="edit"){
                        var content = $("#table").bootstrapTable('getData');
                        $.ajax({
                            url: "production/piece/piece_summary/saveDetail",
                            type: 'post',
                            data: {data: JSON.stringify(content)},
                            dataType: 'json',
                            async: false,
                            success: function (ret) {
                                if(ret.code == 1){
                                    Form.api.submit(this_new,success,error)
                                }else{
                                    layer.msg(ret.msg);
                                }
                                
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }else Form.api.submit(this_new,success,error)
                    return false;
                    // Form.api.submit(this,success,error);
                });
            }
        }
    };
    return Controller;
});