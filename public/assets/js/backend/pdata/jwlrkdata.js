let jwlrkdata = {
	"panels": [{
		"index": 0,
		"height": 125,
		"width": 210,
		"paperHeader": 84,
		"paperFooter": 310.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 24,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司",
				"fontSize": 18,
				"textAlign": "center",
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 43.5,
				"height": 9.75,
				"width": 594,
				"title": "入库单",
				"fontSize": 18,
				"textAlign": "center",
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 76.5,
				"top": 66,
				"height": 9.75,
				"width": 324,
				"field": "ghdw",
				"fontSize": 12,
				"testData": "ghdw",
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 402,
				"top": 66,
				"height": 9.75,
				"width": 63,
				"title": "入库日期:",
				"fontSize": 12,
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 16.5,
				"top": 66,
				"height": 9.75,
				"width": 66,
				"title": "供货单位:",
				"fontSize": 12,
				"fontFamily": "Microsoft YaHei"
				
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 466.5,
				"top": 66,
				"height": 9.75,
				"width": 90,
				"field": "rkrq",
				"fontSize": 12,
				"testData": "rkrq",
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 16.5,
				"top": 84,
				"height": 49.5,
				"width": 556.5,
				"field": "tb",
				"textAlign": "center",
				"fontSize": 12,
				"fontFamily":"Microsoft YaHei",
				"tableHeaderFontSize": 12,
				"tableBodyRowHeight": 19.5,
				"autoCompletion": true,
				"tableHeaderRowHeight": 19.5,
				"columns": [
					[{
						"title": "存货名称",
						"field": "chmc",
						"width": 88.10208391862379,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cpmc"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 144.1820211865381,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ggxh"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 30.427633930600066,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 48.703172179696466,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "单价(元)",
						"field": "dj",
						"width": 78.59673663247516,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl"
					}, {
						"title": "金额(元)",
						"field": "je",
						"width": 81.7206446120816,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}, {
						"title": "备注",
						"field": "bz",
						"width": 84.76770753998477,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 228,
				"top": 318,
				"height": 9.75,
				"width": 34.5,
				"title": "日期:",
				"fontSize": 12,
				"showInPage": "last",
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 318,
				"height": 9.75,
				"width": 120,
				"field": "zdrq",
				"testData": "zdrq",
				"fontSize": 12,
				"showInPage": "last",
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 61.5,
				"top": 318,
				"height": 9.75,
				"width": 120,
				"field": "zdr",
				"fontSize": 12,
				"testData": "zdr",
				"showInPage": "last",
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 421.5,
				"top": 318,
				"height": 9.75,
				"width": 51,
				"title": "经办人：",
				"fontSize": 12,
				"showInPage": "last",
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 318,
				"height": 9.75,
				"width": 43.5,
				"title": "制单人:",
				"fontSize": 12,
				"showInPage": "last",
				"fontFamily": "Microsoft YaHei"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 480,
		"paperNumberTop": 42,
		"paperNumberFormat": "第 paperNo 页，共 paperCount 页"
	}]
}