let qgddata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 82.5,
		"paperFooter": 801,
		"printElements": [{
			"options": {
				"left": 219,
				"top": 36,
				"height": 9.75,
				"width": 120,
				"title": "采购计划",
				"fontSize": 15,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 69,
				"top": 63,
				"height": 9.75,
				"width": 55.5,
				"field": "bh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 406.5,
				"top": 63,
				"height": 9.75,
				"width": 27,
				"title": "日期"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 36,
				"top": 63,
				"height": 9.75,
				"width": 31.5,
				"title": "编号："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 436.5,
				"top": 63,
				"height": 9.75,
				"width": 78,
				"field": "rq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 22.5,
				"top": 82.5,
				"height": 69,
				"width": 550,
				"textAlign": "center",
				"field": "tb",
				"tableBodyRowBorder": "noBorder",
				"tableBodyCellBorder": "noBorder",
				"columns": [
					[{
						"title": "材料名称",
						"field": "clmc",
						"width": 60.010297368273065,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "clmc",
						"formatter": function (value,row) {
							if (row['clmc'] =='合计' || row['clmc'] =='总记录') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else if(row['clmc'] == '回传：' || row['gbj']=='确认人：'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] =='电话：'){
								return "<div style='border-right: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] == '报价说明：' || row['clmc']=='以上报价为：' || row['clmc'] =='采购说明：' || row['clmc'] =='主管签字：' || row['clmc'] =='制单人：'){
								return "<div style='font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "材质",
						"field": "cz",
						"width": 57.94302330269311,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz",
						"formatter": function (value,row) {
							if (row['clmc'] =='合计' || row['clmc'] =='总记录') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else if(row['clmc'] == '回传：' || row['gbj']=='确认人：'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] =='电话：' || row['clmc'] =='报价说明：' || row['clmc'] =='以上报价为：' || row['clmc'] =='采购说明：' || row['clmc'] =='主管签字：' || row['clmc'] =='制单人：'){
								return "<div style='border-right: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "规格",
						"field": "gg",
						"width": 57.962239211831,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg",
						"formatter": function (value,row) {
							if (row['clmc'] =='合计' || row['clmc'] =='总记录') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else if(row['clmc'] == '回传：' || row['gbj']=='确认人：'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] =='电话：' || row['clmc'] =='报价说明：' || row['clmc'] =='以上报价为：' || row['clmc'] =='采购说明：' || row['clmc'] =='主管签字：' || row['clmc'] =='制单人：'){
								return "<div style='border-right: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "长度(m)",
						"field": "cd",
						"width": 57.95477281532189,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl",
						"formatter": function (value,row) {
							if (row['clmc'] =='合计' || row['clmc'] =='总记录') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else if(row['clmc'] == '回传：' || row['gbj']=='确认人：'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] =='电话：' || row['clmc'] =='报价说明：' || row['clmc'] =='以上报价为：' || row['clmc'] =='采购说明：' || row['clmc'] =='主管签字：' || row['clmc'] =='制单人：'){
								return "<div style='border-right: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "数量",
						"field": "sl",
						"width": 57.962073530397284,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl",
						"formatter": function (value,row) {
							if (row['clmc'] =='合计' || row['clmc'] =='总记录') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else if(row['clmc'] == '回传：' || row['gbj']=='确认人：'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] =='电话：' || row['clmc'] =='报价说明：' || row['clmc'] =='以上报价为：' || row['clmc'] =='采购说明：' || row['clmc'] =='主管签字：' || row['clmc'] =='制单人：'){
								return "<div style='border-right: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "重量(kg)",
						"field": "zl",
						"width": 57.9423779826169,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl",
						"formatter": function (value,row) {
							if (row['clmc'] =='合计' || row['clmc'] =='总记录') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else if(row['clmc'] == '回传：' || row['gbj']=='确认人：'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] =='电话：' || row['clmc'] =='报价说明：' || row['clmc'] =='以上报价为：' || row['clmc'] =='采购说明：' || row['clmc'] =='主管签字：' || row['clmc'] =='制单人：'){
								return "<div style='border-right: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "理计价",
						"field": "ljj",
						"width": 58.0159482139432,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ljj",
						"formatter": function (value,row) {
							if (row['clmc'] =='合计' || row['clmc'] =='总记录') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else if(row['ljj']=='供方确认'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] == '回传：' || row['gbj']=='确认人：'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] =='电话：' || row['clmc'] =='报价说明：' || row['clmc'] =='以上报价为：' || row['clmc'] =='采购说明：' || row['clmc'] =='主管签字：' || row['clmc'] =='制单人：'){
								return "<div style='border-right: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "过磅价",
						"field": "gbj",
						"width": 57.966902695032026,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gbj",
						"formatter": function (value,row) {
							if (row['clmc'] =='合计' || row['clmc'] =='总记录') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else if(row['clmc'] == '回传：' || row['gbj']=='确认人：'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] =='电话：' || row['clmc'] =='报价说明：' || row['clmc'] =='以上报价为：' || row['clmc'] =='采购说明：' || row['clmc'] =='主管签字：' || row['clmc'] =='制单人：'){
								return "<div style='border-right: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "备注",
						"field": "bz",
						"width": 84.24236487989157,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz",
						"formatter": function (value,row) {
							if (row['clmc'] =='合计' || row['clmc'] =='总记录') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else if(row['clmc'] == '回传：' || row['gbj']=='确认人：'){
								return "<div style='border: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else if(row['clmc'] =='电话：' || row['clmc'] =='报价说明：' || row['clmc'] =='以上报价为：' || row['clmc'] =='采购说明：' || row['clmc'] =='主管签字：' || row['clmc'] =='制单人：'){
								return "<div style='border-right: none;font-size: 9pt;text-align: right;'>" + value + "</div>";
							}
							else {
								return "<div>" + value + "</div>";
							}
						}
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 189,
				"top": 13.5,
				"height": 9.75,
				"width": 220.5,
				"title": "绍兴电力设备有限公司",
				"fontSize": 18,
				"fontWeight": "bold"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 565,
		"paperNumberTop": 819,
		"paperNumberDisabled":true
	}]
};