let orderbydjhdata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 105,
		"paperFooter": 723,
		"printElements": [{
			"options": {
				"left": 214.5,
				"top": 39,
				"height": 22.5,
				"width": 138,
				"title": "放样原始材料表",
				"fontSize": 18,
				"textDecoration": "underline",
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 229.5,
				"top": 87,
				"height": 12,
				"width": 34.5,
				"field": "dw"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 322.5,
				"top": 87,
				"height": 12,
				"width": 28.5,
				"title": "基数："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 201,
				"top": 87,
				"height": 12,
				"width": 30,
				"title": "段位："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 352.5,
				"top": 87,
				"height": 12,
				"width": 46.5,
				"field": "js"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 87,
				"height": 12,
				"width": 82.5,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 457.5,
				"top": 87,
				"height": 12,
				"width": 30,
				"title": "日期："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 45,
				"top": 87,
				"height": 12,
				"width": 30,
				"title": "塔型："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 487.5,
				"top": 87,
				"height": 12,
				"width": 76.5,
				"field": "rq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 15,
				"top": 105,
				"height": 277.5,
				"width": 564,
				"field": "tb",
				"textAlign": "center",
				"tableFooterRepeat": "page",
				"columns": [
					[{
						"title": "零件编号",
						"field": "ljbh",
						"width": 40.290543242374035,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ljbh"
					}, {
						"title": "材料名称",
						"field": "clmc",
						"width": 27.1625596860557,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "clmc"
					}, {
						"title": "材质",
						"field": "cz",
						"width": 29.752164528256607,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 67.89374638109277,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg"
					}, {
						"title": "长度(mm)",
						"field": "cd",
						"width": 27.19708355363226,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cd"
					}, {
						"title": "宽度(mm)",
						"field": "kd",
						"width": 27.1625596860557,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "kd"
					}, {
						"title": "厚度(mm)",
						"field": "DtMD_iTorch",
						"width": 27,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "DtMD_iTorch"
					}, {
						"title": "类型",
						"field": "type",
						"width": 27,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "type"
					}, {
						"title": "单基数量",
						"field": "djsl",
						"width": 27.354226289678046,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "djsl"
					}, {
						"title": "单基重量",
						"field": "djzl",
						"width": 39.36178667613834,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "djzl"
					}, {
						"title": "孔数",
						"field": "ks",
						"width": 21.878023300024374,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ks"
					}, {
						"title": "电焊",
						"field": "dh",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dh"
					}, {
						"title": "制弯",
						"field": "wq",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "wq"
					}, {
						"title": "切角",
						"field": "qj",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "qj"
					}, {
						"title": "铲背",
						"field": "cb",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cb"
					}, {
						"title": "清根",
						"field": "qg",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "qg"
					}, {
						"title": "打扁",
						"field": "db",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "db"
					}, {
						"title": "开合角",
						"field": "khj",
						"width": 24,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "khj"
					}, {
						"title": "割豁",
						"field": "DtMD_GeHuo",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "DtMD_GeHuo"
					}, {
						"title": "钻孔",
						"field": "zk",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zk"
					}, {
						"title": "备注",
						"field": "bz",
						"width": 59.528674970890196,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 235.5,
				"top": 732,
				"height": 9.75,
				"width": 57,
				"field": "byzsl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 366,
				"top": 732,
				"height": 9.75,
				"width": 58.5,
				"field": "byzzl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 312,
				"top": 732,
				"height": 9.75,
				"width": 49.5,
				"title": "本页总重量"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 732,
				"height": 9.75,
				"width": 49.5,
				"title": "本页总孔数"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 180,
				"top": 732,
				"height": 9.75,
				"width": 51,
				"title": "本页总数量"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 507,
				"top": 732,
				"height": 9.75,
				"width": 55.5,
				"field": "byzks"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 40.5,
				"top": 732,
				"height": 9.75,
				"width": 54,
				"title": "本页小计："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 312,
				"top": 750,
				"height": 9.75,
				"width": 49.5,
				"title": "累计总重量"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 235.5,
				"top": 750,
				"height": 9.75,
				"width": 57,
				"field": "ljzsl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 366,
				"top": 750,
				"height": 9.75,
				"width": 58.5,
				"field": "ljzzl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 180,
				"top": 750,
				"height": 9.75,
				"width": 51,
				"title": "累计总数量"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 750,
				"height": 9.75,
				"width": 49.5,
				"title": "累计总孔数"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 58.5,
				"top": 750,
				"height": 9.75,
				"width": 36,
				"title": "累计："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 507,
				"top": 750,
				"height": 9.75,
				"width": 55.5,
				"field": "ljzks"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 765,
				"height": 9,
				"width": 537
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 267,
				"top": 774,
				"height": 9.75,
				"width": 48,
				"field": "sh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 67.5,
				"top": 774,
				"height": 9.75,
				"width": 42,
				"field": "zb"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 238.5,
				"top": 774,
				"height": 9.75,
				"width": 28.5,
				"title": "审核："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 37.5,
				"top": 774,
				"height": 9.75,
				"width": 28.5,
				"title": "制表："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 460.5,
				"top": 774,
				"height": 9.75,
				"width": 120,
				"field": "page"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 486,
		"paperNumberTop": 772.5,
		"paperNumberDisabled": true,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
};
let orderbyclggdata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 105,
		"paperFooter": 723,
		"printElements": [{
			"options": {
				"left": 214.5,
				"top": 39,
				"height": 22.5,
				"width": 138,
				"title": "放样原始材料表",
				"fontSize": 18,
				"textDecoration": "underline",
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 87,
				"height": 12,
				"width": 82.5,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 457.5,
				"top": 87,
				"height": 12,
				"width": 30,
				"title": "日期："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 45,
				"top": 87,
				"height": 12,
				"width": 30,
				"title": "塔型："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 487.5,
				"top": 87,
				"height": 12,
				"width": 76.5,
				"field": "rq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 15,
				"top": 105,
				"height": 277.5,
				"width": 564,
				"field": "tb",
				"textAlign": "center",
				"tableBodyRowHeight": 18,
				"tableFooterRepeat": "page",
				"columns": [
					[{
						"title": "零件编号",
						"field": "ljbh",
						"width": 40.290543242374035,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ljbh"
					}, {
						"title": "材料名称",
						"field": "clmc",
						"width": 27.1625596860557,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "clmc"
					}, {
						"title": "材质",
						"field": "cz",
						"width": 29.752164528256607,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 63.89374638109277,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg"
					}, {
						"title": "长度(mm)",
						"field": "cd",
						"width": 39,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cd"
					}, {
						"title": "宽度(mm)",
						"field": "kd",
						"width": 39,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "kd"
					}, {
						"title": "厚度(mm)",
						"field": "DtMD_iTorch",
						"width": 25,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "DtMD_iTorch"
					}, {
						"title": "类型",
						"field": "type",
						"width": 30,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "type"
					}, {
						"title": "单基数量",
						"field": "djsl",
						"width": 39,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "djsl"
					}, {
						"title": "单基重量",
						"field": "djzl",
						"width": 39.16178667613834,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "djzl"
					}, {
						"title": "孔数",
						"field": "ks",
						"width": 21.878023300024374,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ks"
					}, {
						"title": "电焊",
						"field": "dh",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dh"
					}, {
						"title": "制弯",
						"field": "wq",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "wq"
					}, {
						"title": "切角",
						"field": "qj",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "qj"
					}, {
						"title": "铲背",
						"field": "cb",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cb"
					}, {
						"title": "清根",
						"field": "qg",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "qg"
					}, {
						"title": "打扁",
						"field": "db",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "db"
					}, {
						"title": "开合角",
						"field": "khj",
						"width": 25.287142682632478,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "khj"
					}, {
						"title": "割豁",
						"field": "DtMD_GeHuo",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "DtMD_GeHuo"
					}, {
						"title": "钻孔",
						"field": "zk",
						"width": 15,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zk"
					}, {
						"title": "备注",
						"field": "bz",
						"width": 44.344674970890196,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 235.5,
				"top": 732,
				"height": 9.75,
				"width": 57,
				"field": "byzsl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 366,
				"top": 732,
				"height": 9.75,
				"width": 58.5,
				"field": "byzzl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 312,
				"top": 732,
				"height": 9.75,
				"width": 49.5,
				"title": "本页总重量"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 732,
				"height": 9.75,
				"width": 49.5,
				"title": "本页总孔数"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 180,
				"top": 732,
				"height": 9.75,
				"width": 51,
				"title": "本页总数量"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 507,
				"top": 732,
				"height": 9.75,
				"width": 55.5,
				"field": "byzks"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 40.5,
				"top": 732,
				"height": 9.75,
				"width": 54,
				"title": "本页小计："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 312,
				"top": 750,
				"height": 9.75,
				"width": 49.5,
				"title": "累计总重量"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 235.5,
				"top": 750,
				"height": 9.75,
				"width": 57,
				"field": "ljzsl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 366,
				"top": 750,
				"height": 9.75,
				"width": 58.5,
				"field": "ljzzl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 180,
				"top": 750,
				"height": 9.75,
				"width": 51,
				"title": "累计总数量"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 750,
				"height": 9.75,
				"width": 49.5,
				"title": "累计总孔数"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 58.5,
				"top": 750,
				"height": 9.75,
				"width": 36,
				"title": "累计："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 507,
				"top": 750,
				"height": 9.75,
				"width": 55.5,
				"field": "ljzks"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 765,
				"height": 9,
				"width": 537
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 267,
				"top": 774,
				"height": 9.75,
				"width": 48,
				"field": "sh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 67.5,
				"top": 774,
				"height": 9.75,
				"width": 42,
				"field": "zb"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 238.5,
				"top": 774,
				"height": 9.75,
				"width": 28.5,
				"title": "审核："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 37.5,
				"top": 774,
				"height": 9.75,
				"width": 28.5,
				"title": "制表："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 460.5,
				"top": 774,
				"height": 9.75,
				"width": 120,
				"field": "page"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 486,
		"paperNumberTop": 772.5,
		"paperNumberDisabled": true,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
};