let bjydkdata = {
	"panels": [{
		"index": 0,
		"paperType": "A5",
		"height": 148,
		"width": 210,
		"paperHeader": 76.5,
		"paperFooter": 391.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 13.5,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司",
				"fontSize": 21.75,
				"fontWeight": "900",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 37.5,
				"height": 9.75,
				"width": 594,
				"title": "部件移动卡片",
				"fontSize": 17.25,
				"fontWeight": "600",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 187.5,
				"top": 57,
				"height": 9,
				"width": 219,
				"borderWidth": "2.25"
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 27,
				"top": 64.5,
				"height": 9.75,
				"width": 120,
				"title": "套用塔型"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 69,
				"top": 76.5,
				"height": 123,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 223.5,
				"top": 76.5,
				"height": 16.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 21,
				"top": 76.5,
				"height": 123,
				"width": 555
			},
			"printElementType": {
				"type": "rect"
			}
		}, {
			"options": {
				"left": 72,
				"top": 81,
				"height": 9.75,
				"width": 120,
				"field": "djh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 228,
				"top": 81,
				"height": 9.75,
				"width": 22.5,
				"title": "段"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 81,
				"height": 9.75,
				"width": 37.5,
				"title": "单据号"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 81,
				"height": 9.75,
				"width": 120,
				"field": "duan"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 93,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 72,
				"top": 96,
				"height": 9.75,
				"width": 498,
				"field": "gcmc"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 96,
				"height": 9.75,
				"width": 42,
				"title": "工程名称"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 223.5,
				"top": 108,
				"height": 91.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 262.5,
				"top": 108,
				"height": 91.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 412.5,
				"top": 108,
				"height": 91.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 447,
				"top": 108,
				"height": 91.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 21,
				"top": 108,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 111,
				"height": 9.75,
				"width": 120,
				"field": "jishu"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 228,
				"top": 111,
				"height": 9.75,
				"width": 34.5,
				"title": "基数"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 72,
				"top": 111,
				"height": 9.75,
				"width": 120,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 415.5,
				"top": 111,
				"height": 9.75,
				"width": 24,
				"title": "件数"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 111,
				"height": 9.75,
				"width": 28.5,
				"title": "塔型"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 111,
				"height": 9.75,
				"width": 120,
				"field": "jianshu"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 123,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 228,
				"top": 126,
				"height": 9.75,
				"width": 28.5,
				"title": "材料"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 72,
				"top": 126,
				"height": 9.75,
				"width": 120,
				"field": "jianhao"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 126,
				"height": 9.75,
				"width": 120,
				"field": "cl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 126,
				"height": 9.75,
				"width": 25.5,
				"title": "件号"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 130.5,
				"height": 21,
				"width": 120,
				"field": "cz",
				"fontSize": 12.75
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 415.5,
				"top": 132,
				"height": 15,
				"width": 31.5,
				"title": "材质",
				"fontSize": 12.75
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 138,
				"height": 9,
				"width": 391.5
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 141,
				"height": 9.75,
				"width": 120,
				"field": "cd"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 72,
				"top": 141,
				"height": 9.75,
				"width": 120,
				"field": "gg"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 141,
				"height": 9.75,
				"width": 30,
				"title": "规格"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 228,
				"top": 142.5,
				"height": 9.75,
				"width": 28.5,
				"title": "长度"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 153,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 228,
				"top": 156,
				"height": 9.75,
				"width": 24,
				"title": "宽度"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 156,
				"height": 9.75,
				"width": 120,
				"field": "kd"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 72,
				"top": 156,
				"height": 9.75,
				"width": 120,
				"field": "hd"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 415.5,
				"top": 156,
				"height": 9.75,
				"width": 30,
				"title": "炉批号"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 156,
				"height": 9.75,
				"width": 27,
				"title": "厚度"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 156,
				"height": 9.75,
				"width": 120
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 168,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 228,
				"top": 171,
				"height": 9.75,
				"width": 28.5,
				"title": "总孔数"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 171,
				"height": 9.75,
				"width": 120,
				"field": "zks"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 72,
				"top": 171,
				"height": 9.75,
				"width": 120,
				"field": "djks"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 415.5,
				"top": 171,
				"height": 9.75,
				"width": 30,
				"title": "重量"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 171,
				"height": 9.75,
				"width": 39,
				"title": "单件孔数"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 171,
				"height": 9.75,
				"width": 120,
				"field": "zl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 183,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 72,
				"top": 186,
				"height": 9.75,
				"width": 120,
				"field": "tianxie"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 228,
				"top": 186,
				"height": 9.75,
				"width": 27,
				"title": "审核"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 186,
				"height": 9.75,
				"width": 120,
				"field": "sh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 186,
				"height": 9.75,
				"width": 25.5,
				"title": "填写"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 415.5,
				"top": 186,
				"height": 9.75,
				"width": 24,
				"title": "日期"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 186,
				"height": 9.75,
				"width": 120,
				"field": "rq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 202.5,
				"height": 180,
				"width": 555
			},
			"printElementType": {
				"type": "rect"
			}
		}, {
			"options": {
				"left": 27,
				"top": 205.5,
				"height": 9.75,
				"width": 34.5,
				"title": "备注"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 222,
				"height": 9.75,
				"width": 543,
				"field": "bz"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 223.5,
				"top": 250.5,
				"height": 130.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 295.5,
				"top": 250.5,
				"height": 130.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 376.5,
				"top": 250.5,
				"height": 130.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 153,
				"top": 250.5,
				"height": 130.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 444,
				"top": 250.5,
				"height": 130.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 69,
				"top": 250.5,
				"height": 130.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 511.5,
				"top": 250.5,
				"height": 130.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 21,
				"top": 250.5,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 99,
				"top": 253.5,
				"height": 9.75,
				"width": 34.5,
				"title": "加工者"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 171,
				"top": 253.5,
				"height": 9.75,
				"width": 34.5,
				"title": "质检员"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 243,
				"top": 253.5,
				"height": 9.75,
				"width": 34.5,
				"title": "日期"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 393,
				"top": 253.5,
				"height": 9.75,
				"width": 34.5,
				"title": "加工者"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 315,
				"top": 253.5,
				"height": 9.75,
				"width": 34.5,
				"title": "工序"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 463.5,
				"top": 253.5,
				"height": 9.75,
				"width": 34.5,
				"title": "质检员"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 253.5,
				"height": 9.75,
				"width": 34.5,
				"title": "工序"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 534,
				"top": 253.5,
				"height": 9.75,
				"width": 34.5,
				"title": "日期"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 267,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 315,
				"top": 270,
				"height": 9.75,
				"width": 34.5,
				"title": "清根"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 27,
				"top": 270,
				"height": 9.75,
				"width": 34.5,
				"title": "剪切"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 283.5,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 27,
				"top": 286.5,
				"height": 9.75,
				"width": 34.5,
				"title": "制弯"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 315,
				"top": 286.5,
				"height": 9.75,
				"width": 34.5,
				"title": "铲背"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 300,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 27,
				"top": 303,
				"height": 9.75,
				"width": 34.5,
				"title": "号料"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 315,
				"top": 303,
				"height": 9.75,
				"width": 34.5,
				"title": "矫正"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 316.5,
				"height": 9,
				"width": 553.5
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 27,
				"top": 319.5,
				"height": 9.75,
				"width": 34.5,
				"title": "冲孔"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 315,
				"top": 319.5,
				"height": 9.75,
				"width": 34.5,
				"title": "开合角"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 333,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 27,
				"top": 336,
				"height": 9.75,
				"width": 34.5,
				"title": "钻孔"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 315,
				"top": 336,
				"height": 9.75,
				"width": 55.5,
				"title": "配钻（扩孔）"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 349.5,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 27,
				"top": 352.5,
				"height": 9.75,
				"width": 34.5,
				"title": "切角"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 315,
				"top": 352.5,
				"height": 9.75,
				"width": 34.5,
				"title": "电焊"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 366,
				"height": 9,
				"width": 555
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 27,
				"top": 369,
				"height": 9.75,
				"width": 34.5,
				"title": "钢印"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 508.5,
		"paperNumberTop": 394.5,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}

