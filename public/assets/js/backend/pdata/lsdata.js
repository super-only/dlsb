let lsdata = {
    "panels": [{
        "index": 0,
        "paperType": "A4",
        "height": 297,
        "width": 210,
        "paperHeader": 121.5,
        "paperFooter": 811.5,
        "printElements": [{
            "options": {
                "left": 220.5,
                "top": 18,
                "height": 9.75,
                "width": 216,
                "title": "绍兴电力设备有限公司",
                "fontSize": 20.25,
                "fontWeight": "bold",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": {"type": "text"}
        }, {
            "options": {
                "left": 262.5,
                "top": 37.5,
                "height": 9.75,
                "width": 120,
                "title": "螺栓统计表",
                "fontSize": 18,
                "fontWeight": "bold",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": {"type": "text"}
        },
            {
                "options": {"left": 24, "top": 55.5, "height": 9.75, "width": 65.5, "title": "工程名称：", "fontSize": 12},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 88.5, "top": 55.5, "height": 9.75, "width": 277.5, "field": "gcmc", "fontSize": 12,"lineHeight":14},
                "printElementType": {"type": "text"}
            },
            {
                "options": {"left": 421.5, "top": 55.5, "height": 9.75, "width": 46.5, "title": "日期：", "fontSize": 12},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 468.5, "top": 55.5, "height": 9.75, "width": 120, "field": "idate", "fontSize": 12},
                "printElementType": {"type": "text"}
            },
            {
                "options": {"left": 24, "top": 85, "height": 9.75, "width": 65.5, "title": "客户名称：", "fontSize": 12},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 88.5, "top": 85, "height": 9.75, "width": 170, "field": "khmc", "fontSize": 12},
                "printElementType": {"type": "text"}
            },
            {
                "options": {"left": 268.5, "top": 85, "height": 9.75, "width": 53.5, "title": "塔型：", "fontSize": 12},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 309.5, "top": 85, "height": 9.75, "width": 120, "field": "tx", "fontSize": 12},
                "printElementType": {"type": "text"}
            },
            {
                "options": {"left": 421.5, "top": 85, "height": 9.75, "width": 46.5, "title": "图号：", "fontSize": 12},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 468.5, "top": 85, "height": 9.75, "width": 120, "field": "th", "fontSize": 12},
                "printElementType": {"type": "text"}
            },
            {
                "options": {"left": 24, "top": 105.5, "height": 9.75, "width": 55.5, "title": "呼高：", "fontSize": 12},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 88.5, "top": 105.5, "height": 9.75, "width": 114, "field": "hg", "fontSize": 12},
                "printElementType": {"type": "text"}
            },
            {
                "options": {"left": 268.5, "top": 105.5, "height": 9.75, "width": 53, "title": "杆塔号：", "fontSize": 12},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 309.5, "top": 105.5, "height": 9.75, "width": 120, "field": "gth", "fontSize": 12},
                "printElementType": {"type": "text"}
            },
            {
                "options": {"left": 421.5, "top": 105.5, "height": 9.75, "width": 46.5, "title": "基数：", "fontSize": 12},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 468.5, "top": 105.5, "height": 9.75, "width": 120, "field": "js", "fontSize": 12},
                "printElementType": {"type": "text"}
            },
            {
                "options": {
                    "left": 25,
                    "top": 140,
                    "height": 43.5,
                    "width": 510,
                    "field": "tb",
                    "tableBodyRowBorder": "noBorder",
                    "tableBodyCellBorder": "noBorder",
                    "tableBodyRowHeight": 5,
                    "fontSize": 11,
                    "columns": [[{
                        "title": "名称",
                        "field": "mc",
                        "width": 53.39971550508369,
                        "align": "center",
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "mc",
                        "formatter": function (value, row) {
                             let h=27;
                                // if(value){
                                //     h=Math.ceil(value.length/4);
                                // }
                                // h=2*h;
                            if (row['hb'] == 2) {
                                return "<div style='border-bottom: none;position:relative;' >&nbsp;<div style='position:absolute;width:7rem;top:0;'>" + value.substring(0,4) + "</div><div style='width:7rem;top:0;'>" + value.substring(4) + "</div></div>";
                            }else if(row['hb'] == 0){                                                                                              
                                return "<div style='border-bottom: none;position:relative;' >&nbsp;<div style='position:absolute;width:7rem;top:0;'>" + value + "</div></div>";
                            }else{
                                if(row['addrow']>0){
                                    h=(row['addrow']+1)*27;
                                    console.log('h',h)
                                }
                                //<div style='height:"+h+"px'>&nbsp;</div>
                                return "<div style='height:"+h+"px'>" + value + "</div>";
                            } 
                        },
                    }, {
                        "title": "等级",
                        "field": "dj",
                        "width": 33.390376153846155,
                        "align": "center",
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "dj",
                        "formatter": function (value) {
                            return "<div>" + value + "</div>";
                        }
                    }, {
                        "title": "规格",
                        "field": "gg",
                        "width": 70,
                        "align": "center",
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "gg",
                        "formatter": function (value) {
                            return "<div>" + value + "</div>";
                        }
                    }, {
                        "title": "单位",
                        "field": "dw",
                        "width": 31.47824409322471,
                        "align": "center",
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "dw",
                        "formatter": function (value) {
                            return "<div>" + value + "</div>";
                        }
                    }, {
                        "title": "单基数量",
                        "field": "djsl",
                        "width": 62.139901994966316,
                        "align": "center",
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "djsl",
                        "formatter": function (value) {
                            return "<div>" + value + "</div>";
                        }
                    }, {
                        "title": "损耗数量",
                        "field": "shsl",
                        "width": 53.359945656579974,
                        "align": "center",
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "shsl",
                        "formatter": function (value) {
                            return "<div>" + value + "</div>";
                        }
                    }, {
                        "title": "合计数量",
                        "field": "hjsl",
                        "width": 52.06496396465854,
                        "align": "center",
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "hjsl",
                        "formatter": function (value) {
                            return "<div>" + value + "</div>";
                        }
                    }, {
                        "title": "合计重量",
                        "field": "hjzl",
                        "width": 62.65968505053822,
                        "align": "center",
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "hjzl",
                        "formatter": function (value) {
                            return "<div>" + value + "</div>";
                        }
                    }, {
                        "title": "备注",
                        "field": "bz",
                        "width": 104,
                        "align": "center",
                        "colspan": 1,
                        "rowspan": 1,
                        "checked": true,
                        "columnId": "bz",
                        "formatter": function (value) {
                            return "<div style='border-right: none'>" + value + "</div>";
                        }
                    }]]
                }, "printElementType": {"title": "表格", "type": "tableCustom"}
            }, {
                "options": {
                    "left": 21,
                    "top": 192,
                    "height": 82.5,
                    "width": 550,
                    "field": "imemo",
                    "fontSize": 15,
                    "lineHeight":17,
                    "fontWeight": "bold",
                },
                "printElementType": {
                    "type": "longText"
                }
            },{
                "options": {"left": 408, "top": 814, "height": 9.75, "width": 55,"fontSize": 15, "title": "审核："},
                "printElementType": {"type": "text"}
            },{
                "options": {"left": 27.5, "top": 814, "height": 9.75, "width": 55, "fontSize": 15,"title": "统计："},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 78.5, "top": 814, "height": 9.75, "width": 120, "fontSize": 15,"field": "tj"},
                "printElementType": {"type": "text"}
            }, {
                "options": {"left": 457.5, "top": 814, "height": 9.75, "width": 120,"fontSize": 15, "field": "sh"},
                "printElementType": {"type": "text"}
            }],
        "paperNumberLeft": 280,
        "paperNumberTop": 815.5,
        "paperNumberFormat": "第 paperNo 页，共 paperCount 页"
    }]
};
