let dysqdata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 225,
		"paperFooter": 594,
		"printElements": [{
			"options": {
				"left": 30,
				"top": 27.5,
				"height": 70.5,
				"width": 84,
				"src": "/assets/img/qr_img.png"
			},
			"printElementType": {
				"type": "image"
			}
		}, {
			"options": {
				"left": 30,
				"top": 33.5,
				"height": 9.75,
				"width": 529.5,
				"title": "绍兴电力设备有限公司",
				"fontSize": 19.5,
				"fontWeight": "700",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 259.5,
				"top": 57.5,
				"height": 9.75,
				"width": 78,
				"title": "代料申请单",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 30,
				"top": 117,
				"height": 9.75,
				"width": 351,
				"title": "送部门：生技部、铁塔车间、采购、质保部门、仓库",
				"fontSize": 14.25
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 222,
				"top": 139.5,
				"height": 57,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 30,
				"top": 139.5,
				"height": 595.5,
				"width": 531
			},
			"printElementType": {
				"type": "rect"
			}
		}, {
			"options": {
				"left": 225,
				"top": 148.5,
				"height": 9.75,
				"width": 64.5,
				"title": "工程名称：",
				"fontSize": 12,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 291,
				"top": 148.5,
				"height": 9.75,
				"width": 262.5,
				"field": "gcmc",
				"testData": "gcmc",
				"fontSize": 12
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 33,
				"top": 150,
				"height": 9.75,
				"width": 67.5,
				"title": "编号：",
				"fontSize": 12,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 102,
				"top": 150,
				"height": 9.75,
				"width": 112.5,
				"field": "bh",
				"fontSize": 12
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 30,
				"top": 168,
				"height": 9,
				"width": 531
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 33,
				"top": 177,
				"height": 9.75,
				"width": 67.5,
				"title": "任务单号：",
				"fontSize": 12,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 102,
				"top": 177,
				"height": 9.75,
				"width": 112.5,
				"field": "rwdh",
				"fontSize": 12
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 225,
				"top": 177,
				"height": 9.75,
				"width": 64.5,
				"title": "塔型：",
				"fontSize": 12,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 291,
				"top": 177,
				"height": 9.75,
				"width": 262.5,
				"field": "tx",
				"fontSize": 12
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 30,
				"top": 196.5,
				"height": 9,
				"width": 529.5
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 253.5,
				"top": 204,
				"height": 9.75,
				"width": 81,
				"title": "待办事项",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 30,
				"top": 225,
				"height": 9,
				"width": 531
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 30,
				"top": 225,
				"height": 369,
				"width": 531,
				"textAlign": "center",
				"tableBorder": "noBorder",
				"field": "tb",
				"columns": [
					[{
						"title": "序号",
						"field": "xh",
						"width": 75.8571687498425,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "hx"
					}, {
						"title": "材质",
						"field": "cz",
						"width": 75.8571687646734,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 75.85710691419563,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg"
					}, {
						"title": "原长度(米)",
						"field": "ycd",
						"width": 75.8571687498425,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ycd"
					}, {
						"title": "代用材质",
						"field": "dycz",
						"width": 75.85711341390439,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dycz"
					}, {
						"title": "代用规格",
						"field": "dygg",
						"width": 75.85710465769913,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dygg"
					}, {
						"title": "代用长度(米)",
						"field": "dycd",
						"width": 75.8571687498425,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dyjh"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 109.5,
				"top": 594,
				"height": 141,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 30,
				"top": 594,
				"height": 9,
				"width": 531
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 33,
				"top": 603,
				"height": 9.75,
				"width": 73.5,
				"title": "备注"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 30,
				"top": 621,
				"height": 9,
				"width": 531
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 33,
				"top": 633,
				"height": 9.75,
				"width": 73.5,
				"title": "采购员"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 30,
				"top": 649.5,
				"height": 9,
				"width": 531
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 33,
				"top": 661.5,
				"height": 9.75,
				"width": 73.5,
				"title": "车间主任"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 30,
				"top": 678,
				"height": 9,
				"width": 531
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 33,
				"top": 688.5,
				"height": 9.75,
				"width": 73.5,
				"title": "审核"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 30,
				"top": 706.5,
				"height": 9,
				"width": 531
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 33,
				"top": 715.5,
				"height": 9.75,
				"width": 73.5,
				"title": "批准"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 234,
				"top": 772.5,
				"height": 9.75,
				"width": 52.5,
				"title": "制表日期："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 63,
				"top": 772.5,
				"height": 9.75,
				"width": 120,
				"field": "zbr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 289.5,
				"top": 772.5,
				"height": 9.75,
				"width": 120,
				"field": "zbrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 33,
				"top": 772.5,
				"height": 9.75,
				"width": 28.5,
				"title": "制表："
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 427.5,
		"paperNumberTop": 771,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}