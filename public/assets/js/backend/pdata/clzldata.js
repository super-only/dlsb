let clzldata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 50,
		"paperFooter": 810,
		"printElements": [{
			"options": {
				"left": 216,
				"top": 7.5,
				"height": 18,
				"width": 148.5,
				"field": 'title',
				"fontSize": 12.75,
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 72,
				"top": 40,
				"height": 9.75,
				"width": 370.5,
				"field": "gcmc"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 22.5,
				"top": 40,
				"height": 9.75,
				"width": 48,
				"title": "工程名称："
			},
			"printElementType": {
				"type": "text"
			}
		},{
			"options": {
				"fontFamily": "SimHei",
				"left": 250.5,
				"top": 30,
				"height": 9.75,
				"width": 30,
				"title": "塔型："
			},
			"printElementType": {
				"type": "text"
			}
		},{
			"options": {
				"fontFamily": "SimHei",
				"left": 282,
				"top": 30,
				"height": 9.75,
				"width": 103.5,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 402,
				"top": 30,
				"height": 9.75,
				"width": 49.5,
				"title": "下达单号："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 72,
				"top": 30,
				"height": 9.75,
				"width": 166.5,
				"field": "sl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 22.5,
				"top": 30,
				"height": 9.75,
				"width": 48,
				"title": "基数："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 453,
				"top": 30,
				"height": 9.75,
				"width": 120,
				"field": "xddh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 15,
				"top": 50,
				"height": 36,
				"width": 550.5,
				"tableHeaderFontSize": 11.25,
				"tableBodyRowHeight": 21,
				"field": "tb",
				"fontSize": 9.5,
				"columns": [
					[{
						"title": "序号",
						"field": "id",
						"width": 42.83879246015706,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "材料名称",
						"field": "stuff",
						"width": 101.09352783675135,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "材质",
						"field": "material",
						"width": 101.80291306326809,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "规格",
						"field": "specification",
						"width": 101.85798816568047,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "长度(mm)",
						"field": "length",
						"width": 101.92307692307693,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "数量",
						"field": "count",
						"width": 101.48370155106616,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}],
		"paperNumberLeft": 565,
		"paperNumberTop": 819
	}]
};