clbyddata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 210,
		"width": 297,
		"paperHeader": 121.5,
		"paperFooter": 595.2755905511812,
		"printElements": [{
			"options": {
				"left": 36,
				"top": 25.5,
				"height": 76.5,
				"width": 84,
				"src": "/uploads/20220415/5b49d09aa5bc0577d1b2f00f46cc6783.png"
			},
			"printElementType": {
				"type": "image"
			}
		}, {
			"options": {
				"left": 340.5,
				"top": 36,
				"height": 9.75,
				"width": 219,
				"title": "绍兴电力设备有限公司",
				"fontSize": 16.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 369,
				"top": 69,
				"height": 9.75,
				"width": 141,
				"title": "材料报验单",
				"fontSize": 20.25,
				"fontWeight": "bold"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 345,
				"top": 105,
				"height": 9.75,
				"width": 49.5,
				"title": "供应商：",
				"fontWeight": "bold",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, 
		{
			"options": {
				"left": 605,
				"top": 105,
				"height": 9.75,
				"width": 58.5,
				"title": "取样车间：",
				"fontWeight": "bold",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		},{
			"options": {
				"left": 662,
				"top": 105,
				"height": 9.75,	
				"width": 120,
				"field": "qycj",
				"fontWeight": "bold"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 109.5,
				"top": 105,
				"height": 9.75,
				"width": 120,
				"field": "bydbh",
				"fontWeight": "bold"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 396,
				"top": 105,
				"height": 9.75,
				"width": 200,
				"field": "gys",
				"fontWeight": "bold"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 36,
				"top": 105,
				"height": 9.75,
				"width": 72,
				"title": "报验单编号：",
				"fontWeight": "bold",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 36,
				"top": 121.5,
				"height": 42,
				"width": 768,
				"textAlign": "center",
				"field": "tb",
				"tableBodyCellBorder": "noBorder",
				"columns": [
					[{
						"title": "材料名称",
						"field": "clmc",
						"width": 54.23133560936371,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "clmc",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "材质规格",
						"field": "czgg",
						"width": 85.08501372989731,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "czgg",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: left;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "长度",
						"field": "cd",
						"width": 63.81749566139036,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cd",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "生产厂家",
						"field": "sccj",
						"width": 63.815708885957086,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sccj",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "到货数量",
						"field": "dhsl",
						"width": 60.917646273052384,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dhsl",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "到货重量",
						"field": "dhzl",
						"width": 73.26522699848972,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dhzl",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "到货日期",
						"field": "dhrq",
						"width": 63.81724382466822,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dhrq",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: left;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "样本数量",
						"field": "ybsl",
						"width": 63.81243446678977,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ybsl",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: left;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "送样人",
						"field": "syr",
						"width": 63.81951316993953,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "syr",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "送样时间",
						"field": "sysj",
						"width": 63.816681052446626,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sysj",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "炉号",
						"field": "lh",
						"width": 51.60170032800534,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "lh",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "压制批号",
						"field": "yzph",
						"width": 60,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "yzph",
						"formatter": function (value,row) {
							if (row['clmc'] =='委托人:') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;text-align: right;'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}],
		"paperNumberLeft": 811.5,
		"paperNumberTop": 573,
		"paperNumberDisabled": true,
		"rotate": true
	}]
};