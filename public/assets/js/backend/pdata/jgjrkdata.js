let jgjrkdata = {
	"panels": [{
		"index": 0,
		"height": 125,
		"width": 210,
		"paperHeader": 109.5,
		"paperFooter": 313.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 35.5,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司产品入库单",
				"fontSize": 21.75,
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 76.5,
				"top": 60,
				"height": 9.75,
				"width": 336,
				"field": "gfdw",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 415.5,
				"top": 60,
				"height": 9.75,
				"width": 63,
				"title": "入库日期:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 480,
				"top": 60,
				"height": 9.75,
				"width": 90,
				"field": "rkrq",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 60,
				"height": 9.75,
				"width": 64.5,
				"title": "供方单位:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 246,
				"top": 76.5,
				"height": 9.75,
				"width": 69,
				"title": "所在仓库:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 78,
				"top": 76.5,
				"height": 9.75,
				"width": 166.5,
				"field": "rkdh",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 415.5,
				"top": 76.5,
				"height": 9.75,
				"width": 63,
				"title": "入库类别:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 315,
				"top": 76.5,
				"height": 9.75,
				"width": 97.5,
				"field": "szck",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 480,
				"top": 76.5,
				"height": 9.75,
				"width": 90,
				"field": "rklb",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 76.5,
				"height": 9.75,
				"width": 64.5,
				"title": "入库单号:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 76.5,
				"top": 93,
				"height": 9.75,
				"width": 493.5,
				"field": "gcmc",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 93,
				"height": 9.75,
				"width": 63,
				"title": "工程名称:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 109.5,
				"height": 54,
				"width": 556.5,
				"field": "tb",
				"textAlign": "center",
				"fontSize": 12,
				"tableHeaderFontSize": 13.5,
				"columns": [
					[{
						"title": "产品名称",
						"field": "cpmc",
						"width": 164.8532312361502,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cpmc",
						"formatter": function(value, row) {
							if ((value.length * 12) > 160) {
								console.log('产品名称');
								return "<div style='font-size:" + 90 / (value.length) + "pt; border:none'>" + value + "</div>";
							} else {
								return "<div style='border:none'>" + value + "</div>";
							}
						}
					}, {
						"title": "级别",
						"field": "jb",
						"width": 46.47127913174186,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "jb"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 73.6354612314147,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ggxh"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 41.712838235341266,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 72.7613671178344,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "不含税单价",
						"field": "bhsdj",
						"width": 78.53290832859663,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}, {
						"title": "不含税金额",
						"field": "bhsje",
						"width": 78.53291471892095,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bhsje"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 138,
				"top": 315,
				"height": 9.75,
				"width": 51,
				"title": "经办人:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 189,
				"top": 315,
				"height": 9.75,
				"width": 120,
				"field": "jbr",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 51,
				"top": 315,
				"height": 9.75,
				"width": 79.5,
				"field": "zd",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 393,
				"top": 315,
				"height": 9.75,
				"width": 163.5,
				"title": "存根 白 财务 红 仓库 蓝",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 315,
				"height": 9.75,
				"width": 37.5,
				"title": "制单:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 324,
		"paperNumberTop": 313.5,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}