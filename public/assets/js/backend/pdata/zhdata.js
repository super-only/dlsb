let zhdata = {
    "panels": [{
        "index": 0,
        "paperType": "A4",
        "height": 297,
        "width": 210,
        "paperHeader": 84,
        "paperFooter": 814.5,
        "printElements": [{
            "options": {
                "left": 282,
                "top": 18,
                "height": 9.75,
                "width": 72,
                "title": "组焊清单",
                "fontSize": 16.5,
                "fontWeight": "bold"
            }, "printElementType": {"type": "text"}
        }, {
            "options": {
                "left": 451.5,
                "top": 18,
                "height": 9.75,
                "width": 120,
                "field": "PT_DHmemo",
                "fontSize": 14.25,
                "fontWeight": "bold"
            }, "printElementType": {"type": "text"}
        }, {
            "options": {
                "left": 67.5,
                "top": 18,
                "height": 9.75,
                "width": 183,
                "title": "绍兴电力设备有限公司",
                "fontSize": 16.5,
                "fontWeight": "bold"
            }, "printElementType": {"type": "text"}
        }, {
            "options": {"left": 208.5, "top": 40.5, "height": 9.75, "width": 55.5, "title": "工程名称："},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 97.5, "top": 40.5, "height": 9.75, "width": 79.5, "field": "t_num"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 283.5, "top": 40.5, "height": 9.75, "width": 300, "field": "C_ProjectName"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 31.5, "top": 40.5, "height": 9.75, "width": 54, "title": "任务单号："},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 208.5, "top": 64.5, "height": 9.75, "width": 57, "title": "塔  型："},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 97.5, "top": 64.5, "height": 9.75, "width": 79.5, "field": "PT_Num"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 283.5, "top": 64.5, "height": 9.75, "width": 120, "field": "TD_TypeName"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 31.5, "top": 64.5, "height": 9.75, "width": 54, "title": "下达单号："},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 423, "top": 40.5, "height": 9.75, "width": 151.5, "field": "dgy"},
            "printElementType": {"type": "text"}
        }, {
            "options": {
                "left": 20,
                "top": 88.5,
                "height": 43.5,
                "width": 550,
                "field": "tb",
                "tableBodyRowBorder":"noBorder",
                "tableBodyCellBorder":"noBorder",
                "columns": [[{
                    "title": "组件号",
                    "field": "zjh",
                    "width": 54.14792589733301,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zjh",
                    // "styler": function (value, row) {
                    //     // if (row['hb']==0) {
                    //     //     return {
                    //     //         "background-color": "#fff",
                    //     //         "position": "relative",
                    //     //         // "top": "5px",
                    //     //     };
                    //     // }
                    //     return {
                    //         "border": "1px solid"
                    //     };
                    // }
                    "formatter":function(value,row){
                        if (row['hb']==0) {
                            return "<div style='border-bottom: none;'>"+value+"</div>";
                        }else{
                            return "<div>"+value+"</div>";
                        }
                    }
                }, {
                    "title": "组数",
                    "field": "zs",
                    "width": 51.839079653394,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zs",
                    // "styler": function (value, row) {
                    //     if (row['hb']==0) {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "top": "5px"
                    //         };
                    //     }
                    // },
                    "formatter":function(value,row){
                        if (row['hb']==0) {
                            return "<div style='border-bottom: none;'>"+value+"</div>";
                        }else{
                            return "<div>"+value+"</div>";
                        }
                    }
                }, {
                    "title": "零件号",
                    "field": "ljh",
                    "width": 49.07217014191024,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "ljh",
                    "formatter":function(value){
                        return "<div>"+value+"</div>";
                    }
                }, {
                    "title": "材质",
                    "field": "cz",
                    "width": 53.48720842705996,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "cz",
                    "formatter":function(value){
                        return "<div>"+value+"</div>";
                    }
                }, {
                    "title": "规格",
                    "field": "gg",
                    "width": 58.904277947237624,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "gg",
                    "formatter":function(value){
                        return "<div>"+value+"</div>";
                    }
                }, {
                    "title": "长*宽",
                    "field": "ck",
                    "width": 54.99843105638533,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "ck",
                    "formatter":function(value){
                        return "<div>"+value+"</div>";
                    }
                }, {
                    "title": "单基",
                    "field": "dj",
                    "width": 51.2636594928059,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "dj",
                    "formatter":function(value){
                        return "<div>"+value+"</div>";
                    }
                }, {
                    "title": "加工",
                    "field": "jg",
                    "width": 71.97021550050012,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "jg",
                    "formatter":function(value){
                        return "<div>"+value+"</div>";
                    }
                }, {
                    "title": "总重量",
                    "field": "zzl",
                    "width": 58.16551696308188,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zzl",
                    // "styler": function (value, row) {
                    //     if (row['hb']==0) {
                    //         return {
                    //             "background-color": "#fff",
                    //             "position": "relative",
                    //             "top": "5px"
                    //         };
                    //     }
                    // },
                    "formatter":function(value,row){
                        if (row['hb']==0) {
                            return "<div style='border-bottom: none;'>"+value+"</div>";
                        }else{
                            return "<div>"+value+"</div>";
                        }
                    }
                }, {
                    "title": "备注",
                    "field": "bz",
                    "width": 73.651514920292065,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "bz",
                    "formatter":function(value){
                        return "<div style='border-right: none;'>"+value+"</div>";
                    }
                }]]
            }, "printElementType": {"title": "表格", "type": "tableCustom"}
        }, {
            "options": {"left": 423, "top": 64.5, "height": 9.75, "width": 150, "field": "dydj"},
            "printElementType": {"type": "text"}
        },
            {
                "options": {
                    "left": 19.5,
                    "top": 148.5,
                    "height": 9.75,
                    "width": 72,
                    "title": "加工总组数：",
                    "fontSize": 10.5,
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 106.5,
                    "top": 148.5,
                    "height": 9.75,
                    "width": 91.5,
                    "field": "zssum",
                    "fontSize": 10.5,
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 210,
                    "top": 148.5,
                    "height": 9.75,
                    "width": 70.5,
                    "title": "加工总重量：",
                    "fontSize": 10.5,
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 289.5,
                    "top": 148.5,
                    "height": 9.75,
                    "width": 204,
                    "field": "zlsum",
                    "fontSize": 10.5,
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 18,
                    "top": 184.5,
                    "height": 9.75,
                    "width": 37.5,
                    "title": "编制：",
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 69,
                    "top": 184.5,
                    "height": 9.75,
                    "width": 49.5,
                    "field": "bz",
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 132,
                    "top": 184.5,
                    "height": 9.75,
                    "width": 45,
                    "title": "日期：",
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 184.5,
                    "top": 184.5,
                    "height": 9.75,
                    "width": 63,
                    "field": "idate",
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 262.5,
                    "top": 184.5,
                    "height": 9.75,
                    "width": 37.5,
                    "title": "审核：",
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 352.5,
                    "top": 184.5,
                    "height": 9.75,
                    "width": 36,
                    "title": "日期：",
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            },
            {
                "options": {
                    "left": 457.5,
                    "top": 184.5,
                    "height": 9.75,
                    "width": 33,
                    "title": "批准：",
                    "fontWeight": "bold"
                },
                "printElementType": {
                    "type": "text"
                }
            }
        ],
        "paperNumberLeft": 565,
        "paperNumberTop": 819
    }]
};
