let cprkdata = {
	"panels": [{
		"index": 0,
		"height": 125,
		"width": 210,
		"paperHeader": 102,
		"paperFooter": 304.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 21,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司产品入库单",
				"fontSize": 21.75,
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 72,
				"top": 54,
				"height": 9.75,
				"width": 339,
				"field": "gfdw",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 411,
				"top": 54,
				"height": 9.75,
				"width": 63,
				"title": "入库日期:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 475.5,
				"top": 54,
				"height": 9.75,
				"width": 90,
				"field": "rkrq",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 9,
				"top": 54,
				"height": 9.75,
				"width": 61.5,
				"title": "供方单位:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 238.5,
				"top": 70.5,
				"height": 9.75,
				"width": 61.5,
				"title": "所在仓库:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 303,
				"top": 70.5,
				"height": 9.75,
				"width": 97.5,
				"field": "szck",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 72,
				"top": 70.5,
				"height": 9.75,
				"width": 166.5,
				"field": "rkdh",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 411,
				"top": 70.5,
				"height": 9.75,
				"width": 63,
				"title": "入库类别:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 475.5,
				"top": 70.5,
				"height": 9.75,
				"width": 90,
				"field": "rklb",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 9,
				"top": 70.5,
				"height": 9.75,
				"width": 61.5,
				"title": "入库单号:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 411,
				"top": 87,
				"height": 9.75,
				"width": 63,
				"title": "任务单号:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 72,
				"top": 87,
				"height": 9.75,
				"width": 339,
				"field": "wtdw",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 475.5,
				"top": 87,
				"height": 9.75,
				"width": 91.5,
				"field": "rwdh",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 9,
				"top": 87,
				"height": 9.75,
				"width": 61.5,
				"title": "委托单位:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 9,
				"top": 102,
				"height": 45,
				"width": 556.5,
				"field": "tb",
				"textAlign": "center",
				"fontSize": 13.5,
				"tableHeaderFontSize": 13.5,
				"columns": [
					[{
						"title": "产品名称",
						"field": "cpmc",
						"width": 65.43392143571094,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cpmc"
					}, {
						"title": "规格型号",
						"field": "ggxh",
						"width": 168.80012466933562,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ggxh"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 47.36737375567377,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 59.44985770226535,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "重量(Kg)",
						"field": "zl",
						"width": 106.03559823254909,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl"
					}, {
						"title": "备注",
						"field": "bz",
						"width": 110.41312420446528,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 129,
				"top": 312,
				"height": 9.75,
				"width": 51,
				"title": "经办人:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 180,
				"top": 312,
				"height": 9.75,
				"width": 120,
				"field": "jbr",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 46.5,
				"top": 312,
				"height": 9.75,
				"width": 79.5,
				"field": "zd",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 378,
				"top": 312,
				"height": 9.75,
				"width": 163.5,
				"title": "存根 白 财务 红 仓库 蓝",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 9,
				"top": 312,
				"height": 9.75,
				"width": 37.5,
				"title": "制单:",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 300,
		"paperNumberTop": 309,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}