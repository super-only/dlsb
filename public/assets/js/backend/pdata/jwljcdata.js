let jwljcdata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 210,
		"width": 297,
		"paperHeader": 85.5,
		"paperFooter": 537,
		"printElements": [{
			"options": {
				"left": 1.5,
				"top": 33,
				"height": 9.75,
				"width": 840,
				"title": "绍兴电力设备有限公司",
				"fontSize": 15,
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 51,
				"height": 9.75,
				"width": 840,
				"title": "结存汇总表",
				"fontSize": 15,
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 69,
				"height": 9.75,
				"width": 120,
				"field": "jcrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 585,
				"top": 69,
				"height": 9.75,
				"width": 39,
				"title": "仓库：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 69,
				"height": 9.75,
				"width": 55.5,
				"title": "结存日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 625.5,
				"top": 69,
				"height": 9.75,
				"width": 120,
				"field": "ck"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 85.5,
				"height": 69,
				"width": 800,
				"field": "tb",
				"textAlign": "center",
				"columns": [
					[{
						"title": "存货",
						"field": "",
						"width": 160,
						"colspan": 3,
						"rowspan": 1,
						"checked": true,
						"columnId": ""
					}, {
						"title": "上期结存",
						"field": "",
						"width": 160,
						"colspan": 3,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "本期入库",
						"field": "",
						"width": 160,
						"colspan": 3,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "本期出库",
						"width": 160,
						"colspan": 3,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "本期结存",
						"width": 160,
						"colspan": 3,
						"rowspan": 1,
						"checked": true
					}],
					[{
						"title": "名称",
						"field": "mc",
						"width": 60.00300015000749,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "mc",
						"formatter": function (value, row) {
							if ((value.length) > 6) {
								return value.substr(0, 5)+'…';
							} 
							else {
								return value;
							}
						}
					}, {
						"title": "规格",
						"field": "gg",
						"width": 70.00350017500875,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg",
						"formatter": function (value, row) {
							// console.log('value', value);
							if (value && (value.length) > 7) {
								return value.substr(0, 6)+'…';
							} 
							else {
								return value;
							}
						}
					}, {
						"title": "单位",
						"field": "dw",
						"width": 30.001500075003744,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "数量",
						"field": "sqjc_sl",
						"width": 53.33266663333169,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "sqjc_sl"
					}, {
						"title": "单价(元)",
						"field": "sqjc_dj",
						"width": 53.33266663333169,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "sqjc_dj"
					}, {
						"title": "金额(元)",
						"field": "sqjc_je",
						"width": 53.33266663333169,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "sqjc_je"
					}, {
						"title": "数量",
						"field": "bqrk_sl",
						"width": 53.33266663333169,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "bqrk_sl"
					}, {
						"title": "单价(元)",
						"field": "bqrk_dj",
						"width": 53.33266663333169,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "bqrk_dj"
					}, {
						"title": "金额(元)",
						"field": "bqrk_je",
						"width": 53.33266663333169,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "bqrk_je"
					}, {
						"title": "数量",
						"field": "bqck_sl",
						"width": 53.33266663333169,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "bqck_sl"
					}, {
						"title": "单价(元)",
						"field": "bqck_dj",
						"width": 52.54366663333159,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "bqck_dj"
					}, {
						"title": "金额(元)",
						"field": "bqck_je",
						"width": 54.12166663333179,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "bqck_je"
					}, {
						"title": "数量",
						"field": "bqjc_sl",
						"width": 53.33266663333169,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "bqjc_sl"
					}, {
						"title": "单价(元)",
						"field": "bqjc_dj",
						"width": 52.87966663333166,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "bqjc_dj"
					}, {
						"title": "金额(元)",
						"field": "bqjc_je",
						"width": 53.78566663333172,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						
						"columnId": "bqjc_je"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 190.5,
				"top": 541.5,
				"height": 9.75,
				"width": 39,
				"title": "审核人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 58.5,
				"top": 541.5,
				"height": 9.75,
				"width": 120,
				"field": "zbr",
				"testData": "zbr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 231,
				"top": 541.5,
				"height": 9.75,
				"width": 120,
				"field": "shr",
				"testData": "shr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 541.5,
				"height": 9.75,
				"width": 39,
				"title": "制表人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 190.5,
				"top": 556.5,
				"height": 9.75,
				"width": 39,
				"title": "日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 231,
				"top": 556.5,
				"height": 9.75,
				"width": 120,
				"field": "shrq",
				"testData": "shrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 463.5,
				"top": 556.5,
				"height": 9.75,
				"width": 51,
				"title": "打印日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 58.5,
				"top": 556.5,
				"height": 9.75,
				"width": 120,
				"field": "zbrq",
				"testData": "zbrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 516,
				"top": 556.5,
				"height": 9.75,
				"width": 120,
				"field": "dyrq",
				"testData": "dyrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 556.5,
				"height": 9.75,
				"width": 39,
				"title": "日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 661.5,
		"paperNumberTop": 555,
		"paperNumberFormat": "第paperNo页，共paperCount页",
		"rotate": true
	}]
}