let scsygcdata = {
    "panels": [{
        "index": 0,
        "paperType": "A3",
        "height": 297,
        "width": 460,
        "paperHeader": 49.5,
        "paperFooter": 821.8897637795277,
        "printElements": [{
            "options": {
                "left": 456,
                "top": 15,
                "height": 19.5,
                "width": 192,
                "title": "绍兴电力设备生产过程检验",
                "fontSize": 15,
                "fontWeight": "bold",
                "textContentVerticalAlign": "middle"
            },
            "printElementType": {
                "type": "text"
            }
        },
        {
            "options": {
                "left": 28.5,
                "top": 55.5,
                "height": 36,
                "width": 1145,
				"field": "tb",
                "columns": [[{
                    "title": "零件部号",
					"field": "parts",
                    "width": 49.16480500551656,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "材料名称",
					"field": "stuff",
                    "width": 57.21791429641019,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "材质",
					"field": "material",
                    "width": 39.208372847559,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "规格",
					"field": "specification",
                    "width": 69.05571180804228,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "长度",
					"field": "length",
                    "width": 55.10405929388773,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "下料件数",
					"field": "xl_count",
                    "width": 40.88668677042041,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "构件类型",
					"field": "xl_type",
                    "width": 39.95532989440359,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "生产时间",
					"field": "xl_creat_time",
                    "width": 88.84559912118208,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "合格/抽检",
					"field": "xl_hg_count",
                    "width": 44.90913827586512,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "制孔件数",
					"field": "zk_count",
                    "width": 49.10160499219222,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "制孔工艺",
					"field": "zk_kong",
                    "width": 43.90447870037963,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "孔数",
					"field": "zk_sum_hole",
                    "width": 27.94652130135387,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "制弯件数",
					"field": "zw_count",
                    "width": 38.12583353014642,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "制弯工艺",
					"field": "zw_bending",
                    "width": 45.936847241897084,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "焊接质量等级",
					"field": "hj_fire",
                    "width": 54.53551454551251,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "加工者",
					"field": "hj_processor",
                    "width": 129.62685809261757,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "镀锌前重量",
					"field": "dx_sum_weight",
                    "width": 62.64738406647276,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "镀锌后重量",
					"field": "dx_late_weight",
                    "width": 58.69715430245715,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "镀锌日期",
					"field": "dx_gal_time",
                    "width": 110.13018591368376,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }]]
            },
            "printElementType": {
                "title": "表格",
                "type": "tableCustom"
            }
        }],
        "paperNumberLeft": 1160,
        "paperNumberTop": 819
    }]
};