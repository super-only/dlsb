let fjbgdata = {
    "panels": [
        {
            "index": 0, 
            "paperType": "A5", 
            "height": 148, 
            "width": 210, 
            "paperHeader": 51, 
            "paperFooter": 419.52755905511816, 
            "printElements": [
                {
                    "options": {
                        "left": 250.5, 
                        "top": 6, 
                        "height": 18, 
                        "width": 66, 
                        "title": "复检报告", 
                        "fontSize": 15, 
                        "fontWeight": "bold", 
                        "textContentVerticalAlign": "middle"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 250.5, 
                        "top": 36, 
                        "height": 9.75, 
                        "width": 75, 
                        "title": "品种", 
                        "field": "pz", 
                        "testData": "角钢"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 414, 
                        "top": 36, 
                        "height": 9.75, 
                        "width": 171, 
                        "title": "编号", 
                        "field": "no", 
                        "testData": "2210-001-1111111111111"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 16.5, 
                        "top": 36, 
                        "height": 9.75, 
                        "width": 160.5, 
                        "title": "铁塔制造单位：绍兴电力设备有限公司"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 202.5, 
                        "top": 54, 
                        "height": 19.5, 
                        "width": 160.5, 
                        "title": "化学成分(%)", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 150, 
                        "top": 54, 
                        "height": 58.5, 
                        "width": 52.5, 
                        "title": "炉批号", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 111, 
                        "top": 54, 
                        "height": 58.5, 
                        "width": 39, 
                        "title": "牌号", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 70.5, 
                        "top": 54, 
                        "height": 58.5, 
                        "width": 40.5, 
                        "title": "规格", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 363, 
                        "top": 54, 
                        "height": 19.5, 
                        "width": 199.5, 
                        "title": "力学性能", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 27, 
                        "top": 54, 
                        "height": 58.5, 
                        "width": 43.5, 
                        "title": "制造厂", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 561, 
                        "top": 54, 
                        "height": 58.5, 
                        "width": 25.5, 
                        "title": "结论", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "borderRight": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 54, 
                        "height": 58.5, 
                        "width": 16.5, 
                        "title": "序号", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 267, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 31.5, 
                        "title": "锰Mn", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 363, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 33, 
                        "title": "屈服ReHMPa", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 396, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 39, 
                        "title": "抗拉强度ReMPa", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 331.5, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 31.5, 
                        "title": "磷P", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 435, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 30, 
                        "title": "伸长率A%", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 298.5, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 33, 
                        "title": "硫S", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 465, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 28.5, 
                        "title": "冷弯试验", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 234, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 33, 
                        "title": "硅Si", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 493.5, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 67.5, 
                        "title": "冲击试验20°C", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 202.5, 
                        "top": 73.5, 
                        "height": 39, 
                        "width": 31.5, 
                        "title": "碳C", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 267, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "mn", 
                        "testData": "0.47"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 298.5, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "s", 
                        "testData": "0.026"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 331.5, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "p", 
                        "testData": "0.014"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 234, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "si", 
                        "testData": "0.24"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 363, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "qf", 
                        "testData": "332"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 202.5, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "c", 
                        "testData": "0.17"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 396, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "kl", 
                        "testData": "443"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 150, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 52.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "lph", 
                        "testData": "11111111111"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 435, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 30, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "scl", 
                        "testData": "38"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 111, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "cz", 
                        "testData": "Q355B"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 465, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 28.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "lw", 
                        "testData": "合格"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 70.5, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 40.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "gg", 
                        "testData": "∠110*80"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 493.5, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 67.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "cj", 
                        "testData": "20,112,108,124"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 27, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 43.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "field": "zzc", 
                        "testData": "江阴锦城"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 561, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 25.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "borderRight": "solid", 
                        "field": "jl", 
                        "testData": "合格"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 112.5, 
                        "height": 28.5, 
                        "width": 16.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "title": "1"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 298.5, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 267, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 331.5, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 234, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 363, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 202.5, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 396, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 150, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 52.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 435, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 30, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 111, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 465, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 28.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 70.5, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 40.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 493.5, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 67.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 27, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 43.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 561, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 25.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "borderRight": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 141, 
                        "height": 28.5, 
                        "width": 16.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 298.5, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 267, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 331.5, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 234, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 363, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 202.5, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 396, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 150, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 52.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 435, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 30, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 111, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 465, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 28.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 70.5, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 40.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 493.5, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 67.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 27, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 43.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 561, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 25.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "borderRight": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 169.5, 
                        "height": 28.5, 
                        "width": 16.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 267, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 298.5, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 331.5, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 234, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 363, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 202.5, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 396, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 150, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 52.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 435, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 30, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 111, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 465, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 28.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 70.5, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 40.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 493.5, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 67.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 27, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 43.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 561, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 25.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "borderRight": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 198, 
                        "height": 28.5, 
                        "width": 16.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 267, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 298.5, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 331.5, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 234, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 363, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 202.5, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 396, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 150, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 52.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 435, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 30, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 111, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 465, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 28.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 70.5, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 40.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 493.5, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 67.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 27, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 43.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 561, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 25.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "borderRight": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 226.5, 
                        "height": 28.5, 
                        "width": 16.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 234, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 202.5, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 267, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 150, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 52.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 298.5, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 363, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 33, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 396, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 331.5, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 31.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 435, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 30, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 111, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 39, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 465, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 28.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 70.5, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 40.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 493.5, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 67.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 27, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 43.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 561, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 25.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "borderRight": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 255, 
                        "height": 28.5, 
                        "width": 16.5, 
                        "fontSize": 7.5, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 363, 
                        "top": 283.5, 
                        "height": 28.5, 
                        "width": 102, 
                        "title": "设备检测及状态", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 111, 
                        "top": 283.5, 
                        "height": 28.5, 
                        "width": 252, 
                        "field": "gf", 
                        "testData": "GB700-2006碳素结构钢及本工程合同技术规范", 
                        "fontSize": 9, 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 465, 
                        "top": 283.5, 
                        "height": 28.5, 
                        "width": 121.5, 
                        "title": "在检定有效期内", 
                        "fontSize": 9, 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "borderRight": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 283.5, 
                        "height": 28.5, 
                        "width": 100.5, 
                        "title": "执行标准", 
                        "fontSize": 9, 
                        "textAlign": "center", 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 111, 
                        "top": 312, 
                        "height": 60, 
                        "width": 475.5, 
                        "title": "1、”化学成分“一栏所化验的项目应与对应的钢厂厂家所处质保单所化验的项目相同。<br>2、按规范标准要求做冲击试验，冲击试验应明确试验温度；<br>3、质量证明文件与复试报告应一一对应。", 
                        "fontSize": 9, 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "borderRight": "solid", 
                        "borderBottom": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 312, 
                        "height": 60, 
                        "width": 100.5, 
                        "title": "备注", 
                        "fontSize": 9, 
                        "textContentVerticalAlign": "middle", 
                        "lineHeight": 17.25, 
                        "borderLeft": "solid", 
                        "borderTop": "solid", 
                        "textAlign": "center", 
                        "borderBottom": "solid"
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 10.5, 
                        "top": 373.5, 
                        "height": 15, 
                        "width": 100.5, 
                        "title": "试验员", 
                        "field": "syy", 
                        "testData": "阮观祥", 
                        "fontSize": 9, 
                        "lineHeight": 17.25
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }, 
                {
                    "options": {
                        "left": 331.5, 
                        "top": 373.5, 
                        "height": 15, 
                        "width": 100.5, 
                        "title": "审核", 
                        "field": "sh", 
                        "testData": "王金淼", 
                        "fontSize": 9, 
                        "lineHeight": 17.25
                    }, 
                    "printElementType": {
                        "type": "text"
                    }
                }
            ], 
            "paperNumberLeft": 565, 
            "paperNumberTop": 397
        }
    ]
};
let fjbgdata_1 = {
    "panels": [{
        "index": 0,
        "paperType": "A5",
        "height": 148,
        "width": 210,
        "paperHeader": 51,
        "paperFooter": 419.52755905511816,
        "printElements": [{
            "options": {
                "left": 250.5,
                "top": 6,
                "height": 18,
                "width": 66,
                "title": "复检报告\n",
                "fontSize": 15,
                "fontWeight": "bold",
                "textContentVerticalAlign": "middle"
            },
            "printElementType": {
                "type": "text"
            }
        },
        {
            "options": {
                "left": 16.5,
                "top": 36,
                "height": 9.75,
                "width": 160.5,
                "title": "铁塔制造单位：绍兴电力设备有限公司\n"
            },
            "printElementType": {
                "type": "text"
            }
        },
        {
            "options": {
                "left": 250.5,
                "top": 36,
                "height": 9.75,
                "width": 75,
                "title": "品种",
                "field": "pz",
                "testData": "角钢"
            },
            "printElementType": {
                "type": "text"
            }
        },
        {
            "options": {
                "left": 414,
                "top": 36,
                "height": 9.75,
                "width": 133.5,
                "title": "编号",
                "field": "no",
                "testData": "2210-001-1111111111111"
            },
            "printElementType": {
                "type": "text"
            }
        },
        {
            "options": {
                "left": 6,
                "top": 55.5,
                "height": 36,
                "width": 571.5,
                "field": 'tb',
                "columns": [[{
                    "title": "序号",
                    "field": "no",
                    "width": 18.762255159286156,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "制造厂",
                    "field": "MTD_Mader",
                    "width": 45.592392797569175,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "规格",
                    "field": "MATSPEC",
                    "width": 41.796960148889156,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "材质",
                    "field": "MATMATERIAL",
                    "width": 32.55180015532643,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "炉批号",
                    "field": "LuPiHao",
                    "width": 60.78597727851795,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "碳C",
                    "field": "MTD_C",
                    "width": 26.206009661405467,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "硅Si",
                    "field": "MTD_Si",
                    "width": 27.232256857451226,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "锰Mn",
                    "field": "MTD_Mn",
                    "width": 25.539343974773743,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "硫S",
                    "field": "MTD_S",
                    "width": 28.327980125210615,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "钒V",
                    "field": "MTD_V",
                    "width": 28.026674668354474,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "屈服MPa",
                    "field": "MTD_Rel",
                    "width": 32.08678622934012,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "抗拉强度MPa",
                    "field": "MTD_Rm",
                    "width": 30.940801899679485,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "伸长率",
                    "field": "MTD_Percent",
                    "width": 33.79780633833522,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "冷弯试验",
                    "field": "MTD_Wq",
                    "width": 27.395546881805537,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "冲击试验20℃",
                    "field": "MTD_Cj",
                    "width": 70.10572621311313,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                },
                {
                    "title": "结论",
                    "field": "MTD_Conclusion",
                    "width": 28.851681610942165,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }]]
            },
            "printElementType": {
                "title": "表格",
                "type": "tableCustom"
            }
        }],
        "paperNumberLeft": 565,
        "paperNumberTop": 397
    }]
};