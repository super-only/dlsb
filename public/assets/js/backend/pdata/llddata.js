let a4print = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 70,
		"paperFooter": 760,
		"printElements": [{
			"options": {
				"fontFamily": "SimHei",
				"left": 226.5,
				"top": 165,
				"height": "auto",
				"width": 70,
				"field": "sy",
				"fixed": true
			},
			"printElementType": {
				"type": "image"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 505.5,
				"top": 450,
				"height": "auto",
				"width": 70,
				"field": "sy",
				"fixed": true
			},
			"printElementType": {
				"type": "image"
			}
		},{
			"options": {
				"fontFamily": "SimHei",
				"left": 250.5,
				"top": 40,
				"height": 9.75,
				"width": 30,
				"title": "塔型："
			},
			"printElementType": {
				"type": "text"
			}
		},{
			"options": {
				"fontFamily": "SimHei",
				"left": 282,
				"top": 40,
				"height": 9.75,
				"width": 103.5,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 402,
				"top": 40,
				"height": 9.75,
				"width": 49.5,
				"title": "下达单号："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 72,
				"top": 40,
				"height": 9.75,
				"width": 166.5,
				"field": "gcmc"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 22.5,
				"top": 40,
				"height": 9.75,
				"width": 48,
				"title": "工程名称："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 453,
				"top": 40,
				"height": 9.75,
				"width": 120,
				"field": "xddh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 22.5,
				"top": 60,
				"height": 9,
				"width": 550.5
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 22.5,
				"top": 70,
				"height": 61.5,
				"width": 550,
				"textAlign": "center",
				"field": "tb",
				"tableHeaderBorder": "noBorder",
				"tableHeaderCellBorder": "noBorder",
				"tableBorder": "noBorder",
				"tableBodyCellBorder": "noBorder",
				"tableBodyRowBorder": "noBorder",
				"columns": [
					[{
						"title": "规格",
						"field": "gg1",
						"width": 71.66666666666667,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg1",
						"formatter": function(value, row) {
							if (row['sl1'] == '' && row['zl1'] == '') {
								return "<div style='text-align: left;border:none;'>" + value + "</div>";
							}
							else if (row['sl1'] == '' && row['zl1'] != ''){
								return "<div style='text-align: center;border:none;'>" + value + "</div>";
							}
							else{
								return "<div style='text-align: right;border:none;'>" + value + "</div>";
							}
						}
					}, {
						"title": "数量",
						"field": "sl1",
						"width": 71.66666666666667,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl1",
						
					}, {
						"title": "重量(kg)",
						"field": "zl1",
						"width": 71.66666666666667,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl1",
						
					}, {
						"title": "",
						"field": "space1",
						"width": 60,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "space1",
						
					},{
						"title": "规格",
						"field": "gg2",
						"width": 71.66666666666667,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg2",
						"formatter": function(value, row) {
							if (row['sl2'] == '' && row['zl2'] == '') {
								return "<div style='text-align: left;border:none;'>" + value + "</div>";
							}
							else if (row['sl2'] == '' && row['zl2'] != ''){
								return "<div style='text-align: center;border:none;'>" + value + "</div>";
							}
							else{
								return "<div style='text-align: right;border:none;'>" + value + "</div>";
							}
						}
					}, {
						"title": "数量",
						"field": "sl2",
						"width": 71.66666666666667,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl2",
						
					}, {
						"title": "重量(kg)",
						"field": "zl2",
						"width": 71.66666666666667,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl2",
						
					}, {
						"title": "",
						"field": "space1",
						"width": 60,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "space1",
						
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 405,
				"top": 775,
				"height": 9.75,
				"width": 57,
				"title": "累计总重量："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 465,
				"top": 775,
				"height": 9.75,
				"width": 64,
				"field": "ljzzl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 502.5,
				"top": 775,
				"height": 9.75,
				"width": 18,
				"title": "Kg"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 22.5,
				"top": 790.5,
				"height": 9,
				"width": 549
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 75,
				"top": 817,
				"height": 9.75,
				"width": 120,
				"field": "llrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 22.5,
				"top": 817,
				"height": 9.75,
				"width": 51,
				"title": "领料日期："
			},
			"printElementType": {
				"type": "text"
			}
		},{
			"options": {
				"fontFamily": "SimHei",
				"left": 250.5,
				"top": 817,
				"height": 9.75,
				"width": 51,
				"title": "备注："
			},
			"printElementType": {
				"type": "text"
			}
		},{
			"options": {
				"fontFamily": "SimHei",
				"left": 282,
				"top": 817,
				"height": 9.75,
				"width": 300,
				"field": "bz"
			},
			"printElementType": {
				"type": "text"
			}
		},{
			"options": {
				"fontFamily": "SimHei",
				"left": 250.5,
				"top": 796.5,
				"height": 9.75,
				"width": 30,
				"title": "塔型："
			},
			"printElementType": {
				"type": "text"
			}
		},{
			"options": {
				"fontFamily": "SimHei",
				"left": 282,
				"top": 796.5,
				"height": 9.75,
				"width": 103.5,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 402,
				"top": 796.5,
				"height": 9.75,
				"width": 49.5,
				"title": "下达单号："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 72,
				"top": 796.5,
				"height": 9.75,
				"width": 166.5,
				"field": "gcmc"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 22.5,
				"top": 796.5,
				"height": 9.75,
				"width": 48,
				"title": "工程名称："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 453,
				"top": 796.5,
				"height": 9.75,
				"width": 120,
				"field": "xddh"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 469.5,
		"paperNumberTop": 812,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
};

let a5print = {
	"panels": [{
		"index": 0,
		"paperType": "A5",
		"height": 148,
		"width": 210,
		"paperHeader": 103.5,
		"paperFooter": 357,
		"printElements": [{
			"options": {
				"fontFamily": "SimHei",
				"left": 211.5,
				"top": 7.5,
				"height": 9.75,
				"width": 156,
				"title": "绍兴电力设备有限公司",
				"fontSize": 14.25
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 16.5,
				"top": 3,
				"height": 43.5,
				"width": 51,
				"src": "/assets/img/qr_img.png"
			},
			"printElementType": {
				"type": "image"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 246,
				"top": 31.5,
				"height": 9.75,
				"width": 78,
				"title": "车间领料单",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 211.5,
				"top": 49.5,
				"height": 9.75,
				"width": 48,
				"title": "领料车间：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 261,
				"top": 49.5,
				"height": 9.75,
				"width": 145.5,
				"field": "llcj"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 69,
				"top": 49.5,
				"height": 9.75,
				"width": 126,
				"field": "lldh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 16.5,
				"top": 49.5,
				"height": 9.75,
				"width": 49.5,
				"title": "领料单号：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 415.5,
				"top": 49.5,
				"height": 9.75,
				"width": 49.5,
				"title": "所在仓库：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 466.5,
				"top": 49.5,
				"height": 9.75,
				"width": 78,
				"field": "szck"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 211.5,
				"top": 63,
				"height": 9.75,
				"width": 48,
				"title": "领料日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 69,
				"top": 63,
				"height": 9.75,
				"width": 126,
				"field": "rwdh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 261,
				"top": 63,
				"height": 9.75,
				"width": 145.5,
				"field": "llrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 16.5,
				"top": 63,
				"height": 9.75,
				"width": 49.5,
				"title": "任务单号：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 415.5,
				"top": 63,
				"height": 9.75,
				"width": 49.5,
				"title": "配料单号：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 466.5,
				"top": 63,
				"height": 9.75,
				"width": 78,
				"field": "pldh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 69,
				"top": 76.5,
				"height": 9.75,
				"width": 337.5,
				"field": "gcmc"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 16.5,
				"top": 76.5,
				"height": 9.75,
				"width": 49.5,
				"title": "工程名称：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 415.5,
				"top": 76.5,
				"height": 9.75,
				"width": 49.5,
				"title": "塔型：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 466.5,
				"top": 76.5,
				"height": 9.75,
				"width": 78,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 69,
				"top": 91.5,
				"height": 9.75,
				"width": 475.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 16.5,
				"top": 91.5,
				"height": 9.75,
				"width": 49.5,
				"title": "备注：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 16.5,
				"top": 103.5,
				"height": 42,
				"width": 550,
				"textAlign": "center",
				"field": "tb",
				"columns": [
					[{
						"title": "存货名称",
						"field": "chmc",
						"width": 50.423168181293335,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "chmc"
					}, {
						"title": "材质",
						"field": "cz",
						"width": 52.13618712526753,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 50.38783213964593,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg"
					}, {
						"title": "宽(mm)",
						"field": "width",
						"width": 50.38808256357784,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "width"
					}, {
						"title": "长(mm)",
						"field": "length",
						"width": 50.386550436939366,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "length"
					}, {
						"title": "实领数量",
						"field": "slsl",
						"width": 50.392695190354694,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "slsl"
					}, {
						"title": "重量(kg)",
						"field": "zl",
						"width": 44.601185763804004,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 39.32187581558227,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "区域",
						"field": "",
						"width": 35.59929116308735,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": ""
					}, {
						"title": "机台",
						"field": "",
						"width": 31.38008803465775,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "jt"
					}, {
						"title": "计划用料时间",
						"field": "",
						"width": 45.18782186320156,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": ""
					}, {
						"title": "实发数量",
						"field": "",
						"width": 49.7952217225884,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": ""
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 186,
				"top": 360,
				"height": 9.75,
				"width": 61.5,
				"field": "shr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 276,
				"top": 360,
				"height": 9.75,
				"width": 40.5,
				"title": "批准人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 319.5,
				"top": 360,
				"height": 9.75,
				"width": 70.5,
				"field": "pzr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 147,
				"top": 360,
				"height": 9.75,
				"width": 37.5,
				"title": "审核人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 405,
				"top": 360,
				"height": 9.75,
				"width": 40.5,
				"title": "领料人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 58.5,
				"top": 360,
				"height": 9.75,
				"width": 57,
				"field": "zdr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 450,
				"top": 360,
				"height": 9.75,
				"width": 120,
				"field": "llr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 16.5,
				"top": 360,
				"height": 9.75,
				"width": 39,
				"title": "制单人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 186,
				"top": 375,
				"height": 9.75,
				"width": 61.5,
				"field": "shrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 147,
				"top": 375,
				"height": 9.75,
				"width": 37.5,
				"title": "日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 276,
				"top": 375,
				"height": 9.75,
				"width": 40.5,
				"title": "日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 58.5,
				"top": 375,
				"height": 9.75,
				"width": 57,
				"field": "zdrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 319.5,
				"top": 375,
				"height": 9.75,
				"width": 70.5,
				"field": "pzrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"fontFamily": "SimHei",
				"left": 16.5,
				"top": 375,
				"height": 9.75,
				"width": 39,
				"title": "日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 261,
		"paperNumberTop": 393,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
};