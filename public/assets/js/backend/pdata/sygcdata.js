let sygcdata = {
    "panels": [{
        "index": 0,
        "paperType": "A3",
        "height": 297,
        "width": 460,
        "paperHeader": 49.5,
        "paperFooter": 821.8897637795277,
        "printElements": [{
            "options": {
                "left": 456,
                "top": 15,
                "height": 19.5,
                "width": 192,
                "title": "绍兴电力设备试验过程检验",
                "fontSize": 15,
                "fontWeight": "bold",
                "textContentVerticalAlign": "middle"
            },
            "printElementType": {
                "type": "text"
            }
        },
        {
            "options": {
                "left": 28.5,
                "top": 55.5,
                "height": 36,
                "width": 1145,
				"field": "tb",
                "columns": [[{
                    "title": "零件部号",
                    "field": "parts",
                    "width": 75.67390976722267,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "parts"
                },
                {
                    "title": "材料名称",
                    "field": "stuff",
                    "width": 88.06916417240717,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "stuff"
                },
                {
                    "title": "材质",
                    "field": "material",
                    "width": 60.349082410738376,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "material"
                },
                {
                    "title": "规格",
                    "field": "specification",
                    "width": 94.08951772483425,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "specification"
                },
                {
                    "title": "长度",
                    "field": "length",
                    "width": 82.35329254807804,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "length"
                },
                {
                    "title": "焊接质量等级",
                    "field": "fire_name",
                    "width": 77.59482414932427,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "fire_name"
                },
                {
                    "title": "无损检测",
                    "field": "hg_count",
                    "width": 61.49879021811334,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "hg_count"
                },
                {
                    "title": "检测者",
                    "field": "processor",
                    "width": 132.13763763111206,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "processor"
                },
                {
                    "title": "镀锌层平均厚度",
                    "field": "ave_thick",
                    "width": 71.68638584540231,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "ave_thick"
                },
                {
                    "title": "镀锌层最小厚度",
                    "field": "min_thick",
                    "width": 77.62638302413258,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "min_thick"
                },
                {
                    "title": "镀锌层附着力",
                    "field": "adhesion",
                    "width": 63.77477523126932,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "adhesion"
                },
                {
                    "title": "镀锌层均匀性",
                    "field": "uniformity",
                    "width": 55.277468350410835,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "uniformity"
                },
                {
                    "title": "试装同心孔率",
                    "field": "con_por",
                    "width": 62.905350181887655,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "con_por"
                },
                {
                    "title": "试装构件就位率",
                    "field": "place_rate",
                    "width": 71.0504738198181,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "place_rate"
                },
                {
                    "title": "主要控制尺寸",
                    "field": "main_control",
                    "width": 70.91294492524916,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "main_control"
                }]]
            },
            "printElementType": {
                "title": "表格",
                "type": "tableCustom"
            }
        }],
        "paperNumberLeft": 1160,
        "paperNumberTop": 819
    }]
};