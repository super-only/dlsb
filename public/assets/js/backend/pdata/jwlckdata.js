let jwlckdata = {
	"panels": [{
		"index": 0,
		"height": 125,
		"width": 210,
		"paperHeader": 145.5,
		"paperFooter": 288,
		"printElements": [{
			"options": {
				"left": 6,
				"top": 33,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司",
				"fontSize": 18,
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 52.5,
				"height": 9.75,
				"width": 594,
				"title": "出库单",
				"fontSize": 18,
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 84,
				"top": 114,
				"height": 9.75,
				"width": 120,
				"field": "cklb",
				"fontSize": 10.5,
				"testData": "cklb"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 211.5,
				"top": 81,
				"height": 9.75,
				"width": 63,
				"title": "出库日期：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 16.5,
				"top": 114,
				"height": 9.75,
				"width": 66,
				"title": "出库类别：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 277.5,
				"top": 81,
				"height": 9.75,
				"width": 90,
				"field": "ckrq",
				"fontSize": 10.5,
				"testData": "ckrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 16.5,
				"top": 145.5,
				"height": 45,
				"width": 556.5,
				"field": "tb",
				"textAlign": "center",
				"fontSize": 10.5,
				"tableHeaderFontSize": 10.5,
				"columns": [
					[{
						"title": "存货编码",
						"field": "chbm",
						"width": 89.15437902008102,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "存货名称",
						"field": "chmc",
						"width": 93.93622313973135,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cpmc"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 140.34067315853065,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ggxh"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 29.6169702248595,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 47.405605167730805,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "单价(元)",
						"field": "dj",
						"width": 76.50273478129866,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl"
					}, {
						"title": "金额(元)",
						"field": "je",
						"width": 79.54341450776798,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 18,
				"top": 306,
				"height": 9.75,
				"width": 43.5,
				"title": "日期：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 63,
				"top": 306,
				"height": 9.75,
				"width": 120,
				"field": "zdrq",
				"testData": "zdrq",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 63,
				"top": 291,
				"height": 9.75,
				"width": 120,
				"field": "zdr",
				"fontSize": 10.5,
				"testData": "zdr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 409.5,
				"top": 291,
				"height": 9.75,
				"width": 43.5,
				"title": "批准人：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 291,
				"height": 9.75,
				"width": 43.5,
				"title": "制单人：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 16.5,
				"top": 81,
				"height": 9.75,
				"width": 66,
				"title": "出库单号：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 84,
				"top": 81,
				"height": 9.75,
				"width": 120,
				"field": "ckdh",
				"testData": "ckdh",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 211.5,
				"top": 97.5,
				"height": 9.75,
				"width": 63,
				"title": "领用部门：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 277.5,
				"top": 97.5,
				"height": 9.75,
				"width": 90,
				"field": "lybm",
				"testData": "lybm",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 16.5,
				"top": 97.5,
				"height": 9.75,
				"width": 66,
				"title": "领用人：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 84,
				"top": 97.5,
				"height": 9.75,
				"width": 120,
				"field": "lyr",
				"testData": "lyr",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 375,
				"top": 81,
				"height": 9.75,
				"width": 63,
				"title": "仓库：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 441,
				"top": 81,
				"height": 9.75,
				"width": 120,
				"field": "ck",
				"testData": "ck",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 375,
				"top": 97.5,
				"height": 9.75,
				"width": 63,
				"title": "领用班组：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 441,
				"top": 97.5,
				"height": 9.75,
				"width": 120,
				"field": "lybz",
				"testData": "lybz",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 211.5,
				"top": 114,
				"height": 9.75,
				"width": 63,
				"title": "工程：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 277.5,
				"top": 114,
				"height": 9.75,
				"width": 283.5,
				"field": "gc",
				"testData": "gc",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 225,
				"top": 291,
				"height": 9.75,
				"width": 43.5,
				"title": "审核人：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 270,
				"top": 291,
				"height": 9.75,
				"width": 120,
				"field": "shr",
				"testData": "shr",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 454.5,
				"top": 291,
				"height": 9.75,
				"width": 120,
				"field": "pzr",
				"testData": "pzr",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 225,
				"top": 307.5,
				"height": 9.75,
				"width": 43.5,
				"title": "日期：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 270,
				"top": 306,
				"height": 9.75,
				"width": 120,
				"field": "shrq",
				"testData": "shrq",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 409.5,
				"top": 306,
				"height": 9.75,
				"width": 43.5,
				"title": "日期：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 454.5,
				"top": 306,
				"height": 9.75,
				"width": 120,
				"field": "pzrq",
				"testData": "pzrq",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 16.5,
				"top": 130.5,
				"height": 9.75,
				"width": 66,
				"title": "备注：",
				"fontSize": 10.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 84,
				"top": 130.5,
				"height": 9.75,
				"width": 477,
				"field": "bz",
				"testData": "bz",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 459,
		"paperNumberTop": 31.5,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}