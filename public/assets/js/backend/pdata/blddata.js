let blddata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 55.5,
		"paperFooter": 771,
		"printElements": [{
			"options": {
				"left": 22.5,
				"top": 36,
				"height": 9.75,
				"width": 550.5,
				"field": "bjlx",
				"fontSize": 18,
				"lineHeight": 18,
				"fontWeight": "900",
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		},   {
			"options": {
				"left": 313.5,
				"top": 774,
				"height": 9.75,
				"width": 39,
				"title": "总数量："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 432,
				"top": 774,
				"height": 9.75,
				"width": 39,
				"title": "总重量："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 468,
				"top": 774,
				"height": 9.75,
				"width": 94.5,
				"field": "zzl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 354,
				"top": 774,
				"height": 9.75,
				"width": 85.5,
				"field": "zsl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 313.5,
				"top": 792,
				"height": 9.75,
				"width": 120,
				"title": "经办人："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 168,
				"top": 792,
				"height": 9.75,
				"width": 28.5,
				"title": "审核："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 195,
				"top": 792,
				"height": 9.75,
				"width": 97.5,
				"field": "sh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 51,
				"top": 792,
				"height": 9.75,
				"width": 100.5,
				"field": "zb"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 433.5,
				"top": 792,
				"height": 9.75,
				"width": 120,
				"title": "业务员："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 792,
				"height": 9.75,
				"width": 28.5,
				"title": "制表："
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 442.5,
		"paperNumberTop": 811.5,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}
let blddata2 = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 43.5,
		"paperFooter": 645,
		"printElements": [
		{
			"options": {
				"left": 244.5,
				"top": 18,
				"height": 9.75,
				"width": 120,
				"title": "标题"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 34.5,
				"top": 49.5,
				"height": 9.75,
				"width": 120,
				"title": "备注"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 423,
				"top": 49.5,
				"height": 9.75,
				"width": 120,
				"title": "日期"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 63,
				"height": 20,
				"width": 550,
				"field": "tb",
				"textAlign": "center",
				"tableBodyCellBorder": "noBorder",
				"columns": [
					[{
						"title": "行1",
						"field": "aa",
						"width": 275,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "行2",
						"field": "bb",
						"width": 275,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}],
		"paperNumberLeft": 565,
		"paperNumberTop": 819
	}]
}

let pdata = {
	"panels": [{
		"index": 0,
		"paperType": "A5",
		"height": 210,
		"width": 148,
		"paperHeader": 84,
		"paperFooter": 595.2755905511812,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 27,
				"height": 9.75,
				"width": 418.5,
				"title": "绍兴电力设备有限公司",
				"fontSize": 21.75,
				"fontWeight": "900",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 36,
				"top": 25,
				"height": 50.5,
				"width": 34.5,
				"title": "补件",
				"fontSize": 21,
				"textAlign": "center",
				"textContentVerticalAlign": "middle",
				"borderLeft": "solid",
				"borderTop": "solid",
				"borderRight": "solid",
				"borderBottom": "solid",
				"fontWeight": "600",
				"lineHeight": 21
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 52.5,
				"height": 9.75,
				"width": 418.5,
				"title": "部件移动卡片",
				"fontSize": 17.25,
				"fontWeight": "600",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 99,
				"top": 70.5,
				"height": 9,
				"width": 219,
				"borderWidth": "2.25"
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 147,
				"top": 81,
				"height": 18,
				"width": 58.5,
				"field": "val3",
				"testData": "铲背1",
				"fontSize": 15.75,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 211.5,
				"top": 81,
				"height": 18,
				"width": 58.5,
				"field": "val4",
				"testData": "制弯1",
				"fontSize": 15.75,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 274.5,
				"top": 81,
				"height": 18,
				"width": 58.5,
				"field": "val5",
				"testData": "清根1",
				"fontSize": 15.75,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 84,
				"top": 81,
				"height": 18,
				"width": 58.5,
				"field": "val2",
				"testData": "电焊1",
				"fontSize": 15.75,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 337.5,
				"top": 81,
				"height": 18,
				"width": 58.5,
				"field": "val6",
				"testData": "开合角1",
				"fontSize": 15.75,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 81,
				"height": 18,
				"width": 58.5,
				"field": "val1",
				"testData": "切角1",
				"fontSize": 15.75,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 84,
				"top": 102,
				"height": 18,
				"width": 58.5,
				"field": "val8",
				"testData": "钻/扩孔",
				"fontSize": 15.75,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 147,
				"top": 102,
				"height": 18,
				"width": 58.5,
				"field": "val9",
				"testData": "打坡口1",
				"fontSize": 15.75,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 102,
				"height": 18,
				"width": 58.5,
				"field": "val7",
				"testData": "打扁1",
				"fontSize": 17.25,
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 130.5,
				"height": 9.75,
				"width": 120,
				"title": "套用塔型"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 65,
				"top": 128.5,
				"height": 9.75,
				"width": 300,
				"fontSize": 15,"fontWeight": "bold", 
				"field": "ty_tower"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 247,
				"top": 130.5,
				"height": 9.75,
				"width": 120,
				"title": "生产下达单号"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 308,
				"top": 128.5,
				"height": 9.75,
				"width": 300,
				"fontSize": 15,"fontWeight": "bold", 
				"field": "pt_num"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 70.5,
				"top": 144,
				"height": 145.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 171,
				"top": 144,
				"height": 18,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 210,
				"top": 144,
				"height": 18,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 21,
				"top": 144,
				"height": 145.5,
				"width": 381
			},
			"printElementType": {
				"type": "rect"
			}
		}, {
			"options": {
				"left": 75,
				"top": 148.5,
				"height": 9.75,
				"width": 93,
				"fontSize": 15,"fontWeight": "bold", 
				"field": "djh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 175.5,
				"top": 148.5,
				"height": 9.75,
				"width": 31.5,
				"title": "段",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 148.5,
				"height": 9.75,
				"width": 40.5,
				"title": "单据号",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 226.5,
				"top": 148.5,
				"height": 9.75,
				"width": 120,
				"fontSize": 15,"fontWeight": "bold",
				"field": "duan"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 162,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 75,
				"top": 166.5,
				"height": 9.75,
				"width": 313.5,
				"fontSize": 12,"fontWeight": "bold", 
				"field": "gcmc"
			},
			"printElementType": {
				"type": "text",
				formatter:function(title,value,options,templateData,target){
                    if(value.length>20){
                        return "<div style='font-size: 12px'>"+value+"</div>";
                    }else{
                        return value;
                    }
                }
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 166.5,
				"height": 9.75,
				"width": 40.5,
				"title": "工程名称"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 171,
				"top": 180,
				"height": 109.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 289.5,
				"top": 180,
				"height": 109.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 210,
				"top": 180,
				"height": 109.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 330,
				"top": 180,
				"height": 109.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 21,
				"top": 180,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 175.5,
				"top": 184.5,
				"height": 9.75,
				"width": 30,
				"title": "基数",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 214.5,
				"top": 184.5,
				"height": 9.75,
				"width": 70.5,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "jishu"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 184.5,
				"height": 9.75,
				"width": 94.5,
				"fontSize": 10,"fontWeight": "bold", "textAlign": "center",
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 294,
				"top": 184.5,
				"height": 9.75,
				"width": 31.5,
				"title": "件数",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 184.5,
				"height": 9.75,
				"width": 40.5,
				"title": "塔型",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 334.5,
				"top": 184.5,
				"height": 9.75,
				"width": 55.5,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "jianshu"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 198,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 175.5,
				"top": 202.5,
				"height": 9.75,
				"width": 30,
				"title": "材料",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 214.5,
				"top": 202.5,
				"height": 9.75,
				"width": 70.5,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "cl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 202.5,
				"height": 9.75,
				"width": 93,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "jianhao"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 202.5,
				"height": 9.75,
				"width": 40.5,
				"title": "件号",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 334.5,
				"top": 207,
				"height": 21,
				"width": 55.5,
				"field": "cz",
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				// "fontSize": 12.75
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 294,
				"top": 210,
				"height": 15,
				"width": 31.5,
				"title": "材质",
				"fontSize": 12.75,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 216,
				"height": 9,
				"width": 268.5
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 175.5,
				"top": 220.5,
				"height": 9.75,
				"width": 30,
				"title": "长度",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 220.5,
				"height": 9.75,
				"width": 93,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "gg"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 220.5,
				"height": 9.75,
				"width": 40.5,
				"title": "规格",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 214.5,
				"top": 220.5,
				"height": 9.75,
				"width": 70.5,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "cd"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 234,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 75,
				"top": 238.5,
				"height": 9.75,
				"width": 93,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "hd"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 214.5,
				"top": 238.5,
				"height": 9.75,
				"width": 70.5,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "kd"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 175.5,
				"top": 238.5,
				"height": 9.75,
				"width": 30,
				"title": "宽度",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 294,
				"top": 238.5,
				"height": 9.75,
				"width": 31.5,
				"title": "炉批号",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 238.5,
				"height": 9.75,
				"width": 40.5,
				"title": "厚度",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 334.5,
				"top": 238.5,
				"height": 9.75,
				"width": 55.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 252,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 175.5,
				"top": 256.5,
				"height": 9.75,
				"width": 30,
				"title": "总孔数",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 214.5,
				"top": 256.5,
				"height": 9.75,
				"width": 70.5,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "zks"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 256.5,
				"height": 9.75,
				"width": 93,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "djks"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 294,
				"top": 256.5,
				"height": 9.75,
				"width": 31.5,
				"title": "重量",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 256.5,
				"height": 9.75,
				"width": 40.5,
				"title": "单件孔数",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 334.5,
				"top": 256.5,
				"height": 9.75,
				"width": 55.5,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "zl"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 270,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 175.5,
				"top": 274.5,
				"height": 9.75,
				"width": 30,
				"title": "审核",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 274.5,
				"height": 9.75,
				"width": 93,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "tianxie"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 214.5,
				"top": 274.5,
				"height": 9.75,
				"width": 70.5,
				"fontSize": 15,"fontWeight": "bold", "textAlign": "center",
				"field": "sh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 274.5,
				"height": 9.75,
				"width": 40.5,
				"title": "填写",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 294,
				"top": 274.5,
				"height": 9.75,
				"width": 31.5,
				"title": "日期",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 334.5,
				"top": 274.5,
				"height": 9.75,
				"width": 55.5,
				"field": "rq",
				"fontSize": 8, "textAlign": "center",
				"testData": "2022-06-08"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 298.5,
				"height": 223.5,
				"width": 381
			},
			"printElementType": {
				"type": "rect"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 303,
				"height": 9.75,
				"width": 39,
				"title": "备注",
				"fontSize": 12,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 316.5,
				"height": 9.75,
				"width": 367.5,
				"fontSize": 15,
				"fontWeight": "bold",
				"field": "bz"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 343.5,
				"top": 355.5,
				"height": 18,
				"width": 52.5,
				"title": "受控",
				"fontSize": 15.75,
				"textAlign": "center",
				"textContentVerticalAlign": "middle",
				"borderLeft": "solid",
				"borderTop": "solid",
				"borderRight": "solid",
				"borderBottom": "solid"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 163.5,
				"top": 376.5,
				"height": 145.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 211.5,
				"top": 376.5,
				"height": 145.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 261,
				"top": 376.5,
				"height": 145.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 117,
				"top": 376.5,
				"height": 145.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 307.5,
				"top": 376.5,
				"height": 145.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 70.5,
				"top": 376.5,
				"height": 145.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 354,
				"top": 376.5,
				"height": 145.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 21,
				"top": 376.5,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 123,
				"top": 381,
				"height": 9.75,
				"width": 34.5,
				"title": "质检员",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 76.5,
				"top": 381,
				"height": 9.75,
				"width": 34.5,
				"title": "加工者",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 219,
				"top": 381,
				"height": 9.75,
				"width": 34.5,
				"title": "工序",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 267,
				"top": 381,
				"height": 9.75,
				"width": 34.5,
				"title": "加工者",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 171,
				"top": 381,
				"height": 9.75,
				"width": 34.5,
				"title": "日期",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 313.5,
				"top": 381,
				"height": 9.75,
				"width": 34.5,
				"title": "质检员",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 381,
				"height": 9.75,
				"width": 34.5,
				"title": "工序",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 361.5,
				"top": 381,
				"height": 9.75,
				"width": 34.5,
				"title": "日期",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 394.5,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 399,
				"height": 9.75,
				"width": 34.5,
				"title": "下料",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 219,
				"top": 399,
				"height": 9.75,
				"width": 34.5,
				"title": "制弯",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 412.5,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 417,
				"height": 9.75,
				"width": 34.5,
				"title": "剪切",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 219,
				"top": 417,
				"height": 9.75,
				"width": 34.5,
				"title": "开合角",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 430.5,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 435,
				"height": 9.75,
				"width": 34.5,
				"title": "矫正",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 219,
				"top": 435,
				"height": 9.75,
				"width": 34.5,
				"title": "打扁",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 448.5,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 453,
				"height": 9.75,
				"width": 34.5,
				"title": "冲孔",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 219,
				"top": 453,
				"height": 9.75,
				"width": 34.5,
				"title": "清根",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 466.5,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 471,
				"height": 9.75,
				"width": 34.5,
				"title": "钻孔",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 219,
				"top": 471,
				"height": 9.75,
				"width": 34.5,
				"title": "铲背",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 484.5,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 489,
				"height": 9.75,
				"width": 34.5,
				"title": "钢印",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 219,
				"top": 489,
				"height": 9.75,
				"width": 34.5,
				"title": "焊接",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 502.5,
				"height": 9,
				"width": 381
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 507,
				"height": 9.75,
				"width": 34.5,
				"title": "切角",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 538.5,
				"height": 9.75,
				"width": 418.5,
				"field": "pageinfo",
				"testData": "第1页，共1页",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 389,
		"paperNumberTop": 573,
		"paperNumberDisabled": true,
		"paperNumberFormat": "第paperNo页，共paperCount页",
		"rotate": true
	}]
}

let topword = {
	hq:{
		"options": {
			"left": 210,
			"top": 51,
			"height": 18,
			"width": 58.5,
			"fontSize": 15.75,
			"textAlign": "center",
			"textContentVerticalAlign": "middle",
			"borderLeft": "solid",
			"borderTop": "solid",
			"borderRight": "solid",
			"borderBottom": "solid",
			"title": "制弯"
		},
		"printElementType": {
			"type": "text"
		}
	},
	qg: {
		"options": {
			"left": 273,
			"top": 51,
			"height": 18,
			"width": 58.5,
			"fontSize": 15.75,
			"textAlign": "center",
			"textContentVerticalAlign": "middle",
			"borderLeft": "solid",
			"borderTop": "solid",
			"borderRight": "solid",
			"borderBottom": "solid",
			"title": "清根"
		},
		"printElementType": {
			"type": "text"
		}
	}, 
	khj:{
		"options": {
			"left": 336,
			"top": 51,
			"height": 18,
			"width": 58.5,
			"fontSize": 15.75,
			"textAlign": "center",
			"textContentVerticalAlign": "middle",
			"borderLeft": "solid",
			"borderTop": "solid",
			"borderRight": "solid",
			"borderBottom": "solid",
			"title": "开合角"
		},
		"printElementType": {
			"type": "text"
		}
	}, 
	cb:{
		"options": {
			"left": 147,
			"top": 51,
			"height": 18,
			"width": 58.5,
			"fontSize": 15.75,
			"textAlign": "center",
			"textContentVerticalAlign": "middle",
			"borderLeft": "solid",
			"borderTop": "solid",
			"borderRight": "solid",
			"borderBottom": "solid",
			"title": "铲背"
		},
		"printElementType": {
			"type": "text"
		}
	}, 
	db:{
		"options": {
			"left": 399,
			"top": 51,
			"height": 18,
			"width": 58.5,
			"fontSize": 17.25,
			"textAlign": "center",
			"textContentVerticalAlign": "middle",
			"borderLeft": "solid",
			"borderTop": "solid",
			"borderRight": "solid",
			"borderBottom": "solid",
			"title": "打扁"
		},
		"printElementType": {
			"type": "text"
		}
	}, 
	dh:{
		"options": {
			"left": 84,
			"top": 51,
			"height": 18,
			"width": 58.5,
			"fontSize": 15.75,
			"textAlign": "center",
			"textContentVerticalAlign": "middle",
			"borderLeft": "solid",
			"borderTop": "solid",
			"borderRight": "solid",
			"borderBottom": "solid",
			"title": "电焊"
		},
		"printElementType": {
			"type": "text"
		}
	}, 
	zk:{
		"options": {
			"left": 462,
			"top": 51,
			"height": 18,
			"width": 58.5,
			"fontSize": 15.75,
			"textAlign": "center",
			"textContentVerticalAlign": "middle",
			"borderLeft": "solid",
			"borderTop": "solid",
			"borderRight": "solid",
			"borderBottom": "solid",
			"title": "钻/扩孔"
		},
		"printElementType": {
			"type": "text"
		}
	}, 
	dpk:{
		"options": {
			"left": 525,
			"top": 51,
			"height": 18,
			"width": 58.5,
			"fontSize": 15.75,
			"textAlign": "center",
			"textContentVerticalAlign": "middle",
			"borderLeft": "solid",
			"borderTop": "solid",
			"borderRight": "solid",
			"borderBottom": "solid",
			"title": "打坡口"
		},
		"printElementType": {
			"type": "text"
		}
	},
	qj:{
		"options": {
			"left": 21,
			"top": 51,
			"height": 18,
			"width": 58.5,
			"fontSize": 15.75,
			"textAlign": "center",
			"textContentVerticalAlign": "middle",
			"borderLeft": "solid",
			"borderTop": "solid",
			"borderRight": "solid",
			"borderBottom": "solid",
			"title": "切角"
		},
		"printElementType": {
			"type": "text"
		}
	},
}  