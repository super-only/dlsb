let scrwddata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 243,
		"paperFooter": 760.5,
		"printElements": [{
			"options": {
				"left": 46.5,
				"top": 45,
				"height": 82.5,
				"width": 88.5,
				"src": "/assets/img/qr_img.png"
			},
			"printElementType": {
				"type": "image"
			}
		}, {
			"options": {
				"left": 0,
				"top": 48,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司",
				"fontSize": 18,
				"fontWeight": "900",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 75,
				"height": 9.75,
				"width": 594,
				"title": "生产任务单",
				"fontSize": 21.75,
				"fontWeight": "900",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 108,
				"top": 133.5,
				"height": 9.75,
				"width": 234,
				"field": "rwdh",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 370.5,
				"top": 133.5,
				"height": 9.75,
				"width": 78,
				"title": "下单日期：",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 133.5,
				"height": 9.75,
				"width": 120,
				"field": "xdrq",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 133.5,
				"height": 9.75,
				"width": 79.5,
				"title": "任务单号：",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 417,
				"top": 147,
				"height": 96,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 87,
				"top": 147,
				"height": 96,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 499.5,
				"top": 147,
				"height": 96,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 24,
				"top": 147,
				"height": 9,
				"width": 549
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 571.5,
				"top": 147,
				"height": 97.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 24,
				"top": 147,
				"height": 97.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 91.5,
				"top": 153,
				"height": 9.75,
				"width": 316.5,
				"field": "gcmc",
				"fontSize": 10.5,
				"lineHeight": 12.75
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 504,
				"top": 153,
				"height": 9.75,
				"width": 61.5,
				"title": "工程编号",
				"fontSize": 12.75,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 33,
				"top": 153,
				"height": 9.75,
				"width": 45,
				"title": "工程名称",
				"textAlign": "justify",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 429,
				"top": 153,
				"height": 9.75,
				"width": 61.5,
				"title": "合同编号",
				"fontSize": 12.75,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 177,
				"height": 9,
				"width": 549
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 91.5,
				"top": 181.5,
				"height": 9.75,
				"width": 315,
				"field": "khmc",
				"fontSize": 12.75
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 427.5,
				"top": 184.5,
				"height": 9.75,
				"width": 63,
				"field": "htbh",
				"fontSize": 10.5,
				"textAlign": "center",
				"testData": "htbh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 33,
				"top": 181.5,
				"height": 9.75,
				"width": 45,
				"title": "客户名称",
				"textAlign": "justify",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 504,
				"top": 184.5,
				"height": 9.75,
				"width": 63,
				"field": "gcbh",
				"fontSize": 10.5,
				"textAlign": "center",
				"testData": "gcbh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 201,
				"height": 9,
				"width": 549
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 91.5,
				"top": 207,
				"height": 9.75,
				"width": 315,
				"field": "sm",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 513,
				"top": 207,
				"height": 9.75,
				"width": 48,
				"title": "业务员",
				"fontSize": 12.75,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 33,
				"top": 207,
				"height": 9.75,
				"width": 45,
				"title": "说明",
				"textAlign": "justify",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 427.5,
				"top": 208.5,
				"height": 9.75,
				"width": 60,
				"title": "交货时间",
				"fontSize": 12.75,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 417,
				"top": 222,
				"height": 9,
				"width": 154.5
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 423,
				"top": 226.5,
				"height": 9.75,
				"width": 75,
				"field": "jhsj",
				"fontSize": 10.5,
				"textAlign": "center",
				"testData": "jhsj"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 507,
				"top": 226.5,
				"height": 9.75,
				"width": 58.5,
				"field": "ywy",
				"fontSize": 12.75,
				"textAlign": "center",
				"testData": "ywy"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 243,
				"height": 57,
				"width": 549,
				"field": "tb",
				"textAlign": "center",
				"tableBodyRowHeight": 24.75,
				"fontSize": 10.5,
				"tableHeaderRowHeight": 26.25,
				"tableHeaderFontSize": 10.5,
				"columns": [
					[{
						"title": "杆塔号",
						"field": "gth",
						"width": 63.06231627965338,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gth"
					}, {
						"title": "塔型/型号",
						"field": "tx",
						"width": 117.430655733122,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "tx"
					}, {
						"title": "呼高",
						"field": "hg",
						"width": 36.83672524629362,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "hg"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 36.83429194072424,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 30.122451382392637,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "重量（Kg)",
						"field": "zl",
						"width": 67.85693881232467,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl"
					}, {
						"title": "单价",
						"field": "dj",
						"width": 56.756894948423295,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dj"
					}, {
						"title": "总价",
						"field": "zj",
						"width": 77.41630543643473,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zj"
					}, {
						"title": "备注",
						"field": "bz",
						"width": 62.68342022063135,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 24,
				"top": 769.5,
				"height": 9.75,
				"width": 364.5,
				"field": "xd",
				"testData": "xd",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 787.5,
				"height": 9.75,
				"width": 120,
				"field": "zdr",
				"testData": "zdr",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 252,
				"top": 787.5,
				"height": 9.75,
				"width": 120,
				"field": "spr",
				"testData": "spr",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 381,
				"top": 787.5,
				"height": 9.75,
				"width": 52.5,
				"title": "关闭人：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 202.5,
				"top": 787.5,
				"height": 9.75,
				"width": 51,
				"title": "审批人：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 435,
				"top": 787.5,
				"height": 9.75,
				"width": 120,
				"field": "gbr",
				"testData": "gbr",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 787.5,
				"height": 9.75,
				"width": 49.5,
				"title": "制单人：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 261,
		"paperNumberTop": 817.5,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}

// 铁附件
let scrwddata2 = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 243,
		"paperFooter": 760.5,
		"printElements": [{
			"options": {
				"left": 46.5,
				"top": 45,
				"height": 82.5,
				"width": 88.5,
				"src": "/assets/img/qr_img.png"
			},
			"printElementType": {
				"type": "image"
			}
		}, {
			"options": {
				"left": 0,
				"top": 48,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司",
				"fontSize": 18,
				"fontWeight": "900",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 75,
				"height": 9.75,
				"width": 594,
				"title": "生产任务单",
				"fontSize": 21.75,
				"fontWeight": "900",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 108,
				"top": 133.5,
				"height": 9.75,
				"width": 234,
				"field": "rwdh",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 370.5,
				"top": 133.5,
				"height": 9.75,
				"width": 78,
				"title": "下单日期：",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 133.5,
				"height": 9.75,
				"width": 120,
				"field": "xdrq",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 133.5,
				"height": 9.75,
				"width": 79.5,
				"title": "任务单号：",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 417,
				"top": 147,
				"height": 96,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 87,
				"top": 147,
				"height": 96,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 499.5,
				"top": 147,
				"height": 96,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 24,
				"top": 147,
				"height": 9,
				"width": 549
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 571.5,
				"top": 147,
				"height": 97.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 24,
				"top": 147,
				"height": 97.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 91.5,
				"top": 153,
				"height": 9.75,
				"width": 316.5,
				"field": "gcmc",
				"fontSize": 10.5,
				"lineHeight": 12.75
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 33,
				"top": 153,
				"height": 9.75,
				"width": 45,
				"title": "工程名称",
				"textAlign": "justify",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 504,
				"top": 153,
				"height": 9.75,
				"width": 61.5,
				"title": "工程编号",
				"fontSize": 12.75,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 417,
				"top": 222,
				"height": 9,
				"width": 154.5
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 429,
				"top": 153,
				"height": 9.75,
				"width": 61.5,
				"title": "合同编号",
				"fontSize": 12.75,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 504,
				"top": 184.5,
				"height": 9.75,
				"width": 63,
				"field": "gcbh",
				"fontSize": 10.5,
				"textAlign": "center",
				"testData": "gcbh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 427.5,
				"top": 208.5,
				"height": 9.75,
				"width": 60,
				"title": "交货时间",
				"fontSize": 12.75,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 513,
				"top": 207,
				"height": 9.75,
				"width": 48,
				"title": "业务员",
				"fontSize": 12.75,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 177,
				"height": 9,
				"width": 549
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 91.5,
				"top": 181.5,
				"height": 9.75,
				"width": 316.5,
				"field": "khmc",
				"fontSize": 12.75
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 33,
				"top": 181.5,
				"height": 9.75,
				"width": 45,
				"title": "客户名称",
				"textAlign": "justify",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 427.5,
				"top": 184.5,
				"height": 9.75,
				"width": 63,
				"field": "htbh",
				"fontSize": 10.5,
				"textAlign": "center",
				"testData": "htbh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 423,
				"top": 226.5,
				"height": 9.75,
				"width": 75,
				"field": "jhsj",
				"fontSize": 10.5,
				"textAlign": "center",
				"testData": "jhsj"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 507,
				"top": 226.5,
				"height": 9.75,
				"width": 58.5,
				"field": "ywy",
				"fontSize": 12.75,
				"textAlign": "center",
				"testData": "ywy"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 201,
				"height": 9,
				"width": 549
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 91.5,
				"top": 207,
				"height": 9.75,
				"width": 316.5,
				"field": "sm",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 33,
				"top": 207,
				"height": 9.75,
				"width": 45,
				"title": "说明",
				"textAlign": "justify",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 243,
				"height": 57,
				"width": 549,
				"field": "tb",
				"textAlign": "center",
				"tableBodyRowHeight": 24.75,
				"fontSize": 10.5,
				"tableHeaderRowHeight": 26.25,
				"tableHeaderFontSize": 10.5,
				"columns": [
					[{
						"title": "杆塔号",
						"field": "gth",
						"width": 62.92919747693335,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gth"
					}, {
						"title": "塔型/型号",
						"field": "tx",
						"width": 119.79408894065324,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "tx"
					}, {
						"title": "呼高",
						"field": "hg",
						"width": 32.35630456119445,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "hg"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 32.35349618714578,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 32.351587533025505,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "单重",
						"field": "dz",
						"width": 47.16967950693379,
						"colspan": 1,
						"rowspan": 1,
						"checked": true
					}, {
						"title": "重量（Kg)",
						"field": "zl",
						"width": 57.40132420333782,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl"
					}, {
						"title": "单价",
						"field": "dj",
						"width": 48.01161067285731,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dj"
					}, {
						"title": "总价",
						"field": "zj",
						"width": 65.4877529808978,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zj"
					}, {
						"title": "备注",
						"field": "bz",
						"width": 53.02495793702098,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 24,
				"top": 769.5,
				"height": 9.75,
				"width": 364.5,
				"field": "xd",
				"testData": "xd",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 787.5,
				"height": 9.75,
				"width": 120,
				"field": "zdr",
				"testData": "zdr",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 252,
				"top": 787.5,
				"height": 9.75,
				"width": 120,
				"field": "spr",
				"testData": "spr",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 381,
				"top": 787.5,
				"height": 9.75,
				"width": 52.5,
				"title": "关闭人：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 202.5,
				"top": 787.5,
				"height": 9.75,
				"width": 51,
				"title": "审批人：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 435,
				"top": 787.5,
				"height": 9.75,
				"width": 120,
				"field": "gbr",
				"testData": "gbr",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 787.5,
				"height": 9.75,
				"width": 49.5,
				"title": "制单人：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 261,
		"paperNumberTop": 817.5,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
};