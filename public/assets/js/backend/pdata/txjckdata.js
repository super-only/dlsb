let txjckdata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 105,
		"paperFooter": 760.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 43.5,
				"height": 22.5,
				"width": 594,
				"title": "放样原始材料表",
				"fontSize": 18,
				"textDecoration": "underline",
				"textAlign": "center",
				"textContentVerticalAlign": "middle"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 234,
				"top": 87,
				"height": 12,
				"width": 63,
				"field": "dw",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 198,
				"top": 87,
				"height": 12,
				"width": 34.5,
				"title": "段位：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 322.5,
				"top": 87,
				"height": 12,
				"width": 33,
				"title": "基数：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 73.5,
				"top": 87,
				"height": 12,
				"width": 118.5,
				"field": "tx",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 357,
				"top": 87,
				"height": 12,
				"width": 63,
				"field": "js",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 34.5,
				"top": 87,
				"height": 12,
				"width": 37.5,
				"title": "塔型：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 453,
				"top": 87,
				"height": 12,
				"width": 36,
				"title": "日期：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 489,
				"top": 87,
				"height": 12,
				"width": 76.5,
				"field": "rq",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 15,
				"top": 105,
				"height": 277.5,
				"width": 564,
				"field": "tb",
				"textAlign": "center",
				"tableFooterRepeat": "page",
				"fontSize": 9.75,
				"tableHeaderRowHeight": 24,
				"tableBodyRowHeight": 24,
				"columns": [
					[{
						"title": "零件编号",
						"field": "ljbh",
						"width": 43.510088149316935,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ljbh"
					}, {
						"title": "材质",
						"field": "cz",
						"width": 34.68039290238309,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 58.86369381918181,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg"
					}, {
						"title": "长度(mm)",
						"field": "cd",
						"width": 29.78511931086724,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cd"
					}, {
						"title": "宽度(mm)",
						"field": "kd",
						"width": 29.943758311424666,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "kd"
					}, {
						"title": "厚度(mm)",
						"field": "DtMD_iTorch",
						"width": 29.943758311424666,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "DtMD_iTorch"
					}, {
						"title": "类型",
						"field": "type",
						"width": 29.943758311424666,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "type"
					}, {
						"title": "单基数量",
						"field": "djsl",
						"width": 26.783602146801986,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "djsl"
					}, {
						"title": "单基重量",
						"field": "djzl",
						"width": 40.479127855480286,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "djzl"
					}, {
						"title": "孔数",
						"field": "ks",
						"width": 31.27677287287164,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ks"
					}, {
						"title": "电焊",
						"field": "dh",
						"width": 15.110042888162253,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dh"
					}, {
						"title": "制弯",
						"field": "wq",
						"width": 15.155631540807713,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "wq"
					}, {
						"title": "切角",
						"field": "qj",
						"width": 15.10448713729342,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "qj"
					}, {
						"title": "铲背",
						"field": "cb",
						"width": 15.112842833157504,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cb"
					}, {
						"title": "清根",
						"field": "qg",
						"width": 15.151340730108354,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "qg"
					}, {
						"title": "打扁",
						"field": "db",
						"width": 15.192322362346387,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "db"
					}, {
						"title": "开合角",
						"field": "khj",
						"width": 22.696963762249545,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "khj"
					}, {
						"title": "割豁",
						"field": "DtMD_GeHuo",
						"width": 15.19586406317535,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "DtMD_GeHuo"
					}, {
						"title": "钻孔",
						"field": "zk",
						"width": 15.19586406317535,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zk"
					}, {
						"title": "备注",
						"field": "bz",
						"width": 64.95794931437186,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 15,
				"top": 765,
				"height": 9,
				"width": 565.5
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 238.5,
				"top": 774,
				"height": 9.75,
				"width": 34.5,
				"title": "审核：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 273,
				"top": 774,
				"height": 9.75,
				"width": 90,
				"field": "sh",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 70.5,
				"top": 774,
				"height": 9.75,
				"width": 97.5,
				"field": "zb",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 442.5,
				"top": 774,
				"height": 9.75,
				"width": 120,
				"field": "page",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 37.5,
				"top": 774,
				"height": 9.75,
				"width": 33,
				"title": "制表：",
				"fontSize": 10.5
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 486,
		"paperNumberTop": 772.5,
		"paperNumberDisabled": true,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
};