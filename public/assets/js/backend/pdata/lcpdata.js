function createrect(left,top,text){
    return [{
        "options": {"left": left, "top": top, "height": 22, "width": 50},
        "printElementType": {"type": "rect"}
    }, {
        "options": {
            "left": left,
            "top": top+5,
            "height": 22,
            "width": 50,
            "title": text,
            "fontSize": 15,
            "fontWeight": "bold",
            "textAlign": "center"
        }, "printElementType": {"type": "text"}
    }];
}

let eleadd={
    'sk':createrect(250,330,'受控'),
    'sz':createrect(80,30,'试制'),
    'dh':createrect(22,60,'电焊'),
    'qj':createrect(82,60,'切角'),
    'hq':createrect(142,60,'制弯'),
};

let pdata = {
    "panels": [{
        "index": 0,
        "paperType": "A5",
        "height": 210,
        "width": 148,
        "paperHeader": 75,
        "paperFooter": 595.2755905511812,
        "printElements": [{
            "options": {
                "left": 88.5,
                "top": 18,
                "height": 9.75,
                "width": 241.5,
                "title": "绍兴电力设备有限公司",
                "fontSize": 20.25,
                "fontWeight": "bold",
                "textAlign": "center",
                "textContentVerticalAlign": "middle"
            }, "printElementType": {"type": "text"}
        }, {
            "options": {
                "left": 322.5,
                "top": 24,
                "height": 9.75,
                "width": 85.5,
                "title": "SDS 1009-03",
                "fontSize": 12,
                "fontWeight": "bold"
            }, "printElementType": {"type": "text"}
        }, {
            "options": {
                "left": 139.5,
                "top": 40.5,
                "height": 9.75,
                "width": 147,
                "title": "部件移动卡片",
                "fontSize": 18,
                "fontWeight": "bold",
                "textAlign": "justify"
            }, "printElementType": {"type": "text"}
        }, {
            "options": {"left": 88.5, "top": 57, "height": 9, "width": 241.5, "borderWidth": "2.25"},
            "printElementType": {"type": "hline"}
        }, {
            "options": {"left": 22.5, "top": 90, "height": 9.75, "width": 55.5, "title": "套用塔型", "fontSize": 12},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 250, "top": 90, "height": 9.75, "width": 130.5, "field": "dzm", "fontSize": 12},
            "printElementType": {"type": "text"}
        }, {
            "options": {
                "left": 22.5,
                "top": 105,
                "height": 165,
                "width": 376.5,
                "tableHeaderBackground": "#ffffff",
                "field": "table1",
                "columns": [[{
                    "title": "单据号",
                    "width": 73.20108855474025,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 84.3886661332634,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "PT_Num"
                }, {
                    "title": "段",
                    "width": 62.43155485136131,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 49.3294447041711,
                    "colspan": 3,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "dh"
                }], [{
                    "title": "工程名称",
                    "width": 73.20108855474025,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 84.3886661332634,
                    "colspan": 5,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "C_ProjectName"
                }], [{
                    "title": "塔型",
                    "width": 73.20108855474025,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 84.3886661332634,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "TD_TypeName"
                }, {
                    "title": "基数",
                    "width": 62.43155485136131,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 49.3294447041711,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "DtMD_iUnitHoleCount"
                }, {
                    "title": "件数",
                    "width": 28.573132201723723,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 78.57611355474026,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "PTD_Count"
                }], [{
                    "title": "件号",
                    "width": 73.20108855474025,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 84.3886661332634,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "DtMD_sPartsID"
                }, {
                    "title": "材料",
                    "width": 62.43155485136131,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 49.3294447041711,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "cl"
                }, {
                    "title": "材质",
                    "width": 100,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 2,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 78.57611355474026,
                    "colspan": 1,
                    "rowspan": 2,
                    "checked": true,
                    "columnId": "cz"
                }], [{
                    "title": "规格",
                    "width": 73.20108855474025,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 84.3886661332634,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "gg"
                }, {
                    "title": "长度",
                    "width": 62.43155485136131,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 100,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "DtMD_iLength"
                }], [{
                    "title": "厚度",
                    "width": 73.20108855474025,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 84.3886661332634,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "DtMD_iTorch"
                }, {
                    "title": "宽度",
                    "width": 62.43155485136131,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 49.3294447041711,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "DtMD_fWidth"
                }, {
                    "title": "炉批号",
                    "width": 28.573132201723723,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 78.57611355474026,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }], [{
                    "title": "单件孔数",
                    "width": 73.20108855474025,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 84.3886661332634,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "DtMD_iUnitHoleCount"
                }, {
                    "title": "总孔数",
                    "width": 62.43155485136131,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 49.3294447041711,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zks"
                }, {
                    "title": "重量",
                    "width": 28.573132201723723,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 78.57611355474026,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "zl"
                }], [{
                    "title": "填写",
                    "width": 54.95103855474021,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 110.18896613326342,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "Writer"
                }, {
                    "title": "审核",
                    "width": 32.427804851361245,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 66.33344470417114,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "Auditor"
                }, {
                    "title": "日期",
                    "width": 33.61988220172367,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 78.97886355474033,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": "idate"
                }]]
            }, "printElementType": {"title": "表格", "type": "tableCustom"}
        }, {
            "options": {"left": 22.5, "top": 249, "height": 112.5, "width": 376.5},
            "printElementType": {"type": "rect"}
        }, {
            "options": {
                "left": 30,
                "top": 262.5,
                "height": 9.75,
                "width": 30,
                "title": "备注",
                "fontSize": 12,
                "textAlign": "justify"
            }, "printElementType": {"type": "text"}
        }, {
            "options": {"left": 30, "top": 289.5, "height": 9.75, "width": 300,"fontSize": 15,"fontWeight": "bold", "field": "DtMD_sRemark"},
            "printElementType": {"type": "text"}
        }, {
            "options": {
                "left": 22.5,
                "top": 361.5,
                "height": 205.5,
                "width": 376.5,
                "tableHeaderBackground": "#ffffff",
                "field": "2",
                "columns": [[{
                    "title": "工序",
                    "width": 51.28127926148224,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "加工者",
                    "width": 47.96361980609419,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "质检员",
                    "width": 47.220923531126985,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "日期",
                    "width": 52.60736842105264,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "工序",
                    "width": 44.834386998258125,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "加工者",
                    "width": 46.44670026178271,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "质检员",
                    "width": 39.958442458720825,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "日期",
                    "width": 44.68727926148226,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }], [{
                    "title": "下料",
                    "width": 51.28127926148224,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "title": "",
                    "field": "",
                    "width": 47.96361980609419,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": ""
                }, {
                    "title": "",
                    "field": "",
                    "width": 47.220923531126985,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": ""
                }, {"width": 52.60736842105264, "colspan": 1, "rowspan": 1, "checked": true}, {
                    "title": "制弯",
                    "width": 44.834386998258125,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 46.44670026178271, "colspan": 1, "rowspan": 1, "checked": true}, {
                    "title": "",
                    "field": "",
                    "width": 39.958442458720825,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": ""
                }, {
                    "title": "",
                    "field": "",
                    "width": 44.68727926148226,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": ""
                }], [{
                    "title": "剪切",
                    "width": 51.28127926148224,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.96361980609419,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.220923531126985,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 52.60736842105264, "colspan": 1, "rowspan": 1, "checked": true}, {
                    "title": "开合角",
                    "width": 44.834386998258125,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 46.44670026178271,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 39.958442458720825,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 44.68727926148226, "colspan": 1, "rowspan": 1, "checked": true}], [{
                    "title": "矫正",
                    "width": 51.28127926148224,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.96361980609419,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.220923531126985,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 52.60736842105264, "colspan": 1, "rowspan": 1, "checked": true}, {
                    "title": "打扁",
                    "width": 44.834386998258125,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 46.44670026178271,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 39.958442458720825,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 44.68727926148226, "colspan": 1, "rowspan": 1, "checked": true}], [{
                    "title": "冲孔",
                    "width": 51.28127926148224,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.96361980609419,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.220923531126985,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 52.60736842105264, "colspan": 1, "rowspan": 1, "checked": true}, {
                    "title": "清根",
                    "width": 44.834386998258125,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 46.44670026178271,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 39.958442458720825,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 44.68727926148226, "colspan": 1, "rowspan": 1, "checked": true}], [{
                    "title": "钻孔",
                    "width": 51.28127926148224,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.96361980609419,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.220923531126985,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 52.60736842105264, "colspan": 1, "rowspan": 1, "checked": true}, {
                    "title": "铲背",
                    "field": "",
                    "width": 44.834386998258125,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true,
                    "columnId": ""
                }, {
                    "width": 46.44670026178271,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 39.958442458720825,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 44.68727926148226, "colspan": 1, "rowspan": 1, "checked": true}], [{
                    "title": "钢印",
                    "width": 51.28127926148224,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.96361980609419,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.220923531126985,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 52.60736842105264, "colspan": 1, "rowspan": 1, "checked": true}, {
                    "title": "焊接",
                    "width": 44.834386998258125,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 46.44670026178271,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 39.958442458720825,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 44.68727926148226, "colspan": 1, "rowspan": 1, "checked": true}], [{
                    "title": "切角",
                    "width": 51.48640437852818,
                    "align": "center",
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 48.155474285318576,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 47.40980722525149,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 52.81779789473686,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 50.72372354625118,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 40.92248806282982,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {
                    "width": 40.118276228555715,
                    "colspan": 1,
                    "rowspan": 1,
                    "checked": true
                }, {"width": 44.86602837852819, "colspan": 1, "rowspan": 1, "checked": true}]]
            }, "printElementType": {"title": "表格", "type": "tableCustom"}
        }, {
            "options": {"left": 81, "top": 109.5, "height": 9.75, "width": 99, "field": "PT_Num","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 231, "top": 109.5, "height": 9.75, "width": 151.5, "field": "dh","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 82.5, "top": 129, "height": 9.75, "width": 301.5, "field": "C_ProjectName","fontSize": 12,"fontWeight": "bold"},
            "printElementType": {"type": "text",
                formatter:function(title,value,options,templateData,target){
                    if(value.length>20){
                        return "<div style='font-size: 12px'>"+value+"</div>";
                    }else{
                        return value;
                    }
                }
            }
        }, {
            "options": {"left": 82.5, "top": 145.5, "height": 9.75, "width": 97.5, "field": "TD_TypeName","textAlign": "center","fontSize": 10,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 226.5, "top": 145.5, "height": 9.75, "width": 54, "field": "js","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 324, "top": 145.5, "height": 9.75, "width": 67.5, "field": "PTD_Count","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 82.5, "top": 165, "height": 9.75, "width": 97.5, "field": "DtMD_sPartsID","fontSize": 15,"fontWeight": "bold","textAlign": "center"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 226.5, "top": 165, "height": 9.75, "width": 52.5, "field": "cl","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {
                "left": 324,
                "top": 162,
                "height": 28.5,
                "width": 69,
                "field": "cz",
                "fontSize": 15,"fontWeight": "bold",
                "textContentVerticalAlign": "middle","textAlign": "center"
            }, "printElementType": {"type": "text"}
        }, {
            "options": {"left": 82.5, "top": 181.5, "height": 9.75, "width": 97.5, "field": "gg","fontSize": 15,"fontWeight": "bold","textAlign": "center"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 226.5, "top": 183, "height": 9.75, "width": 52.5, "field": "DtMD_iLength","fontSize": 15,"fontWeight": "bold","textAlign": "center"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 82.5, "top": 201, "height": 9.75, "width": 97.5, "field": "DtMD_iTorch","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 226.5, "top": 201, "height": 9.75, "width": 54, "field": "DtMD_fWidth","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 82.5, "top": 217.5, "height": 9.75, "width": 97.5, "field": "DtMD_iUnitHoleCount","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 226.5, "top": 217.5, "height": 9.75, "width": 54, "field": "zks","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 324, "top": 217.5, "height": 9.75, "width": 69, "field": "zl","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 82.5, "top": 235.5, "height": 9.75, "width": 97.5, "field": "Writer","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 226.5, "top": 235.5, "height": 9.75, "width": 54, "field": "Auditor","textAlign": "center","fontSize": 15,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 324, "top": 235.5, "height": 9.75, "width": 67.5, "field": "idate","textAlign": "center","fontSize": 12,"fontWeight": "bold"},
            "printElementType": {"type": "text"}
        }, {
            "options": {"left": 81, "top": 85, "height": 19.75, "width": 165, "field": "PT_DHmemo","fontSize": 10,
            "textContentVerticalAlign": "middle"
        },
            "printElementType": {"type": "text"}
        },
            {
                "options": {
                    "left": 80,
                    "top": 21,
                    "height": 20,
                    "width": 50
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['r_sz'];
                    }
                }
            },
            {
                "options": {
                    "left": 340,
                    "top": 30,
                    "height": 12,
                    "width": 50
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['r_tp'];
                    }
                }
            },
            {
                "options": {
                    "left": 20,
                    "top": 50,
                    "height": 20,
                    "width": 40
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['DtMD_iWelding'];
                    }
                }
            },
            {
                "options": {
                    "left": 62,
                    "top": 50,
                    "height": 20,
                    "width": 40
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['DtMD_iFireBending'];
                    }
                }
            },{
                "options": {
                    "left": 104,
                    "top": 50,
                    "height": 20,
                    "width": 40
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['DtMD_iCuttingAngle'];
                    }
                }
            },{
                "options": {
                    "left": 146,
                    "top": 50,
                    "height": 20,
                    "width": 40
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['DtMD_fBackOff'];
                    }
                }
            },{
                "options": {
                    "left": 188,
                    "top": 50,
                    "height": 20,
                    "width": 40
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['DtMD_iBackGouging'];
                    }
                }
            },{
                "options": {
                    "left": 230,
                    "top": 50,
                    "height": 20,
                    "width": 40
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['DtMD_DaBian'];
                    }
                }
            },{
                "options": {
                    "left": 272,
                    "top": 50,
                    "height": 20,
                    "width": 50
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['DtMD_KaiHeJiao'];
                    }
                }
            },{
                "options": {
                    "left": 314,
                    "top": 50,
                    "height": 20,
                    "width": 40
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['DtMD_GeHuo'];
                    }
                }
            },{
                "options": {
                    "left": 356,
                    "top": 50,
                    "height": 20,
                    "width": 40
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['DtMD_ZuanKong'];
                    }
                }
            },

            {
                "options": {
                    "left": 250,
                    "top": 320,
                    "height": 20,
                    "width": 50
                },
                "printElementType": {
                    "title": "html",
                    "type": "html",
                    formatter:function(title,value,options,templateData,target){
                        return options['r_ht'];
                    }
                }
            },
            {
                "options": {
                    "left": 151.5,
                    "top": 570,
                    "height": 9.75,
                    "width": 120,
                    "field": "pg",
                    "textAlign": "center"
                },
                "printElementType": {
                    "type": "text"
                }
            }
        ],
        "leftOffset":15,
        "paperNumberLeft": 389,
        "paperNumberTop": 573,
        "paperNumberDisabled":true,
        "rotate": true
    }]
}

// pdata['panels'][0]['printElements']=pdata['panels'][0]['printElements'].slice(1,4)
