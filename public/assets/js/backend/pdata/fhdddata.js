let fhdddata = {
	"panels": [{
		"index": 0,
		"height": 125,
		"width": 210,
		"paperHeader": 103.5,
		"paperFooter": 328.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 31.5,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司发货订单",
				"fontSize": 21.75,
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 78,
				"top": 54,
				"height": 9.75,
				"width": 321,
				"field": "khjc",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 54,
				"height": 9.75,
				"width": 64,
				"title": "客户简称:",
				"fontSize": 13.5,
				"lineHeight": 13.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 78,
				"top": 70.5,
				"height": 9.75,
				"width": 493.5,
				"field": "xmmc",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 70.5,
				"height": 9.75,
				"width": 64,
				"title": "项目名称:",
				"fontSize": 13.5,
				"lineHeight": 13.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 78,
				"top": 85.5,
				"height": 9.75,
				"width": 319.5,
				"field": "rwd",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 405,
				"top": 54,
				"height": 9.75,
				"width": 70.5,
				"title": "发货单号:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 474,
				"top": 54,
				"height": 9.75,
				"width": 102,
				"field": "fhdh",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 405,
				"top": 85.5,
				"height": 9.75,
				"width": 67.5,
				"title": "发货日期:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 85.5,
				"height": 9.75,
				"width": 64,
				"title": "任务单:",
				"fontSize": 13.5,
				"lineHeight": 13.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 474,
				"top": 85.5,
				"height": 9.75,
				"width": 100.5,
				"field": "fhrq",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 103.5,
				"height": 48,
				"width": 541.5,
				"field": "tb",
				"textAlign": "center",
				"fontSize": 12.5,
				"lineHeight": 12.5,
				"tableHeaderFontSize": 13.5,
				"columns": [
					[{
						"title": "序号",
						"field": "xh",
						"width": 50.34105141982553,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "xh"
					}, {
						"title": "塔型",
						"field": "tx",
						"width": 186.5785655664801,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "tx"
					}, {
						"title": "呼高",
						"field": "hg",
						"width": 50.34204624033997,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "hg"
					}, {
						"title": "杆塔",
						"field": "gt",
						"width": 74.21284335177934,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gt"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 50.341576943884135,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 50.339910284619286,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "重量",
						"field": "zl",
						"width": 79.34396629285072,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 255,
				"top": 328.5,
				"height": 9.75,
				"width": 120,
				"field": "cyr",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 210,
				"top": 328.5,
				"height": 9.75,
				"width": 55.5,
				"title": "承运人:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 61.5,
				"top": 328.5,
				"height": 9.75,
				"width": 120,
				"field": "zdr",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 417,
				"top": 328.5,
				"height": 9.75,
				"width": 120,
				"title": "购方（签字）:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 328.5,
				"height": 9.75,
				"width": 55.5,
				"title": "制单人:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 565.5,
		"paperNumberTop": 331.5,
		"paperNumberDisabled": true
	}]
}

let fhdddata2 = {
	"panels": [{
		"index": 0,
		"height": 125,
		"width": 210,
		"paperHeader": 103.5,
		"paperFooter": 328.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 31.5,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司发货订单",
				"fontSize": 21.75,
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 78,
				"top": 54,
				"height": 9.75,
				"width": 321,
				"field": "khjc",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 54,
				"height": 9.75,
				"width": 64,
				"title": "客户简称:",
				"fontSize": 13.5,
				"lineHeight": 13.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 78,
				"top": 70.5,
				"height": 9.75,
				"width": 493.5,
				"field": "xmmc",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 70.5,
				"height": 9.75,
				"width": 64,
				"title": "项目名称:",
				"fontSize": 13.5,
				"lineHeight": 13.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 78,
				"top": 85.5,
				"height": 9.75,
				"width": 319.5,
				"field": "rwd",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 405,
				"top": 54,
				"height": 9.75,
				"width": 70.5,
				"title": "发货单号:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 474,
				"top": 54,
				"height": 9.75,
				"width": 102,
				"field": "fhdh",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 405,
				"top": 85.5,
				"height": 9.75,
				"width": 67.5,
				"title": "发货日期:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 85.5,
				"height": 9.75,
				"width": 64,
				"title": "任务单:",
				"fontSize": 13.5,
				"lineHeight": 13.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 474,
				"top": 85.5,
				"height": 9.75,
				"width": 100.5,
				"field": "fhrq",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 103.5,
				"height": 48,
				"width": 541.5,
				"field": "tb",
				"textAlign": "center",
				"fontSize": 12.5,
				"lineHeight": 12.5,
				"tableHeaderFontSize": 13.5,
				"columns": [
					[{
						"title": "序号",
						"field": "xh",
						"width": 50.34105141982553,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "xh"
					}, {
						"title": "塔型",
						"field": "tx",
						"width": 112.4137655664801,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "tx"
					}, {
						"title": "呼高",
						"field": "hg",
						"width": 50.34204624033997,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "hg"
					}, {
						"title": "杆塔",
						"field": "gt",
						"width": 74.21284335177934,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gt"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 50.341576943884135,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 50.339910284619286,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "重量",
						"field": "zl",
						"width": 79.34396629285072,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl"
					}, {
						"title": "备注",
						"field": "bz",
						"width": 74.16483990022094,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 255,
				"top": 328.5,
				"height": 9.75,
				"width": 120,
				"field": "cyr",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 210,
				"top": 328.5,
				"height": 9.75,
				"width": 55.5,
				"title": "承运人:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 61.5,
				"top": 328.5,
				"height": 9.75,
				"width": 120,
				"field": "zdr",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 417,
				"top": 328.5,
				"height": 9.75,
				"width": 120,
				"title": "购方（签字）:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 13.5,
				"top": 328.5,
				"height": 9.75,
				"width": 55.5,
				"title": "制单人:",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 565.5,
		"paperNumberTop": 331.5,
		"paperNumberDisabled": true
	}]
}