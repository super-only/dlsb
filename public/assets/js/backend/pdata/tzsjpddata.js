let hzbdata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 97,
		"paperFooter": 801,
		"printElements": [{
			"options": {
				"left": 211.5,
				"top": 26,
				"height": 9.75,
				"width": 156,
				"title": "绍兴电力设备有限公司",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 241.5,
				"top": 45.5,
				"height": 9.75,
				"width": 93,
				"title": "图纸汇总清单",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 415.5,
				"top": 35.5,
				"height": 9.75,
				"width": 120
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 61.5,
				"top": 65,
				"height": 9.75,
				"width": 349.5,
				"field": "gc"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 31.5,
				"top": 65,
				"height": 9.75,
				"width": 28.5,
				"title": "工程："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 31.5,
				"top": 81.5,
				"height": 9.75,
				"width": 28.5,
				"title": "塔型："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 61.5,
				"top": 81.5,
				"height": 9.75,
				"width": 120,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 22.5,
				"top": 97,
				"height": 45,
				"width": 550,
				"textAlign": "center",
				"field": "tb",
				"columns": [
					[{
						"title": "材料",
						"field": "cl",
						"width": 48.278963013181645,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cl"
					}, {
						"title": "材质",
						"field": "cz",
						"width": 49.38713313609466,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 68.25661265361856,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg"
					}, {
						"title": "重量",
						"field": "zl",
						"width": 54.49021070690802,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zl"
					}, {
						"title": "螺栓件重量",
						"field": "lsjzl",
						"width": 59.92521675199917,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "lsjzl"
					}, {
						"title": "电焊件重量",
						"field": "dhjzl",
						"width": 61.747279559383884,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dhjzl"
					}, {
						"title": "总孔数",
						"field": "zks",
						"width": 64.00348655024779,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zks"
					}, {
						"title": "制弯",
						"field": "hq",
						"width": 59.29571301318167,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "hq"
					}, {
						"title": "总计数量",
						"field": "zjsl",
						"width": 84.6153846153846,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zjsl"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom",
			}
		}, {
			"options": {
				"left": 33,
				"top": 814.5,
				"height": 9.75,
				"width": 120,
				"field": "rq"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 496.5,
		"paperNumberTop": 813,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
};

let bjmxdata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 93,
		"paperFooter": 790,
		"printElements": [{
			"options": {
				"left": 208.5,
				"top": 30,
				"height": 9.75,
				"width": 151.5,
				"title": "绍兴电力设备有限公司",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 223.5,
				"top": 45,
				"height": 9.75,
				"width": 130.5,
				"title": "图纸备料部件明细",
				"fontSize": 15
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 207,
				"top": 60,
				"height": 9,
				"width": 153
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 207,
				"top": 62,
				"height": 9,
				"width": 153
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 255,
				"top": 65,
				"height": 9.75,
				"width": 120,
				"field": "rq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 78,
				"top": 78,
				"height": 9.75,
				"width": 441,
				"field": "gcmc"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 33,
				"top": 78,
				"height": 9.75,
				"width": 40.5,
				"title": "工程名称:"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 451,
				"top": 78,
				"height": 9.75,
				"width": 441,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 421,
				"top": 78,
				"height": 9.75,
				"width": 22.5,
				"title": "塔型:"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 22.5,
				"top": 93,
				"height": 58.5,
				"width": 550,
				"textAlign": "center",
				"field": "tb",
				"tableBodyCellBorder": "noBorder",
				"columns": [
					[{
						"title": "序号",
						"field": "xh",
						"width": 27.212065296491105,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "xh",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "部件编号",
						"field": "bjbh",
						"width": 58.96041746167753,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bjbh",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "材质",
						"field": "cz",
						"width": 58.96318839251662,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "规格",
						"field": "gg",
						"width": 58.961763106654836,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "长度",
						"field": "cd",
						"width": 34.366248650835524,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cd",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";	
							}
						}
					}, {
						"title": "宽度",
						"field": "kd",
						"width": 34.3671864094088,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "kd",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "数量",
						"field": "sl",
						"width": 34.36440090779486,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "配料方式",
						"field": "plfs",
						"width": 115.81591292238382,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "plfs",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "备注",
						"field": "bz",
						"width": 68.29533933408223,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}, {
						"title": "总重量",
						"field": "zzl",
						"width": 58.96347751815464,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zzl",
						"formatter": function (value,row) {
							if (row['xh'] =='') {
								return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold'>" + value + "</div>";
							} 
							else {
								return "<div>" + value + "</div>";
							}
						}
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 55.5,
				"top": 800,
				"height": 9.75,
				"width": 120,
				"field": "zb"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 270,
				"top": 800,
				"height": 9.75,
				"width": 60,
				"title": "审核："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 300,
				"top": 800,
				"height": 9.75,
				"width": 120,
				"field": "sh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 24,
				"top": 800,
				"height": 9.75,
				"width": 28.5,
				"title": "制表："
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 465.5,
		"paperNumberTop": 800,
		"paperNumberDisabled": false,
		"paperNumberFormat": "第paperNo页，共paperCount页",
	}]
};