let formatter = function(value, row) {
	if(row['gg']==''){
		return value;
	}else{
		return `<div class='cell' style='text-align: center;border:1px solid;;'>${value}</div>`;
	}
}

let fm = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 115.5,
		"paperFooter": 745.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 70.5,
				"height": 9.75,
				"width": 594,
				"title": "铁塔包装清单",
				"fontSize": 21.75,
				"fontWeight": "bolder",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 138,
				"height": 9.75,
				"width": 96,
				"title": "合同编号：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 138,
				"height": 9.75,
				"width": 220,
				"field": "htbh",
				"testData": "ht",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 181.5,
				"height": 9.75,
				"width": 309,
				"field": "khmc",
				"testData": "kh",
				"fontSize": 18,
				"lineHeight": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 184.5,
				"height": 9.75,
				"width": 96,
				"title": "客户名称：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 228,
				"height": 9.75,
				"width": 309,
				"field": "gcmc",
				"testData": "gc",
				"fontSize": 18,
				"lineHeight": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 231,
				"height": 9.75,
				"width": 96,
				"title": "工程名称：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 277.5,
				"height": 9.75,
				"width": 220,
				"field": "tx",
				"testData": "tx",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 277.5,
				"height": 9.75,
				"width": 96,
				"title": "塔    型：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 324,
				"height": 9.75,
				"width": 220,
				"field": "hg",
				"testData": "hg",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 324,
				"height": 9.75,
				"width": 96,
				"title": "呼    高：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 370.5,
				"height": 9.75,
				"width": 220,
				"field": "zh",
				"testData": "zh",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 370.5,
				"height": 9.75,
				"width": 96,
				"title": "桩    号：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 413.5,
				"height": 9.75,
				"width": 320,
				"field": "dlzh",
				"testData": "dl",
				"fontSize": 18,
				"lineHeight": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 418.5,
				"height": 9.75,
				"width": 96,
				"title": "段落组合：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 465,
				"height": 9.75,
				"width": 220,
				"field": "js",
				"testData": "js",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 465,
				"height": 9.75,
				"width": 96,
				"title": "数    量：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 511.5,
				"height": 9.75,
				"width": 96,
				"title": "单   重：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 511.5,
				"height": 9.75,
				"width": 220,
				"field": "djzl",
				"testData": "zl",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 558,
				"height": 9.75,
				"width": 220,
				"field": "bs",
				"testData": "bs",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 102,
				"top": 558,
				"height": 9.75,
				"width": 97.5,
				"title": "包    数：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 606,
				"height": 9.75,
				"width": 136.5,
				"field": "rq",
				"testData": "rq",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 343.5,
				"top": 606,
				"height": 9.75,
				"width": 64.5,
				"title": "制表：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 102,
				"top": 606,
				"height": 9.75,
				"width": 96,
				"title": "日  期：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 411,
				"top": 606,
				"height": 9.75,
				"width": 156,
				"field": "zb",
				"testData": "zb",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 691.5,
				"height": 9.75,
				"width": 594,
				"fontSize": 20.25,
				"fontWeight": "bold",
				"textAlign": "center",
				"field": "factory_name",
				"testData": "绍兴电力设备有限公司"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 565.5,
		"paperNumberTop": 819,
		"paperNumberDisabled": true
	}]
}

let fmWithoutWeight = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 115.5,
		"paperFooter": 745.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 70.5,
				"height": 9.75,
				"width": 594,
				"title": "铁塔包装清单",
				"fontSize": 21.75,
				"fontWeight": "bolder",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 138,
				"height": 9.75,
				"width": 96,
				"title": "合同编号：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 138,
				"height": 9.75,
				"width": 220,
				"field": "htbh",
				"testData": "ht",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 181.5,
				"height": 9.75,
				"width": 309,
				"field": "khmc",
				"testData": "kh",
				"fontSize": 18,
				"lineHeight": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 184.5,
				"height": 9.75,
				"width": 96,
				"title": "客户名称：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 228,
				"height": 9.75,
				"width": 309,
				"field": "gcmc",
				"testData": "gc",
				"fontSize": 18,
				"lineHeight": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 231,
				"height": 9.75,
				"width": 96,
				"title": "工程名称：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 277.5,
				"height": 9.75,
				"width": 220,
				"field": "tx",
				"testData": "tx",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 277.5,
				"height": 9.75,
				"width": 96,
				"title": "塔    型：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 324,
				"height": 9.75,
				"width": 220,
				"field": "hg",
				"testData": "hg",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 324,
				"height": 9.75,
				"width": 96,
				"title": "呼    高：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 370.5,
				"height": 9.75,
				"width": 220,
				"field": "zh",
				"testData": "zh",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 370.5,
				"height": 9.75,
				"width": 96,
				"title": "桩    号：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 204,
				"top": 413.5,
				"height": 9.75,
				"width": 320,
				"field": "dlzh",
				"testData": "dl",
				"fontSize": 18,
				"lineHeight": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 418.5,
				"height": 9.75,
				"width": 96,
				"title": "段落组合：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 465,
				"height": 9.75,
				"width": 220,
				"field": "js",
				"testData": "js",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 103.5,
				"top": 465,
				"height": 9.75,
				"width": 96,
				"title": "基    数：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 513,
				"height": 9.75,
				"width": 220,
				"field": "bs",
				"testData": "bs",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 102,
				"top": 513,
				"height": 9.75,
				"width": 97.5,
				"title": "包    数：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 205.5,
				"top": 561,
				"height": 9.75,
				"width": 136.5,
				"field": "rq",
				"testData": "rq",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 343.5,
				"top": 561,
				"height": 9.75,
				"width": 64.5,
				"title": "制表：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 102,
				"top": 561,
				"height": 9.75,
				"width": 96,
				"title": "日  期：",
				"fontSize": 18,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 411,
				"top": 561,
				"height": 9.75,
				"width": 156,
				"field": "zb",
				"testData": "zb",
				"fontSize": 18
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 621,
				"height": 9.75,
				"width": 594,
				"fontSize": 20.25,
				"fontWeight": "bold",
				"textAlign": "center",
				"field": "factory_name",
				"testData": "绍兴电力设备有限公司"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 565.5,
		"paperNumberTop": 819,
		"paperNumberDisabled": true
	}]
}

let mx = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 58.5,
		"paperFooter": 801,
		"printElements": [{
			"options": {
				"left": 412.5,
				"top": 25.5,
				"height": 9.75,
				"width": 34.5,
				"title": "日期："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 25.5,
				"height": 9.75,
				"width": 120,
				"field": "rq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 216,
				"top": 36,
				"height": 22.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 36,
				"height": 9,
				"width": 549
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 574.5,
				"top": 36,
				"height": 22.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 36,
				"height": 22.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 42,
				"height": 9.75,
				"width": 120,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 229.5,
				"top": 42,
				"height": 9.75,
				"width": 33,
				"title": "塔形："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 393,
				"top": 42,
				"height": 9.75,
				"width": 43.5,
				"title": "杆塔号："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 34.5,
				"top": 42,
				"height": 9.75,
				"width": 175.5,
				"title": "材质前未标明钢号为Q235B"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 439.5,
				"top": 42,
				"height": 9.75,
				"width": 120,
				"field": "gth"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 58.5,
				"height": 45,
				"width": 550,
				"field": "tb",
				"textAlign": "center",
				"tableBodyCellBorder": "noBorder",
				"columns": [
					[{
						"formatter": formatter,
						"title": "部件编号",
						"field": "bjbh",
						"width": 63.46718022454144,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bjbh"
					},{
						"formatter": formatter,
						"title": "钢印号",
						"field": "gyh",
						"width": 53.67930258575132,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gyh"
					},{
						"formatter": function(value, row) {
							if(row['gg']==''){
								return value;
							}else{
								if(value == 'Q235B') value='';
								return `<div class='cell' style='text-align: center;border:1px solid;'>${value}</div>`;
							}
						},
						"title": "材质",
						"field": "cz",
						"width": 49.66589064948191,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz"
					},{
						"formatter": formatter,
						"title": "规格",
						"field": "gg",
						"width": 71.27209978033083,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg",
						
					},{
						"formatter": formatter,
						"title": "长度",
						"field": "cd",
						"width": 62.296565198741575,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cd"
					},{
						"formatter": formatter,
						"title": "包装数",
						"field": "bzs",
						"width": 63.48242055278111,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bzs"
					},{
						"formatter": formatter,
						"title": "单重",
						"field": "dz",
						"width": 48.99614431389196,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dz"
					},{
						"formatter": formatter,
						"title": "总重",
						"field": "zz",
						"width": 61.29821646993838,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zz"
					},{
						"formatter": formatter,
						"title": "备注",
						"field": "bz",
						"width": 75.84218022454144,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}],
		"paperNumberLeft": 258,
		"paperNumberTop": 807,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}

let mxWithoutWeight = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 58.5,
		"paperFooter": 801,
		"printElements": [{
			"options": {
				"left": 412.5,
				"top": 25.5,
				"height": 9.75,
				"width": 34.5,
				"title": "日期："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 450,
				"top": 25.5,
				"height": 9.75,
				"width": 120,
				"field": "rq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 214.5,
				"top": 36,
				"height": 22.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 36,
				"height": 9,
				"width": 549
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 574.5,
				"top": 36,
				"height": 22.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 36,
				"height": 22.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 265.5,
				"top": 42,
				"height": 9.75,
				"width": 120,
				"field": "tx"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 229.5,
				"top": 42,
				"height": 9.75,
				"width": 33,
				"title": "塔形："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 393,
				"top": 42,
				"height": 9.75,
				"width": 43.5,
				"title": "杆塔号："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 34.5,
				"top": 42,
				"height": 9.75,
				"width": 175.5,
				"title": "材质前未标明钢号为Q235B"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 439.5,
				"top": 42,
				"height": 9.75,
				"width": 120,
				"field": "gth"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 25.5,
				"top": 58.5,
				"height": 45,
				"width": 550,
				"field": "tb",
				"textAlign": "center",
				"tableBodyCellBorder": "noBorder",
				"columns": [
					[{
						"formatter": formatter,
						"title": "部件编号",
						"field": "bjbh",
						"width": 67.38656038222025,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bjbh"
					},{
						"formatter": formatter,
						"title": "钢印号",
						"field": "gyh",
						"width": 68.98057271972624,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gyh"
					},{
						"formatter": function(value, row) {
							if(row['gg']==''){
								return value;
							}else{
								if(value == 'Q235B') value='';
								return `<div class='cell' style='text-align: center;border:1px solid;'>${value}</div>`;
							}
						},
						"title": "材质",
						"field": "cz",
						"width": 53.147924327897336,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cz"
					},{
						"formatter": formatter,
						"title": "规格",
						"field": "gg",
						"width": 108.28976607773387,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg"
					},{
						"formatter": formatter,
						"title": "长度",
						"field": "cd",
						"width": 77.92283701520444,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "cd"
					},{
						"formatter": formatter,
						"title": "包装数",
						"field": "bzs",
						"width": 79.40614854581024,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bzs"
					},{
						"formatter": formatter,
						"title": "备注",
						"field": "bz",
						"width": 94.86619093140764,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bz"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}],
		"paperNumberLeft": 258,
		"paperNumberTop": 807,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}

const cellFormatter = function (value,row) {
	if (value=='') {
		return "<div style='border: 0px solid'>" + value + "</div>";
	} 
	else {
		return "<div style='border: 1px solid;word-break: keep-all;'>" + value + "</div>";
	}
}

let zlqd = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 141,
		"paperFooter": 744,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 51,
				"height": 9.75,
				"width": 594,
				"title": "重量清单",
				"fontSize": 17.25,
				"fontWeight": "900",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 85.5,
				"height": 9.75,
				"width": 72,
				"title": "工程名称",
				"fontSize": 10.5,
				"textAlign": "right"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 94.5,
				"top": 85.5,
				"height": 9.75,
				"width": 478.5,
				"field": "gcmc",
				"fontSize": 10.5,
				"testData": "gcmc",
				"fontWeight": "700"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 94.5,
				"top": 102,
				"height": 9.75,
				"width": 172.5,
				"field": "tx",
				"fontSize": 10.5,
				"testData": "tx",
				"fontWeight": "700"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 274.5,
				"top": 102,
				"height": 9.75,
				"width": 55.5,
				"title": "数量",
				"fontSize": 10.5,
				"textAlign": "right"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 331.5,
				"top": 102,
				"height": 9.75,
				"width": 120,
				"field": "js",
				"fontSize": 10.5,
				"testData": "js",
				"fontWeight": "700"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 102,
				"height": 9.75,
				"width": 72,
				"title": "塔型",
				"fontSize": 10.5,
				"textAlign": "right"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 274.5,
				"top": 118.5,
				"height": 9.75,
				"width": 55.5,
				"title": "总重量",
				"fontSize": 10.5,
				"textAlign": "right"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 94.5,
				"top": 118.5,
				"height": 9.75,
				"width": 172.5,
				"field": "zks",
				"fontSize": 10.5,
				"testData": "zks",
				"fontWeight": "700"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 331.5,
				"top": 118.5,
				"height": 9.75,
				"width": 120,
				"field": "zzl",
				"testData": "zzl",
				"fontSize": 10.5,
				"fontWeight": "700"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 118.5,
				"height": 9.75,
				"width": 72,
				"title": "总捆数",
				"fontSize": 10.5,
				"textAlign": "right"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 21,
				"top": 141,
				"height": 42,
				"width": 547.5,
				"field": "tb",
				"textAlign": "center",
				"fontSize": 10.5,
				"columns": [
					[{
						"title": "呼高",
						"field": "hg",
						"width": 77.99043052612977,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "hg"
					}, {
						"title": "杆塔号",
						"field": "gth",
						"width": 77.99043052612977,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gth"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 77.99043052612977,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "最大长度",
						"field": "zdcd",
						"width": 79.55741684322142,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zdcd"
					}, {
						"title": "包名",
						"field": "bm",
						"width": 77.99043052612977,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bm"
					}, {
						"title": "件号数",
						"field": "jhs",
						"width": 77.99043052612977,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "jhs"
					}, {
						"title": "包重量",
						"field": "bzl",
						"width": 77.99043052612977,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bzl"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 90,
				"top": 765,
				"height": 9.75,
				"width": 120,
				"field": "zb"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 375,
				"top": 765,
				"height": 9.75,
				"width": 40.5,
				"title": "审核:"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 402,
				"top": 765,
				"height": 9.75,
				"width": 120,
				"field": "sh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 63,
				"top": 765,
				"height": 9.75,
				"width": 40.5,
				"title": "制表:"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 258,
		"paperNumberTop": 780,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}