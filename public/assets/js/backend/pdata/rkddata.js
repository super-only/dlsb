let rkddata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 190.5,
		"paperFooter": 790.5,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 31.5,
				"height": 9.75,
				"width": 594,
				"title": "绍兴电力设备有限公司入库单",
				"fontSize": 21.75,
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 91.5,
				"top": 69,
				"height": 9.75,
				"width": 133.5,
				"field": "rkdh",
				"testData": "rkdh",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 303,
				"top": 69,
				"height": 9.75,
				"width": 94.5,
				"field": "rkrq",
				"testData": "rkrq",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 412.5,
				"top": 69,
				"height": 9.75,
				"width": 69,
				"title": "仓库：",
				"fontSize": 13.5,
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 229.5,
				"top": 69,
				"height": 9.75,
				"width": 72.5,
				"title": "入库日期：",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 483,
				"top": 69,
				"height": 9.75,
				"width": 87,
				"field": "szck",
				"testData": "szck",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 22.5,
				"top": 69,
				"height": 9.75,
				"width": 69,
				"title": "入库单号：",
				"textAlign": "justify",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 229.5,
				"top": 99,
				"height": 9.75,
				"width": 72,
				"title": "入库类别：",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 91.5,
				"top": 99,
				"height": 9.75,
				"width": 135,
				"field": "bz",
				"testData": "bz",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 412.5,
				"top": 99,
				"height": 9.75,
				"width": 69,
				"title": "生产厂家：",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 303,
				"top": 99,
				"height": 9.75,
				"width": 94.5,
				"field": "rklb",
				"testData": "rklb",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 483,
				"top": 99,
				"height": 9.75,
				"width": 87,
				"field": "sccj",
				"testData": "sccj",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 22.5,
				"top": 99,
				"height": 9.75,
				"width": 69,
				"title": "备注：",
				"textAlign": "justify",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 91.5,
				"top": 127.5,
				"height": 9.75,
				"width": 478.5,
				"field": "gys",
				"testData": "gys",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 22.5,
				"top": 129,
				"height": 9.75,
				"width": 69,
				"title": "供货单位：",
				"textAlign": "justify",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 22.5,
				"top": 190.5,
				"height": 43.5,
				"width": 549,
				"field": "tb",
				"textAlign": "center",
				"fontSize": 13.5,
				"tableHeaderFontSize": 13.5,
				"columns": [
					[{
						"title": "存货",
						"field": "ch",
						"width": 82.10011212093517,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "chbm"
					}, {
						"title": "材质",
						"field": "cz",
						"width": 82.10616368658206,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "chmc"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 82.10446356183608,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ggxh"
					}, {
						"title": "长度(m)",
						"field": "cd",
						"width": 72.0135033326575,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 72.01523624032129,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "重量(Kg)",
						"field": "zl",
						"width": 72.01362965538898,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bhsdj"
					}, {
						"title": "炉号",
						"field": "lh",
						"width": 109.14689140227892,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bhsje"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 22.5,
				"top": 801,
				"height": 9.75,
				"width": 43.5,
				"title": "制单：",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 244.5,
				"top": 801,
				"height": 9.75,
				"width": 120,
				"title": "经办人：",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 66,
				"top": 801,
				"height": 9.75,
				"width": 88.5,
				"field": "zd",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 22.5,
				"top": 159,
				"height": 9.75,
				"width": 69,
				"title": "工程名称：",
				"fontSize": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 93,
				"top": 157.5,
				"height": 9.75,
				"width": 477,
				"field": "gcmc",
				"testData": "gcmc",
				"fontSize": 13.5,
				"lineHeight": 13.5
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 472.5,
		"paperNumberTop": 801,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}