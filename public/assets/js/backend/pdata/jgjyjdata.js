let jgjyjdata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 210,
		"width": 297,
		"paperHeader": 105,
		"paperFooter": 537,
		"printElements": [{
			"options": {
				"left": 0,
				"top": 13.5,
				"height": 9.75,
				"width": 840,
				"title": "绍兴电力设备有限公司",
				"fontSize": 21.75,
				"fontWeight": "900",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 0,
				"top": 37.5,
				"height": 9.75,
				"width": 840,
				"title": "螺栓收付存结报表",
				"fontSize": 16.5,
				"fontWeight": "900",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 75,
				"top": 69,
				"height": 9.75,
				"width": 120,
				"field": "jcrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 69,
				"height": 9.75,
				"width": 55.5,
				"title": "结存日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 585,
				"top": 69,
				"height": 9.75,
				"width": 39,
				"title": "仓库：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 625.5,
				"top": 69,
				"height": 9.75,
				"width": 120,
				"field": "ck"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 249,
				"top": 85.5,
				"height": 19.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 390,
				"top": 85.5,
				"height": 19.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 534,
				"top": 85.5,
				"height": 19.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 676.5,
				"top": 85.5,
				"height": 19.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 18,
				"top": 85.5,
				"height": 9,
				"width": 802.5
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 820.5,
				"top": 85.5,
				"height": 19.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 18,
				"top": 85.5,
				"height": 19.5,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 390,
				"top": 91.5,
				"height": 9.75,
				"width": 141,
				"title": "本月收进",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 249,
				"top": 91.5,
				"height": 9.75,
				"width": 141,
				"title": "上月结存",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 534,
				"top": 91.5,
				"height": 9.75,
				"width": 142.5,
				"title": "本期付出",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 676.5,
				"top": 91.5,
				"height": 9.75,
				"width": 144,
				"title": "月末结存",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 91.5,
				"height": 9.75,
				"width": 231,
				"title": "存货",
				"textAlign": "center"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 105,
				"height": 46.5,
				"width": 803,
				"field": "tb",
				"textAlign": "center",
				"columns": [
					[{
						"title": "名称",
						"field": "mc",
						"width": 86.0561317572715,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "mc"
					}, {
						"title": "规格",
						"field": "gg",
						"width": 86.05460094677095,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "gg"
					}, {
						"title": "等级",
						"field": "dj",
						"width": 31.768942614467566,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dj"
					}, {
						"title": "单位",
						"field": "dw",
						"width": 27.277177860660743,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "dw"
					}, {
						"title": "数量",
						"field": "sysl",
						"width": 69.74485846405808,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sysl"
					}, {
						"title": "金额(元)",
						"field": "syje",
						"width": 71.71512572657919,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "syje"
					}, {
						"title": "数量",
						"field": "bysl",
						"width": 71.83925947600764,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bysl"
					}, {
						"title": "金额(元)",
						"field": "byje",
						"width": 71.70619142411779,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "byje"
					}, {
						"title": "数量",
						"field": "bqsl",
						"width": 71.70928307356289,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bqsl"
					}, {
						"title": "金额(元)",
						"field": "bqje",
						"width": 71.7120921272667,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bqje"
					}, {
						"title": "数量",
						"field": "ymsl",
						"width": 71.71234554987804,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ymsl"
					}, {
						"title": "金额(元)",
						"field": "ymje",
						"width": 71.70399097935886,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "ymje"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 231,
				"top": 541.5,
				"height": 9.75,
				"width": 120,
				"field": "shr",
				"testData": "shr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 190.5,
				"top": 541.5,
				"height": 9.75,
				"width": 39,
				"title": "审核人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 58.5,
				"top": 541.5,
				"height": 9.75,
				"width": 120,
				"field": "zbr",
				"testData": "zbr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 541.5,
				"height": 9.75,
				"width": 39,
				"title": "制表人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 190.5,
				"top": 556.5,
				"height": 9.75,
				"width": 39,
				"title": "日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 231,
				"top": 556.5,
				"height": 9.75,
				"width": 120,
				"field": "shrq",
				"testData": "shrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 463.5,
				"top": 556.5,
				"height": 9.75,
				"width": 51,
				"title": "打印日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 58.5,
				"top": 556.5,
				"height": 9.75,
				"width": 120,
				"field": "zbrq",
				"testData": "zbrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 516,
				"top": 556.5,
				"height": 9.75,
				"width": 120,
				"field": "dyrq",
				"testData": "dyrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 18,
				"top": 556.5,
				"height": 9.75,
				"width": 39,
				"title": "日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 661.5,
		"paperNumberTop": 555,
		"paperNumberFormat": "第paperNo页，共paperCount页",
		"rotate": true
	}]
}