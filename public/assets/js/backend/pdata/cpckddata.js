let cpckddata = {
	"panels": [{
		"index": 0,
		"paperType": "A4",
		"height": 297,
		"width": 210,
		"paperHeader": 213,
		"paperFooter": 760.5,
		"printElements": [{
			"options": {
				"left": 442.5,
				"top": 27,
				"height": 90,
				"width": 114
			},
			"printElementType": {
				"type": "rect"
			}
		}, {
			"options": {
				"left": 114,
				"top": 27,
				"height": 9.75,
				"width": 250,
				"title": "绍兴电力设备有限公司",
				"fontSize": 16.5,
				"fontWeight": "500"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 483,
				"top": 27,
				"height": 90,
				"width": 9
			},
			"printElementType": {
				"type": "vline"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 27,
				"height": 67.5,
				"width": 78,
				"src": "/assets/img/qr_img.png"
			},
			"printElementType": {
				"type": "image"
			}
		}, {
			"options": {
				"left": 451.5,
				"top": 34.5,
				"height": 9.75,
				"width": 24,
				"title": "单号"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 486,
				"top": 34.5,
				"height": 9.75,
				"width": 63,
				"field": "top_dh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 442.5,
				"top": 49.5,
				"height": 9,
				"width": 114
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 451.5,
				"top": 55.5,
				"height": 9.75,
				"width": 22.5,
				"title": "车号"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 486,
				"top": 55.5,
				"height": 9.75,
				"width": 64.5,
				"field": "top_ch"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 255,
				"top": 66,
				"height": 9.75,
				"width": 120,
				"title": "产品出库",
				"fontSize": 21.75,
				"fontWeight": "800"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 442.5,
				"top": 70.5,
				"height": 9,
				"width": 114
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 451.5,
				"top": 78,
				"height": 9.75,
				"width": 22.5,
				"title": "司机"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 486,
				"top": 78,
				"height": 9.75,
				"width": 64.5,
				"field": "top_sj"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 442.5,
				"top": 94.5,
				"height": 9,
				"width": 114
			},
			"printElementType": {
				"type": "hline"
			}
		}, {
			"options": {
				"left": 486,
				"top": 100.5,
				"height": 9.75,
				"width": 66,
				"field": "top_lxdh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 444,
				"top": 102,
				"height": 9.75,
				"width": 39,
				"title": "联系电话"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 79.5,
				"top": 126,
				"height": 9.75,
				"width": 250.5,
				"field": "shdd"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 126,
				"height": 9.75,
				"width": 49.5,
				"title": "收货地点：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 79.5,
				"top": 142.5,
				"height": 9.75,
				"width": 250.5,
				"field": "shdw"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 337.5,
				"top": 126,
				"height": 9.75,
				"width": 49.5,
				"title": "出库日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 388.5,
				"top": 126,
				"height": 9.75,
				"width": 120,
				"field": "ckrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 142.5,
				"height": 9.75,
				"width": 49.5,
				"title": "收货单位：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 337.5,
				"top": 142.5,
				"height": 9.75,
				"width": 49.5,
				"title": "联系人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 79.5,
				"top": 159,
				"height": 9.75,
				"width": 250.5,
				"field": "gcmc"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 388.5,
				"top": 142.5,
				"height": 9.75,
				"width": 120,
				"field": "lxr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 159,
				"height": 9.75,
				"width": 49.5,
				"title": "工程名称：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 79.5,
				"top": 175.5,
				"height": 9.75,
				"width": 250.5,
				"field": "hth"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 337.5,
				"top": 159,
				"height": 9.75,
				"width": 49.5,
				"title": "联系电话：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 388.5,
				"top": 159,
				"height": 9.75,
				"width": 120,
				"field": "lxdh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 175.5,
				"height": 9.75,
				"width": 49.5,
				"title": "合同号：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 79.5,
				"top": 192,
				"height": 9.75,
				"width": 430.5,
				"field": "bz"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 192,
				"height": 9.75,
				"width": 49.5,
				"title": "备注：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 213,
				"height": 43.5,
				"width": 534,
				"field": "tb",
				"textAlign": "center",
				"autoCompletion":true,
				"columns": [
					[{
						"title": "序号",
						"field": "xh",
						"width": 63.17971453712634,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "xh"
					}, {
						"title": "包号",
						"field": "bh",
						"width": 199.21962511096062,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "bh"
					}, {
						"title": "数量",
						"field": "sl",
						"width": 63.172826291453156,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sl"
					}, {
						"title": "最大长度",
						"field": "zdcd",
						"width": 63.177296534945775,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "zdcd"
					}, {
						"title": "说明",
						"field": "sm",
						"width": 145.25044271776756,
						"colspan": 1,
						"rowspan": 1,
						"checked": true,
						"columnId": "sm"
					}]
				]
			},
			"printElementType": {
				"title": "表格",
				"type": "tableCustom"
			}
		}, {
			"options": {
				"left": 199.5,
				"top": 769.5,
				"height": 9.75,
				"width": 49.5,
				"title": "制单日期：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 250.5,
				"top": 769.5,
				"height": 9.75,
				"width": 120,
				"field": "zdrq"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 379.5,
				"top": 769.5,
				"height": 9.75,
				"width": 28.5,
				"title": "品管："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 67.5,
				"top": 769.5,
				"height": 9.75,
				"width": 120,
				"field": "zdr"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 469.5,
				"top": 769.5,
				"height": 9.75,
				"width": 39,
				"title": "签收人："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 769.5,
				"height": 9.75,
				"width": 37.5,
				"title": "制单人：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 250.5,
				"top": 786,
				"height": 9.75,
				"width": 120,
				"field": "cz"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 199.5,
				"top": 786,
				"height": 9.75,
				"width": 49.5,
				"title": "传真：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 379.5,
				"top": 786,
				"height": 9.75,
				"width": 28.5,
				"title": "电话："
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 67.5,
				"top": 786,
				"height": 9.75,
				"width": 120,
				"field": "dz"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 409.5,
				"top": 786,
				"height": 9.75,
				"width": 120,
				"field": "dh"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 28.5,
				"top": 786,
				"height": 9.75,
				"width": 36,
				"title": "地址：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 337.5,
				"top": 175.5,
				"height": 9.75,
				"width": 49.5,
				"title": "工程编号：",
				"textAlign": "justify"
			},
			"printElementType": {
				"type": "text"
			}
		}, {
			"options": {
				"left": 388.5,
				"top": 175.5,
				"height": 9.75,
				"width": 120,
				"field": "gcbh"
			},
			"printElementType": {
				"type": "text"
			}
		}],
		"paperNumberLeft": 280.5,
		"paperNumberTop": 813,
		"paperNumberFormat": "第paperNo页，共paperCount页"
	}]
}