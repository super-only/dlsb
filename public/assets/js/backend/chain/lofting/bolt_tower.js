define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    //隐藏加载数据
    function HiddenDiv() {
        $("#loading").hide();
    }
    function col_index(div_content = "#tbshow"){
        var col = 0;
        $(div_content).find("tr").each(function () {
            col++;
            $(this).children('td').eq(0).text(col);
        });
    }
    ids = typeof ids=="undefined"?"":ids;
    tower_name = typeof tower_name=="undefined"?"":tower_name;
    tableField = typeof tableField=="undefined"?"":tableField;
    detailTableField = typeof detailTableField=="undefined"?[]:detailTableField;
    TD_TypeName = typeof TD_TypeName=="undefined"?'':TD_TypeName;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/bolt_tower/index' + location.search,
                    add_url: 'chain/lofting/bolt_tower/add',
                    edit_url: 'chain/lofting/bolt_tower/edit',
                    del_url: 'chain/lofting/bolt_tower/del',
                    multi_url: 'chain/lofting/bolt_tower/multi',
                    import_url: 'chain/lofting/bolt_tower/import',
                    table: 'bolttxk',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'BTXK_ID',
                sortName: 'BTXK_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}, operate: false},
                        {checkbox: true},
                        // {field: 'BTXK_ID', title: __('Btxk_id')},
                        {field: 'TD_TypeName', title: __('Td_typename'), operate: 'LIKE'},
                        {field: 'TD_Pressure', title: __('Td_pressure'), operate: 'LIKE'},
                        {field: 'Writer', title: __('Writer'), operate: false},
                        {field: 'WriteDate', title: __('Writedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'BTXK_Memo', title: __('Btxk_memo'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $(document).on('click', "#table_add", function(){
                var col_count = $("#tbshow").find("tr").length+1;
                var field_append='<tr>'
                        +'<td>'+col_count+'</td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-success detail"><i class="fa fa-calendar"></i></a></td>';
                for(var i=0;i<tableField.length;i++){
                    field_append += '<td '+tableField[i][6]+'><input class="small_input" type="'+tableField[i][2]+'" '+tableField[i][4]+' name="table_row['+tableField[i][1]+'][]" value="'+tableField[i][5]+'"></td>';
                }
                field_append += '</tr>';
                $("#tbshow").append(field_append);
            });
            $(document).on('click', ".del", function(e){
                var indexChoose = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[BTXKS_ID][]"]').val();
                if(num!=0){
                    var index = layer.confirm("即将删除该段所有信息，确定删除？", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: 'chain/lofting/bolt_tower/delDetail',
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                layer.msg(ret.msg);
                                if(ret.code==1){
                                    $( e.target ).closest("tr").remove();
                                    col_index("#tbshow");
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }else{
                    $( e.target ).closest("tr").remove();
                    col_index("#tbshow");
                }
                
                
            });
            $(document).on('click', ".detail", function(e){
                var indexChoose = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[BTXKS_ID][]"]').val();
                if(num==0){
                    var index = layer.confirm("请先保存段", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        layer.close(index);
                    })
                }else{
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        end:function(){
                            window.location.reload();
                        }
                    };
                    Fast.api.open('chain/lofting/bolt_tower/detail/ids/'+num,"螺栓塔型库段明细",options);
                }
                
            });
            $(document).on('click', "#configure", function(e){
                var Td_typename = $("#c-TD_TypeName").val();
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        window.location.reload();
                    }
                };
                Fast.api.open('chain/lofting/bolt_tower/configure/ids/'+ids,"配置杆塔螺栓数",options);
            })
            $(document).on('click', "#copyLS", function(e){
                var TD_TypeName = $("#c-TD_TypeName").val();
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length == 0){
                            layer.msg("复制失败，请刷新后重试！");
                        }else{
                            layer.msg("请稍等，正在复制中");
                            $.ajax({
                                url: 'chain/lofting/bolt_tower/copyDetail',
                                type: 'post',
                                dataType: 'json',
                                async: false,
                                data: {ids: ids,sectId: value},
                                success: function (ret) {
                                    layer.msg(ret.msg);
                                    if (ret.hasOwnProperty("code")) {
                                        if (ret.code === 1) {
                                            window.location.reload();
                                        }
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }
                    }
                };
                Fast.api.open('chain/lofting/bolt_tower/copyLs/TD_TypeName/'+TD_TypeName,"复制螺栓塔型库",options);
            })
        },
        copyls: function () {
            var height = document.body.clientHeight-$('#topDiv').outerHeight(true)-100;
            Controller.api.bindevent("copyls");
            var isExist = $("#table");
            // 初始化表格
            isExist.bootstrapTable({
                data: [],
                search: false,
                pagination: false,
                clickToSelect: true,
                singleSelect: true,
                height: height,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'C_Num', title: '合同号'},
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'TD_Pressure', title: '电压等级'},
                        {field: 'Writer', title: '制表人'}
                    ]
                ]
            });
            var isSect = $("#fieldTable");
            // 初始化表格
            isSect.bootstrapTable({
                data: [],
                height: height,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {checkbox: true},
                        {field: 'BTXKS_ID', title: 'BTXKS_ID',visible: false},
                        {field: 'BTXKS_SectName', title: '段名'},
                        {field: 'count', title: '单段数'},
                        {field: 'weight', title: '单段重量'},
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(isExist);
            Table.api.bindevent(isSect);
            ajax_copy_td({TD_TypeName: $("#TD_TypeName").val()});
            $(document).on('click', "#search", function(e){
                var TD_TypeName =  $("#TD_TypeName").val();
                var search = {TD_TypeName: TD_TypeName};
                ajax_copy_td(search);
            });
            function ajax_copy_td(search={TD_TypeName: ""}){
                $.ajax({
                    url: 'chain/lofting/bolt_tower/copyLs/TD_TypeName/'+TD_TypeName,
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: search,
                    success: function (ret) {
                        var tableHtml = [];
                        if(ret.code==1) tableHtml = ret.data;
                        isExist.bootstrapTable('load',tableHtml);
                        isSect.bootstrapTable('load',[]);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }

            isExist.on("click-row.bs.table",function(row, $element){
                var BTXK_ID = $element.BTXK_ID;
                if(BTXK_ID == "") layer.msg("请刷新后重试！");
                else{
                    $.ajax({
                        url: 'chain/lofting/bolt_tower/copyLsDetail',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {BTXK_ID: BTXK_ID},
                        success: function (ret) {
                            var tableHtml = [];
                            if(ret.code==1) tableHtml = ret.data;
                            isSect.bootstrapTable('load',tableHtml);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }
            });

            $(document).on('click', "#copy", function(e){
                var content = isSect.bootstrapTable('getSelections');
                if(content.length == 0) layer.msg("请选择段后再复制！");
                else{
                    var sectId = [];
                    $.each(content,function(index,e){
                        sectId.push(e["BTXKS_ID"]);
                    });    
                    Fast.api.close(sectId);
                }
            })
            
        },
        configure: function () {
            var leftTableHeight,rightTableHeight;
            leftTableHeight = document.body.clientHeight-$('#topDiv').outerHeight(true);
            rightTableHeight = leftTableHeight-$('#rightTopDiv').outerHeight(true);
            $('.rightTable').height(rightTableHeight);
            Controller.api.bindevent("configure");
            var table = $("#leftTable");
            // 初始化表格
            table.bootstrapTable({
                data: [],
                search: false,
                pagination: false,
                height: leftTableHeight,
                clickToSelect: true,
                singleSelect: true,
                idField: 'SCD_ID',
                rowStyle: function (row, index) {
                    var style = {};
                    if(row.BTXK_ID){
                        style = {css:{'background':'#87CEEB'}};
                    }
                    return style;
                },
                columns: [
                    [
                        {checkbox:true},
                        {field: 'id', title: "序号"},
                        {field: 'SCD_ID', title: 'SCD_ID',visible:false},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'TH_Height', title: '呼高'},
                        {field: 'sect', title: '段落组合'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on('click', "#choose", function(e){
                var Td_typename = $("#TD_TypeName").val();
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value){
                            $("#c-C_Num").val(value.C_Num);
                            $("#c-Customer_Name").val(value.Customer_Name);
                            $("#c-TD_TypeName").val(value.TD_TypeName);
                            $("#c-T_Num").val(value.T_Num);
                            $("#c-t_project").val(value.t_project);
                            $("#c-TD_Pressure").val(value.TD_Pressure);
                            var TD_ID = value.TD_ID;
                            $.ajax({
                                url: 'chain/lofting/bolt_tower/sectconfigdetail',
                                type: 'post',
                                dataType: 'json',
                                data: {TD_ID:TD_ID},
                                async: false,
                                success: function (ret) {
                                    var tableContent = [];
                                    if (ret.code === 1) {
                                        tableContent = ret.data;
                                    }
                                    table.bootstrapTable('load',tableContent);
                                    // clear();
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }
                    }
                };
                Fast.api.open('chain/lofting/bolt_tower/chooseCompact/tower/'+Td_typename,"任务单塔型选择",options);
            })

            table.on('check.bs.table',function(row, $element){
                var SCD_ID = $element.SCD_ID;
                if(!SCD_ID) return false;
                $.ajax({
                    url: 'chain/lofting/bolt_tower/createmain',
                    type: 'post',
                    dataType: 'json',
                    data: {SCD_ID:SCD_ID},
                    async: false,
                    success: function (ret) {
                        $("#SCD_ID").val(SCD_ID);
                        if (ret.code === 1) {
                            $("#c-AllPTSect").val(ret.data['AllPTSect']);
                            $("#c-AllFDSect").val(ret.data['AllFDSect']);
                            $("#c-BanSect").val(ret.data['BanSect']);
                            if(ret.data['ifDP'] == 1){
                                $("#c-ifDP").prop("checked",true);
                                localStorage.setItem("ifDP",true);
                            }else{
                                $("#c-ifDP").prop("checked",false);
                                localStorage.setItem("ifDP",false);
                            }
                            if(ret.data['ifKJLM'] == 1){
                                $("#c-ifKJLM").prop("checked",true);
                                localStorage.setItem("ifKJLM",true);
                            }else{
                                $("#c-ifKJLM").prop("checked",false);
                                localStorage.setItem("ifKJLM",false);
                            }
                            if(ret.data['ifBLM'] == 1){
                                $("#c-ifBLM").prop("checked",true);
                                localStorage.setItem("ifBLM",true);
                            }else{
                                $("#c-ifBLM").prop("checked",false);
                                localStorage.setItem("ifBLM",false);
                            }
                            $("#ifFDType").find("option[value='"+ret.data['ifFDType']+"']").prop("selected",true);
                            localStorage.setItem("ifFDType",ret.data['ifFDType']);
                            if(ret.data['ifDPPDTD_P'] == 1){
                                $("#c-ifDPPDTD_P").prop("checked",true);
                                localStorage.setItem("ifDPPDTD_P",true);
                            }else{
                                $("#c-ifDPPDTD_P").prop("checked",false);
                                localStorage.setItem("ifDPPDTD_P",false);
                            }
                            if(ret.data['ifDPPDTD_T'] == 1){
                                $("#c-ifDPPDTD_T").prop("checked",true);
                                localStorage.setItem("ifDPPDTD_T",true);
                            }else{
                                $("#c-ifDPPDTD_T").prop("checked",false);
                                localStorage.setItem("ifDPPDTD_T",false);
                            }
                            $("#tbodyDetail").html(ret.data['table']);
                        }else{
                            layer.msg(ret.msg);
                            clear();
                            $("#c-ifDP").prop("checked",localStorage.getItem("ifDP")==="true"?true:false);
                            $("#c-ifKJLM").prop("checked",localStorage.getItem("ifKJLM")==="true"?true:false);
                            $("#c-ifBLM").prop("checked",localStorage.getItem("ifBLM")==="true"?true:false);
                            $("#ifFDType").find("option[value='"+localStorage.getItem("ifFDType")+"']").prop("selected",true);
                            $("#c-ifDPPDTD_P").prop("checked",localStorage.getItem("ifDPPDTD_P")==="true"?true:false);
                            $("#c-ifDPPDTD_T").prop("checked",localStorage.getItem("ifDPPDTD_T")==="true"?true:false);
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });

            table.on('uncheck.bs.table',function(row, $element){
                clear();
            });

            function clear(){
                $("#c-AllPTSect").val("");
                $("#c-AllFDSect").val("");
                $("#c-BanSect").val("");
                $("#c-ifDP").prop("checked",false);
                $("#c-ifKJLM").prop("checked",false);
                $("#c-ifBLM").prop("checked",false);
                $("#ifFDType").find("option[text='']").prop("selected",true);
                $("#c-ifDPPDTD_P").prop("checked",false);
                $("#c-ifDPPDTD_T").prop("checked",false);
                $("#tbodyDetail").html("");
            }

            $(document).on('click', "#c-ifDP", function(e){
                let flag = $(this).is(':checked');
                localStorage.setItem("ifDP",flag);
            })
            $(document).on('click', "#c-ifKJLM", function(e){
                let flag = $(this).is(':checked');
                localStorage.setItem("ifKJLM",flag);
            })
            $(document).on('click', "#c-ifBLM", function(e){
                let flag = $(this).is(':checked');
                localStorage.setItem("ifBLM",flag);
            })
            $(document).on('change', "#ifFDType", function(e){
                let flag = $(this).val();
                localStorage.setItem("ifFDType",flag);
            })
            $(document).on('click', "#c-ifDPPDTD_P", function(e){
                let flag = $(this).is(':checked');
                localStorage.setItem("ifDPPDTD_P",flag);
            })
            $(document).on('click', "#c-ifDPPDTD_T", function(e){
                let flag = $(this).is(':checked');
                localStorage.setItem("ifDPPDTD_T",flag);
            })


            $(document).on('click', "#allocation", function(){
                var ban = $("#c-BanSect").val();
                var BTXK_ID = $("#BTXK_ID").val();
                var SCD_ID = $("#SCD_ID").val();
                if(ban == ''){
                    layer.msg("没有半防盗段落");
                }else if(SCD_ID == ''){
                    layer.msg("先选择杆塔");
                }else{
                    $.ajax({
                        url: 'chain/lofting/bolt_tower/createmainDetail',
                        type: 'post',
                        dataType: 'json',
                        data: {BTXK_ID:BTXK_ID, ban: ban,SCD_ID: SCD_ID},
                        async: false,
                        success: function (ret) {
                            if(ret.code == 1){
                                $("#tbodyDetail").html(ret.data);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }
            });
            $('#rightTable').on('keyup','td input[name="BD_PTCount"]',function (e) {
                if(e.keyCode == 13){
                    var col = $(this).parents('tr').index();
                    var row = $(this).parents("tr").find("td").index($(this).parents('td'));
                    var allCol = $("#tbodyDetail > tr").length;
                    if((col+1)<=allCol){
                        $("#tbodyDetail").find("tr").eq(col+1).find('td').eq(row).find('input').select();
                    }
                }else{
                    var index = $(this).parents('tr').index();
                    var sum = $(this).parents('tbody').find('tr').eq(index).find('td').eq(8).find('input').val().trim();
                    var pt = $(this).parents('tbody').find('tr').eq(index).find('td').eq(9).find('input').val().trim();
                    sum==''?sum=0:'';
                    pt==''?pt=0:'';
                    sum = parseInt(sum);
                    pt = parseInt(pt);
                    var yu = sum-pt;
                    if(yu < 0){
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(9).find('input').val(0);
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(10).find('input').val(sum);
                    }else{
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(10).find('input').val(yu);
                    }
                }
                
            })
            $('#rightTable').on('keyup','td input[name="BD_FDCount"]',function (e) {
                if(e.keyCode == 13){
                    var col = $(this).parents('tr').index();
                    var row = $(this).parents("tr").find("td").index($(this).parents('td'));
                    var allCol = $("#tbodyDetail > tr").length;
                    if((col+1)<=allCol){
                        $("#tbodyDetail").find("tr").eq(col+1).find('td').eq(row).find('input').select();
                    }
                }else{
                    var index = $(this).parents('tr').index();
                    var sum = $(this).parents('tbody').find('tr').eq(index).find('td').eq(8).find('input').val().trim();
                    var fd = $(this).parents('tbody').find('tr').eq(index).find('td').eq(10).find('input').val().trim();
                    sum==''?sum=0:'';
                    fd==''?fd=0:'';
                    sum = parseInt(sum);
                    fd = parseInt(fd);
                    var yu = sum-fd;
                    if(yu < 0){
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(9).find('input').val(sum);
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(10).find('input').val(0);
                    }else{
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(9).find('input').val(yu);
                    }
                }
            })

            $(document).on('click', ".table_del", function(e){
                $( e.target ).closest("tr").remove();
                col_index("#tbodyDetail");
            });

            $(document).on('click', "#c-save", function(e){
                var form = $("#rightFrom").serializeArray();
                var table_content = $("#rightTableFrom").serializeArray();
                $.ajax({
                    url: 'chain/lofting/bolt_tower/configureSave',
                    type: 'post',
                    dataType: 'json',
                    data: {form: JSON.stringify(form), table: JSON.stringify(table_content)},
                    async: false,
                    success: function (ret) {
                        layer.msg(ret.msg);
                        if(ret.code==1){
                            var content = ret.data;
                            var content_btxk_id = content.BTXK_ID;
                            var list = table.bootstrapTable('getSelections')[0];
                            list.BTXK_ID = content_btxk_id;
                            table.bootstrapTable('updateRow',{"index":list.id-1,"row":list});
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            })

            $(document).on('click', "#c-create", function(e){
                var SCD_ID = $("#SCD_ID").val();
                var BTXK_ID = $("#BTXK_ID").val();
                if(SCD_ID == ''){
                    layer.msg("先选择杆塔");
                }else{
                    $.ajax({
                        url: 'chain/lofting/bolt_tower/createSave',
                        type: 'post',
                        dataType: 'json',
                        data: {SCD_ID: SCD_ID,BTXK_ID:BTXK_ID},
                        async: false,
                        success: function (ret) {
                            layer.msg(ret.msg);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }
            })
        },
        choosecompact: function () {
            $(document).on('click', ".btn-close", function(){
                Fast.api.close();
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/bolt_tower/chooseCompact/tower/'+tower_name + location.search,
                    table: 'tasksect',
                }
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'T_Num',
                sortName: 'TD_ID',
                sortOrder: 'DESC',
                search: false,
                showToggle: false,
                showExport: false,
                clickToSelect: true,
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'C_Num', title: __('C_Num'), operate: 'LIKE'},
                        {field: 'PC_Num', title: __('PC_Num'), operate: 'LIKE'},
                        {field: 'T_Num', title: __('T_Num'), operate: 'LIKE'},
                        {field: 'T_Sort', title: __('T_Sort'), opearte: false},
                        {field: 'T_Company', title: __('T_Company'), opearte: false},
                        {field: 'T_WriterDate', title: __('T_WriterDate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 't_project', title: __('t_project'), operate: 'LIKE'},
                        {field: 'TD_TypeName', title: __('TD_TypeName'), defaultValue:Config.TD_TypeName, operate: 'LIKE'},
                        {field: 'TD_TypeNameGY', title: __('TD_TypeNameGY'), operate: false},
                        {field: 'TD_Pressure', title: __('TD_Pressure'), operate:'LIKE'},
                        {field: 'TD_Count', title: __('TD_Count'), operate:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
            
        },
        detail: function () {
            Controller.api.bindevent("detail");
            $("#detailTable").height(document.body.clientHeight-100);
            var ids = $("#c-ids").val();
            $(document).on('click', "#table_add", function(){
                var col_count = $("#tbshow").find("tr").length+1;
                var BTXKS_SectName = $("#c-BTXKS_SectName").val();
                var field_append='<tr>'
                        +'<td>'+col_count+'</td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                        +'<td>'+BTXKS_SectName+'</td>';
                for(var i=0;i<detailTableField.length;i++){
                    if(detailTableField[i][0]==""){
                        field_append += '<td><input class="small_input" type="text" value=""></td>';
                    }else if(detailTableField[i][1]=="BD_Flag"){
                        field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" '+detailTableField[i][3]+' name="'+detailTableField[i][1]+'[]" value="0"></td>';
                    }else if(detailTableField[i][1]=="BD_sFlag"){
                        field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" '+detailTableField[i][3]+' name="'+detailTableField[i][1]+'[]" value="普通"></td>';
                    }else{
                        field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" '+detailTableField[i][3]+' name="'+detailTableField[i][1]+'[]" value=""></td>';
                    }
                }
                field_append += '</tr>';
                $("#tbshow").append(field_append);
            });
            
            $(document).on("change","#searchNum", function(e){
                var detai_id = $("#c-detail_id").val();
                var searchNum = $("#searchNum").find("option:selected").val();
                var searchData = {ids: ids, detai_id: detai_id, searchNum:searchNum};
                $.ajax({
                    url: 'chain/lofting/bolt_tower/detailTable',
                    type: 'post',
                    dataType: 'json',
                    data: searchData,
                    async: false,
                    success: function (ret) {
                        if (ret.code === 1) {
                            tableContent = ret.data;
                            $("#tbshow").html(tableContent);
                            $("#c-detail_id").val(searchNum);
                            $("#c-BTXKS_SectName").val($("#searchNum").find("option:selected").text());
                        }else layer.msg("请刷新后重试！");
                        
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });

            var table = $("#rightTable");
            // 初始化表格
            table.bootstrapTable({
                data: {},
                search: false,
                pagination: false,
                height: 250,
                columns: [
                    [
                        {checkbox: true,formatter(value,row,index){
                            if(index==0){
                                return {
                                    checked:true
                                };
                            }
                        }},
                        {field: 'BD_MaterialName', title: '材料名称'},
                        {field: 'BD_Limber', title: '等级'},
                        {field: 'BD_Type', title: '规格'},
                        {field: 'BD_Lenth', title: '无扣长'},
                        {field: 'BD_Other', title: '另附规格'},
                        {field: 'BD_PerWeight', title: '比重(kg)'},
                        {field: 'FastInPut', title: '代号'}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            rightContent();
            $(document).on('click', "#searchMaterial", function(e){
                var BD_MaterialName = $("#BD_MaterialName").val();
                var BD_Limber = $("#BD_Limber").val();
                var BD_Type = $("#BD_Type").val();
                var BD_Other = $("#BD_Other").val();
                var searchData = {BD_MaterialName:BD_MaterialName,BD_Limber:BD_Limber,BD_Type:BD_Type,BD_Other:BD_Other};
                rightContent(searchData);
            });

            function rightContent(searchData={}){
                $.ajax({
                    url: 'chain/lofting/bolt_tower/rightTable',
                    type: 'post',
                    dataType: 'json',
                    data: searchData,
                    async: false,
                    success: function (ret) {
                        var tableContent = [];
                        if (ret.code === 1) {
                            tableContent = ret.data;
                        }
                        table.bootstrapTable('load',tableContent);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }

            $(document).on('click', "#echoPrint", function(e){
                var hidden = $("#searchFast").is(":hidden");
                if(hidden){
                    var height = document.body.clientHeight-390;
                    $("#detailTable").height(height);
                    $("#searchFast").show();
                }else{
                    $("#detailTable").height(document.body.clientHeight-100);
                    $("#searchFast").hide();
                }
            });

            $(document).on('keyup', "#daihao", function(e){
                var val = $("#daihao").val();
                table.bootstrapTable("uncheckAll");
                $('#rightTable tbody tr').hide().find("td:eq(7)").filter(":contains('" +(val) + "')").parent("tr").show();
                var chooseId = $("#rightTable tbody tr:visible:first").data("index");
                table.bootstrapTable("check",chooseId);
                if(e.keyCode == 13){
                    var BTXKS_SectName = $("#c-BTXKS_SectName").val();
                    var content = table.bootstrapTable("getAllSelections");
                    if(content.length==0){
                        layer.msg("未选中！")
                        return false;
                    }
                    addNewDetail(BTXKS_SectName,content);
                    $('#rightTable tbody tr').show();
                    table.bootstrapTable("uncheckAll");
                    table.bootstrapTable("check",0);
                    $("#daihao").val("");
                }
            });

            $(document).on('keyup', "#rightTable", function(event){
                if (event.keyCode == 13) {
                    var BTXKS_SectName = $("#c-BTXKS_SectName").val();
                    var content = table.bootstrapTable("getAllSelections");
                    if(content.length==0){
                        layer.msg("未选中！")
                        return false;
                    }
                    addNewDetail(BTXKS_SectName,content);
                }
            });

            table.on('dbl-click-row.bs.table',function(row, $element){
                var BTXKS_SectName = $("#c-BTXKS_SectName").val();
                addNewDetail(BTXKS_SectName,[$element]);
            });

            function addNewDetail(num,content)
            {
                var field_append= "";
                var col_count = $("#tbshow").find("tr").length+1;
                $.each(content,function(index,e){
                    field_append += '<tr>'
                        +'<td>'+col_count+'</td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                        +'<td>'+num+'</td>';
                    for(var i=0;i<detailTableField.length;i++){
                        if(detailTableField[i][0]==""){
                            field_append += '<td><input class="small_input" type="text" value=""></td>';
                        }else if(detailTableField[i][1]=="BD_Flag"){
                            field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" data-rule="'+detailTableField[i][3]+'" name="'+detailTableField[i][1]+'[]" value="0"></td>';
                        }else if(detailTableField[i][1]=="BD_sFlag"){
                            field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" data-rule="'+detailTableField[i][3]+'" name="'+detailTableField[i][1]+'[]" value="普通"></td>';
                        }else{
                            field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" data-rule="'+detailTableField[i][3]+'" name="'+detailTableField[i][1]+'[]" value="'+(e[detailTableField[i][5]]==undefined?detailTableField[i][5]:e[detailTableField[i][5]])+'"></td>';
                        }
                    }
                    field_append += '</tr>';
                    col_count++;
                });
                $("#tbshow").append(field_append);
            }
            $(document).on('click', ".btn-success", function(e){
                var formData = $("#detail-form").serializeArray();
                var flag= true;
                $.each(formData,function(index,e){
                    if(e.name=="BD_SumCount[]" && e.value==""){
                        layer.msg("数量必须填写");
                        flag = false;
                    }
                });
                if(flag == false) return false;
                $.ajax({
                    url: 'chain/lofting/bolt_tower/detail/ids/'+$("#c-detail_id").val(),
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData)},
                    success: function (ret) {
                        layer.msg(ret.msg);
                        if (ret.hasOwnProperty("code")) {
                            if (ret.code === 1) {
                                window.location.reload();
                            }
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on('click', ".del", function(e){
                var indexChoose = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(3).find('input').val();
                if(num!=''){
                    var index = layer.confirm("确定删除？", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: 'chain/lofting/bolt_tower/delDetailContent',
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                layer.msg(ret.msg);
                                if(ret.code==1){
                                    $( e.target ).closest("tr").remove();
                                    col_index("#tbshow");
                                }
                                
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }else{
                    $( e.target ).closest("tr").remove();
                    col_index("#tbshow");
                }
                
                
            });
            $('#tbshow').on('keyup','td',function (e) {
                if(e.keyCode ==13){
                    var col = $(this).parents('tr').index();
                    var row = $(this).parents("tr").find("td").index($(this));
                    var allCol = $("#tbshow > tr").length;
                    if((col+1)<=allCol){
                        $("#tbshow").find("tr").eq(col+1).find('td').eq(row).find('input').select();
                    }
                }
            })
            $('#tbshow').on('keyup','td input[name="BD_SumCount[]"]',function () {
                getWeight($(this));
            })
            $('#tbshow').on('keyup','td input[name="BD_SingelWeight[]"]',function () {
                getWeight($(this));
            })
            function getWeight(content)
            {
                var dz = content.parents('tr').find("input[name='BD_SingelWeight[]']").val().trim();
                dz = dz==''?0:parseFloat(dz);
                var number = content.parents('tr').find("input[name='BD_SumCount[]']").val().trim();
                number = number==''?0:parseInt(number);
                var weight=0;
                weight = (dz*number).toFixed(2);
                content.parents('tr').find("input[name='BD_SumWeight[]']").val(weight);
            }
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data;
                    }else if(field != "detail"){
                        window.location.reload();
                    }
                    return false;
                });
                $(document).on('click', ".btn-close", function(){
                    Fast.api.close();
                });
            }
        }
    };
    return Controller;
});