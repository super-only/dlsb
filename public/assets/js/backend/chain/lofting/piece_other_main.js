define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jquerybase64'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/piece_other_main/index' + location.search,
                    add_url: 'chain/lofting/piece_other_main/add',
                    edit_url: 'chain/lofting/piece_other_main/edit',
                    del_url: 'chain/lofting/piece_other_main/del',
                    multi_url: 'chain/lofting/piece_other_main/multi',
                    import_url: 'chain/lofting/piece_other_main/import',
                    table: 'piece_other_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate: false},
                        {field: 'pt_num', title: "任务下达单号",operate: "="},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'record_time', title: __('Record_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'order', title: __('Order'), searchList: Config.orderArr,formatter:function(value,row,index){return Config.orderArr[value]}},
                        {field: 'device', title: __('Device'), searchList: Config.machine_arr,formatter:function(value,row,index){return Config.machine_arr[value]}},
                        {field: 'zg_amount', title: __('Zg_amount'),operate: false},
                        {field: 'zg_hours', title: __('Zg_hours'),operate: false},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
        },
        selectpeople: function () {
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'operator', title: '操作者'},
                        {field: 'hours', title: "时间"},
                        {
                            field: 'buttons',
                            // width: "120px",
                            title: '操作',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'del',
                                    title: '删除1',
                                    classname: 'btn btn-xs btn-danger btn-click btn-del-click',
                                    icon: 'fa fa-trash',
                                    // url: 'example/bootstraptable/detail',
                                    // callback: function (data) {
                                    //     Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    // },
                                    // visible: function (row) {
                                    //     //返回true时按钮显示,返回false隐藏
                                    //     return true;
                                    // }
                                }
                            ],
                            formatter: Table.api.formatter.buttons
                        }
                    ]
                ],
                onClickCell: function(field, value, row, $element) {
                    if(field=='hours'){
                        $element.attr('contenteditable', true);
                        $element.attr('style', "display:block");
                        $element.blur(function() {
                            let index = $element.parent().data('index');
                            let tdValue = $element.html();
                            saveData(index, field, tdValue);
                        })
                    }
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            function saveData(index, field, value) {
                table.bootstrapTable('updateCell', {
                    index: index,
                    field: field,
                    value: value
                });
                return false;
            }
            $(document).on('click', ".btn-del-click", function(e){
                let index = $(this).parents('tr').index();
                let all_data = table.bootstrapTable('getData');
                all_data.splice(index,1);
                table.bootstrapTable('load',all_data);
            });
            
            $(document).on('click', "#sure", function(e){
                let all_data = table.bootstrapTable('getData');
                Fast.api.close(all_data);
            });
            
            $(document).on('click', "#selectPeople", function(e){
                var url = "chain/sale/project_cate_log/selectSaleMan/type/1";
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        let act_value = value[0];
                        table.bootstrapTable("append",{
                            "title": act_value.E_PerNum,
                            "operator": act_value.E_PerNum,
                            "hours": 8
                        });
                    }
                };
                Fast.api.open(url,"员工选择",options);
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }
                    return false;
                });
                $("#tab_tower").height(document.body.clientHeight-100);
                var PT_Num_list = Config.pt_list;
                var jt_left_table;
                var jt_right_table;
                var left_table_height = document.body.clientHeight-270;
                var right_table_height = document.body.clientHeight-200;
                var flag = Config.flag;

                
                $(document).on('change', "#c-device", function(e){
                    $("#c-operator").val("");
                    $(".sj_operator").text("");
                    // var pack_val = $(e.target).selectpicker('val');
                    // $.ajax({
                    //     url: 'chain/lofting/piece_other_main/selectMachine',
                    //     type: 'post',
                    //     dataType: 'json',
                    //     data: {num:pack_val},
                    //     success: function (ret) {
                    //         // if(ret.code==1){
                    //             $("#c-operator_recent").parent("div").html(ret.data);
                    //             $("#c-operator_recent").selectpicker("refresh")
                    //         // }
                    //         $("#c-operator").val("");
                            
                    //     }, error: function (e) {
                    //         Backend.api.toastr.error(e.message);
                    //     }
                    // });
                    
                });

                $(document).on('click', "#selectxd", function(e){
                    if(!Config.ids){
                        layer.msg("请先保存主信息");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            console.log(value);
                            if($.inArray(value.PT_Num, PT_Num_list)===-1){
                                console.log(value.PT_Num,PT_Num_list);
                                PT_Num_list.push(value.PT_Num);
                                $("#tab_tower ul").append('<li class=""><a href="#'+value.PT_Num+'" data-toggle="tab">'+value.TD_TypeName+'('+value.PT_Num+')<i class="close-tab fa fa-remove"></i></a></li>');
                                var table_content = '<div class="tab-pane fade" id="'+value.PT_Num+'">'
                                                  + '<div class="form-group col-xs-12 col-sm-5 leftPart">'
                                                  + '<div class="form-group col-xs-12 col-sm-12">'
                                                  +'<div class="form-group col-xs-12 col-sm-4 search_field">'
                                                    +'<label class="control-label col-xs-12 col-sm-4">材料:</label>'
                                                    +'<div class="col-xs-12 col-sm-8">'
                                                        +'<select class="form-control selectpicker" name="stuff" autocomplete="off">'
                                                            +'<option value="" selected="selected">请输入</option>'
                                                        +'</select>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group col-xs-12 col-sm-4 search_field">'
                                                    +'<label class="control-label col-xs-12 col-sm-4">规格:</label>'
                                                    +'<div class="col-xs-12 col-sm-8">'
                                                        +'<select class="form-control selectpicker" name="specification" autocomplete="off">'
                                                            +'<option value="" selected="selected">请输入</option>'
                                                        +'</select>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group col-xs-12 col-sm-4 search_field">'
                                                    +'<label class="control-label col-xs-12 col-sm-4">材质:</label>'
                                                    +'<div class="col-xs-12 col-sm-8">'
                                                        +'<select class="form-control selectpicker" name="material" autocomplete="off">'
                                                            +'<option value="" selected="selected">请输入</option>'
                                                        +'</select>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group col-xs-12 col-sm-8">'
                                                    +'<button type="button" class="btn btn-success edit_search" data-id="'+value.PT_Num+'">搜索</button>'
                                                    +'<button type="button" class="btn btn-info right_move" data-id="'+value.PT_Num+'">右移</button>'
                                                +'</div></div><div class="form-group col-xs-12 col-sm-12">'
                                                    + '<table id="left_'+value.PT_Num+'_table" class="table table-striped table-bordered table-hover table-nowrap" width="100%"></table>'
                                                    + '</div></div>'
                                                    + '<div class="form-group col-xs-12 col-sm-7 rightPart">'
                                                    + '<table id="right_'+value.PT_Num+'_table" class="table table-striped table-bordered table-hover table-nowrap" width="100%"></table>'
                                                    + '</div></div>';
                                $("#myTabContent").append(table_content);
                                jt_left_table = $('#left_'+value.PT_Num+'_table');
                                jt_right_table = $('#right_'+value.PT_Num+'_table');
                                // 初始化表格
                                jt_left_table.bootstrapTable({
                                    data:[],
                                    uniqueId: "id",
                                    height: left_table_height,
                                    columns: [
                                        [
                                            {checkbox: true},
                                            {field: 'stuff', title: '材料'},
                                            {field: 'specification', title: '规格'},
                                            {field: 'material', title: '材质'},
                                            {field: 'pt_num', title: '下达单号'},
                                            {field: 'id', title: 'id',visible: false},
                                            {field: 'piece_detail_id', title: 'piece_detail_id',visible: false}
                                        ]
                                    ]
                                });
                                // 为表格绑定事件
                                Table.api.bindevent(jt_left_table);
                                // 初始化表格
                                createtable(jt_right_table,[],right_table_height);
                                // 为表格绑定事件
                                Table.api.bindevent(jt_right_table);
                                $.ajax({
                                    url: 'chain/lofting/piece_other_main/getPieceList',
                                    type: 'post',
                                    dataType: 'json',
                                    async: false,
                                    data: {num:value.PT_Num},
                                    success: function (ret) {
                                        var left_table_content = [];
                                        if(ret.code == 1){
                                            left_table_content = ret.data["list"];
                                            $.each(ret.data["search"],function(index,item){
                                                $("#"+value.PT_Num+" .leftPart").find(".search_field").eq(index).find("div").html(item);
                                            });
                                        }
                                        jt_left_table.bootstrapTable('load',left_table_content);
                                    }, error: function (e) {
                                        Backend.api.toastr.error(e.message);
                                    }
                                });
                                $('a[href="#'+value.PT_Num+'"]').trigger("click");
                                jt_left_table.on('dbl-click-row.bs.table',function(row,$elemente){
                                    if(!jt_right_table.bootstrapTable('getRowByUniqueId', $elemente.id)){
                                        jt_left_table.bootstrapTable("uncheckAll");
                                        jt_left_table.bootstrapTable("checkBy", {field:"id", values:[$elemente.id]});
                                        var right_table_content = jt_right_table.bootstrapTable("getData");
                                        var table_content = jt_left_table.bootstrapTable("getAllSelections");
                                        table_content = $.merge(right_table_content,table_content);
                                        jt_right_table.bootstrapTable("destroy");
                                        createtable(jt_right_table,table_content,right_table_height);
                                        // 为表格绑定事件
                                        Table.api.bindevent(jt_right_table);
                                        jt_left_table.bootstrapTable("uncheckAll");
                                        jt_right_table.bootstrapTable("uncheckAll");
                                    }
                                });
                            }
                        }
                    };
                    Fast.api.open('chain/lofting/piece_main/selectxd',"任务单塔型选择",options);
                });

                $(document).on("click", "#author", function () {
                    check('审核之后无法修改，确定审核？',"chain/lofting/piece_other_main/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    check('确定弃审？',"chain/lofting/piece_other_main/giveUp",Config.ids);

                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            async: false,
                            success: function (ret) {
                                if(ret.code==1){
                                    layer.msg(ret.msg, {
                                        icon: 1,
                                        time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                      }, function(){
                                        window.location.href = "edit/ids/"+Config.ids;
                                    }); 
                                }else{
                                    layer.msg(ret.msg, {
                                        icon: 2,
                                        time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                    }); 
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }

                function createtable(jt_right_table,add_list=[],right_table_height)
                {
                    jt_right_table.bootstrapTable({
                        data:add_list,
                        height: right_table_height,
                        uniqueId: "id",
                        columns: [
                            [
                                {checkbox: true},
                                {field: 'id', title: 'id'},
                                {field: 'buttons', title: '删除',
                                    buttons: [{
                                        name: 'delete',
                                        text: '',
                                        title: '删除',
                                        classname: 'btn btn-xs btn-danger btn-ajax-button',
                                        icon: 'fa fa-trash',
                                    }],
                                    table: jt_right_table,
                                    formatter: Table.api.formatter.operate
                                },
                                {field: 'sum_weight', title: '登记量(kg/米)'},
                                {field: 'hole_number', title: '登记孔数',visible: false},
                                {field: 'piece_detail_id', title: 'piece_detail_id',visible: false},
                                {field: 'stuff', title: '材料'},
                                {field: 'specification', title: '规格'},
                                {field: 'material', title: '材质'}
                            ]
                        ],
                        onClickCell: function(field, value, row, $element) {
                            var height = $element.parents(".fixed-table-body").scrollTop();
                            if(field=="sum_weight" || field=="hole_number"){
                                $element.attr('contenteditable', true);
                                $element.blur(function() {
                                    let index = $element.parent().data('index');
                                    let tdValue = $element.html();
                                    saveData(index, field, tdValue);
                                })
                            }
                            jt_right_table.bootstrapTable('scrollTo', height);
                        }       
                    });
                };
                function saveData(index, field, value) {
                    jt_right_table.bootstrapTable('updateCell', {
                        index: index,
                        field: field,
                        value: value
                    });
                    return false;
                }

                $(document).on('click', ".btn-ajax-button", function(e){
                    if(flag) return false;
                    var this_index = $(this).parents("tr").index();
                    var add_list = [];
                    var all_data = jt_right_table.bootstrapTable('getData');
                    let pid_string = $.base64.encode(all_data[this_index]["id"]);
                    $.ajax({
                        url: 'chain/lofting/piece_other_main/delDetail/piece_id/'+(Config.ids??0)+'/pid/'+pid_string,
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        success: function (ret) {
                            if(ret.code==0) return false;
                            $.each(all_data,function(index,item){
                                if(item["id"]!=all_data[this_index]["id"]) add_list.push(item);
                            })
                            jt_right_table.bootstrapTable("destroy");
                            createtable(jt_right_table,add_list,right_table_height);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });

                $(document).on("click", "#tab_tower .fa-remove", function(e){
                    var class_name = $(this).parents("a").attr("href");
                    if((Config.ids??0)){
                        $.ajax({
                            url: 'chain/lofting/piece_other_main/deletePiece/ids/'+Config.ids,
                            type: 'post',
                            dataType: 'json',
                            data: {num:class_name},
                            success: function (ret) {
                                if(ret.code==1){
                                    $.each(PT_Num_list,function(index,item){
                                        if(item == class_name.substr(1)) {
                                            PT_Num_list.splice(index,1);
                                        }
                                    });
                                    $(e.target).closest("li").remove();
                                    $(class_name).remove();
                                    jt_left_table.bootstrapTable('destroy');
                                    jt_right_table.bootstrapTable('destroy');
                                    $('#tab_tower ul').find("li:eq(0)>a").trigger("click");
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }else{
                        $.each(PT_Num_list,function(index,item){
                            if(item == class_name.substr(1)) {
                                PT_Num_list.splice(index,1);
                            }
                        });
                        $(e.target).closest("li").remove();
                        $(class_name).remove();
                        jt_left_table.bootstrapTable('destroy');
                        jt_right_table.bootstrapTable('destroy');
                        $('#tab_tower ul').find("li:eq(0)>a").trigger("click");
                    }
                });

                $(document).on('click', "#selectPeople", function(e){
                    let machine = $("#c-device").val();
                    if(!machine){
                        layer.msg("请先选择设备号");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            let operator_string = JSON.stringify(value);
                            $("#c-operator").val(operator_string);
                            let new_operator_list = [];
                            $.each(value,function(v_index,v_e){
                                new_operator_list.push(v_e.title+'：'+v_e.hours);
                            })
                            $(".sj_operator").text(new_operator_list.toString());
                        }
                    };
                    Fast.api.open('chain/lofting/piece_other_main/selectPeople/machine/'+machine+'/ids/'+(Config.ids??0),"操作员选择",options);
                });

                $(document).on("click", "#table_save", function(e){
                    // jt_right_table = $('#'+value.pt_num+' .rightPart .table');

                    var table_head = $("form").serializeArray();
                    var table_list = $("#tab_tower li");
                    var table_input_news = [];
                    $.each(table_list,function(index,item){
                        var href_field = $(item).find("a").attr("href");
                        var need_save_news = $(href_field+' .rightPart .table').bootstrapTable("getData");
                        if(need_save_news.length != 0){
                            table_input_news.push(need_save_news);
                        }
                    });
                    // console.log(table_input_news);
                    // return false;
                    $.ajax({
                        url: 'chain/lofting/piece_other_main/savePiece/ids/'+(Config.ids??0),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {main_data: JSON.stringify(table_head),table_data: JSON.stringify(table_input_news)},
                        success: function (ret) {
                            if(ret.code==1){
                                layer.msg(ret.msg, {
                                    icon: 1,
                                    time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                  }, function(){
                                    if(!(Config.ids??0)) window.location.href = "edit/ids/"+ret.data;
                                });   
                            }else{
                                layer.msg(ret.msg, {
                                    icon: 2,
                                    time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                }); 
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });

                $(document).on('click', ".edit_search", function(e){
                    var pt_num_id = $(this).attr("data-id");
                    $.ajax({
                        url: 'chain/lofting/piece_other_main/searchContent/ids/'+Config.ids,
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {
                            pt_num:pt_num_id,
                            stuff:$("#"+pt_num_id+" select[name='stuff']").val(),
                            specification:$("#"+pt_num_id+" select[name='specification']").val(),
                            material:$("#"+pt_num_id+" select[name='material']").val()
                        },
                        success: function (ret) {
                            var left_table_content = [];
                            if(ret.code == 1){
                                left_table_content = ret.data;
                            }
                            jt_left_table.bootstrapTable('load',left_table_content);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });

                $(document).on('click', ".right_move", function(e){
                    let right_table_content = jt_right_table.bootstrapTable("getData");
                    let length = right_table_content.length;
                    let right_id = [];
                    $.each(right_table_content,function(c_e,c_v){
                        right_id.push(c_v.id);
                    })
                    let choose_data = jt_left_table.bootstrapTable('getSelections');
                    let pid_data = [];
                    $.each(choose_data,function(c_e,c_v){
                        console.log(c_v);
                        if($.inArray(c_v.id,pid_data)===-1 && $.inArray(c_v.id,right_id)===-1) right_table_content.push(c_v);
                    })
                    if(right_table_content.length != length){
                        jt_right_table.bootstrapTable("destroy");
                        // 初始化表格
                        createtable(jt_right_table,right_table_content,right_table_height);
                        // 为表格绑定事件
                        Table.api.bindevent(jt_right_table);
                        jt_left_table.bootstrapTable("uncheckAll");
                        jt_right_table.bootstrapTable("uncheckAll");
                    }else{
                        jt_left_table.bootstrapTable("uncheckAll");
                        jt_right_table.bootstrapTable("uncheckAll");
                    }
                });

                var edit_table_list = $("#tab_tower li");
                $.each(edit_table_list,function(index,item){
                    var href_field = $(item).find("a").attr("href");
                    jt_left_table = $('#left_'+href_field.substr(1)+'_table');
                    jt_right_table = $('#right_'+href_field.substr(1)+'_table');
                    // 初始化表格
                    jt_left_table.bootstrapTable({
                        data:[],
                        uniqueId: "id",
                        height: left_table_height,
                        columns: [
                            [
                                {checkbox: true},
                                {field: 'stuff', title: '材料'},
                                {field: 'specification', title: '规格'},
                                {field: 'material', title: '材质'},
                                {field: 'pt_num', title: '下达单号'},
                                {field: 'id', title: 'id',visible: false},
                                {field: 'piece_detail_id', title: 'piece_detail_id',visible: false}
                            ]
                        ]
                    });
                    // 为表格绑定事件
                    Table.api.bindevent(jt_left_table);
                    $.ajax({
                        url: 'chain/lofting/piece_other_main/getPieceList',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {num:href_field.substr(1)},
                        success: function (ret) {
                            var left_table_content = [];
                            if(ret.code == 1){
                                left_table_content = ret.data["list"];
                                $.each(ret.data["search"],function(index,item){
                                    $(href_field+" .leftPart").find(".search_field").eq(index).find("div").html(item);
                                });
                            }
                            jt_left_table.bootstrapTable('load',left_table_content);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    
                    jt_left_table.on('dbl-click-row.bs.table',function(row,$elemente){
                        if(!jt_right_table.bootstrapTable('getRowByUniqueId', $elemente.id)){
                            jt_left_table.bootstrapTable("uncheckAll");
                            jt_left_table.bootstrapTable("checkBy", {field:"id", values:[$elemente.id]});
                            var right_table_content = jt_right_table.bootstrapTable("getData");
                            var table_content = jt_left_table.bootstrapTable("getAllSelections");
                            table_content = $.merge(right_table_content,table_content);
                            jt_right_table.bootstrapTable("destroy");
                            createtable(jt_right_table,table_content,right_table_height);
                            // 为表格绑定事件
                            Table.api.bindevent(jt_right_table);
                            jt_left_table.bootstrapTable("uncheckAll");
                            jt_right_table.bootstrapTable("uncheckAll");
                        }
                    });
                    // 初始化表格
                    createtable(jt_right_table,Config.list[href_field.substr(1)],right_table_height);
                    // 为表格绑定事件
                    Table.api.bindevent(jt_right_table);
                    if(index==(edit_table_list.length-1)){
                        $('#tab_tower ul').find("li:eq("+index+")>a").trigger("click");
                    }
                });
                $(document).on("click","#tab_tower li a",function (e) {
                    var this_href = $(this).attr("href");
                    jt_left_table = $('#left_'+this_href.substr(1)+'_table');
                    jt_right_table = $('#right_'+this_href.substr(1)+'_table');
                    // Table.api.bindevent(jt_left_table);
                    // Table.api.bindevent(jt_right_table);
                });

            }
        }
    };
    return Controller;
});