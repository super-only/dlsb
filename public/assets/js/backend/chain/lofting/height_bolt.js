const { isEmptyObject } = require("jquery");

define(['jquery', 'bootstrap', 'backend', 'table', 'form','hpbundle','lsdata'], function ($, undefined, Backend, Table, Form,hp,undefined) {
    function col_index(div_content = "#tbshow"){
        var col = 0;
        $(div_content).find("tr").each(function () {
            col++;
            $(this).children('td').eq(0).text(col);
        });
    }
    ids = typeof ids=="undefined"?"":ids;
    tower_name = typeof tower_name=="undefined"?"":tower_name;
    tableField = typeof tableField=="undefined"?"":tableField;
    detailTableField = typeof detailTableField=="undefined"?[]:detailTableField;
    printData = typeof printData=="undefined"?"":printData;
    //隐藏加载数据
    function HiddenDiv() {
        $("#loading").hide();
    }
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/height_bolt/index' + location.search,
                    add_url: 'chain/lofting/height_bolt/add',
                    edit_url: 'chain/lofting/height_bolt/edit',
                    // del_url: 'chain/lofting/height_bolt/del',
                    multi_url: 'chain/lofting/height_bolt/multi',
                    import_url: 'chain/lofting/height_bolt/import',
                    table: 'heightbolt',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'HB_ID',
                sortName: 'HB_ID',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'T_Company', title: '分公司', operate: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 't.C_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 'SCD_TPNum', title: '杆塔号', operate: false},
                        {field: 'scd.SCD_TPNum', title: '杆塔号', operate: 'LIKE', visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'c.PC_Num', title: '工程编号', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 'td.T_Num', title: '任务单号', operate: 'LIKE', visible: false},
                        {field: 't_project', title: '工程名', operate: false},
                        {field: 't.t_project', title: '工程名', operate: 'LIKE', visible: false},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'scd.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 't_shortproject', title: '工程简称', operate: false},
                        {field: 'T_Sort', title: '产品类别',operate:false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'td.TD_Pressure', title: '电压等级', operate: 'LIKE', visible: false},
                        {field: 'TH_Height', title: '呼高', operate: false},
                        {field: 'th.TH_Height', title: '呼高', operate: '=', visible: false},
                        {field: 'SCD_Count', title: '基数', operate: false},
                        {field: 'Writer', title: '制单人',operate: false},
                        {field: 'HB_WDate', title: '制单时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on('click', ".btn-print", function(){
                var content = table.bootstrapTable('getSelections');
                if(content.length == 0){
                    layer.msg("请选择需要打印的内容");
                    return false;
                }
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"]
                };
                Fast.api.open('chain/lofting/height_bolt/print/ids/'+content[0]["C_Num"],"螺栓汇总选择",options);
            });
        },
        add: function () {
            Controller.api.bindevent("add");
            $(document).on('click', "#chooseTower", function(){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        $("#c-T_Company").val(value.C_Company);
                        $("#c-C_Num").val(value.C_Num);
                        $("#c-T_Num").val(value.T_Num);
                        $("#c-t_project").val(value.t_project);
                        $("#c-TD_ID").val(value.TD_ID);
                        $("#c-TD_TypeName").val(value.TD_TypeName);
                        $("#c-TD_Pressure").val(value.TD_Pressure);
                        $("#c-Customer_Name").val(value.Customer_Name);
                    }
                };
                Fast.api.open('chain/lofting/height_bolt/chooseTower',"选择塔型",options);
            });

            $(document).on('click', "#choosePole", function(){
                var td_id = $("#c-TD_ID").val();
                if(!td_id) layer.msg("请选择塔型！");
                else{
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#c-SCD_ID").val(value.SCD_ID);
                            $("#c-SCD_TPNum").val(value.SCD_TPNum);
                            $("#c-TH_Height").val(value.TH_Height);
                        }
                    };
                    Fast.api.open('chain/lofting/height_bolt/choosePole/td_id/'+td_id,"选择杆塔",options);
                }
            });
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $(document).on('click', "#table_add", function(){
                var col_count = $("#tbshow").find("tr").length+1;
                var field_append='<tr>'
                        +'<td>'+col_count+'</td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-success detail"><i class="fa fa-calendar"></i></a></td>';
                for(var i=0;i<tableField.length;i++){
                    field_append += '<td '+tableField[i][6]+'><input type="'+tableField[i][2]+'" '+tableField[i][4]+' name="table_row['+tableField[i][1]+'][]" value="'+tableField[i][5]+'"></td>';
                }
                field_append += '</tr>';
                $("#tbshow").append(field_append);
            });
            $(document).on('click', ".del", function(e){
                $( e.target ).closest("tr").remove();
                col_index("#tbshow");
                // var indexChoose = $(this).parents('tr').index();
                // var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(3).find('input').val();
                // if(num!=0){
                //     var index = layer.confirm("即将删除该段所有信息，确定删除？", {
                //         btn: ['确定', '取消'],
                //     }, function(data) {
                //         $.ajax({
                //             url: 'chain/lofting/height_bolt/delDetail',
                //             type: 'post',
                //             dataType: 'json',
                //             data: {num: num},
                //             success: function (ret) {
                //                 layer.msg(ret.msg);
                //                 if(ret.code==1){
                //                     $( e.target ).closest("tr").remove();
                //                     col_index("#tbshow");
                //                 }
                //             }, error: function (e) {
                //                 Backend.api.toastr.error(e.message);
                //             }
                //         });
                //         layer.close(index);
                //     })
                // }else{
                //     $( e.target ).closest("tr").remove();
                //     col_index("#tbshow");
                // }


            });
            $(document).on('click', ".detail", function(e){
                var indexChoose = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(3).find('input').val();
                if(num==0){
                    var index = layer.confirm("请先保存段", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        layer.close(index);
                    })
                }else{
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        end:function(){
                            window.location.reload();
                        }
                    };
                    Fast.api.open('chain/lofting/height_bolt/detail/ids/'+num,"螺栓塔型库段明细",options);
                }

            });

            $(document).on('click', "#summary", function(e){
                if(ids){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            window.location.reload();
                        }
                    };
                    Fast.api.open('chain/lofting/height_bolt/summary/ids/'+ids,"单基螺栓汇总",options);

                }else{
                    layer.msg("请刷新后重试！");
                }

            });
            $(document).on('click', "#copyLS", function(e){
                var HB_ID = $("#c-HB_ID").val();
                if(!HB_ID){
                    layer.msg("复制失败，请刷新后重试！");
                    return false;
                }
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["80%","100%"],
                    callback:function(value){
                        if(value.length == 0){
                            layer.msg("复制失败，请刷新后重试！");
                        }else{
                            layer.msg("请稍等，正在复制中");
                            $.ajax({
                                url: 'chain/lofting/height_bolt/copyDetail',
                                type: 'post',
                                dataType: 'json',
                                async: false,
                                data: {ids: ids,sectId: value},
                                success: function (ret) {
                                    layer.msg(ret.msg);
                                    if (ret.hasOwnProperty("code")) {
                                        if (ret.code === 1) {
                                            window.location.reload();
                                        }
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }
                    }
                };
                Fast.api.open('chain/lofting/height_bolt/copyLs/ids/'+HB_ID,"复制杆塔的螺栓明细",options);
            })

            $(document).on('click', ".btn-delete", function(e){
                if(ids!=false){
                    var index = layer.confirm("确定删除？", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: 'chain/lofting/height_bolt/allDel',
                            type: 'post',
                            dataType: 'json',
                            data: {num: ids},
                            success: function (ret) {
                                Layer.msg(ret.msg);
                                if(ret.code==1){
                                    parent.location.reload();
                                }
                                
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }else{
                    layer.msg("删除失败");
                }

            });
        },
        choosetower: function () {
            $(document).on('click', ".btn-close", function(){
                Fast.api.close();
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/height_bolt/chooseTower' + location.search,
                    table: 'taskdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TD_ID',
                sortName: 'TD_ID',
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        //任务单号td.T_Num 产品名称td.P_Name 塔型td.TD_TypeName 工程编号c.PC_Num 合同号c.C_Num 工程名称t.t_project 工程简称t.t_shortproject 客户c.Customer_Name 总基数count 电压等级td.TD_Pressure 总重(kg)td.T_SumWeight
                        {checkbox: true},
                        {field: 'T_Num', title: "任务单号", operate: false},
                        {field: 'td.T_Num', title: "任务单号", operate: 'LIKE',visible: false},
                        {field: 'P_Name', title: "产品名称", operate: false,visible: false},
                        {field: 'TD_TypeName', title: "塔型", operate: false},
                        {field: 'td.TD_TypeName', title: "塔型", operate: 'LIKE',visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'c.PC_Num', title: '工程编号', operate: 'LIKE',visible: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE',visible: false},
                        {field: 't_project', title: '工程名称', operate: false},
                        {field: 't.t_project', title: '工程名称', operate:'LIKE',visible: false},
                        {field: 't_shortproject', title: '工程简称', operate: false,visible: false},
                        {field: 'Customer_Name', title: '客户', operate: false},
                        {field: 'c.Customer_Name', title: '客户', operate: 'LIKE',visible: false},
                        {field: 'count', title: '总基数', operate: false,visible: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'td.TD_Pressure', title: '电压等级', operate: 'LIKE',visible: false},
                        {field: 'T_SumWeight', title: '总重', operate: false,visible: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        choosepole: function () {
            $(document).on('click', ".btn-close", function(){
                Fast.api.close();
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/height_bolt/choosePole/td_id/' + Config.td_id + location.search,
                    table: 'sectconfigdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SCD_ID',
                sortName: 'SCD_ID',
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        //塔型td.TD_TypeName 杆塔号scd.SCD_TPNum 配段信息pd_news 配段基数scd.SCD_Count 任务单号td.T_Num 电压等级td.TD_Pressure
                        {checkbox: true},
                        {field: 'TD_TypeName', title: "塔型", operate: false},
                        {field: 'SCD_TPNum', title: "杆塔号", operate: false},
                        {field: 'scd.SCD_TPNum', title: "杆塔号", operate: 'LIKE', visible: false},
                        {field: 'TH_Height', title: "呼高", operate: false},
                        {field: 'th.TH_Height', title: "呼高", operate: 'LIKE', visible: false},
                        {field: 'pd_news', title: "配段信息", operate: false},
                        {field: 'SCD_Count', title: '配段基数', operate: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 'TD_Pressure', title: '电压等级', operate:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        copyls: function () {
            $(document).on('click', ".btn-close", function(){
                Fast.api.close();
            });
            var table = $("#show");

            // 初始化表格
            table.bootstrapTable({
                data: [],
                columns: [
                    [
                        {checkbox: true},
                        {field: 'BS_ID', title: 'BS_ID', visible: false},
                        {field: 'BS_SectName', title: '段名'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on('click', "#choose", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["80%","100%"],
                    callback:function(value){
                        $("#c-T_Num").val(value.T_Num);
                        $("#c-t_project").val(value.t_project);
                        $("#c-TD_TypeName").val(value.TD_TypeName);
                        $("#c-TD_Pressure").val(value.TD_Pressure);
                        $("#c-TH_Height").val(value.TH_Height);
                        $("#c-SCD_TPNum").val(value.SCD_TPNum);
                        $("#c-HB_ID").val(value.HB_ID);
                        var HB_ID = value.HB_ID;
                        $.ajax({
                            url: 'chain/lofting/height_bolt/copyList',
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            data: {HB_ID:HB_ID},
                            success: function (ret) {
                                var content = [];
                                if(ret.code==1){
                                    content = ret.data;
                                    table.bootstrapTable("load",content);
                                }else{
                                    Layer.msg("请重试");
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }
                };
                Fast.api.open('chain/lofting/height_bolt/chooseScd/ids/'+ids,"选择杆塔",options);
            })
            $(document).on('click', "#surecopy", function(){
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length != 0){
                    var sectId = [];
                    $.each(tableContent,function(index,e){
                        sectId.push(e.BS_ID);
                    })
                    Fast.api.close(sectId);
                }
            });
        },
        choosescd: function () {
            $(document).on('click', ".btn-close", function(){
                Fast.api.close();
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/height_bolt/chooseScd/ids/'+ids + location.search,
                    table: 'heightbolt',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'HB_ID',
                sortName: 'HB_ID',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'T_Company', title: '分公司', operate: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 't.C_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 'SCD_TPNum', title: '杆塔号', operate: 'LIKE'},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 't.T_Num', title: '任务单号', operate: 'LIKE', visible: false},
                        {field: 't_project', title: '工程名', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'scd.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 't_shortproject', title: '工程简称', operate: false},
                        {field: 'T_Sort', title: '产品类别',operate:false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'scd.TD_Pressure', title: '电压等级', operate: 'LIKE', visible: false},
                        {field: 'TH_Height', title: '呼高', operate: false},
                        {field: 'scd.TH_Height', title: '呼高', operate: '=', visible: false},
                        {field: 'SCD_Count', title: '基数', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on('click', "#sure", function(){
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length != 0) Fast.api.close(tableContent[0]);
            });
        },
        detail: function () {
            Controller.api.bindevent("detail");
            $("#detailTable").height(document.body.clientHeight-100);
            var ids = $("#c-ids").val();
            $(document).on('click', "#table_add", function(){
                var col_count = $("#tbshow").find("tr").length+1;
                var BS_SectName = $("#c-BS_SectName").val();
                var field_append='<tr>'
                        +'<td>'+col_count+'</td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                        +'<td>'+BS_SectName+'</td>';
                for(var i=0;i<detailTableField.length;i++){
                    if(detailTableField[i][0]==""){
                        field_append += '<td><input class="small_input" type="text" value=""></td>';
                    }else if(detailTableField[i][1]=="BD_Flag"){
                        field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" '+detailTableField[i][3]+' name="'+detailTableField[i][1]+'[]" value="0"></td>';
                    }else if(detailTableField[i][1]=="BD_sFlag"){
                        field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" '+detailTableField[i][3]+' name="'+detailTableField[i][1]+'[]" value="普通"></td>';
                    }else{
                        field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" '+detailTableField[i][3]+' name="'+detailTableField[i][1]+'[]" value=""></td>';
                    }
                }
                field_append += '</tr>';
                $("#tbshow").append(field_append);
            });

            $(document).on("change","#searchNum", function(e){
                var detai_id = $("#c-detail_id").val();
                var searchNum = $("#searchNum").find("option:selected").val();
                var searchData = {ids: ids, detai_id: detai_id, searchNum:searchNum};
                $.ajax({
                    url: 'chain/lofting/height_bolt/detailTable',
                    type: 'post',
                    dataType: 'json',
                    data: searchData,
                    async: false,
                    success: function (ret) {
                        if (ret.code === 1) {
                            tableContent = ret.data;
                            $("#tbshow").html(tableContent);
                            $("#c-detail_id").val(searchNum);
                            $("#c-BS_SectName").val($("#searchNum").find("option:selected").text());
                        }else layer.msg("请刷新后重试！");

                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });

            var table = $("#rightTable");
            // 初始化表格
            table.bootstrapTable({
                data: {},
                search: false,
                pagination: false,
                height: 300,
                // clickToSelect: true,
                // singleSelect: true,
                columns: [
                    [
                        {checkbox: true,formatter(value,row,index){
                            if(index==0){
                                return {
                                    checked:true
                                };
                            }
                        }},
                        {field: 'BD_MaterialName', title: '材料名称'},
                        {field: 'BD_Limber', title: '等级'},
                        {field: 'BD_Type', title: '规格'},
                        {field: 'BD_Lenth', title: '无扣长'},
                        {field: 'BD_Other', title: '另附规格'},
                        {field: 'BD_PerWeight', title: '比重(kg)'},
                        {field: 'FastInPut', title: '代号'}
                    ]
                ],
                // onPostBody: function ()
                // {
                //     var header = $(".fixed-table-header table thead tr th");
                //     var body = $(".fixed-table-header table tbody tr td");
                //     var footer = $(".fixed-table-header table tr td");
                //     body.each(function(){
                //         header.width($(this).width());
                //         footer.width($(this).width());
                //     })
                // }
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            rightContent();
            $(document).on('click', "#searchMaterial", function(e){
                var BD_MaterialName = $("#BD_MaterialName").val();
                var BD_Limber = $("#BD_Limber").val();
                var BD_Type = $("#BD_Type").val();
                var BD_Other = $("#BD_Other").val();
                var searchData = {BD_MaterialName:BD_MaterialName,BD_Limber:BD_Limber,BD_Type:BD_Type,BD_Other:BD_Other};
                rightContent(searchData);
            });

            function rightContent(searchData={}){
                $.ajax({
                    url: 'chain/lofting/height_bolt/rightTable',
                    type: 'post',
                    dataType: 'json',
                    data: searchData,
                    async: false,
                    success: function (ret) {
                        var tableContent = [];
                        if (ret.code === 1) {
                            tableContent = ret.data;
                        }
                        table.bootstrapTable('load',tableContent);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }

            $(document).on('click', "#echoPrint", function(e){
                var hidden = $("#searchFast").is(":hidden");
                if(hidden){
                    var height = document.body.clientHeight-390;
                    $("#detailTable").height(height);
                    $("#searchFast").show();
                }else{
                    $("#detailTable").height(document.body.clientHeight-100);
                    $("#searchFast").hide();
                }
            });

            $(document).on('keyup', "#daihao", function(e){
                var val = $("#daihao").val();
                table.bootstrapTable("uncheckAll");
                $('#rightTable tbody tr').hide().find("td:eq(7)").filter(":contains('" +(val) + "')").parent("tr").show();
                // $("#rightTable").bootstrapTable("resetView");
                var chooseId = $("#rightTable tbody tr:visible:first").data("index");
                table.bootstrapTable("check",chooseId);
                if(e.keyCode == 13){
                    var BS_SectName = $("#c-BS_SectName").val();
                    var content = table.bootstrapTable("getAllSelections");
                    if(content.length==0){
                        layer.msg("未选中！")
                        return false;
                    }
                    addNewDetail(BS_SectName,content);
                    $('#rightTable tbody tr').show();
                    table.bootstrapTable("uncheckAll");
                    table.bootstrapTable("check",0);
                    $("#daihao").val("");
                }
            });

            $(document).on('keyup', "#rightTable", function(event){
                if (event.keyCode == 13) {
                    var BS_SectName = $("#c-BS_SectName").val();
                    var content = table.bootstrapTable("getAllSelections");
                    if(content.length==0){
                        layer.msg("未选中！")
                        return false;
                    }
                    addNewDetail(BS_SectName,content);
                }
            });

            table.on('dbl-click-row.bs.table',function(row, $element){
                var BS_SectName = $("#c-BS_SectName").val();
                addNewDetail(BS_SectName,[$element]);
            });

            function addNewDetail(num,content)
            {
                var field_append= "";
                var col_count = $("#tbshow").find("tr").length+1;
                $.each(content,function(index,e){
                    field_append += '<tr>'
                        +'<td>'+col_count+'</td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                        +'<td>'+num+'</td>';
                    for(var i=0;i<detailTableField.length;i++){
                        if(detailTableField[i][0]==""){
                            field_append += '<td><input class="small_input" type="text" value=""></td>';
                        }else if(detailTableField[i][1]=="BD_Flag"){
                            field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" data-rule="'+detailTableField[i][3]+'" name="'+detailTableField[i][1]+'[]" value="0"></td>';
                        }else if(detailTableField[i][1]=="BD_sFlag"){
                            field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" data-rule="'+detailTableField[i][3]+'" name="'+detailTableField[i][1]+'[]" value="普通"></td>';
                        }else{
                            field_append += '<td '+detailTableField[i][4]+'><input class="small_input" type="'+detailTableField[i][2]+'" data-rule="'+detailTableField[i][3]+'" name="'+detailTableField[i][1]+'[]" value="'+(e[detailTableField[i][5]]==undefined?detailTableField[i][5]:e[detailTableField[i][5]])+'"></td>';
                        }
                    }
                    field_append += '</tr>';
                });
                $("#tbshow").append(field_append);
            }


            $(document).on('click', ".btn-success", function(e){
                var formData = $("#detail-form").serializeArray();
                var flag= true;
                $.each(formData,function(index,e){
                    if(e.name=="BD_SumCount[]" && e.value==""){
                        layer.msg("数量必须填写");
                        flag = false;
                    }
                });
                if(flag == false) return false;
				layer.msg("保存中，请稍等");
                $.ajax({
                    url: 'chain/lofting/height_bolt/detail/ids/'+$("#c-detail_id").val(),
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData)},
					async: false,
                    success: function (ret) {
                        layer.msg(ret.msg);
                        // if (ret.hasOwnProperty("code")) {
                        //     if (ret.code === 1) {
                        //         window.location.reload();
                        //     }
                        // }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });



            $(document).on('click', ".del", function(e){
                $( e.target ).closest("tr").remove();
                col_index("#tbshow");
                // var indexChoose = $(this).parents('tr').index();
                // var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(3).find('input').val();
                // if(num!=''){
                //     var index = layer.confirm("确定删除？", {
                //         btn: ['确定', '取消'],
                //     }, function(data) {
                //         $.ajax({
                //             url: 'chain/lofting/height_bolt/delDetailContent',
                //             type: 'post',
                //             dataType: 'json',
                //             data: {num: num},
                //             success: function (ret) {
                //                 layer.msg(ret.msg);
                //                 if(ret.code==1){
                //                     $( e.target ).closest("tr").remove();
                //                     col_index("#tbshow");
                //                 }

                //             }, error: function (e) {
                //                 Backend.api.toastr.error(e.message);
                //             }
                //         });
                //         layer.close(index);
                //     })
                // }else{
                //     $( e.target ).closest("tr").remove();
                //     col_index("#tbshow");
                // }


            });


            $('#tbshow').on('keyup','td',function (e) {
                if(e.keyCode ==13){
                    var col = $(this).parents('tr').index();
                    var row = $(this).parents("tr").find("td").index($(this));
                    var allCol = $("#tbshow > tr").length;
                    if((col+1)<=allCol){
                        $("#tbshow").find("tr").eq(col+1).find('td').eq(row).find('input').select();
                    }
                }
            })

            $('#tbshow').on('keyup','td input[name="BD_SumCount[]"]',function () {
                getWeight($(this));
            })
            $('#tbshow').on('keyup','td input[name="BD_SingelWeight[]"]',function () {
                getWeight($(this));
            })
            function getWeight(content)
            {
                var dz = content.parents('tr').find("input[name='BD_SingelWeight[]']").val().trim();
                dz = dz==''?0:parseFloat(dz);
                var number = content.parents('tr').find("input[name='BD_SumCount[]']").val().trim();
                number = number==''?0:parseInt(number);
                var weight=0;
                weight = (dz*number).toFixed(2);
                content.parents('tr').find("input[name='BD_SumWeight[]']").val(weight);
            }

        },
        summary: function (){
            Controller.api.bindevent("summary");
            var height = document.body.clientHeight-225;
            $("#parent_div").height(height+5);
            $(document).on('click', "#summaryDetail", function(e){
                var HB_ID = $("#c-ids").val();
                var LS_Loss = $("#c-LS_Loss").val();
                if(HB_ID == false){
                    layer.confirm('请重试！');
                    return false;
                }
                $("#show").bootstrapTable('destroy');
                $("#loading").show();
                $.ajax({
                    url: 'chain/lofting/height_bolt/summaryDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {HB_ID:HB_ID, LS_Loss:LS_Loss},
                    async: false,
                    complete: function () {
                        HiddenDiv();
                    },
                    success: function (ret) {
                        if (ret.code === 1) {
                            $("#show").bootstrapTable({
                                data: ret.data.content,
                                search: false,
                                pagination: false,
                                height: height,
                                fixedColumns:true,
                                fixedNumber:6,
                                columns: ret.data.field,
                                onClickCell: function(field, value, row, $element) {
                                    if(field=="0-loss" || field=="1-loss"){
                                        $element.attr('contenteditable', true);
                                        $element.blur(function() {
                                            let index = $element.parent().data('index');
                                            let tdValue = $element.html();
    
                                            saveData(index, field, tdValue);
                                        })
                                    }
                                    
                                }
                            });
                            Table.api.bindevent($("#show"));
                            $("#show").bootstrapTable("resetView");
                            multiple_checks();
                        }

                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });

            });
            function saveData(index, field, value) {
                $("#show").bootstrapTable('updateCell', {
                    index: index,
                    field: field,
                    value: value
                });
                return false;
            }
            $(document).on("click", "#export", function () {
                var HB_ID = $("#c-ids").val();
                var LS_Loss = $("#c-LS_Loss").val();
                if(HB_ID == false){
                    layer.confirm('请重试！');
                    return false;
                }
                var content = $("#show").bootstrapTable("getData");
                if(content.length == 0){
                    layer.msg("请先汇总单基螺栓！");
                }
                $.ajax({
                    type: 'POST',
                    url: 'chain/lofting/height_bolt/setcache',
                    data: {value:JSON.stringify(content)},
                    success: function(result){
                        if(result.code == 1){
                            window.open('/admin.php/chain/lofting/height_bolt/export/HB_ID/'+HB_ID+'/LS_Loss/'+LS_Loss+'/key/'+result.data);
                        }
                    }
                });
                // localStorage.setItem("height_bolt_print", json.stringify(content));
                // window.open('/admin.php/chain/lofting/height_bolt/export/HB_ID/'+HB_ID+'/LS_Loss/'+LS_Loss);
            });

            $(document).on("click", "#save", function () {
                var LS_Loss = $("#c-LS_Loss").val();
                var content = $("#show").bootstrapTable("getData");
                if(content.length == 1){
                    layer.msg("先汇总单基螺栓");
                    return false;
                }else{
                    $.ajax({
                        url: 'chain/lofting/height_bolt/summary/ids/'+Config.ids,
                        type: 'post',
                        dataType: 'json',
                        data: {LS_Loss:LS_Loss, content:JSON.stringify(content)},
                        async: false,
                        success: function (ret) {
                            if (ret.code === 1) {
                                layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }
                
            });


            function multiple_checks(table_id='show')
            {
                var mousedown=false;var check = 0;var choose;
                $("body").on("mousedown", "#"+table_id+" tbody tr",function(e){
                    mousedown=true;
                    check = $(this).index();
                    check_element =$("#"+table_id).bootstrapTable("getData")[check];
                    choose = check_element.choose;
                });
                $("body").on("mouseup", "#"+table_id+" tbody tr",function(e){
                    if(mousedown){
                        var mousedown_check = $(this).index();
                        var big = check>mousedown_check?check:mousedown_check;
                        var small = check<mousedown_check?check:mousedown_check;
                        if(big == small){
                            if(choose) $("#"+table_id).bootstrapTable('uncheck', big);
                            else $("#"+table_id).bootstrapTable('check', big);

                        }else{
                            for( var i=small;i<=big;i++){
                                if(choose) $("#"+table_id).bootstrapTable('uncheck', i);
                                else $("#"+table_id).bootstrapTable('check', i);
                            }
                        }
                    }
                    mousedown=false;
                });
            }
        },
        print: function () {

            Controller.api.bindevent("summary");
            var project = $("#project");
            var sect = $("#sect");
            // 初始化表格
            project.bootstrapTable({
                data: printData,
                columns: [
                    [
                        {field: 'C_Num', title: '合同号'},
                        {field: 'T_Company', title: '分公司'},
                        {field: 't_project', title: '工程名'}
                    ]
                ]
            });
            sect.bootstrapTable({
                data: [],
                columns: [
                    [
                        {checkbox: true},
                        {field: 'T_Num', title: '任务单号'},
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'TH_Height', title: '呼高'},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'SCD_Count', title: '基数'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(project);
            Table.api.bindevent(sect);
            project.on('click-row.bs.table',function(row, $element){
                $.ajax({
                    url: 'chain/lofting/height_bolt/printRight',
                    type: 'post',
                    dataType: 'json',
                    data: {C_Num:$element.C_Num},
                    async: false,
                    success: function (ret) {
                        var content = [];
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        sect.bootstrapTable('load',content);

                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
             $("#sum").click(function(){
                let st=sect.bootstrapTable('getSelections');
                if(st.length<1){
                    Fast.api.toastr.error('请先勾选！')
                    return;
                }
                let arr=[];
                st.forEach(val=>arr.push(val['SCD_ID']));
                 window.top.Fast.api.open('chain/lofting/height_bolt/showHzPrint/scds/' + arr.join(','), '螺栓汇总打印', {
                     area: ["100%", "100%"]
                 });



            })
            $("#sumpart").click(function(){
                let st=sect.bootstrapTable('getSelections');
                if(st.length<1){
                    Fast.api.toastr.error('请先勾选！')
                    return;
                }
                let arr=[];
                st.forEach(val=>arr.push(val['SCD_ID']));
                window.top.Fast.api.open('chain/lofting/height_bolt/showHzPrintNoscd/scds/' + arr.join(','), '螺栓汇总不分杆塔', {
                    area: ["100%", "100%"]
                });
            })
        },
        showhzprint:function(){
            let mxdata=Config.mxdata;
			
            hiprint.init();
            let htemp =  new hiprint.PrintTemplate({template: lsdata});

            let zdatamb=[];
            for(let i in mxdata){
                let gts=mxdata[i];
                let v0=gts[0];
                let datamb={
                    gcmc:v0.C_Project,
                    idate:v0.idate,
                    khmc:v0.customer_name,
                    tx:v0.TD_TypeName,
                    th:v0.c_num,
                    hg:v0.th_height,
                    gth:v0.SCD_TPNum,
                    js:v0.scd_count,
                    tj:v0.writer,
                    tb:[],
                    // imemo:v0.imemo
                };
                for(let v of gts){
                    let tmp={
                        mc:v.hbh==1?v.BD_MaterialName:'',
                        dj:v.BD_Limber,
                        gg:v.BD_Type,
                        dw:v.BD_MaterialName.indexOf('垫')>-1?'片':'只',
                        djsl:v.BD_SumCount,
                        shsl:v.BD_LossCount,
                        hjsl:v.BD_SumCount+v.BD_LossCount,
                        hjzl:v.BD_SumWeight,
                        bz:v.BD_Remark,
                        hb:v.hb,
                        addrow:0,
                    }
                    if(v.hbh){
                        mcrows=Math.ceil(tmp['mc'].length/4);
                        rowspancount=1;
                    }else{
                        rowspancount=rowspancount+1;
                    }
                    if(v.hb==1){                        
                        tmp['addrow']=mcrows-rowspancount                        
                    }   
                    datamb.tb.push(tmp);
                }
                zdatamb.push(datamb);
            }

            $('#p_mx').html(htemp.getHtml(zdatamb));
            $("#handleprint").click(function(){
                layer.prompt({
                    formType: 2,
                    value: '',
                    title: '备注',
                    area: ['300px', '120px'] //自定义文本域宽高
                }, function(value, index, elem){
                    // alert(value); //得到value
                    value='备注：'+value
                    for(let v of zdatamb){
                        v['imemo']=value;
                    }
                    layer.close(index);
                    htemp.print(zdatamb);
                });



                // htemp.printByHtml($('#p_mx').html());
                // $('#p_mx').print()
            })
        },
        showhzprintnoscd:function(){
            let mxdata=Config.mxdata;
            hiprint.init();
            let htemp =  new hiprint.PrintTemplate({template: lsdata});

            let zdatamb=[];
            for(let i in mxdata){
                let gts=mxdata[i];
                let v0=gts[0];
                console.log(v0)
                let datamb={
                    gcmc:v0.C_Project,
                    idate:v0.idate,
                    khmc:v0.customer_name,
                    tx:v0.TD_TypeName,
                    th:v0.c_num,
                    hg:v0.th_height,
                    gth:v0.SCD_TPNum,
                    js:Config.js+'基',
                    tj:v0.writer,
                    tb:[],
					// imemo:v0.imemo
                };
                let mcrows=1;
                let rowspancount=1;
                for(let v of gts){
                    let tmp={
                        mc:v.hbh==1?v.BD_MaterialName:'',
                        dj:v.BD_Limber,
                        gg:v.BD_Type,
                        dw:v.BD_MaterialName.indexOf('垫')>-1?'片':'只',
                        djsl:v.BD_SumCount,
                        shsl:v.BD_LossCount,
                        hjsl:parseFloat(v.BD_SumCount)+parseFloat(v.BD_LossCount),
                        hjzl:v.BD_SumWeight,
                        bz:v.BD_Remark,
                        hb:v.hb,
                        addrow:0,
                    }
                    if(v.hbh){
                        mcrows=Math.ceil(tmp['mc'].length/4);
                        rowspancount=1;
                    }else{
                        rowspancount=rowspancount+1;
                    }
                    if(v.hb==1){                        
                        tmp['addrow']=mcrows-rowspancount                        
                    }                    

                    datamb.tb.push(tmp);
                }
                zdatamb.push(datamb);
            }

            $('#p_mx').html(htemp.getHtml(zdatamb));
            $("#handleprint").click(function(){
                layer.prompt({
                    formType: 2,
                    value: '',
                    title: '备注',
                    area: ['300px', '120px'] //自定义文本域宽高
                }, function(value, index, elem){
                    // alert(value); //得到value
                    value='备注：'+value
                    for(let v of zdatamb){
                        v['imemo']=value;
                    }
                    layer.close(index);
                    htemp.print(zdatamb);
                });
                // htemp.print(zdatamb);
                //$('#p_mx').print()
            })
        },
		api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data;
                    }else if(field != "summary"){
                        window.location.reload();
                    }
                    return false;
                });
                $(document).on('click', ".btn-close", function(){
                    Fast.api.close();
                });
            }
        }
    };
    return Controller;
});
