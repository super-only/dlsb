define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree', 'hpbundle', 'lcpdata','scmxdata','zhdata'], function ($, undefined, Backend, Table, Form, undefined, hp, undefined,undefined,undefined) {
    ids = typeof ids == 'undefined' ? "" : ids;
    rightList = typeof rightList == 'undefined' ? [] : rightList;
    groupTableContent = typeof groupTableContent == 'undefined' ? [] : groupTableContent;
    welding = typeof welding == 'undefined' ? [] : welding;
    titleField = typeof titleField == 'undefined' ? [] : titleField;
    result = typeof result == 'undefined' ? [] : result;
    function col_index(div_content = "#tbshow"){
        var col = 0;
        $(div_content).find("tr").each(function () {
            col++;
            $(this).children('td').eq(0).text(col);
        });
    }
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/produce_task/index' + location.search,
                    add_url: 'chain/lofting/produce_task/add',
                    edit_url: 'chain/lofting/produce_task/edit',
                    del_url: 'chain/lofting/produce_task/del',
                    multi_url: 'chain/lofting/produce_task/multi',
                    import_url: 'chain/lofting/produce_task/import',
                    table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PT_Num',
                sortName: 'WriterDate',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                sortOrder: 'DESC',
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'flag', title: '状态', searchList: Config.flag_list,
                        formatter:function(value,row,index){
                            return Config.flag_list[value];
                        }},
                        {field: 'PT_Num', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'PT_Company', title: __('Pt_company'), operate: false},
                        {field: 'T_Num', title: __('T_Num'), operate:false, visible:false},
                        {field: 'd.T_Num', title: __('T_Num'), operate: 'LIKE'},
                        {field: 'C_ProjectName', title: __('C_projectname'), operate: 'LIKE'},
                        {field: 'Customer_Name', title: __('Customer_Name'), operate: false},
                        {field: 'P_Name', title: __('P_Name'), operate: false},
                        {field: 'TD_TypeName', title: __('TD_TypeName'), operate: 'LIKE'},
                        // {field: 'PT_Flag', title: __('Pt_flag'), operate: 'LIKE'},
                        {field: 'PT_Number', title: __('Pt_number'), operate: false},
                        // {field: 'PT_Urgent', title: __('Pt_urgent'),operate: false},
                        {field: 'PT_sumWeight', title: __('Pt_sumweight'), operate:false},
                        {field: 'PT_szMemo', title: __('Pt_szmemo'), operate: false},
                        {field: 'PT_PlanTime', title: __('Pt_plantime'), operate:false},
                        {field: 'Writer', title: __('Writer'), operate: false},
                        {field: 'WriterDate', title: __('Writerdate'),operate: false},
                        {field: 'Auditor', title: __('Auditor'), operate: false},
                        {field: 'AuditorDate', title: __('Auditordate'),operate: false},
                        // {field: 'Approver', title: __('Approver'), operate: false},
                        // {field: 'ApproverDate', title: __('Approverdate'),operate: false},
                        {field: 'PT_Memo', title: __('Pt_memo'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
            var leftTop = $("#leftTop");
                leftTopFun({});
                // var leftbottom = $("#leftBottom");
                // var right = $("#right");
                function leftTopFun(tableData){
                    leftTop.bootstrapTable({
                        data: tableData,
                        search: false,
                        pagination: false,
                        height: 400,
                        columns: [
                            [
                                {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                                {field: 'DtMD_sPartsID', title: '段名'},
                                {field: 'DtMD_sStuff', title: '任务下达数'},
                                {field: 'DtMD_sMaterial', title: '已下总段数(不包括本次)'},
                                {field: 'DtMD_sSpecification', title: '本次下段'},
                                {checkbox: true,title:'试组装'},
                                {field: 'DHS_Height', title: '段数'}
                            ]
                        ]
                    });
                }
        },
        edit: function () {
            var PT_Num = $("#c-PT_Num").val();
            var TD_ID = $("#c-TD_ID").val();
            var right = $("#right");
            $(document).on("click", "#updatePurchase", function () {
                $.ajax({
                    url: 'chain/lofting/produce_task/eipUpload',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {PT_Num: PT_Num},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            // 初始化表格
            right.bootstrapTable({
                data: rightList,
                search: false,
                pagination: false,
                height: 750,
                columns: [
                    [
                        {checkbox:true},
                        {field: 'SCD_ID', title: 'SCD_ID', visible: false},
                        {field: 'SCD_TPNum', title: '段名'}
                    ]
                ]
            });
            Table.api.bindevent(right);
            xddetail();
            right.on('check.bs.table',function(){
                getNum();
            });
            right.on('uncheck.bs.table',function(){
                getNum();
            });
            right.on('check-all.bs.table',function(){
                getNum();
            });
            right.on('uncheck-all.bs.table',function(){
                getNum();
            });

            $(document).on('click', "#setingD", function(e){
                var chooseD = $('#leftTop').bootstrapTable('getAllSelections');
                var arr = [];
                var searchField = "";
                $.each(chooseD,function(index,e){
                    if($.inArray(e.PTS_Name,arr)==-1){
                        arr.push(e.PTS_Name);
                    }
                });
                $.each(arr,function(index,e){
                    searchField += e+',';
                });
                if(searchField.length == 0){
                    $.ajax({
                        url: 'chain/lofting/produce_task/testSectionGroupSave/pt_num/'+PT_Num,
                        type: 'post',
                        dataType: 'json',
                        data: {content: JSON.stringify([])},
                        success: function (ret) {
                            layer.msg(ret.msg);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }else{
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["50%","80%"]
                    };
                    Fast.api.open('chain/lofting/produce_task/testSectionGroup/PT_Num/'+PT_Num+'/ptsName/'+searchField,"试组设置",options);
                }

            });

            $(document).on('click', "#generateProduction", function(e){
                layer.msg("正在生成中，请稍等");
                $.ajax({
                    url: 'chain/lofting/produce_task/generateProduction',
                    type: 'post',
                    dataType: 'json',
                    data: {TD_ID: TD_ID, PT_Num: PT_Num},
                    async: false,
                    success: function (ret) {
                        if(ret.code>=1){
                            var options = {
                                shadeClose: false,
                                shade: [0.3, '#393D49'],
                                area: ["100%","100%"],
                                end:function(){
                                    window.location.reload();
                                }
                            };
                            Fast.api.open('chain/lofting/produce_task/viewProduction/ids/'+PT_Num,"查看生产明细",options);
                            // if(ret.code==1){
                            //     Fast.api.ajax({
                            //         url: "chain/lofting/produce_task/savepdf/PT_Num/"+PT_Num,
                            //         type: "post"
                            //     });
                            // }
                        }else{
                            layer.msg(ret.msg);
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });

            });

            $(document).on('click', "#viewProduction", function(e){
                var index = layer.confirm('请确保已生成最新生产明细', {
                    btn: ['确定', '取消'],
                }, function(data) {
                    layer.close(index);
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                    };
                    Fast.api.open('chain/lofting/produce_task/viewProduction/ids/'+PT_Num,"查看生产明细",options);
                })

            });

            $(document).on('click', "#weldingDetail", function(e){
                layer.msg("正在生成中，请稍等");
                $.ajax({
                    url: 'chain/lofting/produce_task/weldingDetail',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {TD_ID: TD_ID, PT_Num: PT_Num},
                    success: function (ret) {
                        if(ret.code==1){
                            var options = {
                                shadeClose: false,
                                shade: [0.3, '#393D49'],
                                area: ["100%","100%"]
                            };
                            Fast.api.open('chain/lofting/produce_task/viewWelding/ids/'+PT_Num,"查看电焊明细",options);
                        }else{
                            layer.msg(ret.msg);
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });

            });

            $(document).on('click', "#viewWelding", function(e){
                var index = layer.confirm('请确保已生成最新电焊明细', {
                    btn: ['确定', '取消'],
                }, function(data) {
                    layer.close(index);
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                    };
                    Fast.api.open('chain/lofting/produce_task/viewWelding/ids/'+PT_Num,"查看电焊明细",options);
                })

            });

            $(document).on('click', "#joinView", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        layer.msg("正在保存中，请稍等");
                        $.ajax({
                            url: 'chain/lofting/produce_task/planeRelease',
                            type: 'post',
                            dataType: 'json',
                            data: {pt_num:PT_Num,data: JSON.stringify(value)},
                            async: false,
                            success: function (ret) {
                                layer.msg(ret.msg);
                                if(ret.code == 1){
                                    window.location.reload();
                                }

                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }

                };
                Fast.api.open('chain/lofting/produce_task/joinView/ids/'+PT_Num,"塔型段下达二维平面",options);
            });


            function getNum(){
                var choose = right.bootstrapTable('getSelections');
                var arr = [];
                $.each(choose,function(index,e){
                    arr.push(e.SCD_ID);
                });
                xddetail(arr);
                // console.log(choose);
            }

            function xddetail(SCD_ID=[]){
                $.ajax({
                    url: 'chain/lofting/produce_task/taskReleaseDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {TD_ID: TD_ID, PT_Num: PT_Num, SCD_ID: SCD_ID},
                    success: function (ret) {
                        var tableHtml = [];
                        if(ret.code==1) tableHtml = ret.data;
                        $("#leftTop").bootstrapTable('destroy');
                        $("#leftTop").bootstrapTable({
                            data: tableHtml,
                            search: false,
                            pagination: false,
                            height: 600,
                            columns: [
                                [
                                    {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                                    {field: 'TS_Name', title: '段名'},
                                    {field: 'all_Count', title: '任务下达数'},
                                    {field: 'is_Count', title: '已下总段数(不包括本次)'},
                                    {field: 'PTS_Count', title: '本次下段'},
                                    // {field: 'PTS_IFShi', title:'试组装', formatter: Table.api.formatter.toggle},
                                    {checkbox: true, field: 'PTS_IFShi', formatter(value,row,index){
                                        if(value == 1){
                                            return {
                                                checked: true
                                            }
                                        }
                                    }},
                                    {field: 'DHS_Height', title: '段数'}
                                ]
                            ]
                        });
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            $(document).on("click", "#author", function () {
                var num = $("#c-PT_Num").val();
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                $.ajax({
                    url: 'chain/lofting/produce_task/generateProduction',
                    type: 'post',
                    dataType: 'json',
                    data: {TD_ID: TD_ID, PT_Num: PT_Num},
                    // success: function (ret) {
                    //     if(ret.code==1){
                    //         Fast.api.ajax({
                    //             url: "chain/lofting/produce_task/savepdf/PT_Num/"+PT_Num,
                    //             type: "post"
                    //         });
                    //     }
                    // }
                    
                });
                $.ajax({
                    url: 'chain/lofting/produce_task/weldingDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {TD_ID: TD_ID, PT_Num: PT_Num}
                });
                check('审核之后无法修改，确定审核？',"chain/lofting/produce_task/auditor",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = $("#c-PT_Num").val();
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('确定弃审？',"chain/lofting/produce_task/giveUp",num);

            });
            function check(msg,url,num){
                // console.log(url,msg,num);return false;
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                window.location.reload();
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
            Controller.api.bindevent("edit");
        },
        showzhprint:function(){
            let mxdata=Config.mxdata;
            let ptask=Config.ptask;
            let zssum=Config.zssum;
            let zlsum=Config.zlsum;
            // console.log(mxdata);
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: zhdata});
            let zdatamb={
                PT_DHmemo:ptask.PT_DHmemo,
                t_num:ptask.t_num,
                C_ProjectName:ptask.C_ProjectName,
                PT_Num:ptask.PT_Num,
                TD_TypeName:ptask.TD_TypeName,
                dgy:ptask.PT_GYMemo?'打钢印 '+ptask.PT_GYMemo:(ptask.PT_GYMemo?'打钢印 '+ptask.PT_GYMemo:''),
                'dydj':'电压等级 '+ptask.TD_Pressure,
                zssum:zssum,
                zlsum:zlsum,
                idate:ptask.idate,
                bz:ptask.Writer
            };
            let tb=[];
            for(let v of mxdata){
                let tmp={
                    // zjh:v.DCD_PartNum,
                    zjh:v.hbh==1?v.DCD_PartNum:'',
                    // zjh:v.hb==1?v.DCD_PartNum:'',
                    zs:v.hbh==1?v.DCD_Count:'',
                    ljh:v.DtMD_sPartsID,
                    cz:v.DtMD_sMaterial,
                    gg:v.DtMD_sSpecification,
                    ck:v.DHS_Thick+(v.DHS_Height?'*'+v.DHS_Height:''),
                    dj:v.DHS_Count,
                    jg:v.DHS_Count*v.DCD_Count,
                    // zzl:v.hbh==1?(v.DCD_SWeight*v.DCD_Count).toFixed(2):'',
                    zzl:v.hbh==1?(v.SumWeight).toFixed(2):'',
                    bz:v.DtMD_sRemark,
                    hb:v.hb,
                    // txm:v.bcdcode,
                };
                tb.push(tmp);
            }
            zdatamb['tb']=tb;
            $('#p_mx').html(htemp.getHtml(zdatamb));
            // for(let v of mxdata){
            //     JsBarcode("#"+v.bcdcode, v.bcdcode,{
            //         // width:1,
            //         // height:40,
            //         displayValue: false
            //     });
            // }
            $("#handleprint").click(function(){
                htemp.print(zdatamb);
                // htemp.printByHtml($('#p_mx').html());
                // $('#p_mx').print()
            })

        },
        showjgprint:function(){
            // let loadindx = Layer.load(0);
            let jindu=0;
            let jd=0;
            let ispdf=Config.ispdf;
            if(ispdf==1){
                $("#tabheader > li:nth-child(5)").hide();
                $("#tabheader > li:nth-child(4)").hide();
                $("#tabheader > li:nth-child(3)").hide();
                $('div.progress').hide();
            }else{
                $("#tabheader > li:nth-child(8)").hide();
                $("#tabheader > li:nth-child(7)").hide();
                $("#tabheader > li:nth-child(6)").hide();
            }
            
            // let layerindex=layer.open({
            //     title: '加载进度',
            //     type: 1,
            //     area: ['500px', '100px'],
            //     content: `<div  class="progress" style="margin:10px;">
            //     <div id="avatar-progress" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40"
            //     aria-valuemin="0" aria-valuemax="100" style="width:0%">
            //       <span id="avajd">0%</span> 
            //     </div>
            //   </div>             
            //    `
            // });
            hiprint.init();
            // let worker = new Worker("/assets/js/backend/chain/lofting/work.js");          
            // worker.onmessage = function (event) {
            //     console.log(event);
            // };
            // window.top.addEventListener("message", function (ev) {                
            //     if (Array.isArray(ev.data) && ev.data[0]==='hpres') {   
            //         console.log(new Date().toLocaleString(),'msg')               
            //         let [,qry,key,ht]=ev.data
            //         $(qry).html(ht);
            //         pageto(qry,0);
            //     }              
            // });
                                    
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: zdata});
            let htemp2 =  new hiprint.PrintTemplate({template: zdata2});
            let htempmx1 =  new hiprint.PrintTemplate({template: mx1data});

            let htempmx2 =  new hiprint.PrintTemplate({template: mx2data});
         
            let htempmx3 =  new hiprint.PrintTemplate({template: mx3data});
            let htempmx4 =  new hiprint.PrintTemplate({template: mx4data});

            //获取表头数据
            let ptask=Config.ptask;
            let dhzarr=Config.dhzarr; //呼高段数汇总
            let dbtjstr=Config.dbtjstr; //段别统计
            let szzdltjstr=Config.szzdltjstr; //试组装段落统计
            let szzdlzlstr=Config.szzdlzlstr; //试组装段落重量
            let mxdata1=Config.mxdata1;
            let mxdata2=Config.mxdata2;
            let mxdata3=Config.mxdata2;
            let mxdata4=Config.mxdata3;
            let zssum=Config.zssum;
            let zlsum=Config.zlsum;
			let DtMD_sType=mxdata1[0]['DtMD_sType']!=0?mxdata1[0]['DtMD_sType']:'';
           
            // console.log('dbtj',dbtj);
            //显示html数据
            //封面
            let zdatamb={
                PT_Num:ptask.PT_Num,
                customer_name:ptask.customer_name,
                C_ProjectName:ptask.C_ProjectName,
                TD_TypeName:ptask.TD_TypeName,
                PT_DHmemo:ptask.PT_DHmemo,
                PT_Memo:ptask.PT_Memo,
                dzm:ptask.PT_GYMemo,
                PT_sumWeight:ptask.PT_sumWeight+'Kg',
                tb:[],
                dbtj:dbtjstr,
                dltj:szzdltjstr,
                dlzl:szzdlzlstr,
                Writer:ptask.Writer,
                Auditor:ptask.Auditor,
                idate:ptask.idate,
				DtMD_sType:DtMD_sType
            };
            // dhzarr=dhzarr.concat(dhzarr);
            // dhzarr=dhzarr.concat(dhzarr);
            // dhzarr=dhzarr.concat(dhzarr);
            for(let i in dhzarr){
                // if(i>4){
                //     break;
                // }
                zdatamb['tb'].push({
                    hz:dhzarr[i]
                });
            }
			zdatamb['tb'].push({
                hz:"<div style='display: flex;flex-direction: column;padding:10px;'>" +
                    "<div style='display:flex;flex-direction:row;'><div>备注：</div><div></div></div>" +
                    "<div style='display:flex;flex-direction:row;'><div style='width: 5em'>·总重：</div><div>"+zdatamb['PT_sumWeight']+"</div></div>" +
                    "<div style='display:flex;flex-direction:row;'><div style='width: 7em'>·段别统计：</div><div>"+dbtjstr+"</div></div>" +
                    "<div style='display:flex;flex-direction:row;'><div style='width: 10em'>·试组装段落统计：</div><div>"+szzdltjstr+"</div></div>" +
                    "<div style='display:flex;flex-direction:row;'><div style='width:25em;'>·试组装段落重量：</div><div>"+szzdlzlstr+"</div></div>" +
                    "</div>"
            });
            $('#p_fm').html(htemp.getHtml(zdatamb));
            jindu=jindu+Math.round(Math.random()*10);           
            $("#avajd").html(jindu + "%");
            $("#avatar-progress").css("width", jindu + "%");
            // if(jindu>=100){                
            //     Layer.closeAll();
            // }
           
           
            //呼高段数汇总页面
            let zdatamb2={
                PT_Num:ptask.PT_Num,
                customer_name:ptask.customer_name,
                C_ProjectName:ptask.C_ProjectName,
                tb:[],
            };
            for(let i in dhzarr){
                zdatamb2['tb'].push({
                    hz:dhzarr[i]
                });
            }
            $('#p_fm2').html(htemp2.getHtml(zdatamb2));
            jindu=jindu+Math.round(Math.random()*10);           
            $("#avajd").html(jindu + "%");
            $("#avatar-progress").css("width", jindu + "%");
            // if(jindu>=100){
            //     Layer.closeAll();
            // }
            let pg_mx= {
                "#p_mx1": 1,
                "#p_mx2": 1,
                "#p_mx3": 1,
                "#p_mx4": 1
            }; //明细，当前页

            //翻页
            window.pageto=function(id,num){
                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };

            //原材料汇总 处理数值
            function pval1(v,key){
                let val=v[key];
                val = parseFloat(val);
                return val?parseFloat(val).toFixed(1):'';
            }
            //明细1
            let mx1datamb={
                TD_TypeName:ptask.TD_TypeName,
                C_ProjectName:ptask.C_ProjectName,
                gyh:ptask.PT_GYMemo,
                DtMD_sType:DtMD_sType,
                PT_DHmemo:ptask.PT_DHmemo
            };
            let tb=[];
            // console.log('mx1datamb', mx1datamb);
            for(let v of mxdata1){
                // console.log(v);
                let lszl=v['zl']-v['dh'];
                if(lszl){
                    lszl=lszl.toFixed(1);
                    if(lszl==0){
                        lszl='';
                    }
                }else{
                    lszl='';
                }
                let tmp={
                    'gg':v['gg'],
                    'cz':v['cz'],
                    'zl':pval1(v,'zl'),
                    'lszl':lszl,//((v['zl']-v['dh']) && (v['zl']-v['dh'])>0.1)?(v['zl']-v['dh']).toFixed(1):'',
                    'dhzl':pval1(v,'dh'),
                    'zjks':v['zjks']!=null?v['zjks']:'',
                    'hq':pval1(v,'hq'),
                    'qj':pval1(v,'qj'),
                    'cb':pval1(v,'cb'),
                    'khj':pval1(v,'khj'),
                    'yb':pval1(v,'yb'),
                    'qg':pval1(v,'qg'),
                    'gh':pval1(v,'gh'),
                    'zjzl':v['zjzl']!=null?v['zjzl']:''
                };
                tb.push(tmp);
            }
            mx1datamb['tb']=tb;
          
            $('#p_mx1').hide();
            $('#p_mx1').html(htempmx1.getHtml(mx1datamb));
            pageto('#p_mx1',0);
            
         
            jindu=jindu+Math.round(Math.random()*20);           
            $("#avajd").html(jindu + "%");
            $("#avatar-progress").css("width", jindu + "%");
            // if(jindu>=100){
            //     Layer.closeAll();
            // }
          
            let sinv=setInterval(() => {                
                jindu=jindu+Math.round(Math.random()*3);           
                if(jd>=3){
                    jindu=100;
                }else{
                    if(jindu>90){
                        jindu=90
                    }
                }
                if(jindu>100){
                    jindu=100;
                    clearInterval(sinv);
                }
                $("#avajd").html(jindu + "%");
                $("#avatar-progress").css("width", jindu + "%");                
            }, 1000);
          
            // $.ajax({
            //     url:'http://'+location.hostname+':3000/hpesec',
            //     type:'post',                
            //     headers: {'Content-Type': 'application/json'},
            //     data:JSON.stringify({itype:'mx1data',idata:mx1datamb}),                
            //     success:function(ht)    {                    
            //         $('#p_mx1').html(ht);                    
            //         pageto('#p_mx1',0);
            //     },
            //     error:function(xhr,status,error){
            //         console.log(xhr,status,error)
            //     }
            // })

            // let mx1page=$('#p_mx1 .hiprint-printPanel .hiprint-printPaper');
            // for(let v of mx1page){
            //     $(v).hide();
            // }
            // $('#p_mx1').show();
            // $(mx1page[0]).show();

            //明细2
            let mx2datamb={
                TD_TypeName:ptask.TD_TypeName,
                C_ProjectName:ptask.C_ProjectName,
                idate:ptask.idate,
                zbr:ptask.Writer,
                shr:ptask.Auditor,
                dzm:ptask.PT_GYMemo,
                hj:"总数量："+zssum+' 件，总重量：'+Math.round(zlsum)+' kg',
                DtMD_sType:DtMD_sType,
                PT_DHmemo:ptask.PT_DHmemo
                // ywy:,
                // jbr:,
            };
            let tb2=[];
			
            for(let v of mxdata2){
                // console.log(v);
                let tmp={
                    'xh':v['xh'],
                    'bjbh':v['bjbh'],
                    'cz':v['cz'],
                    'gg':v['gg'],
                    'cd':v['cd'],
                    'kd':Number(v['kd']) || '',
                    'DtMD_iTorch': v['DtMD_iTorch'],
                    'type': v['type'],
                    'sl':v['sl'],
                    'pl':'',
                    'bz':v['gy']+(v['bz']?v['bz']:''),
                    'zl':v['zl']?parseFloat(v['zl']).toFixed(1):'',
                    'hb':v['hb'],
                    'zjks':v['zjks'],
                    'gy':'制弯、切角、开合角'
                };
                tb2.push(tmp);                      
            }
			
            mx2datamb['tb']=tb2;
            $('#p_mx2').hide();
            // console.log(new Date().toLocaleString(),0)
            // $.ajax({
            //     url:'http://'+location.hostname+':3000/hpesec',
            //     type:'post',                
            //     headers: {'Content-Type': 'application/json'},
            //     data:JSON.stringify({itype:'mx2data',idata:mx2datamb}),                
            //     success:function(ht)    {       
            //         jd++;             
            //         $('#p_mx2').html(ht);                    
            //         pageto('#p_mx2',0);                    
            //         if(jindu<100){
            //             jindu=jindu+Math.round((100-jindu)/3);           
            //           }      
            //         if(jindu>100){
            //             jindu=100;
            //         }
            //         $("#avajd").html(jindu + "%");
            //         $("#avatar-progress").css("width", jindu + "%");
            //         // if(jindu>=100){
            //         //     Layer.closeAll();
            //         // }
            //     },
            //     error:function(xhr,status,error){
            //         console.log(xhr,status,error)
            //     }
            // })
            if(ispdf!=1){
                $.ajax({
                    // url: 'chain/lofting/produce_task/hpserver',
                    url:'http://'+location.hostname+':3000/hpesec',
                    type:'post',                
                    headers: {'Content-Type': 'application/json'},
                    data:JSON.stringify({itype:'mx2data',idata:mx2datamb}),                
                    success:function(ht)    {       
                        jd++;             
                        $('#p_mx2').html(ht);                    
                        pageto('#p_mx2',0);                    
                        if(jindu<100){
                            jindu=jindu+Math.round((100-jindu)/3);           
                        }      
                        if(jindu>100){
                            jindu=100;
                        }
                        $("#avajd").html(jindu + "%");
                        $("#avatar-progress").css("width", jindu + "%");

                    },
                    error:function(xhr,status,error){
                        console.log(xhr,status,error)                    
                        $('#p_mx2').html(htempmx2.getHtml(mx2datamb));
                        pageto('#p_mx2',0);
                        jd++;             
                        $('#p_mx2').html(ht);                    
                        pageto('#p_mx2',0);                    
                        if(jindu<100){
                            jindu=jindu+Math.round((100-jindu)/3);           
                        }      
                        if(jindu>100){
                            jindu=100;
                        }
                        $("#avajd").html(jindu + "%");
                        $("#avatar-progress").css("width", jindu + "%");
                    }
                })
            }
            // Fast.api.ajax({
            //     url: 'chain/lofting/produce_task/hpserver',
            //     contentType: 'application/json',
            //     data: JSON.stringify({itype:'mx2data',idata:mx2datamb}),                 
            // }, function (_, ret) {
            //         let ht=ret.data;
            //         jd++;             
            //         $('#p_mx2').html(ht);                    
            //         pageto('#p_mx2',0);                    
            //         if(jindu<100){
            //             jindu=jindu+Math.round((100-jindu)/3);           
            //           }      
            //         if(jindu>100){
            //             jindu=100;
            //         }
            //         $("#avajd").html(jindu + "%");
            //         $("#avatar-progress").css("width", jindu + "%");                           
            // }, function (_, ret) {
            //     console.log('err', arguments)
            // })
          
			// console.log(new Date().toLocaleString(),0)
            // worker.postMessage(mx2datamb);                    
            // window.top.postMessage(['hp','#p_mx2','mx2data',mx2datamb], "*");
            // console.log(mx2datamb);
            // return;
			// let ht=htempmx2.getHtml(mx2datamb)
			// console.log(new Date().toLocaleString(),1)
           
            // $('#p_mx2').html(ht);
			// console.log(new Date().toLocaleString(),2)
            // pageto('#p_mx2',0);
           

            // return;
          
            //明细3
            //处理数值
            function pval3(v,key){
                let val=v[key];
                return val?parseFloat(val).toFixed(['合计','小计'].indexOf(v['gg'])>-1?0:1):'';
            }
            let mx3datamb={
                TD_TypeName:ptask.TD_TypeName,
                C_ProjectName:ptask.C_ProjectName,
                idate:ptask.idate,
                zbr:ptask.Writer,
                shr:ptask.Auditor,
                dzm:ptask.PT_GYMemo,
                hj:"总数量："+zssum+' 件，总重量：'+Math.round(zlsum)+' kg',
                DtMD_sType:DtMD_sType,
                PT_DHmemo:ptask.PT_DHmemo
                // ywy:,
                // jbr:,
            };
            let tb3=[];
            //v['gg'] == '小计' || v['gg'] == '合计'
            for(let v of mxdata3){
                // console.log(v);
                let tmp={
                    'xh':v['xh'],
                    'bjbh':v['bjbh'],
                    'cz':v['cz'],
                    'gg':v['gg'],
                    'cd':v['cd'],
                    'kd':Number(v['kd']) || '',
                    'sl':v['sl'],
                    'DtMD_iTorch': v['DtMD_iTorch'],
                    'type': v['type'],
                    'pl':'',
                    'bz':v['bz']?v['bz']:'',
                    'zl':pval3(v,'zl'),
                    'hb':v['hb'],
                    'zjks':v['zjks'],
                    'gy':v['gy']
                };
                tb3.push(tmp);
            }
            mx3datamb['tb']=tb3;
            $('#p_mx3').hide();
            // $('#p_mx3').html(htempmx3.getHtml(mx3datamb));
            // pageto('#p_mx3',0);
            // window.top.postMessage(['hp','#p_mx3','mx3data',mx3datamb], "*");
            if(ispdf!=1){
                $.ajax({
                    // url: 'chain/lofting/produce_task/hpserver',
                    url:'http://'+location.hostname+':3000/hpesec',
                    type:'post',                
                    headers: {'Content-Type': 'application/json'},
                    data:JSON.stringify({itype:'mx3data',idata:mx3datamb}),                
                    success:function(ht)    {            
                        jd++;        
                        $('#p_mx3').html(ht);                    
                        pageto('#p_mx3',0);
                        if(jindu<100){
                            jindu=jindu+Math.round((100-jindu)/3);            
                        }         
                        if(jindu>100){
                            jindu=100;
                        }        
                        $("#avajd").html(jindu + "%");
                        $("#avatar-progress").css("width", jindu + "%");

                    },
                    error:function(xhr,status,error){
                        console.log(xhr,status,error)
                        $('#p_mx3').html(htempmx3.getHtml(mx3datamb));
                        pageto('#p_mx3',0);
                        jd++;        
                        $('#p_mx3').html(ht);                    
                        pageto('#p_mx3',0);
                        if(jindu<100){
                            jindu=jindu+Math.round((100-jindu)/3);            
                        }         
                        if(jindu>100){
                            jindu=100;
                        }        
                        $("#avajd").html(jindu + "%");
                        $("#avatar-progress").css("width", jindu + "%");
                    }
                })
            }
           

            //明细4
            let mx4datamb={
                tx:ptask.TD_TypeName,
                dw:'',
                
                rq:ptask.idate,
                zb:ptask.Writer,
                sh:ptask.Auditor,
                // ljzsl:mainInfos['number'],
                // ljzzl:mainInfos['weight'],
                // ljzks:mainInfos['knumber'],
            };
            let tb4=[];
    
            for(let d of mxdata4){
                // console.log(v);
                let lszl=d['zl']-d['dh'];
                if(lszl){
                    lszl=lszl.toFixed(1);
                    if(lszl==0){
                        lszl='';
                    }
                }else{
                    lszl='';
                }
                let tmp={
                    'ljbh':d['bjbh'], //零件编号
                    'cz':d['cz']=="Q235B"?"":d['cz'],//材质
                    'gg':d['gg'],//规格
                    'cd':Number(d['cd']) || '',//长度
                    'kd':Number(d['kd']) || '',//宽度
                    'DtMD_iTorch': Number(d['DtMD_iTorch']) || '',
                    'djjs':d['DD_Count'],//单段件数
                    'zsl':d['sl'],//总数量
                    'djzl':d['zl'],//重量（kg）
                    'ks':d['zjks'],//单件孔数
                    'dh': Number(d['DtMD_iWelding']) || '',//电焊
                    'wq': Number(d['DtMD_iFireBending']) || '',//弯曲
                    'qj': Number(d['DtMD_iCuttingAngle']) || '',//切角
                    'cb': Number(d['DtMD_fBackOff']) || '',//铲背
                    'qg': Number(d['DtMD_iBackGouging']) || '',//清根
                    'db': Number(d['DtMD_DaBian']) || '',//打扁
                    'khj':Number(d['DtMD_KaiHeJiao']) || '',//开合角
                    'zk': Number(d['DtMD_ZuanKong']) || '',//钻孔
                    'gh': Number(d['DtMD_GeHuo']) || '',
                    'bz':d['bz'],//备注
                };
                tb4.push(tmp);
            }
            mx4datamb['tb']=tb4;

            $('#p_mx4').hide();
            
            // $('#p_mx4').html(htempmx4.getHtml(mx4datamb));
            // pageto('#p_mx4',0);
            // window.top.postMessage(['hp','#p_mx4','mx4data',mx4datamb], "*");

            if(ispdf!=1){
                $.ajax({
                    // url: 'chain/lofting/produce_task/hpserver',
                    url:'http://'+location.hostname+':3000/hpesec',
                    type:'post',                
                    headers: {'Content-Type': 'application/json'},
                    data:JSON.stringify({itype:'mx4data',idata:mx4datamb}),                
                    success:function(ht)    {         
                        jd++;           
                        $('#p_mx4').html(ht);                    
                        pageto('#p_mx4',0);
                        if(jindu<100){
                            jindu=jindu+Math.round((100-jindu)/3);            
                        }
                        if(jindu>100){
                            jindu=100;
                        }       
                        $("#avajd").html(jindu + "%");
                        $("#avatar-progress").css("width", jindu + "%");
                    },
                    error:function(xhr,status,error){
                        console.log(xhr,status,error)
                        $('#p_mx4').html(htempmx4.getHtml(mx4datamb));
                        pageto('#p_mx4',0);
                        jd++;           
                        $('#p_mx4').html(ht);                    
                        pageto('#p_mx4',0);
                        if(jindu<100){
                            jindu=jindu+Math.round((100-jindu)/3);            
                        }
                        if(jindu>100){
                            jindu=100;
                        }       
                        $("#avajd").html(jindu + "%");
                        $("#avatar-progress").css("width", jindu + "%");
                    }
                })
            }

            $("#handleprintfm").click(function(){
                htemp.print(zdatamb);
            });

            $("#handlewordfm").click(function(){
                // $('#p_fm').html($('#p_fm').html().replaceAll('pt', 'px'))
                // console.log($('#p_fm').html());
                // $("#p_fm").wordExport("加工明细表封面-"+ptask.PT_Num);
                // console.log(zdatamb)
                //
                // htemp.toPdf(zdatamb,'测试导出1')
                // console.log(1)

                // let data=encodeURI(JSON.stringify({"data":zdatamb}));
                // Fast.api.ajax({
                //     url:'chain/lofting/produce_task/printlck',
                //     contentType: 'application/json',
                //     data:JSON.stringify({js:data,pt:ptask.PT_Num,itype:'doc',fr:'Fm'}),
                // },function(_,ret){
                //     window.open(ret.url);
                //     console.log('ok',arguments)
                // },function(_,ret){
                //     console.log('err',arguments)
                // })

            });

            $("#handleprintfm2").click(function(){
                htemp2.print(zdatamb2);
            });

            $("#handleprintmx1").click(function(){
                htempmx1.print(mx1datamb);
            });

            $("#handleprintmx2").click(function(){
                htempmx2.print(mx2datamb);
            });

            $("#handleprintmx3").click(function(){
                htempmx3.print(mx3datamb);
            });

            $("#handleprintmx4").click(function(){
                htempmx4.print(mx4datamb);
            });


          
            if(ispdf==1){
                Fast.api.ajax({
                    url: 'chain/lofting/produce_task/getpdf/PT_Num/' + ptask.PT_Num+'/ifg/jg',                      
                }, function (_, ret) {   
                    // console.log(ret);   
                    $('#mx21 embed').attr('src',ret.data['mx2datamb']);
                    $('#mx31 embed').attr('src',ret.data['mx3datamb']);
                    $('#mx41 embed').attr('src',ret.data['mx4datamb']);               
    
                    // $("#mx2datambpdf").click(function(){
                    //     window.top.Fast.api.open(ret.data['mx2datamb'], '加工明细表1', {
                    //         area: ["100%", "100%"]
                    //     });
                    // });
                    // $("#mx3datambpdf").click(function(){
                    //     window.top.Fast.api.open(ret.data['mx3datamb'], '加工明细表2', {
                    //         area: ["100%", "100%"]
                    //     });
                    // });
                    // $("#mx4datambpdf").click(function(){
                    //     window.top.Fast.api.open(ret.data['mx4datamb'], '放样原始材料表', {
                    //         area: ["100%", "100%"]
                    //     });
                    // });
                }, function (_, ret) {
                    console.log('err', arguments)
                });
            }
           
          

           
            
            


            // console.log($('#mx1 div.well.btn-bar'))
            // console.log($('div.well.btn-bar').height())
            // console.log($('body').height(),$('ul.nav.nav-tabs').height(),$('div.well.btn-bar').height(),$('body').height()-(15+$('ul.nav.nav-tabs').height()+6+$('#mx1 div.well.btn-bar').height()+6+5+15+65));
            $(".lcprint .well").height($('body').height()-(15+$('ul.nav.nav-tabs').height()+6+$('div.well.btn-bar').height()+6+5+15+45));
        },
        showlckprint: function () {
            // console.log(Config.treedata);
            let treedata=Config.treedata;
            let printdata=Config.printdata;
            hiprint.init();
            //获取模板
            // function getpt(elearr=[]){
            //     let newdata=$.extend(true,{}, pdata);
            //     //受控
            //     for(let v of elearr){
            //         newdata.panels[0].printElements.push(...eleadd[v]);
            //     }
            //     return new hiprint.PrintTemplate({template: newdata});
            // }

            // let htemp = new hiprint.PrintTemplate({template: pdata});
            // let htemp =  getpt(['sk','sz','dh','qj','hq']);

            let htemp =  new hiprint.PrintTemplate({template: pdata});
            // let htemp =  new hiprint.PrintTemplate({
            //     template:{"panels":[{"index":0,"paperType":"A5","height":210,"width":148,"paperHeader":43.5,"paperFooter":595.2755905511812,"printElements":[{"options":{"left":99,"top":148.5,"height":9.75,"width":120,"title":"sdsdfsdf"},"printElementType":{"type":"text"}}],"paperNumberLeft":389,"paperNumberTop":573,"rotate":true}]}
            // });

            let ptask=Config.ptask;
            // htemp.addPrintPanel({ paperType:'A5',rotate:true });

            //生成数据
            let pagecount=Config.pagecount
            function getPdata(vo,pgcount=pagecount){
                // console.log(vo)
                let pd={
                    PT_DHmemo:ptask.PT_DHmemo,
                    PT_Num:ptask.PT_Num,
                    dh:vo.data.dh,
                    C_ProjectName:vo.data.C_ProjectName,
                    TD_TypeName:vo.data.TD_TypeName,
                    PTD_Count:vo.data.PTD_Count,
                    dzm:'打钢印:'+(ptask.PT_GYMemo?ptask.PT_GYMemo:''),
                    js:vo.data.js,
                    DtMD_sPartsID:vo.data.DtMD_sPartsID,
                    cl:vo.data.cl,
                    cz:vo.data.cz,
                    gg:vo.data.gg,
                    DtMD_iLength:vo.data.DtMD_iLength,
                    
					DtMD_iTorch:parseFloat(vo.data.DtMD_iTorch)?vo.data.DtMD_iTorch:'',
                    DtMD_fWidth:parseFloat(vo.data.DtMD_fWidth)?vo.data.DtMD_fWidth:'',
					
                    DtMD_iUnitHoleCount:vo.data.DtMD_iUnitHoleCount,
                    zks:(vo.data.PTD_Count*vo.data.DtMD_iUnitHoleCount),
                    zl:(vo.data.PTD_Count*vo.data.DtMD_fUnitWeight).toFixed(1),
                    DtMD_sRemark:vo.data.DtMD_sRemark,
                    Writer:ptask.Writer,
                    Auditor:ptask.Auditor,
                    idate:ptask.idate,
                    r_tp:vo.data.DtMD_sType?"<h2 style='border:solid black 1px;text-align: center'>"+vo.data.DtMD_sType+"</h2>":'',
                    r_sz:vo.data.IF_Shi==1?"<h2 style='border:solid black 1px;text-align: center'>试制</h2>":'',
                    // r_dh:vo.data.DtMD_iWelding==1?"<h3 style='border:solid black 1px;text-align: center'>电焊</h3>":'',
                    // r_qj:vo.data.DtMD_iCuttingAngle==1?"<h3 style='border:solid black 1px;text-align: center'>切角</h3>":'',
                    // r_hq:vo.data.DtMD_iFireBending==1?"<h3 style='border:solid black 1px;text-align: center'>制弯</h3>":'',
                    DtMD_sType:vo.data.DtMD_sType,
                    IF_Shi:vo.data.IF_Shi,
                    DtMD_iWelding:vo.data.DtMD_iWelding>=1?"<h3 style='border:solid black 1px;text-align: center'>电焊</h3>":'',
                    DtMD_iFireBending:vo.data.DtMD_iFireBending>=1?"<h3 style='border:solid black 1px;text-align: center'>制弯</h3>":'',
                    DtMD_iCuttingAngle:vo.data.DtMD_iCuttingAngle>=1?"<h3 style='border:solid black 1px;text-align: center'>切角</h3>":'',
                    DtMD_fBackOff:vo.data.DtMD_fBackOff>=1?"<h3 style='border:solid black 1px;text-align: center'>铲背</h3>":'',
                    DtMD_iBackGouging:vo.data.DtMD_iBackGouging>=1?"<h3 style='border:solid black 1px;text-align: center'>清根</h3>":'',
                    DtMD_DaBian:vo.data.DtMD_DaBian>=1?"<h3 style='border:solid black 1px;text-align: center'>打扁</h3>":'',
                    DtMD_KaiHeJiao:vo.data.DtMD_KaiHeJiao>=1?"<h3 style='border:solid black 1px;text-align: center'>开合角</h3>":'',
                    DtMD_ZuanKong:vo.data.DtMD_ZuanKong>=1?"<h3 style='border:solid black 1px;text-align: center'>钻孔</h3>":'',
                    DtMD_GeHuo:vo.data.DtMD_GeHuo>=1?"<h3 style='border:solid black 1px;text-align: center'>割豁</h3>":'',

                    f_IF_Shi:vo.data.IF_Shi?'试制':'',
                    f_DtMD_iWelding:vo.data.DtMD_iWelding>=1?"电焊":'',
                    f_DtMD_iFireBending:vo.data.DtMD_iFireBending>=1?"制弯":'',
                    f_DtMD_iCuttingAngle:vo.data.DtMD_iCuttingAngle>=1?"切角":'',
                    f_DtMD_fBackOff:vo.data.DtMD_fBackOff>=1?"铲背":'',
                    f_DtMD_iBackGouging:vo.data.DtMD_iBackGouging>=1?"清根":'',
                    f_DtMD_DaBian:vo.data.DtMD_DaBian>=1?"打扁":'',
                    f_DtMD_KaiHeJiao:vo.data.DtMD_KaiHeJiao>=1?"开合角":'',
                    f_DtMD_ZuanKong:vo.data.DtMD_ZuanKong>=1?"钻孔":'',
                    f_DtMD_GeHuo:vo.data.DtMD_GeHuo>=1?"割豁":'',

                    r_ht:"<h1 style='border:solid black 1px;text-align: center'>受控</h1>",
                    pg:'第 '+vo.data.pg+' 页，共'+pgcount+'页'
                };
                //console.log(pd);
                return pd;
            }
            // console.log(treedata)
            $('#treeview').jstree({
                'core': {
                    'data': treedata
                },
                "plugins" : [ "checkbox","search"]
            });
            $("#treesearch").keyup(function(){
                $('#treeview').jstree(true).search($.trim($("#treesearch").val()));
            })

            // $('#treeview').on('changed.jstree', function (e, data) {
            //     console.log(data)
            // }).jstree();
            let curvo=null;
            $('#treeview').on('changed.jstree', function (e, data) {
                // console.log('changed',data);return;
                //console.log(data,data.node.id,document.getElementById('p_'+data.node.id).innerText)
                // console.log(data);
                let sid = 'p_' + data.node.id;
                let vo=data.node;
                // curnode=vo;
                //如果不是末节点。定位到最近的一个末节点
                if (data.node.data.length < 1) {
                    for (let v of data.node.children_d) {
                        if (v.indexOf('&') < 0) {
                            sid = 'p_' + v;
                            vo=printdata[v];
                            break;
                        }
                    }
                }

                //方法1，每次点击节点的时候渲染html
                // $('#' + sid).html(htemp.getHtml(getPdata(vo)));
                // console.log(vo)
                curvo=vo;
                $('#p_lc').html(htemp.getHtml(getPdata(vo)));
                // document.getElementById(sid).scrollIntoView()
            }).jstree();

            window.nodeto=function(num){
                //获取当前页，获得id
                // let tree = $ ('#treeview').jstree(true);
                // let curr = tree.get_selected(false);
                // tree.deselect_all();
                // console.log(curvo);
                let pg=curvo?curvo['data']['pg']:1;
                $(".pgpre").removeClass('disabled');
                $(".pgnxt").removeClass('disabled');
                pg=pg+num;
                if(pg<=1){
                    pg=1;
                    $(".pgpre").addClass('disabled');
                }
                if(pg>=pagecount){
                    pg=pagecount;
                    $(".pgnxt").addClass('disabled');
                }
                // console.log(pg)
                for(let v in printdata){
                    if(printdata[v]['data']['pg']==pg){
                        let vo=printdata[v];
                        curvo=vo;
                        // console.log(curvo)
                        $('#p_lc').html(htemp.getHtml(getPdata(vo)));
                        break;
                    }
                }


                // console.log(printdata[curvo['id']]);
                // let n = tree.get_next_dom (curr);
                // console.log(tree.get_node(n))
                // tree.open_node(n);
                // tree.select_node(n);
                // alert(1);
            };

            //默认渲染第一个页面。找出第一个末节点，并渲染html
            nodeto(0);
            // for(let v in printdata){
            //     // console.log(Config.printdata[0]);
            //     let vo=printdata[v];
            //     curvo=vo;
            //     $('#p_lc').html(htemp.getHtml(getPdata(vo)));
            //     break;
            // }

            //方法2，一次性渲染全部打印html
            // let printjson = [];
            // for (let v in printdata) {
            //     // console.log('p_'+v.id)
            //     // let printjson= {};
            //     // printjson['tb']=[];
            //     // let tmp={};
            //     // tmp['aa']='aa';
            //     // tmp['bb']='bb';
            //     // printjson['tb'].push(tmp);
            //     // printjson['dd']='dddd';
            //     // console.log('#p_'+v.id)
            //     // $('#p_'+v.id).html(htemp.getHtml(printjson));
            //     // console.log(v)
            //     printjson.push(getPdata(printdata[v]));
            // }
            let treeobj=$("#treeview").jstree(true);
            //执行打印
            $('#handleprint').click(function () {

                // console.log(new Date().toLocaleString());
                //获取勾选项
                let selectnodes=treeobj.get_selected();
                if(selectnodes.length<1){
                    Fast.api.toastr.error('请先勾选');
                    return ;
                }



                // console.log(selectnodes)
                //组合成新的打印数据包
                let printjson=[];
                let i=1;
                let pcount=0;
                for(let v of selectnodes){
                    if(printdata.hasOwnProperty(v)){
                        pcount=pcount+1;
                    }
                }

                for(let v of selectnodes){
                    if(printdata.hasOwnProperty(v)){
                        printdata[v]['data']['pg']=i;
                        let vo=$.extend(true,{},printdata[v]);
                        vo['data']['pg']=i;
                        // console.log(vo)
                        let row=getPdata(printdata[v],pcount);
                        for(let row_key in row){
                            if(row[row_key] && row[row_key].toString().indexOf('>')>-1){
                                delete row[row_key];
                            }
                        }
                        printjson.push(row);
                        i++;
                    }
                };
                //执行打印

                // let Lindex=Layer.load(0);
                let index = layer.confirm('共勾选'+printjson.length+'项，是否立即打印？', {
                    btn: ['确定', '取消'],
                }, function(data) {
                    htemp.print(printjson);

                    layer.close(index);
                })
                // htemp.print(printjson);

                // Layer.close(Lindex);
            });

            $('#a5pdf').click(function () {
                Fast.api.ajax({
                    url: 'chain/lofting/produce_task/getpdf/PT_Num/' + ptask.PT_Num+'/ifg/a5',                      
                }, function (_, ret) {   
                    console.log(ret);
                    window.top.Fast.api.open(ret.data['A5'], 'A5流程卡', {
                        area: ["100%", "100%"]
                    });
                }, function (_, ret) {
                    console.log('err', arguments)
                });
            });

            $('#handleprintpdf').click(function () {
                // console.log(new Date().toLocaleString());
                //获取勾选项
                let selectnodes=treeobj.get_selected();
                if(selectnodes.length<1){
                    Fast.api.toastr.error('请先勾选');
                    return ;
                }
                // console.log(selectnodes)
                //组合成新的打印数据包
                let printjson=[];
                let i=1;
                let pcount=0;
                for(let v of selectnodes){
                    if(printdata.hasOwnProperty(v)){
                        pcount=pcount+1;
                    }
                }
                for(let v of selectnodes){                    
                    if(printdata.hasOwnProperty(v)){
                        printdata[v]['data']['pg']=i;
                        let vo=$.extend(true,{},printdata[v]);
                        vo['data']['pg']=i;
                        // console.log(vo)
                        let row=getPdata(printdata[v],pcount);
                        for(let row_key in row){
                            if(row[row_key] && row[row_key].toString().indexOf('>')>-1){
                                delete row[row_key];
                            }
                        }
                        printjson.push(row);
                        i++;
                    }
                };
                //执行打印
                // console.log(printjson)
                // htemp.print(printjson);
                // let htmlData=htemp.getHtml(printjson);
                // console.log(htmlData);
                // console.log(new Date().toLocaleString());
                // $.ajax({
                //     type: "POST",
                //     dataType: "json",
                //     url:'chain/lofting/produce_task/topdf',
                //     data:{data:htmlData.html()}
                // })

                // htemp.print2(printjson);
                // console.log({"data":printjson});

                let indexp = layer.prompt({
                    title: '共勾选' + pcount + '项，是否立即打印？',
                    placeholder: '页码选择,例如:1-5,8 默认打印全部',
                }, function (value, index, elem) {
                    value = value.replace(/ /g, "");
                    if (value === '') {
                        printjson = encodeURI(JSON.stringify({"data": printjson}));
                    }else{
                        var arr = value.split(',');
                        var pageNumberRegExp = new RegExp("\\d+(-\\d+)?");
                        var printjsonnew = [];
                        for (let v of arr) {
                            let p = v.match(pageNumberRegExp)
                            if (!p) {
                                console.log('格式有误！')
                                return;
                            }
                            var varr = v.split('-');
                            if (varr.length === 1) {
                                printjsonnew.push(printjson[v - 1]);
                            } else {
                                for (let i = parseInt(varr[0]); i < parseInt(varr[1]) + 1; i++) {
                                    printjsonnew.push(printjson[i - 1]);
                                }
                            }
                        }
                        // console.log(printjsonnew)
                        printjson = encodeURI(JSON.stringify({"data": printjsonnew}));
                    }

                    Fast.api.ajax({
                        url: 'chain/lofting/produce_task/printlck',
                        contentType: 'application/json',
                        data: JSON.stringify({js: printjson, pt: ptask.PT_Num}),
                    }, function (_, ret) {
                       console.log('ok',ret.url,arguments)
						//window.open(ret.url,'_blank');
						layer.open({type:2,content:ret.url,area:['100%','100%'],title:false})
                    }, function (_, ret) {
                        console.log('err', arguments)
                    })
                    layer.close(indexp);
                })


            });
        },
        viewproduction: function (){
            $(document).on("click", "#export", function () {
                if(Config.PT_Num == false){
                    layer.confirm('请重试！');
                    return false;
                }
                window.open('/admin.php/chain/lofting/produce_task/exportProduction/ids/'+Config.PT_Num);
            });
            $("#lckprint").click(function () {
                let ptd_ids_obj=$('#product').bootstrapTable('getSelections');
                let ptd_ids=[];
                for(let v of ptd_ids_obj){
                    ptd_ids.push(v['PTD_ID']);
                }
                ptd_ids=ptd_ids.join(',');
                $.ajax({
                    type: 'POST',
                    url: 'chain/lofting/produce_task/setcache',
                    data: {value:ptd_ids},
                    success: function(result){
                        if(result.code == 1){
                            window.top.Fast.api.open('chain/lofting/produce_task/showLckPrint/PT_Num/' + Config.PT_Num+'/ptdid/'+result.data, 'A5流程卡', {
                                area: ["100%", "100%"]
                            });
                        }
                    }
                });
            });
           
            $("#jgprint").click(function () {
                let ptd_ids_obj=$('#product').bootstrapTable('getSelections')
                let ptd_ids=[];
                for(let v of ptd_ids_obj){
                    ptd_ids.push(v['PTD_ID']);
                }
                ptd_ids=ptd_ids.join(',');
                $.ajax({
                    type: 'POST',
                    url: 'chain/lofting/produce_task/setcache',
                    data: {value:ptd_ids},
                    success: function(result){
                        if(result.code == 1){
                            window.top.Fast.api.open('chain/lofting/produce_task/showJgPrint/PT_Num/' + Config.PT_Num+'/ptdid/'+result.data, '加工明细表', {
                                area: ["100%", "100%"]
                            });
                        }
                    }
                });
                
            });
            
            // $("#lckprintpdf").click(function () {
            //     let ptd_ids_obj=$('#product').bootstrapTable('getSelections');
            //     let ptd_ids=[];
            //     for(let v of ptd_ids_obj){
            //         ptd_ids.push(v['PTD_ID']);
            //     }
            //     ptd_ids=ptd_ids.join(',');
            //     $.ajax({
            //         type: 'POST',
            //         url: 'chain/lofting/produce_task/setcache',
            //         data: {value:ptd_ids},
            //         success: function(result){
            //             if(result.code == 1){
            //                 $.ajax({
            //                     type: 'POST',
            //                     url: 'chain/lofting/produce_task/getpdf/PT_Num/' + Config.PT_Num+'/ifg/a5/ptdid/'+result.data,                                
            //                     success: function(result){
            //                         if(result.code == 1){                                        
            //                             window.top.Fast.api.open(result.data['A5'], 'A5流程卡', {
            //                                 area: ["100%", "100%"]
            //                             });
            //                         }
            //                     }
            //                 })                            
            //             }
            //         }
            //     });
            // });
            $("#lckprintpdf").click(function () {
                Fast.api.ajax({
                    url: 'chain/lofting/produce_task/getpdf/PT_Num/' + Config.PT_Num+'/ifg/a5',                      
                }, function (_, ret) {   
                    console.log(ret);
                    window.top.Fast.api.open(ret.data['A5'], 'A5流程卡', {
                        area: ["100%", "100%"]
                    });
                }, function (_, ret) {
                    console.log('err', arguments)
                });
            });
            
            $("#jgprintpdf").click(function () {
                window.top.Fast.api.open('chain/lofting/produce_task/showJgPrint/PT_Num/' + Config.PT_Num+'/ispdf/1', '加工明细表', {
                    area: ["100%", "100%"]
                });                
            });

           

            getTableContent();

            $(document).on("change","select[name=material_list]", function(e){
                var selectText = $(this).find('option:selected').val();
                getTableContent(selectText);

            })
            function getTableContent(selectText = ""){
                $.ajax({
                    url: 'chain/lofting/produce_task/viewProduction/ids/'+Config.PT_Num,
                    type: 'post',
                    dataType: 'json',
                    data: {num: selectText},
                    success: function (ret) {
                        if(ret.code==1){
                            $('#product').bootstrapTable('destroy');
                            // 初始化表格
                            $("#product").bootstrapTable({
                                data: ret.data,
                                height: document.body.clientHeight - 50,
                                columns: [
                                    [
                                        {checkbox:true, field: "choose"},
                                        {field:'PTD_ID',title:'PTD_ID',visible:false},
                                        {field:'id',title:'id',visible:false},
                                        {field: 'ids', title: ret.count, formatter: function(value,row,index){return ++index;}},
                                        {field: 'PT_Sect', title: __('PT_Sect')},
                                        {field: 'DtMD_sPartsID', title: __('DtMD_sPartsID')},
                                        {field: 'DtMD_sStuff', title: __('DtMD_sStuff')},
                                        {field: 'DtMD_sMaterial', title: __('DtMD_sMaterial')},
                                        {field: 'DtMD_sSpecification', title: __('DtMD_sSpecification')},
                                        {field: 'DtMD_iLength', title: __('DtMD_iLength')},
                                        {field: 'DtMD_fWidth', title: __('DtMD_fWidth'), formatter: function(value,row,index){return value!=='0'?value:'';}},
                                        {field: 'DtMD_iTorch', title: __('DtMD_iTorch'), formatter: function(value,row,index){return value!=='0'?value:'';}},
                                        {field: 'DtMD_sType', title: '材料标准'},
                                        {field: 'type', title: '类型'},
                                        {field: 'DD_Count', title: '单段件数'},
                                        {field: 'PTD_Count', title: '加工数'},
                                        {field: 'DtMD_iUnitHoleCount', title: __('DtMD_iUnitHoleCount')},
                                        {field: 'SumCount', title: __('SumCount')},
                                        {field: 'DtMD_fUnitWeight', title: __('DtMD_fUnitWeight')},
                                        {field: 'SumWeight', title: __('SumWeight')},
                                        {field: 'DtMD_sRemark', title: __('DtMD_sRemark')},
                                        {field: 'IF_Shi', title: __('IF_Shi')},
                                        // {field: 'DtMD_iWelding', title: __('DtMD_iWelding')}
                                    ]
                                ]
                            });
                            // 为表格绑定事件
                            Table.api.bindevent($("#product"));
                            multiple_checks("product");
                        }else{
                            layer.msg(ret.msg);
                        }

                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            function multiple_checks(table_id='product')
            {
                var mousedown=false;var check = 0;var choose;
                var allData;
                $("body").on("mousedown", "#"+table_id+" tbody tr",function(e){
					allData = $("#"+table_id).bootstrapTable("getData");
                    mousedown=true;
                    check = $(this).index();
                    var check_element =allData[check];
                    choose = check_element.choose;
                });
                $("body").on("mouseup", "#"+table_id+" tbody tr",function(e){
                    if(mousedown){
                        var mousedown_check = $(this).index();
                        var big = check>mousedown_check?check:mousedown_check;
                        var small = check<mousedown_check?check:mousedown_check;
                        if(big == small){
                            if(choose && choose!='undefined') $("#"+table_id).bootstrapTable('uncheck', big);
                            else $("#"+table_id).bootstrapTable('check', big);
                        }else{
                            var id_list = [];
                            for( var i=small;i<=big;i++){
                                id_list.push(allData[i].id);
                            }
                            if(choose && choose!='undefined'){
                                if(id_list.length>0) $("#"+table_id).bootstrapTable("uncheckBy", {field:"id", values:id_list})
                            }else{
                                if(id_list.length>0) $("#"+table_id).bootstrapTable("checkBy", {field:"id", values:id_list})
                            }
                        }
                    }
                    mousedown=false;
                    window.getSelection().empty()
                });
            }
        },
        viewwelding: function (){
            $("#zhprint").click(function () {
                window.top.Fast.api.open('chain/lofting/produce_task/showZhPrint/PT_Num/' + Config.PT_Num, '组焊清单', {
                    area: ["100%", "100%"]
                });
            });
            // 初始化表格
            $("#welding").bootstrapTable({
                data: welding,
                pk: 'PDH_ID',
                height: 550,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'operate', title: '删除',
                            buttons: [{name: 'comfirmDel',
                            text: '删除',
                            title: '删除',
                            classname: 'btn btn-xs btn-danger btn-view btn-ajax',
                            icon: 'fa fa-trash',
                            url: 'chain/lofting/produce_task/comfirmDel/dcd/{PDH_ID}',
                            refresh: true}],
                            table: $("#welding"), events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                        {field: 'DCD_PartName', title: __('DCD_PartName')},
                        {field: 'DCD_PartNum', title: __('DCD_PartNum')},
                        {field: 'PDH_Count', title: __('PDH_Count')},
                        {field: 'DCD_SWeight', title: __('DCD_SWeight')},
                        {field: 'SumWeight', title: __('SumWeight')},
                        {field: 'DHS_Count', title: __('DHS_Count')},
                        {field: 'DCD_Type', title: __('DCD_Type')},
                        {field: 'DCD_Memo', title: __('DCD_Memo')},
                        {field: 'DCD_ID', title: __('DCD_ID'),visible: false},
                        {field: 'PDH_ID', title: __('PDH_ID'),visible: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent($("#welding"));
            $("#welding").on("click-row.bs.table",function(row,$elemente){
                $.ajax({
                    url: 'chain/lofting/produce_task/viewWeldingDetail',
                    type: 'post',
                    dataType: 'json',
                    data: $elemente,
                    success: function (ret) {
                        var tableContent = [];
                        if(ret.code==1) tableContent = ret.data;
                        $("#weldingDetail").bootstrapTable('destroy');
                        $("#weldingDetail").bootstrapTable({
                            data: tableContent,
                            height: 200,
                            columns: [
                                [
                                    {field: 'DtMD_sPartsID', title: __('DtMD_sPartsID')},
                                    {field: 'DtMD_sStuff', title: __('DtMD_sStuff')},
                                    {field: 'DtMD_sMaterial', title: __('DtMD_sMaterial')},
                                    {field: 'DtMD_sSpecification', title: __('DtMD_sSpecification')},
                                    {field: 'DtMD_fWidth', title: __('DtMD_fWidth')},
                                    {field: 'DtMD_fUnitWeight', title: __('DtMD_fUnitWeight')},
                                    {field: 'DHS_Count', title: __('DHS_Count')},
                                    {field: 'Sum_Count', title: '件数'},
                                    {field: 'DHS_Length', title: __('DHS_Length')},
                                    {field: 'DHS_Grade', title: __('DHS_Grade')},
                                    {field: 'DHS_Memo', title: __('DHS_Memo')}
                                ]
                            ]
                        });
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        joinview: function(){
            Controller.api.bindevent("joinview");
            $(document).on("click", "#save", function () {
                var arr = [];
                var chooseCtrlTr = $("#myTabContent").find("tbody tr");
                $.each(chooseCtrlTr,function(index,e){
                    if($(e).find("input[name='choose']").prop("checked") != false){
                        var chooseCtrlInput = $(e).find("input");
                        $.each(chooseCtrlInput,function(index_i,e_i){
                            var name = $(e_i).prop("name");
                            if(name == "choose" || name == "ctrlz"){
                                arr.push({name:name,value:$(e_i).prop("checked")});
                            }else if($(e_i).data("value") != undefined){
                                var self_value = $(e_i).data("value");
                                var change_value = $(e_i).val()==""?0:parseInt($(e_i).val());
                                if(change_value > self_value){
                                    arr.push({name:name,value:self_value});
                                }else if(change_value<0){
									arr.push({name:name,value:0});
								}else{
                                    arr.push({name:name,value:change_value});
                                }
                            }else{
                                var change_value = $(e_i).val();
                                arr.push({name:name,value:change_value});
                            }
                        })
                    }
                });

                Fast.api.close(arr);
            });
            $(document).on("click", "#all_select", function () {
                $("#tableContent tbody").find("tr").each(function () {
                    var checkedField = $(this).children('td').eq(0).find('input');
                    checkedField.prop("checked","true");
                });
            });
            $(document).on("click", "#giveup_select", function () {
                $("#tableContent tbody").find("tr").each(function () {
                    var checkedField = $(this).children('td').eq(0).find('input');
                    checkedField.prop("checked","");
                });
            });

        },
        testsectiongroup: function (){
            var groupTable = $("#groupTable");
            // 初始化表格
            groupTable.bootstrapTable({
                data: groupTableContent,
                columns: [
                    [
                        {field: 'PT_Num', title: __('Pt_num'), visible: false},
                        {field: 'PT_Sect', title: '段名'},
                        {field: 'PT_shiCount', title: '段数'}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(groupTable);
            $(document).on("keyup", "input[name='PT_shiCount[]']", function(e){
                var change_val = $(this).val();
                $(this).text(change_val);
                // $(e.target).attr("value",change_val);
            });
            $(document).on("click", "#setingDSave", function () {
                var content = groupTable.bootstrapTable('getData');
                let endContent = [];
                $.each(content,function(index,e){
                    let detail = {};
                    detail.PT_Num = e.PT_Num;
                    detail.PT_Sect = e.PT_Sect;
                    let PT_shiCount = $("input[name='PT_shiCount[]']").eq(index).val();
                    detail.PT_shiCount = PT_shiCount;
                    endContent.push(detail);
                })
                var contentStr = JSON.stringify(endContent);
                $.ajax({
                    url: 'chain/lofting/produce_task/testSectionGroupSave/pt_num/'+Config.pt_num,
                    type: 'post',
                    dataType: 'json',
                    data: {content: contentStr},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });

            });

        },
        selectdetail: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/produce_task/selectDetail' + location.search,
                    table: 'taskdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TD_ID',
                sortName: 'dm.DtM_dTime',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                sortOrder: 'DESC',
                showToggle: false,
                showExport: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'd.TD_TypeName', title: '塔型', operate: 'LIKE',visible: false},
                        {field: 'T_Num', title: '任务单(生产单)号', operate: false},
                        {field: 'd.T_Num', title: '任务单(生产单)号', operate: 'LIKE',visible: false},
                        {field: 'P_Name', title: '产品名称', operate: false},
                        // {field: 'P_Num', title: '线路名称', operate: false},
                        // {field: 'd.P_Num', title: '线路名称', operate: 'LIKE',visible: false},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        // {field: 'c.PC_Num', title: '工程编号', operate: 'LIKE',visible: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE',visible: false},
                        {field: 'C_Project', title: '工程名称', operate: false},
                        {field: 'c.C_Project', title: '工程名称', operate: 'LIKE',visible: false},
                        {field: 'C_Company', title: '下发公司', operate: false},
                        {field: 'C_SortProject', title: '工程简写', operate: false},
                        {field: 'Customer_Name', title: '客户', operate: false},
                        {field: 'T_Count', title: '总数', operate: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        // {field: 'PT_sumWeight', title: '总重(kg)', operate: false},
                        // {field: 'C_ProjectName', title: '审核时间', operate: false},
                        {field: 'C_SaleMan', title: '业务员', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent[0]);

            });
        },
        api: {
            bindevent: function (controllerName) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(controllerName!="add"){
                        window.location.reload();
                    }else{
                        window.location.href = "edit/ids/"+data;
                    }
                    return false;

                });

                $(document).on('click', "#selectDetail", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            if(value.length==0) return false;
                            $("#c-TD_ID").val(value.TD_ID);
                            $("#c-TD_TypeName").val(value.TD_TypeName);
                            $("#c-C_ProjectName").val(value.C_Project);
                            $("#c-PT_ShortProject").val(value.C_SortProject);
                            $("#c-TD_Pressure").val(value.TD_Pressure);
                            // $("#c-PT_sumCount").val(value.PT_sumCount);
                            // $("#c-PT_sumWeight").val(value.PT_sumWeight);
                            $.ajax({
                                url: 'chain/lofting/produce_task/release',
                                type: 'post',
                                dataType: 'json',
                                data: {num: value.TD_ID},
                                success: function (ret) {
                                    var tableHtml = '';
                                    if(ret.code==1) tableHtml = ret.data;
                                    $("#leftbottom").html(tableHtml);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }
                    };
                    Fast.api.open('chain/lofting/produce_task/selectDetail',"任务单塔型选择",options);
                });
            }

        }
    };
    return Controller;
});
