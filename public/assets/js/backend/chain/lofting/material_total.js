define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/material_total/index' + location.search,
                    table: 'materialreplacedetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'MR_Num',
                sortName: 'WriterDate',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                columns: [
                    [
                        // {checkbox: true},新材质
                        {field: 'NewLimber', title: '材质', operate: 'LIKE'},
                        {field: 'NewType', title: '规格', operate: 'LIKE'},
                        {field: 'NewClass', title: '材料名称', operate: 'LIKE'},
                        {field: 'NewLength', title: '长度', operate: 'LIKE'},
                        {field: 'NewWidth', title: '宽度', operate: 'LIKE'},
                        {field: 'NewCount', title: '数量', operate: false},
                        {field: 'NewWeight', title: '重量', operate: false},
                        {field: 'PT_Num', title: '下达单号', operate: 'LIKE'},
                        {field: 't_project', title: '工程', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'WriterDate', title: '时间',defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});