define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'txjckdata'], function ($, undefined, Backend, Table, Form) {

    function group(array, subGroupLength) {
        let index = 0;
        let newArray = [];
        while(index < array.length) {
            newArray.push(array.slice(index, index += subGroupLength));
        }
        return newArray;
    }
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/base_library/index' + location.search,
                    add_url: 'chain/lofting/base_library/add',
                    edit_url: 'chain/lofting/base_library/edit',
                    del_url: 'chain/lofting/base_library/del',
                    multi_url: 'chain/lofting/base_library/multi',
                    import_url: 'chain/lofting/base_library/import',
                    table: 'baselibrary',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'DtM_iID_PK',
                sortName: 'DtM_iID_PK',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DtM_iID_PK', title: __('Dtm_iid_pk'), visible: false, operate: false},
                        {field: 'DtM_sTypeName', title: __('Dtm_stypename'), operate: 'LIKE'},
                        {field: 'DtM_sPressure', title: __('Dtm_spressure'), operate: false},
                        {field: 'DtM_sAuditor', title: __('Dtm_sauditor'), operate: false},
                        {field: 'DtM_dTime', title: __('Dtm_dtime'), operate:false},
                        {field: 'DtM_sRemark', title: __('Dtm_sremark'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/base_library/edit/ids/'+Config.ids + location.search,
                    table: 'baselibrary',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                search: false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                showExport: false,
                columns: [
                    [
                        {field: 'DtS_Name', title: '段名'},
                        {field: 'DtMD_sPartsID', title: '零件编号'},
                        {field: 'DtMD_sMaterial', title: '材质'},
                        {field: 'DtMD_sSpecification', title: '规格'},
                        {field: 'DtMD_iLength', title: '长度(mm)'},
                        {field: 'DtMD_fWidth', title: '宽度(mm)'},
                        {field: 'DtMD_iTorch', title: '厚度(mm)'},
                        {field: 'DtMD_iUnitCount', title: '单基数量'},
                        {field: 'DtMD_iUnitHoleCount', title: '孔数'},
                        {field: 'type', title: '类型'},
                        {field: 'DtMD_iWelding', title: '电焊(0/1/2/3)',formatter: function(value){ return value?value:'';}},
                        {field: 'DtMD_iFireBending', title: '制弯(0/1/2)',formatter: function(value){ return value?value:'';}},
                        {field: 'DtMD_iCuttingAngle', title: '切角(0/1)',formatter: function(value){ return value?value:'';}},
                        {field: 'DtMD_fBackOff', title: '铲背(0/1)',formatter: function(value){ return value?value:'';}},
                        {field: 'DtMD_iBackGouging', title: '清根(0/1)',formatter: function(value){ return value?value:'';}},
                        {field: 'DtMD_DaBian', title: '打扁(0/1)',formatter: function(value){ return value?value:'';}},
                        {field: 'DtMD_KaiHeJiao', title: '开合角(0/1/2)',formatter: function(value){ return value?value:'';}},
                        {field: 'DtMD_GeHuo', title: '割豁(0/1)',formatter: function(value){ return value?value:'';}},
                        {field: 'DtMD_ZuanKong', title: '钻孔(0/1)',formatter: function(value){ return value?value:'';}},
                        {field: 'DtMD_sRemark', title: '备注'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $("#print").click(function () {
                window.top.Fast.api.open('chain/lofting/base_library/print/ids/' + Config.ids, '塔型基础库', {
                    area: ["100%", "100%"]
                });
            });
            Controller.api.bindevent("edit");
        },
        importview: function () {
            Form.api.bindevent($("form[role=form]"),function(){
                parent.location.reload();
            });
        },
        openedit: function () {
            Form.api.bindevent($("form[role=form]"));
            var height = document.body.clientHeight-100;
            var tableCopyField = Config.tableCopyField;
            $("#table_div").height(height);
            $(document).on('click', "#tbshow input[type='checkbox']", function(e){
                var checkbox_div = $(e.target);
                if(checkbox_div.is(':checked')){
                    $(e.target).parents("tr").css("background-color","#E0FFFF");
                }else{
                    $(e.target).parents("tr").removeAttr("style");
                }
            });

            multiple_checks();
            function multiple_checks(table_id='show')
            {
                var mousedown=false;var check = 0;var choose;
                $("body").on("mousedown", "#"+table_id+" tbody tr",function(e){
                    mousedown=true;
                    check = $(this).index();
                    check_element =$("#"+table_id+" tbody").find("tr").eq(check);
                    choose = check_element.find("td:eq(0) input").is(':checked');
                });
                $("body").on("mouseup", "#"+table_id+" tbody tr",function(e){
                    if(mousedown){
                        var mousedown_check = $(this).index();
                        var big = check>mousedown_check?check:mousedown_check;
                        var small = check<mousedown_check?check:mousedown_check;
                        if(big == small){
                            if(choose){
                                $("#"+table_id+" tbody tr:eq("+big+")").find("td:eq(0) input").prop("checked",true);
                                $("#"+table_id+" tbody tr:eq("+big+")").css("background-color","#E0FFFF");
                            }else{
                                $("#"+table_id+" tbody tr:eq("+big+")").find("td:eq(0) input").prop("checked",false);
                                $("#"+table_id+" tbody tr:eq("+big+")").removeAttr("style");
                            }

                        }else{
                            for( var i=small;i<=big;i++){
                                if(choose){
                                    $("#"+table_id+" tbody tr:eq("+i+")").find("td:eq(0) input").prop("checked",false);
                                    $("#"+table_id+" tbody tr:eq("+i+")").removeAttr("style");
                                }else{
                                    $("#"+table_id+" tbody tr:eq("+i+")").find("td:eq(0) input").prop("checked",true);
                                    $("#"+table_id+" tbody tr:eq("+i+")").css("background-color","#E0FFFF");
                                }
                            }
                        }
                    }
                    mousedown=false;
                });
            }
            var position = localStorage.getItem("scroll_top");
            $("#table_div").scrollTop(position);
            localStorage.setItem('scroll_top',0);
            $(document).on('click', "#save", function(e){
                var formData = [];
                $.each($("td"),function(index,e){
                    formData.push($(this).text());
                });
                var arr = [];
                arr = group(formData,22);
                var scroll_top = $("#table_div").scrollTop();
                localStorage.setItem("scroll_top", scroll_top);
                $.ajax({
                    url: 'chain/lofting/base_library/openEdit/ids/'+Config.ids,
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(arr)},
                    success: function (ret) {
                        layer.msg(ret.msg);
                        if (ret.hasOwnProperty("code")) {
                            if (ret.code === 1) {
                                window.location.reload();
                            }
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on('click', ".del", function(e){
                $( e.target ).closest("tr").remove();
            });

            $(document).on('click', "#del", function(e){
                $("#tbshow").find("tr").each(function () {
                    var checkedField = $(this).children('td').eq(0).find('input');
                    if(checkedField.is(':checked')) $( this).remove();
                });
            });
            $(document).on('click', "#copy", function(e){
                var checkedField = $('#show tbody tr').find("td:eq(0) input:checked");
                var text = "";
                //copyfield 还要修改
                var copyField="";
                checkedField.prop("checked",false);
                checkedField.each(function (index,e){
                    $(e).parents("tr").css("background-color","");
                    text += "<tr>"+$(e).parents("tr")[0].innerHTML+"</tr>";
                    copyField += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a><a href="javascript:;" class="btn btn-xs btn-info detailImport"><i class="fa fa-upload"></i></a></td><td><input type="checkbox"></td><td></td>';
                    for(var i=0;i<tableCopyField.length;i++){
                        if($("#thshow th[data-name='"+tableCopyField[i][0]+"']")){
                            var th_field = $("#thshow th[data-name='"+tableCopyField[i][0]+"']").index();
                            var td_content =  '';
                            if(th_field!=-1) td_content = $(e).parents("tr").find("td:eq("+th_field+")").text();
                            copyField += '<td '+tableCopyField[i][2]+'><input class="small_input" type="text" data-rule="'+ tableCopyField[i][1] +'" name="'+ tableCopyField[i][0] +'[]" value="'+(td_content)+'"></td>';
                            
                        }else{
                            copyField += '<td '+tableCopyField[i][2]+'><input class="small_input" type="text" data-rule="'+ tableCopyField[i][1] +'" name="'+ tableCopyField[i][0] +'[]" value=""></td>';
                        }
                    }
                    copyField += '</tr>';
                });
                console.log(copyField);
                localStorage.setItem('checkedField',text);
                localStorage.setItem('commonCheckedField',copyField);
            });

            $(document).on('click', "#stick", function(e){
                var checkedField = localStorage.getItem('checkedField');
                if(checkedField){
                    $('#show tbody').append(checkedField);
                }else{
                    var text = '<tr><td><input class="small_input" type="checkbox" value="1"></td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a><a href="javascript:;" class="btn btn-xs btn-success plus"><i class="fa fa-plus"></i></a></td>';
                    $.each(Config.tableField,function(index,e){
                        text += '<td contenteditable></td>';
                    });
                    text += "</tr>";
                    $('#show tbody').append(text);
                }
                //localStorage.setItem('checkedField','');
            });
            // $(document).on('click', "#replace", function(e){
            //     var replaceField = $('#show tbody tr').find("td:eq(0) input:checked");
            //     var index = $(replaceField[0]).parents("tr").index()-1;
            //     var checkedField = localStorage.getItem('checkedField');
            //     // console.log(checkedField);return false;
            //     if(checkedField.length == 0){ layer.msg("复制为空，请重新复制！"); }
            //     else if(replaceField.length != 0){
            //         replaceField.each(function (index,e){
            //             $( e ).parents("tr").remove();
            //         });
            //         $('#show tbody tr:eq('+index+')').after(checkedField);
            //         //localStorage.setItem('checkedField','');
            //     }
            // });

            $(document).on('click', ".plus", function(e){
                var index_ini = $(this).parents("tr").index();
                var checkedField = localStorage.getItem('checkedField');
                if(checkedField){
                    $('#show tbody').find("tr:eq("+index_ini+")").before(checkedField);
                }else{
                    var text = '<tr><td><input class="small_input" type="checkbox" value="1"></td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a><a href="javascript:;" class="btn btn-xs btn-success plus"><i class="fa fa-plus"></i></a></td>';
                    $.each(Config.tableField,function(index,e){
                        text += '<td contenteditable></td>';
                    });
                    text += "</tr>";
                    $('#show tbody').find("tr:eq("+index_ini+")").before(text);
                }
                //localStorage.setItem('checkedField','');
            });
            // $(document).on("keyup", "#tbshow input[type='text']", function(e){
            //     var change_val = $(this).val();
            //     $(e.target).attr("value",change_val);
            // });
            // $(document).on("click", "#tbshow input[type='checkbox']", function(e){
            //     var flag = $(this).prop("checked");
            //     $(e.target).attr("checked",flag);
            // });
            //点击表头选择某一列
            $(document).on("click", "#thshow th", function(e){
                var flag = $(e.target).attr("data-flag");
                var index = $(e.target).index();
                if(flag==1){
                    $(e.target).attr("data-flag",0);
                    $("#show tr").find("td:eq("+index+")").css("background-color","");
                    $("#show tr").find("th:eq("+index+")").css("background-color","");
                }else{
                    $(e.target).attr("data-flag",1);
                    $("#show tr").find("td:eq("+index+")").css("background-color","pink");
                    $("#show tr").find("th:eq("+index+")").css("background-color","pink");
                }
            });

            //替换
            var height=0;
            var search_content_input;
            var search_height=[];
            $(document).on('click', "#search", function(e){
                search_height=[];
                var search_content = $("#search_input").val();
                if(search_content_input && search_content_input.length != 0){
                    // search_content_input.parent("td").removeClass("td_height_light");
                    search_content_input[height].classList.remove("input_height_light");
                }
                // var search_content = $("#search_input").val();
                search_content = search_content.replace('\\','\\\\');
                if(search_content){
                    search_content_input = $("#tbshow tr td:contains("+search_content+")");
                    $.each(search_content_input,function(index, e){
                        search_height.push($(e).position().top);
                    })
                    height = 0;
                    search_content_input[height].classList.add("input_height_light");
                    $("#table_div").scrollTop(search_height[height]-30);
                }
            });

            $(document).on('keyup', "#search_input", function(e){
                if(e.keyCode == 13){
                    if(search_content_input && search_content_input.length != 0){
                        search_content_input[height].classList.remove("input_height_light");
                    }
                    var search_content = $("#search_input").val();
                    search_content = search_content.replace('\\','\\\\');
                    if(search_content){
                        search_content_input = $("#tbshow tr td:contains("+search_content+")");
                        height = 0;
                        search_content_input[height].classList.add("input_height_light");
                        $("#table_div").scrollTop(search_height[height]-30);
                    }
                }
            });

            $(document).on('click', "#give_up_search", function(e){
                $("#search_input").val('');
                // search_content_input.parent("td").removeClass("td_height_light");
                search_content_input[height].classList.remove("input_height_light");
            });
            $(document).on('click', "#next_search", function(e){
                search_content_input[height].classList.remove("input_height_light");
                height++;
                if(!search_content_input[height]) height=0;
                search_content_input[height].classList.add("input_height_light");
                $("#table_div").scrollTop(search_height[height]-30);
            });
            $(document).on('click', "#content_all_replace", function(e){
                var search_content = $("#search_input").val();
                search_content = search_content.replace('\\','\\\\');
                var replace_content = $("#replace_input").val();
                if(!search_content){
                    layer.msg("不存在查找字符！");
                    return false;
                }
                // var selector = "";
                search_content_input = $("#tbshow tr td:contains('"+search_content+"')");
                var search_choose_flag = 1;
                //找到勾选列和勾选行 一旦找到 search_choose_flag为0
                let accurate_row = [];
                $.each($("#tbshow tr td [type=checkbox]:checked"),function(cindex,crow){
                    accurate_row.push($(crow).closest("tr").index());
                })
                let accurate_col = [];
                $('#thshow th').each(function(e_index,e_e) {
                    if($(e_e).attr("data-flag")==1) accurate_col.push(e_index);
                });
                if(accurate_row.length!=0 || accurate_col!=0) search_choose_flag = 0;
                let flag=0;
                if(search_content_input && search_content_input.length != 0){
                    $.each(search_content_input,function(i_index,i_e){
                        if(search_choose_flag) flag=1;
                        else{
                            if(accurate_row.length!=0 && accurate_col!=0){
                                if($.inArray($(this).parents("tr").find("td").index($(this)),accurate_col)!=-1 && $.inArray($(this).closest("tr").index(),accurate_row)!=-1) flag=1;;
                            }else if(accurate_row.length!=0){
                                if($.inArray($(this).closest("tr").index(),accurate_row)!=-1) flag=1;
                            }else if($.inArray($(this).parents("tr").find("td").index($(this)),accurate_col)!=-1) flag=1;
                        }
                        if(flag){
                            var value = $(i_e).text();
                            value = value.replace($("#search_input").val(),replace_content);
                            $(i_e).text(value);
                        }
                        flag=0;
                    });
                }

                if(search_content_input[height]) search_content_input[height].classList.remove("input_height_light");
                height = 0;
                search_content_input = [];
            });
            $(document).on('click', "#content_this_replace", function(e){
                var search_content = $("#search_input").val();
                var length = search_content.length;
                search_content = search_content.replace('\\','\\\\');
                var replace_content = $("#replace_input").val();
                if(!search_content){
                    layer.msg("不存在查找字符！");
                    return false;
                }
                search_content_input = $("#tbshow tr td:contains('"+search_content+"')");
                var search_choose_flag = 1;
                //找到勾选列和勾选行 一旦找到 search_choose_flag为0
                let accurate_row = [];
                $.each($("#tbshow tr td [type=checkbox]:checked"),function(cindex,crow){
                    accurate_row.push($(crow).closest("tr").index());
                })
                let accurate_col = [];
                $('#thshow th').each(function(e_index,e_e) {
                    if($(e_e).attr("data-flag")==1) accurate_col.push(e_index);
                });
                if(accurate_row.length!=0 || accurate_col!=0) search_choose_flag = 0;
                let flag=0;
                if(search_content_input && search_content_input.length != 0){
                    $.each(search_content_input,function(i_index,i_e){
                        if(($(i_e).text()).substring(0,length)==search_content){
                            if(search_choose_flag) flag=1;
                            else{
                                if(accurate_row.length!=0 && accurate_col!=0){
                                    if($.inArray($(this).parents("tr").find("td").index($(this)),accurate_col)!=-1 && $.inArray($(this).closest("tr").index(),accurate_row)!=-1) flag=1;;
                                }else if(accurate_row.length!=0){
                                    if($.inArray($(this).closest("tr").index(),accurate_row)!=-1) flag=1;
                                }else if($.inArray($(this).parents("tr").find("td").index($(this)),accurate_col)!=-1) flag=1;
                            }
                        }
                        if(flag){
                            var value = $(i_e).text();
                            value = value.replace($("#search_input").val(),replace_content);
                            $(i_e).text(value);
                        }
                        flag=0;
                    });
                }
                if(search_content_input[height]) search_content_input[height].classList.remove("input_height_light");
                height = 0;
                search_content_input = [];
            });
            $(document).on('click', "#content_accurate_replace", function(e){
                var search_content = $("#search_input").val();
                search_content = search_content.replace('\\','\\\\');
                var replace_content = $("#replace_input").val();
                if(!search_content){
                    layer.msg("不存在查找字符！");
                    return false;
                }
                search_content_input = $("#tbshow tr td:contains('"+search_content+"')");
                var search_choose_flag = 1;
                //找到勾选列和勾选行 一旦找到 search_choose_flag为0
                let accurate_row = [];
                $.each($("#tbshow tr td [type=checkbox]:checked"),function(cindex,crow){
                    accurate_row.push($(crow).closest("tr").index());
                })
                let accurate_col = [];
                $('#thshow th').each(function(e_index,e_e) {
                    if($(e_e).attr("data-flag")==1) accurate_col.push(e_index);
                });
                if(accurate_row.length!=0 || accurate_col!=0) search_choose_flag = 0;
                let flag=0;
                if(search_content_input && search_content_input.length != 0){
                    $.each(search_content_input,function(i_index,i_e){
                        if($(i_e).text()==search_content){
                            if(search_choose_flag) flag=1;
                            else{
                                if(accurate_row.length!=0 && accurate_col!=0){
                                    if($.inArray($(this).parents("tr").find("td").index($(this)),accurate_col)!=-1 && $.inArray($(this).closest("tr").index(),accurate_row)!=-1) flag=1;
                                }else if(accurate_row.length!=0){
                                    if($.inArray($(this).closest("tr").index(),accurate_row)!=-1) flag=1;
                                }else if($.inArray($(this).parents("tr").find("td").index($(this)),accurate_col)!=-1) flag=1;
                            }
                        }
                        if(flag) $(i_e).text(replace_content);
                        flag=0;
                    });
                }
                if(search_content_input[height]) search_content_input[height].classList.remove("input_height_light");
                height = 0;
                search_content_input = [];
            });
            $('#tbshow').on('keyup','td',function () {
                var index = $(this).index();
                var value = $(this).text();
                if(index == 5){
                    var first_string = value.substring(0,1);
                    var itorch = 0;
                    if(first_string=='∠'){
                        var localwz = value.lastIndexOf("*");
                        itorch = localwz==-1?0:value.substring(localwz+1);
                    }else if(first_string=="-"){
                        itorch = value.substring(1);
                    }
                    $(this).parent("tr").find("td:eq(8)").text(itorch);
                }
                if(index == 9){
                    if(value.indexOf(".")>=0) $(this).parent('tr').addClass("wrong");
                    else $(this).parent('tr').removeClass("wrong");
                }
                
            })
            $(document).on('click', "#refresh", function(e){
                window.location.reload();
            });
            $(document).on("click", "#export", function () {
                window.open('/admin.php/chain/lofting/base_library/export/ids/'+Config.ids);
            });
            $(document).on('click', "#import", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["60%","40%"],
                    callback:function(value){
                        if(value == 1) window.location.reload();
                        else layer.msg("导入失败"); 
                    }
                };
                Fast.api.open('chain/lofting/base_library/importView/ids/'+Config.ids,"导入放样部件",options);
            });
            $(document).on("click","#edit-number", function(e){
                var searchNum = $("#searchNum").val();
                var symbol = $("#symbol").val();
                var num = $("#edit-num").val();
                num = (Number)(num);
                if(num <= 0){
                    layer.msg("数量不可以小于1");
                    return false;
                }
                $('#show #tbshow tr').each(function(index,e) {
                    if($.inArray($(e).find("td:eq(2)").text(),searchNum)!==-1){
                        var base_num = $(e).find("td:eq(9)").text();
                        base_num = (parseFloat)(base_num);
                        var change_num = 0;
                        switch (symbol){
                            case "/": change_num = (base_num/num).toFixed(2);break;
                            case "+": change_num = base_num+num;break;
                            case "-": change_num = base_num-num;break;
                            case "*": change_num = (base_num*num).toFixed(2);break;
                        }
                        if(change_num == Math.floor(change_num)) change_num = Math.floor(change_num);
                        change_num += "";
                        if(change_num.indexOf(".")>=0) $(this).addClass("wrong");
                        else $(this).removeClass("wrong");
                        $(e).find("td:eq(9)").text((Number)(change_num));
                    }
                });
                
            });
        },
        print: function() {
            // console.log('row', Config.row);
            console.log('list', Config.list);
           
            let row = Config.row;
            let duanArrObj = Config.list.reduce((pre, cur) => {
                if(!pre[cur['DtS_Name']]) pre[cur['DtS_Name']]=[];
                pre[cur['DtS_Name']].push(cur);
                return pre;
            }, {})
            let duanArr = [];
            for (const key in duanArrObj) {
                if (Object.hasOwnProperty.call(duanArrObj, key)) {
                    const ele = duanArrObj[key];
                    let countInfo = ele.reduce((pre, cur) => {
                        pre.knumber += cur['DtMD_iUnitHoleCount']*100*cur['DtMD_iUnitCount'];
                        pre.number += cur['DtMD_iUnitCount']*100;
                        pre.weight += cur['DtMD_fUnitWeight']*100*cur['DtMD_iUnitCount'];
                        return pre;
                    }, {
                        knumber: 0,
                        number: 0,
                        weight: 0
                    })
                    let duan = {
                        mainInfos: {
                            ...row,
                            DtS_sWriter: row['DtM_sAuditor'],
                            DtS_Name: ele[0]['DtS_Name'],
                            knumber: countInfo.knumber/100,
                            number: countInfo.number/100,
                            weight: countInfo.weight/100
                        },
                        detailArr: ele
                    }
                    duanArr.push(duan);
                }
            }
            // console.log('duanArr', duanArr);
            // let duanArr = Config.duanArr;
            
            let printDataArr = duanArr.map((e) => {
                let detailArr=e.detailArr;
                let mainInfos = e.mainInfos;

                let tb_tmp = detailArr.map((d) => {
                    return {
                        'ljbh':d['DtMD_sPartsID'], //零件编号
                        'clmc':d['DtMD_sStuff'], //材料名称
                        'cz':d['DtMD_sMaterial']=="Q235B"?"":d['DtMD_sMaterial'],//材质
                        'gg':d['DtMD_sSpecification'],//规格
                        'cd':d['DtMD_iLength'],//长度
                        'kd':d['DtMD_fWidth'],//宽度
                        'DtMD_iTorch': d['DtMD_iTorch'] || '',
                        'type': d['type'],
                        'djsl':d['DtMD_iUnitCount'],//总数量
                        'djzl':Number(d['DtMD_fUnitWeight']).toFixed(1),//重量（kg）
                        'ks':d['DtMD_iUnitHoleCount'],//单件孔数
                        'dh':Number(d['DtMD_iWelding']) || '',//电焊
                        'wq':Number(d['DtMD_iFireBending']) || '',//弯曲
                        'qj':Number(d['DtMD_iCuttingAngle']) || '',//切角
                        'cb':Number(d['DtMD_fBackOff']) || '',//铲背
                        'qg':Number(d['DtMD_iBackGouging']) || '',//清根
                        'db':Number(d['DtMD_DaBian']) || '',//打扁
                        'khj':Number(d['DtMD_KaiHeJiao']) || '',//开合角
                        'zk':Number(d['DtMD_ZuanKong']) ||'',//钻孔
                        'DtMD_GeHuo': Number(d['DtMD_GeHuo']) || '',
                        'bz':d['DtMD_sRemark'],//备注
                    }
                })
                tb_tmp.push({
                    'ljbh':'', //零件编号
                    
                    'cz':'',//材质
                    'gg':'共'+detailArr.length+'项明细',//规格
                    'cd':'',//长度
                    'kd':'',//宽度
                    'djsl':'',//总数量
                    'djzl':'',//重量（kg）
                    'ks':'',//单件孔数
                    'dh':'',//电焊
                    'wq':'',//弯曲
                    'qj':'',//切角
                    'cb':'',//铲背
                    'qg':'',//清根
                    'db':'',//打扁
                    'khj':'',//开合角
                    'zk':'',//钻孔
                    'DtMD_GeHuo': '',
                    'bz':'',//备注
                })

                return {
                    tx:mainInfos['DtM_sTypeName'],
                    dw:mainInfos['DtS_Name'],
                    js:'1',
                    rq:mainInfos['DtM_dTime'],
                    zb:mainInfos['DtS_sWriter'],
                    sh:mainInfos['DtS_sAuditor'],
                    ljzsl:mainInfos['number'],
                    ljzzl:mainInfos['weight'],
                    ljzks:mainInfos['knumber'],
                    tb: tb_tmp
                }
            })

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
            
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: txjckdata});
            // console.log('dataArr', dataArr);
            $('#p_mx1').html(htemp.getHtml(printDataArr));
            $("#handleprintmx1").click(function(){
                htemp.print(printDataArr);
            });
            
            pageto('#p_mx1',0);

            let pLengthArr = Array.from($('#p_mx1 .hiprint-printPanel .hiprint-printPaper')).map(e => $(e).find("td[field='ks']").length);
            
            let newPrintDataArr = [];
            printDataArr.map(e=>{
                let tempArr = [];
                let sum = 0;
                
                while(pLengthArr.length>0 && sum<e.tb.length){
                    let pageSize = pLengthArr.shift();
                    let newTb = e.tb.slice(sum, sum+pageSize);

                    sum=sum + pageSize;
                    
                    let countInfo = newTb.reduce((pre, cur) => {
                        pre['djsl']+=cur.djsl*100;
                        pre['djzl']+=cur.djzl*100;
                        pre['ks']+=cur.ks*100;

                        return pre;
                    }, {
                        djsl: 0,
                        djzl: 0,
                        ks: 0
                    })

                    let newPrintData = {...e};
                    newPrintData.tb = newTb;
                    
                    newPrintData['byzks'] = countInfo['ks']/100;
                    newPrintData['byzsl'] = countInfo['djsl']/100;
                    newPrintData['byzzl'] = countInfo['djzl']/100;

                    tempArr.push(newPrintData);
                }

                for (let i = 0; i < tempArr.length; i++) {
                    const temp = tempArr[i];
                    
                    newPrintDataArr.push(temp);
                }
            });

            for(let i=0;i<newPrintDataArr.length;i++){
                const temp = newPrintDataArr[i]; 
                temp['page'] = '第 '+(i+1)+' 页， 共 '+newPrintDataArr.length+' 页';
            }

            $('#p_mx1').html(htemp.getHtml(newPrintDataArr));
            $("#handleprintmx1").click(function(){
                htemp.print(newPrintDataArr);
            });
            
            pageto('#p_mx1',0);
            
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data; 
                    }else if(field=="openedit"){
                        return false;
                    }else{
                        window.location.reload();
                    }
                    return false;
                });
                $(document).on('click', "#refresh", function(e){
                    window.location.reload();
                });
                $(document).on('click', "#import", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["60%","40%"],
                        callback:function(value){
                            if(value == 1) window.location.reload();
                            else layer.msg("导入失败"); 
                        }
                    };
                    Fast.api.open('chain/lofting/base_library/importView/ids/'+Config.ids,"导入放样部件",options);
                });
                $(document).on('click', "#open", function(e){
                    window.top.Fast.api.open('chain/lofting/base_library/openEdit/ids/' + Config.ids, Config.task_name, {
                        area: ["100%", "100%"]
                    });
                });
                $(document).on('click', "#openTarget", function(e){
                    window.open('/admin.php/chain/lofting/base_library/openEdit/ids/' + Config.ids);
                });
                $(document).on("click", "#export", function () {
                    window.open('/admin.php/chain/lofting/base_library/export/ids/'+Config.ids);
                });
                $(document).on("click", "#allchoose", function () {
                    var flag = $(this).prop("checked");
                    $("#tbshow").find("tr").each(function () {
                        var checkedField = $(this).children('td').eq(0).find('input');
                        if(flag) checkedField.prop("checked","true");
                        else checkedField.prop("checked","");
                    });
                });
            }
        }
    };
    return Controller;
});