define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    ids = typeof ids =='undefined'?"":ids;
    //显示加载数据
    function ShowDiv() {
        $("#loading").show();
    }
    
    //隐藏加载数据
    function HiddenDiv() {
        $("#loading").hide();
    }
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/component_details/index' + location.search,
                    edit_url: 'chain/lofting/component_details/edit',
                    del_url: 'chain/lofting/component_details/del',
                    multi_url: 'chain/lofting/component_details/multi',
                    import_url: 'chain/lofting/component_details/import',
                    table: 'tasksect',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TD_ID',
                sortName: 'TD_ID',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'state', title: __('state'), operate: 'LIKE'},
                        {field: 'C_Num', title: __('C_Num'), operate: false},
                        {field: 't.C_Num', title: __('C_Num'), operate: "LIKE", visible:false},
                        {field: 'PC_Num', title: __('PC_Num'), operate: false},
                        {field: 'c.PC_Num', title: __('PC_Num'), operate: "LIKE", visible:false},
                        {field: 'T_Num', title: __('T_Num'), operate: false},
                        {field: 't.T_Num', title: __('T_Num'), operate: "LIKE", visible:false},
                        {field: 'T_Sort', title: __('T_Sort'), operate: false},
                        {field: 'T_Company', title: __('T_Company'), operate: false},
                        {field: 'T_WriterDate', title: __('T_WriterDate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 't_project', title: __('t_project'), operate: 'LIKE'},
                        {field: 'TD_TypeName', title: __('TD_TypeName'), operate: 'LIKE'},
                        // {field: 'TD_TypeNameGY', title: __('TD_TypeNameGY'), operate: false},
                        {field: 'TD_Pressure', title: __('TD_Pressure'), operate:'LIKE'},
                        {field: 'TD_Count', title: '总数', operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Controller.api.bindevent();
            liandong();
            var deal = $("#deal");
            ShowDiv();
            fieldBootTable();
            $(document).on("change", "select[name='height']", function(e){
                var num = $(this).find("option:selected").val();
                liandong(num,'');
            });
            $(document).on("change", "select[name='tpNum']", function(e){
                var height = $("select[name='height']").find("option:selected").val();
                var towerNum = $(this).find("option:selected").val();
                liandong(height,towerNum);
            });
            $(document).on("click", ".lookfor", function(e){
                ShowDiv();
                var height = $("select[name='height']").find("option:selected").val();
                var towerNum = $("select[name='tpNum']").find("option:selected").val();
                var part = $("select[name='part']").find("option:selected").val();
                deal.bootstrapTable('destroy');
                var select_search = get_select_list();
                fieldBootTable(height,towerNum,part,select_search);
            });
            function fieldBootTable(height='',towerNum='',part='',select_search=[]){
                $.ajax({
                    url: 'chain/lofting/component_details/tableContent',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {td_id: ids,height:height,towerNum:towerNum,part:part,select_search:select_search},
                    complete: function () {
                        HiddenDiv();
                    },
                    success: function (ret) {
                        var tableContent = {};
                        if (ret.code === 1) {
                            tableContent = ret.data;
                            $("#allCountCount").text(ret.sum.count);
                            $("#allCountNumber").text(ret.sum.number);
                            $("#allCountWeight").text(ret.sum.weight);
                            $("#DtMD_sMaterial_select").html(ret.select_list.DtMD_sMaterial_select);
                            $("#DtMD_sSpecification_select").html(ret.select_list.DtMD_sSpecification_select);
                        }
                        // 初始化表格
                        deal.bootstrapTable({
                            data: tableContent,
                            search: false,
                            pagination: false,
                            height: 600,
                            columns: [
                                [
                                    {field: 'TH_Height', title: '呼高', sortable: true},
                                    {field: 'TP_SectName', title: '段位名', sortable: true},
                                    {field: 'TP_PartsID', title: '部件号', sortable: true},
                                    {field: 'DtMD_sMaterial', title: '材质', sortable: true},
                                    {field: 'DtMD_sStuff', title: '材料', sortable: true},
                                    {field: 'DtMD_sSpecification', title: '规格', sortable: true},
                                    {field: 'DtMD_iLength', title: '长度(mm)', sortable: true},
                                    {field: 'DtMD_fWidth', title: '宽度(mm)', sortable: true},
                                    {field: 'DtMD_iTorch', title: '厚度(mm)', sortable: true},
                                    {field: 'type', title: '类型', sortable: true},
                                    {field: 'TP_UnitCount', title: '单件数量', sortable: true},
                                    {field: 'TP_PackCount', title: '包装数量', sortable: true},
                                    {field: 'TP_SumCount', title: '总数量', sortable: true},
                                    {field: 'DtMD_iUnitCount', title: '单段数量', sortable: true},
                                    {field: 'TP_UnitWeight', title: '单件重量(kg)', sortable: true},
                                    {field: 'TP_SumWeight', title: '总重量(kg)', sortable: true},
                                    {field: 'DtMD_sRemark', title: '备注', sortable: true},
                                    {field: 'DtMD_iWelding', title: '电焊', sortable: true},
                                    {field: 'DtMD_iFireBending', title: '制弯', sortable: true},
                                    {field: 'DtMD_iCuttingAngle', title: '切角', sortable: true},
                                    {field: 'DtMD_fBackOff', title: '铲背', sortable: true},
                                    {field: 'DtMD_iBackGouging', title: '清根', sortable: true},
                                    {field: 'DtMD_DaBian', title: '打扁', sortable: true},
                                    {field: 'DtMD_KaiHeJiao', title: '开合角', sortable: true},
                                    {field: 'DtMD_GeHuo', title: '割豁', sortable: true},
                                    {field: 'DtMD_ZuanKong', title: '钻孔', sortable: true},
                                    // {field: 'DtMD_ISZhuanYong', title: '是否专用', sortable: true},
                                    // {field: 'DtMD_sType', title: '类型'},
                                ]
                            ]
                        });
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            function liandong(height="",towerNum=""){
                $.ajax({
                    url: 'chain/lofting/component_details/heightLinkage',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {td_id: ids,height:height,towerNum:towerNum},
                    success: function (ret) {
                        if (ret.code === 1) {
                            $("#th_height").html(ret.data['height']);
                            $("#scd_tpnum").html(ret.data['tpNum']);
                            $("#chooseDuan").html(ret.data['part']);
                            $("#sectionDetail").html(ret.data['fieldList']);
                        }else{
                            layer.msg(ret.msg);
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            $(document).on("click", ".select_lookfor", function(e){
                var select_search = get_select_list();
                deal.bootstrapTable('destroy');
                fieldBootTable('','','',select_search);
            });
            function get_select_list(){
                let select_search = [];
                var TP_PartsID = $("#c-TP_PartsID").val();
                var DtMD_sStuff = $("#c-DtMD_sStuff").val();
                var DtMD_sMaterial = $("#c-DtMD_sMaterial").val();
                var DtMD_sSpecification = $("#c-DtMD_sSpecification").val();
                var DtMD_iWelding = $("#c-DtMD_iWelding").val();
                if(TP_PartsID) select_search.push({["TP_PartsID"]: TP_PartsID});
                if(DtMD_sStuff) select_search.push({["DtMD_sStuff"]: DtMD_sStuff});
                if(DtMD_sMaterial) select_search.push({["DtMD_sMaterial"]: DtMD_sMaterial});
                if(DtMD_sSpecification) select_search.push({["DtMD_sSpecification"]: DtMD_sSpecification});
                if(DtMD_iWelding) select_search.push({["DtMD_iWelding"]: DtMD_iWelding});
                return select_search;
            }
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                // $(document).on("click", "#selectCustomer", function () {
                //     var url = "chain/sale/project_cate_log/selectCustomer";
                //     var options = {
                //         shadeClose: false,
                //         shade: [0.3, '#393D49'],
                //         area: ["80%","100%"],
                //         callback:function(value){
                //             $("input[name='row[C_Name]']").val(value.C_Name);
                //             $("input[name='row[PC_Contactor]']").val(value.C_C_Contacter1);
                //             $("input[name='row[PC_ContactTel]']").val(value.C_C_Tel);
                //             $("input[name='row[PC_Fax]']").val(value.C_C_Fax);
                //             $("input[name='row[PC_SalesMan]']").val(value.C_C_SalesMan);
                //             $("select[name='row[PC_Place]']").val(value.AD_Name);
                //             console.log($("select[name='row[PC_Place]']"));
                //             // CallBackFun(value);//在回调函数里可以调用你的业务代码实现前端的各种逻辑和效果
                //         }
                //     };
                //     Fast.api.open(url,"客户选择",options);
                // });
                // $(document).on("click", "#selectSale", function () {
                //     var url = "chain/sale/project_cate_log/selectSaleMan";
                //     var options = {
                //         shadeClose: false,
                //         shade: [0.3, '#393D49'],
                //         area: ["80%","100%"],
                //         callback:function(value){
                //             $("input[name='row[PC_SalesMan]']").val(value.E_Name);
                //         }
                //     };
                //     Fast.api.open(url,"员工选择",options);
                // });
            }
        }
    };
    return Controller;
});