define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    //隐藏加载数据
    function HiddenDiv() {
        $("#loading").hide();
    }
    ids = typeof ids == 'undefined' ? "" : ids;
    drawTableContent = typeof drawTableContent == 'undefined' ? "" : drawTableContent;
    leftTableContent = typeof leftTableContent == 'undefined' ? "" : leftTableContent;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/dh_cooperatek/index' + location.search,
                    add_url: 'chain/lofting/dh_cooperatek/add',
                    edit_url: 'chain/lofting/dh_cooperatek/edit',
                    del_url: 'chain/lofting/dh_cooperatek/del',
                    multi_url: 'chain/lofting/dh_cooperatek/multi',
                    import_url: 'chain/lofting/dh_cooperatek/import',
                    table: 'dhcooperatek',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'DC_Num',
                sortName: 'DC_Num',
                sortOrder: 'DESC',
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DC_Num', title: __('Dc_num'), operate: false},
                        {field: 'DtM_sTypeName', title: __('DtM_sTypeName'), operate: 'LIKE'},
                        {field: 'DtM_sPressure', title: __('DtM_sPressure'), operate: false},
                        {field: 'DC_Editer', title: __('Dc_editer'), operate: false},
                        {field: 'DC_EditDate', title: __('Dc_editdate'), operate:false},
                        {field: 'DC_Memo', title: __('Dc_memo'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            $(document).on("click", "#table_add", function(e){
                var url = "chain/lofting/dh_cooperatek/selectTower";
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["80%","100%"],
                    callback:function(value){
                        $("#c-DtM_sTypeName").val(value.DtM_sTypeName);
                        $("#c-DtM_iID_PK").val(value.DtM_iID_PK);
                        $("#c-DtM_sPressure").val(value.DtM_sPressure);
                    }
                };
                Fast.api.open(url,"选择塔型",options);
            });
            // Form.api.bindevent($("form[role=form]"), function(data, ret){
            //     window.location.href = "edit/ids/"+data;
            // });
            Controller.api.bindevent("add");
        },
        selecttower: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/dh_cooperatek/selectTower' + location.search,
                    table: 'fytxk',
                }
            });

            var tableTower = $("#tableTower");

            // 初始化表格
            tableTower.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'DtM_iID_PK',
                sortName: 'DtM_iID_PK',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DtM_sTypeName', title: __('Dtm_stypename'), operate: 'LIKE'},
                        {field: 'DtM_sPressure', title: __('Dtm_spressure'), operate: 'LIKE'}
                    ]
                ]
            });
            
            // 为表格绑定事件
            Table.api.bindevent(tableTower);
            $(document).on("click", ".data-return", function () {
                var formData=tableTower.bootstrapTable('getAllSelections');
                Fast.api.close(formData[0]);
            });
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $(document).on("click", "#table_add", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    end:function(){
                        window.location.reload();
                    }
                };
                Fast.api.open('chain/lofting/dh_cooperatek/drawingAssemblyDetail/dc_num/'+ids,"电焊组件明细",options);
            });

            $(document).on("click", "#save-detail", function () {
                var filterData = $("#edit-detail").serializeArray();
                $.ajax({
                    url: 'chain/lofting/dh_cooperatek/saveEditDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(filterData)},
                    async: false,
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });


        },
        drawingassemblydetail: function () {
            
            Form.api.bindevent($("form[role=form]"),function(){
                clearLeft();
                return false;
            });
            var height = document.body.clientHeight - $("#filter-form").height() - 70;
            var rightTableFromDiv = document.body.clientHeight - $("#rightHead").height() - 105;
            $("#rightTableFrom").parent("div").height(rightTableFromDiv);
            var leftTable = $("#leftTable");
            // var rightTable = $("#rightTable");
            var rightDcdList = [];
            leftTableFunction();
            $(document).on("click", "#filter", function(e){
                leftTable.bootstrapTable('destroy');
                leftTableFunction();
            });
            $(document).on("change","select[name=DtS_Name]", function(e){
                leftTable.bootstrapTable('destroy');
                leftTableFunction();
            })
            rightTableFunction();
            //修改
            $(document).on("click", "#down", function(e){
                var choose = leftTable.bootstrapTable('getSelections');
                if(choose == []) return false;
                appendFun(choose);
            });
            leftTable.on('dbl-click-row.bs.table',function(row,$elemente){
                appendFun([$elemente]);
            });

            $(document).on('click', ".table_del", function(e){
                var index = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(index).find('td').eq(14).find('input').val().trim();
                $( e.target ).closest("tr").remove();
                rightDcdList = $.grep(rightDcdList, function( n ) {
                    return ( n != num );
                });
            });

            function appendFun(elemente){
                var dcd_id = $("input[name='DCD_ID']").val();
                var appendObject = '';
                $.each(elemente,function(index,ele){
                    if($.inArray(parseInt(ele["DtMD_ID_PK"]),rightDcdList)==-1){
                        if(rightDcdList.length == 0){
                            $("input[name='DCD_ID']").val(dcd_id);
                            $("#c-DCD_PartName").val(ele.DtS_Name);
                            $("#c-DCD_PartNum").val(ele.DtMD_sPartsID);
                            $("#c-DCD_Count").val(ele.DtMD_iUnitCount);
                        }
                        // appendObject += '<tr><td><input type="button" class="table_del" value="删除" readonly></td>';
                        appendObject += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger table_del"><i class="fa fa-trash"></i></a></td>';
                        appendObject += '<td><input name="DtMD_sPartsID" type="text" value="'+ele.DtMD_sPartsID+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DtMD_sStuff" type="text" value="'+ele.DtMD_sStuff+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DtMD_sMaterial" type="text" value="'+ele.DtMD_sMaterial+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DtMD_sSpecification" type="text" value="'+ele.DtMD_sSpecification+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DHS_Thick" type="text" value="'+ele.DtMD_iLength+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DHS_Height" type="text" value="'+ele.DtMD_fWidth+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DHS_Count" type="text" value="'+ele.DtMD_iUnitCount+'" class="small_input"></td>';
                        appendObject += '<td><input name="DtMD_fUnitWeight" type="text" value="'+(parseFloat)(ele.DtMD_fUnitWeight).toFixed(2)+'" class="small_input"></td>';
                        appendObject += '<td><input name="DHS_isM" type="text" value="0" class="small_input"></td>';
                        appendObject += '<td><input name="DHS_Length" type="text" value="0" class="small_input"></td>';
                        appendObject += '<td><input name="DHS_Memo" type="text" value="" class="small_input"></td>';
                        appendObject += '<td hidden><input name="DHS_ID" type="text" value="0" class="small_input"></td>';
                        appendObject += '<td hidden><input name="DCD_ID" type="text" value="'+dcd_id+'" class="small_input"></td>';
                        appendObject += '<td hidden><input name="DtMD_ID_PK" type="text" value="'+ele.DtMD_ID_PK+'" class="small_input"></td></tr>';
                        
                        rightDcdList.push(ele.DtMD_ID_PK);
                    }
                });
                $("#tbodyDetail").append(appendObject);
                
            }

            function rightTableFunction(){
                var filterData = $("#rightHead").serializeArray();
                $.ajax({
                    url: 'chain/lofting/dh_cooperatek/rightTable',
                    type: 'post',
                    dataType: 'json',
                    data: filterData,
                    async: false,
                    success: function (ret) {
                        var tableContent = '';
                        if (ret.code === 1) {
                            tableContent = ret.data;
                            rightDcdList = ret.rightDcdList;
                        }
                        $("#tbodyDetail").html(tableContent);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            function leftTableFunction(){
                var filterData = $("#filter-form").serializeArray();
                $.ajax({
                    url: 'chain/lofting/dh_cooperatek/leftTable',
                    type: 'post',
                    dataType: 'json',
                    data: filterData,
                    async: false,
                    complete: function () {
                        HiddenDiv();
                    },
                    success: function (ret) {
                        var tableContent = {};
                        if (ret.code === 1) {
                            tableContent = ret.data;
                        }
                        leftTable.bootstrapTable({
                            data: tableContent,
                            search: false,
                            pagination: false,
                            height: height,
                            columns: [
                                [
                                    {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                                    {checkbox: true},
                                    {field: 'DtS_Name', title: __('DtS_Name')},
                                    {field: 'DtMD_sPartsID', title: __('DtMD_sPartsID'),width:100},
                                    {field: 'DtMD_iUnitCount', title: __('DtMD_iUnitCount')},
                                    {field: 'DtMD_sMaterial', title: __('DtMD_sMaterial')},
                                    {field: 'DtMD_sSpecification', title: __('DtMD_sSpecification')},
                                    {field: 'DtMD_iLength', title: __('DtMD_iLength')},
                                    {field: 'DtMD_fWidth', title: __('DtMD_fWidth')},
                                    {field: 'DtMD_sRemark', title: __('DtMD_sRemark'),width:100},
                                    {field: 'DtMD_fUnitWeight', title: __('DtMD_fUnitWeight'),formatter: function(value,row,index){return parseFloat(value).toFixed(2)}},
                                    {field: 'DtMD_iUnitHoleCount', title: __('DtMD_iUnitHoleCount')},
                                    {field: 'DtMD_iWelding', title: __('DtMD_iWelding')},
                                    {field: 'DtMD_iCuttingAngle', title: __('DtMD_iCuttingAngle')},
                                    {field: 'DtMD_fBackOff', title: __('DtMD_fBackOff')},
                                    {field: 'DtMD_iFireBending', title: __('DtMD_iFireBending')},
                                    {field: 'DtMD_iPressed', title: __('DtMD_iPressed')},
                                    {field: 'DtMD_ISZhuanYong', title: __('DtMD_ISZhuanYong')},
                                    {field: 'DtMD_sStuff', title: __('DtMD_sStuff')}
                                ]
                            ]
                        });
                        Table.api.bindevent(leftTable);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
                
            }

            $(document).on("click", "#saveContent", function(e){
                var rightHead = $("#rightHead").serializeArray();
                // var memo = $("#rightHeadMemo").serializeArray();
                var tableContent = $("#rightTableFrom").serializeArray();
                // console.log(tableContent);return false;
                // var tableContent = rightTable.bootstrapTable('getData');
                // rightHead.push(memo[0]);
                var table_row = {row: rightHead, tableRow: tableContent};
                //记得修改
                $.ajax({
                    url: 'chain/lofting/dh_cooperatek/saveDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(table_row)},
                    async: false,
                    success: function (ret) {
                        if(ret.code == 1) {
                            // var flag = false;
                            if(ret.msg != "success"){
                                var index = layer.confirm(ret.msg, {
                                    btn: ['确定', '取消'],
                                }, function(data) {
                                    // flag = true;
                                    layer.close(index);
                                    $.ajax({
                                        url: 'chain/lofting/dh_cooperatek/sureSaveDetail',
                                        type: 'post',
                                        dataType: 'json',
                                        async: false,
                                        data: {list: ret.data['list'], tableList: ret.data['tableList']},
                                        success: function (ret) {
                                            if (ret.hasOwnProperty("code")) {
                                                Layer.msg(ret.msg);
                                                // if (ret.code === 1) {
                                                //     clearLeft();
                                                // }
                                            }else{
                                                Layer.msg(ret.msg);
                                            }
                                        }, error: function (e) {
                                            Backend.api.toastr.error(e.message);
                                        }
                                    })
                                })
                            }else{
                                // flag = true;
                                $.ajax({
                                    url: 'chain/lofting/dh_cooperatek/sureSaveDetail',
                                    type: 'post',
                                    dataType: 'json',
                                    async: false,
                                    data: {list: ret.data['list'], tableList: ret.data['tableList']},
                                    success: function (ret) {
                                        if (ret.hasOwnProperty("code")) {
                                            Layer.msg(ret.msg);
                                            // if (ret.code === 1) {
                                            //     clearLeft();
                                            // }
                                        }else{
                                            Layer.msg(ret.msg);
                                        }
                                    }, error: function (e) {
                                        Backend.api.toastr.error(e.message);
                                    }
                                })
                            }
                        }else{
                            layer.msg(ret.msg);
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
                
            });
            $(document).on("click", "#addContent", function(e){
                var right_name = $("#c-DCD_PartName").val();
                var left_name = $("select[name='DtS_Name'] option:selected").val();
                
                if(left_name!=0 && right_name!=left_name && right_name){
                    $("#c-change_name").val(right_name);
                    leftTable.bootstrapTable('destroy');
                    leftTableFunction();
                }
                clearLeft();
            });

            $(document).on("click", "#delContent", function(e){
                var DCD_ID = $("input[name='DCD_ID']").val();
                if(DCD_ID == false) layer.msg("删除失败，不存在");
                var index = layer.confirm('确定删除该段零部件组成明细吗？', {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: 'chain/lofting/dh_cooperatek/delDetail/DCD_ID/'+DCD_ID,
                        type: 'post',
                        dataType: 'json',
                        // data: {num: DCD_ID},
                        success: function (ret) {
                            layer.msg(ret.msg);
                            if(ret.code==1){
                                parent.location.reload();
                                // clearLeft();
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    
                    layer.close(index);
                })
            });
            function clearLeft(){
                $("input[name='DCD_ID']").val("");
                $("input[name='DCD_PartName']").val("");
                $("input[name='DCD_PartNum']").val("");
                $("input[name='DHS_Memo']").val("");
                $("input[name='DCD_Count']").val("");
                $("#tbodyDetail").html("");
                leftTable.bootstrapTable('uncheckAll');
                rightDcdList = [];
                appendObject = {};
            }
            

        },
        typelist: function () {
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                // return ret;
                // console.log(data,ret);
                Fast.api.close(data);
            });
        },
        memolist: function () {
            $(document).on("click", ".choosefast", function(e){
                var content = $("#c-DCD_Memo").val();
                var fast = $(this).text();
                if(content=='') content += fast;
                else content += ','+fast;
                $("#c-DCD_Memo").val(content);
            });
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                Fast.api.close(data);
            });
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="edit"){
                        window.location.reload();
                        return false;
                    }
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                        return false;
                    }
                    
                });
                var height_one = $("#part_one").height();
                var part = document.body.clientHeight - height_one;
                var height = part*0.7;
                var edit_detail = part*0.25;
                $("#divTable").height(edit_detail);
                var deal = $("#deal");
                dealContent([{name:'ids',value:ids}]);
                deal.on('post-body.bs.table',function(){
                    $(".btn-dialog").data("area",["100%","100%"]);
                    $(".btn-small").data("area",["80%","50%"]);
                });
                Table.api.bindevent(deal);
                $(document).on("click", "#filter", function(e){
                    var filterData = $("#filter-form").serializeArray();
                    filterData.push({name:'ids',value:ids})
                    deal.bootstrapTable('destroy');
                    $("#divTable").hide();
                    dealContent(filterData);
                });
                var timer=null;
                deal.on('click-row.bs.table',function(row,$elemente){
                    clearTimeout(timer);
                    timer=setTimeout(function(){
                        $("#divTable").show();
                        $.ajax({
                            url: 'chain/lofting/dh_cooperatek/detailDrawing',
                            type: 'post',
                            dataType: 'json',
                            data: {DCD_ID:$elemente.DCD_ID},
                            success: function (ret) {
                                var tableContent = '';
                                if (ret.code === 1) {
                                    tableContent = ret.data;
                                }
                                $("#show").html(tableContent);
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        })
                    },300); 
                });

                //双击行
                deal.on('dbl-click-row.bs.table',function(row,$elemente){
                    clearTimeout(timer);
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                    };
                    Fast.api.open('chain/lofting/dh_cooperatek/drawingAssemblyDetail/dc_num/'+$elemente.DC_Num+'/dcd/'+$elemente.DCD_ID,"电焊组件明细",options);
                });

                $(document).on("click", "#table_del", function(e){
                    var choose_detail = deal.bootstrapTable('getSelections');
                    if(choose_detail.length == 0) layer.msg("请选择删除内容！");
                    else{
                        var dcd_id_list = [];
                        $.each(choose_detail,function(index,e){
                            dcd_id_list.push(e.DCD_ID);
                        })
                        var DCD_ID = dcd_id_list.join(",");
                        var index = layer.confirm('确定删除该段零部件组成明细吗？', {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/lofting/dh_cooperatek/delDetail/DCD_ID/'+DCD_ID,
                                type: 'post',
                                dataType: 'json',
                                // data: {num: DCD_ID},
                                success: function (ret) {
                                    layer.msg(ret.msg);
                                    if(ret.code==1){
                                        parent.location.reload();
                                        // clearLeft();
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            
                            layer.close(index);
                        })
                    }
                    

                    // var filterData = $("#filter-form").serializeArray();
                    // filterData.push({name:'ids',value:ids})
                    // deal.bootstrapTable('destroy');
                    // $("#divTable").hide();
                    // dealContent(filterData);
                });
                function dealContent(filterData){
                    $.ajax({
                        url: 'chain/lofting/dh_cooperatek/weldingDrawing',
                        type: 'post',
                        dataType: 'json',
                        data: filterData,
                        idField: 'DCD_ID',
                                uniqueId: 'DCD_ID',
                        complete: function () {
                            HiddenDiv();
                        },
                        success: function (ret) {
                            
                            var tableContent = {};
                            if (ret.code === 1) {
                                tableContent = ret.data;
                            }
                            // 初始化表格
                            deal.bootstrapTable({
                                data: tableContent,
                                idField: 'DCD_ID',
                                uniqueId: "DCD_ID",
                                search: false,
                                // pagination: false,
                                height: height,
                                // clickToSelect: true,
                                // singleSelect: true,
                                columns: [
                                    [
                                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                                        {checkbox: true},
                                        {field: 'DCD_ID', title: 'DCD_ID', visible:false},
                                        // {field: 'operate', title: '焊图', table: deal, 
                                        //     buttons: [
                                        //         {
                                        //             name: 'detail',
                                        //             text: '',
                                        //             title: '电焊组件明细',
                                        //             classname: 'btn btn-xs btn-primary btn-dialog',
                                        //             icon: 'fa fa-cogs',
                                        //             url: 'chain/lofting/dh_cooperatek/drawingAssemblyDetail/dc_num/'+ids+'/dcd/{DCD_ID}',
                                        //         }
                                        //     ],
                                        //     events: Table.api.events.operate, 
                                        //     formatter: Table.api.formatter.operate},
                                        {field: 'operate', title: '删除',
                                            buttons: [
                                                {name: 'comfirmDel',
                                                    text: '',
                                                    title: '删除',
                                                    classname: 'btn btn-xs btn-danger btn-view btn-ajax',
                                                    icon: 'fa fa-trash',
                                                    url: 'chain/lofting/dh_cooperatek/delDetail/DCD_ID/{DCD_ID}',
                                                    success: function(){
                                                        window.location.reload();
                                                    },
                                                    refresh: true}
                                                ],
                                            table: deal, events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                                        {field: 'DCD_PartName', title: __('DCD_PartName')},
                                        {field: 'DCD_PartNum', title: __('DCD_PartNum')},
                                        {field: 'DCD_Count', title: __('DCD_Count')},
                                        {field: 'DCD_SWeight', title: __('DCD_SWeight')},
                                        // {field: 'operate', title: '类别',
                                        //     buttons: [
                                        //         {   name: 'type',
                                        //             text: '',
                                        //             title: '修改',
                                        //             classname: 'btn btn-xs btn-primary btn-dialog btn-small',
                                        //             icon: 'fa fa-list',
                                        //             url: 'chain/lofting/dh_cooperatek/typeList/DCD_ID/{DCD_ID}',
                                        //             // refresh:true
                                        //             // success: function(data,index,e){
                                        //             //     console.log(data,index,e);
                                        //             // }
                                        //             callback:function(data){
                                        //                 // console.log(data.DCD_Type,data.DCD_ID);
                                        //                 // var id = data.DCD_ID;
                                        //                 // console.log(id);
                                        //                 // var content = deal.bootstrapTable('getRowByUniqueId', id);
                                        //                 // console.log(content);
                                        //                 // deal.bootstrapTable('updateRow', {index:content["id"],row:content});
                                        //                 // var index = parent.Layer.getFrameIndex(window.name);
                                        //                 // var dialogRowIndex = parent.$("#layui-layer" + index).data("dialogRowIndex");
                                        //                 window.location.reload();
                                        //                 // deal.bootstrapTable('updateCell', {index:1,field:"DCD_Type",value:1})
                                        //             }
                                        //         }],
                                        //     table: deal, events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                                        // {field: 'DCD_Type', title: __('DCD_Type')},
                                        // {field: 'DCD_Memo', title: __('DCD_Memo')},
                                        {field: 'DCD_Type_Input', title: __('DCD_Type')},
                                        {field: 'Writer', title: __('Writer')},
                                        {field: 'WriteDate', title: __('WriteDate')},
                                        // {field: 'Auditor', title: __('Auditor')},
                                        // {field: 'AuditDate', title: __('AuditDate')},
                                        // {field: 'operate', title: '备注',
                                        //     buttons: [
                                        //         {name: 'memo',
                                        //             text: '',
                                        //             title: '修改',
                                        //             classname: 'btn btn-xs btn-primary btn-dialog btn-small',
                                        //             icon: 'fa fa-list',
                                        //             url: 'chain/lofting/dh_cooperatek/memoList/DCD_ID/{DCD_ID}',
                                        //             callback:function(data){
                                        //                 window.location.reload();
                                        //                 // deal.bootstrapTable('updateCell', {index:1,field:"DCD_Memo",value:data})
                                        //             }
                                        //         }],
                                        //     table: deal, events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                                        {field: 'DCD_Memo_Input', title: __('DCD_Memo')}
                                    ]
                                ]
                            });
                            Table.api.bindevent(deal);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    })
                }
                // $(document).on("keyup", "input[name='DCD_Memo_Input']", function(e){
                    // var tr_index = $(this).parents("tr").index();
                    // var value = $(this).val();
                    // deal.bootstrapTable('updateCell', {index:tr_index,field:"DCD_Memo",value:value});
                // });
                // $(document).on("keyup", "input[name='DCD_Type_Input']", function(e){
                    // var tr_index = $(this).parents("tr").index();
                    // var value = $(this).val();
                    // deal.bootstrapTable('updateCell', {index:tr_index,field:"DCD_Type",value:value});
                // });

                $(document).on("click", "#table_save", function(e){
                    var table_content = deal.bootstrapTable('getData');
                    $.each($("input[name='DCD_Memo_Input']"),function (mindex,mrow){
                        if($(mrow).val() || $("input[name='DCD_Type_Input']").eq(mindex).val()){
                            table_content[mindex]["DCD_Memo"] = $(mrow).val();
                            table_content[mindex]["DCD_Type"] = $("input[name='DCD_Type_Input']").eq(mindex).val();
                        }
                    })
                    if(table_content.length==0) layer.msg("保存失败");
                    else{
                        $.ajax({
                            url: 'chain/lofting/dh_cooperatek/saveDetailSect',
                            type: 'post',
                            dataType: 'json',
                            data: {data: JSON.stringify(table_content)},
                            success: function (ret) {
                                layer.msg(ret.msg);
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }
                });
                
            }
        }
        
    };
    return Controller;
});