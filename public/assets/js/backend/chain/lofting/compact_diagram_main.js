define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    tableField = typeof tableField=="undefined"?"":tableField;
    detailTableField = typeof detailTableField=="undefined"?"":detailTableField;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/compact_diagram_main/index' + location.search,
                    add_url: 'chain/lofting/compact_diagram_main/add',
                    edit_url: 'chain/lofting/compact_diagram_main/edit',
                    del_url: 'chain/lofting/compact_diagram_main/del',
                    multi_url: 'chain/lofting/compact_diagram_main/multi',
                    import_url: 'chain/lofting/compact_diagram_main/import',
                    table: 'compactdiagrammain',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'CDM_Num',
                sortName: 'CDM_Num',
                sortOrder: "DESC",
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CDM_Num', title: __('Cdm_num'), operate: 'LIKE'},
                        {field: 'td.T_Num', title: '任务单号', operate: 'LIKE'},
                        {field: 'c.PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 't.C_Num', title: '合同号', operate: 'LIKE'},
                        {field: 'TD_ID', title: __('Td_id'),visible: false,operate: false},
                        {field: 'CDM_CProject', title: __('Cdm_cproject'), operate: 'LIKE'},
                        {field: 't.t_shortproject', title: '工程简称', operate: false},
                        {field: 'CDM_TypeName', title: __('Cdm_typename'), operate: 'LIKE'},
                        {field: 'CDM_Pressure', title: __('Cdm_pressure'), operate: 'LIKE'},
                        {field: 'TD_Type', title: __('Td_type'), operate: false},
                        {field: 'Writer', title: __('Writer'),operate: false},
                        {field: 'WriteDate', title: __('Writedate'),operate: false},
                        {field: 'Auditor', title: __('Auditor'),operate: false},
                        {field: 'AuditeDate', title: __('Auditedate'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent('add');
            $(document).on('click', "#choose", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        $("#T_Num").val(value.T_Num);
                        $("#C_Num").val(value.C_Num);
                        $("#CDM_CProject").val(value.C_Project);
                        $("#t_shortproject").val(value.C_SortProject);
                        $("#TD_ID").val(value.TD_ID);
                        $("#CDM_TypeName").val(value.TD_TypeName);
                        $("#CDM_Pressure").val(value.TD_Pressure);
                    }
                };
                Fast.api.open('chain/lofting/compact_diagram_main/chooseTower',"选择塔型",options);
            })
        },
        edit: function () {
            Controller.api.bindevent('edit');
            var auditor = Config.auditor;
            var flag = judge_approve(auditor);
            $(document).on('click', "#table_add", function(){
                if(flag){
                    var field_append='<tr>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-success detail"><i class="fa fa-calendar"></i></a></td>';
                    for(var i=0;i<tableField.length;i++){
                        field_append += '<td '+tableField[i][6]+'><input class="small_input" type="'+tableField[i][2]+'" '+tableField[i][4]+' name="table_row['+tableField[i][1]+'][]" value="'+tableField[i][5]+'"></td>';
                    }
                    field_append += '</tr>';
                    $("#tbshow").append(field_append);
                }
            });
            $(document).on('click', "#copy", function(e){
                if(flag){
                    var index = layer.confirm("如果已经有存在的段，选择复制，将覆盖原来的段。你确定要复制塔型嘛？", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        openField();
                        layer.close(index);
                    })
                }
            });
            function openField(){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        layer.msg("复制中，请稍等！");
                        $.ajax({
                            url: 'chain/lofting/compact_diagram_main/copySect/ids/'+Config.ids,
                            type: 'post',
                            dataType: 'json',
                            data: {data: JSON.stringify(value.data),way: value.way},
                            async: false,
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    Layer.msg(ret.msg);
                                    if (ret.code === 1) {
                                        window.location.reload();
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }
                };
				Fast.api.open('chain/lofting/compact_diagram_main/downField/ids/'+Config.ids,"复制塔型",options);
            };
            $(document).on('click', "#import", function(e){
                if(flag){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["60%","40%"],
                        callback:function(value){
                            if(value == 1) window.location.reload();
                            else layer.msg("导入失败"); 
                        }
                    };
                    Fast.api.open('chain/lofting/compact_diagram_main/importView/ids/'+Config.ids,"导入放样部件",options);
                }
            });
            $(document).on('click', ".del", function(e){
                if(flag){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(2).find('input').val();
                    if(num!=0){
                        var index = layer.confirm("即将删除该段所有信息，确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/lofting/compact_diagram_main/delDetail',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                }
                
            });
            $(document).on('click', ".detail", function(e){
                var indexChoose = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(indexChoose).find("td input[name='table_row[CDS_ID][]']").val();
                if(num==0){
                    var index = layer.confirm("请先保存段", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        layer.close(index);
                    })
                }else{
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            window.location.reload();
                        }
                    };
                    Fast.api.open('chain/lofting/compact_diagram_main/detail/ids/'+num,"杆塔型部件明细",options);
                }
            });
            $(document).on('click', "#choose", function(e){
                if(flag){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#T_Num").val(value.T_Num);
                            $("#C_Num").val(value.C_Num);
                            $("#CDM_CProject").val(value.C_Project);
                            $("#t_shortproject").val(value.C_SortProject);
                            $("#TD_ID").val(value.TD_ID);
                            $("#CDM_TypeName").val(value.TD_TypeName);
                            $("#CDM_Pressure").val(value.TD_Pressure);
                        }
                    };
                    Fast.api.open('chain/lofting/compact_diagram_main/chooseTower',"选择塔型",options);
                }
            })

            function judge_approve(auditor='')
            {
                var judge_flag = true;
                if(auditor) judge_flag = false;
                return judge_flag;
            }

            $(document).on("click", "#author", function () {
                var num = $("#CDM_Num").val();
                if(num == false){
                    layer.confirm('没有获取到编码，请稍后重试');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/lofting/compact_diagram_main/auditor",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = $("#CDM_Num").val();
                if(num == false){
                    layer.confirm('没有获取到编码，请稍后重试');
                    return false;
                }
                check('确定弃审？',"chain/lofting/compact_diagram_main/giveUp",num);
                
            });
            function check(msg,url,num){
                // console.log(url,msg,num);return false;
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                window.location.reload();
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        choosetower: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/compact_diagram_main/chooseTower/type/'+Config.type + location.search,
                    table: 'taskdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TD_ID',
                sortName: 'TD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                sortOrder: 'DESC',
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'd.T_Num', title: '任务单(生产单)号', operate: 'LIKE',visible:false},
                        {field: 'T_Num', title: '任务单(生产单)号', operate: false},
                        {field: 'P_Name', title: '产品名称', operate: false},
                        {field: 'P_Num', title: '线路名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE',visible:false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'C_Project', title: '工程名称', operate: 'LIKE'},
                        {field: 'C_Company', title: '下发公司', operate: false},
                        {field: 'C_SortProject', title: '工程简写', operate: false},
                        {field: 'Customer_Name', title: '客户', operate: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: 'LIKE'},
                        {field: 'C_SaleMan', title: '业务员', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent[0]);

            });
        },
        importview: function () {
            Form.api.bindevent($("form[role=form]"),function(){
                parent.location.reload();
            });
        },
        downfield: function () {
            var height = document.body.clientHeight-($("#down-from").height()+$("#down-form").height());
            var fieldTable = $("#fieldTable");
            fieldTable.bootstrapTable({
                data: [],
                search: false,
                pagination: false,
                height: height,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'type', title: '类型', visible: false},
                        {field: 'CDS_ID', title: 'id',visible: false},
                        {field: 'CDS_Name', title: '段名'},
                    ]
                ]
            });
            
            Table.api.bindevent(fieldTable);
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data:[],
                clickToSelect: true,
                singleSelect: true,
                height:height,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CDM_Num', title: '图纸编号'},
                        {field: 'CDM_CProject', title: '工程名称'},
                        {field: 'CDM_TypeName', title: '塔型'},
                        {field: 'CDM_Pressure', title: '电压等级'},
                        {field: 'type', title: '复制类型',visible:false}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            
            table.on('check.bs.table',function(row,$e){
                var value = $("input[name='from']:checked").val()
                if(value == 'drawing'){
                    var searchData = {type:$e.type,num:$e.CDM_Num};
                    var columns = [
                        [
                            {checkbox: true},
                            {field: 'type', title: '类型', visible: false},
                            {field: 'CDS_ID', title: 'id',visible: false},
                            {field: 'CDS_Name', title: '段名'},
                        ]
                    ];
                }else{
                    var searchData = {type:$e.type,num:$e.DtM_iID_PK};
                    var columns = [
                        [
                            {checkbox: true},
                            {field: 'type', title: '类型', visible: false},
                            {field: 'DtS_ID_PK', title: 'id',visible: false},
                            {field: 'DtS_Name', title: '段名'},
                        ]
                    ];
                }
                $.ajax({
                    url: 'chain/lofting/compact_diagram_main/downDetailContent',
                    type: 'post',
                    dataType: 'json',
                    data: searchData,
                    success: function (ret) {
                        if(ret.code==1){
                            fieldTable.bootstrapTable("destroy");
                            // 初始化表格
                            fieldTable.bootstrapTable({
                                data:ret.data,
                                height:height,
                                columns: columns
                            });
                            Table.api.bindevent(fieldTable);
                        }else{
                            Layer.msg("请重试");
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            })
            $(document).on('click', "#copy", function(){
                var tableContent = fieldTable.bootstrapTable('getSelections');
                if(tableContent.length == 0){
                    layer.msg("请先选择");
                    return false;
                }
                var way = $("input[name='way']:checked").val();
                Fast.api.close({"data":tableContent,"way":way});
                
            });
            ajaxTowerFromData("chain/lofting/compact_diagram_main/downFieldList/type/drawing",{ids: Config.ids},typeColumns("drawing"));
            $(document).on('change', "input[name='from']", function(){
                var value = $(this).val();
                var url = "chain/lofting/compact_diagram_main/downFieldList/type/"+value;
                var dataSearch = $("#down-form").serializeArray();
                dataSearch[dataSearch.length] = {'name': 'ids', 'value': Config.ids};
                var columns = typeColumns(value);
                ajaxTowerFromData(url,dataSearch,columns);
            });

            $(document).on('click', "#search", function(){
                fieldTable.bootstrapTable('load',[]);
                var value = $("input[name='from']:checked").val()
                var url = "chain/lofting/compact_diagram_main/downFieldList/type/"+value;
                var dataSearch = $("#down-form").serializeArray();
                dataSearch[dataSearch.length] = {'name': 'ids', 'value': Config.ids};
                var columns = typeColumns(value);
                ajaxTowerFromData(url,dataSearch,columns);
            });

            function typeColumns(value='tower'){
                var data = [];
                if(value == "tower"){
                    data = [
                        [
                            {checkbox: true},
                            {field: 'DtM_sTypeName', title: '塔型'},
                            {field: 'DtM_sPictureNum', title: '图号'},
                            {field: 'DtM_sPressure', title: '电压等级'},
                            {field: 'type', title: '复制类型',visible:false}
                        ]
                    ];
                }else if(value == "project"){
                    data = [
                        [
                            {checkbox: true},
                            {field: 'T_Num', title: '任务单号'},
                            {field: 'DtM_sProject', title: '工程名称'},
                            {field: 'DtM_sTypeName', title: '塔型'},
                            {field: 'DtM_sPressure', title: '电压等级'},
                            {field: 'type', title: '复制类型',visible:false}
                        ]
                    ];
                }else{
                    data = [
                        [
                            {checkbox: true},
                            {field: 'CDM_Num', title: '图纸编号'},
                            {field: 'CDM_CProject', title: '工程名称'},
                            {field: 'CDM_TypeName', title: '塔型'},
                            {field: 'CDM_Pressure', title: '电压等级'},
                            {field: 'type', title: '复制类型',visible:false}
                        ]
                    ];
                }
                return data;
            }
            function ajaxTowerFromData(url='',dataSearch={},colums=[]){
                if(url == '') return false;
                // console.log(dataSearch);
                // return false;
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: dataSearch,
                    height: height,
                    success: function (ret) {
                        var content = [];
                        if(ret.code==1){
                            content = ret.data;
                            table.bootstrapTable("destroy");
                            // 初始化表格
                            table.bootstrapTable({
                                data:content,
                                clickToSelect: true,
                                singleSelect: true,
                                height:height,
                                columns: colums
                            });
                        }else{
                            Layer.msg("请重试");
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
                // 为表格绑定事件
                Table.api.bindevent(table);
            }
        },
        detail: function () {
            Controller.api.bindevent("detail");
            var nav_ini_index = localStorage.getItem('nav_index');
            if(nav_ini_index){
                $.each($(".nav-tabs li"),function(index,e){
                    $(e).removeClass("active");
                    if(index==nav_ini_index) $(e).addClass("active");
                });
                $.each($("#myTabContent div"),function(index,e){
                    $(e).removeClass("active in");
                    if(index==nav_ini_index) $(e).addClass("active in");
                });
            }
            var auditor = Config.auditor;
            var flag = judge_approve(auditor);
            // hide_function();
            var height = document.body.clientHeight-$("#topDiv").height()-50;
            $(".table-responsive").height(height);
            $(document).on('click', "#table_add", function(){
                if(flag){
                    var field_append='<tr>'
                            +'<td data-name=""><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                            +'<td data-name=""><input type="checkbox"></td>'
                            +'<td data-name="">'+Config.CDS_Name+'</td>';
                    for(var i=0;i<detailTableField.length;i++){
                        var class_name = "small_input";
                        field_append += '<td '+detailTableField[i][4]+' data-name="'+(detailTableField[i][4]=='hidden'?'':detailTableField[i][1])+'"><input class="'+class_name+'" type="'+detailTableField[i][2]+'" data-rule="'+detailTableField[i][3]+'" name="'+detailTableField[i][1]+'[]" value=""></td>';
                    }
                    field_append += '</tr>';
                    $("#tbshow").append(field_append);
                }
            });
            
            var thisIds = Config.CDS_ID;
            $(document).on('change', "#searchNum", function(){
                var lookIds = $("#searchNum").find("option:selected").val();
                if(lookIds==thisIds || lookIds=='') return false;
                else window.location.href = "edit/ids/"+lookIds;
            });
            $(document).on('click', ".btn-success", function(e){
                if(flag){
                    var formData = $("#detail-form").serializeArray();
                    searchDetail(formData);
                }
            });
            $(document).on('click', ".del", function(e){
                if(flag){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="CD_ID[]"]').val();
                    if(num!=''){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/lofting/compact_diagram_main/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                }
            });

            $('#tbshow').on('keyup','td input[name="DtMD_sMaterial[]"]',function () {
                var index = $(this).parents('tr').index();
                var DtMD_sPartsID = $(this).parents('tbody').find('tr').eq(index).find('td').eq(4).find('input').val().trim();
                if(DtMD_sPartsID != ""){
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td').eq(5).find('input').val().trim();
                    var list = ['Q235','Q355','Q420','Q235B','Q355B','Q420B','Q460B','HH'];
                    var field_name = list[num];
                    if(field_name !== undefined){
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(5).find('input').val(field_name);
                    }
                }
                
            })
            $('#tbshow').on('keyup','td input[name="DtMD_sStuff[]"]',function () {
                var index = $(this).parents('tr').index();
                var DtMD_sPartsID = $(this).parents('tbody').find('tr').eq(index).find('td').eq(5).find('input').val().trim();
                if(DtMD_sPartsID != ""){
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td').eq(6).find('input').val().trim();
                    var list = ['角钢','钢板','圆钢','槽钢','钢管','花纹钢板','格栅板','工字钢','扁钢','锥形管'];
                    var symbol = ['∠','-','Φ','[','Φ','-','G','I','-','Z'];
                    var field_name = list[num];
                    if(field_name !== undefined){
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(6).find('input').val(field_name);
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(7).find('input').val(symbol[num]);
                    }
                }
                
            })
            $(document).on('click','#edit-number',function () {
                if(flag){
                    var symbol = $("#symbol").val();
                    var num = $("#edit-num").val();
                    if(num <= 0){
                        layer.msg("数量不可以小于1");
                        return false;
                    }
                    var formData = $("#detail-form").serializeArray();
                    searchDetail(formData,symbol,num);
                }
            })
            function searchDetail(formData={},symbol="",num=""){
                $.ajax({
                    url: 'chain/lofting/compact_diagram_main/detail/ids/'+Config.CDS_ID,
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData),symbol:symbol, num:num},
                    success: function (ret) {
                        Layer.msg(ret.msg);
                        if (ret.code === 1) {
                            window.location.reload();
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            
            $(document).on('click', "#editB", function(){
                if(flag){
                    var type = "editB";
                    editDetailAll(thisIds,type);
                }
            });
            $(document).on('click', "#editC", function(){
                if(flag){
                    var type = "editC";
                    editDetailAll(thisIds,type);
                }
            });
            $(document).on('click', "#giveupB", function(){
                if(flag){
                    var type = "giveupB";
                    editDetailAll(thisIds,type);
                }
            });
            $(document).on('click', "#giveupC", function(){
                if(flag){
                    var type = "giveupC";
                    editDetailAll(thisIds,type);
                }
            });
            function editDetailAll(ids='',type=''){
                if(ids=='' || type==''){
                    layer.msg("请刷新后重试！");
                }else{
                    $.ajax({
                        url: 'chain/lofting/compact_diagram_main/editDetailAll',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {ids:ids,type:type},
                        success: function (ret) {
                            Layer.msg(ret.msg);
                            if (ret.code === 1) {
                                window.location.reload();
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }
                
            }

            $(document).on("click", "#all_add", function () {
                if(flag){
                    $("#tbshow").find("tr").each(function () {
                        var checkedField = $(this).children('td').eq(1).find('input');
                        checkedField.prop("checked","true");
                    });
                }
            });
            $(document).on("click", "#giveup_add", function () {
                if(flag){
                    $("#tbshow").find("tr").each(function () {
                        var checkedField = $(this).children('td').eq(1).find('input');
                        checkedField.prop("checked","");
                    });
                }
            });

            $(document).on('click', "#copy_add", function(){
                if(flag){
                    var copyChoose,text;
                    $("#tbshow").find("tr").each(function () {
                        if($(this).children('td').eq(1).find('input').prop("checked")){
                            $(this).children('td').eq(1).find('input').prop("checked","");
                            copyChoose = $(this).html();
                            text += "<tr style='background-color: #E0FFFF'>"+copyChoose.replace(/name\=\"CD\_ID\[\]\" value\=\"\d+\"/,'name="CD_ID[]" value=""')+"</tr>";
                        }
                    });
                    $("#tbshow").append(text);
                }
            });
            function judge_approve(auditor='')
            {
                var judge_flag = true;
                if(auditor) judge_flag = false;
                return judge_flag;
            }
            $(document).on('click', ".nav-tabs li", function(){
                localStorage.setItem('nav_index', $(this).index());
            });
        },
        api: {
            bindevent: function (field='edit') {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data;
                    }else if(field != "detail"){
                        window.location.reload();
                    }
                    return false;
                });
            }
        }
    };
    return Controller;
});