define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/piece_arrange/index' + location.search,
                    table: 'piece_arrange_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pt_num',
                sortName: 'pt_num',
                sortOrder: 'DESC',
                columns: [
                    [
                        {checkbox: true, rowspan: 2},
                        {field: 'record_time', title: '时间', rowspan: 2, visible:false, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'pt_num', title: '生产任务下达单', rowspan: 2, operate:false},
                        {field: 'pt.PT_Num', title: '生产任务下达单', rowspan: 2, visible:false, operate:'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', rowspan: 2, operate: 'LIKE'},
                        // {field: 'time', title: '时间', rowspan: 2, operate:'RANGE', addclass:'datetimerange', autocomplete:false, visible: false},
                        {field: 'normal', title: '普通下料', colspan: 3, operate: false},
                        {field: 'group', title: '制弯打扁开合角', colspan: 3, operate: false},
                        {field: 'welding', title: '电焊', colspan: 3, operate: false},
                        {field: 'cut', title: '切角', colspan: 3, operate: false},
                        {field: 'backhoe', title: '铲背', colspan: 3, operate: false},
                    ],
                    [
                        {field: 'll_normal', title: '理论', operate: false},
                        {field: 'sj_normal', title: '实际', operate: false},
                        {field: 'normal_detail', title: '详情', table: table, events: Table.api.events.operate, operate: false,
                         buttons: [
                            {
                                name: 'detail',
                                text: "详情",
                                title: "普通下料详情",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'chain/lofting/piece_arrange/detail/type/1',
                            }
                         ],
                         formatter: Table.api.formatter.buttons
                        },
                        {field: 'll_group', title: '理论', operate: false},
                        {field: 'sj_group', title: '实际', operate: false},
                        {field: 'group_detail', title: '详情', table: table, events: Table.api.events.operate, operate: false,
                         buttons: [
                            {
                                name: 'detail',
                                text: "详情",
                                title: "制弯打扁开合角详情",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'chain/lofting/piece_arrange/detail/type/11',
                            }
                         ],
                         formatter: Table.api.formatter.buttons
                        },
                        {field: 'll_welding', title: '理论', operate: false},
                        {field: 'sj_welding', title: '实际', operate: false},
                        {field: 'welding_detail', title: '详情', table: table, events: Table.api.events.operate, operate: false,
                         buttons: [
                            {
                                name: 'detail',
                                text: "详情",
                                title: "电焊详情",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'chain/lofting/piece_arrange/detail/type/2',
                            }
                         ],
                         formatter: Table.api.formatter.buttons
                        },
                        {field: 'll_cut', title: '理论', operate: false},
                        {field: 'sj_cut', title: '实际', operate: false},
                        {field: 'cut_detail', title: '详情', table: table, events: Table.api.events.operate, operate: false,
                         buttons: [
                            {
                                name: 'detail',
                                text: "详情",
                                title: "切角详情",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'chain/lofting/piece_arrange/detail/type/4',
                            }
                         ],
                         formatter: Table.api.formatter.buttons
                        },
                        {field: 'll_backhoe', title: '理论', operate: false},
                        {field: 'sj_backhoe', title: '实际', operate: false},
                        {field: 'backhoe_detail', title: '详情', table: table, events: Table.api.events.operate, operate: false,
                         buttons: [
                            {
                                name: 'detail',
                                text: "详情",
                                title: "铲背详情",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'chain/lofting/piece_arrange/detail/type/5',
                            }
                         ],
                         formatter: Table.api.formatter.buttons
                        },
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function ()
        {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                pk: 'id',
                idField: 'id',
                columns: [
                    [
                        {checkbox: true, rowspan: 2},
                        {field: 'id', title: 'ID', rowspan: 2, visible: false},
                        {field: 'pt_num', title: 'pt_num', rowspan: 2, visible: false},
                        {field: 'stuff', title: '材料', rowspan: 2},
                        {field: 'specification', title: '规格', rowspan: 2},
                        {field: 'material', title: '材质', rowspan: 2},
                        {field: 'weight', title: '总重量', colspan: 3},
                        {field: 'hole', title: '总孔数', colspan: 3},
                        {field: 'button', title: '详情', rowspan: 2, table: table, events: Table.api.events.operate,
                         buttons: [
                            {
                                name: 'timeline',
                                text: "时间线",
                                title: "时间线",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'chain/lofting/piece_arrange/timeline/content_id/{row.id}',
                                visible: function (row){
                                    return row['stuff']=='总计'?false:true;
                                }
                            },{
                                name: 'timeline',
                                text: "时间线",
                                title: "时间线",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'chain/lofting/piece_arrange/timelineSum/pt_num/{row.pt_num}/type/'+Config.type,
                                visible: function (row){
                                    return row['stuff']=='总计'?true:false;
                                }
                            }
                         ],
                         formatter: Table.api.formatter.buttons
                        },
                    ],
                    [
                        {field: 'sum_weight', title: '理论',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                        {field: 'use_sum_weight', title: '实际',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                        {field: 'surplus_sum_weight', title: '剩余',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                        {field: 'hole_number', title: '理论',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                        {field: 'use_hole_number', title: '实际',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                        {field: 'surplus_hole_number', title: '剩余',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        timeline: function () {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                pk: 'id',
                idField: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'stuff', title: '材料'},
                        {field: 'specification', title: '规格'},
                        {field: 'material', title: '材质'},
                        {field: 'sum_weight', title: '登记重量',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                        {field: 'hole_number', title: '登记孔数',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                        {field: 'record_time', title: '计件日期'},
                        {field: 'writer', title: '制单人'},
                        {field: 'create_time', title: '制单时间'},
                        {field: 'auditor', title: '审核人'},
                        {field: 'auditor_time', title: '审核时间'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        timelinesum: function () {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'stuff', title: '材料'},
                        {field: 'specification', title: '规格'},
                        {field: 'material', title: '材质'},
                        {field: 'sum_weight', title: '登记重量',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                        {field: 'hole_number', title: '登记孔数',formatter: function(value){ return (parseFloat(value).toFixed(2))}},
                        {field: 'record_time', title: '计件日期'},
                        {field: 'writer', title: '制单人'},
                        {field: 'create_time', title: '制单时间'},
                        {field: 'auditor', title: '审核人'},
                        {field: 'auditor_time', title: '审核时间'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});