define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/piece_main/index' + location.search,
                    add_url: 'chain/lofting/piece_main/add',
                    edit_url: 'chain/lofting/piece_main/edit',
                    del_url: 'chain/lofting/piece_main/del',
                    multi_url: 'chain/lofting/piece_main/multi',
                    import_url: 'chain/lofting/piece_main/import',
                    table: 'piece_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {
                            field: 'buttons',
                            width: "50px",
                            title: '计件',
                            table: table,
                            operate: false,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'print',
                                    text: '',
                                    title: '计件',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-plus-circle',
                                    url: 'chain/lofting/piece_main/pieceList/ids/{id}',
                                },
                                {
                                    name: 'editamount',
                                    text: '',
                                    title: '修改杂工',
                                    classname: 'btn btn-xs btn-info btn-dialog',
                                    icon: 'fa fa-plus-circle',
                                    url: 'chain/lofting/piece_main/editAmount/ids/{id}',
                                    extend: 'data-area=["80%","50%"]'
                                },
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'id', title: __('Id'),operate: false},
                        {field: 'pt_num', title: "任务下达单号",operate: "="},
                        {field: 'create_time', title: __('Create_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'record_time', title: __('Record_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'order', title: '工序', searchList: Config.orderArr,formatter:function(value,row,index){return Config.orderArr[value]}},
                        {field: 'device', title: __('Device'), searchList: Config.machine_arr,formatter:function(value,row,index){return Config.machine_arr[value]}},
                        {field: 'zg_amount', title: '杂工工资',operate: false},
                        {field: 'zg_hours', title: '杂工小时',operate: false},
                        {field: 'writer', title: '制单人', operate: 'LIKE'},
                        {field: 'auditor', title: '审核人', operate: 'LIKE'},
                        {field: 'auditor_time', title: '审核时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $(document).on("click", "#updatePurchase", function () {
                $.ajax({
                    url: 'chain/lofting/piece_main/eipUpload',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {id: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        selectxd: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/piece_main/selectxd' + location.search,
                    table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PT_Num',
                sortName: 'AuditorDate',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {
                            field: 'buttons',
                            width: "120px",
                            title: '操作',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'gener',
                                    text: '生成',
                                    title: '生成计件',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-magic',
                                    url: 'chain/lofting/piece_main/genPiece',
                                    confirm: '确认生成计件？',
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg);
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    },
                                    error: function (data, ret) {
                                        console.log(data, ret);
                                        Layer.alert(ret.msg);
                                        return false;
                                    },
                                    visible: function (value){
                                        if(value['status']) return false;
                                        else return true;
                                    }
                                },
                                // {
                                //     name: 'update',
                                //     text: '更新',
                                //     title: '更新计件',
                                //     classname: 'btn btn-xs btn-info btn-magic btn-ajax',
                                //     icon: 'fa fa-magic',
                                //     url: 'chain/lofting/piece_main/updatePiece',
                                //     confirm: '确认更新计件？',
                                //     success: function (data, ret) {
                                //         Layer.alert(ret.msg);
                                //         //如果需要阻止成功提示，则必须使用return false;
                                //         //return false;
                                //     },
                                //     error: function (data, ret) {
                                //         console.log(data, ret);
                                //         Layer.alert(ret.msg);
                                //         return false;
                                //     },
                                //     visible: function (value){
                                //         if(value['status']) return true;
                                //         else return false;
                                //     }
                                // }
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'PT_Num', title: '下达单号', operate: false},
                        {field: 'p.PT_Num', title: '下达单号', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 'd.T_Num', title: '任务单号', operate: 'LIKE',visible: false},
                        {field: 'TD_ID', title: 'TD_ID', operate: false, visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'd.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'PT_Number', title: '基数', operate: false},
                        {field: 'C_ProjectName', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_Pressure', title: '电压等级', operate: 'LIKE'},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'},
                        {field: 'WriterDate', title: "制单时间", visible: false, defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else Fast.api.close(tableContent[0]);
            });
        },
        piecelist: function () {
            var edit_table_list = $("#tab_tower li");
            $.each(edit_table_list,function(index,item){
                var href_field = $(item).find("a").attr("href");
                jt_left_table = $('#left_'+href_field.substr(1)+'_table');
                jt_right_table = $('#right_'+href_field.substr(1)+'_table');
                // 初始化表格
                jt_right_table.bootstrapTable({
                    data:Config.list[href_field.substr(1)],
                    height: document.body.clientHeight-200,
                    uniqueId: "id",
                    columns: [
                        [
                            {checkbox: true},
                            {field: 'id', title: 'id'},
                            {
                                field: 'id',
                                title: '<a href="javascript:;" class="btn btn-success btn-xs btn-toggle" style="border-top:none;"><i class="fa fa-chevron-up"></i></a>',
                                operate: false,
                                formatter: Controller.api.formatter.subnode
                            },
                            {field: 'piece_detail_id', title: 'piece_detail_id',visible: false},
                            {field: 'pt_num', title: '下达单号'},
                            {field: 'parts_id', title: '部件号'},
                            {field: 'stuff', title: '材料'},
                            {field: 'specification', title: '规格'},
                            {field: 'material', title: '材质'},
                            {field: 'workmanship', title: '工序'},
                            {field: 'surplus_sum_weight', title: '登记重量'},
                            {field: 'surplus_hole_number', title: '登记孔数'},
                        ]
                    ],
                });
                // 为表格绑定事件
                Table.api.bindevent(jt_right_table);
                if(index==(edit_table_list.length-1)){
                    $('#tab_tower ul').find("li:eq("+index+")>a").trigger("click");
                }
            });
            //显示隐藏子节点
            $(document).on("click", ".btn-node-sub", function (e) {
                var status = $(this).data("shown") ? true : false;
                $(this).closest("table").find("a.btn[data-pid='" + $(this).data("id") + "']").each(function () {
                    $(this).closest("tr").toggle(!status);
                });
                $(this).data("shown", !status);
                return false;
            });
            //展开隐藏全部
            $(document.body).on("click", ".btn-toggle-all", function (e) {
                var that = this;
                var show = $("i", that).hasClass("fa-plus");
                $("i", that).toggleClass("fa-plus", !show);
                $("i", that).toggleClass("fa-minus", show);
                $(".btn-node-sub.disabled").closest("tr").toggle(show);
                $(".btn-node-sub").data("shown", show);
            });
        },
        selectpeople: function () {
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'operator', title: '操作者'},
                        {field: 'hours', title: "时间"},
                        {
                            field: 'buttons',
                            // width: "120px",
                            title: '操作',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'del',
                                    title: '删除1',
                                    classname: 'btn btn-xs btn-danger btn-click btn-del-click',
                                    icon: 'fa fa-trash',
                                    // url: 'example/bootstraptable/detail',
                                    // callback: function (data) {
                                    //     Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    // },
                                    // visible: function (row) {
                                    //     //返回true时按钮显示,返回false隐藏
                                    //     return true;
                                    // }
                                }
                            ],
                            formatter: Table.api.formatter.buttons
                        }
                    ]
                ],
                onClickCell: function(field, value, row, $element) {
                    if(field=='hours'){
                        $element.attr('contenteditable', true);
                        $element.attr('style', "display:block");
                        $element.blur(function() {
                            let index = $element.parent().data('index');
                            let tdValue = $element.html();
                            saveData(index, field, tdValue);
                        })
                    }
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            function saveData(index, field, value) {
                table.bootstrapTable('updateCell', {
                    index: index,
                    field: field,
                    value: value
                });
                return false;
            }
            $(document).on('click', ".btn-del-click", function(e){
                let index = $(this).parents('tr').index();
                let all_data = table.bootstrapTable('getData');
                all_data.splice(index,1);
                table.bootstrapTable('load',all_data);
            });
            
            $(document).on('click', "#sure", function(e){
                let all_data = table.bootstrapTable('getData');
                Fast.api.close(all_data);
            });
            
            $(document).on('click', "#selectPeople", function(e){
                var url = "chain/sale/project_cate_log/selectSaleMan/type/1";
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        let act_value = value[0];
                        table.bootstrapTable("append",{
                            "title": act_value.E_PerNum,
                            "operator": act_value.E_PerNum,
                            "hours": 8
                        });
                    }
                };
                Fast.api.open(url,"员工选择",options);
            });
        },
        editamount: function(){
            Form.api.bindevent($("form[role=form]"));
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }
                    return false;
                });
                $("#tab_tower").height(document.body.clientHeight-100);
                var PT_Num_list = Config.pt_list;
                var jt_left_table;
                var jt_right_table;
                var left_table_height = document.body.clientHeight-270;
                var right_table_height = document.body.clientHeight-200;
                var flag = Config.flag;

                
                $(document).on('change', "#c-device", function(e){
                    $("#c-operator").val("");
                    $(".sj_operator").text("");
                    // var pack_val = $(e.target).selectpicker('val');
                    // $.ajax({
                    //     url: 'chain/lofting/piece_main/selectMachine',
                    //     type: 'post',
                    //     dataType: 'json',
                    //     data: {num:pack_val},
                    //     success: function (ret) {
                    //         // if(ret.code==1){
                    //             $("#c-operator_recent").parent("div").html(ret.data);
                    //             $("#c-operator_recent").selectpicker("refresh")
                    //         // }
                    //         $("#c-operator").val("");
                            
                    //     }, error: function (e) {
                    //         Backend.api.toastr.error(e.message);
                    //     }
                    // });
                    
                });

                var edit_table_list = $("#tab_tower li");
                $.each(edit_table_list,function(index,item){
                    var href_field = $(item).find("a").attr("href");
                    jt_left_table = $('#left_'+href_field.substr(1)+'_table');
                    jt_right_table = $('#right_'+href_field.substr(1)+'_table');
                    // 初始化表格
                    jt_left_table.bootstrapTable({
                        data:[],
                        uniqueId: "id",
                        height: left_table_height,
                        columns: [
                            [
                                {checkbox: true},
                                {field: 'stuff', title: '材料'},
                                {field: 'specification', title: '规格'},
                                {field: 'material', title: '材质'},
                                {field: 'workmanship', title: '工序'},
                                {field: 'surplus_sum_weight_info', title: '剩余重量'},
                                {field: 'surplus_hole_number_info', title: '剩余孔数'},
                                {field: 'pt_num', title: '下达单号'},
                                {field: 'id', title: 'id'},
                                // {field: 'pid', title: 'pid',visible: false},
                                {
                                    field: 'id',
                                    title: '<a href="javascript:;" class="btn btn-success btn-xs btn-toggle" style="border-top:none;"><i class="fa fa-chevron-up"></i></a>',
                                    operate: false,
                                    formatter: Controller.api.formatter.subnode
                                },
                                {field: 'parts_id', title: '部件号'},
                                {field: 'piece_detail_id', title: 'piece_detail_id',visible: false}
                                
                            ]
                        ],
                        rowAttributes: function (row, index) {
                            return row.pid == 0 ? {} : {style: "display:none"};
                        }
                    });
                    // 为表格绑定事件
                    Table.api.bindevent(jt_left_table);
                    $.ajax({
                        url: 'chain/lofting/piece_main/getPieceList',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {num:href_field.substr(1)},
                        success: function (ret) {
                            var left_table_content = [];
                            if(ret.code == 1){
                                left_table_content = ret.data["list"];
                                $.each(ret.data["search"],function(index,item){
                                    $(href_field+" .leftPart").find(".search_field").eq(index).find("div").html(item);
                                });
                            }
                            jt_left_table.bootstrapTable('load',left_table_content);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    
                    jt_left_table.on('dbl-click-row.bs.table',function(row,$elemente){
                        if(!$elemente.pid && !jt_right_table.bootstrapTable('getRowByUniqueId', $elemente.id)){
                            var this_display_state = [];
                            jt_right_table.find("tbody tr").each(function(index,value){
                                this_display_state.push($(this).css("display"));
                            })
                            jt_left_table.bootstrapTable("uncheckAll");
                            jt_left_table.bootstrapTable("checkBy", {field:"id", values:[$elemente.id]});
                            jt_left_table.bootstrapTable("checkBy", {field:"pid", values:[$elemente.id]}); 
                            var right_table_content = jt_right_table.bootstrapTable("getData");
                            var table_content = jt_left_table.bootstrapTable("getAllSelections");
                            table_content = $.merge(right_table_content,table_content);
                            jt_right_table.bootstrapTable("destroy");
                            // 初始化表格
                            createtable(jt_right_table,table_content,right_table_height);
                            $.each(this_display_state,function(index,value){
                                var status = value=="none"?false:true;
                                jt_right_table.find("tbody tr").eq(index).toggle(status);
                            })
                            // 为表格绑定事件
                            Table.api.bindevent(jt_right_table);
                            jt_left_table.bootstrapTable("uncheckAll");
                            jt_right_table.bootstrapTable("uncheckAll");
                        }
                    });
                    // 初始化表格
                    createtable(jt_right_table,Config.list[href_field.substr(1)],right_table_height);
                    // 为表格绑定事件
                    Table.api.bindevent(jt_right_table);
                    if(index==(edit_table_list.length-1)){
                        $('#tab_tower ul').find("li:eq("+index+")>a").trigger("click");
                    }
                });
                $(document).on('click', "#selectxd", function(e){
                    if(!Config.ids){
                        layer.msg("请先保存主信息");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            if($.inArray(value.PT_Num, PT_Num_list)==-1){
                                PT_Num_list.push(value.PT_Num);
                                $("#tab_tower ul").append('<li class=""><a href="#'+value.PT_Num+'" data-toggle="tab">'+value.TD_TypeName+'('+value.PT_Num+')<i class="close-tab fa fa-remove"></i></a></li>');
                                var table_content = '<div class="tab-pane fade" id="'+value.PT_Num+'">'
                                                  + '<div class="form-group col-xs-12 col-sm-5 leftPart">'
                                                  + '<div class="form-group col-xs-12 col-sm-12">'
                                                  +'<div class="form-group col-xs-12 col-sm-4 search_field">'
                                                    +'<label class="control-label col-xs-12 col-sm-4">材料:</label>'
                                                    +'<div class="col-xs-12 col-sm-8">'
                                                        +'<select class="form-control selectpicker" name="stuff" autocomplete="off">'
                                                            +'<option value="" selected="selected">请输入</option>'
                                                        +'</select>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group col-xs-12 col-sm-4 search_field">'
                                                    +'<label class="control-label col-xs-12 col-sm-4">规格:</label>'
                                                    +'<div class="col-xs-12 col-sm-8">'
                                                        +'<select class="form-control selectpicker" name="specification" autocomplete="off">'
                                                            +'<option value="" selected="selected">请输入</option>'
                                                        +'</select>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group col-xs-12 col-sm-4 search_field">'
                                                    +'<label class="control-label col-xs-12 col-sm-4">材质:</label>'
                                                    +'<div class="col-xs-12 col-sm-8">'
                                                        +'<select class="form-control selectpicker" name="material" autocomplete="off">'
                                                            +'<option value="" selected="selected">请输入</option>'
                                                        +'</select>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group col-xs-12 col-sm-4 search_field">'
                                                    +'<label class="control-label col-xs-12 col-sm-4">工序:</label>'
                                                    +'<div class="col-xs-12 col-sm-8">'
                                                        +'<select class="form-control selectpicker" name="workmanship" autocomplete="off">'
                                                            +'<option value="" selected="selected">请输入</option>'
                                                        +'</select>'
                                                    +'</div>'
                                                +'</div>'
                                                +'<div class="form-group col-xs-12 col-sm-8">'
                                                    +'<button type="button" class="btn btn-success edit_search" data-id="'+value.PT_Num+'">搜索</button>'
                                                    +'<button type="button" class="btn btn-info right_move" data-id="'+value.PT_Num+'">右移</button>'
                                                    +'<button type="button" class="btn btn-info weight_update" data-id="'+value.PT_Num+'">重量</button>'
                                                    +'<button type="button" class="btn btn-info hole_update" data-id="'+value.PT_Num+'">孔数</button>'
                                                +'</div></div><div class="form-group col-xs-12 col-sm-12">'
                                                  + '<table id="left_'+value.PT_Num+'_table" class="table table-striped table-bordered table-hover table-nowrap" width="100%"></table>'
                                                  + '</div></div>'
                                                  + '<div class="form-group col-xs-12 col-sm-7 rightPart">'
                                                  + '<table id="right_'+value.PT_Num+'_table" class="table table-striped table-bordered table-hover table-nowrap" width="100%"></table>'
                                                  + '</div></div>';
                                $("#myTabContent").append(table_content);
                                jt_left_table = $('#left_'+value.PT_Num+'_table');
                                jt_right_table = $('#right_'+value.PT_Num+'_table');
                                // 初始化表格
                                jt_left_table.bootstrapTable({
                                    data:[],
                                    uniqueId: "id",
                                    height: left_table_height,
                                    columns: [
                                        [
                                            {checkbox: true},
                                            {field: 'stuff', title: '材料'},
                                            {field: 'specification', title: '规格'},
                                            {field: 'material', title: '材质'},
                                            {field: 'workmanship', title: '工序'},
                                            {field: 'surplus_sum_weight_info', title: '剩余重量'},
                                            {field: 'surplus_hole_number_info', title: '剩余孔数'},
                                            {field: 'pt_num', title: '下达单号'},
                                            {field: 'id', title: 'id'},
                                            // {field: 'pid', title: 'pid',visible: false},
                                            {
                                                field: 'id',
                                                title: '<a href="javascript:;" class="btn btn-success btn-xs btn-toggle" style="border-top:none;"><i class="fa fa-chevron-up"></i></a>',
                                                operate: false,
                                                formatter: Controller.api.formatter.subnode
                                            },
                                            {field: 'parts_id', title: '部件号'},
                                            {field: 'piece_detail_id', title: 'piece_detail_id',visible: false}
                                        ]
                                    ],
                                    rowAttributes: function (row, index) {
                                        return row.pid == 0 ? {} : {style: "display:none"};
                                    }
                                });
                                // 为表格绑定事件
                                Table.api.bindevent(jt_left_table);
                                // 初始化表格
                                createtable(jt_right_table,[],right_table_height);
                                // 为表格绑定事件
                                Table.api.bindevent(jt_right_table);
                                $.ajax({
                                    url: 'chain/lofting/piece_main/getPieceList',
                                    type: 'post',
                                    dataType: 'json',
                                    async: false,
                                    data: {num:value.PT_Num},
                                    success: function (ret) {
                                        var left_table_content = [];
                                        if(ret.code == 1){
                                            left_table_content = ret.data["list"];
                                            $.each(ret.data["search"],function(index,item){
                                                $("#"+value.PT_Num+" .leftPart").find(".search_field").eq(index).find("div").html(item);
                                            });
                                        }
                                        jt_left_table.bootstrapTable('load',left_table_content);
                                    }, error: function (e) {
                                        Backend.api.toastr.error(e.message);
                                    }
                                });
                                $('a[href="#'+value.PT_Num+'"]').trigger("click");
                                
                                // //显示隐藏子节点
                                // $(document).on("click", ".btn-node-sub", function (e) {
                                //     var status = $(this).data("shown") ? true : false;
                                //     $(this).closest("table").find("a.btn[data-pid='" + $(this).data("id") + "']").each(function () {
                                //         $(this).closest("tr").toggle(!status);
                                //     });
                                //     $(this).data("shown", !status);
                                //     return false;
                                // });

                                jt_left_table.on('dbl-click-row.bs.table',function(row,$elemente){
                                    if(!$elemente.pid && !jt_right_table.bootstrapTable('getRowByUniqueId', $elemente.id)){
                                        var this_display_state = [];
                                        jt_right_table.find("tbody tr").each(function(index,value){
                                            this_display_state.push($(this).css("display"));
                                        })
                                        jt_left_table.bootstrapTable("uncheckAll");
                                        jt_left_table.bootstrapTable("checkBy", {field:"id", values:[$elemente.id]});
                                        jt_left_table.bootstrapTable("checkBy", {field:"pid", values:[$elemente.id]}); 
                                        var right_table_content = jt_right_table.bootstrapTable("getData");
                                        var table_content = jt_left_table.bootstrapTable("getAllSelections");
                                        // console.log(right_table_content,table_content);
                                        table_content = $.merge(right_table_content,table_content);
                                        jt_right_table.bootstrapTable("destroy");
                                        createtable(jt_right_table,table_content,right_table_height);
                                        // jt_right_table.bootstrapTable("append",table_content);
                                        $.each(this_display_state,function(index,value){
                                            var status = value=="none"?false:true;
                                            jt_right_table.find("tbody tr").eq(index).toggle(status);
                                        })
                                        // 为表格绑定事件
                                        Table.api.bindevent(jt_right_table);
                                        jt_left_table.bootstrapTable("uncheckAll");
                                        jt_right_table.bootstrapTable("uncheckAll");
                                    }
                                });
                            }
                        }
                    };
                    Fast.api.open('chain/lofting/piece_main/selectxd',"任务单塔型选择",options);
                });
                $(document).on("click","#tab_tower li a",function (e) {
                    var this_href = $(this).attr("href");
                    jt_left_table = $('#left_'+this_href.substr(1)+'_table');
                    jt_right_table = $('#right_'+this_href.substr(1)+'_table');
                    // Table.api.bindevent(jt_left_table);
                    // Table.api.bindevent(jt_right_table);
                });
                $(document).on("click", "#table_save", function(e){
                    // jt_right_table = $('#'+value.PT_Num+' .rightPart .table');

                    var table_head = $("form").serializeArray();
                    var table_list = $("#tab_tower li");
                    var table_input_news = [];
                    $.each(table_list,function(index,item){
                        var href_field = $(item).find("a").attr("href");
                        var need_save_news = $(href_field+' .rightPart .table').bootstrapTable("getData");
                        if(need_save_news.length != 0){
                            table_input_news.push(need_save_news);
                        }
                    });
                    // console.log(table_input_news);
                    // return false;
                    $.ajax({
                        url: 'chain/lofting/piece_main/savePiece/ids/'+(Config.ids??0),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {main_data: JSON.stringify(table_head),table_data: JSON.stringify(table_input_news)},
                        success: function (ret) {
                            if(ret.code==1){
                                layer.msg(ret.msg, {
                                    icon: 1,
                                    time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                  }, function(){
                                    if(!(Config.ids??0)) window.location.href = "edit/ids/"+ret.data;
                                });  
                            }else{
                                layer.msg(ret.msg, {
                                    icon: 2,
                                    time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                }); 
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });
                $(document).on("click", "#tab_tower .fa-remove", function(e){
                    var class_name = $(this).parents("a").attr("href");
                    if(Config.ids!=0){
                        $.ajax({
                            url: 'chain/lofting/piece_main/deletePiece/ids/'+Config.ids,
                            type: 'post',
                            dataType: 'json',
                            data: {num:class_name},
                            success: function (ret) {
                                if(ret.code==1){
                                    $.each(PT_Num_list,function(index,item){
                                        if(item == class_name.substr(1)) {
                                            PT_Num_list.splice(index,1);
                                        }
                                    });
                                    $(e.target).closest("li").remove();
                                    $(class_name).remove();
                                    jt_left_table.bootstrapTable('destroy');
                                    jt_right_table.bootstrapTable('destroy');
                                    $('#tab_tower ul').find("li:eq(0)>a").trigger("click");
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }else{
                        $.each(PT_Num_list,function(index,item){
                            if(item == class_name.substr(1)) {
                                PT_Num_list.splice(index,1);
                            }
                        });
                        $(e.target).closest("li").remove();
                        $(class_name).remove();
                        jt_left_table.bootstrapTable('destroy');
                        jt_right_table.bootstrapTable('destroy');
                        $('#tab_tower ul').find("li:eq(0)>a").trigger("click");
                    }
                });
                //显示隐藏子节点
                $(document).on("click", ".btn-node-sub", function (e) {
                    var status = $(this).data("shown") ? true : false;
                    $(this).closest("table").find("a.btn[data-pid='" + $(this).data("id") + "']").each(function () {
                        $(this).closest("tr").toggle(!status);
                    });
                    $(this).data("shown", !status);
                    return false;
                });
                // //展开隐藏全部
                // $(document.body).on("click", ".btn-toggle-all", function (e) {
                //     var that = this;
                //     var show = $("i", that).hasClass("fa-plus");
                //     $("i", that).toggleClass("fa-plus", !show);
                //     $("i", that).toggleClass("fa-minus", show);
                //     $(".btn-node-sub.disabled").closest("tr").toggle(show);
                //     $(".btn-node-sub").data("shown", show);
                // });
                // $(document).on("keyup", ".rightPart input[name='surplus_sum_weight_input']", function(e){
                //     var tr_index = $(this).parents("tr").index();
                //     var value = parseFloat($(this).val());
                //     jt_right_table.bootstrapTable('updateCell', {index:tr_index,field:"surplus_sum_weight",value:value});
                // });
                // $(document).on("keyup", "input[name='surplus_hole_number_input']", function(e){
                //     var tr_index = $(this).parents("tr").index();
                //     var value = parseFloat($(this).val());
                //     jt_right_table.bootstrapTable('updateCell', {index:tr_index,field:"surplus_hole_number",value:value});
                // });
                $(document).on("click", "#author", function () {
                    check('审核之后无法修改，确定审核？',"chain/lofting/piece_main/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    check('确定弃审？',"chain/lofting/piece_main/giveUp",Config.ids);

                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            async: false,
                            success: function (ret) {
                                if(ret.code==1){
                                    layer.msg(ret.msg, {
                                        icon: 1,
                                        time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                      }, function(){
                                        window.location.href = "edit/ids/"+Config.ids;
                                    }); 
                                }else{
                                    layer.msg(ret.msg, {
                                        icon: 2,
                                        time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                    }); 
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
                function saveData(index, field, value) {
                    jt_right_table.bootstrapTable('updateCell', {
                        index: index,
                        field: field,
                        value: value
                    });
                    return false;
                }


                $(document).on('click', ".edit_search", function(e){
                    var pt_num_id = $(this).attr("data-id");
                    $.ajax({
                        url: 'chain/lofting/piece_main/searchContent/ids/'+Config.ids,
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {
                            pt_num:pt_num_id,
                            stuff:$("#"+pt_num_id+" select[name='stuff']").val(),
                            specification:$("#"+pt_num_id+" select[name='specification']").val(),
                            material:$("#"+pt_num_id+" select[name='material']").val(),
                            workmanship:$("#"+pt_num_id+" select[name='workmanship']").val()
                        },
                        success: function (ret) {
                            var left_table_content = [];
                            if(ret.code == 1){
                                left_table_content = ret.data;
                            }
                            jt_left_table.bootstrapTable('load',left_table_content);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });

                $(document).on('click', ".right_move", function(e){
                    let this_display_state = [];
                    jt_right_table.find("tbody tr").each(function(index,value){
                        this_display_state.push($(this).css("display"));
                    })
                    let choose_data = jt_left_table.bootstrapTable('getAllSelections');
                    let pid_data = [];
                    $.each(choose_data,function(c_e,c_v){
                        //pid=0 pid_data不存在 用id
                        //pid<>0 pid_data有pid则跳过，没有则用pid
                        // console.log(jt_right_table.bootstrapTable('getRowByUniqueId', c_v.id),jt_right_table.bootstrapTable('getRowByUniqueId', c_v.id)==null);
                        if(!c_v.pid && $.inArray(c_v.id,pid_data)===-1 && !jt_right_table.bootstrapTable('getRowByUniqueId', c_v.id)) pid_data.push(c_v.id);
                        if(c_v.pid && $.inArray(c_v.pid,pid_data)===-1 && !jt_right_table.bootstrapTable('getRowByUniqueId', c_v.pid)) pid_data.push(c_v.pid);
                    })
                    if(pid_data.length != 0){
                        jt_left_table.bootstrapTable("uncheckAll");
                        jt_left_table.bootstrapTable("checkBy", {field:"id", values:pid_data});
                        jt_left_table.bootstrapTable("checkBy", {field:"pid", values:pid_data}); 
                        var right_table_content = jt_right_table.bootstrapTable("getData");
                        var table_content = jt_left_table.bootstrapTable("getAllSelections");
                        table_content = $.merge(right_table_content,table_content);
                        jt_right_table.bootstrapTable("destroy");
                        // 初始化表格
                        createtable(jt_right_table,table_content,right_table_height);
                        $.each(this_display_state,function(index,value){
                            var status = value=="none"?false:true;
                            jt_right_table.find("tbody tr").eq(index).toggle(status);
                        })
                        // 为表格绑定事件
                        Table.api.bindevent(jt_right_table);
                        jt_left_table.bootstrapTable("uncheckAll");
                        jt_right_table.bootstrapTable("uncheckAll");
                    }else{
                        jt_left_table.bootstrapTable("uncheckAll");
                        jt_right_table.bootstrapTable("uncheckAll");
                    }
                });

                $(document).on('click', ".weight_update", function(e){
                    let choose_weight = jt_right_table.bootstrapTable("getData");
                    $.each(choose_weight,function(ci,cv){
                        if(cv.pid==0) choose_weight[ci].surplus_sum_weight = cv.surplus_sum_weight_info;
                    })
                    jt_right_table.bootstrapTable("load",choose_weight);
                });
                $(document).on('click', ".hole_update", function(e){
                    let choose_weight = jt_right_table.bootstrapTable("getData");
                    $.each(choose_weight,function(ci,cv){
                        if(cv.pid==0) choose_weight[ci].surplus_hole_number = cv.surplus_hole_number_info;
                    })
                    jt_right_table.bootstrapTable("load",choose_weight);
                });

                // function batchUpdate(type=0){
                //     if()
                // }

                $(document).on('click', ".btn-ajax-button", function(e){
                    if(flag) return false;
                    var this_index = $(this).parents("tr").index();
                    var add_list = [];
                    var this_display_state = [];
                    jt_right_table.find("tbody tr").each(function(index,value){
                        this_display_state.push($(this).css("display"));
                    })
                    // var table_id = $(this).parents("tr").find("td").eq(1)[0]["innerText"];
                    var all_data = jt_right_table.bootstrapTable('getData');
                    if(all_data[this_index]["pid"]!=0){
                        all_data[this_index]["surplus_sum_weight"] = 0;
                        all_data[this_index]["surplus_hole_number"] = 0;
                        var left_height = jt_right_table.bootstrapTable('getScrollPosition');
                        jt_right_table.bootstrapTable("destroy");
                        // 初始化表格
                        createtable(jt_right_table,all_data,right_table_height); 
                        $.each(this_display_state,function(index,value){
                            var status = value=="none"?false:true;
                            jt_right_table.find("tbody tr").eq(index).toggle(status);
                        })
                        jt_right_table.bootstrapTable('scrollTo', left_height);
                    }else{
                        $.ajax({
                            url: 'chain/lofting/piece_main/delDetail/piece_id/'+Config.ids+'/pid/'+all_data[this_index]["id"],
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            success: function (ret) {
                                if(ret.code==0) return false;
                                var splice_list = [];
                                $.each(all_data,function(index,item){
                                    if(item["id"]!=all_data[this_index]["id"] && item["pid"]!=all_data[this_index]["id"]) add_list.push(item);
                                    else splice_list.push(index);
                                })
                                for(var i=splice_list.length-1;i>=0;i--){
                                    this_display_state.splice(splice_list[i],1)
                                }
                                jt_right_table.bootstrapTable("destroy");
                                createtable(jt_right_table,add_list,right_table_height);
                                // console.log(this_display_state);
                                $.each(this_display_state,function(index,value){
                                    var status = value=="none"?false:true;
                                    jt_right_table.find("tbody tr").eq(index).toggle(status);
                                })
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }
                });

                function createtable(jt_right_table,add_list=[],right_table_height)
                {
                    jt_right_table.bootstrapTable({
                        data:add_list,
                        height: right_table_height,
                        uniqueId: "id",
                        columns: [
                            [
                                {checkbox: true},
                                {field: 'id', title: 'id'},
                                {field: 'buttons', title: '删除',
                                    buttons: [{
                                        name: 'delete',
                                        text: '',
                                        title: '删除',
                                        classname: 'btn btn-xs btn-danger btn-ajax-button',
                                        icon: 'fa fa-trash',
                                    }],
                                    table: jt_right_table,
                                    formatter: Table.api.formatter.operate
                                },
                                {
                                    field: 'id',
                                    title: '<a href="javascript:;" class="btn btn-success btn-xs btn-toggle" style="border-top:none;"><i class="fa fa-chevron-up"></i></a>',
                                    operate: false,
                                    formatter: Controller.api.formatter.subnode
                                },
                                {field: 'tbweight', title: '重量', formatter: function(value,row,index){return '同步';}},
                                {field: 'tbhole', title: '孔数', formatter: function(value,row,index){return '同步';}},
                                {field: 'surplus_sum_weight', title: '登记重量'},
                                {field: 'surplus_hole_number', title: '登记孔数'},
                                {field: 'piece_detail_id', title: 'piece_detail_id',visible: false},
                                // {field: 'pt_num', title: '下达单号'},
                                {field: 'parts_id', title: '部件号'},
                                {field: 'stuff', title: '材料'},
                                {field: 'specification', title: '规格'},
                                {field: 'material', title: '材质'},
                                {field: 'workmanship', title: '工序'},
                                {field: 'surplus_sum_weight_info', title: '剩余重量'},
                                {field: 'surplus_hole_number_info', title: '剩余孔数'},
                            ]
                        ],
                        rowAttributes: function (row, index) {
                            return row.pid == 0 ? {} : {style: "display:none"};
                        },
                        onClickCell: function(field, value, row, $element) {
                            var height = $element.parents(".fixed-table-body").scrollTop();
                            var this_display_state = [];
                            jt_right_table.find("tbody tr").each(function(index,value){
                                this_display_state.push($(this).css("display"));
                            })
                            if(field=="surplus_sum_weight" || field=="surplus_hole_number"){
                                $element.attr('contenteditable', true);
                                $element.blur(function() {
                                    let index = $element.parent().data('index');
                                    let tdValue = $element.html();
                                    saveData(index, field, tdValue);
                                    $.each(this_display_state,function(index,value){
                                        var status = value=="none"?false:true;
                                        jt_right_table.find("tbody tr").eq(index).toggle(status);
                                    })
                                })
                            }
                            if(field=='tbweight'){
                                // console.log(height);
                                let index = $element.parent().data('index');
                                saveData(index, 'surplus_sum_weight', row["surplus_sum_weight_info"]);
                                $.each(this_display_state,function(index,value){
                                    var status = value=="none"?false:true;
                                    jt_right_table.find("tbody tr").eq(index).toggle(status);
                                })
                            }
                            if(field=='tbhole'){
                                let index = $element.parent().data('index');
                                saveData(index, 'surplus_hole_number', row["surplus_hole_number_info"]);
                                $.each(this_display_state,function(index,value){
                                    var status = value=="none"?false:true;
                                    jt_right_table.find("tbody tr").eq(index).toggle(status);
                                })
                            }
                            jt_right_table.bootstrapTable('scrollTo', height);
                        }       
                    });
                };

                $(document).on('click', "#selectPeople", function(e){
                    let machine = $("#c-device").val();
                    if(!machine){
                        layer.msg("请先选择设备号");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            let operator_string = JSON.stringify(value);
                            $("#c-operator").val(operator_string);
                            let new_operator_list = [];
                            $.each(value,function(v_index,v_e){
                                new_operator_list.push(v_e.title+'：'+v_e.hours);
                            })
                            $(".sj_operator").text(new_operator_list.toString());
                        }
                    };
                    Fast.api.open('chain/lofting/piece_main/selectPeople/machine/'+machine+'/ids/'+(Config.ids??0),"操作员选择",options);
                });
            },
            formatter: {
                subnode: function (value, row, index) {
                    return '<a href="javascript:;" data-toggle="tooltip" title="' + __('Toggle sub menu') + '" data-id="' + row.id + '" data-pid="' + row.pid + '" class="btn btn-xs '
                        + (row.haschild == 1 || row.ismenu == 1 ? 'btn-success' : 'btn-default disabled') + ' btn-node-sub"><i class="fa fa-sitemap"></i></a>';
                }
            },
        }
    };
    return Controller;
});