define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: [],
                height: 250,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'Auditor', title: '审核人'},
                        {field: 'AuditeDate', title: '审核时间'},
                        {field: 'TH_Height', title: '呼高(米)'},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'SCD_Part', title: '公共段'},
                        {field: 'SCD_SpPart', title: '专用段'},
                        {field: 'SCD_Count', title: '配段基数'},
                        {field: 'SCD_Unit', title: '单位'},
                        {field: 'BiaoDuan', title: '标段'},
                        {field: 'SCD_lineName', title: '线路'},
                        {field: 'TD_Pressure', title: '电压等级'},
                        {field: 'SCD_ProductName', title: '产品名称'},
                        {field: 'SCD_ALength', title: 'A腿长(米)'},
                        {field: 'SCD_APart', title: 'A段位'},
                        {field: 'SCD_BLength', title: 'B腿长(米)'},
                        {field: 'SCD_BPart', title: 'B段位'},
                        {field: 'SCD_CLength', title: 'C腿长(米)'},
                        {field: 'SCD_CPart', title: 'C段位'},
                        {field: 'SCD_DLength', title: 'D腿长(米)'},
                        {field: 'SCD_DPart', title: 'D段位'},
                        {field: 'SCD_Corner', title: '转角度数'},
                        {field: 'SCD_BStyle', title: '基础形式'},
                        {field: 'SCD_MountPoint', title: '挂点'},
                        {field: 'Writer', title: '制表人'},
                        {field: 'WriteDate', title: '制表时间'},
                        
                    ]
                ]
            });
            
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/tower_section/index' + location.search,
                    edit_url: 'chain/lofting/tower_section/edit',
                    multi_url: 'chain/lofting/tower_section/multi',
                    import_url: 'chain/lofting/tower_section/import',
                    table: 'tasksect',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TD_ID',
                sortName: 'T_WriterDate',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                // height: 500,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'state', title: __('state'), operate: false},
                        {field: 'd.TD_TypeName', title: __('TD_TypeName'), operate: 'LIKE', visible: false},
                        {field: 'C_Num', title: __('C_Num'), operate: false},
                        {field: 't.C_Num', title: __('C_Num'), operate: 'LIKE', visible: false},
                        {field: 'PC_Num', title: __('PC_Num'), operate: false},
                        {field: 'T_Num', title: __('T_Num'), operate: false},
                        {field: 't.T_Num', title: __('T_Num'), operate: 'LIKE', visible: false},
                        {field: 'T_Sort', title: __('T_Sort'), operate: false},
                        {field: 'T_Company', title: __('T_Company'), operate: false},
                        {field: 'T_WriterDate', title: __('T_WriterDate'), operate: false},
                        {field: 't_project', title: __('t_project'), operate: 'LIKE'},
                        {field: 'TD_TypeName', title: __('TD_TypeName'), operate: false},
                        {field: 'TD_TypeNameGY', title: __('TD_TypeNameGY'), operate: false},
                        {field: 'TD_Pressure', title: __('TD_Pressure'), operate: false},
                        {field: 'TD_Count', title: __('TD_Count'), operate:false},
                        // {field: 'unit', title: '单位', operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var ids = $element.TD_ID;
                if(ids == '') return false;
                $.ajax({
                    url: 'chain/lofting/tower_section/contrast',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:ids},
                    success: function (ret) {
                        var content = {};
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
            
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $("#table_div").height(document.body.clientHeight-150);
            $(document).on("click", "#author", function () {
                var num = $("#c-TD_ID").val();
                if(num == false){
                    layer.confirm('请先保存！');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/lofting/tower_section/auditor",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = $("#c-TD_ID").val();
                if(num == false){
                    layer.confirm('请重试！');
                    return false;
                }
                check('确定弃审？',"chain/lofting/tower_section/giveUp",num);
                
            });
            function check(msg,url,num){
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                Layer.msg(ret.msg);
                                if (ret.code === 1) {
                                    window.location.reload();
                                }
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        selectproject: function() {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/tower_section/selectProject' + location.search,
                    table: 'tasksect',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'T_Num',
                sortName: 'TD_ID',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'd.TD_TypeName', title: __('TD_TypeName'), operate: 'LIKE',visible:false},
                        {field: 'C_Num', title: __('C_Num'), operate: false},
                        // {field: 't.C_Num', title: __('C_Num'), operate: 'LIKE',visible:false},
                        {field: 'PC_Num', title: __('PC_Num'), operate: false},
                        {field: 'T_Num', title: __('T_Num'), operate: false},
                        {field: 't.T_Num', title: __('T_Num'), operate: 'LIKE',visible:false},
                        {field: 'T_Sort', title: __('T_Sort'), operate: false},
                        {field: 'T_Company', title: __('T_Company'), operate: false},
                        {field: 'T_WriterDate', title: __('T_WriterDate'), operate: false},
                        {field: 't_project', title: __('t_project'), operate: 'LIKE'},
                        {field: 'TD_TypeName', title: __('TD_TypeName'), operate: false},
                        {field: 'P_Num', title: '线路', operate: false},
                        {field: 'TD_TypeNameGY', title: __('TD_TypeNameGY'), operate: false},
                        {field: 'TD_Pressure', title: __('TD_Pressure'), operate: false},
                        {field: 'TD_Count', title: __('TD_Count'), operate:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                Fast.api.close(tableContent);
                
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    return false;
                });
            }
        }
    };
    return Controller;
});