define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    //隐藏加载数据
    function HiddenDiv() {
        $("#loading").hide();
    }
    ids = typeof ids == 'undefined' ? "" : ids;
    drawTableContent = typeof drawTableContent == 'undefined' ? [] : drawTableContent;
    leftTableContent = typeof leftTableContent == 'undefined' ? [] : leftTableContent;
    situation = typeof situation == 'undefined' ? [] : situation;
    isNoDeal = typeof isNoDeal == 'undefined' ? [] : isNoDeal;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/dh_cooperate/index' + location.search,
                    add_url: 'chain/lofting/dh_cooperate/add',
                    edit_url: 'chain/lofting/dh_cooperate/edit',
                    del_url: 'chain/lofting/dh_cooperate/del',
                    multi_url: 'chain/lofting/dh_cooperate/multi',
                    import_url: 'chain/lofting/dh_cooperate/import',
                    table: 'dhcooperate',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'DC_Num',
                sortName: 'DC_Num',
                sortOrder: 'DESC',
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DC_Num', title: __('DC_Num'), operate: false},
                        // {field: 'T_Company', title: __('T_Company'), operate: false},
                        {field: 'f.DtM_sTypeName', title: __('DtM_sTypeName'), operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: __('T_Num'), operate: false},
                        {field: 't.T_Num', title: __('T_Num'), operate: 'LIKE', visible: false},
                        {field: 'T_Sort', title: __('T_Sort'), operate: false},
                        {field: 'DtM_sProject', title: __('DtM_sProject'), operate: 'LIKE'},
                        {field: 'DtM_sTypeName', title: __('DtM_sTypeName'), operate: false},
                        {field: 'DtM_sPressure', title: __('DtM_sPressure'), operate: false},
                        {field: 'DC_Editer', title: __('DC_Editer'), operate: false},
                        {field: 'DC_Editdate', title: __('DC_Editdate'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            $(document).on("click", "#table_add", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        var tableData = value[0];
                        $("#c-DtM_iID_PK").val(tableData.DtM_iID_PK);
                        // $("#c-T_Company").val(tableData.T_Company);
                        $("#c-C_Num").val(tableData.C_Num);
                        $("#c-TD_ID").val(tableData.TD_ID);
                        $("#c-DtM_sProject").val(tableData.DtM_sProject);
                        $("#c-DtM_sTypeName").val(tableData.DtM_sTypeName);
                        $("#c-DtM_sPressure").val(tableData.DtM_sPressure);
                        // $("#c-CustomerName").val(tableData.CustomerName);
                    }
                };
                Fast.api.open('chain/lofting/dh_cooperate/selectProject',"选择工程",options);
            });
            // Form.api.bindevent($("form[role=form]"), function(data, ret){
            //     window.location.href = "edit/ids/"+data;
            // });
            Controller.api.bindevent("add");
        },
        selectproject: function() {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/dh_cooperate/selectProject' + location.search,
                    table: 'tasksect',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TD_ID',
                sortName: 'd.DtM_dTime',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'C_SaleType', title: __('C_SaleType'), operate: false},
                        {field: 'C_Num', title: __('C_Num'), operate: false},
                        {field: 'PC_Num', title: __('PC_Num'), operate: false},
                        {field: 'T_Num', title: __('T_Num'), operate: false},
                        {field: 't.T_Num', title: __('T_Num'), operate: 'LIKE', visible: false},
                        {field: 'T_Company', title: __('T_Company'), operate:false},
                        {field: 'DtM_sProject', title: __('DtM_sProject'), operate: 'LIKE'},
                        // {field: 'DtM_sSortProject', title: __('DtM_sSortProject'), operate: 'LIKE'},
                        {field: 'DtM_sTypeName', title: __('DtM_sTypeName'), operate: 'LIKE'},
                        {field: 'TD_Count', title: __('TD_Count'), operate:false},
                        {field: 'P_Num', title: __('P_Num'), operate: false},
                        {field: 'DtM_sPressure', title: __('DtM_sPressure'), operate:false},
                        {field: 'CustomerName', title: __('CustomerName'), operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                Fast.api.close(tableContent);
                
            });
        },
        edit: function () {
            Controller.api.bindevent("edit");
            var DC_Num = $("#c-DC_Num").val();
            $(document).on("click", "#table_add", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    end:function(){
                        window.location.reload();
                    }
                };
                Fast.api.open('chain/lofting/dh_cooperate/drawingAssemblyDetail/dc_num/'+ids,"电焊组件明细",options);
            });

            $(document).on("click", "#complete", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        // title: '电焊组件明细',
                       
                    }
                };
                Fast.api.open('chain/lofting/dh_cooperate/drawingAssemblySituation/dc_num/'+ids,"电焊件配置情况",options);
            });

            $(document).on("click", "#copyWelding", function () {
                $.ajax({
                    url: 'chain/lofting/dh_cooperate/confirmModification',
                    type: 'post',
                    dataType: 'json',
                    data: {num:DC_Num},
                    success: function (ret) {
                        if (ret.code === 1) {
                            var options = {
                                shadeClose: false,
                                shade: [0.3, '#393D49'],
                                area: ["100%","100%"],
                                callback:function(value){
                                    var name_arr = [];
                                    var contentAjax = value[0];
                                    var copy_dc_num = value[1];
                                    var choose = value[2];
                                    $.each(contentAjax,function(index,e){
                                        name_arr.push(e.name);
                                    });
                                    if(name_arr.length != 0){
                                        $.ajax({
                                            url: 'chain/lofting/dh_cooperate/copyTower',
                                            type: 'post',
                                            dataType: 'json',
                                            data: {name_arr:name_arr , copy_dc_num:copy_dc_num, choose: choose,dc_num: DC_Num},
                                            success: function (ret) {
                                                layer.msg(ret.msg);
                                                if(ret.code==1){
                                                    window.location.reload();
                                                }
                                            }, error: function (e) {
                                                Backend.api.toastr.error(e.message);
                                            }
                                        });
                                    }
                                   
                                }
                            };
                            Fast.api.open('chain/lofting/dh_cooperate/copyWelding',"复制其他塔型的电焊组件",options);
                        }else{
                            layer.msg(ret.msg);
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
                
            });
            // $(document).on("click", "#save-detail", function () {
            //     var filterData = $("#edit-detail").serializeArray();
            //     $.ajax({
            //         url: 'chain/lofting/dh_cooperate/saveEditDetail',
            //         type: 'post',
            //         dataType: 'json',
            //         data: {data: JSON.stringify(filterData)},
            //         async: false,
            //         success: function (ret) {
            //             layer.msg(ret.msg);
            //         }, error: function (e) {
            //             Backend.api.toastr.error(e.message);
            //         }
            //     });
            // });
        },
        copywelding:function () {
            $("#deal").bootstrapTable({
                data: {},
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {checkbox: true},
                        {field: 'name', title: '段名'}
                    ]
                ]
            });
            Table.api.bindevent($("#deal"));

            $(document).on("click", "#chooseTower", function () {
                var chooseType = $('input[name="choose"]:checked').val();
                var url = '';
                if(chooseType ==1) url = 'chain/lofting/dh_cooperate/copyWeldingProject';
                else url = 'chain/lofting/dh_cooperate/copyWeldingTower';

                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        var tableContet = value[0];
                        var type = value[1];
                        $("#c-Type_Name").val(tableContet["TD_TypeName"]);
                        $("#c-DC_Num").val(tableContet["DC_Num"]);
                        if(type == 1){
                            $("#c-project_name").val(tableContet["t_project"]);
                        }
                        $.ajax({
                            url: 'chain/lofting/dh_cooperate/searchParam',
                            type: 'post',
                            dataType: 'json',
                            data: {num:tableContet["DC_Num"],type: type},
                            success: function (ret) {
                                if (ret.code === 1) {
                                    $("#deal").bootstrapTable('load',ret.data);
                                }else{
                                    layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }
                };
                Fast.api.open(url,"选择电焊塔型",options);
            });

            $(document).on("click", "#chooseCopy", function () {
                var choose = $('input[name="choose"]:checked').val();
                var chooseType = $('input[name="chooseCopy"]:checked').val();
                var content;
                var dc_num = $("#c-DC_Num").val();
                if(dc_num == false){
                    layer.msg("请先选择塔型");
                    return false;
                }
                if(chooseType ==1){
                    content = $("#deal").bootstrapTable('getData');
                }else{
                    content = $("#deal").bootstrapTable('getAllSelections');
                }
                if(content.length == 0){
                    layer.msg("无段可以复制");
                    return false;
                }
                Fast.api.close([content,dc_num,choose]);
            });
        },
        copyweldingproject: function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/dh_cooperate/copyWeldingProject' + location.search,
                    table: 'dhcooperate',
                }
            });

            var table_project = $("#table");

            // 初始化表格
            table_project.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'DC_Num',
                sortName: 'DC_Num',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DC_Num', title: '编号', operate: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 't.T_Num', title: '任务单号' , operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务单号' , operate: false},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'},
                        {field: 't_shortproject', title: '工程简称', operate: false},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table_project);
            $(document).on("click", "#sure", function () {
                var formData=table_project.bootstrapTable('getAllSelections');
                if(formData.length == 0){
                    layer.msg("请选择！");
                    return false;
                }
                Fast.api.close([formData[0],1]);
            });
        },
        copyweldingtower: function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/dh_cooperate/copyWeldingTower' + location.search,
                    table: 'dhcooperate',
                }
            });

            var table_project = $("#table");

            // 初始化表格
            table_project.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'DC_Num',
                sortName: 'DC_Num',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DC_Num', title: '编号', operate: 'LIKE'},
                        {field: 'DtM_sTypeName', title: '塔型', operate: 'LIKE',visible: false},
                        {field: 'TD_TypeName', title: '塔型', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table_project);
            $(document).on("click", "#sure", function () {
                var formData=table_project.bootstrapTable('getAllSelections');
                if(formData.length == 0){
                    layer.msg("请选择！");
                    return false;
                }
                Fast.api.close([formData[0],2]);
            });
        },
        drawingassemblysituation: function(){
            $("#deal").bootstrapTable({
                data: situation,
                rowStyle: Controller.api.rowStyle,
                height:document.body.clientHeight - 80,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'name', title: '部件号'},
                        {field: 'preCount', title: '部件数'},
                        {field: 'isCount', title: '电焊配置数'},
                        {field: 'detail', title: '配置情况'}
                    ]
                ]
            });
            $("#isNoDeal").bootstrapTable({
                data: isNoDeal,
                height:document.body.clientHeight - 80,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'DtS_Name', title: '段名'},
                        {field: 'DtMD_sPartsID', title: '未作电焊件的部件'},
                        {field: 'DtMD_sMaterial', title: '材质'},
                        {field: 'DtMD_sSpecification', title: '规格'}
                    ]
                ]
            });
            // Table.api.bindevent(rightTable);
        },
        drawingassemblydetail: function () {
            Form.api.bindevent($("form[role=form]"),function(){
                clearLeft();
                return false;
            });
            var height = document.body.clientHeight - $("#filter-form").height() - 70;
            var rightTableFromDiv = document.body.clientHeight - $("#rightHead").height() - 105;
            $("#rightTableFrom").parent("div").height(rightTableFromDiv);
            var leftTable = $("#leftTable");
            // var rightTable = $("#rightTable");
            var rightDcdList = [];
            leftTableFunction();
            $(document).on("click", "#filter", function(e){
                leftTable.bootstrapTable('destroy');
                leftTableFunction();
            });
            $(document).on("change","select[name=DtS_Name]", function(e){
                leftTable.bootstrapTable('destroy');
                leftTableFunction();
            })
            rightTableFunction();

            $(document).on("click", "#down", function(e){
                var choose = leftTable.bootstrapTable('getSelections');
                if(choose == []) return false;
                appendFun(choose);
            });
            leftTable.on('dbl-click-row.bs.table',function(row,$elemente){
                appendFun([$elemente]);
            });

            $(document).on('click', ".table_del", function(e){
                var index = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(index).find('td').eq(14).find('input').val().trim();
                $( e.target ).closest("tr").remove();
                rightDcdList = $.grep(rightDcdList, function( n ) {
                    return ( n != num );
                });
            });

            function appendFun(elemente){
                var dcd_id = $("input[name='DCD_ID']").val();
                var appendObject = '';
                $.each(elemente,function(index,ele){
                    if($.inArray(parseInt(ele["DtMD_ID_PK"]),rightDcdList)==-1){
                        if(rightDcdList.length == 0){
                            $("input[name='DCD_ID']").val(dcd_id);
                            $("#c-DCD_PartName").val(ele.DtS_Name);
                            $("#c-DCD_PartNum").val(ele.DtMD_sPartsID);
                            $("#c-DCD_Count").val(ele.DtMD_iUnitCount);
                        }
                        appendObject += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger table_del"><i class="fa fa-trash"></i></a></td>';
                        appendObject += '<td><input name="DtMD_sPartsID" type="text" value="'+ele.DtMD_sPartsID+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DtMD_sStuff" type="text" value="'+ele.DtMD_sStuff+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DtMD_sMaterial" type="text" value="'+ele.DtMD_sMaterial+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DtMD_sSpecification" type="text" value="'+ele.DtMD_sSpecification+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DHS_Thick" type="text" value="'+ele.DtMD_iLength+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DHS_Height" type="text" value="'+ele.DtMD_fWidth+'" class="small_input" readonly></td>';
                        appendObject += '<td><input name="DHS_Count" type="text" value="'+ele.DtMD_iUnitCount+'" class="small_input"></td>';
                        appendObject += '<td><input name="DtMD_fUnitWeight" type="text" value="'+(parseFloat)(ele.DtMD_fUnitWeight).toFixed(2)+'" class="small_input"></td>';
                        appendObject += '<td><input name="DHS_isM" type="text" value="0" class="small_input"></td>';
                        appendObject += '<td><input name="DHS_Length" type="text" value="0" class="small_input"></td>';
                        appendObject += '<td><input name="DHS_Memo" type="text" value="" class="small_input"></td>';
                        appendObject += '<td hidden><input name="DHS_ID" type="text" value="0" class="small_input"></td>';
                        appendObject += '<td hidden><input name="DCD_ID" type="text" value="'+dcd_id+'" class="small_input"></td>';
                        appendObject += '<td hidden><input name="DtMD_ID_PK" type="text" value="'+ele.DtMD_ID_PK+'" class="small_input"></td></tr>';
                        
                        rightDcdList.push(ele.DtMD_ID_PK);
                    }
                });
                $("#tbodyDetail").append(appendObject);
                
            }

            function rightTableFunction(){
                var filterData = $("#rightHead").serializeArray();
                $.ajax({
                    url: 'chain/lofting/dh_cooperate/rightTable',
                    type: 'post',
                    dataType: 'json',
                    data: filterData,
                    success: function (ret) {
                        var tableContent = '';
                        if (ret.code === 1) {
                            tableContent = ret.data;
                            rightDcdList = ret.rightDcdList;
                        }
                        $("#tbodyDetail").html(tableContent);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            function leftTableFunction(){
                var filterData = $("#filter-form").serializeArray();
                $.ajax({
                    url: 'chain/lofting/dh_cooperate/leftTable',
                    type: 'post',
                    dataType: 'json',
                    data: filterData,
                    async: false,
                    complete: function () {
                        HiddenDiv();
                    },
                    success: function (ret) {
                        var tableContent = {};
                        if (ret.code === 1) {
                            tableContent = ret.data;
                        }
                        leftTable.bootstrapTable({
                            data: tableContent,
                            search: false,
                            pagination: false,
                            height: height,
                            columns: [
                                [
                                    {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                                    {checkbox: true},
                                    {field: 'DtS_Name', title: __('DtS_Name')},
                                    {field: 'DtMD_sPartsID', title: __('DtMD_sPartsID'),width:100},
                                    {field: 'DtMD_iUnitCount', title: __('DtMD_iUnitCount')},
                                    {field: 'DtMD_sMaterial', title: __('DtMD_sMaterial')},
                                    {field: 'DtMD_sSpecification', title: __('DtMD_sSpecification')},
                                    {field: 'DtMD_iLength', title: __('DtMD_iLength')},
                                    {field: 'DtMD_fWidth', title: __('DtMD_fWidth')},
                                    {field: 'DtMD_sRemark', title: __('DtMD_sRemark'),width:100},
                                    {field: 'DtMD_fUnitWeight', title: __('DtMD_fUnitWeight')},
                                    {field: 'DtMD_iUnitHoleCount', title: __('DtMD_iUnitHoleCount')},
                                    {field: 'DtMD_iCuttingAngle', title: __('DtMD_iCuttingAngle')},
                                    {field: 'DtMD_iWelding', title: __('DtMD_iWelding')},
                                    {field: 'DtMD_fBackOff', title: __('DtMD_fBackOff')},
                                    {field: 'DtMD_iFireBending', title: __('DtMD_iFireBending')},
                                    {field: 'DtMD_iPressed', title: __('DtMD_iPressed')},
                                    {field: 'DtMD_ISZhuanYong', title: __('DtMD_ISZhuanYong')},
                                    {field: 'DtMD_sStuff', title: __('DtMD_sStuff')}
                                ]
                            ]
                        });
                        Table.api.bindevent(leftTable);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
                
            }

            $(document).on("click", "#saveContent", function(e){
                var rightHead = $("#rightHead").serializeArray();
                // var memo = $("#rightHeadMemo").serializeArray();
                var tableContent = $("#rightTableFrom").serializeArray();
                // rightHead.push(memo[0]);
                var table_row = {row: rightHead, tableRow: tableContent};

                $.ajax({
                    url: 'chain/lofting/dh_cooperate/saveDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(table_row)},
                    async: false,
                    success: function (ret) {
                        if(ret.code == 1) {
                            // var flag = false;
                            if(ret.msg != "success"){
                                var index = layer.confirm(ret.msg, {
                                    btn: ['确定', '取消'],
                                }, function(data) {
                                    // flag = true;
                                    layer.close(index);
                                    $.ajax({
                                        url: 'chain/lofting/dh_cooperate/sureSaveDetail',
                                        type: 'post',
                                        dataType: 'json',
                                        async: false,
                                        data: {list: ret.data['list'], tableList: ret.data['tableList']},
                                        success: function (ret) {
                                            if (ret.hasOwnProperty("code")) {
                                                if(ret.code==1) $("input[name='DCD_ID']").val(ret.dcd);
                                                Layer.msg(ret.msg);
                                                // if (ret.code === 1) {
                                                //     clearLeft();
                                                // }
                                            }else{
                                                Layer.msg(ret.msg);
                                            }
                                        }, error: function (e) {
                                            Backend.api.toastr.error(e.message);
                                        }
                                    })
                                })
                            }else{
                                // flag = true;
                                $.ajax({
                                    url: 'chain/lofting/dh_cooperate/sureSaveDetail',
                                    type: 'post',
                                    dataType: 'json',
                                    async: false,
                                    data: {list: ret.data['list'], tableList: ret.data['tableList']},
                                    success: function (ret) {
                                        if (ret.hasOwnProperty("code")) {
                                            if(ret.code==1) $("input[name='DCD_ID']").val(ret.dcd);
                                            Layer.msg(ret.msg);
                                            // if (ret.code === 1) {
                                            //     clearLeft();
                                            // }
                                        }else{
                                            Layer.msg(ret.msg);
                                        }
                                    }, error: function (e) {
                                        Backend.api.toastr.error(e.message);
                                    }
                                })
                            }
                        }else{
                            layer.msg(ret.msg);
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
                
            });
            $(document).on("click", "#addContent", function(e){
                var right_name = $("#c-DCD_PartName").val();
                var left_name = $("select[name='DtS_Name'] option:selected").val();
                
                if(left_name!=0 && right_name!=left_name && right_name){
                    $("#c-change_name").val(right_name);
                    leftTable.bootstrapTable('destroy');
                    leftTableFunction();
                }
                clearLeft();
            });

            $(document).on("click", "#delContent", function(e){
                var DCD_ID = $("input[name='DCD_ID']").val();
                if(DCD_ID == false) layer.msg("删除失败，不存在");
                var index = layer.confirm('确定删除该段零补件组成明细吗？', {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: 'chain/lofting/dh_cooperate/delDetail/DCD_ID/'+DCD_ID,
                        type: 'post',
                        dataType: 'json',
                        // data: {num: DCD_ID},
                        success: function (ret) {
                            layer.msg(ret.msg);
                            if(ret.code==1){
                                parent.location.reload();
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    
                    layer.close(index);
                })
            });
            function clearLeft(){
                $("input[name='DCD_ID']").val("");
                $("input[name='DCD_PartName']").val("");
                $("input[name='DCD_PartNum']").val("");
                $("input[name='DHS_Memo']").val("");
                $("input[name='DCD_Count']").val("");
                leftTable.bootstrapTable('uncheckAll');
                // rightTableFunction();
                $("#tbodyDetail").html("");
                rightDcdList = [];
                appendObject = {};
            }

            
        },
        typelist: function () {
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                Fast.api.close(data);
            });
        },
        memolist: function () {
            $(document).on("click", ".choosefast", function(e){
                var content = $("#c-DCD_Memo").val();
                var fast = $(this).text();
                if(content=='') content += fast;
                else content += ','+fast;
                $("#c-DCD_Memo").val(content);
            });
            Form.api.bindevent($("form[role=form]"), function(data, ret){
                Fast.api.close(data);
            });
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="edit"){
                        window.location.reload();
                        return false;
                    }
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                        return false;
                    }
                    
                    // window.location.href = "edit/ids/"+data;
                });
                // var height_one = document.body.clientHeight - 80,
                var height_one = $("#part_one").height();
                var part = document.body.clientHeight - height_one;
                var height = part*0.7;
                var edit_detail = part*0.25;
                $("#divTable").height(edit_detail);

                // Form.api.bindevent($("form[role=form]"), function(data, ret){
                //     if(field=="add"){
                //         window.location.href = "edit/ids/"+data;
                //     }else{
                //         window.location.reload();
                //     }
                //     return false;
                // });
                var deal = $("#deal");
                // var detail = $("#detail");
                dealContent([{name:'ids',value:ids}]);
                // Table.api.bindevent(detail);
                deal.on('post-body.bs.table',function(){
                    $(".btn-dialog").data("area",["100%","100%"]);
                    $(".btn-small").data("area",["80%","50%"]);
                });
                $(document).on("click", "#filter", function(e){
                    var filterData = $("#filter-form").serializeArray();
                    filterData.push({name:'ids',value:ids})
                    deal.bootstrapTable('destroy');
                    // detail.bootstrapTable('destroy');
                    $("#divTable").hide();
                    dealContent(filterData);
                });
                var timer=null;
                deal.on('click-row.bs.table',function(row,$elemente){
                    clearTimeout(timer);
                    timer=setTimeout(function(){
                        $("#divTable").show();
                        $.ajax({
                            url: 'chain/lofting/dh_cooperate/detailDrawing',
                            type: 'post',
                            dataType: 'json',
                            data: {DCD_ID:$elemente.DCD_ID},
                            success: function (ret) {
                                var tableContent = '';
                                if (ret.code === 1) {
                                    tableContent = ret.data;
                                }
                                $("#show").html(tableContent);
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        })
                    },300); 
                    
                });

                //双击行
                deal.on('dbl-click-row.bs.table',function(row,$elemente){
                    clearTimeout(timer);
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                    };
                    Fast.api.open('chain/lofting/dh_cooperate/drawingAssemblyDetail/dc_num/'+$elemente.DC_Num+'/dcd/'+$elemente.DCD_ID,"电焊组件明细",options);
                });

                $(document).on("click", "#table_del", function(e){
                    var choose_detail = deal.bootstrapTable('getSelections');
                    if(choose_detail.length == 0) layer.msg("请选择删除内容！");
                    else{
                        var dcd_id_list = [];
                        $.each(choose_detail,function(index,e){
                            dcd_id_list.push(e.DCD_ID);
                        })
                        var DCD_ID = dcd_id_list.join(",");
                        var index = layer.confirm('确定删除该段零部件组成明细吗？', {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/lofting/dh_cooperate/delDetail/DCD_ID/'+DCD_ID,
                                type: 'post',
                                dataType: 'json',
                                // data: {num: DCD_ID},
                                success: function (ret) {
                                    layer.msg(ret.msg);
                                    if(ret.code==1){
                                        window.location.reload();
                                        // clearLeft();
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            
                            layer.close(index);
                        })
                    }
                    

                    // var filterData = $("#filter-form").serializeArray();
                    // filterData.push({name:'ids',value:ids})
                    // deal.bootstrapTable('destroy');
                    // $("#divTable").hide();
                    // dealContent(filterData);
                });
                function dealContent(filterData){
                    $.ajax({
                        url: 'chain/lofting/dh_cooperate/weldingDrawing',
                        type: 'post',
                        dataType: 'json',
                        data: filterData,
                        complete: function () {
                            HiddenDiv();
                        },
                        success: function (ret) {
                            
                            var tableContent = {};
                            if (ret.code === 1) {
                                tableContent = ret.data;
                            }

                            // 初始化表格
                            deal.bootstrapTable({
                                data: tableContent,
                                idField: 'DCD_ID',
                                uniqueId: 'DCD_ID',
                                search: false,
                                pagination: false,
                                height: height,
                                //clickToSelect: true,
                                //singleSelect: true,
                                columns: [
                                    [
                                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                                        {checkbox: true},
                                        {field: 'DCD_ID', title: 'DCD_ID', visible:false},
                                        // {field: 'operate', title: '焊图', table: deal, 
                                        //     buttons: [
                                        //         {
                                        //             name: 'detail',
                                        //             text: '',
                                        //             title: '电焊组件明细',
                                        //             classname: 'btn btn-xs btn-primary btn-dialog',
                                        //             icon: 'fa fa-cogs',
                                        //             url: 'chain/lofting/dh_cooperate/drawingAssemblyDetail/dc_num/'+ids+'/dcd/{DCD_ID}',
                                        //         }
                                        //     ],
                                        //     events: Table.api.events.operate, 
                                        //     formatter: Table.api.formatter.operate},
                                        {field: 'operate', title: '删除',
                                            buttons: [
                                                {name: 'comfirmDel',
                                                    text: '',
                                                    title: '删除',
                                                    classname: 'btn btn-xs btn-danger btn-view btn-ajax',
                                                    icon: 'fa fa-trash',
                                                    url: 'chain/lofting/dh_cooperate/delDetail/DCD_ID/{DCD_ID}',
                                                    success: function(e,data){
                                                        console.log(e,data);
                                                        window.location.reload();
                                                    },
                                                    refresh: true}
                                                ],
                                            table: deal, events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                                        {field: 'DCD_PartName', title: __('DCD_PartName')},
                                        {field: 'DCD_PartNum', title: __('DCD_PartNum')},
                                        {field: 'DCD_Count', title: __('DCD_Count')},
                                        {field: 'DCD_SWeight', title: __('DCD_SWeight')},
                                        // {field: 'operate', title: '类别',
                                        //     buttons: [
                                        //         {name: 'type',
                                        //             text: '',
                                        //             title: '修改',
                                        //             classname: 'btn btn-xs btn-primary btn-dialog btn-small',
                                        //             icon: 'fa fa-list',
                                        //             url: 'chain/lofting/dh_cooperate/typeList/DCD_ID/{DCD_ID}',
                                        //             callback:function(data){
                                        //                 window.location.reload();
                                        //                 // deal.bootstrapTable('updateCell', {index:1,field:"DCD_Type",value:data})
                                        //             }
                                        //         }],
                                        //     table: deal, events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                                        // {field: 'DCD_Type', title: __('DCD_Type')},
                                        {field: 'DCD_Type_Input', title: __('DCD_Type')},
                                        {field: 'Writer', title: __('Writer')},
                                        {field: 'WriteDate', title: __('WriteDate')},
                                        // {field: 'Auditor', title: __('Auditor')},
                                        // {field: 'AuditDate', title: __('AuditDate')},
                                        // {field: 'operate', title: '备注',
                                        //     buttons: [
                                        //         {name: 'memo',
                                        //             text: '',
                                        //             title: '修改',
                                        //             classname: 'btn btn-xs btn-primary btn-dialog btn-small',
                                        //             icon: 'fa fa-list',
                                        //             url: 'chain/lofting/dh_cooperate/memoList/DCD_ID/{DCD_ID}',
                                        //             callback:function(data){
                                        //                 window.location.reload();
                                        //                 // deal.bootstrapTable('updateCell', {index:1,field:"DCD_Memo",value:data})
                                        //             }
                                        //         }],
                                        //     table: deal, events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                                        // {field: 'DCD_Memo', title: __('DCD_Memo')},
                                        {field: 'DCD_Memo_Input', title: __('DCD_Memo')}
                                    ]
                                ]
                            });
                            Table.api.bindevent(deal);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    })
                }

                // $(document).on("keyup", "input[name='DCD_Memo_Input']", function(e){
                //     var tr_index = $(this).parents("tr").index();
                //     var value = $(this).val();
                //     deal.bootstrapTable('updateCell', {index:tr_index,field:"DCD_Memo",value:value});
                // });
                // $(document).on("keyup", "input[name='DCD_Type_Input']", function(e){
                //     var tr_index = $(this).parents("tr").index();
                //     var value = $(this).val();
                //     deal.bootstrapTable('updateCell', {index:tr_index,field:"DCD_Type",value:value});
                // });

                $(document).on("click", "#table_save", function(e){
                    var table_content = deal.bootstrapTable('getData');
                    $.each($("input[name='DCD_Memo_Input']"),function (mindex,mrow){
                        if($(mrow).val() || $("input[name='DCD_Type_Input']").eq(mindex).val()){
                            table_content[mindex]["DCD_Memo"] = $(mrow).val();
                            table_content[mindex]["DCD_Type"] = $("input[name='DCD_Type_Input']").eq(mindex).val();
                        }
                    })
                    if(table_content.length==0) layer.msg("保存失败");
                    else{
                        $.ajax({
                            url: 'chain/lofting/dh_cooperate/saveDetailSect',
                            type: 'post',
                            dataType: 'json',
                            data: {data: JSON.stringify(table_content)},
                            success: function (ret) {
                                layer.msg(ret.msg);
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }
                });
            },
            rowStyle: function (row,index) {
                var style = {};
                
                if(row.common){
                    style = {css:{'color':"#FF0000"}};
                }
                return style;
            }
        }
        
    };
    return Controller;
});