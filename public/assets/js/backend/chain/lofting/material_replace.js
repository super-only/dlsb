define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'dysqdata'], function ($, undefined, Backend, Table, Form) {
    function col_index(div_content = "#tbshow"){
        var col = 0;
        $(div_content).find("tr").each(function () {
            col++;
            $(this).children('td').eq(0).text(col);
        });
    }
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/material_replace/index' + location.search,
                    // add_url: 'chain/lofting/material_replace/add',
                    edit_url: 'chain/lofting/material_replace/edit',
                    del_url: 'chain/lofting/material_replace/del',
                    // multi_url: 'chain/lofting/material_replace/multi',
                    import_url: 'chain/lofting/material_replace/import',
                    table: 'materialreplace',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'MR_Num',
                sortName: 'WriterDate',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'MR_Num', title: __('Mr_num'), operate: 'LIKE'},
                        {field: 'MR_Dept', title: __('Mr_dept'), operate: 'LIKE'},
                        {field: 'T_Num', title: '任务单号', operate: 'LIKE'},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'Writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'WriterDate', title: __('Writerdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        // add: function () {
        //     Controller.api.bindevent();
        // },
        edit: function () {
            Controller.api.bindevent();
            $("#print").click(function () {
                window.top.Fast.api.open('chain/lofting/material_replace/print/ids/' + Config.ids, '代料申请单', {
                    area: ["100%", "100%"]
                });
            });
            $(document).on('click', ".del", function(e){
                var indexChoose = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(2).find('input').val();
                if(num!=0){
                    var index = layer.confirm("即将删除该行，是否确定？", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: 'chain/lofting/material_replace/delDetail',
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                // layer.msg(ret.msg);
                                if(ret.code==1){
                                    $( e.target ).closest("tr").remove();
                                    col_index("#tbshow");
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }


                // var indexChoose = $(this).parents('tr').index();
                // var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(3).find('input').val();
                // if(num!=0){
                //     var index = layer.confirm("即将删除该段所有信息，确定删除？", {
                //         btn: ['确定', '取消'],
                //     }, function(data) {
                //         $.ajax({
                //             url: 'chain/lofting/height_bolt/delDetail',
                //             type: 'post',
                //             dataType: 'json',
                //             data: {num: num},
                //             success: function (ret) {
                //                 layer.msg(ret.msg);
                //                 if(ret.code==1){
                //                     $( e.target ).closest("tr").remove();
                //                     col_index("#tbshow");
                //                 }
                //             }, error: function (e) {
                //                 Backend.api.toastr.error(e.message);
                //             }
                //         });
                //         layer.close(index);
                //     })
                // }else{
                //     $( e.target ).closest("tr").remove();
                //     col_index("#tbshow");
                // }


            });
        },
        print: function() {
            // console.log('limberList',Config.limberList);
            console.log('detail_list',Config.row);
            console.log('mainInfos',Config.tableList);
            
            let list = Config.tableList;
            let mainInfos = Config.row;
            let tb_tmp = [];
            let index = 0;
            tb_tmp = list.map(p=>{
                index++;
                return {
                    xh: index,
                    cz: p['OldLimber'],
                    gg: p['OldType'],
                    ycd: Number(p['OldLength'])?Number(p['OldLength'])/1000:'',
                    dycz: p['NewLimber'],
                    dygg: p['NewType'],
                    dycd:Number(p['NewLength'])?Number(p['NewLength'])/1000:'',
                }
            })
            
            console.log('tb_tmp', tb_tmp)

            let data_tmp={
                bh:mainInfos['MR_Num'],
                gcmc: mainInfos['t_project'],
                rwdh: mainInfos['T_Num'],
                tx: mainInfos['TD_TypeName'],
                zbr: mainInfos['Writer'],
                zbrq:mainInfos['WriterDate'],
                
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: dysqdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});