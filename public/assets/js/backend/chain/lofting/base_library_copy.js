define(['jquery', 'bootstrap', 'backend', 'table', 'form','jquery-gi'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/base_library_copy/index' + location.search,
                    add_url: 'chain/lofting/base_library_copy/add',
                    edit_url: 'chain/lofting/base_library_copy/edit',
                    del_url: 'chain/lofting/base_library_copy/del',
                    multi_url: 'chain/lofting/base_library_copy/multi',
                    import_url: 'chain/lofting/base_library_copy/import',
                    table: 'baselibrary',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'DtM_iID_PK',
                sortName: 'DtM_iID_PK',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DtM_iID_PK', title: __('Dtm_iid_pk'), visible: false, operate: false},
                        {field: 'DtM_sTypeName', title: __('Dtm_stypename'), operate: 'LIKE'},
                        {field: 'DtM_sPressure', title: __('Dtm_spressure'), operate: 'LIKE'},
                        {field: 'DtM_sAuditor', title: __('Dtm_sauditor'), operate: 'LIKE'},
                        {field: 'DtM_dTime', title: __('Dtm_dtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'DtM_sRemark', title: __('Dtm_sremark'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
        },
        importview: function () {
            Form.api.bindevent($("form[role=form]"),function(){
                parent.location.reload();
            });
        },
        openedit: function () {
            localStorage.setItem('checkedField','');
            Controller.api.bindevent("openedit");
            var height = document.body.clientHeight-50;
            $("#table_div").height(height);
            multiple_checks();
            function multiple_checks(table_id='show')
            {
                var mousedown=false;var check = 0;var choose;
                $("body").on("mousedown", "#"+table_id+" tbody tr",function(e){
                    mousedown=true;
                    check = $(this).index();
                    check_element =$("#"+table_id+" tbody").find("tr").eq(check);
                    choose = check_element.find("td:eq(0) input").is(':checked');
                });
                $("body").on("mouseup", "#"+table_id+" tbody tr",function(e){
                    if(mousedown){
                        var mousedown_check = $(this).index();
                        var big = check>mousedown_check?check:mousedown_check;
                        var small = check<mousedown_check?check:mousedown_check;
                        if(big == small){
                            if(choose) $("#"+table_id+" tbody tr:eq("+big+")").find("td:eq(0) input").prop("checked",true);
                            else $("#"+table_id+" tbody tr:eq("+big+")").find("td:eq(0) input").prop("checked",false);

                        }else{
                            for( var i=small;i<=big;i++){
                                if(choose) $("#"+table_id+" tbody tr:eq("+i+")").find("td:eq(0) input").prop("checked",false);
                                else $("#"+table_id+" tbody tr:eq("+i+")").find("td:eq(0) input").prop("checked",true);
                            }
                        }
                    }
                    mousedown=false;
                });
            }
            var position = localStorage.getItem("scroll_top");
            $("#table_div").scrollTop(position);
            localStorage.setItem('scroll_top',0);
            $(document).on('click', "#del", function(e){
                $("#tbshow").find("tr").each(function () {
                    var checkedField = $(this).children('td').eq(0).find('input');
                    if(checkedField.is(':checked')) $( this).remove();
                });
            });
            $(document).on('click', "#copy", function(e){
                var checkedField = $('#show tbody tr').find("td:eq(0) input:checked");
                var text = "";
                checkedField.each(function (index,e){
                    text += "<tr>"+$(e).parents("tr")[0].innerHTML+"</tr>";
                });
                localStorage.setItem('checkedField',text);
                checkedField.prop("checked",false);
            });

            $(document).on('click', "#stick", function(e){
                var checkedField = localStorage.getItem('checkedField',checkedField);
                if(checkedField){
                    $('#show tbody').append(checkedField);
                }else{
                    var text = '<tr><td><input class="small_input" type="checkbox" value="1"></td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a><a href="javascript:;" class="btn btn-xs btn-success plus"><i class="fa fa-plus"></i></a></td>';
                    $.each(Config.tableField,function(index,e){
                        if(e[2]=="checkbox") text += '<td><input class="small_input" name="'+e[1]+'" type="checkbox" value="1"></td>';
                        else text += '<td><input class="small_input" name="'+e[1]+'" '+e[3]+' type="text"></td>';
                    });
                    text += "</tr>";
                    $('#show tbody').append(text);
                }
                localStorage.setItem('checkedField','');
            });
            $(document).on('click', "#replace", function(e){
                var replaceField = $('#show tbody tr').find("td:eq(0) input:checked");
                var index = $(replaceField[0]).parents("tr").index()-1;
                var checkedField = localStorage.getItem('checkedField',checkedField);
                // console.log(checkedField);return false;
                if(checkedField.length == 0){ layer.msg("复制为空，请重新复制！"); }
                else if(replaceField.length != 0){
                    replaceField.each(function (index,e){
                        $( e ).parents("tr").remove();
                    });
                    $('#show tbody tr:eq('+index+')').after(checkedField);
                    localStorage.setItem('checkedField','');
                }
            });

            $(document).on('click', "#save", function(e){
                var formData = $("#edit-form").serializeArray();
                var scroll_top = $("#table_div").scrollTop();
                localStorage.setItem("scroll_top", scroll_top);
                $.ajax({
                    url: 'chain/lofting/base_library_copy/openEdit/ids/'+Config.ids,
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData)},
                    success: function (ret) {
                        layer.msg(ret.msg);
                        if (ret.hasOwnProperty("code")) {
                            if (ret.code === 1) {
                                window.location.reload();
                            }
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on('click', ".del", function(e){
                $( e.target ).closest("tr").remove();
            });
            $(document).on('click', ".plus", function(e){
                var index_ini = $(this).parents("tr").index();
                var checkedField = localStorage.getItem('checkedField',checkedField);
                if(checkedField){
                    $('#show tbody').find("tr:eq("+index_ini+")").before(checkedField);
                }else{
                    var text = '<tr><td><input class="small_input" type="checkbox" value="1"></td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                    $.each(Config.tableField,function(index,e){
                        if(e[2]=="checkbox") text += '<td><input class="small_input" name="'+e[1]+'" type="checkbox" value="1"></td>';
                        else text += '<td><input class="small_input" name="'+e[1]+'" '+e[3]+' type="text"></td>';
                    });
                    text += "</tr>";
                    $('#show tbody').find("tr:eq("+index_ini+")").before(text);
                }
                localStorage.setItem('checkedField','');
            });
            $(document).on('click', "#search", function(e){
                var search_content = $("#search_input").val();
                // var searchTerm = $("#search_input").val();
                // console.log(searchTerm);
				// if(searchTerm) {
				// 	$("#tbshow").highlight(searchTerm);
                //     next();
				// }
            });
            $(document).on('keyup', "#search_input", function(e){
                var searchTerm = $("#search_input").val();
				if(searchTerm) {
					$("#tbshow").highlight(searchTerm);
                    // next();
				}
            });
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data; 
                    }else if(field=="openedit"){
                        return false;
                    }else{
                        window.location.reload();
                    }
                    return false;
                });
                $(document).on('click', "#refresh", function(e){
                    window.location.reload();
                });
                $(document).on('click', "#import", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["60%","40%"],
                        callback:function(value){
                            if(value == 1) window.location.reload();
                            else layer.msg("导入失败"); 
                        }
                    };
                    Fast.api.open('chain/lofting/base_library_copy/importView/ids/'+Config.ids,"导入放样部件",options);
                });
                $(document).on('click', "#open", function(e){
                    window.top.Fast.api.open('chain/lofting/base_library_copy/openEdit/ids/' + Config.ids, Config.task_name, {
                        area: ["100%", "100%"]
                    });
                });
                $(document).on("click", "#export", function () {
                    window.open('/admin.php/chain/lofting/base_library_copy/export/ids/'+Config.ids);
                });
                $(document).on("click", "#allchoose", function () {
                    var flag = $(this).prop("checked");
                    $("#tbshow").find("tr").each(function () {
                        var checkedField = $(this).children('td').eq(0).find('input');
                        if(flag) checkedField.prop("checked","true");
                        else checkedField.prop("checked","");
                    });
                });
            }
        }
    };
    return Controller;
});