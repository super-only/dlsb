define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/piece_main_statistics/index' + location.search,
                    multi_url: 'chain/lofting/piece_main_statistics/multi',
                    import_url: 'chain/lofting/piece_main_statistics/import',
                    table: 'piece_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                // pk: 'id',
                sortName: 'record_time',
                onlyInfoPagination: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ID', title: "", formatter: function(value,row,index){return ++index;}, operate: false},
                        {field: 'record_time', title: '登记时间', defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operator', title: '操作者', operate: 'LIKE'},
                        {field: 'device', title: '设备号', searchList: Config.machine_arr,formatter:function(value,row,index){return Config.machine_arr[value]}},
                        {field: 'PT_Num', title: '任务下达单', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'workmanship', title: '工艺', searchList: Config.ship_list,
                        formatter:function(value,row,index){
                            return Config.ship_list[value];
                        }},
                        {field: 'sum_weight', title: '重量', operate: false},
                        // {field: 'hole_number', title: '孔数', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});