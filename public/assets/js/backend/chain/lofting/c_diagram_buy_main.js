define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'tzsjpddata'], function ($, undefined, Backend, Table, Form) {
    tableField = typeof tableField=="undefined"?"":tableField;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/c_diagram_buy_main/index' + location.search,
                    add_url: 'chain/lofting/c_diagram_buy_main/add',
                    edit_url: 'chain/lofting/c_diagram_buy_main/edit',
                    del_url: 'chain/lofting/c_diagram_buy_main/del',
                    multi_url: 'chain/lofting/c_diagram_buy_main/multi',
                    import_url: 'chain/lofting/c_diagram_buy_main/import',
                    table: 'cdiagrambuymain',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                // data: [],
                pk: 'CDBM_Num',
                sortName: 'CDBM_Num',
                search: false,
                // clickToSelect: true,
                // singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CDBM_Num', title: __('Cdbm_num'), operate: 'LIKE'},
                        {field: 'CDM_Num', title: __('Cdm_num'), operate: false,visible: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'CDM_CProject', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_ID', title: __('Td_id'),visible: false,operate: false},
                        {field: 'CDM_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'CDM_Pressure', title: '电压等级', operate: 'LIKE'},
                        {field: 'CDBM_Mark', title: '批次', operate: 'LIKE'},
                        {field: 'SCDCount', title: '基数'},
                        {field: 'Writer', title: __('Writer'), operate: false},
                        {field: 'WriteDate', title: __('Writedate'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on("click", ".btn-arrange", function () {
                var tableContent = table.bootstrapTable("getSelections");
                var fieldStr = [];
                $.each(tableContent,function(index,e){
                    fieldStr.push(e.CDBM_Num);
                });
                if(fieldStr.length == 0) layer.msg("请选择后重新点击整理");
                else window.open('/admin.php/chain/lofting/c_diagram_buy_main/arrange/ids/' + fieldStr.toString());
                // window.open('/admin.php/chain/lofting/c_diagram_buy_main/export/ids/' + Config.ids);
            });
        },
        add: function () {
            Controller.api.bindevent("add");
            
        },
        edit: function () {
            $("#bjmxprint").click(function () {
                window.top.Fast.api.open('chain/lofting/c_diagram_buy_main/bjmxPrint/ids/' + Config.ids, '打印部件明细(按长度排序)', {
                    area: ["100%", "100%"]
                });
            });
            $("#bjmxprintbybh").click(function () {
                window.top.Fast.api.open('chain/lofting/c_diagram_buy_main/bjmxPrintByBh/ids/' + Config.ids, '打印部件明细(按编号排序)', {
                    area: ["100%", "100%"]
                });
            });
            
            $("#hzbprint").click(function () {
                window.top.Fast.api.open('chain/lofting/c_diagram_buy_main/hzbPrint/ids/' + Config.ids, '打印汇总表', {
                    area: ["100%", "100%"]
                });
            });
            Controller.api.bindevent("edit");
        },
        choosediagram: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/c_diagram_buy_main/chooseDiagram' + location.search,
                    table: 'compactdiagrammain',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'CDM_Num',
                sortName: 'CDM_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                sortOrder: 'DESC',
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CDM_Num', title: '图纸单据号', operate: 'LIKE'},
                        {field: 't.C_Num', title: '合同号', operate: 'LIKE'},
                        {field: 'td.T_Num', title: '任务单(生产单)号', operate: 'LIKE'},
                        {field: 'TD_ID', title: 'TD_ID', operate: false, visible: false},
                        {field: 'CDM_CProject', title: '工程名称', operate: 'LIKE'},
                        {field: 'CDM_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'CDM_Pressure', title: '电压等级', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent[0]);

            });
        },
        hzbprint: function() {
            let list = Config.detailList;
            // console.log(list);
            let mainInfos = Config.mainInfos;
            let tb_tmp = [];
            
            let cl_weight_tmp = 0;
            let cl_count_tmp = 0;
            let cz_weight_tmp = 0;
            let cz_count_tmp = 0;
            let weight_sum = 0;
            let count_sum = 0;

            for(let index=0;index<list.length;index++){
                
                let tmp={
                    'cl':list[index]['DtMD_sStuff'], //材料名称
                    'cz':list[index]['DtMD_sMaterial'],//材质
                    'gg':list[index]['DtMD_sSpecification'],//规格
                    'zl':list[index]['groupWeightSum'].toFixed(2),//重量
                    'lsjzl':list[index]['groupWeightSum'].toFixed(2),//螺栓件重量
                    'dhjzl':'0.00',//电焊件重量
                    'zks':'0',//总孔数
                    'hq': '0', //制弯
                    'zjsl':list[index]['groupCountSum'],//总计数量
                };
                tb_tmp.push(tmp);
                weight_sum += tmp['zl']*100;
                count_sum += Number(tmp['zjsl']);
               
                cz_weight_tmp += tmp['zl']*100; //小数转成整数计算
                cz_count_tmp += Number(tmp['zjsl']);

                if(index==(list.length-1) || list[index]['DtMD_sMaterial'] != list[index+1]['DtMD_sMaterial'] || list[index]['DtMD_sStuff'] != list[index+1]['DtMD_sStuff']){
                    let czxj_row={
                        'cl':"", //材料名称
                        'cz':"材质小计",//材质
                        'gg':"",
                        'zl': (cz_weight_tmp/100).toFixed(2),
                        'lsjzl':(cz_weight_tmp/100).toFixed(2),
                        'dhjzl':'0.00',//电焊件重量
                        'zks':'0',//总孔数
                        'hq': '0', //制弯
                        'zjsl':cz_count_tmp
                    };
                    tb_tmp.push(czxj_row)
                    cl_weight_tmp+=cz_weight_tmp;
                    cl_count_tmp+=cz_count_tmp;
                    cz_weight_tmp = 0;
                    cz_count_tmp = 0;
                }

                if(index==(list.length-1) || list[index]['DtMD_sStuff'] != list[index+1]['DtMD_sStuff']){
                    let clxj_row={
                        'cl':"", //材料名称
                        'cz':"材料小计",//材质
                        'gg':"",
                        'zl': cl_weight_tmp/100,
                        'lsjzl':cl_weight_tmp/100,
                        'dhjzl':'0.00',//电焊件重量
                        'zks':'0',//总孔数
                        'hq': '0', //制弯
                        'zjsl':cl_count_tmp
                    };
                    tb_tmp.push(clxj_row);
                    cl_weight_tmp = 0;
                    cl_count_tmp = 0;
                }
                if(index==(list.length-1)){
                    let zj_row={
                        'cl':"", //材料名称
                        'cz':"总计",//材质
                        'gg':"",
                        'zl': weight_sum/100,
                        'lsjzl':weight_sum/100,
                        'dhjzl':'0.00',//电焊件重量
                        'zks':'0',//总孔数
                        'hq': '0', //制弯
                        'zjsl':count_sum
                    };
                    tb_tmp.push(zj_row);

                }       
            }

            let data_tmp={
                gc:mainInfos['CDM_CProject'],
                tx:mainInfos['CDM_TypeName'],
                rq:mainInfos['idate'],
                tb:tb_tmp
            };
            

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: hzbdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        bjmxprint: function() {
            let list = Config.detailList;
            let mainInfos = Config.mainInfos;
            console.log('list', list);
            let tb_tmp = [];
            
            let weight_sum = 0;
            let count_sum = 0;

            let typeObj = {};
            list.forEach(el => {
                weight_sum += el['WeightSum']*100;
                count_sum += Number(el['sl'])
                let k = el.DtMD_sStuff+el.DtMD_sMaterial+el.DtMD_sSpecification;
                if(!typeObj[k]){
                    typeObj[k] = [];
                }
                typeObj[k].push(el);
            });
            // console.log(typeObj)
            for (const key in typeObj) {
                let weight_tmp = 0;
                let count_tmp = 0;
                let arrTmp = typeObj[key].map((value,index)=>{
                    weight_tmp += value['WeightSum']*100;
                    count_tmp += Number(value['sl']);
                    
                    return {
                        'xh':(index+1).toString(),
                        'bjbh':value['DtMD_sPartsID'], 
                        'cz':value['DtMD_sMaterial'],//材质
                        'gg':value['DtMD_sSpecification'],//规格
                        'cd':value['DtMD_iLength'].toString(),
                        'kd':value['DtMD_fWidth'].toString(),
                        'DtMD_iTorch': value['DtMD_iTorch'] || '',
                        'type': value['type'],
                        'sl':value['sl'].toString(),
                        'plfs':'',
                        'bz':value['DtMD_sRemark'],
                        'zzl':value['WeightSum'].toFixed(2),
                    };
                })
                arrTmp.push({
                    'xh':'',
                    'bjbh':'小计', 
                    'cz':'',
                    'gg':'',//规格
                    'cd':'',
                    'kd':'',
                    'sl':count_tmp,
                    'plfs':'',
                    'bz':'',
                    'zzl':(weight_tmp/100).toFixed(2),
                })
                
                tb_tmp = tb_tmp.concat(arrTmp);
                // console.log('tb_tmp', tb_tmp);
            }

            tb_tmp.push({
                'xh':'',
                'bjbh':'总计', 
                'cz':'',
                'gg':'',//规格
                'cd':'',
                'kd':'',
                'sl':count_sum,
                'plfs':'',
                'bz':'',
                'zzl':(weight_sum/100).toFixed(2),
            })

            let data_tmp={
                gcmc:mainInfos['CDM_CProject'],
                tx:mainInfos['CDM_TypeName'],
                rq:mainInfos['idate'],
                zb:mainInfos['Writer'],
                sh:mainInfos['Auditor'],
                tb:tb_tmp
            };
            

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: bjmxdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        bjmxprintbybh: function() {
            let list = Config.detailList;
            let mainInfos = Config.mainInfos;
            console.log('list', list);
            let tb_tmp = [];
            
            let weight_sum = 0;
            let count_sum = 0;

            let typeObj = {};
            list.forEach(el => {
                weight_sum += el['WeightSum']*100;
                count_sum += Number(el['sl'])
                let k = el.DtMD_sStuff+el.DtMD_sMaterial+el.DtMD_sSpecification;
                if(!typeObj[k]){
                    typeObj[k] = [];
                }
                typeObj[k].push(el);
            });
            // console.log(typeObj)
            for (const key in typeObj) {
                let weight_tmp = 0;
                let count_tmp = 0;
                let arrTmp = typeObj[key].map((value,index)=>{
                    weight_tmp += value['WeightSum']*100;
                    count_tmp += Number(value['sl']);
                    
                    return {
                        'xh':(index+1).toString(),
                        'bjbh':value['DtMD_sPartsID'], 
                        'cz':value['DtMD_sMaterial'],//材质
                        'gg':value['DtMD_sSpecification'],//规格
                        'cd':value['DtMD_iLength'].toString(),
                        'kd':value['DtMD_fWidth'].toString(),
                        'DtMD_iTorch': value['DtMD_iTorch'] || '',
                        'type': value['type'],
                        'sl':value['sl'].toString(),
                        'plfs':'',
                        'bz':value['DtMD_sRemark'],
                        'zzl':value['WeightSum'].toFixed(2),
                    };
                })
                arrTmp.push({
                    'xh':'',
                    'bjbh':'小计', 
                    'cz':'',
                    'gg':'',//规格
                    'cd':'',
                    'kd':'',
                    'sl':count_tmp,
                    'plfs':'',
                    'bz':'',
                    'zzl':weight_tmp/100,
                })
                
                tb_tmp = tb_tmp.concat(arrTmp);
                // console.log('tb_tmp', tb_tmp);
            }

            tb_tmp.push({
                'xh':'',
                'bjbh':'总计', 
                'cz':'',
                'gg':'',//规格
                'cd':'',
                'kd':'',
                'sl':count_sum,
                'plfs':'',
                'bz':'',
                'zzl':weight_sum/100,
            })

            let data_tmp={
                gcmc:mainInfos['CDM_CProject'],
                tx:mainInfos['CDM_TypeName'],
                rq:mainInfos['idate'],
                zb:mainInfos['Writer'],
                sh:mainInfos['Auditor'],
                tb:tb_tmp
            };
            

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: bjmxdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        api: {
            bindevent: function (field='edit') {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data;
                    }
                    return false;
                });
                var auditor = Config.auditor;
                var flag = judge_approve(auditor);
                $(document).on("click", "#export", function () {
                    window.open('/admin.php/chain/lofting/c_diagram_buy_main/export/ids/' + Config.ids);
                });
                $(document).on('click', "#chooseDiagram", function(e){
                    if(flag){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                console.log(value.CDM_Num);
                                $("#CDM_CProject").val(value.CDM_CProject);
                                $("#TD_ID").val(value.TD_ID);
                                $("#CDM_Num").val(value.CDM_Num);
                                $("#CDM_TypeName").val(value.CDM_TypeName);
                                $("#CDM_Pressure").val(value.CDM_Pressure);
                                $("#C_Num").val(value['t.C_Num']);
                                $.ajax({
                                    url: 'chain/lofting/c_diagram_buy_main/getChooseDiagram',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {CDM_Num: value.CDM_Num},
                                    success: function (ret) {
                                        var content = '';
                                        if(ret.code==1){
                                            content = ret.data;
                                        }
                                        $("#tbshow").html(content);
                                    }, error: function (e) {
                                        Backend.api.toastr.error(e.message);
                                    }
                                });
                            }
                        };
                        Fast.api.open('chain/lofting/c_diagram_buy_main/chooseDiagram',"选择图纸塔型",options);
                    }
                    
                })
                $(document).on('click', ".del", function(e){
                    if(flag){
                        var indexChoose = $(this).parents('tr').index();
                        var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[CDB_ID][]"]').val();
                        if(num!=0){
                            var index = layer.confirm("即将删除该段所有信息，确定删除？", {
                                btn: ['确定', '取消'],
                            }, function(data) {
                                $.ajax({
                                    url: 'chain/lofting/c_diagram_buy_main/delDetail',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {num: num},
                                    success: function (ret) {
                                        Layer.msg(ret.msg);
                                        if(ret.code==1){
                                            $( e.target ).closest("tr").remove();
                                        }
                                    }, error: function (e) {
                                        Backend.api.toastr.error(e.message);
                                    }
                                });
                                layer.close(index);
                            })
                        }else{
                            $( e.target ).closest("tr").remove();
                        }
                    }
                    
                });
                $(document).on('click', "#table_add", function(){
                    if(flag){
                        var field_append='<tr>'
                                +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                        for(var i=0;i<tableField.length;i++){
                            field_append += '<td '+tableField[i][6]+'><input class="small_input" type="'+tableField[i][2]+'" '+tableField[i][4]+' name="table_row['+tableField[i][1]+'][]" value="'+tableField[i][5]+'"></td>';
                        }
                        field_append += '</tr>';
                        $("#tbshow").append(field_append);
                    }
                });
                $(document).on('click', "#allocation", function(){
                    if(flag){
                        var CDBM_Num = $("#CDBM_Num").val();
                        var TD_ID= $("#TD_ID").val();
                        if(TD_ID){
                            var index = layer.confirm("是否确认覆盖原有数据？", {
                                btn: ['确定', '取消'],
                            }, function(data) {
                                layer.msg("调拨中，请稍等");
                                $.ajax({
                                    url: 'chain/lofting/c_diagram_buy_main/allocationDiagram',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {CDBM_Num: CDBM_Num,TD_ID: TD_ID},
                                    async: false,
                                    success: function (ret) {
                                        var content = '';
                                        if(ret.code==1){
                                            content = ret.data;
                                            $("#tbshow").html(content);
                                        }else{
                                            layer.msg(ret.msg);
                                        }
                                        
                                    }, error: function (e) {
                                        Backend.api.toastr.error(e.message);
                                    }
                                });
                                layer.close(index);
                            })
                        }else{
                            layer.msg("未关联塔型不能调拨杆塔汇总配段");
                        }
                    }
                });

                function judge_approve(auditor='')
                {
                    var judge_flag = true;
                    if(auditor) judge_flag = false;
                    return judge_flag;
                }

                $(document).on("click", "#author", function () {
                    var num = $("#CDBM_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到编码，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/lofting/c_diagram_buy_main/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#CDBM_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到编码，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/lofting/c_diagram_buy_main/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
            }
        }
    };
    return Controller;
});