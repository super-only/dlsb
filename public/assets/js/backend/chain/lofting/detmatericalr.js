define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'fytxsjdata'], function ($, undefined, Backend, Table, Form) {
    tableField = typeof tableField=="undefined"?"":tableField;
    // metailtype = typeof metailtype=="undefined"?[]:metailtype;
    closeData = typeof closeData=="undefined"?[]:closeData;
    selfTableContent = typeof selfTableContent=="undefined"?[]:selfTableContent;
    selfArr = typeof selfArr=="undefined"?[]:selfArr;
    tdId = typeof tdId=="undefined"?"":tdId;
    ids = typeof ids=="undefined"?"":ids;
    detailTableField = typeof detailTableField=="undefined"?[]:detailTableField;
    // var ids = tdId;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/detmatericalr/index' + location.search,
                    add_url: 'chain/lofting/detmatericalr/add',
                    edit_url: 'chain/lofting/detmatericalr/edit',
                    del_url: 'chain/lofting/detmatericalr/del',
                    multi_url: 'chain/lofting/detmatericalr/multi',
                    import_url: 'chain/lofting/detmatericalr/import',
                    table: 'dtmaterial',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'DtM_iID_PK',
                sortName: 'DtM_iID_PK',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                rowStyle: Controller.api.rowStyle,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'T_Company', title: __('T_company'), operate: false},
                        {field: 'm.DtM_sTypeName', title: __('Dtm_stypename'), operate: 'LIKE', visible: false},
                        {field: 'task.T_Num', title: __('T_Num'),visible: false, operate: 'LIKE'},
                        {field: 'T_Num', title: __('T_Num'), operate: false},
                        {field: 'T_Sort', title: __('T_sort'), operate: false},
                        {field: 'DtM_sProject', title: __('Dtm_sproject'), operate: 'LIKE'},
                        {field: 'DtM_sTypeName', title: __('Dtm_stypename'), operate: false},
                        {field: 'DtM_sPressure', title: __('Dtm_spressure'), operate: false},
                        {field: 'DtM_sRemark', title: '备注', operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            
            $("#orderbydjhprint").click(function () {
                window.top.Fast.api.open('chain/lofting/detmatericalr/orderByDjhprint/ids/' + ids, '按段件号排序', {
                    area: ["100%", "100%"]
                });
            });
            $("#orderbyggxhprint").click(function () {
                window.top.Fast.api.open('chain/lofting/detmatericalr/orderByGgxhprint/ids/' + ids, '按材料规格排序', {
                    area: ["100%", "100%"]
                });
            });
            $("#gjmxprint").click(function () {
                window.top.Fast.api.open('chain/lofting/detmatericalr/gjmxPrint/ids/' + ids, '打印构件明细表', {
                    area: ["100%", "100%"]
                });
            });

            Controller.api.bindevent("edit");
			var author = $("#author").prop("disabled");
            if(author){
                $("#table_add").prop("disabled",true);
                $("#show thead").find("tr").children('th').eq(0).prop("hidden",true);
                $("#tbshow").find("tr").each(function () {
                    var checkedField = $(this).children('td').eq(0);
                    checkedField.prop("hidden",true);
                });
            }
            $(document).on('click', "#copyTower", function(e){
                var index = layer.confirm("如果已经有存在的段，选择复制，将覆盖原来的段。你确定要复制塔型嘛？", {
                    btn: ['确定', '取消'],
                }, function(data) {
                    openCopyField();
                    layer.close(index);
                })
                
                
            });
            function openCopyField(){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        else layer.msg("请稍等，正在复制中");
                        $.ajax({
                            url: 'chain/lofting/detmatericalr/copyTowerSect/ids/'+ids,
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            data: {data: JSON.stringify(value.data),way: value.way},
                            async: false,
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    if (ret.code === 1) {
                                        window.location.reload();
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }
                };
                Fast.api.open('chain/lofting/detmatericalr/downField/ids/'+ids,"复制塔型",options);
            };
            $(document).on('click', "#closeTower", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                    }
                };
                Fast.api.open('chain/lofting/detmatericalr/closeTower/ids/'+ids,"合并塔型",options);
            });
            $(document).on('click', "#copyDetail", function(e){
                layer.msg("正在调拨中，请稍等");
                $.ajax({
                    url: 'chain/lofting/detmatericalr/copyDetail',
                    type: 'post',
                    dataType: 'json',
                    async:false,
                    data: {ids: ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#author", function () {
                check('审核之后无法修改，确定审核？',"chain/lofting/detmatericalr/auditor",Config.ids);
                $.ajax({
                    url: 'chain/lofting/detmatericalr/copyDetail',
                    type: 'post',
                    dataType: 'json',
                    async:false,
                    data: {ids: Config.ids}
                });
            });
            $(document).on("click", "#giveupA", function () {
                check('确定弃审？',"chain/lofting/detmatericalr/giveUpA",Config.ids);
                
            });
            function check(msg,url,num){
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                Layer.msg(ret.msg);
                                if (ret.code >= 1) {
                                    // if(ret.code == 2){
                                    //     Fast.api.ajax({
                                    //         url: "chain/lofting/produce_task/savepdf/PT_Num/"+PT_Num,
                                    //         type: "post"
                                    //     });
                                    // }
                                    window.location.reload();
                                }
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }

            $(document).on('click', "#import", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["60%","40%"],
                    callback:function(value){
                        if(value == 1) window.location.reload();
                        else layer.msg("导入失败"); 
                    }
                };
				Fast.api.open('chain/lofting/detmatericalr/importView/ids/'+ids,"导入放样部件",options);
            });
            $(document).on('click', "#batchSetting", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["60%","40%"],
                    callback:function(value){
                        if(value == 1) window.location.reload();
                        else layer.msg("设置失败"); 
                    }
                };
				Fast.api.open('chain/lofting/detmatericalr/batchSetting/ids/'+ids,"批量设置件号类型",options);
            });
            $(document).on("click", "#export", function () {
                if(ids == false){
                    layer.confirm('请重试！');
                    return false;
                }
                window.open('/admin.php/chain/lofting/detmatericalr/export/ids/'+ids);
            });
            $(document).on('click', "#table_add", function(){
                var field_append='<tr>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-success detail"><i class="fa fa-calendar"></i></a></td>';
                for(var i=0;i<tableField.length;i++){
                    field_append += '<td '+tableField[i][6]+'><input type="'+tableField[i][2]+'" '+tableField[i][4]+' name="table_row['+tableField[i][1]+'][]" value="'+tableField[i][5]+'"></td>';
                }
                field_append += '</tr>';
                $("#tbshow").append(field_append);
            });
            $(document).on('click', ".del", function(e){
                var indexChoose = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(2).find('input').val();
                if(num!=0){
                    var index = layer.confirm("即将删除该段所有信息，确定删除？", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: 'chain/lofting/detmatericalr/delDetail',
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                layer.msg(ret.msg);
                                if(ret.code==1){
                                    $( e.target ).closest("tr").remove();
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }else{
                    $( e.target ).closest("tr").remove();
                }
                
                
            });
            $(document).on('click', ".detail", function(e){
                var indexChoose = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(2).find('input').val();
                if(num==0){
                    var index = layer.confirm("请先保存段", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        layer.close(index);
                    })
                }else{
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            window.location.reload();
                        }
                    };
                    Fast.api.open('chain/lofting/detmatericalr/detail/ids/'+num,"杆塔型部件明细",options);
                }
                
            });
            
            $(document).on("click", "#partImport", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["500px","300px"],
                };
                Fast.api.open('chain/lofting/detmatericalr/partImport/ids/'+ids,"上传文件压缩包",options);
            });
        },
        batchsetting: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        importview: function () {
            Form.api.bindevent($("form[role=form]"),function(){
                parent.location.reload();
            });
        },
        partimport: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        detailimport: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        detail: function () {
            Controller.api.bindevent("detail");
            var nav_ini_index = localStorage.getItem('nav_index');
            if(nav_ini_index){
                $.each($(".nav-tabs li"),function(index,e){
                    $(e).removeClass("active");
                    if(index==nav_ini_index) $(e).addClass("active");
                });
                $.each($("#myTabContent div"),function(index,e){
                    $(e).removeClass("active in");
                    if(index==nav_ini_index) $(e).addClass("active in");
                });
            }
            var height = document.body.clientHeight-$("#topDiv").height()-100;
            $(".table-responsive").height(height);
            $(document).on('click', "#table_add", function(){
                var field_append='<tr>'
                        +'<td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a><a href="javascript:;" class="btn btn-xs btn-info detailImport"><i class="fa fa-upload"></i></a></td>'
                        +'<td><input type="checkbox"></td>'
                        +'<td>'+DtS_Name+'</td>';
                for(var i=0;i<detailTableField.length;i++){
                    var class_name = "small_input";
                    field_append += '<td '+detailTableField[i][4]+'><input class="'+class_name+'" type="'+detailTableField[i][2]+'" data-rule="'+detailTableField[i][3]+'" name="'+detailTableField[i][1]+'[]" value=""></td>';
                }
                field_append += '</tr>';
                $("#tbshow").append(field_append);
            });
            var thisIds = ids;
            $(document).on('change', "#searchNum", function(){
                var lookIds = $("#searchNum").find("option:selected").val();
                if(lookIds==thisIds || lookIds=='') return false;
                else window.location.href = "edit/ids/"+lookIds;
            });
            $(document).on('click', ".btn-success", function(e){
                var formData = $("#detail-form").serializeArray();
                // return false;
                $.ajax({
                    url: 'chain/lofting/detmatericalr/detail/ids/'+ids,
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData)},
                    success: function (ret) {
                        layer.msg(ret.msg);
                        if (ret.hasOwnProperty("code")) {
                            if (ret.code === 1) {
                                window.location.reload();
                            }
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on('click', ".del", function(e){
                if(Config.check_flag){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(4).find('input').val();
                    if(num!=''){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/lofting/detmatericalr/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                }
            });

            $(document).on('click', ".copy", function(e){
                var copyChoose = $(this).parents('tr').html();
                var text = copyChoose.replace(/name\=\"DtMD\_ID\_PK\[\]\" value\=\"\d+\"/,'name="DtMD_ID_PK[]" value=""');
                $("#tbshow").append("<tr>"+text+"</tr>");
            });
            $('#tbshow').on('keyup','td input[name="DtMD_sMaterial[]"]',function () {
                var index = $(this).parents('tr').index();
                var DtMD_sPartsID = $(this).parents('tbody').find('tr').eq(index).find('td').eq(4).find('input').val().trim();
                if(DtMD_sPartsID != ""){
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td').eq(5).find('input').val().trim();
                    var list = ['Q235','Q355','Q420','Q235B','Q355B','Q420B','Q460B','HH'];
                    var field_name = list[num];
                    if(field_name !== undefined){
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(5).find('input').val(field_name);
                    }
                }
                
            })
            $('#tbshow').on('keyup','td input[name="DtMD_sMaterial[]"]',function () {
                var index = $(this).parents('tr').index();
                var DtMD_sPartsID = $(this).parents('tbody').find('tr').eq(index).find('td').eq(5).find('input').val().trim();
                if(DtMD_sPartsID != ""){
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td').eq(6).find('input').val().trim();
                    var list = ['Q235','Q355','Q420','Q235B','Q355B','Q420B','Q460B','HH'];
                    var field_name = list[num];
                    if(field_name !== undefined){
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(6).find('input').val(field_name);
                    }
                }
                
            })
            $('#tbshow').on('keyup','td input[name="DtMD_sStuff[]"]',function () {
                var index = $(this).parents('tr').index();
                var DtMD_sPartsID = $(this).parents('tbody').find('tr').eq(index).find('td').eq(6).find('input').val().trim();
                if(DtMD_sPartsID != ""){
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td').eq(7).find('input').val().trim();
                    var list = ['角钢','钢板','圆钢','槽钢','钢管','花纹钢板','格栅板','工字钢','法兰','锥形管'];
                    var symbol = ['∠','-','Φ','[','Φ','-','G','I','Φ','Z'];
                    var field_name = list[num];
                    if(field_name !== undefined){
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(7).find('input').val(field_name);
                        $(this).parents('tbody').find('tr').eq(index).find('td').eq(8).find('input').val(symbol[num]);
                    }
                }
                
            })
            $('#tbshow').on('keyup','td input[name="DtMD_sSpecification[]"]',function () {
                var spec = $(this).val();
                var first_string = spec.substring(0,1);
                var itorch = 0;
                if(first_string=='∠'){
                    var localwz = spec.lastIndexOf("*");
                    itorch = localwz==-1?0:spec.substring(localwz+1);
                }else if(first_string=="-"){
                    itorch = spec.substring(1);
                }
                $(this).parents("tr").find('td input[name="DtMD_iTorch[]"]').val(itorch);
                $(this).parents("tr").find('td input[name="DtMD_iTorch[]"]').text(itorch);
            })
            $(document).on('click','#edit-number',function () {
                var symbol = $("#symbol").val();
                var num = $("#edit-num").val();
                if(num <= 0){
                    layer.msg("数量不可以小于1");
                    return false;
                }
                var formData = $("#detail-form").serializeArray();
                searchDetail(formData,symbol,num);
            })
            function searchDetail(formData={},symbol="",num=""){
                $.ajax({
                    url: 'chain/lofting/detmatericalr/detail/ids/'+ids,
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData),symbol:symbol, num:num},
                    success: function (ret) {
                        if (ret.hasOwnProperty("code")) {
                            if (ret.code === 1) {
                                Layer.msg(ret.msg);
                                window.location.reload();
                            }
                        }else{
                            Layer.msg(ret.msg);
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            $(document).on('click', "#editB", function(){
                
                var type = "editB";
                editDetailAll(thisIds,type);
            });
            $(document).on('click', "#editC", function(){
                var type = "editC";
                editDetailAll(thisIds,type);
            });
            $(document).on('click', "#giveupB", function(){
                var type = "giveupB";
                editDetailAll(thisIds,type);
            });
            $(document).on('click', "#giveupC", function(){
                var type = "giveupC";
                editDetailAll(thisIds,type);
            });
            function editDetailAll(ids='',type=''){
                if(ids=='' || type==''){
                    layer.msg("请刷新后重试！");
                }else{
                    $.ajax({
                        url: 'chain/lofting/detmatericalr/editDetailAll',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {ids:ids,type:type},
                        success: function (ret) {
                            Layer.msg(ret.msg);
                            if (ret.code === 1) {
                                window.location.reload();
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }
                
            }
            $(document).on("click", "#all_add", function () {
                $("#tbshow").find("tr").each(function () {
                    var checkedField = $(this).children('td').eq(1).find('input');
                    checkedField.prop("checked","true");
                });
            });
            $(document).on("click", "#giveup_add", function () {
                $("#tbshow").find("tr").each(function () {
                    var checkedField = $(this).children('td').eq(1).find('input');
                    checkedField.prop("checked","");
                });
            });

            $(document).on('click', "#copy_add", function(){
                var copyChoose,text;
                $("#tbshow").find("tr").each(function () {
                    if($(this).children('td').eq(1).find('input').prop("checked")){
                        $(this).children('td').eq(1).find('input').prop("checked","");
                        copyChoose = $(this).html();
                        text += "<tr style='background-color: #E0FFFF'>"+copyChoose.replace(/name\=\"DtMD\_ID\_PK\[\]\" value\=\"\d+\"/,'name="DtMD_ID_PK[]" value=""')+"</tr>";
                    }
                });
                $("#tbshow").append(text);
            });
            $(document).on('click', ".nav-tabs li", function(){
                localStorage.setItem('nav_index', $(this).index());
            });
            $(document).on("keyup", "#tbshow input[type='text']", function(e){
                var change_val = $(this).val();
                $(e.target).attr("value",change_val);
            });
            $(document).on("click", "#tbshow input[type='checkbox']", function(e){
                var flag = $(this).prop("checked");
                $(e.target).attr("checked",flag);
            });
            $(document).on('click', "#stick", function(e){
                var checkedField = localStorage.getItem('commonCheckedField');
                if(checkedField){
                    $('#tbshow').append(checkedField);
                }else layer.msg("粘贴失败");
            });
            $(document).on("click", "#partImport", function () {
                // if(Config.check_flag){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["500px","300px"],
                    };
                    Fast.api.open('chain/lofting/detmatericalr/partImport/ids/'+Config.dids,"上传文件压缩包",options);
                // }
            });

            $(document).on("click", ".detailImport", function () {
                // if(Config.check_flag){
                    var detailId = $(this).parents("tr").find("td input[name='DtMD_ID_PK[]']").val();
                    if(detailId){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["500px","300px"],
                        };
                        Fast.api.open('chain/lofting/detmatericalr/detailImport/ids/'+detailId,"上传单文件",options);
                    }else{
                        layer.msg("请先保存后，在上传文件");
                    }
                // }
                
            });
        },
        downfield: function () {
            var height = document.body.clientHeight-($("#down-from").height()+$("#down-form").height());
            var fieldTable = $("#fieldTable");
            fieldTable.bootstrapTable({
                data: [],
                search: false,
                pagination: false,
                height: 600,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DtS_ID_PK', title: 'id',visible: false},
                        {field: 'DtS_Name', title: '段名'},
                    ]
                ]
            });
            Table.api.bindevent(fieldTable);
            var selfTable = $("#selfTable");
            selfTable.bootstrapTable({
                data: selfTableContent,
                search: false,
                pagination: false,
                height: 600,
                columns: [
                    [
                        {field: 'DtS_Name', title: '已调拨段名'},
                    ]
                ]
            });
            Table.api.bindevent(selfTable);
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data:[],
                clickToSelect: true,
                singleSelect: true,
                height:height,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DtM_sTypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'DtM_sPictureNum', title: '图号', operate: 'LIKE'},
                        {field: 'DtM_sPressure', title: '电压等级', operate: 'LIKE'},
                        {field: 'type', title: '复制类型',visible:false}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('check.bs.table',function(row,$e){
                var num = $e.DtM_iID_PK;
                var searchData = {type:$e.type,num:$e.DtM_iID_PK};
                if(num){
                    $.ajax({
                        url: 'chain/lofting/library/downDetailContent',
                        type: 'post',
                        dataType: 'json',
                        data: searchData,
                        success: function (ret) {
                            if(ret.code==1){
                                fieldTable.bootstrapTable('load',ret.data);
                                
                            }else{
                                Layer.msg("请重试");
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }else{
                    Layer.msg("请先勾选");
                }
            })

            $(document).on('click', "#copy", function(){
                var tableContent = fieldTable.bootstrapTable('getSelections');
                if(tableContent.length == 0){
                    layer.msg("请先选择");
                    return false;
                }
                var cfName = [];
                var way = $("input[name='way']:checked").val();
                $.each(tableContent,function(k,v){
                    if($.inArray(parseInt(v["DtS_Name"]),selfArr)==-1){
                        cfName.push(v["DtS_Name"]);
                    }
                });
                if(cfName.length!=0){
                    var index = layer.confirm("已调拨段中不存在"+cfName.toString()+"这些段，这些段将不被复制，确定继续复制吗？", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        layer.close(index);
                        Fast.api.close({"data":tableContent,"way":way});
                    })
                }else{
                    Fast.api.close({"data":tableContent,"way":way});
                }
                
            });

            ajaxTowerFromData("chain/lofting/detmatericalr/downFieldList/type/tower",{},typeColumns());
            $(document).on('change', "input[name='from']", function(){
                var value = $(this).val();
                var url = "chain/lofting/detmatericalr/downFieldList/type/"+value;
                var dataSearch = $("#down-form").serializeArray();
                dataSearch[dataSearch.length] = {'name': 'ids', 'value': ids};
                var columns = typeColumns(value);
                ajaxTowerFromData(url,dataSearch,columns);
            });

            $(document).on('click', "#search", function(){
                fieldTable.bootstrapTable('load',[]);
                var value = $("input[name='from']:checked").val()
                var url = "chain/lofting/detmatericalr/downFieldList/type/"+value;
                var dataSearch = $("#down-form").serializeArray();
                dataSearch[dataSearch.length] = {'name': 'ids', 'value': ids};
                var columns = typeColumns(value);
                ajaxTowerFromData(url,dataSearch,columns);
            });

            function typeColumns(value='tower'){
                var data = [];
                if(value == "tower"){
                    data = [
                        [
                            {checkbox: true},
                            {field: 'DtM_sTypeName', title: '塔型'},
                            {field: 'DtM_sPictureNum', title: '图号'},
                            {field: 'DtM_sPressure', title: '电压等级'},
                            {field: 'type', title: '复制类型',visible:false}
                        ]
                    ];
                }else if(value == "project"){
                    data = [
                        [
                            {checkbox: true},
                            {field: 'T_Num', title: '任务单号'},
                            {field: 'DtM_sProject', title: '工程名称'},
                            {field: 'DtM_sTypeName', title: '塔型'},
                            {field: 'DtM_sPressure', title: '电压等级'},
                            {field: 'type', title: '复制类型',visible:false}
                        ]
                    ];
                }else{
                    data = [
                        [
                            {checkbox: true},
                            {field: 'DtM_sTypeName', title: '塔型'},
                            {field: 'DtM_sPressure', title: '电压等级'},
                            {field: 'type', title: '复制类型',visible:false}
                        ]
                    ];
                }
                return data;
            }
            function ajaxTowerFromData(url='',dataSearch={},colums=[]){
                if(url == '') return false;
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: dataSearch,
                    height: height,
                    success: function (ret) {
                        var content = [];
                        if(ret.code==1){
                            content = ret.data;
                            table.bootstrapTable("destroy");
                            // 初始化表格
                            table.bootstrapTable({
                                data:content,
                                clickToSelect: true,
                                singleSelect: true,
                                columns: colums
                            });
                        }else{
                            Layer.msg("请重试");
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
                // 为表格绑定事件
                Table.api.bindevent(table);
            }
        },
        closetower: function () {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: closeData,
                search: false,
                pagination: false,
                pk: 'TD_ID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 't_project', title: '工程名称'},
                        {field: 'C_Num', title: '合同号'},
                        {field: 'T_Num', title: '任务单号'},
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'TD_Pressure', title: '电压等级'},
                        {field: 'TD_ID', title: 'TD_ID'}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            
            $(document).on('click', "#del", function(e){
                var delArr = [];
                var selectArr = table.bootstrapTable('getSelections');
                if(selectArr.length == 0){
                    layer.msg("请选择要删除的塔型");
                    return false;
                }
                selectArr.forEach(function(element) {
                    delArr.push(element.TD_ID); 
                });
                table.bootstrapTable('remove',{field:"TD_ID",values:delArr});
            });
            $(document).on('click', "#save", function(e){
                var saveArr = [];
                var selectArr = table.bootstrapTable('getSelections');
                if(selectArr.length == 0) {
                    layer.msg("请选择要保存的塔型");
                    return false;
                }
                selectArr.forEach(function(element) {
                    saveArr.push(element.TD_ID); 
                });
                $.ajax({
                    url: 'chain/lofting/detmatericalr/closeTowerSave',
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(saveArr),tdId:tdId},
                    success: function (ret) {
                        if(ret.code==1){
                            layer.msg("保存成功");
                            window.location.reload();
                        }else{
                            layer.msg("请重试");
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
                
            });
            $(document).on('click', "#chooseCopyTower", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        var tableContent = table.bootstrapTable('getData',true);
                        var issetContent = [];
                        if(tableContent.length == 0){
                            table.bootstrapTable('append',value);
                        }else{
                            tableContent.forEach(function(element) {
                                issetContent.push(element.TD_ID); 
                            });
                            value.forEach(function(element) {
                                if($.inArray(element.TD_ID,issetContent)==-1) table.bootstrapTable('append',element);
                            });
                        }
                        
                    }
                };
                Fast.api.open('chain/lofting/detmatericalr/selectProject',"选择合并塔型",options);
            });
            
        },
        selectproject: function() {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/lofting/detmatericalr/selectProject' + location.search,
                    table: 'tasksect',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'T_Num',
                sortName: 't.T_WriterDate',
                sortOrder: 'DESC',
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'TD_TypeName', title: __('TD_TypeName'), visible: false, operate: 'LIKE'},
                        {field: 'C_Num', title: __('C_Num'), operate: false},
                        {field: 'PC_Num', title: __('PC_Num'), operate: 'LIKE'},
                        {field: 'T_Num', title: __('T_Num'), operate: false},
                        {field: 'T_Sort', title: __('T_Sort'), operate: false},
                        {field: 'T_Company', title: __('T_Company'), operate: false},
                        {field: 'T_WriterDate', title: __('T_WriterDate'), operate:false},
                        {field: 't_project', title: __('t_project'), operate: 'LIKE'},
                        {field: 'TD_TypeName', title: __('TD_TypeName'), operate: false},
                        {field: 'TD_TypeNameGY', title: __('TD_TypeNameGY'), operate: false},
                        {field: 'TD_Pressure', title: __('TD_Pressure'), operate:false},
                        {field: 'TD_Count', title: __('TD_Count'), operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                Fast.api.close(tableContent);
                
            });
        },
        orderbyggxhprint: function() {
            
            const pageNum = 32; //模板不变的情况下固定
            let detailArr=Config.detailArr;
            let mainInfos = Config.mainInfos;
            // console.log('detailArr', detailArr)
            let dataArr = [];
            let byzsl = 0;
            let byzzl = 0;
            let byzks = 0;
            let tb_tmp = [];

            for(let index=0;index<detailArr.length;index++){
                byzsl = byzsl+Number(detailArr[index]['DtMD_iUnitCount']);
                byzzl = Math.floor(byzzl*100 + detailArr[index]['DtMD_fUnitWeight']*100)/100;
                byzks = byzks+Number(detailArr[index]['DtMD_iUnitHoleCount']);
                let tmp={
                    'ljbh':detailArr[index]['DtMD_sPartsID'], //零件编号
                    'clmc':detailArr[index]['DtMD_sStuff'], //材料名称
                    'cz':detailArr[index]['DtMD_sMaterial'],//材质
                    'gg':detailArr[index]['DtMD_sSpecification'],//规格
                    'cd':Number(detailArr[index]['DtMD_iLength']) || '',//长度
                    'kd':Number(detailArr[index]['DtMD_fWidth']) || '',//宽度
                    'DtMD_iTorch': Number(detailArr[index]['DtMD_iTorch']) || '',
                    'type': detailArr[index]['type'],
                    'djsl':detailArr[index]['DtMD_iUnitCount'],//总数量
                    'djzl':detailArr[index]['DtMD_fUnitWeight'],//重量（kg）
                    'ks':detailArr[index]['DtMD_iUnitHoleCount'],//单件孔数
                    'dh':detailArr[index]['DtMD_iWelding'],//电焊
                    'wq':detailArr[index]['DtMD_iFireBending'],//弯曲
                    'qj':detailArr[index]['DtMD_iCuttingAngle'],//切角
                    'cb':detailArr[index]['DtMD_fBackOff'],//铲背
                    'qg':detailArr[index]['DtMD_iBackGouging'],//清根
                    'db':detailArr[index]['DtMD_DaBian'],//打扁
                    'khj':detailArr[index]['DtMD_KaiHeJiao'],//开合角
                    'zk':detailArr[index]['DtMD_ZuanKong'],//钻孔
                    'DtMD_GeHuo': detailArr[index]['DtMD_GeHuo'] || '',
                    'bz':detailArr[index]['DtMD_sRemark'],//备注
                };
                tb_tmp.push(tmp);
                if(((index+1)%pageNum==0 || index==detailArr.length-1) && index!=0){
                    let data_tmp={
                        tx:mainInfos['DtM_sTypeName'],
                        dw:mainInfos['DtS_Name'],
                        js:'1',
                        rq:mainInfos['idate'],
                        zb:mainInfos['DtS_sWriter'],
                        sh:mainInfos['DtS_sAuditor'],
                        ljzsl:mainInfos['number'].toString(),
                        ljzzl:mainInfos['weight'].toString(),
                        ljzks:mainInfos['knumber'].toString(),
                    };
                    // console.log('tb_tmp',tb_tmp);
                    data_tmp['tb']=JSON.parse(JSON.stringify(tb_tmp));;
                    data_tmp['byzsl'] = byzsl;
                    data_tmp['byzzl'] = byzzl;
                    data_tmp['byzks'] = byzks;
                    data_tmp['page'] = '第'+(parseInt((index)/pageNum)+1)+'页，共'+(parseInt((detailArr.length)/pageNum)+1)+'页';
                    // console.log(parseInt((index+1)/pageNum)+1);
                    dataArr.push(data_tmp);
                    byzsl = 0;
                    byzzl = 0;
                    byzks = 0;
                    // tb_tmp.splice(0, tb_tmp.length);
                    tb_tmp = [];
                }
            }

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){
                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: orderbyclggdata});

            $('#p_mx1').html(htemp.getHtml(dataArr));
            pageto('#p_mx1',0);
            $("#handleprintmx1").click(function(){
                htemp.print(dataArr);
            });
        },
        orderbydjhprint: function() {
            let duanArr = Config.duanArr;
            
            let printDataArr = duanArr.map((e) => {
                let detailArr=e.detailArr;
                let mainInfos = e.mainInfos;

                let tb_tmp = detailArr.map((d) => {
                    return {
                        'ljbh':d['DtMD_sPartsID'], //零件编号
                        'clmc':d['DtMD_sStuff'], //材料名称
                        'cz':d['DtMD_sMaterial'],//材质
                        'gg':d['DtMD_sSpecification'],//规格
                        'cd':Number(d['DtMD_iLength']) || '',//长度
                        'kd':Number(d['DtMD_fWidth']) || '',//宽度
                        'DtMD_iTorch': Number(d['DtMD_iTorch']) || '',
                        'type': d['type'],
                        'djsl':d['DtMD_iUnitCount'],//总数量
                        'djzl':d['DtMD_fUnitWeight'],//重量（kg）
                        'ks':d['DtMD_iUnitHoleCount'],//单件孔数
                        'dh':d['DtMD_iWelding'],//电焊
                        'wq':d['DtMD_iFireBending'],//弯曲
                        'qj':d['DtMD_iCuttingAngle'],//切角
                        'cb':d['DtMD_fBackOff'],//铲背
                        'qg':d['DtMD_iBackGouging'],//清根
                        'db':d['DtMD_DaBian'],//打扁
                        'khj':d['DtMD_KaiHeJiao'],//开合角
                        'zk':d['DtMD_ZuanKong'],//钻孔
                        'DtMD_GeHuo': Number(d['DtMD_GeHuo']) || '',
                        'bz':d['DtMD_sRemark'],//备注
                    }
                })

                return {
                    tx:mainInfos['DtM_sTypeName'],
                    dw:mainInfos['DtS_Name'],
                    js:'1',
                    rq:mainInfos['idate'],
                    zb:mainInfos['DtS_sWriter'],
                    sh:mainInfos['DtS_sAuditor'],
                    ljzsl:mainInfos['number'].toString(),
                    ljzzl:mainInfos['weight'].toString(),
                    ljzks:mainInfos['knumber'].toString(),
                    tb: tb_tmp
                }
            })

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
            
                for(let v of mxpage){

                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: orderbydjhdata});
            // console.log('dataArr', dataArr);
            $('#p_mx1').html(htemp.getHtml(printDataArr));
            $("#handleprintmx1").click(function(){
                htemp.print(printDataArr);
            });
            
            pageto('#p_mx1',0);

            let pLengthArr = Array.from($('#p_mx1 .hiprint-printPanel .hiprint-printPaper')).map(e => $(e).find("td[field='ks']").length);
            
            let newPrintDataArr = [];
            printDataArr.map(e=>{
                let tempArr = [];
                let sum = 0;
                
                while(pLengthArr.length>0 && sum<e.tb.length){
                    let pageSize = pLengthArr.shift();
                    let newTb = e.tb.slice(sum, sum+pageSize);

                    sum=sum + pageSize;
                    
                    let countInfo = newTb.reduce((pre, cur) => {
                        pre['djsl']+=cur.djsl*100;
                        pre['djzl']+=cur.djzl*100;
                        pre['ks']+=cur.ks*100;

                        return pre;
                    }, {
                        djsl: 0,
                        djzl: 0,
                        ks: 0
                    })

                    let newPrintData = {...e};
                    newPrintData.tb = newTb;
                    
                    newPrintData['byzks'] = countInfo['ks']/100;
                    newPrintData['byzsl'] = countInfo['djsl']/100;
                    newPrintData['byzzl'] = countInfo['djzl']/100;

                    tempArr.push(newPrintData);
                }

                for (let i = 0; i < tempArr.length; i++) {
                    const temp = tempArr[i];
                    temp['page'] = '第'+(i+1)+'页， 共'+tempArr.length+'页';
                    newPrintDataArr.push(temp);
                }
            });

            $('#p_mx1').html(htemp.getHtml(newPrintDataArr));
            $("#handleprintmx1").click(function(){
                htemp.print(newPrintDataArr);
            });
            
            pageto('#p_mx1',0);

        },
        gjmxprint: function() {
            // const pageNum = 32; //orderbydjhdata模板不变的情况下固定
            let duanArr = Config.duanArr;

            let printDataArr = [];
            for(duanData of duanArr){
                let detailArr=duanData.detailArr;
                let mainInfos = duanData.mainInfos;
                let dataArr = [];
                let tb_tmp = [];

                for(let index=0;index<detailArr.length;index++){
                    
                    let tmp={
                        'bh':detailArr[index]['DtMD_sPartsID'], //编号
                        'gg':detailArr[index]['DtMD_sSpecification'],//规格
                        'cd':Number(detailArr[index]['DtMD_iLength']) || '',//长度
                        'kd':Number(detailArr[index]['DtMD_fWidth']) || '',//宽度
                        'sl':detailArr[index]['DtMD_iUnitCount'],//数量
                        'zl':detailArr[index]['DtMD_fUnitWeight'],//重量（kg）
                        'zlxj':detailArr[index]['DtMD_iUnitCount']*detailArr[index]['DtMD_fUnitWeight'].toFixed(2),//单件孔数
                        'bz':detailArr[index]['DtMD_sRemark'],//备注
                    };
                    // console.log('bz',detailArr[index]['DtMD_sRemark']);
                    tb_tmp.push(tmp);    
                }
                let data_tmp={
                    tx:mainInfos['DtM_sTypeName'],
                    dw:mainInfos['DtS_Name'],
                    js:'1',
                    rq:mainInfos['idate'],
                    zb:mainInfos['DtS_sWriter'],
                    sh:mainInfos['DtS_sAuditor'],
                    ljzsl:mainInfos['number'].toString(),
                    ljzzl:mainInfos['weight'].toString(),
                    ljzks:mainInfos['knumber'].toString(),
                };
                data_tmp['tb']=JSON.parse(JSON.stringify(tb_tmp));;
                dataArr.push(data_tmp); 
                printDataArr = printDataArr.concat(dataArr);
            }

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: gjmxdata});

            $('#p_mx1').html(htemp.getHtml(printDataArr));
            $("#handleprintmx1").click(function(){
                htemp.print(printDataArr);
            });
            
            pageto('#p_mx1',0);
            
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                    }
                    return false;
                });
                $(document).on("click", "#select", function () {
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            if(value.length==0) return false;
                            var tableData = value[0];
                            // $("#c-T_Company").val(tableData.T_Company);
                            $("#c-T_Num").val(tableData.T_Num);
                            $("#c-TD_ID").val(tableData.TD_ID);
                            $("#c-DtM_sProject").val(tableData.t_project);
                            // $("#c-DtM_sSortProject").val(tableData.t_shortproject);
                            $("#c-DtM_sTypeName").val(tableData.TD_TypeName);
                            $("#c-DtM_sPressure").val(tableData.TD_Pressure);
                            $("#c-CustomerName").val(tableData.Customer_Name);
                        }
                    };
                    Fast.api.open('chain/lofting/tower_section/selectProject',"选择工程",options);
                });
                // $("th").mouseover(function (e) {
                //     if (($(this).find("div").length <= 0)) {
                //         $(this).append("<div class='th-sisehandler'></div>")
                //         $(".th-sisehandler").mousedown(function (evt) {
                //             let dragTh = $(this).parent()
                //             let oldClientX = evt.clientX;
                //             let oldWidth = dragTh.width();
                //             let changeSizeLayer = $('<div class="siselayer"></div>');
                //             $("body").append(changeSizeLayer);
            
                //             changeSizeLayer.on('mousemove', function (evt) {
                //                 var newWidth =evt.clientX - oldClientX + oldWidth;
                //                 dragTh.attr('width',Math.max(newWidth,1));
            
                //             });
            
                //             changeSizeLayer.on('mouseup', function (evt) {
                //                 changeSizeLayer.remove();
                //                 dragTh.find('.th-sisehandler').remove();
                //             });
                //         })
                //     }
            
                //     $(this).mouseleave(function () {
                //         $(this).find("div").remove()
                //     })
                // })
            },
            rowStyle: function (row,index) {
                var style = {};
                
                if(row.common){
                    style = {css:{'color':"#FF0000"}};
                }
                return style;
            }
        }
    };
    return Controller;
});