define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/supplement/sup_statistics/index' + location.search
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'TRNM_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                onlyInfoPagination: true,
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'TRNM_WriteDate', title: '日期', defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'C_Num', title: '合同号', operate: 'LIKE'},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'TRNM_Sort', title: '补件类型', searchList:{"工地补件":"工地补件","厂内补件":"厂内补件"}},
                        {field: 'count', title: '总件数', operate: false},
                        {field: 'weight', title:'总重(kg)', operate: false},
                        {
                            field: 'buttons',
                            width: "50px",
                            title: '详情',
                            table: table,
                            operate: false,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    text: '',
                                    title: '详情',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-plus-circle',
                                    url: 'chain/supplement/sup_statistics/detail/num/{TRNM_Num}',
                                },
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                columns: [
                    [
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'DtS_Name', title: '段号'},
                        {field: 'TRN_Project', title: '杆塔号'},
                        {field: 'TRN_TPNum', title: '部件号'},
                        {field: 'TRN_TPNumber', title: '数量'},
                        {field: 'DtMD_sStuff', title: '材料'},
                        {field: 'DtMD_sMaterial', title:'材质'},
                        {field: 'DtMD_sSpecification', title: '规格'},
                        {field: 'DtMD_iLength', title: '长度'},
                        {field: 'DtMD_fWidth', title: '宽度'},
                        {field: 'DtMD_iTorch', title: '厚度'},
                        {field: 'DtMD_iUnitHoleCount', title: '单件孔数'},
                        {field: 'TRN_TPWeight', title:'单重'},
                        {field: 'sum_weight', title: '总重'},
                        {field: 'type', title:'类型'},
                        {field: 'DtMD_sRemark', title:'工艺备注'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});