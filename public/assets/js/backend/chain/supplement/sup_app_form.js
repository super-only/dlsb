define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'blddata',], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/supplement/sup_app_form/index' + location.search,
                    add_url: 'chain/supplement/sup_app_form/add',
                    edit_url: 'chain/supplement/sup_app_form/edit',
                    // del_url: 'chain/supplement/sup_app_form/del',
                    // multi_url: 'chain/supplement/sup_app_form/multi',
                    // import_url: 'chain/supplement/sup_app_form/import',
                    table: 'taskrenewmain',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TRNM_Num',
                sortName: 'TRNM_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'TRNM_Num', title: __('Trnm_num'), operate: 'LIKE'},
                        {field: 'newBnum', title: __('Trnm_bnum'), operate: 'LIKE'},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 't.T_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 't_project', title: '工程', operate: false},
                        {field: 't.t_project', title: '工程', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: __('T_num'), operate: false},
                        {field: 'td.T_Num', title: __('T_num'), operate: 'LIKE', visible: false},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'td.TD_TypeName', title: __('塔型'), operate: 'LIKE', visible: false},
                        {field: 'old_TypeName', title: '打钢印', operate: false},
                        {field: 'nottn.old_TypeName', title: __('打钢印'), operate: 'LIKE', visible: false},
                        {field: 'TRNM_Writer', title: __('Trnm_writer'), operate: 'LIKE'},
                        {field: 'TRNM_WriteDate', title: __('Trnm_writedate')},
                        {field: 'TRNM_Auditor', title: __('Trnm_auditor'), operate: 'LIKE'},
                        {field: 'TRNM_AuditorDate', title: __('Trnm_auditordate')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $("#print").click(function () {
                window.top.Fast.api.open('chain/supplement/sup_app_form/print/ids/' + Config.ids, '补件申请单', {
                    area: ["100%", "100%"]
                });
            });
            $("#A5print").click(function () {
                window.top.Fast.api.open('chain/supplement/sup_app_form/a5print/ids/' + Config.ids, '补件申请单', {
                    area: ["100%", "100%"]
                });
            });
            
        },
        choosedetail: function () {
            var TRNM_Num = Config.TRNM_Num;
            var show = $("#show");
            var rightTable = $("#rightTable");
            var height = document.body.clientHeight/2;
            show.parents("div").css("height",height);
            // 初始化表格
            rightTable.bootstrapTable({
                data:[],
                height: height,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'TRNM_Num', title: '单据号', width: 45, visible: false},
                        {field: 'TD_ID', title: 'TD_ID', width: 45, visible: false},
                        {field: 'DtS_Name', title: '段名', width: 45},
                        {field: 'TRN_TPNum', title: '部件号', width: 45,visible: false},
                        {field: 'TRN_TPNum_Input', title: '部件号', width: 100},
                        {field: 'DtMD_sRemark', title: '备注', width: 100},
                        {field: 'DtMD_sStuff', title: '材料', width: 45},
                        {field: 'DtMD_sMaterial', title: '材质', width: 55},
                        {field: 'DtMD_sSpecification', title: '规格', width: 70},
                        {field: 'DtMD_iLength', title: '长度(mm)', width: 75},
                        {field: 'DtMD_fWidth', title: '宽度(mm)', width: 75},
                        {field: 'DtMD_iTorch', title: '厚度(mm)', width: 75},
                        {field: 'DtMD_iWelding', title: '是否电焊', width: 45},
                        {field: 'type', title: '类型', width: 50},
                        {field: 'TRN_TPNumber', title: '数量', width: 45,visible: false},
                        {field: 'DtMD_iUnitCount', title: '单基数量', width: 75},
                        {field: 'TRN_TPWeight', title: '单件重量(kg)', width: 75},
                        {field: 'sum_weight', title: '总重',visible: false},
                        {field: 'DtMD_iUnitHoleCount', title: '单件孔数', width: 75},
                        {field: 'T_Num', title: '任务单', width: 75},
                        // {field: 'DtM_sProject', title: '工程名称'},
                        {field: 'TD_TypeName', title: '塔型', width: 75},
                        {field: 'DtM_sPressure', title: '电压等级', width: 75},
                        {field: 'DtMD_iFireBending', title: '制弯', width: 45},
                        {field: 'DtMD_KaiHeJiao', title: '开合角', width: 60},
                        {field: 'DtMD_DaBian', title: '压扁', width: 45},
                        {field: 'DtMD_iCuttingAngle', title: '切角', width: 45},
                        {field: 'DtMD_fBackOff', title: '铲背', width: 45},
                        {field: 'DtMD_iBackGouging', title: '清根', width: 45},
                        {field: 'DtMD_GeHuo', title: '割豁', width: 45},
                    ]
                ],
                onLoadSuccess:function()
                {
                    //重点就在这里，获取渲染后的数据列td的宽度赋值给对应头部的th,这样就表头和列就对齐了
                    var header=$(".fixed-table-header table thead tr th");
                    var body=$(".fixed-table-header table tbody tr td");
                    var footer=$(".fixed-table-header table tr td");
                    body.each(function(){
                    header.width((this).width());
                    footer.width((this).width());
                    });
                }
            });

            ajax_copy_td({TRNM_Num: TRNM_Num, DtS_Name: '', DtMD_iWelding:2});
            $(document).on('click', "#searchMaterial", function(e){
                var search = {TRNM_Num: TRNM_Num};
                var DtS_Name =  $("#DtS_Name").val();
                var DtMD_iWelding =  $("#DtMD_iWelding").val();
                search.DtS_Name = DtS_Name;
                search.DtMD_iWelding = DtMD_iWelding;
                ajax_copy_td(search);
            });
            $(document).on('click', "#sure", function(e){
                var tableObject = $("#detail-form").serializeArray();
                var content;
                var tableArray = [];
                var count = tableObject.length/28;
                for(var j=0;j<count;j++){
                    var first_array = {};
                    
                    for(var i=0;i<=27;i++){
                        var js = j*28+i;
                        content = tableObject[js];
                        first_array[content.name]=content.value;
                    }
                    tableArray.push(first_array);
                }
                
                if(tableArray.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableArray);
            });
            function ajax_copy_td(search){
                $.ajax({
                    url: 'chain/supplement/sup_app_form/detailMaterial',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: search,
                    success: function (ret) {
                        var tableHtml = [];
                        if(ret.code==1) tableHtml = ret.data;
                        rightTable.bootstrapTable('load',tableHtml);
                        rightTable.bootstrapTable("resetView");
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            $(document).on('keyup', "#daihao", function(e){
                var val = $("#daihao").val();
                rightTable.bootstrapTable("uncheckAll");
                if(val) $('#rightTable tbody tr').hide().find("td input[value^='" +(val) + "']").parents("tr").show();
                else $('#rightTable tbody tr').show();
                var chooseId = $("#rightTable tbody tr:visible:first").data("index");
                rightTable.bootstrapTable("check",chooseId);
                if(e.keyCode == 13){
                    var content = rightTable.bootstrapTable("getAllSelections");
                    if(content.length==0){
                        layer.msg("未选中！")
                        return false;
                    }
                    show_append(content);
                    // show.bootstrapTable("append",content);
                    $('#rightTable tbody tr').show();
                    rightTable.bootstrapTable("uncheckAll");
                    rightTable.bootstrapTable("check",0);
                    $("#daihao").val("");
                }
            });
            $(document).on('click', ".del", function(e){
                $( e.target ).closest("tr").remove();
            });

            $(document).on('keyup', "#rightTable", function(event){
                if (event.keyCode == 13) {
                    var content = rightTable.bootstrapTable("getAllSelections");
                    if(content.length==0){
                        layer.msg("未选中！")
                        return false;
                    }
                    rightTable.bootstrapTable("uncheckAll");
                    show_append(content);
                    // show.bootstrapTable("append",content);
                }
            });
            function show_append(content){
                var tableContent = "";
                var detailField = Config.tableDetailField;
                if(content.length != 0){
                    $.each(content,function(index_c,e_c){
                        tableContent += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                        $.each(detailField,function(index_d,e_d){
                            tableContent += '<td '+e_d[2]+'><input type="text" class="small_input" '+e_d[3]+' name="'+e_d[1]+'" value="'+(e_c[e_d[1]]==undefined?'':e_c[e_d[1]])+'"></td>';
                        });
                        tableContent += '</tr>';
                    });
                }
                $("#left_tbshow").append(tableContent);
                var left_height = $("#show").height();
                $("#de_height").scrollTop(left_height);
            }
        },
        print: function() {
            // console.log('limberList',Config.limberList);
            // console.log('detail_list',Config.detail_list);
            // console.log('mainInfos',Config.row);
            
            let list = Config.detail_list;
            let mainInfos = Config.row;
            
            let weight_sum = 0; 
            let count_sum = 0

            let projectType = {}

            list.forEach(el => {
                count_sum+=Number(el['TRN_TPNumber']);
                weight_sum+=Number(el['sum_weight']);
                let k = el['PC_Num'];
                
                if(projectType[k]===undefined){
                    projectType[k] = [];
                }
                projectType[k].push(el);
            });
            // console.log('projectType', projectType);
            let dataObj = {};
            let i=0;
            for (const key in projectType) {
                i++;
                let memo_arr = {};
                let tb_tmp = [];

                let temObj = {};
                projectType[key].forEach(el => {
                    
                    let k = el['TD_TypeName'];
                    // console.log('k', k);
                    if(temObj[k]===undefined){
                        temObj[k] = [];
                    }
                    temObj[k].push(el);
                    if(memo_arr[key]===undefined) memo_arr[key]=[];
                    if(el['TRNM_Memo']) memo_arr[key].push(el['TRNM_Memo']);
                });
                for (const key in temObj) {
                    let count_tmp = 0;
                    let weight_tmp = 0;
                    const ele = temObj[key];
                    let tmpArr = ele.map((element)=>{
                        count_tmp+=Number(element['TRN_TPNumber']);
                        weight_tmp+=Number(element['sum_weight'])*100;
                        let tmp = {
                            'gth': element['TRN_Project'],
                            'jh': element['TRN_TPNum'],
                            'cz': element['DtMD_sMaterial'],
                            'gg': element['DtMD_sSpecification'],
                            'kd': Number(element['DtMD_fWidth']) || '',
                            'cd': element['DtMD_iLength'],
                            'sl': element['TRN_TPNumber'],
                            'dz': element['TRN_TPWeight'],
                            'zl': element['sum_weight'],
                            'ks': element['DtMD_iUnitHoleCount'],
                            'bz': element['DtMD_sRemark'],
                            'dh': element['DtMD_iWelding'] || '',
                            'wq': element['DtMD_iFireBending'] || '',
                            'qj': element['DtMD_iCuttingAngle'] || '',
                            'cb': element['DtMD_fBackOff'] || '',
                            'qg': element['DtMD_iBackGouging'] || '',
                            'db': element['DtMD_DaBian'] || '',
                            'khj': element['DtMD_KaiHeJiao'] || '',
                            'zk': element['DtMD_ZuanKong'] || '',
                            'DtMD_GeHuo': element['DtMD_GeHuo'] || '',
                        }
                        return tmp;
                    });
                    //小计
                    tmpArr.unshift({
                        'gth': '塔型:',
                        'jh': key,
                        'cz': '',
                        'gg': '',
                        'kd': '',
                        'cd': '',
                        'sl': '',
                        'dz': '',
                        'zl': '',
                        'ks': '',
                        'bz': '',
                        'dh': '',
                        'wq': '',
                        'qj': '',
                        'cb': '',
                        'qg': '',
                        'db': '',
                        'khj': '',
                        'zk': '',
                    })
                    tmpArr.push({
                        'gth': '',
                        'jh': '',
                        'cz': '',
                        'gg': '',
                        'kd': '',
                        'cd': '合计:',
                        'sl': count_tmp,
                        'dz': '',
                        'ks': '',
                        'zl':( weight_tmp/100).toFixed(2),
                        'bz': '',
                        'dh': '',
                        'wq': '',
                        'qj': '',
                        'cb': '',
                        'qg': '',
                        'db': '',
                        'khj': '',
                        'zk': '',
                    })
                    tb_tmp = tb_tmp.concat(tmpArr);
                }
               
                dataObj['tb'+i] = tb_tmp;
                
                if(projectType[key][0]['t_project'].length>40){
                    projectType[key][0]['t_project'] = projectType[key][0]['t_project'].slice(0, 40)+'...'
                }
                dataObj['gcmc'+i] = projectType[key][0]['t_project'];
                dataObj['rq'+i] = projectType[key][0]['TRNM_WriteDate'];
                dataObj['beizhu'+i] = $.unique(memo_arr[key].sort()).join();
                this.addEle(blddata,i);
            }
            console.log(dataObj);
            
            let data_tmp={
                kh: mainInfos['Customer_Name'],
                htbh: mainInfos['T_Num'],
                ggh: mainInfos['ty_tower'],
                zsl: count_sum,
                zzl: weight_sum.toFixed(2),
                bjlx: mainInfos['TRNM_Sort'],
                zb: mainInfos['TRNM_Writer'],
                sh: mainInfos['Approver'],
                ...dataObj,
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: blddata});

            $('#p_mx1').html(htemp.getHtml([data_tmp]));
            
            $("#handleprintmx1").click(function(){
                htemp.print([data_tmp]);
            });
            
            pageto('#p_mx1',0);
            
        },
        addEle: function(template, i){
            let tb_ele = {
                "options": {
                    "left": 22.5,
                    "top": 85.5,
                    "height": 36,
                    "width": 550,
                    "field": "tb",
                    "textAlign": "center",
                    "tableBodyCellBorder": "noBorder",
                    "columns": [
                        [{
                            "title": "杆塔号",
                            "field": "gth",
                            "width": 49.36157878710943,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "gth",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;font-size:12.75pt'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value +"</div>";
                                }
                            }
                        }, {
                            "title": "件号",
                            "field": "jh",
                            "width": 55.35910916959801,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "jh",
                            "formatter": function (value,row) {
                                
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'><span style='position: absolute;margin-left: -30px;font-size:12.75pt;'>" + value + "</span></div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "材质",
                            "field": "cz",
                            "width": 39.361183577375726,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "cz",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "规格",
                            "field": "gg",
                            "width": 49.356336275766196,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "gg",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "宽度",
                            "field": "kd",
                            "width": 35.35594819415328,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "kd",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "长度",
                            "field": "cd",
                            "width": 35.35933329869133,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "cd",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "数量",
                            "field": "sl",
                            "width": 35.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "sl",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "单重",
                            "field": "dz",
                            "width": 40.359593790285835,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "dz",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "重量",
                            "field": "zl",
                            "width": 45.35853678436136,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "zl",
                            "formatter": function (value,row) {
                                // if (row['cz'] =='') {
                                // 	return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                // } 
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'><span style='position: absolute;margin-left: -20px;'>" + value + "</span></div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "孔数",
                            "field": "ks",
                            "width": 35.35853678436136,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "ks",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'><span>" + value + "</span></div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "电焊",
                            "field": "dh",
                            "width": 17.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "dh",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "制弯",
                            "field": "wq",
                            "width": 17.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "wq",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "切角",
                            "field": "qj",
                            "width": 17.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "qj",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "铲背",
                            "field": "cb",
                            "width": 17.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "cb",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "清根",
                            "field": "qg",
                            "width": 17.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "qg",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "打扁",
                            "field": "db",
                            "width": 17.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "db",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "开合角",
                            "field": "khj",
                            "width": 24.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "khj",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "割豁",
                            "field": "DtMD_GeHuo",
                            "width": 17.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "DtMD_GeHuo",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + (value || '') + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + (value || '') + "</div>";
                                }
                            }
                        }, {
                            "title": "钻孔",
                            "field": "zk",
                            "width": 17.356256047855915,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "zk",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }, {
                            "title": "备注",
                            "field": "bz",
                            "width": 122.07212407480287,
                            "colspan": 1,
                            "rowspan": 1,
                            "checked": true,
                            "columnId": "bz",
                            "formatter": function (value,row) {
                                if (row['cz'] =='') {
                                    return "<div style='border-right: none;border-top: 1px solid;border-bottom:2px solid;font-weight: bold;'>" + value + "</div>";
                                } 
                                
                                else {
                                    return "<div>" + value + "</div>";
                                }
                            }
                        }]
                    ]
                },
                "printElementType": {
                    "title": "表格",
                    "type": "tableCustom"
                }
            }
            tb_ele.options.top+=(70*(i-1));
            tb_ele.options.field='tb'+i;
            template.panels[0].printElements.push(tb_ele);

            let gcmc_ele = {
                "options": {
                    "left": 21,
                    "top": 57,
                    "height": 9.75,
                    "width": 552.5,
                    "lineHeight": 10.75,
                    "field": "gcmc",
                    "fontSize": 10.75,
                    "fontWeight": "800",
                    "textAlign": "center"
                },
                "printElementType": {
                    "type": "text"
                }
            };
            gcmc_ele.options.top+=(70*(i-1));
            gcmc_ele.options.field='gcmc'+i;
            template.panels[0].printElements.push(gcmc_ele);

            let bz_ele ={
                "options": {
                    "left": 70.5,
                    "top": 73.5,
                    "height": 9.75,
                    "width": 339,
                    "field": "beizhu"
                },
                "printElementType": {
                    "type": "text"
                }
            };
            bz_ele.options.top+=(70*(i-1));
            bz_ele.options.field='beizhu'+i;
            template.panels[0].printElements.push(bz_ele);

            let bz_title_ele = {
                "options": {
                    "left": 22.5,
                    "top": 73.5,
                    "height": 9.75,
                    "width": 30.5,
                    "title": "备注:",
                    "textAlign": "justify"
                },
                "printElementType": {
                    "type": "text"
                }
            };
            bz_title_ele.options.top+=(70*(i-1));
            template.panels[0].printElements.push(bz_title_ele);

            let rq_ele = {
                "options": {
                    "left": 453,
                    "top": 73.5,
                    "height": 9.75,
                    "width": 120,
                    "field": "rq"
                },
                "printElementType": {
                    "type": "text"
                }
            };
            rq_ele.options.top+=(70*(i-1));
            rq_ele.options.field='rq'+i;
            template.panels[0].printElements.push(rq_ele);

            let rq_title_ele = {
                "options": {
                    "left": 414,
                    "top": 73.5,
                    "height": 9.75,
                    "width": 30.5,
                    "title": "日期:",
                    "textAlign": "justify"
                },
                "printElementType": {
                    "type": "text"
                }
            };
            rq_title_ele.options.top+=(70*(i-1));
            template.panels[0].printElements.push(rq_title_ele);

        },
        print2: function() {
            let tb_ele = blddata.panels[0].printElements.find((e) => e.options.field=='tb');
           
            tb_ele = JSON.parse(JSON.stringify(tb_ele));
            tb_ele.options.top+=50;
            tb_ele.options.field='tb2';
            blddata.panels[0].printElements.push(tb_ele);
            console.log('blddata', blddata);
            let data_tmp={
                tb:[{aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1},{aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}],
                tb2:[{aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, {aa: 1, bb: 1}, ],
                tb3:[{aa: 1, bb: 1}],
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: blddata});

            $('#p_mx1').html(htemp.getHtml([data_tmp]));
            
            $("#handleprintmx1").click(function(){
                htemp.print([data_tmp]);
            });
            
            pageto('#p_mx1',0);
            
        },
        a5print: function() {
            // console.log('limberList',Config.limberList);
            // console.log('detail_list',Config.detail_list);
            // console.log('mainInfos',Config.row);
            let list = Config.detail_list;
            let mainInfos = Config.row;
            let dataArr = [];
            
            let pageSize = list.length;
            list.forEach(e => {
                let pageNow = dataArr.length+1;
                let data_tmp={
                    ty_tower: e['ty_tower'],
                    pt_num: e['PT_Num'],
                    djh:mainInfos['TRNM_Num'],
                    gcmc: e['t_project'],
                    tx: e['TD_TypeName'],
                    jianhao: e['TRN_TPNum'],
                    gg: e['DtMD_sSpecification'],
                    hd: '',
                    djks: e['DtMD_iUnitHoleCount'],
                    tianxie: mainInfos['TRNM_Writer'],
                    bz: e['DtMD_sRemark'],
                    duan: e['DtS_Name'],
                    jishu: e['TRN_TPNumber'],
                    cl: e['DtMD_sStuff'],
                    cd: e['DtMD_iLength'],
                    kd: e['DtMD_fWidth']?e['DtMD_fWidth']:'',
                    zks: e['TRN_TPNumber']*e['DtMD_iUnitHoleCount'],
                    sh: mainInfos['Approver'],
                    jianshu: e['TRN_TPNumber'],
                    cz: e['DtMD_sMaterial'],
                    lph:'',
                    zl:e['sum_weight'],
                    rq:mainInfos['TRNM_WriteDate'],
                    pageinfo: '第'+pageNow+'页，共'+pageSize+'页',
                    val1: e['DtMD_iWelding']
                    ?`<div style="border-style:solid;height:100%;width:100%;text-align: center;border-width: thin;line-height: initial;">电焊</div>`
                    :'',
                    val2: e['DtMD_iFireBending']
                    ?`<div style="border-style:solid;height:100%;width:100%;text-align: center;border-width: thin;line-height: initial;">制弯</div>`
                    :'',
                    val3: e['DtMD_iCuttingAngle']
                    ?`<div style="border-style:solid;height:100%;width:100%;text-align: center;border-width: thin;line-height: initial;">切角</div>`
                    :'',
                    val4: e['DtMD_fBackOff']
                    ?`<div style="border-style:solid;height:100%;width:100%;text-align: center;border-width: thin;line-height: initial;">铲背</div>`
                    :'',
                    val5: e['DtMD_iBackGouging']
                    ?`<div style="border-style:solid;height:100%;width:100%;text-align: center;border-width: thin;line-height: initial;">清根</div>`
                    :'',
                    val6: e['DtMD_DaBian']
                    ?`<div style="border-style:solid;height:100%;width:100%;text-align: center;border-width: thin;line-height: initial;">打扁</div>`
                    :'',
                    val7: e['DtMD_KaiHeJiao']
                    ?`<div style="border-style:solid;height:100%;width:100%;text-align: center;border-width: thin;line-height: initial;">开合角</div>`
                    :'',
                    val8: e['DtMD_GeHuo']
                    ?`<div style="border-style:solid;height:100%;width:100%;text-align: center;border-width: thin;line-height: initial;">割豁</div>`
                    :'',
                    val9: e['DtMD_ZuanKong']
                    ?`<div style="border-style:solid;height:100%;width:100%;text-align: center;border-width: thin;line-height: initial;">钻孔</div>`
                    :'',
                };
                dataArr.push(data_tmp)
            });

            

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            console.log(pdata);
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: pdata});
            $('#p_mx1').html(htemp.getHtml(dataArr));
            $("#handleprintmx1").click(function(){
                htemp.print(dataArr);
            });
            
            pageto('#p_mx1',0);
            
        },
        selectproject: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/supplement/sup_app_form/selectProject' + location.search,
                    table: 'projectcatalog',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PC_Num',
                sortName: 'PC_Num',
                sortOrder: 'DESC',
                // clickToSelect: true,
                // singleSelect: true,
                showToggle: false,
                showExport: false,
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'pc.PC_Num', title: '工程编号', operate: 'LIKE', visible: false},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'PC_ProjectName', title: '工程名称', operate: false},
                        {field: 'pc.PC_ProjectName', title: '工程名称', operate: 'LIKE', visible: false},
                        {field: 'PC_Pressure', title: '电压等级', operate: 'LIKE'},
                        {field: 'PC_Place', title: '工程地区', operate: 'LIKE'},
                        {field: 'PC_StartPlace', title: '工程地点', operate: 'LIKE'},
                        {field: 'PC_StartDate', title: '开工时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'PC_EditDate', title: '制表时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'PC_SalesMan', title: '业务员', operate: 'LIKE'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var formData=table.bootstrapTable('getAllSelections');
                Fast.api.close(formData);
            });
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });

                $(document).on('click', '#chooseTower', function (e){
                    var bjtype = $("select[name='row[TRNM_Sort]']").val();
                    // var bnum = $("#c-TRNM_Bnum").val();
                    var url = "chain/supplement/sup_app_form/selectProject";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            if(value.length == 0) return false;
                            var pcNumList = [];
                            $.each(value,function(key,val){
                                pcNumList.push(val.PC_Num);
                            })
                            $.ajax({
                                url: 'chain/supplement/sup_app_form/searchNo',
                                type: 'post',
                                dataType: 'json',
                                data: {num:JSON.stringify(pcNumList), bjtype:bjtype},
                                success: function (ret) {
                                    if (ret.code != 0) {
                                        window.location.href = "edit/ids/"+ret.data;
                                    }else layer.msg(ret.msg);
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            })
                        }
                    };
                    Fast.api.open(url,"选择工程",options);
                })

                $(document).on('click', '#chooseDetail', function (e){
                    var TRNM_Num = $("#c-TRNM_Num").val();
                    var tableRightField = Config.tableRightField;
                    if(!TRNM_Num) layer.msg("有误，请刷新后重试");
                    else{
                        var url = "chain/supplement/sup_app_form/chooseDetail/ids/"+TRNM_Num;
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                if(value.length == 0) return false;
                                // console.log(value);
                                var tableHtml = "";
                                $.each(value,function(index,e){
                                    tableHtml += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                    $.each(tableRightField,function(tindex,te){
                                        tableHtml += '<td '+te[2]+'><input type="text" class="small_input" '+te[3]+' name="table_right_row['+te[1]+'][]" value="'+(e[te[1]]==undefined?'':e[te[1]])+'"></td>';
                                    })
                                    tableHtml += '</tr>';
                                });
                                $("#right_show").append(tableHtml);
                            }
                        };
                        Fast.api.open(url,"选择补件明细",options);
                    }
                })

                $(document).on('click', ".del", function(e){
                    // var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tr').find("input[name='table_right_row[TRN_ID][]']").val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/supplement/sup_app_form/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                });

                $(document).on('click', "#allDel", function(e){
                    // var indexChoose = $(this).parents('tr').index();
                    var num = $("#c-TRNM_Num").val();
                    if(num){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/supplement/sup_app_form/allDel',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    if(ret.code==1){
                                        parent.location.reload();
                                    }else{
                                        layer.msg(ret.msg);
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        layer.msg("有误，请刷新后再试");
                    }
                });

                $(document).on('click', '#left_tbshow tr', function(e){
                    var state = $(this).find("input[type='checkbox']").is(':checked');
                    $("#left_tbshow").find("tr input[type='checkbox']").each(function () {
                        $(this).prop("checked",false);
                    });
                    if(state==false){
                        $(this).find("input[type='checkbox']").prop("checked",true);
                        var bj_num = $(this).find("input[type='checkbox']").val();
                        if(!bj_num) layer.msg("有误，请刷新后重试");
                        else ajaxTableHead(bj_num);
                    }
                })

                $(document).on("click", "#left_tbshow tr input[type='checkbox']", function(e){
                    var state = $(this).is(':checked');
                    $("#left_tbshow").find("tr input[type='checkbox']").each(function () {
                        $(this).prop("checked",false);
                    });
                    if(state==false){
                        $(this).prop("checked",true);
                        var bj_num = $(this).val();
                        if(!bj_num) layer.msg("有误，请刷新后重试");
                        else ajaxTableHead(bj_num);
                    }
                })

                function ajaxTableHead(bj_num)
                {
                    $.ajax({
                        url: 'chain/supplement/sup_app_form/editContent',
                        type: 'post',
                        dataType: 'json',
                        data: {num: bj_num},
                        success: function (ret) {
                            if(ret.code==1){
                                $("#c-TD_ID").val(ret.data.TD_ID);
                                $("#c-T_Num").val(ret.data.T_Num);
                                $("#c-TRNM_Num").val(ret.data.TRNM_Num);
                                $("#c-TD_TypeName").val(ret.data.TD_TypeName);
                                $("#c-TRNM_Time").val(ret.data.TRNM_Time);
                                $("#c-TRNM_Memo").val(ret.data.TRNM_Memo);
                                $("#c-ty_tower").val(ret.data.ty_tower);
                            }else Layer.msg(ret.msg);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }
            }
            
        }
    };
    return Controller;
});