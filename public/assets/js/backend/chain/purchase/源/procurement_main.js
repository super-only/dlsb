define(['jquery', 'bootstrap', 'backend', 'table', 'form','input-tag', 'jquerybase64'], function ($, undefined, Backend, Table, Form, InputTag) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/purchase/procurement_main/index' + location.search,
                    add_url: 'chain/purchase/procurement_main/add',
                    edit_url: 'chain/purchase/procurement_main/edit',
                    del_url: 'chain/purchase/procurement_main/del',
                    auditor_url: 'chain/purchase/procurement_main/auditor',
                    giveup_url: 'chain/purchase/procurement_main/giveUp',
                    multi_url: 'chain/purchase/procurement_main/multi',
                    import_url: 'chain/purchase/procurement_main/import',
                    table: 'procurement_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pm_num',
                sortName: 'pm_num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pm_num', title: __('Pm_num'), operate: 'LIKE'},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'type', title: __('Type'), searchList: Config.purchaseList, formatter: function(value){
                            return Config.purchaseList[value]??value;
                        }},
                        {field: 'unit', title: __('Unit'), searchList: Config.vendorList, formatter: function(value){
                            return Config.vendorList[value]??value;
                        }},
                        {field: 'header', title: __('Header'), operate: 'LIKE'},
                        {field: 'sign_date', title: __('Sign_date')},
                        {field: 'currency', title: __('Currency'),searchList: Config.bzList, formatter:function(value){
                            return Config.bzList[value];
                        }},
                        {field: 'rate', title: __('Rate'), operate:'BETWEEN'},
                        {field: 'department', title: __('Department'), searchList: Config.deptList, formatter: function(value){
                            return Config.deptList[value]??value;
                        }},
                        {field: 'salesman', title: __('Salesman'), operate: 'LIKE'},
                        {field: 'amount', title: __('Amount'), operate:'BETWEEN'},
                        {field: 'count', title: __('Count'), operate:'BETWEEN'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                data: [],
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pm_num', title: __('Pm_num'), operate: 'LIKE'},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'type', title: __('Type')},
                        {field: 'unit', title: __('Unit'), operate: 'LIKE'},
                        {field: 'header', title: __('Header'), operate: 'LIKE'},
                        {field: 'sign_date', title: __('Sign_date')},
                        {field: 'currency', title: __('Currency')},
                        {field: 'rate', title: __('Rate'), operate:'BETWEEN'},
                        {field: 'department', title: __('Department'), operate: 'LIKE'},
                        {field: 'salesman', title: __('Salesman'), operate: 'LIKE'},
                        {field: 'amount', title: __('Amount'), operate:'BETWEEN'},
                        {field: 'count', title: __('Count'), operate:'BETWEEN'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Controller.api.bindevent();
        },
        selectprocurement: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/purchase/procurement_main/selectprocurement/v_num/'+Config.v_num+'/type/' +Config.type+ location.search,
                    table: 'procurement_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pm_num',
                sortName: 'pm_num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pm_num', title: __('Pm_num'), operate: 'LIKE'},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'type', title: __('Type'), searchList: Config.purchaseList, formatter: function(value){
                            return Config.purchaseList[value]??value;
                        }},
                        {field: 'unit', title: __('Unit'), searchList: Config.vendorList, formatter: function(value){
                            return Config.vendorList[value]??value;
                        }},
                        {field: 'header', title: __('Header'), operate: 'LIKE'},
                        {field: 'sign_date', title: __('Sign_date')},
                        {field: 'currency', title: __('Currency')},
                        {field: 'rate', title: __('Rate'), operate:'BETWEEN'},
                        {field: 'department', title: __('Department'), searchList: Config.deptList, formatter: function(value){
                            return Config.deptList[value]??value;
                        }},
                        {field: 'salesman', title: __('Salesman'), operate: 'LIKE'},
                        {field: 'amount', title: __('Amount'), operate:'BETWEEN'},
                        {field: 'count', title: __('Count'), operate:'BETWEEN'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'update_time', title: __('Update_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var tableContent=table.bootstrapTable('getAllSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        selectprocurementlist: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/purchase/procurement_main/selectProcurementList/v_num/'+Config.v_num+'/type/' +Config.type+ location.search,
                    table: 'procurement_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pm_id',
                sortName: 'writer_time',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pm_num', title: __('Pm_num'), operate: 'LIKE'},
                        {field: 'im_num', title: '材料编号', operate: 'LIKE'},
                        {field: 'im_class', title: '材料名称', operate: 'LIKE'},
                        {field: 'im_spec', title: '材料规格', operate: 'LIKE'},
                        {field: 'l_name', title:'材质', operate: 'LIKE'},
                        {field: 'IM_PerWeight', title: '比重', operate: false},
                        {field: 'length', title: '长度(mm)/无扣长'},
                        {field: 'width', title: '宽度(mm)', operate: false},
                        {field: 'unit', title: '单位', operate: false},
                        {field: 'count', title: '数量', operate: false},
                        {field: 'weight', title: __('Salesman'), operate: false},
                        {field: 'way', title: '计算价格方式', operate: false,formatter:function(value){
                            return value==1?'按重量':'按数量';
                        }},
                        {field: 'price', title: '单价', operate: false},
                        {field: 'writer', title: __('Writer'), operate: false},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var tableContent=table.bootstrapTable('getAllSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                let table_height = document.body.clientHeight-300-$(".part_one").height();
                $(".table-responsive").height(table_height);
                var init_data = Config.init_data;
                var tagObj1 = InputTag.render({
                    elem: '.c-qg',
                    data: init_data,//初始值
                    number: init_data,
                    onNumberChange: function (data, value, type) {
                        $('#c-qg').val(JSON.stringify(data));
                    }
                });
                $(document).on('click', "#syncMaterial", function(e){
                    let type = $("#c-procurement_type").val();
                    console.log(type);
                    if(type===''){
                        layer.msg("必须选择采购类型");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            let existValue = [];
                            let endValue = [];
                            $.each($('input[name="table[AD_ID][]"]'),function(s_index,s_v){
                                existValue.push($(s_v).val());
                            });
                            $.each(value,function(v_index,v_e){
                                if($.inArray(v_e["AD_ID"]+'',existValue)===-1){
                                    endValue.push(v_e);
                                }
                            });
                            $.ajax({
                                url: 'chain/purchase/procurement_main/addMaterial/type/'+type,
                                type: 'post',
                                dataType: 'json',
                                data: {data:JSON.stringify(endValue)},
                                success: function (ret) {
                                    var content = '';
                                    if(ret.code==1){
                                        content = ret.data;
                                    }
                                    $("#tbshow").append(content);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            })
                        }
                    };
                    var url = 'chain/purchase/requisition/syncRequisition/type/'+type;
                    // if($(this).attr("id")=="chooseMaterial"){
                    //     url = 'chain/purchase/requisition/chooseMaterial';
                    // }else{
                    //     var string = $("#c-qg").val();
                    //     if(string=='') return false;
                    //     url = 'chain/purchase/requisition/syncRequisition/ids/'+$.base64.encode(string);
                    // }
                    Fast.api.open(url,"选择材料",options);
                });
                $(document).on('click', ".overtext", function(e){
                    let field = $(this).data("table");
                    //c-unit c-salesman c-qg
                    if(field == "c-unit"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                $("#c-unit_num").val(value.V_Num);
                                $("#c-unit").val(value.V_Name);
                            }
                        };
                        Fast.api.open('chain/material/material_note/chooseVendor',"对方单位选择",options);
                    }
                    if(field == "c-salesman"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                $("#c-salesman").val(value.E_Name);
                            }
                        };
                        Fast.api.open("chain/sale/project_cate_log/selectSaleMan","业务员选择",options);
                    }
                    if(field == "c-qg"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                if(value.length==0) return false;
                                $.each(value,function(index,e){
                                    if($.inArray(e.AL_Num, init_data)===-1){
                                        init_data.push(e.AL_Num);
                                    }
                                });
                                $('#c-qg').val(JSON.stringify(init_data));
                                tagObj1.clearData();
                                tagObj1.init({
                                    elem: '.c-qg',
                                    data: init_data,//初始值
                                    number: init_data
                                });
                            }
                        };
                        Fast.api.open('chain/purchase/requisition/selectRequisition',"请购单选择",options);
                    }
                });
                $(document).on('click', ".del", function(e){
                    $( e.target ).closest("tr").remove();
                });
                $(document).on('change', "#c-procurement_type", function(e){
                    $("#tbshow").html('');
                });

                function getPrice(object)
                {
                    var index = object.parents('tr').index();
                    var way = object.parents("tbody").find("tr").eq(index).find('td select[name="table[way][]"]').val();
                    if(way==1) var count = object.parents('tbody').find('tr').eq(index).find('td input[name="table[weight][]"]').val().trim();
                    else var count = object.parents('tbody').find('tr').eq(index).find('td input[name="table[count][]"]').val().trim();
                    var amount = object.parents('tbody').find('tr').eq(index).find('td input[name="table[amount][]"]').val().trim();
                    var price = 0;
                    count==''?count=0:'';
                    amount==''?amount=0:'';
                    count = parseFloat(count);
                    amount = parseFloat(amount);
                    price = count?(amount/count).toFixed(5):0;
                    console.log(way,count,amount);
                    object.parents('tbody').find('tr').eq(index).find('td input[name="table[price][]"]').val(price);
                }

                $('#tbshow').on('keyup','td input[name="table[count][]"],td input[name="table[weight][]"],td input[name="table[amount][]"]',function () {
                    getPrice($(this));
                })
                $('#tbshow').on('change','td select[name="table[way][]"]',function () {
                    getPrice($(this));
                })

                $(document).on("click", "#auditor", function () {
                    if(Config.flag) return false;
                    check('审核之后无法修改，确定审核？',"chain/purchase/procurement_main/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    if(!Config.flag) return false;
                    check('确定弃审？',"chain/purchase/procurement_main/giveUp",Config.ids);
                    
                });
                function check(msg,url,num){
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {ids: num},
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    Layer.msg(ret.msg);
                                    if (ret.code === 1) {
                                        window.location.reload();
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                };
            }
        }
    };
    return Controller;
});