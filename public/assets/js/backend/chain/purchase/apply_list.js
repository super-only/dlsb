define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/purchase/apply_list/index' + location.search,
                    auditor_url: 'chain/purchase/apply_list/auditor',
                    giveup_url: 'chain/purchase/apply_list/giveUp',
                    table: 'applylist',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'AD_ID',
                sortName: 'AD_ID',
                search: false,
                showToggle: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ad_cp_check', title: '是否采购', searchList: Config.status, formatter: Table.api.formatter.status},
                        {field: 'AL_Num', title: '请购编号', operate: false},
                        {field: 'AD_ID', title: '请购ID', operate: 'LIKE'},
                        {field: 'IM_Num', title: '材料编号', operate: false},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'IM_Spec', title: '材料规格', operate: 'LIKE'},
                        {field: 'L_Name', title: '材料材质', operate: 'LIKE'},
                        {field: 'AD_Length', title: '长度(米)', operate:'=1000'},
                        {field: 'AD_Width', title: '宽度(米)', operate: '=1000'},
                        {field: 'AD_Count', title: '请购数量', operate: false},
                        {field: 'AD_BuyCount', title: '采购数量', operate: false},
                        {field: 'sy_count', title: '剩余数量', operate: false},
                        {field: 'AD_Weight', title: '请购重量', operate: false},
                        {field: 'AD_BuyWeight', title: '采购重量', operate: false},
                        {field: 'sy_weight', title: '剩余重量', operate: false},
                        // {field: 'AL_Writer', title: '请购人', operate: 'LIKE'},
                        // {field: 'AL_WriterDate', title: '请购时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});