define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'cprkdata'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/produce_unit_main/index' + location.search,
                    add_url: 'chain/pack/produce_unit_main/add',
                    edit_url: 'chain/pack/produce_unit_main/edit',
                    del_url: 'chain/pack/produce_unit_main/del',
                    multi_url: 'chain/pack/produce_unit_main/multi',
                    import_url: 'chain/pack/produce_unit_main/import',
                    table: 'produceunitmain',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PUM_Num',
                sortName: 'PUM_Date',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight-60,
                showExport: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PUM_Num', title: __('Pum_num'), operate: false},
                        {field: 'pum.PUM_Num', title: __('Pum_num'), operate: false, visible: false},
                        {field: 'category', title: '产品类别', searchList: {"铁塔": "铁塔", "铁附件": "铁附件"}},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 't.T_Num', title: '任务单号', operate: 'LIKE', visible: false},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'PUM_Date', title: __('Pum_date'), defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'weight', title: '进仓总重(kg)', operate: false},
                        {field: 'count', title: '进仓数', operate: false},
                        // {field: 'PU_PRICENS', title: '进仓总价(元)', operate: false},
                        {field: 'Writer', title: __('Writer'), operate: false},
                        {field: 'Writedate', title: __('Writedate'), operate: false},
                        {field: 'Auditor', title: __('Auditor'), operate: false},
                        {field: 'AuditorDate', title: __('Auditordate'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            
            $("#print").click(function () {
                let pu_lb = $("#c-wxc").val();
                console.log('wx', pu_lb);
                window.top.Fast.api.open('chain/pack/produce_unit_main/print/ids/' + Config.ids + '/pu_lb/'+pu_lb, '产成品入库单', {
                    area: ["100%", "100%"]
                });
            });
            $("#c-PU_LB").on("change", () => {
                if($("#c-PU_LB").val()=='外协'){
                    $("#wxc").show();
                }
               
            })
        },
        print: function() {
            // console.log('limberList',Config.limberList);
            // console.log('detail_list',Config.list);
            console.log('mainInfos',Config.row);
            
            let list = Config.list;
            let mainInfos = Config.row;
            let tb_tmp = [];

            
            let weightSum =0;
            let countSun = 0;
            // 处理数据
            tb_tmp = list.map(p=>{
                let ggxh = p['TD_TypeName'];
                
                if(p['TH_Height']) ggxh=ggxh+'-'+p['TH_Height'];
                if(!(p['TD_TypeName']==p['SCD_TPNum'] || ggxh==p['SCD_TPNum'] || mainInfos['category']!=='铁塔')) ggxh=ggxh+'/'+p['SCD_TPNum'];
                weightSum+=p['PU_Weight']*100;
                countSun+=Number(p['count']);
                return {
                    cpmc: mainInfos['category'],
                    ggxh: ggxh,
                    dw: p['IM_Measurement']?p['IM_Measurement']:'基',
                    sl: p['count'],
                    zl:p['PU_Weight'],
                    bz:p['PU_Memo'],
                }
            })
            
            tb_tmp.push({
                cpmc: '合计:',
                ggxh: '',
                sl: countSun,
                zl: (weightSum/100).toFixed(2),
                bz:'',
            });
            
            console.log('tb_tmp', tb_tmp)

            rklblist = {
                '11':'正常入库'
            }
            let data_tmp={
                gfdw:Config.pu_lb,
                rkdh:mainInfos['PUM_Num'],
                szck: '成品仓库',
                rkrq: mainInfos['PUM_Date'],
                rklb: mainInfos['PU_LB']? mainInfos['PU_LB']+'入库': '完工入库',
                wtdw: mainInfos['Customer_Name'],
                rwdh:mainInfos['T_Num'],
                zd:mainInfos['Writer'],
                jbr:'',
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: cprkdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        choosedetail: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/produce_unit_main/chooseDetail/td_id/' + Config.td_id + location.search,
                    table: 'sectconfigdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SCD_ID',
                sortName: 'SCD_ID',
                sortOrder: 'ASC',
                search: false,
                showToggle: false,
                showExport: false,
                onlyInfoPagination: true,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'},
                        {field: 't_shortproject', title: '工程简称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'SCD_TPNum', title: '杆塔号', operate: 'LIKE'},
                        {field: 'Part', title: '配段信息', operate: 'LIKE'},
                        {field: 'SCD_Count', title: '配段基数', operate: 'LIKE'},
                        {field: 'T_Num', title: '任务单号', operate: 'LIKE'},
                        {field: 'TD_Pressure', title: '电压等级', operate: 'LIKE'},
                        {field: 'TH_Height', title: '呼高', operate: 'LIKE'},
                        {field: 'SCD_Weight', title: '单基重量', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        selectdetail: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/produce_unit_main/selectDetail' + location.search,
                    table: 'taskdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TD_ID',
                sortName: 'TD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                sortOrder: 'DESC',
                showToggle: false,
                showExport: false,
                searchFormVisible: true,
                rowStyle: function (row,index) {
                    var style = {};
                    
                    if(row.unit_state=="存在入库"){
                        style = {css:{'background-color':"pink"}};
                    }
                    return style;
                },
                columns: [
                    [
                        {checkbox: true},
                        {field: 'unit_state', title: '入库状态', operate: false},
                        {field: 'pack_state', title: '打包状态', operate: false},
                        {field: 'fy_state', title: '下发状态', operate: false},
                        {field: 'T_Num', title: '任务单(生产单)号', operate: false},
                        {field: 'd.T_Num', title: '任务单(生产单)号', operate: 'LIKE',visible: false},
                        {field: 'P_Name', title: '产品名称', operate: false},
                        {field: 'P_Num', title: '线路名称', operate: false},
                        {field: 'd.P_Num', title: '线路名称', operate: 'LIKE',visible: false},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'd.TD_TypeName', title: '塔型', operate: 'LIKE',visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'c.PC_Num', title: '工程编号', operate: 'LIKE',visible: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE',visible: false},
                        {field: 'C_Project', title: '工程名称', operate: false},
                        {field: 'c.C_Project', title: '工程名称', operate: 'LIKE',visible: false},
                        {field: 'C_Company', title: '下发公司', operate: false},
                        {field: 'C_SortProject', title: '工程简写', operate: false},
                        {field: 'Customer_Name', title: '客户', operate: false},
                        {field: 'T_Count', title: '总基数', operate: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'd.TD_Pressure', title: '电压等级', operate: 'LIKE',visible: false},
                        {field: 'PT_sumWeight', title: '总重(kg)', operate: false},
                        {field: 'C_ProjectName', title: '审核时间', operate: false},
                        {field: 'C_SaleMan', title: '业务员', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent[0]);

            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                $(document).on('click', "#chooseTower", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#c-TD_ID").val(value.TD_ID);
                            $("#c-TD_TypeName").val(value.TD_TypeName);
                            $("#c-category").val(value.P_Name);
                        }
                    };
                    Fast.api.open('chain/pack/produce_unit_main/selectDetail',"选择塔型",options);
                })
                $(document).on('click', "#chooseDetail", function(e){
                    var td_id = $("#c-TD_ID").val();
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var tableField = Config.tableField;
                            var content = "";
                            $.each(value,function(v_index,v_e){
                                content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                $.each(tableField,function(t_index,e){
                                    if(e[1]=="PU_Weight" || e[1]=="z_PU_Weight"){
                                        content += '<td '+e[4]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(v_e[e[5]]*v_e['SY_Count'])+'"></td>';
                                    }else content += '<td '+e[4]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[5]]) == 'undefined'?e[3]:v_e[e[5]])+'"></td>';
                                });
                                content += "</tr>";
                            });
                            $("#tbshow").append(content);
                        }
                    };
                    Fast.api.open('chain/pack/produce_unit_main/chooseDetail/td_id/'+td_id,"选择进仓明细",options);
                })
                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[PU_ID][]"]').val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/pack/produce_unit_main/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                    
                });
                $(document).on("click", "#author", function () {
                    var num = $("#c-PUM_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到入库单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/pack/produce_unit_main/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#c-PUM_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到入库单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/pack/produce_unit_main/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
            }
        }
    };
    return Controller;
});