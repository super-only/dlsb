define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'bzqddata'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            $("#towerListTable").bootstrapTable({
                data: [],
                height: document.body.clientHeight*0.4,
                clickToSelect: true,
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'P_ID', title: __('P_id'),visible: false},
                        {field: 'SCD_ID', title: 'SCD_ID',visible: false},
                        {field: 'TD_ID', title: 'TD_ID',visible: false},
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'old_TypeName', title: '打钢印'},
                        {field: 'TD_Pressure', title: '电压等级'},
                        {field: 'TH_Height', title: '呼高'},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'SCD_Count', title: '数量'}
                    ]
                ]
            });
            Table.api.bindevent($("#towerListTable"));
            $("#towerPackageDetailsTable").bootstrapTable({
                data: [],
                height: document.body.clientHeight*0.4,
                clickToSelect: true,
                singleSelect: true,
                columns: [
                    [
                        {field: 'PD_ID', title: 'PD_ID',visible: false},
                        {field: 'P_ID', title: __('P_id'),visible: false},
                        {field: 'SCD_ID', title: 'SCD_ID',visible: false},
                        {field: 'TD_ID', title: 'TD_ID',visible: false},
                        {field: 'PD_Name', title: '包名'},
                        {field: 'PD_Content', title: '包内容'},
                        {field: 'SCD_Count', title: '基数'},
                        {field: 'PD_Type', title: '包类型'},
                        {field: 'old_TypeName', title: '打钢印'},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'Writer', title: '制表人'},
                        {field: 'Auditor', title: '审核人'},
                        {field: 'WriteDate', title: '制表时间'},
                        {field: 'AuditorDate', title: '审核时间'},
                        {field: 'P_Memo', title: '备注'}
                    ]
                ]
            });
            Table.api.bindevent($("#towerPackageDetailsTable"));

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/pack_list/index' + location.search,
                    edit_url: 'chain/pack/pack_list/edit',
                    import_url: 'chain/pack/pack_list/import',
                    table: 'pack',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TD_ID',
                sortName: 'WriteDate',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                height: document.body.clientHeight*0.5,
                showToggle: false,
                showExport: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'P_ID', title: __('P_id'),visible: false, operate:false},
                        {field: 'TD_ID', title: 'TD_ID',visible: false, operate:false},
                        // {field: 'count', title: '数量', operate: false},
                        // {field: 'weight', title: '重量', operate: false},
                        // {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        // {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'T_Num', title: '任务单', operate: false},
                        {field: 'td.T_Num', title: '任务单', operate: 'like', visible: false},
                        // {field: 't_project', title: '线路名称', operate: 'LIKE'},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'},
                        // {field: 't_shortproject', title: '工程简称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'td.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'old_TypeName', title: '打钢印', operate: 'LIKE'},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {
                            field: 'buttons',
                            width: "100px",
                            title: '重量清单',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'print',
                                    text: '',
                                    title: '重量清单',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-print',
                                    url: 'chain/pack/pack_list/printZlqd',
                                    
                                    visible: function (row) {
                                        //返回true时按钮显示,返回false隐藏
                                        return true;
                                    }
                                },
                                
                               
                            ],
                            formatter: Table.api.formatter.buttons
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            
            //主table单击触发
            table.on('click-row.bs.table',function(row, $element){
                var url = "chain/pack/pack_list/towerListTable";
                var supplement = {TD_TypeName:$element["TD_TypeName"],old_TypeName:$element["old_TypeName"],TD_Pressure:$element["TD_Pressure"]};
                var search = {tdId: $element["TD_ID"],supplement:JSON.stringify(supplement)};
                var table_id = "towerListTable";
                ajaxIndexContent(url,search,table_id);
                showHideDiv(show="#towerListLi",div="#towerList");
                $("#towerPackageDetailsTable").bootstrapTable("load",[]);
            });

            //选择某一行
            $("#towerListTable").on('check.bs.table',function(row, $element){
                var url = "chain/pack/pack_list/towerPackageDetailsTable";
                var supplement = {TD_TypeName:$element["TD_TypeName"],old_TypeName:$element["old_TypeName"],SCD_Count:$element["SCD_Count"],SCD_TPNum:$element["SCD_TPNum"]};
                var search = {pId: $element["P_ID"],supplement:JSON.stringify(supplement)};
                var table_id = "towerPackageDetailsTable";
                ajaxIndexContent(url,search,table_id);
                showHideDiv(show="#towerPackageDetailsLi",div="#towerPackageDetails");
            });

            function ajaxIndexContent(url='',search={},table_id=""){
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: search,
                    success: function (ret) {
                        var contentData = [];
                        if(ret.code == 1){
                            contentData = ret.data;
                        }
                        $("#"+table_id+"").bootstrapTable("load",contentData);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            function showHideDiv(show="",div=""){
                var table_hd = $(".nav-tabs li");
                $.each(table_hd,function(index,e){
                    $(e).removeClass("active");
                });
                var table_div = $(".nav-tab-content div");
                $.each(table_div,function(index,e){
                    $(e).removeClass("active in");
                });
                $(show).addClass("active");
                $(div).addClass("active in");
            }
        },
        edit: function () {
            $("#pack_list").data("params",function(obj) {
                return {scd: $("#scd_list").val()}
            });
            
            $(document).on("change", "#scd_list", function(){
                //后续操作
                let ss = $('#scd_list').selectPageText();
                if(ss!="全部"){
                    $('#pack_list').selectPageDisabled(false);
                }else{
                    $('#pack_list').selectPageClear();
                    $('#pack_list').selectPageDisabled(true);
                }
            });
            
            $("#all_print").click(() => {
                let showMain = $("#c-welding").prop("checked");
                let showWithoutWeight = $("#c-showWithoutWeight").prop("checked");
                let factoryName = $("#factory_name").val();
                
                window.top.Fast.api.open('chain/pack/pack_list/print/ids/' + Config.ids+ '/showMain/'+showMain+
                '/showWithoutWeight/'+showWithoutWeight+'/factoryName/'+factoryName, '报表打印', {
                    area: ["100%", "100%"]
                });
            })
            $("#scd_print").click(() => {
                let showMain = $("#c-welding").prop("checked");
                let showWithoutWeight = $("#c-showWithoutWeight").prop("checked");
                let factoryName = $("#factory_name").val();
                let pack_list_pids = $("#pack_list").val();
                let scd_list_id = $("#scd_list").val();
                console.log('pack_list_pids', pack_list_pids);
                console.log('scd_list_id', scd_list_id);
                window.top.Fast.api.open('chain/pack/pack_list/print/ids/' + Config.ids+ '/showMain/'+showMain+
                '/showWithoutWeight/'+showWithoutWeight+'/factoryName/'+factoryName+'/scdid/'+scd_list_id+'/pack_list_pids/'+pack_list_pids, '报表打印', {
                    area: ["100%", "100%"]
                });
            })
            Controller.api.bindevent();
        },  
        print: function (){
            const cTop = $('.top_div').offset().top;
           
            $(window).scroll(function(){
                
                if($(this).scrollTop()>cTop){
                    $('.top_div').css('position', 'fixed').css('top', 0);
                }else{
                    $('.top_div').css('position', 'relative');
                }
            })

            // console.log('scd_list_id in ', Config.scd_list_id);
            // console.log('pack_list_pids in ', Config.pack_list_pids);
            // const list = Config.list;
            // console.log('rows', Config.row);
            
            let rows = Config.row;
            
            

            if(Config.scd_list_id && Config.scd_list_id!='all'){
                
                let scd_id = Config.scd_list_id;
                rows = rows.filter((e) => {
                    if(e['P_ID']==scd_id){
                        let pids = Config.pack_list_pids? Config.pack_list_pids.toString().split(','): false;
                        if(pids){
                            console.log('pids', pids);
                            e['packlist'] = e['packlist'].filter((p) => {
                                if(pids.indexOf(p['pdid'].toString())>-1){
                                    return p;
                                }
                            })
                        }
                        return e;
                    }
                })
            }
            // const row = rows.length>0 ?rows[0]: {};
            // console.log('row', row);
			
            if(Config.showMain=='true'){
                //只显示焊接主件的rows
                let showMainRows=rows.map((e) => {
                    let packlist = [];
                    let tmpArr = [];
                    e['packlist'].forEach((p) => {
                        if(p['PD_IsM']==0){
                            tmpArr.push(p);
                        }
                        else{
                            packlist.push(p);
                        }
                    });

                    packlist = packlist.map((p) => {
                        if(p['PD_IsM']==1){
                            tmpArr.forEach((t) => {
                                if(p['DCD_ID']==t['DCD_ID']){
                                    //p['TP_UnitWeight']=((p['TP_UnitWeight']*100 + t['TP_UnitWeight']*(t['TP_PackCount']/p['TP_PackCount'])*100)/100).toFixed(2);
                                    
                                    p['TP_SumWeight']=Number(p['TP_SumWeight']) + Number(t['TP_SumWeight'])
                                    
                                }
                            })
                        }
                        
                        return p;
                    })

                    e['packlist'] = packlist;
                    return e;
                })
                // console.log("showMainRows", showMainRows);
                rows = showMainRows;
            }

            if(Config.row.length>0){
                // 封面开始
                let fm_data_arr = [];
                rows.forEach((row) => {
                    let res = row['packlist'].reduce((res, cur) => {
                        res['rowPackCount']+=Number(cur['TP_PackCount']);
						res['rowSumWeight']+=parseFloat(cur['TP_SumWeight']);
                        let bm = cur['PD_Name'];
                        if(res['pd_name'].find(e => e==bm) == undefined){
                            res['pd_name'].push(bm);
                        }
                        return res;
                    }, {
                        rowPackCount:0,
						rowSumWeight:0,
                        pd_name: [],
                    }); 
    
                    let fm_data = {
                        htbh:row['T_Num'],
                        khmc:row['Customer_Name'],
                        gcmc:row['t_project'],
                        tx: row['TD_TypeName'],
                        hg: row['TH_Height']+'M',
                        zh: row['SCD_TPNum'],
                        dlzh: row['SCD_Part']+(row['SCD_SpPart']?','+row['SCD_SpPart']:''),
                        js: row['SCD_Count']+'基',
                        djzl: res['rowSumWeight'].toFixed(2)+'Kg',
                        bs: res['pd_name'].length+'包，共'+res['rowPackCount']+'件',
                        rq:row['NowDate'],
                        zb:row['Writer'],
                        factory_name: Config.factoryName,
                    }
                    fm_data_arr.push(fm_data);
                })
                let fmTemp = Config.showWithoutWeight=='true'? fmWithoutWeight: fm;
                let htemp =  new hiprint.PrintTemplate({template: fmTemp});
                $('#p_fm').html(htemp.getHtml(fm_data_arr)); 
                $("#handleprintfm").click(function(){
                    htemp.print(fm_data_arr);
                });
                // 封面结束

                // 明细开始
                let dataArr = [];
                rows.forEach(e => {
                    let tb_data = [];
                    let tmp_count = 0;
                    let sum_count = 0;
                    let tmp_weight = 0
                    let sum_weight = 0;
                    let tmp_last = '';
                    e['packlist'].forEach((p, i) => {
                        if(tmp_last && tmp_last!=p['PD_Name']){
                            let splitArr = tmp_last.split('+');
                            tb_data.push({
                                bjbh:'包 名：',
                                gyh: splitArr.length>1?splitArr[splitArr.length-1]:'',
                                cz: '',
                                gg: '',
                                cd: '小计：',
                                bzs: tmp_count,
                                dz: '',
                                zz: (tmp_weight/100).toFixed(2),
                                bz: '',
                            });
                            tmp_count = 0;
                            tmp_weight = 0;
                        }
                        tmp_last = p['PD_Name'];
                        
                        let tb_tmp = {
                            bjbh:p['DtMD_sPartsID'],
                            gyh: e['old_TypeName'] || '',
                            cz: p['DtMD_sMaterial'],
                            gg: p['DtMD_sSpecification']+(parseFloat(p['DtMD_fWidth'])?'*'+p['DtMD_fWidth']:''),
                            cd: p['DtMD_iLength'],
                            bzs: p['TP_PackCount'],
                            dz: (parseFloat(p['TP_SumWeight'])/parseFloat(p['TP_PackCount'])).toFixed(2),
                            zz: Number(p['TP_SumWeight']).toFixed(2),
                            bz: p['DtMD_sRemark'] || '',
                        }
                        tb_data.push(tb_tmp);
                        tmp_count+=tb_tmp.bzs;
                        sum_count+=tb_tmp.bzs;
                        tmp_weight+=tb_tmp.zz*100;
                        sum_weight+=tb_tmp.zz*100;
                        if((i+1)==e['packlist'].length){
                            let splitArr = tmp_last.split('+');
                            tb_data.push({
                                bjbh:'包 名：',
                                gyh: splitArr.length>1?splitArr[1]:'',
                                cz: '',
                                gg: '',
                                cd: '小计：',
                                bzs: tmp_count,
                                dz: '',
                                zz: (tmp_weight/100).toFixed(2),
                                bz: '',
                            });
                            tmp_count = 0;
                            tmp_weight = 0;
                        }
                    });
                    tb_data.push({
                        bjbh:'',
                        gyh: '',
                        cz: '',
                        gg: '',
                        cd: '合计：',
                        bzs: sum_count,
                        dz: '',
                        zz: (sum_weight/100).toFixed(2),
                        bz: '',
                    });
                    
                    let mx_data = {
                        tx: e['TD_TypeName']+'-'+e['TH_Height']+'M',
                        gth:e['SCD_TPNum'],
                        rq:e['WriteDate'],
                        tb:tb_data,
                    }
                    dataArr.push(mx_data);
                });
                let mxTemp = Config.showWithoutWeight=='true'? mxWithoutWeight: mx;
                let htemp1 =  new hiprint.PrintTemplate({template: mxTemp});
                $('#p_mx1').html(htemp1.getHtml(dataArr)); 
                // $('.cell').parent().css('border','1px solid');
                $("#handleprintmx1").click(function(){
                    htemp1.print(dataArr);
                });
                let pg_mx= {
                    "#p_mx1": 1,
                    "#p_fm": 1,
                }; //明细，当前页
                window.pageto=function(id,num){

                    let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                    let pmx=pg_mx[id];
                    pmx=pmx+num;
                    $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                    $(id).parent().parent().find(".pgpre").removeClass('disabled');
                    if(pmx<=1){
                        pmx=1;
                        $(id).parent().parent().find(".pgpre").addClass('disabled');
                    }
                    if(pmx>=pcount){
                        pmx=pcount;
                        // console.log($(id+" .pgnxt"))
                        $(id).parent().parent().find(".pgnxt").addClass('disabled');
                    }
                    pg_mx[id]=pmx;
    
                    // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                    // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();
    
                    let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                    for(let v of mxpage){
                        $(v).hide();
                    }
    
                    $(mxpage[pmx-1]).show();
                    $(id).show();
                };
                pageto('#p_mx1',0);
                pageto('#p_fm',0);
                // 明细结束
            }
            
        },
        printzlqd: function (){
            console.log('rows', Config.row);
            let pageSize = 28;
            let rows = Config.row
            let row = rows.length>0? rows[0]: {};
            let tb_data = [];
            let js_tmp = 0;
            let zks_tmp = 0;
            let zzl_tmp = 0;
            // 处理数据
            rows.forEach(e => {
                js_tmp += e['SCD_Count'];

                if(e['packlist'].length>0){
                    
                    zks_tmp+=e['packlist'].length;
                    tb_data.push({
                        hg:'',
                        gth: '',
                        js: '',
                        zdcd: '',
                        bm: '',
                        jhs: '',
                        bzl: '',
                    });
                         
                    e['packlist'].forEach((p) => {
                        
                        let tb_tmp = {
                            hg:e['TH_Height'],
                            gth: e['SCD_TPNum'],
                            sl: e['SCD_Count'],
                            zdcd: p['MaxLength'],
                            bm: p['PD_Name'].split('+').pop(),
                            jhs: Number(e['SCD_Count']*p['PD_SumCount']),
                            bzl: Number(e['SCD_Count']*p['PD_sumWeight']).toFixed(2),
                        }
                        zzl_tmp+=tb_tmp['bzl']*100;
                        
                        tb_data.push(tb_tmp);
                    })
                }
            });
            

            data_tmp = {
                gcmc: row['t_project'],
                tx: row['TD_TypeName'],
                zks: zks_tmp,
                js: js_tmp,
                zzl: (zzl_tmp/100).toFixed(2),
                sh: row['Auditor'],
                zb: row['Writer'],
                tb: tb_data,
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: zlqd});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                
            }
        }
    };
    return Controller;
});