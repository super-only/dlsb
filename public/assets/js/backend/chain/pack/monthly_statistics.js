define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'cpytjdata', 'hpbundle'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/monthly_statistics/index' + location.search,
                    import_url: 'chain/pack/monthly_statistics/import',
                    table: 'saleinvoice',
                }
            });

            var table = $("#table");

            var monthForTitle = new Date().getMonth()+1;
            const getSearch = (a, b) => {
                let params = JSON.parse(b['filter']);
                let startDate = params['time'].split(' ')[0];
                
                monthForTitle = new Date(startDate).getMonth()+1;
                console.log('monthForTitle', monthForTitle);
            }

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'SI_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}, operate: false},
                        // {field: 'PC_Num', title: '工程编号', operate: '='},
                        {field: 'category', title: '产品类别', searchList: {"铁塔": "铁塔", "铁附件": "铁附件"}},
                        {field: 'T_Num', title: '任务单号', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'TH_Height', title: '呼高', operate: false},
                        {field: 'C_Project', title: '工程名称', operate: 'LIKE'},
                        {field: 'qc_count', title: '期初数', formatter: function(value,row,index){return Number(value);}, operate: false},
                        {field: 'qc_weight', title: '期初重', formatter: function(value,row,index){return parseFloat(value).toFixed(2);}, operate: false},
                        {field: 'by_rk_count', title: '入库数', formatter: function(value,row,index){return Number(value);}, operate: false},
                        {field: 'by_rk_weight', title: '入库重', formatter: function(value,row,index){return parseFloat(value).toFixed(2);}, operate: false},
                        {field: 'by_ck_count', title: '出库数', formatter: function(value,row,index){return Number(value);}, operate: false},
                        {field: 'by_ck_weight', title: '出库重', formatter: function(value,row,index){return parseFloat(value).toFixed(2);}, operate: false},
                        {field: 'kc_weight', title: '库存重', formatter: function(value,row,index){return parseFloat(value).toFixed(2);}, operate: false},
                        {field: 'kc_count', title: '库存数', formatter: function(value,row,index){return Number(value);}, operate: false},
                        {field: 'time', title: '时间', operate:'RANGE',defaultValue:Config.defaultTime, addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime,visible:false}
                    ]
                ],
                onCommonSearch: getSearch,
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            

            $("#print").click(function () {
                
                window.top.Fast.api.open('chain/pack/monthly_statistics/print/ids/' + Config.ids + '/month/' +monthForTitle, '成品月统计', {
                    area: ["100%", "100%"]
                });
            });
            
        },
        print: function() {
            let list = Config.list;
            console.log('title', Config.title);
            console.log('zb', Config.zb);

            let tb_tmp = [];
            for (const key in list) {
                if (Object.hasOwnProperty.call(list, key)) {
                    const ele = list[key];
                    tb_tmp.push({
                        rwdh: ele['T_Num'],
                        tx: ele['TD_TypeName'],
                        hg: ele['TH_Height'],
                        gcmc: ele['C_Project'],
                        qcs: ele['qc_count'],
                        qcz: Number(ele['qc_weight']).toFixed(2),
                        rkjs: ele['by_rk_count'],
                        rkz: Number(ele['by_rk_weight']).toFixed(2),
                        ckjs: ele['by_ck_count'],
                        ckz: Number(ele['by_ck_weight']).toFixed(2),
                        kcjs: ele['kc_count'],
                        kcz: Number(ele['kc_weight']).toFixed(2),
                    })
                }
            }          
            
            let data_tmp={
                tb:tb_tmp,
                title: Config.title,
                zb: Config.zb
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: cpytjdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});