define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jquerybase64'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            $("#towerListTable").bootstrapTable({
                data: [],
                height: document.body.clientHeight*0.4,
                clickToSelect: true,
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'P_ID', title: __('P_id'),visible: false},
                        {field: 'SCD_ID', title: 'SCD_ID',visible: false},
                        {field: 'TD_ID', title: 'TD_ID',visible: false},
                        {field: 'status', title: '是否审核'},
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'old_TypeName', title: '打钢印'},
                        {field: 'TD_Pressure', title: '电压等级'},
                        {field: 'TH_Height', title: '呼高'},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'SCD_Count', title: '数量'}
                    ]
                ]
            });
            // Table.api.bindevent($("#towerListTable"));
            $("#towerPackageDetailsTable").bootstrapTable({
                data: [],
                height: document.body.clientHeight*0.4,
                clickToSelect: true,
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PD_ID', title: 'PD_ID',visible: false},
                        {field: 'P_ID', title: __('P_id'),visible: false},
                        {field: 'SCD_ID', title: 'SCD_ID',visible: false},
                        {field: 'TD_ID', title: 'TD_ID',visible: false},
                        {field: 'PD_Name', title: '包名'},
                        {field: 'PD_Content', title: '包内容'},
                        {field: 'SCD_Count', title: '数量'},
                        {field: 'PD_Type', title: '包类型'},
                        {field: 'old_TypeName', title: '打钢印'},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'Writer', title: '制表人'},
                        {field: 'Auditor', title: '审核人'},
                        {field: 'WriteDate', title: '制表时间'},
                        {field: 'AuditorDate', title: '审核时间'},
                        {field: 'P_Memo', title: '备注'}
                    ]
                ]
            });
            // Table.api.bindevent($("#towerPackageDetailsTable"));
            $("#packageDetailsTable").bootstrapTable({
                data: [],
                height: document.body.clientHeight*0.4,
                clickToSelect: true,
                singleSelect: true,
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'DtS_Name', title: '段号'},
                        {field: 'DtMD_sPartsID', title: '件号'},
                        {field: 'DtMD_sStuff', title: '名称'},
                        {field: 'DtMD_sMaterial', title: '材质'},
                        {field: 'DtMD_sSpecification', title: '规格'},
                        {field: 'DtMD_iLength', title: '长度(mm)'},
                        {field: 'DtMD_fWidth', title: '宽度(mm)'},
                        {field: 'DtMD_iUnitCount', title: '数量'},
                        {field: 'DtMD_fUnitWeight', title: '重量(kg)'},
                        {field: 'DtMD_iFireBending', title: '制弯'},
                        {field: 'DtMD_iWelding', title: '电焊'},
                        {field: 'DtMD_sRemark', title: '备注'}
                    ]
                ]
            });
            // Table.api.bindevent($("#packageDetailsTable"));

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/pack/index' + location.search,
                    add_url: 'chain/pack/pack/add',
                    // edit_url: 'chain/pack/pack/edit',
                    del_url: 'chain/pack/pack/del',
                    auditor_url: 'chain/pack/pack/mulAuditor',
                    giveup_url: 'chain/pack/pack/mulGiveup',
                    multi_url: 'chain/pack/pack/multi',
                    import_url: 'chain/pack/pack/import',
                    table: 'pack',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'P_ID',
                sortName: 'WriteDate',
                sortOrder: 'DESC',
                search: false,
                height: document.body.clientHeight*0.5,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'P_ID', title: __('P_id'),visible: false, operate:false},
                        {field: 'SCD_ID', title: 'SCD_ID',visible: false, operate:false},
                        {field: 'TD_ID', title: 'TD_ID',visible: false, operate:false},
                        {field: 'T_Num', title: '任务单', operate: false},
                        {field: 'td.T_Num', title: '任务单', operate: 'like', visible: false},
                        // {field: 'T_Company', title: '分公司', operate: false},
                        {field: 'P_Name', title: '产品类别', searchList: {"铁塔": "铁塔", "铁附件": "铁附件"}},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'},
                        {field: 't_shortproject', title: '工程简称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'td.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'old_TypeName', title: '打钢印', operate: 'LIKE'},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'Writer', title: '制单人', operate: false},
                        {field: 'WriteDate', title: '制单时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Writer', title: '制单人', operate: false},
                        {field: 'WriteDate', title: '制单时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            
            //主table单击触发
            table.on('click-row.bs.table',function(row, $element){
                var url = "chain/pack/pack/towerListTable";
                var supplement = {TD_TypeName:$element["TD_TypeName"],old_TypeName:$element["old_TypeName"],TD_Pressure:$element["TD_Pressure"]};
                var search = {tdId: $element["TD_ID"],supplement:JSON.stringify(supplement)};
                var table_id = "towerListTable";
                ajaxIndexContent(url,search,table_id);
                showHideDiv(show="#towerListLi",div="#towerList");
                $("#towerPackageDetailsTable").bootstrapTable("load",[]);
                $("#packageDetailsTable").bootstrapTable("load",[]);
            });

            //双击某一行(弹窗)
            table.on('dbl-click-row.bs.table',function(row, $element){
                layer.msg("请双击杆塔列表中的杆塔进入打包界面！");
            });

            //选择某一行
            $("#towerListTable").on('check.bs.table',function(row, $element){
                var url = "chain/pack/pack/towerPackageDetailsTable";
                var supplement = {TD_TypeName:$element["TD_TypeName"],old_TypeName:$element["old_TypeName"],SCD_Count:$element["SCD_Count"],SCD_TPNum:$element["SCD_TPNum"]};
                var search = {pId: $element["P_ID"],supplement:JSON.stringify(supplement)};
                var table_id = "towerPackageDetailsTable";
                ajaxIndexContent(url,search,table_id);
                showHideDiv(show="#towerPackageDetailsLi",div="#towerPackageDetails");
                $("#packageDetailsTable").bootstrapTable("load",[]);
            });

            //双击某一行(弹窗)
            $("#towerListTable").on('dbl-click-row.bs.table',function(row, $element){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                };
                Fast.api.open('chain/pack/pack/edit/ids/'+$element.P_ID,"编辑",options);
            });

            //选择某一行
            $("#towerPackageDetailsTable").on('check.bs.table',function(row, $element){
                var url = "chain/pack/pack/packageDetailsTable";
                var search = {pdId:$element["PD_ID"],scdId:$element["SCD_ID"]};
                var table_id = "packageDetailsTable";
                ajaxIndexContent(url,search,table_id);
                showHideDiv(show="#packageDetailsLi",div="#packageDetails");
            });

            //双击某一行(弹窗)
            $("#towerPackageDetailsTable").on('dbl-click-row.bs.table',function(row, $element){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                };
                Fast.api.open('chain/pack/pack/edit/ids/'+$element.P_ID,"编辑",options);
            });
            function ajaxIndexContent(url='',search={},table_id=""){
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: search,
                    success: function (ret) {
                        var contentData = [];
                        if(ret.code == 1){
                            contentData = ret.data;
                        }
                        $("#"+table_id+"").bootstrapTable("load",contentData);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            }
            function showHideDiv(show="",div=""){
                var table_hd = $(".nav-tabs li");
                $.each(table_hd,function(index,e){
                    $(e).removeClass("active");
                });
                var table_div = $(".nav-tab-content div");
                $.each(table_div,function(index,e){
                    $(e).removeClass("active in");
                });
                $(show).addClass("active");
                $(div).addClass("active in");
            }

            $(document).on("click", ".btn-multi-pack", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback: function(value){
                        table.bootstrapTable("refresh",{silent: true});
                    }
                };
                Fast.api.open('chain/pack/pack/multiPack',"批量空包",options);
            });
        },
        add: function () {
            Controller.api.bindevent("add");
            $(document).on('click', "#chooseTower", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        $("#TD_ID").val(value["TD_ID"]);
                        $("#old_TypeName").val(value["old_TypeName"]);
                        $("#T_Num").val(value["T_Num"]);
                        $("#C_ProjectName").val(value["C_ProjectName"]);
                        $("#TD_TypeName").val(value["TD_TypeName"]);
                        $("#TD_Pressure").val(value["TD_Pressure"]);
                        $("#P_Count").val("");
                        $("#TH_Height").val("");
                        $("#SCD_ID").val("");
                        $("#SCD").val("");
                        layer.msg("请继续选择杆塔！");
                    }
                };
                Fast.api.open('chain/pack/pack/chooseTower/',"任务单选择框",options);
            });
            $(document).on('click', "#chooseSCD", function(e){
                var TD_ID = $("#TD_ID").val();
                if(TD_ID == ''){
                    layer.msg("请先选择塔型");
                    return false;
                }
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        $("#P_Count").val(value["SCD_Count"]);
                        $("#TH_Height").val(value["TH_Height"]);
                        $("#SCD_ID").val(value["SCD_ID"]);
                        $("#SCD").val(value["SCD_TPNum"]);
                    }
                };
                Fast.api.open('chain/pack/pack/chooseScd/td_id/'+TD_ID,"杆塔信息选择窗体",options);
            });
        },
        choosetower: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/pack/chooseTower' + location.search,
                    table: 'taskdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'TD_ID',
                sortName: 'TD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                sortOrder: 'DESC',
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'TD_ID', title: 'TD_ID', operate: false,visible:false},
                        {field: 'old_TypeName', title: 'old_TypeName', operate: false,visible:false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 't.T_Num', title: '任务单号', operate: 'LIKE', visible: false},
                        {field: 'C_Num', title: '合同号', operate: 'LIKE', operate: false},
                        // {field: 'C_Num', title: '合同号', operate: 'LIKE'},
                        {field: 'P_Name', title: '产品名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'td.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'C_ProjectName', title: '工程名称', operate: false},
                        {field: 'P_Num', title: '线路名称', operate: 'LIKE'},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'T_Company', title: '下发公司', operate: false},
                        {field: 't_shortproject', title: '工程简写', operate: 'LIKE'},
                        {field: 'Customer_Name', title: '客户', operate: 'LIKE'},
                        {field: 'C_SaleMan', title: '业务员', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent[0]);

            });
        },
        choosescd: function () {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.content,
                clickToSelect: true,
                singleSelect: true,
                sortOrder: 'DESC',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'SCD_ID', title: 'SCD_ID', operate: false, visible: false},
                        {field: 'P_ID', title: 'P_ID', operate: false, visible: false},
                        {field: 'state', title: '打包状态'},
                        {field: 't_project', title: '工程名称'},
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'PD_Sect', title: '配段信息'},
                        {field: 'T_Num', title: '任务单号'},
                        {field: 'SCD_Count', title: '数量'},
                        {field: 'TH_Height', title: '呼高'},
                        {field: 'TD_Pressure', title: '电压等级'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent[0]);

            });
        },
        edit: function () {
            Controller.api.bindevent("edit");
            // var options = {
            //     shadeClose: false,
            //     shade: [0.3, '#393D49'],
            //     area: ["50%","70%"]
            // };
            // Fast.api.open('chain/pack/pack/packWeldCompact/ids/'+Config.ids,"放样-包装电焊件组数对比",options);

            var left_table = $("#leftPartTable");
            var right_table = $("#rightPartTable");
            var height = document.body.clientHeight-$(".edit-part").height()-$(".rightPartDiv").height()-$(".operate").height();
            $(".leftPartDiv").css("height", $(".rightPartDiv").height()+"px");
            // 初始化表格
			
            left_table.bootstrapTable({
                data: Config.leftList,
                height:height,
                uniqueId:"id",
                rowStyle:function(row,index){
                    var rowstyle = {};
                    if(row.TP_Welding!=0){
                        rowstyle = {css:{'background':"#E0FFFF"}};
                    }
                    return rowstyle;
                },
                // rowAttributes: function (row, index) {
                //     return row.pid == 0 ? {} : {style: "display:none"};
                // },
                columns: [
                    [
                        {field: 'index_id', title: "", formatter: function(value,row,index){
                            return ++index;
                        }},
                        {checkbox: true,field: 'choose'},
                        {field: 'DtMD_ID_PK', title: 'DtMD_ID_PK',visible:false},
                        {field: 'id', title: 'id',visible:false},
                        {field: 'DCD_ID', title: 'DCD_ID',visible:false},
                        {field: 'TP_SectName', title: '段号', sortable: true},
                        {field: 'TP_PartsID', title: '件号', sortable: true,cellStyle: function(value,row,index){
                            var style = {};
                            if(row.PD_IsM==1){
                                style = {css:{'font-size':"14px",'font-weight':700,'background':"#E0FFFF"}};
                            }
                            return style;
                        }},
                        {field: 'DtMD_sSpecification', title: '规格', sortable: true},
                        {field: 'DtMD_sMaterial', title: '材质'},
                        {field: 'DtMD_iLength', title: '长度(mm)'},
                        {field: 'DtMD_fWidth', title: '宽度(mm)'},
                        {field: 'TP_PackCount', title: '数量'},
                        {field: 'DtMD_fWeight', title: '重量(kg)'},
                        {field: 'welding', title: '电焊', sortable: true},
                        {field: 'welding_group', title: '电焊组数'},
                        {field: 'DtMD_iFireBending', title: '制弯'},
                        {field: 'DtMD_iBackGouging', title: '清根'},
                        {field: 'DtMD_fBackOff', title: '铲背'},
                        {field: 'DtMD_DaBian', title: '打扁'},
                        {field: 'DtMD_sRemark', title: '备注', sortable: true},
                        {field: 'PD_IsM', title: 'PD_IsM',visible:false},
                        // {
                        //     field: 'DCD_ID',
                        //     title: '<a href="javascript:;" class="btn btn-success btn-xs btn-toggle" style="border-top:none;"><i class="fa fa-chevron-up"></i></a>',
                        //     operate: false,
                        //     formatter: Controller.api.formatter.subnode
                        // }
                    ]
                ]
            });
			
            Table.api.bindevent(left_table);
            left_table.on('sort.bs.table',function(name, order){
                multiple_checks("leftPartTable");
            });
            // 初始化表格
            right_table.bootstrapTable({
                data: [],
                height:height,
                uniqueId:"id",
                rowStyle:function(row,index){
                    var rowstyle = {};
                    if(row.TP_Welding==1){
                        rowstyle = {css:{'background':"#E0FFFF"}};
                    }
                    return rowstyle;
                },
                columns: [
                    [
                        {field: 'index_id', title: "", formatter: function(value,row,index){return ++index;}},
                        {checkbox: true,field: "choose"},
                        {field: 'DtMD_ID_PK', title: 'DtMD_ID_PK',visible:false},
                        {field: 'DCD_ID', title: 'DCD_ID',visible:false},
                        {field: 'TP_SectName', title: '段号', sortable: true},
                        {field: 'TP_PartsID', title: '件号', sortable: true,cellStyle: function(value,row,index){
                            var style = {};
                            if(row.PD_IsM==1){
                                style = {css:{'font-size':"14px",'font-weight':700,'background':"#E0FFFF"}};
                            }
                            return style;
                        }},
                        {field: 'DtMD_sSpecification', title: '规格', sortable: true},
                        {field: 'DtMD_sMaterial', title: '材质', sortable: true},
                        {field: 'DtMD_iLength', title: '长度(mm)', sortable: true},
                        {field: 'DtMD_fWidth', title: '宽度(mm)'},
                        {field: 'TP_PackCount', title: '数量'},
                        {field: 'DtMD_fWeight', title: '重量(kg)'},
                        {field: 'welding', title: '电焊', sortable: true},
                        {field: 'welding_group', title: '电焊组数'},
                        {field: 'DtMD_iFireBending', title: '制弯'},
                        {field: 'DtMD_iBackGouging', title: '清根'},
                        {field: 'DtMD_fBackOff', title: '铲背'},
                        {field: 'DtMD_DaBian', title: '打扁'},
                        {field: 'DtMD_sRemark', title: '备注', sortable: true},
                        {field: 'DCD_ID', title: 'DCD_ID',visible: false},
                        {field: 'PD_IsM', title: 'IsM',visible:false}
                    ]
                ]
            });
            Table.api.bindevent(right_table);
            $(document).on('click', "#packpro", function(e){
                var pack_content = right_table.bootstrapTable('getData');
                var select_val = $("select[name='pack_list'] option:selected").val();
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["40%","60%"],
                    callback:function(value){
                        layer.msg("正在打包中，请稍等");
                        $.ajax({
                            url: 'chain/pack/pack/packSetSave',
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            data: {row:JSON.stringify(value), data: JSON.stringify(pack_content)},
                            success: function (ret) {
                                layer.msg(ret.msg);
                                if (ret.hasOwnProperty("code")) {
                                    if (ret.code === 1) {

                                        //left表格刷新
                                        left_table.bootstrapTable('load',[]);
                                        $("#sumTj").text("");
                                        $.ajax({
                                            url:'chain/pack/pack/leftTable',
                                            type: 'post',
                                            dataType: 'json',
                                            async: false,
                                            data: {num: Config.ids},
                                            success: function (ret) {
                                                var tableContent = [];
                                                var msg = "";
                                                if(ret.code==1){
                                                    tableContent = ret.data;
                                                    msg = ret.msg;
                                                }
                                                left_table.bootstrapTable('load',tableContent);
                                                $("#sumTj").text(msg);
                                            }
                                        })

                                        $("#sumDb span").text("0");
                                        $("#sumDb").css("color","black");
                                        // window.location.reload();
                                        $("#pack").html(ret.select_html);
                                        $('#rightPartTable').bootstrapTable('removeAll');
                                        if(ret.saveResult!=0 && ret.pack_pl_list.length > 2){
                                            var options = {
                                                shadeClose: false,
                                                shade: [0.3, '#393D49'],
                                                area: ["80%","80%"],
                                            };
                                            var scd_list = $.base64.encode(ret.pack_pl_list);
                                            Fast.api.open('chain/pack/pack/packBatch/pd_id/'+ret.saveResult+'/scd_list/'+scd_list,"批量打包",options);
                                        }
                                    }
                                }
                                
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }
                };
                Fast.api.open('chain/pack/pack/packSet/p_id/'+Config.ids+'/pd_id/'+select_val,"打包",options);
            });

            $(document).on('click', "#delPack", function(e){
                var select_val = $("select[name='pack_list'] option:selected").val();
                if(select_val!=0){
                    $.ajax({
                        url: 'chain/pack/pack/delPack',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {pd_id:select_val},
                        success: function (ret) {
                            layer.msg(ret.msg);
                            // if (ret.hasOwnProperty("code")) {
                                if (ret.code === 1) {
                                    window.location.reload();
                                    // right_table.bootstrapTable('load',ret.data);
                                    // $("#pack").html(ret.select_html);
                                    // right_table.bootstrapTable('load',[]);
                                }
                            // }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }else{
                    layer.msg("无法删除");
                }
            });
            $(document).on('change', "#pack_list", function(e){
                var pack_val = $("#pack_list option:selected").val();
				multiple_checks("leftPartTable");				
                if(pack_val!=0){
                    $.ajax({
                        url: 'chain/pack/pack/packContent',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {pd_id:pack_val},
                        success: function (ret) {
                            if (ret.code === 1) {
                                right_table.bootstrapTable('load',ret.data);
                                $("#sumDb span").text(parseFloat(ret.weight).toFixed(2));
                                if(ret.weight>=3000) $("#sumDb").css("color","red");
                                else $("#sumDb").css("color","black");	
								multiple_checks("rightPartTable");
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                }else{

                    right_table.bootstrapTable('load',[]);	
                    $("#sumDb span").text("0");
                    $("#sumDb").css("color","black");
					multiple_checks("rightPartTable");						
                }
				
                //left表格刷新
                left_table.bootstrapTable('load',[]);
                $("#sumTj").text("");
                $.ajax({
                    url:'chain/pack/pack/leftTable',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {num: Config.ids},
                    success: function (ret) {
                        var tableContent = [];
                        var msg = "";
                        if(ret.code==1){
                            tableContent = ret.data;
                            msg = ret.msg;
                        }
                        left_table.bootstrapTable('load',tableContent);
                        $("#sumTj").text(msg);
                    }
                })
					
				
            });

            $(document).on('click', "#leftToRight", function(e){
                var pack_content = left_table.bootstrapTable('getAllSelections');
                if(pack_content.length == 0) layer.msg("请选择！");
                else{
                    var field_list = [];
                    var weight = parseFloat($("#sumDb span").text());
                    $.each(pack_content,function(e,index){
                        field_list.push(index.id);
                        weight += parseFloat(index.total_weight);
                    })
                    right_table.bootstrapTable("append",pack_content);
                    right_table.bootstrapTable('uncheckAll');
                    left_table.bootstrapTable("remove",{field:"id",values:field_list});
                    $("#sumDb span").text(parseFloat(weight).toFixed(2));
                    if(weight>=3000) $("#sumDb").css("color","red");
                    else $("#sumDb").css("color","black");
                    multiple_checks("leftPartTable");
                    multiple_checks("rightPartTable");
                }
            });

            $(document).on('click', "#rightToLeft", function(e){
				//console.log(left_table.bootstrapTable("getData"));
                var pack_content = right_table.bootstrapTable('getAllSelections');
                if(pack_content.length == 0) layer.msg("请选择！");
                else{
                    var field_list = [];
					var olddata=left_table.bootstrapTable("getData");
                    var weight = parseFloat($("#sumDb span").text());
                    $.each(pack_content,function(e,index){
						// right_table.bootstrapTable("removeByUniqueId","index.id");
                        field_list.push(index.id);
                        //left_table.bootstrapTable("insertRow",{index:index.id-1,row:index});
						olddata.splice(index.id-1, 0, index);
                        weight -= parseFloat(index.total_weight);
                    })
					left_table.bootstrapTable("load",olddata)
                    left_table.bootstrapTable('uncheckAll');
                    right_table.bootstrapTable("remove",{field:"id",values:field_list});
                    $("#sumDb span").text(parseFloat(weight).toFixed(2));
                    if(weight>=3000) $("#sumDb").css("color","red");
                    else $("#sumDb").css("color","black");
					multiple_checks("leftPartTable");	
					multiple_checks("rightPartTable");	
                }
            });
            multiple_checks("leftPartTable");
            multiple_checks("rightPartTable");

            function multiple_checks(table_id='leftPartTable')
            {
                var mousedown=false;var check = 0;var choose;
                var allData;
                $("body").on("mousedown", "#"+table_id+" tbody tr",function(e){
					allData = $("#"+table_id).bootstrapTable("getData");
                    mousedown=true;
                    check = $(this).index();
                    var check_element =allData[check];
                    choose = check_element.choose;
                });
                $("body").on("mouseup", "#"+table_id+" tbody tr",function(e){
                    if(mousedown){
                        var mousedown_check = $(this).index();
                        var big = check>mousedown_check?check:mousedown_check;
                        var small = check<mousedown_check?check:mousedown_check;
                        if(big == small){
                            if(choose) $("#"+table_id).bootstrapTable('uncheck', big);
                            else $("#"+table_id).bootstrapTable('check', big);
                        }else{
                            var id_list = [];
                            for( var i=small;i<=big;i++){
                                id_list.push(allData[i].id);
                            }
                            if(choose){
                                if(id_list.length>0) $("#"+table_id).bootstrapTable("uncheckBy", {field:"id", values:id_list})
                            }else{
                                if(id_list.length>0) $("#"+table_id).bootstrapTable("checkBy", {field:"id", values:id_list})
                            }
                        }
                    }
                    mousedown=false;
                });
            }

            
            $(document).on("click", "#author", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/pack/pack/auditor",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('确定弃审？',"chain/pack/pack/giveUp",num);
                
            });
            function check(msg,url,num){
                // console.log(url,msg,num);return false;
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                window.location.reload();
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }

            $(document).on("click", "#editPack", function () {
                let indexp = layer.prompt({
                    title: '修改件数',
                    placeholder: '例如1#+20需要改成1#+40 则输入40',
                }, function (value, index, elem) {
                    $.ajax({
                        url: 'chain/pack/pack/updatePackName',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {p_id:Config.ids,change:value},
                        success: function (ret) {
                            if(ret.code === 1){
                                window.location.reload();
                            }else layer.msg(ret.msg);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                })
            });

            $(document).on('click', "#chooseSCD", function(e){
                var TD_ID = $("#TD_ID").val();
                if(TD_ID == ''){
                    layer.msg("无塔型，请稍后再试");
                    return false;
                }
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        if(value.P_ID!=0){
                            window.location.href = "/admin.php/chain/pack/pack/edit/ids/"+value.P_ID;
                        }else{
                            $.ajax({
                                url: 'chain/pack/pack/add',
                                type: 'post',
                                dataType: 'json',
                                async: false,
                                data: {'row[TD_ID]':TD_ID,'row[P_Count]':1,'row[SCD_ID]':value.SCD_ID},
                                success: function (ret) {
                                    if(ret.code === 1){
                                        window.location.href = "/admin.php/chain/pack/pack/edit/ids/"+ret.data;
                                    }else layer.msg(ret.msg);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }
                    }
                };
                Fast.api.open('chain/pack/pack/chooseScd/td_id/'+TD_ID,"杆塔信息选择窗体",options);
            });

            $(document).on("click", "#showWeld", function () {
                var select_val = $("select[name='pack_list'] option:selected").val();
                if(select_val!=0){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["80%","100%"]
                    };
                    Fast.api.open('chain/pack/pack/showWeld/pd_id/'+select_val,"显示电焊件",options);
                }else{
                    layer.msg("请先保存包");
                }
            });
        },
        showweld: function () {
            var table = $("#table");
            table.bootstrapTable({
                data: Config.list,
                height:document.body.clientHeight-100,
                rowStyle:function(row,index){
                    var rowstyle = {};
                    if(row.TP_Welding==1){
                        rowstyle = {css:{'background':"#E0FFFF"}};
                    }
                    return rowstyle;
                },
                columns: [
                    [
                        {field: 'index_id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'DtMD_ID_PK', title: 'DtMD_ID_PK',visible:false},
                        {field: 'DCD_ID', title: 'DCD_ID',visible:false},
                        {field: 'TP_SectName', title: '段号', sortable: true},
                        {field: 'TP_PartsID', title: '件号', sortable: true,cellStyle: function(value,row,index){
                            var style = {};
                            if(row.PD_IsM==1){
                                style = {css:{'font-size':"14px",'font-weight':700,'background':"#E0FFFF"}};
                            }
                            return style;
                        }},
                        {field: 'DtMD_sSpecification', title: '规格', sortable: true},
                        {field: 'DtMD_sMaterial', title: '材质', sortable: true},
                        {field: 'DtMD_iLength', title: '长度(mm)', sortable: true},
                        {field: 'DtMD_fWidth', title: '宽度(mm)'},
                        {field: 'TP_PackCount', title: '数量'},
                        {field: 'DtMD_fWeight', title: '重量(kg)'},
                        {field: 'welding', title: '电焊', sortable: true},
                        {field: 'welding_group', title: '电焊组数'},
                        {field: 'DtMD_iFireBending', title: '制弯'},
                        {field: 'DtMD_iBackGouging', title: '清根'},
                        {field: 'DtMD_fBackOff', title: '铲背'},
                        {field: 'DtMD_DaBian', title: '打扁'},
                        {field: 'DtMD_sRemark', title: '备注', sortable: true},
                    ]
                ]
            });
            Table.api.bindevent(table);
        },
        packset: function(){
            $(document).on('click', ".btn-success", function(e){
                tijiao();
            });
            $(document).on("keyup", function (e) {
                if (e.keyCode == 13) {
                    tijiao();
                    return false;
                }
            });
            $(document).on("keyup", "input[name='PD_Name']", function (e) {
                if (e.keyCode == 13) {
                    tijiao();
                    return false;
                }
            });
            function tijiao()
            {
                var PD_Name = $("#PD_Name").val();
                if(PD_Name == ''){
                    layer.msg("请输入包名后再提交！");
                    return false;
                }
                var content = $("#set-form").serializeArray();
                Fast.api.close(content);
            }
        },
        packbatch: function () {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.scd_list,
                height:document.body.clientHeight,
                columns: [
                    [
                        {field: 'id', title: "ID", formatter: function(value,row,index){return ++index;}},
                        {checkbox: true},
                        {field: 'SCD_ID', title: 'SCD_ID', visible: false},
                        {field: 'TD_ID', title: 'TD_ID', visible: false},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'TH_Height', title: '呼高'},
                        {field: 'TD_TypeName', title: '塔型'}
                    ]
                ]
            });
            $(document).on('click', "#pack", function(e){
                var content = table.bootstrapTable('getSelections');
                $.ajax({
                    url: 'chain/pack/pack/packBatchSet',
                    type: 'post',
                    dataType: 'json',
                    data: {pd_id:Config.pd_id,data: JSON.stringify(content)},
                    success: function (ret) {
                        Layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        packweldcompact: function(){
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.content,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'DCD_PartName', title: '段名'},
                        {field: 'DCD_PartNum', title: '电焊组件'},
                        {field: 'DCD_Count', title: '放样电焊组数'},
                        {field: 'packnum', title: '已包电焊组数'},
                        {field: 'discount', title: '差额',cellStyle: function(value,row,index){
                            var style = {};
                            if(value!=0){
                                style = {css:{'background':"#FFC0CB"}};
                            }
                            return style;
                        }}
                    ]
                ]
            });
        },
        multipack: function () {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: [],
                sortOrder: 'DESC',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'SCD_ID', title: 'SCD_ID', operate: false, visible: false},
                        {field: 'P_ID', title: 'P_ID', operate: false, visible: false},
                        {field: 'state', title: '打包状态'},
                        {field: 't_project', title: '工程名称'},
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'T_Num', title: '任务单号'},
                        {field: 'SCD_Count', title: '数量'},
                        {field: 'TH_Height', title: '呼高'},
                        {field: 'TD_Pressure', title: '电压等级'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent[0]);
            });
            $(document).on('click', "#chooseTower", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        table.bootstrapTable('load',[]);
                        $("#TD_ID").val(value["TD_ID"]);
                        $("#TD_TypeName").val(value["TD_TypeName"]);
                        Fast.api.ajax({
                            url: "chain/pack/pack/chooseMultiScd", 
                            data: {"TD_ID":value["TD_ID"]}
                        }, function(data, ret){
                            if(ret.code==1) table.bootstrapTable('load',data);
                        });
                    }
                };
                Fast.api.open('chain/pack/pack/chooseTower/',"塔型选择",options);
            });
            $(document).on("click", "#multiPack", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else{
                    var TD_ID = $("#TD_ID").val();
                    Fast.api.ajax({
                        url: "chain/pack/pack/multiPack", 
                        data: {"TD_ID":TD_ID,"tableContent":JSON.stringify(tableContent)}
                    }, function(data, ret){
                        if(ret.code==1) Fast.api.close('');
                    });
                }
                // else Fast.api.close(tableContent[0]);
            });
        },
        api: {
            // formatter: {
            //     subnode: function (value, row, index) {
            //         return '<a href="javascript:;" data-toggle="tooltip" title="' + __('Toggle sub menu') + '" data-id="' + row.id + '" data-pid="' + row.pid + '" class="btn btn-xs '
            //             + (row.haschild == 1 || row.ismenu == 1 ? 'btn-success' : 'btn-default disabled') + ' btn-node-sub"><i class="fa fa-sitemap"></i></a>';
            //     }
            // },
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    layer.msg("保存成功！");
                    if(field == "add") window.location.href = "edit/ids/"+data;
                    return false;
                });
                $(document).on('click', "#packWeldCompact", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["80%","100%"]
                    };
                    Fast.api.open('chain/pack/pack/packWeldCompact/ids/'+Config.ids,"放样-包装电焊件组数对比",options);
                });
                
                // $(document).on('click', "#leftPartTable tbody input[name='btSelectItem']", function(e)
                // {
                //     console.log(1,e,e.shiftKey);
                //     if (e.shiftKey)
                //     {
                //         console.log(1);
                //     }
                // })
                // $(document).on('click', "#leftPartTable .bs-checkbox", function(e)
                // {
                //     console.log(2,e,e.shiftKey);
                //     if (e.shiftKey)
                //     {
                //         console.log(1);
                //     }
                // })

                
            }
        }
    };
    return Controller;
});