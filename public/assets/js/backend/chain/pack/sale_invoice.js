define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'fhdddata', 'cpckddata'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: [],
                height: 250,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'sum_weight', title: '总重(kg)'},
                        {field: 'SIM_CarNumber', title: '车牌号'},
                        {field: 'SIM_Money', title: '运费金额'},
                        {field: 'SIM_Person', title: '司机'},
                        {field: 'SIM_fyphone', title: '联系电话'},
                        {field: 'NEWweight', title: '过磅重(kg)'},
                        {field: 'SIM_Num', title: '发运单号'},
                        {field: 'SI_Num', title: '通知单号'},
                        {field: 'SIM_Date', title: '出库时间'},
                        {field: 'C_Project', title: '工程名称'}
                    ]
                ]
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/sale_invoice/index' + location.search,
                    add_url: 'chain/pack/sale_invoice/add',
                    edit_url: 'chain/pack/sale_invoice/edit',
                    del_url: 'chain/pack/sale_invoice/del',
                    multi_url: 'chain/pack/sale_invoice/multi',
                    import_url: 'chain/pack/sale_invoice/import',
                    table: 'saleinvoice',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SI_Num',
                sortName: 'SI_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                height: 500,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {
                            field: 'buttons',
                            title: '新增发运',
                            operate: false,
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'invoice',
                                    text: '新增发运',
                                    title: '新增发运',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-plus',
                                    url: 'chain/pack/sale_invoice/detailAdd/ids/{ids}'
                                }
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'SI_Num', title: '发货单号', operate: 'LIKE'},
                        {field: 'category', title: '类别', searchList: {"铁塔": "铁塔", "铁附件": "铁附件"}},
                        {field: 'SI_Customer', title: '客户名称', operate: 'LIKE'},
                        {field: 'T_Num', title: '任务单号', operate: 'LIKE'},
                        {field: 'C_Project', title: '工程名称', operate: 'LIKE'},
                        {field: 'SI_Date', title: '发货时间', operate: 'LIKE', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'SI_Way', title: '发货方式', searchList:Config.way_list},
                        {field: 'SI_Address', title: '发货地址', operate: false},
                        {field: 'SI_SaleMan', title: '业务员', operate: false},
                        {field: 'SI_Contractor', title: '联系人', operate: false},
                        {field: 'SI_Phone', title: '联系电话', operate: false},
                        {field: 'Writer', title: '制单人', operate: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'si.Writer', title: '制单人', operate: 'LIKE', visible:false},
                        // {field: 'Writedate', title: '制单时间', operate: false},
                        {field: 'Writedate', title: '制单时间', operate: 'LIKE', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Auditor', title: '审核人', operate: false},
                        {field: 'Auditordate', title: '审核时间', operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var ids = $element.ids;
                if(ids == '') return false;
                $.ajax({
                    url: 'chain/pack/sale_invoice/content',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:ids},
                    success: function (ret) {
                        var content = {};
                        if (ret.code === 1) {
                            content = ret.data;
                            $.each(content,function(index,e){
                                content[index]['C_Project'] = $element.C_Project;
                            });
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
            
            // 为表格绑定事件
            Table.api.bindevent($("#no_issued"));
            $("#no_issued").on('dbl-click-row.bs.table',function(row, $element){
                var ids = $element.SIM_Num;
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                };
                Fast.api.open('chain/pack/sale_invoice/detailEdit/ids/'+ids,"发运记录",options);
            })
        },
        add: function () {
            Controller.api.bindevent("add");
            $('#tbshow').on('keyup','td input[name="table_row[SIS_Count][]"]',function () {
                var count = $(this).val();
                count==''?count=0:parseFloat(count);
                var td_index = $(this).parents('tr');
                var single_weight = td_index.find("td input[name='table_row[weight][]']").val();
                single_weight==''?single_weight=0:parseFloat(single_weight);
                var sum_weight = (count*single_weight).toFixed(2);
                td_index.find("td input[name='table_row[SIS_Weight][]']").val(sum_weight);
            })
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $("#print").click(function () {
                window.top.Fast.api.open('chain/pack/sale_invoice/print/ids/' + Config.ids, '成品发运', {
                    area: ["100%", "100%"]
                });
            });
            $('#tbshow').on('keyup','td input[name="table_row[SIS_Count][]"]',function () {
                var count = $(this).val();
                count==''?count=0:parseFloat(count);
                var td_index = $(this).parents('tr');
                var single_weight = td_index.find("td input[name='table_row[weight][]']").val();
                single_weight==''?single_weight=0:parseFloat(single_weight);
                var sum_weight = (count*single_weight).toFixed(2);
                td_index.find("td input[name='table_row[SIS_Weight][]']").val(sum_weight);
            })
        },
        choosedetail: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/sale_invoice/chooseDetail/c_num/' + Config.C_Num + location.search,
                    table: 'sectconfigdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SCD_ID',
                sortName: 'SCD_ID',
                sortOrder: 'ASC',
                search: false,
                showToggle: false,
                showExport: false,
                onlyInfoPagination: true,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'},
                        {field: 't_shortproject', title: '工程简称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'SCD_TPNum', title: '杆塔号', operate: 'LIKE'},
                        {field: 'Part', title: '配段信息', operate: 'LIKE'},
                        {field: 'SCD_Count', title: '配段基数', operate: 'LIKE'},
                        {field: 'T_Num', title: '任务单号', operate: 'LIKE'},
                        {field: 'TD_Pressure', title: '电压等级', operate: 'LIKE'},
                        {field: 'TH_Height', title: '呼高', operate: 'LIKE'},
                        {field: 'SCD_Weight', title: '单基重量', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        detailadd: function () {
            Form.api.bindevent($("form[role=form]"),function(data,ret){
                window.location.href = "/admin.php/chain/pack/sale_invoice/detailEdit/ids/"+data;
                return false;
            });
            $(document).on("click", "#allchoose", function () {
                var flag = $(this).prop("checked");
                $("#left_tbshow").find("tr").each(function () {
                    var checkedField = $(this).children('td').eq(0).find('input');
                    if(flag) checkedField.prop("checked","true");
                    else checkedField.prop("checked","");
                });
            });
            $(document).on('click', "#left_tbshow tr", function(e){
                var choose_index = $(e.target).parents("tr").find("input[name='table_row[choose][]']");
                if(choose_index.prop("checked")===true) choose_index.prop("checked","");
                else choose_index.prop("checked","true");
            })
        },
        detailedit: function () {
            Form.api.bindevent($("form[role=form]"),function(data,ret){
                window.location.reload();
                return false;
            });
            $(document).on("click", "#allchoose", function () {
                var flag = $(this).prop("checked");
                $("#left_tbshow").find("tr").each(function () {
                    var checkedField = $(this).children('td').eq(0).find('input');
                    if(flag) checkedField.prop("checked","true");
                    else checkedField.prop("checked","");
                });
            });
            $(document).on('click', ".ctrlz", function(e){
                var SID_ID = $(this).parents("tr").find('td').eq(2).text().trim();
                $.ajax({
                    url: 'chain/pack/sale_invoice/ctrlzContent',
                    type: 'post',
                    dataType: 'json',
                    data: {num: SID_ID},
                    success: function (ret) {
                        if(ret.code ==1){
                            window.location.reload();
                        }else layer.msg(ret.msg);
                        
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            })
            $(document).on('click', "#allDel", function(e){
                var num = Config.ids;
                $.ajax({
                    url: 'chain/pack/sale_invoice/detailDel',
                    type: 'post',
                    dataType: 'json',
                    data: {num: num},
                    success: function (ret) {
                        if(ret.code ==1){
                            Fast.api.close();
                        }else layer.msg(ret.msg);
                        
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            })
            $(document).on('click', "#left_tbshow tr", function(e){
                var choose_index = $(e.target).parents("tr").find("input[name='table_row[choose][]']");
                if(choose_index.prop("checked")===true) choose_index.prop("checked","");
                else choose_index.prop("checked","true");
            })

            $("#print").click(function () {
                window.top.Fast.api.open('chain/pack/sale_invoice/detailprint/ids/' + Config.ids, '产品出库', {
                    area: ["100%", "100%"]
                });
            });

            // $(document).on('click', "#print", function(e){
            //     var options = {
            //         shadeClose: false,
            //         shade: [0.3, '#393D49'],
            //         area: ["70%","50%"],
            //     };
            //     Fast.api.open('chain/pack/sale_invoice/detailPrintMain/ids/' + Config.ids,"选择车牌",options);
            // })
            
            
            
        },
        // detailprintmain: function () {
        //     $("#print").click(function () {
        //         var card = $("select[name='row[card_list]']").val();
        //         // console.log(card);
        //         window.top.Fast.api.open('chain/pack/sale_invoice/detailprint/ids/' + card, '产品出库', {
        //             area: ["100%", "100%"]
        //         });
        //     });
        // },
        print: function() {
            // console.log('limberList',Config.limberList);
            console.log('detail_list',Config.list);
            console.log('mainInfos',Config.row);
            
            let list = Config.list;
            let mainInfos = Config.row;
            let tb_tmp = [];
            let pageSize = 11;
            let pageCountSum = 0;
            let pageWeightSum = 0;
            // 处理数据
            let dataArr = list.map(p=>{
                return {
                    xh: '',
                    tx: p['TD_TypeName'],
                    hg: p['TH_Height'],
                    gt: p['SIS_TpNum'],
                    dw: p['SCD_Unit']=='基'? '公斤': p['SCD_Unit'],
                    sl: p['SIS_Count'],
                    zl:p['SIS_Weight'],
                    bz:p['SID_Memo'],
                }
            })

            // 补齐空白行
            for (let i = 0; i < (pageSize-1)-list.length%pageSize; i++) {
                
                dataArr.push({
                    xh: '',
                    tx: '',
                    hg: '',
                    gt: '',
                    dw: '',
                    sl: '',
                    zl:'',
                    bz:'',
                });
            }

            // 添加页小计行
            tb_tmp = dataArr.reduce((tmpArr, p, index)=>{
                p.xh = index+1
                tmpArr.push(p);
                pageCountSum+=p.sl*100;
                pageWeightSum+=p.zl*100
                
                if((index+1)==dataArr.length){
                    tmpArr.push({
                        xh: '合计',
                        tx: '',
                        hg: '',
                        gt: '',
                        dw: '',
                        sl: pageCountSum/100,
                        zl:pageWeightSum/100,
                        bz:'',
                    });
                    
                }
                return tmpArr;
            },[]);
            
            // console.log('tb_tmp', tb_tmp)

            rklblist = {
                '11':'正常入库'
            }
            let data_tmp={
                khjc:mainInfos['Customer_Name'],
                xmmc: mainInfos['C_Project'],
                ddh: mainInfos['C_Num'],
                rwd: mainInfos['T_Num'],
                zdr: mainInfos['Writer'],
                fhdh:mainInfos['SI_Num'],
                fhrq:mainInfos['SI_Date'],
                
                cyr:'',
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            
            let htemp =  new hiprint.PrintTemplate({template: fhdddata});
            if(mainInfos['category']=='铁塔'){
                htemp =  new hiprint.PrintTemplate({template: fhdddata2});
            }

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        detailprint: function() {
            // console.log('limberList',Config.limberList);
            
            console.log('mainInfos',Config.row);
            
            let list = Config.list;
            console.log('list', list);
            let mainInfos = Config.row;
            let tb_tmp = [];
            let pageSize = 29;
            
            tb_tmp = list.map((p, i)=>{
                return {
                    xh: i+1,
                    bh: p['bh'],
                    blx: p['PD_Content'],
                    sl: p['SID_Count'],
                    zdcd:p['MaxLength'],
                    sm:'',
                    tx:p['TD_TypeName'],
                }
            });
            

            // 补齐空白行
        //    if(tb_tmp.length%pageSize!=0){
        //     let emptyNum = pageSize-(tb_tmp.length%pageSize);
        //     // console.log('ss', pageSize-(tb_tmp.length%pageSize))
        //     for (let i = 0; i < emptyNum; i++) {
        //         // console.log(i)
        //         tb_tmp.push({
        //             xh: '',
        //             bh: '',
        //             blx: '',
        //             sl: '',
        //             zdcd: '',
        //             sm:'',
        //             tx:''
        //         });
        //     }
        //    }

            rklblist = {
                '11':'正常入库'
            }
            let data_tmp={
                shdd:mainInfos['SI_Address'],
                shdw: mainInfos['SI_Customer'],
                gcmc: mainInfos['C_Project'],
                hth: mainInfos['C_Num'],
                bz: mainInfos['SIM_Memo'],
                ckrq:mainInfos['SI_Date'],
                lxr:mainInfos['SI_Contractor'],
                lxdh:mainInfos['SI_Phone'],
                gcbh:mainInfos['PC_Num'],
                top_dh:mainInfos['SI_Num'],
                top_ch:mainInfos['SIM_CarNumber'],
                top_sj:mainInfos['SIM_Person'],
                top_lxdh:mainInfos['SIM_fyphone'],
                zdr:mainInfos['Writer'],
                zdrq:mainInfos['WriterDate'],
                dz:'绍兴市柯桥区钱清工业开发区',
                cz:'0575-84051968',
                dh:'0575-84050892',
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: cpckddata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        selectcompact: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/pack/sale_invoice/selectCompact' + location.search,
                    table: 'task',
                }
            });

            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'C_Num',
                // sortName: 'C_WriteDate',
                sortOrder: 'DESC',
                clickToSelect: true,
                singleSelect: true,
                search: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'T_Num', title: '任务单号', operate: 'LIKE'},
                        {field: 'C_Num', title: '合同号', operate: false},
                        // {field: 'c.C_Num', title: '合同号', operate: 'LIKE',visible: false},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE',},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'C_Project', title: '工程名称', operate: 'LIKE'},
                        {field: 'C_SortProject', title: '工程简称', operate: 'LIKE'},
                        {field: 'C_SaleMan', title: '业务员', operate: 'LIKE'},
                        
                    ]
                ]
            });
            
            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var formData=table.bootstrapTable('getAllSelections');
                Fast.api.close(formData[0]);
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                var js_way = Config.js_way;
                $(document).on('click', "#chooseCompact", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            if($("#c-C_Num").val() != value.C_Num) $("#tbshow").html("");
                            $("#c-C_Num").val(value.C_Num);
                            $("#c-C_Project").val(value.C_Project);
                            $("#c-SI_Customer").val(value.Customer_Name);
                            $("#c-T_Num").val(value.T_Num);
                            $("#c-category").val(value.T_Sort);
                            $("#c-unit").val(value.produce_type);
                            js_way = value.C_unit;
                        }
                    };
                    Fast.api.open('chain/pack/sale_invoice/selectcompact',"选择合同",options);
                })
                $(document).on('click', "#chooseDetail", function(e){
                    var larr=[];
                    $("#tbshow tr").find("td:eq(2) input").each(function () {
                        larr.push((parseFloat)($(this).val()));
                    });
                    var c_num = $("#c-C_Num").val();
                    if(c_num){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                var tableField = Config.tableField;
                                tableField[11][5] = js_way==1?"sum_price":"sum_price_unit";
                                var content = "";
                                $.each(value,function(v_index,v_e){
                                    if($.inArray(v_e["SCD_ID"],larr)==-1){
                                        content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                        $.each(tableField,function(t_index,e){
                                            content += '<td '+e[4]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[5]]) == 'undefined'?e[3]:v_e[e[5]])+'"></td>';
                                        });
                                        content += "</tr>";
                                    }
                                });
                                $("#tbshow").append(content);
                            }
                        };
                        Fast.api.open('chain/pack/sale_invoice/chooseDetail/c_num/'+c_num,"选择进仓明细",options);
                    }else{
                        layer.msg("请先选择合同！");
                    }
                    
                })
                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[SIS_ID][]"]').val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/pack/sale_invoice/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                    
                });
                $(document).on("click", "#author", function () {
                    if(!Config.ids){
                        layer.confirm('没有获取到发运单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/pack/sale_invoice/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    if(!Config.ids){
                        layer.confirm('没有获取到发运单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/pack/sale_invoice/giveUp",Config.ids);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
                
            }
        }
    };
    return Controller;
});