define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sequence/sequence/index' + location.search,
                    add_url: 'chain/sequence/sequence/add',
                    edit_url: 'chain/sequence/sequence/edit',
                    del_url: 'chain/sequence/sequence/del',
                    multi_url: 'chain/sequence/sequence/multi',
                    import_url: 'chain/sequence/sequence/import',
                    table: 'pc_sequence',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'sNum',
                sortName: 'sNum',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'sNum', title: __('Snum'), operate: 'LIKE'},
                        {field: 'PT_Num', title: __('Pt_num'), operate: false},
                        {field: 's.PT_Num', title: __('Pt_num'), operate: 'LIKE', visible: false},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: 'LIKE'},
                        {field: 'TD_Pressure', title: '电压等级', operate: 'LIKE'},
                        // {field: 'choose_order', title: __('Choose_order'), operate: 'LIKE'},
                        {field: 'PlanStartTime', title: __('Planstarttime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'PlanFinishTime', title: __('Planfinishtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'ActStartTime', title: __('Actstarttime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, 
                        formatter: function(value){ return value=='0000-00-00 00:00:00'?"":value;}},
                        {field: 'ActFinishTime', title: __('Actfinishtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false,
                        formatter: function(value){ return value=='0000-00-00 00:00:00'?"":value;}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate,
                        buttons: [
                            {
                                name: '下料',
                                text: "下料",
                                title: "下料",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-hand-pointer-o',
                                url: 'chain/sequence/sequence/detail/operate/TTGX01',
                                extend: 'data-area=["100%","100%"]',
                                visible: function (row) {
                                    //返回true时按钮显示,返回false隐藏
                                    if(row['TTGX01']) return true;
                                }
                            },{
                                name: '制孔',
                                text: "制孔",
                                title: "制孔",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-hand-pointer-o',
                                url: 'chain/sequence/sequence/detail/operate/TTGX02',
                                extend: 'data-area=["100%","100%"]',
                                visible: function (row) {
                                    //返回true时按钮显示,返回false隐藏
                                    if(row['TTGX02']) return true;
                                }
                            },{
                                name: '组装',
                                text: "组装",
                                title: "组装",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-hand-pointer-o',
                                url: 'chain/sequence/sequence/detail/operate/TTGX04',
                                extend: 'data-area=["100%","100%"]',
                                visible: function (row) {
                                    //返回true时按钮显示,返回false隐藏
                                    if(row['TTGX04']) return true;
                                }
                            },{
                                name: '焊接',
                                text: "焊接",
                                title: "焊接",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-hand-pointer-o',
                                url: 'chain/sequence/sequence/detail/operate/TTGX05',
                                extend: 'data-area=["100%","100%"]',
                                visible: function (row) {
                                    //返回true时按钮显示,返回false隐藏
                                    if(row['TTGX05']) return true;
                                }
                            },{
                                name: '镀锌',
                                text: "镀锌",
                                title: "镀锌",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-hand-pointer-o',
                                url: 'chain/sequence/sequence/detail/operate/TTGX06',
                                extend: 'data-area=["100%","100%"]',
                                visible: function (row) {
                                    //返回true时按钮显示,返回false隐藏
                                    if(row['TTGX06']) return true;
                                }
                            },{
                                name: '制弯',
                                text: "制弯",
                                title: "制弯",
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-hand-pointer-o',
                                url: 'chain/sequence/sequence/detail/operate/TTGX07',
                                extend: 'data-area=["100%","100%"]',
                                visible: function (row) {
                                    //返回true时按钮显示,返回false隐藏
                                    if(row['TTGX07']) return true;
                                }
                            }
                        ]}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            $(document).on('click', "#selectPtNum", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback: function (value){
                        $("#c-PT_Num").val(value.PT_Num);
                    }
                };
                Fast.api.open('chain/material/nesting/xddh',"选择下达单号",options);
            })
        },
        edit: function () {
            Controller.api.bindevent();
        },
        detail: function () {
            Controller.api.bindevent();
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: [],
                height: document.body.clientHeight-120,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'sStuff', title: '名称'},
                        {field: 'sMaterial', title: '材质'},
                        {field: 'sSpecification', title: '规格'},
                        {field: 'machine', title: '机器名称'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            getTable();
            function getTable()
            {
                Fast.api.ajax({
                    url: "chain/sequence/sequence/detailMachine", 
                    data: {"sdNum":Config.ids, "PT_Num":Config.PT_Num}
                }, function(data, ret){
                    table.bootstrapTable('load',data);
                });
            }

            $(document).on("click", "#machine", function () {
                var select_html = Config.machineList;
                var table_data = table.bootstrapTable('getSelections');
                if(table_data.length==0) return false;

                var indexp = Layer.confirm("确定修改机器设备<form action='' method='post'>"+select_html+"</form>", {
                    title: '修改机器设备',
                    btn: ["确定", "取消"],
                    yes: function (index, layero) {
                        //发送一个POST请求，并附带参数`name`和`age`
                        Fast.api.ajax({
                            url: "chain/sequence/sequence/saveMachine", 
                            data: {"sdNum":Config.ids, "machine":$("select[name='machine']").val(), "data":JSON.stringify(table_data)}
                        }, function(data, ret){
                            getTable();
                            //成功的回调
                            layer.close(indexp);
                        });
                    }
                })
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});