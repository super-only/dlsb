define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sequence/feed_back/index' + location.search,
                    // add_url: 'chain/sequence/feed_back/add',
                    // edit_url: 'chain/sequence/feed_back/edit',
                    // del_url: 'chain/sequence/feed_back/del',
                    // multi_url: 'chain/sequence/feed_back/multi',
                    // import_url: 'chain/sequence/feed_back/import',
                    table: 'pc_feedback',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'ID',
                sortName: 'ID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ID', title: __('Id'),operate: false},
                        {field: 'PtNum', title: '生产任务单号'},
                        {field: 'IMName', title: __('材料名称'), operate: 'LIKE'},
                        {field: 'IMStd', title: __('规格型号')},
                        {field: 'IMMaterial', title: __('材质')},
                        {field: 'PartName', title: __('件号')},
                        {field: 'SingleQTY', title: __('单根计划件数')},
                        {field: 'QTY', title: __('完成数量'),operate: false},
                        {field: 'FinishedTime', title: __('Finishedtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'pdID', title: __('Pdid'), operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});