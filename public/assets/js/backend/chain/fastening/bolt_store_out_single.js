define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/bolt_store_out_single/index' + location.search,
                    add_url: 'chain/fastening/bolt_store_out_single/add',
                    edit_url: 'chain/fastening/bolt_store_out_single/edit',
                    // del_url: 'chain/fastening/bolt_store_out_single/del',
                    // multi_url: 'chain/fastening/bolt_store_out_single/multi',
                    import_url: 'chain/fastening/bolt_store_out_single/import',
                    table: 'boltstoreoutsingle_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'Out_Num',
                sortName: 'Out_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'auditor_type', title: '审核状态', searchList:{'未审核':"未审核","已审核":"已审核"}},
                        {field: 'bso.Out_Num', title: __('Out_num'), operate: '=', visible: false},
                        {field: 'Out_Num', title: __('Out_num'), operate: false},
                        {field: 'In_Num', title: '入库单号', operate: '='},
                        {field: 'OutDate', title: '出库日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'IM_Class', title: '存货名称', operate: '='},
                        {field: 'bsos.IM_LName', title: __('Im_lname'), operate: '=', visible: false},
                        {field: 'IM_LName', title: __('Im_lname'), operate: false},
                        {field: 'IM_Spec', title: '规格', operate: '='},
                        {field: 'IM_Length', title: __('Im_length'), operate: false},
                        {field: 'IM_Measurement', title: '单位', operate: '='},
                        {field: 'OutCount', title: __('Outcount'), operate: false},
                        {field: 'OutWeight', title: __('Outweight'), operate: false, formatter:function(value){
                            return value>0?(parseFloat)(value).toFixed(2):0;
                        }},
                        {field: 'OutSort', title: __('Outsort'), operate: false},
                        {field: 'OutPlace', title: '所在仓库', operate: false},
                        {field: 'IM_Num', title: __('Im_num'), operate: false},
                        {field: 'bsos.IM_Num', title: __('Im_num'), operate: '=', visible: false},
                        {field: 'BsOut_ID', title: __('Bsout_id'), operate: false},
                        {field: 'OutNaxPrice', title: __('Outnaxprice'), operate:false, formatter:function(value){
                            return value>0?(parseFloat)(value).toFixed(4):0;
                        }},
                        {field: 'OutNaxSumPrice', title: __('Outnaxsumprice'), operate:false, formatter:function(value){
                            return value>0?(parseFloat)(value).toFixed(2):0;
                        }},
                        //select一下
                        {field: 'OutType', title: '出库类别', searchList: Config.receive_list},
                        {field: 'V_ShortName', title: '供应商简称', operate: '='},
                        {field: 'Writer', title: '制单人', operate: false},
                        {field: 'Auditor', title: '审核人', operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
            $(document).on('click', "#save", function(){
                var form = $("#add-form");
                var formData = form.serializeArray();
                $.ajax({
                    url: 'chain/fastening/bolt_store_out_single/add',
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData)},
                    success: function (ret) {
                        if(ret.code==1){
                            layer.msg(ret.msg, {
                                icon: 1,
                                time: 2000 //2秒关闭（如果不配置，默认是3秒）
                              }, function(){
                                window.location.href = "edit/ids/"+ret.data;
                            }); 
                        }else{
                            layer.msg(ret.msg, {
                                icon: 2,
                                time: 2000 //2秒关闭（如果不配置，默认是3秒）
                            }); 
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $(document).on('click', "#save", function(){
                var form = $("#edit-form");
                var formData = form.serializeArray();
                $.ajax({
                    url: 'chain/fastening/bolt_store_out_single/edit/ids/'+Config.ids,
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData)},
                    success: function (ret) {
                        if(ret.code==1){
                            layer.msg(ret.msg, {
                                icon: 1,
                                time: 2000 //2秒关闭（如果不配置，默认是3秒）
                              }, function(){
                                window.location.href = "edit/ids/"+Config.ids;
                            }); 
                        }else{
                            layer.msg(ret.msg, {
                                icon: 2,
                                time: 2000 //2秒关闭（如果不配置，默认是3秒）
                            }); 
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#offerU", function () {
                $.ajax({
                    url: 'chain/fastening/bolt_store_out_single/offerU',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {id: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        choosematerial: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/bolt_store_out_single/chooseMaterial/warehouse/'+ Config.warehouse + location.search,
                    table: 'boltstorein',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                // pk: 'SSC_Num',
                // sortName: 'SSC_Date',
                height: document.body.clientHeight-100,
                search: false,
                showToggle: false,
                showExport: false,
                onlyInfoPagination: true,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'Bsid', title: '入库ID', operate: false, visible: false},
                        {field: 'si.In_Num', title: '入库编号', operate: '=',visible: false},
                        {field: 'In_Num', title: '入库编号', operate: false},
                        {field: 'InDate', title: '入库日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'V_ShortName', title: '供应商简称', operate: '='},
                        {field: 'IM_Num', title: '存货编号', operate: false, visible: false},
                        {field: 'IM_Class', title: '存货名称', operate: '='},
                        {field: 'IM_LName', title: '级别', operate: '='},
                        {field: 'IM_Spec', title: '规格', operate: '='},
                        {field: 'IM_Length', title: '无扣', operate: '='},
                        {field: 'IM_Measurement', title: '单位', operate: '='},
                        {field: 'restcount', title: '数量', operate: false},
                        {field: 'restweight', title: '重量', operate: false, formatter:function(value){
                            return value>0?(parseFloat)(value).toFixed(2):0;
                        }},
                        {field: 'InSort', title: '类别', operate: '='},
                        {field: 'InNaxPrice', title: '不含税单价', operate: false, formatter:function(value){
                            return value>0?(parseFloat)(value).toFixed(4):0;
                        }},
                        {field: 'restsumprice', title: '不含税金额', operate: false, formatter:function(value){
                            return value>0?(parseFloat)(value).toFixed(2):0;
                        }},
                        {field: 'Memo', title: '备注', operate: 'LIKE'},
                        {field: 'Writer', title: '制单人', operate: '='},
                        {field: 'Auditor', title: '审核人', operate: false},
                        {field: 'BsMemo', title: '明细备注', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        selectproject: function() {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/bolt_store_out_single/selectProject' + location.search,
                    table: 'task',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'T_Num',
                sortName: 'T_WriterDate',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 't.C_Num', title: '合同号', operate: 'LIKE',visible:false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 't.T_Num', title: '任务单号', operate: 'LIKE',visible:false},
                        // {field: 'T_Sort', title: '', operate: false},
                        // {field: 'T_Company', title: __('T_Company'), operate: false},
                        {field: 'T_WriterDate', title: '制单时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 't_project', title: '工程名称', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                Fast.api.close(tableContent);
                
            });
        },
        checkproject: function() {
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                searchFormVisible: true,
                columns: [
                    [
                        {field: 'BD_MaterialName', title: '名称', sortable: true, order:'asc'},
                        {field: 'BD_Limber', title: '等级', sortable: true, order:'asc'},
                        {field: 'BD_Type', title: '规格', sortable: true, order:'asc'},
                        {field: 'BD_Lenth', title: '无扣长', sortable: true, order:'asc'},
                        {field: 'BD_SumCount', title: '数量', sortable: true, order:'asc'}
                    ]
                ]
            });

        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                $('#show').on('keyup','td',function (e) {
                    if(e.keyCode ==13){
                        var col = $(this).parents('tr').index();
                        var row = $(this).parents("tr").find("td").index($(this));
                        var allCol = $("#tbshow > tr").length;
                        if((col+1)<=allCol){
                            $("#tbshow").find("tr").eq(col+1).find('td').eq(row).find('input').select();
                        }
                    }
                })
                $(document).on('click', "#llr", function(e){
                    var url = "chain/sale/project_cate_log/selectSaleMan";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#c-E_Name").val(value.E_Name);
                        }
                    };
                    Fast.api.open(url,"选择领料人",options);
                })
                $(document).on('click', "#chooseMaterial", function(e){
                    var warehouse = $("#OutPlace").val();
                    if(warehouse == false){
                        layer.msg("请选择仓库！");
                        return false;
                    }
                    var url = "chain/fastening/bolt_store_out_single/chooseMaterial/warehouse/"+ warehouse;
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var tableField = Config.tableField;
                            var content = "";
                            var col = $("#tbshow>tr").length;
                            $.each(value,function(v_index,v_e){
                                col = col+1;
                                content += '<tr><td>'+col+'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                $.each(tableField,function(t_index,e){
                                    content += '<td '+e[4]+'><input type="text" '+e[2]+(e[5]=="save"?' name="table_row['+e[1]+'][]"':"")+' value="'+(typeof(v_e[e[6]]) == 'undefined'?e[3]:v_e[e[6]])+'"></td>';
                                });
                                content += "</tr>";
                            });
                            $("#tbshow").append(content);
                        }
                    };
                    Fast.api.open(url,"选择入库明细",options);
                })
                $('#tbshow').on('keyup','td input[name="table_row[OutCount][]"]',function () {
                    var weight = 0;
                    var dz = $(this).parents('tr').find("input[name='table_row[dz][]']").val().trim();
                    var number = $(this).parents('tr').find("input[name='table_row[OutCount][]']").val().trim();
                    var price = $(this).parents('tr').find("input[name='table_row[OutNaxPrice][]']").val().trim();
                    number = number==''?0:parseInt(number);
                    dz = dz==''?0:parseFloat(dz);
                    price = price==''?0:parseFloat(price);
                    weight = (number*dz).toFixed(2);
                    sumprice = (number*price).toFixed(2);
                    $(this).parents('tr').find("input[name='table_row[OutWeight][]']").val(weight);
                    $(this).parents('tr').find("input[name='table_row[OutNaxSumPrice][]']").val(sumprice);
                })

                $(document).on("click", "#author", function () {
                    var num = $("#c-Out_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到出库单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/fastening/bolt_store_out_single/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#c-Out_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到出库单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/fastening/bolt_store_out_single/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }

                $(document).on('click', "#allDel", function(env){
                    var num = $("#c-Out_Num").val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/fastening/bolt_store_out_single/allDel',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        parent.location.reload();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        layer.msg("删除失败");
                    }
                })

                $(document).on('click', "#chooseProject", function(env){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            // console.log(value);
                            if(value.length == 0) layer.msg("请重试！");
                            else{
                                var project = value[0];
                                $("#c-T_Num").val(project.T_Num);
                                $("#c-project").val(project.t_project);
                            }
                        }
                    };
                    Fast.api.open('chain/fastening/bolt_store_out_single/selectProject',"工程选择",options);
                })

                $(document).on('click', "#checkproject", function(env){
                    var t_num = $("#c-T_Num").val();
                    if(t_num){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"]
                        };
                        Fast.api.open('chain/fastening/bolt_store_out_single/checkProject/ids/'+t_num,"核对工程螺栓",options);
                    }else{
                        layer.msg("请先选择工程！");
                    }
                    
                })

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[BsOut_ID][]"]').val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/fastening/bolt_store_out_single/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                        reSort();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                        reSort();
                    }
                    
                });

                function reSort()
                {
                    var col = 0;
                    $("#tbshow").find("tr").each(function () {
                        col++;
                        $(this).children('td').eq(0).text(col);
                    });
                }
            }
        }
    };
    return Controller;
});