define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'jgjyjdata'], function ($, undefined, Backend, Table, Form) {
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/bolt_month_end/index' + location.search,
                    add_url: 'chain/fastening/bolt_month_end/add',
                    edit_url: 'chain/fastening/bolt_month_end/edit',
                    del_url: 'chain/fastening/bolt_month_end/del',
                    // multi_url: 'chain/fastening/bolt_month_end/multi',
                    import_url: 'chain/fastening/bolt_month_end/import',
                    table: 'boltmonthend',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'YME_Num',
                sortName: 'YME_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'YME_Num', title: __('Yme_num'), operate: '='},
                        {field: 'YME_Year', title: __('Yme_year'), operate: '='},
                        {field: 'YME_Month', title: __('Yme_month'), operate: '='},
                        {field: 'YME_DateFrom', title: __('Yme_datefrom'), operate:'=', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD"},
                        {field: 'YME_DateTo', title: __('Yme_dateto'), operate:'=', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD"},
                        {field: 'Writer', title: __('Writer'), operate: '='},
                        {field: 'WriterDate', title: __('Writerdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Auditor', title: __('Auditor'), operate: false},
                        {field: 'AuditorDate', title: __('Auditordate'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $("#print").click(function () {
                window.top.Fast.api.open('chain/fastening/bolt_month_end/print/ids/' + Config.ids, '紧固件月结', {
                    area: ["100%", "100%"]
                });
            });
            $(document).on("click", "#export", function () {
                window.open('/admin.php/chain/fastening/bolt_month_end/export/ids/'+Config.ids);
            });
        },
        print: function() {
            // console.log('limberList',Config.limberList);
            console.log('list',Config.list);
            
            let list = Config.list;
            let mainInfos = Config.row;
            console.log('mainInfos', mainInfos)
            tb_tmp = list.map((e) => {
                let tmp = {
                    mc: e['IM_Class'],
                    gg: e['IM_Spec'],
                    dj: e['IM_LName'],
                    dw: e['IM_Measurement'],
                    sysl: e['LastCount'],
                    syje: e['LastMoney'],
                    bysl: e['InCount'],
                    byje: e['InMoney'],
                    bqsl: e['OutCount'],
                    bqje: e['OutMoney'],
                    ymsl: e['FinalCount'],
                    ymje: e['AvgMoney'],
                }
                return tmp;
            });
            let data_tmp={
                zbr:mainInfos['Writer'],
                zbrq: mainInfos['WriterDate'],
                shr: mainInfos['Auditor'],
                shrq: mainInfos['AuditorDate'],
                jcrq: mainInfos['YME_DateTo'],
                ck: list.length>0?list[0]['BSS_Place']: '',
                dyrq:mainInfos['PrintDate'],
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: jgjyjdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                var time_range = Config.time_range;
                var table = $("#table");
                // 初始化表格
                table.bootstrapTable({
                    data: Config.list,
                    height:600,
                    columns: [
                        [
                            {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                            {field: 'V_ShortName', title: '供应商'},
                            {field: 'IM_Class', title: '存货名称'},
                            {field: 'IM_LName', title: '级别'},
                            {field: 'IM_Spec', title: '规格'},
                            {field: 'IM_Length', title: '无扣'},
                            {field: 'IM_Measurement', title: '单位'},
                            {field: 'BSS_Sort', title: '类别'},
                            {field: 'LastCount', title: '期初数量'},
                            {field: 'LastMoney', title: '期初金额', formatter: function(value,row,index){return (parseFloat(value).toFixed(2));}},
                            {field: 'InCount', title: '本月收进数量'},
                            {field: 'InMoney', title: '本月收进金额', formatter: function(value,row,index){return (parseFloat(value).toFixed(2));}},
                            {field: 'OutCount', title: '本月付出数量'},
                            {field: 'OutMoney', title: '本月付出金额', formatter: function(value,row,index){return (parseFloat(value).toFixed(2));}},
                            {field: 'FinalCount', title: '期末数量'},
                            {field: 'AvgMoney', title: '期末金额', formatter: function(value,row,index){return (parseFloat(value).toFixed(2));}},
                            {field: 'BSS_Place', title: '所在仓库'},
                        ]
                    ]
                });

                $(document).on('change', "input[name='row[YME_Year]']", function(e){
                    var year = $(this).val();
                    var month = $("select[name='row[YME_Month]']").val();
                    var start_time = year+time_range[month]["month_start_day"].substr(4);
                    var end_time = year+time_range[month]["month_end_day"].substr(4);
                    $("input[name='row[YME_DateFrom]']").val(start_time);
                    $("input[name='row[YME_DateTo]']").val(end_time);
                })

                $(document).on('change', "select[name='row[YME_Month]']", function(e){
                    var year = $("input[name='row[YME_Year]']").val();
                    var month = $("select[name='row[YME_Month]']").val();
                    var start_time = year+time_range[month]["month_start_day"].substr(4);
                    var end_time = year+time_range[month]["month_end_day"].substr(4);
                    $("input[name='row[YME_DateFrom]']").val(start_time);
                    $("input[name='row[YME_DateTo]']").val(end_time);
                })

                $(document).on("click", "#summary", function () {
                    var YME_DateFrom = $("#c-YME_DateFrom").val();
                    var YME_DateTo = $("#c-YME_DateTo").val();
                    var row = {};
                    row.YME_DateFrom = YME_DateFrom;
                    row.YME_DateTo = YME_DateTo;
                    $.ajax({
                        url: 'chain/fastening/bolt_month_end/summary',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {row:JSON.stringify(row)},
                        success: function (ret) {
                            if(ret.code==1){
                                $("#table").bootstrapTable('load',ret.data);
                            }else layer.msg(ret.msg);
                            
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });

                $(document).on("click", "#author", function () {
                    var num = $("#c-YME_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到月结单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/fastening/bolt_month_end/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#c-YME_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到月结单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/fastening/bolt_month_end/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
            }
        }
    };
    return Controller;
});