define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'jgjrkdata'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/bolt_store_in_single/index' + location.search,
                    add_url: 'chain/fastening/bolt_store_in_single/add',
                    edit_url: 'chain/fastening/bolt_store_in_single/edit',
                    del_url: 'chain/fastening/bolt_store_in_single/del',
                    multi_url: 'chain/fastening/bolt_store_in_single/multi',
                    import_url: 'chain/fastening/bolt_store_in_single/import',
                    table: 'boltstoreinsingle_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'In_Num',
                sortName: 'In_Num',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'auditor_type', title: '审核状态', searchList:{'未审核':"未审核","已审核":"已审核"}},
                        {field: 'In_Num', title: __('In_num'), operate: false},
                        {field: 'bsi.In_Num', title: __('In_num'), operate: '=', visible: false},
                        {field: 'InDate', title: '入库日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'V_ShortName', title: '供应商简称', operate: '='},
                        {field: 'IM_Class', title: '存货名称', operate: '='},
                        {field: 'IM_LName', title: __('Im_lname'), operate: '='},
                        {field: 'IM_Spec', title: '规格', operate: '='},
                        {field: 'IM_Length', title: '无扣长', operate: false},
                        {field: 'bsis.IM_Length', title: '无扣长', operate: '=', visible: false},
                        {field: 'IM_Measurement', title: '单位', operate: '='},
                        {field: 'InCount', title: __('Incount'), operate: false},
                        {field: 'InWeight', title: __('Inweight'), operate: false, formatter:function(value){
                            return value>0?(parseFloat)(value).toFixed(2):0;
                        }},
                        {field: 'InSort', title: __('Insort'), operate: '='},
                        {field: 'InNaxPrice', title: __('Innaxprice'), operate:false, formatter:function(value){
                            return value>0?(parseFloat)(value).toFixed(4):0;
                        }},
                        {field: 'InNaxSumPrice', title: __('Innaxsumprice'), operate:false, formatter:function(value){
                            return value>0?(parseFloat)(value).toFixed(2):0;
                        }},
                        {field: 'Memo', title: __('Bsmemo'), operate: 'LIKE'},
                        {field: 'im.IM_Num', title: '存货编号', operate: "=", visible: false},
                        {field: 'IM_Num', title: '存货编号', operate: false},
                        {field: 'Writer', title: '制单人', operate: false},
                        {field: 'WriterDate', title: '制单时间', operate: false},
                        {field: 'Writer', title: '制单人', operate: false},
                        {field: 'WriterDate', title: '制单时间', operate: false},
                        {field: 'Auditor', title: '审核人', operate: false},
                        {field: 'AuditorDate', title: '审核时间', operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
            $(document).on('click', "#save", function(){
                var form = $("#add-form");
                var formData = form.serializeArray();
                $.ajax({
                    url: 'chain/fastening/bolt_store_in_single/add',
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData)},
                    success: function (ret) {
                        if(ret.code==1){
                            layer.msg(ret.msg, {
                                icon: 1,
                                time: 2000 //2秒关闭（如果不配置，默认是3秒）
                              }, function(){
                                window.location.href = "edit/ids/"+ret.data;
                            }); 
                        }else{
                            layer.msg(ret.msg, {
                                icon: 2,
                                time: 2000 //2秒关闭（如果不配置，默认是3秒）
                            }); 
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $("#print").click(function () {
                window.top.Fast.api.open('chain/fastening/bolt_store_in_single/print/ids/' + Config.ids, '紧固件入库', {
                    area: ["100%", "100%"]
                });
            });
            $(document).on('click', "#save", function(){
                console.log(Config.ids);
                var form = $("#edit-form");
                var formData = form.serializeArray();
                $.ajax({
                    url: 'chain/fastening/bolt_store_in_single/edit/ids/'+Config.ids,
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(formData)},
                    success: function (ret) {
                        if(ret.code==1){
                            layer.msg(ret.msg, {
                                icon: 1,
                                time: 2000 //2秒关闭（如果不配置，默认是3秒）
                              }, function(){
                                window.location.href = "edit/ids/"+Config.ids;
                            }); 
                        }else{
                            layer.msg(ret.msg, {
                                icon: 2,
                                time: 2000 //2秒关闭（如果不配置，默认是3秒）
                            }); 
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#offerU", function () {
                $.ajax({
                    url: 'chain/fastening/bolt_store_in_single/offerU',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {id: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        print: function() {
            console.log('紧固件入库');
            console.log('detail_list',Config.list);
            console.log('mainInfos',Config.row);
            
            let list = Config.list;
            let mainInfos = Config.row;
            let tb_tmp = [];
            let countSum = 0;
            let naxSum = 0;
            // 处理数据
            tb_tmp = list.map(p=>{
                naxSum+=p['InNaxSumPrice']*100;
                countSum+=p['InCount'];
                return {
                   
                    cpmc: p['IM_Class'],
                    jb: p['IM_LName'],
                    gg: p['IM_Spec'],
                    dw: p['IM_Measurement'],
                    sl: p['InCount'],
                    bhsdj:p['InNaxPrice'],
                    bhsje:p['InNaxSumPrice']
                }
            })

            
            tb_tmp.push({
                cpmc: '总计',
                jb: '',
                gg: '',
                dw: '',
                sl: countSum,
                bhsdj:'',
                bhsje:naxSum/100
            });
            console.log('tb_tmp', tb_tmp)

            rklblist = {
                '11':'正常入库'
            }
            let data_tmp={
                gfdw:mainInfos['V_Name'],
                rkdh:mainInfos['In_Num'],
                szck: mainInfos['InPlace'],
                rkrq: mainInfos['InDate'],
                rklb: rklblist[mainInfos['RSC_ID']],
                gys: mainInfos['V_Name'],
                gcmc:mainInfos['Memo'],
                zd:mainInfos['Writer'],
                sh:mainInfos['Auditor'],
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: jgjrkdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        selectarrival: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/bolt_store_in_single/selectArrival' + location.search,
                    table: 'materialgetnotice_jgj',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'MGN_ID',
                sortName: 'MGN_ID2',
                sortOrder: 'DESC',
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'MN_Date', title: '到货日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'V_Name', title: '供应商', operate:'LIKE'},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'IM_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'L_Name', title: '材质', operate: false},
                        {field: 'IM_Measurement', title: '单位', operate: false},
                        {field: 'MGN_Length', title: '无扣长', operate: false},
                        {field: 'MGN_Count', title: '数量', operate: false},
                        {field: 'MGN_Weight', title: '重量(kg)', operate: false},
                        {field: 'InCount', title: '剩余数量', operate: false},
                        {field: 'InWeight', title: '剩余重量', operate: false},
                        {field: 'MN_Num', title: '供通号', operate: false},
                        {field: 'MGN_ID', title: "MGN_ID", operate: false, visible: false},
                        {field: 'MGN_ID2', title: "MGN_ID2", operate: false, visible: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                $('#show').on('keyup','td',function (e) {
                    if(e.keyCode ==13){
                        var col = $(this).parents('tr').index();
                        var row = $(this).parents("tr").find("td").index($(this));
                        var allCol = $("#tbshow > tr").length;
                        if((col+1)<=allCol){
                            $("#tbshow").find("tr").eq(col+1).find('td').eq(row).find('input').select();
                        }
                    }
                })
                // $(document).on('click', "#chooseMaterial", function(e){
                //     var options = {
                //         shadeClose: false,
                //         shade: [0.3, '#393D49'],
                //         area: ["100%","100%"],
                //         callback:function(value){
                //             var tableField = Config.tableField;
                //             var content = "";
                //             var col = $("#tbshow>tr").length;
                //             $.each(value,function(v_index,v_e){
                //                 col = col+1;
                //                 content += '<tr><td>'+col+'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                //                 $.each(tableField,function(t_index,e){
                //                     content += '<td '+e[4]+'><input type="text" '+e[2]+(e[5]=="save"?' name="table_row['+e[1]+'][]"':"")+' value="'+(typeof(v_e[e[1]]) == 'undefined'?e[3]:v_e[e[1]])+'"></td>';
                //                 });
                //                 content += "</tr>";
                //             });
                //             $("#tbshow").append(content);
                //         }
                //     };
                //     // Fast.api.open('chain/fastening/materialnote_jgj/chooseMaterial/v_num/'+v_num,"选择材料",options);
                //     Fast.api.open('chain/purchase/requisition/chooseMaterial/type/2',"选择材料",options);
                // })
                
                $(document).on('click', "#chooseArrival", function(env){
                    var ware = $("select[name='row[SI_WareHouse]'] option:selected").text();
                    if(ware!="请选择"){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                var tableField = Config.tableField;
                                var content = "";
                                var col = $("#tbshow>tr").length;
                                $.each(value,function(v_index,v_e){
                                    col = col+1;
                                    content += '<tr><td>'+col+'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                    $.each(tableField,function(t_index,e){
                                        content += '<td '+e[4]+'><input type="text" '+e[2]+(e[5]=="save"?' name="table_row['+e[1]+'][]"':"")+' value="'+(typeof(v_e[e[1]]) == 'undefined'?e[3]:v_e[e[1]])+'"></td>';
                                    });
                                    content += "</tr>";
                                });
                                $("#tbshow").append(content);
                            }
                        };
                        Fast.api.open('chain/fastening/bolt_store_in_single/selectArrival',"到货选择",options);
                    }else layer.msg("必须选择所在仓库！");
                    
                })
                $(document).on('click', '.gysxz', function (e){
                    var content = $(this);
                    var url = "chain/material/material_note/chooseVendor";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            if(value.length!=0){
                                $("#c-V_Num").val(value.V_Num);
                                $("#c-V_ShortName").val(value.V_ShortName);
                            }
                        }
                    };
                    Fast.api.open(url,"选择单位名称",options);
                })

                $(document).on("click", "#author", function () {
                    check('审核之后无法修改，确定审核？',"chain/fastening/bolt_store_in_single/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    check('确定弃审？',"chain/fastening/bolt_store_in_single/giveUp",Config.ids);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }

                $(document).on('click', "#allDel", function(env){
                    var num = $("#c-In_Num").val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/fastening/bolt_store_in_single/allDel',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        parent.location.reload();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        layer.msg("删除失败");
                    }
                })

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[Bsid][]"]').val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/fastening/bolt_store_in_single/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                        reSort();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                        reSort();
                    }
                    
                });

                $('#tbshow').on('keyup','td input[name="table_row[InCount][]"],td input[name="table_row[InWeight][]"],td input[name="table_row[InNaxPrice][]"]',function () {
                    let way = $(this).parents('tr').find('td input[name="table_row[way][]"]').val();
                    way = way==''?0:parseFloat(way);
                    let count = 0;
                    if(way) count = $(this).parents('tr').find('td input[name="table_row[InWeight][]"]').val();
                    else count = $(this).parents('tr').find('td input[name="table_row[InCount][]"]').val();
                    count = count==''?0:parseFloat(count);
                    let price = $(this).parents('tr').find('td input[name="table_row[InNaxPrice][]"]').val();
                    price = price==''?0:parseFloat(price);
                    let sumPrice = count*price;
                    $(this).parents('tr').find('td input[name="table_row[InNaxSumPrice][]"]').val(sumPrice.toFixed(5));
                    console.log(way,count,price,sumPrice);
                })

                function reSort()
                {
                    var col = 0;
                    $("#tbshow").find("tr").each(function () {
                        col++;
                        $(this).children('td').eq(0).text(col);
                    });
                }
            }
        }
    };
    return Controller;
});