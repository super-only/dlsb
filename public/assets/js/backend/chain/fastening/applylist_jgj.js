define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree', 'hpbundle', 'qgddata','ui'], function ($, undefined, Backend, Table, Form, undefined) {

    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: [],
                height: 250,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'AD_ID', title: '请购明细ID'},
                        {field: 'AL_Num', title: '请购单号'},
                        {field: 'IM_Class', title: '材料名称'},
                        {field: 'IM_Spec', title: '规格'},
                        {field: 'L_Name', title: '材质'},
                        {field: 'AD_Length', title: '无扣长'},
                        {field: 'AD_BCount', title: '单基数量'},
                        {field: 'AD_SCount', title: '损耗数量'},
                        {field: 'AD_Count', title: '总数'},
                        {field: 'AD_Weight', title: '重量(kg)'},
                        {field: 'AD_Memo', title: '备注'}
                    ]
                ]
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/applylist_jgj/index' + location.search,
                    add_url: 'chain/fastening/applylist_jgj/add',
                    edit_url: 'chain/fastening/applylist_jgj/edit',
                    del_url: 'chain/fastening/applylist_jgj/del',
                    auditor_url: 'chain/fastening/applylist_jgj/auditor',
                    giveup_url: 'chain/fastening/applylist_jgj/giveUp',
                    multi_url: 'chain/fastening/applylist_jgj/multi',
                    import_url: 'chain/fastening/applylist_jgj/import',
                    table: 'applylist_jgj',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'AL_ID',
                sortName: 'AL_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                // showExport: false,
                height: 500,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'AL_ID', title: __('Al_id'),visible:false,operate: false},
                        {field: 'AL_Num', title: __('Al_num'), operate: 'LIKE'},
                        {field: 'AL_PurchaseType', title: __('Al_purchasetype'), searchList: Config.purchaseList},
                        {field: 'AL_ArriveDate', title: __('Al_arrivedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'DD_Num', title: __('Dd_num'), searchList: Config.deptList},
                        {field: 'AL_ApplyPepo', title: __('Al_applypepo'), operate: 'LIKE'},
                        {field: 'AL_WriteDate', title: __('Al_writedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'AL_ProjectName', title: '工程名称', operate: 'LIKE'},
                        {field: 'al_c_num', title: '合同号', operate: 'LIKE'},
                        {field: 'al_t_num', title: '任务单号', operate: 'LIKE'},
                        // {field: 'scd_id', title: '杆塔号', operate: false},
                        {field: 'scd_count', title: '基数', operate: false},
                        {field: 'AL_Writer', title: __('Al_writer'), operate: 'LIKE'},
                        {field: 'AL_WriterDate', title: __('Al_writerdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Modifyer', title: '审核人', operate: 'LIKE'},
                        {field: 'ModifyTime', title: '审核时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var AL_Num = $element.AL_Num;
                if(AL_Num == '') return false;
                $.ajax({
                    url: 'chain/fastening/applylist_jgj/requisitionDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:AL_Num},
                    success: function (ret) {
                        var content = [];
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $("#printDetail").click(function () {
                window.top.Fast.api.open('chain/fastening/applylist_jgj/printDetail/ids/' + Config.ids, '原材料请购单', {
                    area: ["100%", "100%"]
                });
            });
            
            $(document).on("click", "#export", function () {
                window.open('/admin.php/chain/fastening/applylist_jgj/export/ids/' + Config.ids);
            });

            $(document).on("click", "#importDetail", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["60%","60%"],
                    callback: function(value){
                        if(value) $("#tbshow").append(value);
                    }
                };
                Fast.api.open('chain/fastening/applylist_jgj/importDetail/ids/' + Config.ids,"导入材料",options);
            });
            $(document).on("click", "#selectAccpect", function () {
                var url = "chain/purchase/requisition/selectOperator";
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        $("input[name='row[al_reciever]']").val(value[0].id);
                        $("#al_reciever_name").val(value[0].username);
                    }
                };
                Fast.api.open(url,"接收人选择",options);
            });

            $(document).on("click", "#author", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/fastening/applylist_jgj/auditor",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('确定弃审？',"chain/fastening/applylist_jgj/giveUp",num);
                
            });
            function check(msg,url,num){
                // console.log(url,msg,num);return false;
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {ids: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                window.location.reload();
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        importdetail: function () {
            Form.api.bindevent($("form[role=form]"),function(data,ret){
                if(ret.code == 1) Fast.api.close(ret.data);
            });
        },
        printdetail: function() {
            // console.log('limberList',Config.limberList);
            console.log('list',Config.list);
            
            let list = Config.list;
            let mainInfos = Config.mainInfos;
            let tb_tmp = [];
            let weightSum = 0;

            for(let index=0;index<list.length;index++){
                
                let tmp={
                    'clmc':list[index]['IM_Class'], //材料名称
                    'cz':list[index]['L_Name'],//材质
                    'gg':list[index]['IM_Spec'],//规格
                    'cd':Number(list[index]['AD_Length']).toFixed(1),//长度(m)
                    'sl':list[index]['AD_Count'],//数量
                    'zl':Number(list[index]['AD_Weight']).toFixed(2),//重量（kg）
                    'ljj':'',//理计价
                    'gbj':'',//过磅价
                    'bz':list[index]['AD_Memo'],//备注
                };
                weightSum+=Math.floor(list[index]['AD_Weight']*100)/100;
                
                tb_tmp.push(tmp);    
            }

            // 添加合计行
            tb_tmp.push({
                'clmc':'合计', //材料名称
                'cz':'',
                'gg':'',
                'cd':'',
                'sl':'',
                'zl':weightSum.toFixed(2),
                'ljj':'',//理计价
                'gbj':'',//过磅价
                'bz':'',
            });
            tb_tmp.push({
                'clmc':'回传：', //材料名称
                'cz':'',
                'gg':'',
                'cd':'',
                'sl':'',
                'zl':'',
                'ljj':'供方确认',//理计价
                'gbj':'（盖章）：',//过磅价
                'bz':'',
            })
            tb_tmp.push({
                'clmc':'', //材料名称
                'cz':'',
                'gg':'',
                'cd':'',
                'sl':'',
                'zl':'',
                'ljj':'',//理计价
                'gbj':'确认人：',//过磅价
                'bz':'',
            })
            tb_tmp.push({
                'clmc':'电话：', //材料名称
                'cz':'',
                'gg':'',
                'cd':'',
                'sl':'',
                'zl':'',
                'ljj':'',//理计价
                'gbj':'确认日期：',//过磅价
                'bz':'',
            })
            tb_tmp.push({
                'clmc':'报价说明：', //材料名称
                'cz':'',
                'gg':'',
                'cd':'',
                'sl':'',
                'zl':'',
                'ljj':'',//理计价
                'gbj':'',//过磅价
                'bz':'',
            })
            tb_tmp.push({
                'clmc':'以上报价为：', //材料名称
                'cz':'自提价 □',
                'gg':'到厂价 □',
                'cd':'',
                'sl':'',
                'zl':'',
                'ljj':'',//理计价
                'gbj':'',//过磅价
                'bz':'',
            })
            tb_tmp.push({
                'clmc':'采购说明：', 
                'cz':'',
                'gg':'',
                'cd':'',
                'sl':'',
                'zl':'',
                'ljj':'',//理计价
                'gbj':'',//过磅价
                'bz':'',
            })
            tb_tmp.push({
                'clmc':'主管签字：', 
                'cz':'',
                'gg':'',
                'cd':'',
                'sl':'',
                'zl':'',
                'ljj':'',//理计价
                'gbj':'',//过磅价
                'bz':'',
            })
            tb_tmp.push({
                'clmc':'制单人：', 
                'cz':'',
                'gg':'',
                'cd':'',
                'sl':'',
                'zl':'',
                'ljj':'',//理计价
                'gbj':'',//过磅价
                'bz':'',
            })
            


            let data_tmp={
                bh:mainInfos['AL_Num'],
                rq:mainInfos['time'],
                tb:tb_tmp
            };
            

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: qgddata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        choosematerial: function () {
            var height = document.body.clientHeight/2;
            $("#chooseTable").bootstrapTable({
                data: [],
                height: height,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'operate', title: '删除',
                            formatter: function(value,row,index){return '<button class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></button>';}
                        },
                        {field: 'IM_Num', title: '存货编码', operate: 'LIKE'},
                        {field: 'type', title: '类别',operate: false},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'IM_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'IM_PerWeight', title: '比重', operate:'BETWEEN'},
                        {field: 'IM_Sign', title: '符号', operate: 'LIKE'},
                        {field: 'IM_Measurement', title: '计量单位', operate: 'LIKE'},
                        {field: 'IM_Min', title: '最低库存',operate: false},
                        {field: 'IM_Max', title: '最高库存',operate: false},
                        {field: 'price', title: '计划单位(元/吨)',operate: false},
                        {field: 'IM_BD_Other', title: '另付规格', operate: 'LIKE'},
                        {field: 'IM_Length', title: '无长扣', operate: 'LIKE'},
                        {field: 'IM_Rax', title: '税率',operate: false},
                        {field: 'IM_Day', title: '库龄(天)',operate: false}
                    ]
                ]
            });

            $(document).on('click', ".del", function(e){
                var left_height = $("#chooseTable").bootstrapTable('getScrollPosition');
                var this_index = $(this).parents("tr").index();
                var search_content = $("#chooseTable").bootstrapTable('getData');
                $.each(search_content,function(e,index){
                    if(e==this_index) search_content.splice(e,1);
                })
                $("#chooseTable").bootstrapTable('load',search_content);
                $("#chooseTable").bootstrapTable('scrollTo', search_content);
            });
            Table.api.bindevent($("#chooseTable"));
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/ch/inventory_material/companytree/type/'+Config.type,
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/applylist_jgj/chooseMaterial/type/'+Config.type + location.search,
                    table: 'InventoryMaterial',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'ID',
                sortName: 'IM_Num',
                sortOrder: 'ASC',
                search: false,
                showToggle: false,
                showExport: false,
                height:height,
                pagination: false,
                clickToSelect: Config.type==2?false:true,
                singleSelect: Config.type==2?false:true,
                limit: 10000,
                columns: [
                    [
                        {checkbox: true, field: 'check'},
                        {field: 'id', title: '序号', operate: false, visible: false},
                        {field: 'IM_Num', title: '存货编码', operate: 'LIKE'},
                        {field: 'type', title: '类别',operate: false},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'IM_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'IM_PerWeight', title: '比重', operate:'BETWEEN'},
                        {field: 'IM_Sign', title: '符号', operate: 'LIKE'},
                        {field: 'IM_Measurement', title: '计量单位', operate: 'LIKE'},
                        {field: 'IM_Min', title: '最低库存',operate: false},
                        {field: 'IM_Max', title: '最高库存',operate: false},
                        {field: 'price', title: '计划单位(元/吨)',operate: false},
                        {field: 'IM_BD_Other', title: '另付规格', operate: 'LIKE'},
                        {field: 'IM_Length', title: '无长扣', operate: 'LIKE'},
                        {field: 'IM_Rax', title: '税率',operate: false},
                        {field: 'IM_Day', title: '库龄(天)',operate: false}
                    ]
                ]
            });


            // 为表格绑定事件
            Table.api.bindevent(table);
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'IM_Num': dd_num}),
                        op: JSON.stringify({'IM_Num': 'LIKE %'}),
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
                
            }).jstree();

            $(document).on("click", "#choose", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else{
                    $("#chooseTable").bootstrapTable("append",tableContent);
                    var left_height = $("#chooseTable tbody").height();
                    $("#two .fixed-table-body").scrollTop(left_height)
                    // table.bootstrapTable('uncheckAll');
                }

            });
            $(document).on("click", "#sure", function () {
                var tableContent = $("#chooseTable").bootstrapTable('getData');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
            table.on('click-row.bs.table', function (row, $element) {
                if($element.check) table.bootstrapTable('uncheck', $element["id"]);
                else table.bootstrapTable('check', $element["id"]);
            });
        },
        syncrequisition: function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/applylist_jgj/syncRequisition' + location.search,
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'AD_ID',
                sortName: 'AD_ID',
                search: false,
                // clickToSelect: true,
                // singleSelect: true,
                showToggle: false,
                // showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'AL_Num', title: '请购编号', operate: false},
                        {field: 'AD_ID', title: '请购ID', operate: 'LIKE'},
                        {field: 'IM_Num', title: '材料编号', operate: false},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'IM_Spec', title: '材料规格', operate: 'LIKE'},
                        {field: 'L_Name', title: '材料材质', operate: 'LIKE'},
                        {field: 'IM_PerWeight', title: '单重', operate: false},
                        {field: 'IM_Measurement', title: '单位', operate: false},
                        {field: 'AD_Length', title: '无扣长', operate:false},
                        {field: 'AD_Count', title: '请购数量', operate: false},
                        {field: 'AD_BuyCount', title: '采购数量', operate: false},
                        {field: 'sy_count', title: '剩余未采购数量', operate: false},
                        {field: 'AD_Weight', title: '请购重量', operate: false},
                        {field: 'AD_BuyWeight', title: '采购重量', operate: false},
                        {field: 'sy_weight', title: '剩余未采购重量', operate: false},
                        {field: 'AL_Writer', title: '请购人', operate: 'LIKE'},
                        {field: 'AL_WriterDate', title: '请购时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var tableContent=table.bootstrapTable('getAllSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });

                $(document).on('click', "#chooseTower", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#AL_ProjectName").val(value.C_Project);
                            $("#al_c_num").val(value.C_Num);
                            $("#al_t_num").val(value.T_Num);
                        }
                    };
                    Fast.api.open('chain/lofting/compact_diagram_main/chooseTower/type/1',"选择工程",options);
                })

                $(document).on('click', "#chooseMaterial", function(e){
                    var AL_ID = $("#AL_ID").val();
                    if(!AL_ID){
                        layer.msg("请先保存主信息！");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $.ajax({
                                url: 'chain/fastening/applylist_jgj/addMaterial',
                                type: 'post',
                                dataType: 'json',
                                data: {data:JSON.stringify(value)},
                                success: function (ret) {
                                    var content = '';
                                    if(ret.code==1){
                                        content = ret.data;
                                    }
                                    $("#tbshow").append(content);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            })
                        }
                    };
                    Fast.api.open('chain/fastening/applylist_jgj/chooseMaterial',"选择材料",options);
                })

                $('#tbshow').on('keyup','td input[name="table_row[AD_BCount][]"]',function () {
                    get_count($(this));
                    get_weight($(this));
                })
                $('#tbshow').on('keyup','td input[name="table_row[AD_SCount][]"]',function () {
                    get_count($(this));
                    get_weight($(this));
                })

                function get_count(content){
                    var bcount = content.parents('tr').find("input[name='table_row[AD_BCount][]']").val().trim();
                    var scount = content.parents('tr').find("input[name='table_row[AD_SCount][]']").val().trim();
                    bcount = bcount==''?0:parseFloat(bcount);
                    scount = scount==''?0:parseFloat(scount);
                    var sumCount = bcount+scount;
                    content.parents('tr').find("input[name='table_row[AD_Count][]']").val(sumCount);
                }

                function get_weight(content){
                    var number = content.parents('tr').find("input[name='table_row[AD_Count][]']").val().trim();
                    var bz = content.parents('tr').find("input[name='table_row[IM_PerWeight][]']").val().trim();
                    number = number==''?0:parseInt(number);
                    bz = bz==''?0:parseFloat(bz);
                    var weight = (number*bz).toFixed(2);
                    content.parents('tr').find("input[name='table_row[AD_Weight][]']").val(weight);
                }

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(1).find('input').val();
                    if(num != false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/fastening/applylist_jgj/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                });

                var kj_limberList = Config.kj_limberList;
                $('#tbshow').on('keyup','td input[name="table_row[L_Name][]"]',function () {
                    var text_count = $(this).parents('tr').find("input[name='table_row[L_Name][]']").val().trim();
                    if(kj_limberList[text_count] != undefined ) $(this).parents('tr').find("input[name='table_row[L_Name][]']").val(kj_limberList[text_count])
                })
                
            }
        }
    };
    return Controller;
});