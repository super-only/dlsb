define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/fastening/bolt_store_stock/index' + location.search,
                    // add_url: 'chain/fastening/bolt_store_stock/add',
                    // edit_url: 'chain/fastening/bolt_store_stock/edit',
                    // del_url: 'chain/fastening/bolt_store_stock/del',
                    // multi_url: 'chain/fastening/bolt_store_stock/multi',
                    import_url: 'chain/fastening/bolt_store_stock/import',
                    table: 'boltstorestock',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'BSS_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight-100,
                searchFormVisible: true,
                columns: [
                    [
                        {field: 'V_ShortName', title: __('V_num'), operate: '='},
                        {field: 'IM_Class', title: '存货名称', operate: '='},
                        {field: 'IM_LName', title: __('Im_lname'), operate: '='},
                        {field: 'IM_Spec', title: '规格', operate: '='},
                        {field: 'IM_Length', title: __('Im_length'), operate: false},
                        {field: 'm.IM_Length', title: __('Im_length'), operate: '=', visible: false},
                        {field: 'IM_Measurement', title: '单位', operate: '='},
                        {field: 'BSS_Count', title: __('Bss_count'), operate: false},
                        {field: 'BSS_Weight', title: __('Bss_weight'), operate: false},
                        {field: 'BSS_Sort', title: __('Bss_sort'), operate: '='},
                        {field: 'BSS_NaxPrice', title: __('Bss_naxprice'), operate: false},
                        {field: 'BSS_NaxSumMoney', title: __('Bss_naxsummoney'), operate: false},
                        {field: 'BSS_Place', title: __('Bss_place'), operate: false},
                        {field: 'IM_Num', title: __('Im_num'), operate: false},
                        {field: 'm.IM_Num', title: __('Im_num'), operate: '=', visible: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});