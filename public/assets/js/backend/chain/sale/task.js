define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'scrwddata'], function ($, undefined, Backend, Table, Form) {
    showField = typeof showField=="undefined"?"":showField;
    // dealshowField = typeof dealshowField=="undefined"?"":dealshowField;
    nowtime = typeof nowtime=="undefined"?"":nowtime;
    showList = typeof showList=="undefined"?[]:showList;
    dealList = typeof dealList=="undefined"?[]:dealList;
    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: {},
                height: 250,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'T_Date', title: '交货时间'},
                        {field: 'P_Num', title: '线路名称'},
                        {field: 'P_Name', title: '产品名称'},
                        {field: 'TD_Pressure', title: '电压等级'},
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'TD_OtherTypeName', title: '打钢印'},
                        {field: 'TH_Height', title: '呼高'},
                        {field: 'state', title: '状态'},
                        {field: 'SCD_Count', title: '数量'},
                        {field: 'SCD_Weight', title: '单重'},
                        {field: 'Sum_Weight', title: '总重',formatter: function(value,row,index){return parseFloat(value).toFixed(3)}},
                        {field: 'SCD_Unit', title: '单位'},
                        {field: 'SCD_Memo', title: '备注'}
                    ]
                ]
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/task/index' + location.search,
                    add_url: 'chain/sale/task/add',
                    edit_url: 'chain/sale/task/edit',
                    derive_url: 'chain/sale/task/derive',
                    del_url: 'chain/sale/task/del',
                    multi_url: 'chain/sale/task/multi',
                    import_url: 'chain/sale/task/import',
                    table: 'task',
                }
            });

            var table = $("#table");
            $(".btn-derive").data("area",["20%","20%"]);
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'T_Num',
                sortName: 'T_WriterDate',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                // height:500,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'td.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: __('T_num'), operate: 'LIKE'},
                        {field: 'c.PC_Num', title: '工程编号', operate: 'LIKE',visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'C_Num', title: __('C_num'), operate: 'LIKE'},
                        {field: 'c.Customer_Name', title: '客户', operate: 'LIKE',visible: false},
                        {field: 'Customer_Name', title: '客户', operate: false},
                        {field: 'T_Company', title: __('T_company'),
                            searchList: {"公司": "公司", "外协": "外协", "铁塔": "铁塔"},
                            formatter: function(value) {
                                return value;
                            }
                        },
                        {field: 't_project', title: __('T_project'), operate: 'LIKE'},
                        {field: 't_shortproject', title: __('T_shortproject'), operate: 'LIKE'},
                        {field: 'T_Date', title: __('T_date'), operate:false},
                        {field: 'C_SaleType', title: '销售类型', operate: false},
                        {field: 'T_Sort', title: __('T_sort'), 
                            searchList: Config.sortList,
                            formatter: function(value) {
                                return value;
                            }
                        },
                        {field: 'T_SumWeight', title: __('T_sumweight'), operate:false, formatter: function (value,row,index){
                            return parseFloat(value).toFixed(3);
                        }},
                        {field: 'c.C_SaleMan', title: '业务员', operate: 'LIKE',visible: false},
                        {field: 'C_SaleMan', title: '业务员', operate: false},
                        {field: 'Writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'T_WriterDate', title: __('T_writerdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'Auditor', title: __('Auditor'), operate:false},
                        {field: 'T_AuditorDate', title: __('T_auditordate'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ],
                onPostBody: function ()
                {
                    var header = $(".fixed-table-header table thead tr th");
                    var body = $(".fixed-table-header table tbody tr td");
                    var footer = $(".fixed-table-header table tr td");
                    body.each(function(){
                        header.width($(this).width());
                        footer.width($(this).width());
                    })
                }
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var ids = $element.T_Num;
                if(ids == '') return false;
                $.ajax({
                    url: 'chain/sale/task/contrast',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:ids},
                    success: function (ret) {
                        var content = {};
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
        },
        add: function () {
            Controller.api.bindevent("#add-form");
        },
        edit: function () {
            Controller.api.bindevent("#edit-form");
            $(document).on("click", "#updatePurchase", function () {
                $.ajax({
                    url: 'chain/sale/task/eipUpload',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {T_Num: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#derive", function () {
                window.open('/admin.php/chain/sale/task/derive/ids/' + Config.ids);
            });
            $("#print").click(function () {
                window.top.Fast.api.open('chain/sale/task/print/t_num/' + Config.ids, '生产任务单', {
                    area: ["100%", "100%"]
                });
            });
            $(document).on("click", "#author", function () {
                var C_Num = $("input[name='row[T_Num]']").val();
                if(C_Num == false){
                    layer.confirm('请先保存！');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/sale/task/auditor",C_Num);
            });
            $(document).on("click", "#giveup", function () {
                var C_Num = $("input[name='row[T_Num]']").val();
                if(C_Num == false){
                    layer.confirm('请重试！');
                    return false;
                }
                check('确定弃审？',"chain/sale/task/giveUp",C_Num);
                
            });
            function check(msg,url,num){
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {T_Num: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                // Layer.msg(ret.msg);
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        selectcompact: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/task/selectcompact' + location.search,
                    table: 'compact',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'C_Num',
                sortName: 'C_WriteDate',
                sortOrder: 'DESC',
                clickToSelect: true,
                singleSelect: true,
                search: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 's.C_Num', title: '合同号', operate: 'LIKE',visible: false},
                        // {field: 'C_Name', title: "合同编号(外部)", operate: 'LIKE'},
                        {field: 'Customer_Name', title: '客户名称', operate: false},
                        {field: 'c.Customer_Name', title: '客户名称', operate: 'LIKE',visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'C_Project', title: '工程名称', operate: false},
                        {field: 'c.C_Project', title: '工程名称', operate: 'LIKE',visible: false},
                        {field: 'C_SortProject', title: '工程简称', operate: false},
                        {field: 'c.C_SortProject', title: '工程简称', operate: 'LIKE',visible: false},
                        {field: 'C_SaleMan', title: '业务员', operate: 'LIKE'},
                        {field: 'produce_type', title: '产品类型', searchList:Config.produce_type,visible:false},
                        {field: 'type', title: '产品类型', operate: false}
                        
                    ]
                ]
            });
            
            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var formData=table.bootstrapTable('getAllSelections');
                Fast.api.close(formData[0]);
            });
        },
        print: function() {
            // console.log('limberList',Config.limberList);
            console.log('detail_list',Config.list);
            console.log('mainInfos',Config.row);
            //console.log('lists',Config.lists);
            let stateArr = Config.list;
            let stateList = {};
            let xdStr = '';
            stateArr.forEach(e => {
                if(!Object.hasOwnProperty.call(stateList, e.state)){
                    stateList[e.state] = [];
                }
                stateList[e.state].push(e);
            });
            for (const key in stateList) {
                if (Object.hasOwnProperty.call(stateList, key)) {
                    const ele = stateList[key];
                    let countSum = 0;
                    let weightSum = 0;
                    ele.forEach(el=>{
                        countSum+=el['SCD_Count'];
                        weightSum+=el['Sum_Weight'];
                    })
                    xdStr+=key+countSum+'，共'+weightSum.toFixed(3)+'kg';
                }
                
            }

            let list = Config.list;
            let mainInfos = Config.row;
            let tb_tmp = [];
            let pageSize = 19;
            let pageCountSum = 0;
            let pageWeightSum = 0;
            let pagePriceSum = 0;
            // 处理数据
            let dataArr = list.map(p=>{
                return {
                    gth: p['SCD_TPNum'],
                    tx: p['TD_TypeName'],
                    hg: p['TH_Height'],
                    sl: p['SCD_Count'],
                    dw: p['TPN_ID'],
                    dz: Number(p['TH_Weight']).toFixed(3),
                    zl:(p['TH_Weight']*p['SCD_Count']).toFixed(3),
                    dj:(p['price']).toFixed(5),
                    zj:(p['sum_price']).toFixed(2),
                    bz:p['SCD_Memo'],
                }
            })

            // 补齐空白行 (pageSize-1)-list.length%pageSize
            for (let i = 0; i < (pageSize-1)-list.length%pageSize; i++) {
                
                dataArr.push({
                    gth: '',
                    tx: '',
                    hg: '',
                    sl: '',
                    dw: '',
                    zl: '',
                    dj: '',
                    zj: '',
                    bz: '',
                });
            }

            // 添加页小计行
            tb_tmp = dataArr.reduce((tmpArr, p, index)=>{
                p.xh = index+1
                tmpArr.push(p);
                pageCountSum+=p.sl*100;
                pageWeightSum+=Number(p.zl);
                //pagePriceSum+=p.zl*p.dj;
				pagePriceSum += (p.zj?parseFloat(p.zj):0);
                if((index+1)==dataArr.length){
                    tmpArr.push({
                        gth: '合计',
                        tx: '',
                        hg: '',
                        sl: pageCountSum/100,
                        dw: '',
                        zl: pageWeightSum.toFixed(3),
                        dj: '',
                        zj: pagePriceSum.toFixed(2),
                        bz: '',
                    });
                    
                }
                return tmpArr;
            },[]);
            
            console.log('mainInfos', mainInfos)

            let data_tmp={
                rwdh:mainInfos['T_Num'],
                xdrq: mainInfos['T_WriterDate'],
                gcmc: mainInfos['t_project'],
                khmc: mainInfos['Customer_Name'],
                sm: mainInfos['T_Memo'],
                htbh:mainInfos['C_Num'],
                gcbh:mainInfos['PC_Num'],
                jhsj:mainInfos['T_Date'],
                ywy:mainInfos['C_SaleMan'],
                zdr:mainInfos['Writer'],
                spr:mainInfos['Auditor'],
                xd:xdStr,
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let template = mainInfos['T_Sort']=="铁附件"? scrwddata2: scrwddata;
            let htemp =  new hiprint.PrintTemplate({template: template});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        api: {
            bindevent: function (field='edit') {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="#add-form"){
                        window.location.href = "edit/ids/"+data; 
                    }else{
                        window.location.reload();
                    }
                    return false;
                });
                
                //铁塔1 铁附件3 默认铁塔
                var js_way = Config.js_way;
                var dealshowField = Config.dealshowField[js_way];

                var author = $("#author").prop("disabled");
                if(author){
                    $("#tbdeal").find("tr").each(function () {
                        var checkedField = $(this).children('td').eq(0).find('input');
                        checkedField.prop("disabled",true);
                    });
                }
                $(document).on("click", "#select", function () {
                    var url = "chain/sale/task/selectCompact";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("input[name='row[C_Num]']").val(value.C_Num);
                            $("#c-C_Name").val(value.C_Name);
                            $("#c-Customer_Name").val(value.Customer_Name);
                            $("input[name='row[t_project]']").val(value.C_Project);
                            $("input[name='row[t_shortproject]']").val(value.C_SortProject);
                            $("input[name='row[T_SumWeight]']").val(value.C_Sumweight);
                            $('select[name="row[T_Sort]"]').selectpicker('val', value.type);
                            js_way = value.produce_type;
                            dealshowField = Config.dealshowField[js_way];
                            var head_field = '<tr><th width="40">操作</th>';
                            $.each(dealshowField,function(index,e){
                                head_field += '<th '+e[4]+' width="60">'+e[0]+'</th>';
                            });
                            head_field += '</tr>';
                            $("#deal thead").html(head_field);

                            $.ajax({
                                url: 'chain/sale/task/showTable',
                                type: 'post',
                                dataType: 'json',
                                data: {C_Num: value.C_Num},
                                success: function (ret) {
                                    if (ret.hasOwnProperty("code")) {
                                        if (ret.code === 1) {
                                            var addHtml = "";
                                            for(key in ret.data){
                                                for(var i=0;i<showField.length;i++){
                                                    var field = showField[i][1];
                                                    var hidden = showField[i][2];
                                                    if(hidden) addHtml += '<tr><td><input class="small_input" type="checkbox" name="tableShow[check][]" value="'+ret.data[key][field]+'"></td><td '+hidden+'><input type="text" name="tableShow['+field+'][]" value="'+ret.data[key][field]+'"></td>'
                                                    else addHtml += '<td>'+(typeof ret.data[key][field]!="undefined"?ret.data[key][field]:"")+'</td>';
                                                }
                                                addHtml += '</tr>';
                                            }
                                            $("#tbshow").html(addHtml);
                                            $("#tbdeal").html("");
                                            showList = ret.showList;
                                        }else if(ret.code === 2){
                                            window.location.href = 'edit/ids/'+ret.data;
                                        }else{
                                            Layer.msg(ret.msg);
                                        }
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }
                    };
                    Fast.api.open(url,"选择合同",options);
                });
                $(document).on('click', ".table_del", function(e){
                    var target = $( e.target ).closest("tr");
                    var index_num = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(index_num).find('td input[name="table_row[SCD_ID][]"]').val().trim();
                    var td = $(this).parents('tbody').find('tr').eq(index_num).find('td input[name="table_row[TD_ID][]"]').val().trim();
                    var th = $(this).parents('tbody').find('tr').eq(index_num).find('td input[name="table_row[TH_ID][]"]').val().trim();
                    var showfind = $.inArray(parseInt(num),showList);
                    if(th){
                        $.ajax({
                            url: 'chain/sale/task/delSectTask',
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            data: {num: td},
                            success: function (ret) {
                                if(ret.code == 0){
                                    Layer.confirm("已放样，仍强制删除？", function (index) {
                                        $.ajax({
                                            url: 'chain/sale/task/delPretend',
                                            type: 'post',
                                            dataType: 'json',
                                            async: false,
                                            data: {num: num},
                                            success: function (ret) {
                                                layer.msg(ret.msg);
                                                if(ret.code == 1){
                                                    // target.find('td input[name="table_row[SCD_Count][]"]').val(0);
                                                    target.find('td input[name="table_row[SCD_Count][]"]').attr("value",0);
                                                    // target.find('td input[name="table_row[task_count][]"]').val(0);
                                                    target.find('td input[name="table_row[task_count][]"]').attr("value",0);
                                                    target.hide();
                                                }
                                                // if(ret.code == 0) flag = false;
                                            }, error: function (e) {
                                                Backend.api.toastr.error(e.message);
                                            }
                                        });
                                    });
                                }else{
                                    layer.msg(ret.msg);
                                    if(showfind==-1){
                                        showList = showfindajax(showList,num);
                                    }else{
                                        delchecked(num);
                                    }
                                    target.remove();
                                    dealList = $.grep(dealList, function( n ) {
                                        return ( n != num );
                                    });
                                }
                                // if(ret.code == 0) flag = false;
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }else{
                        if(showfind==-1){
                            showList = showfindajax(showList,num);
                        }else{
                            delchecked(num);
                        }
                        target.remove();
                        dealList = $.grep(dealList, function( n ) {
                            return ( n != num );
                        });
                    }
                    $("#c-T_SumWeight").val(sumWeightjs());
                });

                function showfindajax(showList,num)
                {
                    $.ajax({
                        url: 'chain/sale/task/addShowTable',
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                if (ret.code === 1) {
                                    var addHtml = "";
                                    for(key in ret.data){
                                        for(var i=0;i<showField.length;i++){
                                            var field = showField[i][1];
                                            var hidden = showField[i][2];
                                            if(hidden) addHtml += '<tr><td><input class="small_input" type="checkbox" name="tableShow[check][]" value="'+ret.data[key][field]+'"></td><td '+hidden+'><input type="text" name="tableShow['+field+'][]" value="'+ret.data[key][field]+'"></td>'
                                            else addHtml += '<td>'+(typeof ret.data[key][field]!="undefined"?ret.data[key][field]:"")+'</td>';
                                        }
                                        addHtml += '</tr>';
                                    }
                                    $("#tbshow").append(addHtml);
                                    showList.push(ret.data[key]["SCD_ID"]);
                                }
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    return showList;
                }

                function delchecked(num){
                    $("#tbshow").find("tr").each(function () {
                        var checkedField = $(this).children('td').eq(0).find('input');
                        if(checkedField.val().trim() == num){
                            checkedField.prop("checked","");
                        }
                    });
                }

                $(document).on("click", "#send_tower", function () {
                    var formData = $(field).serializeArray();
                    $.ajax({
                        url: 'chain/sale/task/dealTable',
                        type: 'post',
                        dataType: 'json',
                        data: formData,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                if (ret.code === 1) {
                                    // $("#tbdeal").html("");
                                    var addHtml = "";
                                    for(key in ret.data){
                                        if($.inArray(parseInt(ret.data[key]["SCD_ID"]),dealList)==-1){
                                            addHtml +='<tr><td style="width:90px"><input type="button" class="table_del" value="删除" readonly></td>';
                                            for(var i=0;i<dealshowField.length;i++){
                                                if(dealshowField[i][2]=="button"){
                                                    addHtml += '<td><a href="javascript:;" class="btn btn-xs btn-info table_update"><i class="fa fa-pencil"></i></a></td>';
                                                }else if(dealshowField[i][1]=='TH_DeliverTime'){
                                                    addHtml += '<td><input type="text" class="small_input" placeholder="YYYY-MM-DD HH:mm:ss" '+dealshowField[i][3]+' name="table_row['+dealshowField[i][1]+'][]"  value="'+nowtime+'"></td>';
                                                }else{
                                                    var hidden = dealshowField[i][4];
                                                    var field = dealshowField[i][1];
                                                    addHtml += '<td '+hidden+'><input class="small_input" type="'+dealshowField[i][2]+'" '+dealshowField[i][3]+' name="table_row['+dealshowField[i][1]+'][]" value="'+(typeof ret.data[key][field]!="undefined"?ret.data[key][field]:"")+'"></td>';
                                                }
                                            }
                                            addHtml += '</tr>';
                                            dealList.push(ret.data[key]["SCD_ID"])
                                            // dealList = dealList.concat(ret.data[key]["SCD_ID"]);
                                        }
                                    }
                                    $("#tbdeal").append(addHtml);
                                    $("#c-T_SumWeight").val(sumWeightjs());
                                    // $("#c-T_SumWeight").val(ret.weight);
                                }
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });
                $(document).on("click", ".table_update", function () {
                    var index = $(this).parents('tr').index();
                    var SCD_ID = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[SCD_ID][]"]').val().trim();
                    var change_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[SCD_ChangeWeight][]"]').val().trim();
                    $.ajax({
                        url: 'chain/sale/task/confirmChange',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        data: {SCD_ID: SCD_ID, change_weight:change_weight},
                        success: function (ret) {
                            if(ret.code == 1){
                                $.ajax({
                                    url: 'chain/sale/task/changeWeight',
                                    type: 'post',
                                    dataType: 'json',
                                    async: false,
                                    data: {SCD_ID: SCD_ID, change_weight:change_weight},
                                    success: function (ret) {
                                        layer.msg(ret.msg);
                                    }, error: function (e) {
                                        Backend.api.toastr.error(e.message);
                                    }
                                });
                            }else{
                                var index = layer.confirm(ret.msg, {
                                    btn : ['确定', '取消']
                                    }, function() {
                                        $.ajax({
                                            url: 'chain/sale/task/changeWeight',
                                            type: 'post',
                                            dataType: 'json',
                                            async: false,
                                            data: {SCD_ID: SCD_ID, change_weight:change_weight},
                                            success: function (ret) {
                                                layer.msg(ret.msg);
                                                layer.close(index);
                                            }, error: function (e) {
                                                Backend.api.toastr.error(e.message);
                                            }
                                        });
                                });
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    
                });
                $(document).on("click", "#all_select", function () {
                    $("#tbshow").find("tr").each(function () {
                        var checkedField = $(this).children('td').eq(0).find('input');
                        checkedField.prop("checked","true");
                    });
                });
                $(document).on("click", "#giveup_select", function () {
                    $("#tbshow").find("tr").each(function () {
                        var checkedField = $(this).children('td').eq(0).find('input');
                        checkedField.prop("checked","");
                    });
                });

                function sumWeightjs(){
                    var weight = 0;
                    var count = 0;
                    var end_weight = 0;
                    var chooseCtrlTr = $("#deal").find("tbody tr");
                    // if(js_way==1){
                        $.each(chooseCtrlTr,function(index,e){
                            var e_weight = $(e).find('td input[name="table_row[SCD_Weight][]"]').val().trim();
                            e_weight = isNaN(e_weight)?0:parseFloat(e_weight);
                            var e_count = $(e).find('td input[name="table_row[SCD_Count][]"]').val().trim();
                            e_count = isNaN(e_count)?0:parseFloat(e_count);
                            weight += parseFloat((e_weight*e_count).toFixed(3));
                            var e_count = $(e).find('td input[name="table_row[SCD_Count][]"]').val().trim();
                            e_count = isNaN(e_count)?0:parseFloat(e_count);
                            count += parseFloat(e_count);
                        });
                    if(weight==0) end_weight=count;
                    else end_weight = weight;
                    // }else{
                        // $.each(chooseCtrlTr,function(index,e){
                        //     var e_count = $(e).find('td input[name="table_row[SCD_Count][]"]').val().trim();
                        //     e_count = isNaN(e_count)?0:parseFloat(e_count);
                        //     //e_count = e_count?parseFloat(e_count):0;
                        //     count += parseFloat(e_count);
                        // });
                    // }
                    return parseFloat(end_weight.toFixed(3));
                }

                $('#deal').on('keyup','td input[name="table_row[TH_Weight][]"]',function () {
                    var weight = sumWeightjs();
                    $("#c-T_SumWeight").val(weight);
                })

                // //新增部分 改变单位
                // $(document).on('change', "#c-C_unit", function(){
                //     var unit = $(this).selectpicker("val");
                //     if(unit=="kg") js_way = 1;
                //     else js_way = 0;
                // });
            }
        }
    };
    return Controller;
});