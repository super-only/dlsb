
define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    saleTypeList = typeof saleTypeList=="undefined"?"":saleTypeList;
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/project_cate_log/index' + location.search,
                    add_url: 'chain/sale/project_cate_log/add',
                    edit_url: 'chain/sale/project_cate_log/edit',
                    del_url: 'chain/sale/project_cate_log/del',
                    multi_url: 'chain/sale/project_cate_log/multi',
                    import_url: 'chain/sale/project_cate_log/import',
                    table: 'projectcatalog',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PC_Num',
                sortName: 'PC_Num',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'ST_Num', title: __('St_num'), operate: 'LIKE'},
                        {field: 'ST_Num', title: __('St_num'), searchList: saleTypeList},
                        {field: 'PC_type', title: '产品名称', searchList: {1:'角钢塔',3:'铁附件',4:'钢管杆',5:'钢管塔'},formatter: function(e){
                            let value_name = '';
                            switch(e){
                                case 1:value_name='角钢塔';break;
                                case 3:value_name='铁附件';break;
                                case 4:value_name='钢管杆';break;
                                default:value_name='钢管塔';break;
                            }
                            return value_name;
                        }},
                        {field: 'PC_Num', title: __('Pc_num'), operate: 'LIKE'},
                        {field: 'C_Name', title: __('C_name'), operate: 'LIKE'},
                        {field: 'PC_ProjectName', title: __('Pc_projectname'), operate: 'LIKE'},
                        {field: 'PC_ShortName', title: __('Pc_shortname'), operate: false},
                        // {field: 'PC_Employer', title: __('Pc_employer'), operate: 'LIKE'},
                        {field: 'PC_Pressure', title: __('Pc_pressure'), operate: 'LIKE'},
                        {field: 'PC_Place', title: __('Pc_place'), operate: 'LIKE'},
                        {field: 'PC_StartPlace', title: __('Pc_startplace'), operate: 'LIKE'},
                        {field: 'PC_StartDate', title: __('Pc_startdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'PC_gross', title: __('Pc_gross'), operate:false},
                        {field: 'PC_EditDate', title: __('Pc_editdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'PC_SalesMan', title: __('Pc_salesman'), operate: 'LIKE'},
                        {field: 'Writer', title: __('Writer'), operate: false},
                        {field: 'Auditor', title: __('Auditor'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $(document).on("click", "#author", function () {
                var PC_Num = $("input[name='row[PC_Num]']").val();
                if(PC_Num == false){
                    layer.confirm('请先保存！');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/sale/project_cate_log/auditor",PC_Num);
            });
            $(document).on("click", "#giveup", function () {
                var PC_Num = $("input[name='row[PC_Num]']").val();
                if(PC_Num == false){
                    layer.confirm('请重试！');
                    return false;
                }
                check('确定弃审？',"chain/sale/project_cate_log/giveUp",PC_Num);
                
            });
            function check(msg,url,num){
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {PC_Num: num},
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                if (ret.code === 1) {
                                    window.location.reload();
                                }
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        selectcustomer: function() {
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'chain/sale/project_cate_log/selectType',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/project_cate_log/selectCustomer' + location.search,
                    table: 'customer',
                }
            });

            var table = $("#select-customer-table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'C_Num',
                sortName: 'C_Num',
                sortOrder: 'DESC',
                clickToSelect: true,
                singleSelect: true,
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'C_Num', title: __('C_num'), operate: "LIKE"},
                        {field: 'C_Name', title: __('C_name'), operate: "LIKE"},
                        {field: 'CC_Num', title: __('Cc_num'), addClass: "selectpage"},
                        {field: 'AD_Name', title: __('Ad_name'), addClass: "selectpage"},
                        {field: 'C_C_Contacter1', title: __('C_c_contacter1'), operate: "LIKE"},
                        {field: 'C_C_Tel', title: __('C_c_tel'), operate: false},
                        {field: 'C_C_Fax', title: __('C_c_fax'), operate: false},
                        {field: 'C_C_SalesMan', title: __('C_c_salesMan'), operate: "LIKE"},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            var cc_num_search = $('form input[name="CC_Num"]');
            cc_num_search.data('source', 'chain/sale/project_cate_log/selectCcNum');
            cc_num_search.data('showField', 'title');
            cc_num_search.data('keyField', 'id');
            var ad_name_search = $('form input[name="AD_Name"]');
            ad_name_search.data('source', 'chain/sale/project_cate_log/selectAdName');
            ad_name_search.data('showField', 'title');
            ad_name_search.data('keyField', 'id');
            ad_name_search.data('pagination', false);
            ad_name_search.data('listSize', 7);
            Controller.api.bindevent();

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('#treeview').on('changed.jstree', function (e, data) {
                var CC_Num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'CC_Num': CC_Num}),
                        op: JSON.stringify({'CC_Num': 'like'}),
                        tree: CC_Num
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
            }).jstree();
            $(document).on("click", ".data-return", function () {
                var formData=table.bootstrapTable('getAllSelections');
                Fast.api.close(formData[0]);
            });

            $(document).on("change", "select[name='row[CC_Num]']", function () {
                var area = $(this).find("option:selected").val();
                $.ajax({
                    url: "jichu/wl/Customer/customerType",
                    type: 'post',
                    dataType: 'json',
                    data: {num: area},
                    success: function (ret) {
                        if (ret.hasOwnProperty("code")) {
                            if (ret.code === 1) {
                                $("#c-CC_Num").val(ret.data["C_Num"]);
                            }
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });

            Form.api.bindevent($("form[role=customer-form]"),function(data,ret){
                window.location.reload()
                return false;
            });


        },
        selectsaleman: function() {
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'jichu/jg/deptdetail/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/project_cate_log/selectSaleMan/type/'+Config.type + location.search,
                    table: 'employee',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'E_Num',
                sortName: 'E_Num',
                sortOrder: 'ASC',
                search: false,
                clickToSelect: Config.type==1?false:true,
                singleSelect: Config.type==1?false:true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'Valid', title: __('Valid'), 
                            searchList: {"0": "离职", "1": "在职", "2": "开除", "3": "辞职"},
                            formatter: function(value) {
                                var valid = ["离职","在职","开除","辞职"];
                                return valid[value];
                            }
                        },
                        {field: 'E_Num', title: __('E_num'), operate: 'LIKE'},
                        {field: 'E_Name', title: __('E_name'), operate: 'LIKE'},
                        {field: 'E_PerNum', title: '人员编码', operate: 'LIKE'},
                        {field: 'E_Sex', title: __('E_sex'), 
                            searchList: {"0": "女", "1": "男"},
                            formatter: function(value) {
                                var sex = ["女","男"];
                                return sex[value];
                            }
                        },
                        {field: 'E_hjCerif', title: '电焊证书', operate: false},
                        {field: 'DD_Num', title: __('Dd_num'), operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'DD_Num': dd_num}),
                        op: JSON.stringify({'DD_Num': 'like'}),
                        tree: dd_num
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
            }).jstree();
            $(document).on("click", ".data-return", function () {
                var formData=table.bootstrapTable('getAllSelections');
                if(Config.type==1) Fast.api.close(formData);
                else Fast.api.close(formData[0]);
                
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data; 
                    }else{
                        window.location.reload();
                    }
                    return false;
                });
                $(document).on("click", "#selectCustomer", function () {
                    var url = "chain/sale/project_cate_log/selectCustomer";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("input[name='row[C_Name]']").val(value.C_Name);
                            $("input[name='row[PC_Contactor]']").val(value.C_C_Contacter1);
                            $("input[name='row[PC_ContactTel]']").val(value.C_C_Tel);
                            $("input[name='row[PC_Fax]']").val(value.C_C_Fax);
                            $("input[name='row[PC_SalesMan]']").val(value.C_C_SalesMan);
                            //有误
                            $("#PC_Place").find("option:selected").val(value.AD_Name);
                        }
                    };
                    Fast.api.open(url,"客户选择",options);
                });
                $(document).on("click", "#selectSale", function () {
                    var url = "chain/sale/project_cate_log/selectSaleMan";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("input[name='row[PC_SalesMan]']").val(value.E_Name);
                        }
                    };
                    Fast.api.open(url,"员工选择",options);
                });
            }
        }
    };
    return Controller;
});