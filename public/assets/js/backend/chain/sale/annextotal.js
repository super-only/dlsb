define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/annextotal/index' + location.search,
                    add_url: 'chain/sale/annextotal/add',
                    edit_url: 'chain/sale/annextotal/edit',
                    del_url: 'chain/sale/annextotal/del',
                    multi_url: 'chain/sale/annextotal/multi',
                    import_url: 'chain/sale/annextotal/import',
                    table: 'annextotal',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), operate: false, formatter: function (value, row, index) {
                            return ++index;
                        }},
                        {
                            field: 'buttons',
                            width: "120px",
                            title: '任务单',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    text: '任务单',
                                    title: '任务单',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'chain/sale/task/print/t_num/{T_Num}',
                                }
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'date', title: __('Date'), operate: 'RANGE', addclass:'datetimerange', autocomplete:false, formatter: function (value, row, index) {
                            return value.substring(0,10);
                        }},
                        {field: 'T_Num', title: '任务单号', operate: 'LIKE'},
                        {field: 'PC_ProjectName', title: __('Pc_projectname'), operate: 'LIKE'},
                        {field: 'specifications', title: '规格', searchList: Config.specifitype, formatter: function (value, row, index){
                            return Config.specifitype[value];
                        }},
                        {field: 'PC_SalesMan', title: '业务员', operate: '='},
                        {field: 'count', title: __('Count'), operate: false},
                        {field: 'unit', title: __('Unit'), operate: false},
                        {field: 'C_Name', title: __('C_name'), operate: 'LIKE'},
                        {field: 'amount', title: __('Amount'), operate: false},
                        {field: 'kp_date', title: __('Kp_date'), operate: false, addclass:'datetimerange', autocomplete:false, formatter: function (value, row, index) {
                            return value.substring(0,10);
                        }},
                        // {field: 'remark', title: __('Remark'), operate: false},
                        {field: 'price', title: __('Price'), operate: false},
                        {field: 'jg_count', title: __('Jg_count'), operate: false},
                        {field: 'jg_amount', title: __('Jg_amount'), operate: false},
                        {field: 'jg_date', title: __('Jg_date'), operate: false, addclass:'datetimerange', autocomplete:false, formatter: function (value, row, index) {
                            return value.substring(0,10);
                        }},
                        {field: 'writer', title: __('Writer'), operate: false},
                        {field: 'writer_time', title: __('Writer_time'), operate: false, addclass:'datetimerange', autocomplete:false, formatter: function (value, row, index) {
                            return value.substring(0,10);
                        }},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
            $("#print").click(function () {
                var t_num = $("#c-T_Num").val();
                if(t_num){
                    window.top.Fast.api.open('chain/sale/task/print/t_num/' + t_num, '生产任务单', {
                        area: ["100%", "100%"]
                    });
                }
            });
        },
        selectdetail: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/annextotal/selectDetail' + location.search,
                    table: 'taskdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'T_WriterDate',
                sortName: 'T_WriterDate',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                sortOrder: 'DESC',
                showToggle: false,
                showExport: false,
                searchFormVisible: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'T_Num', title: '任务单(生产单)号', operate: 'LIKE'},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'c.PC_Num', title: '工程编号', operate: 'LIKE',visible: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE',visible: false},
                        {field: 'C_Project', title: '工程名称', operate: 'LIKE'},
                        {field: 'Customer_Name', title: '客户', operate: false},
                        {field: 'C_SaleMan', title: '业务员', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent[0]);

            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
                $(document).on("click", "#selectProject", function () {
                    var url = "chain/sale/annextotal/selectDetail";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            if(value.length == 0) return false;
                            else{
                                $("#c-T_Num").val(value.T_Num);
                                $("#c-C_Name").val(value.Customer_Name);
                                $("#c-PC_ProjectName").val(value.C_Project);
                                $("#c-PC_SalesMan").val(value.C_SaleMan);
                            }
                        }
                    };
                    Fast.api.open(url,"任务单选择",options);
                });
                $(document).on('keyup','input[name="row[price]"]',function () {
                    var price = $(this).val();
                    price==''?price=0:'';
                    price = parseFloat(price);
                    var count = $('input[name="row[jg_count]"]').val();
                    count==''?count=0:'';
                    count = parseFloat(count);
                    var amount = (price*count).toFixed(2);
                    $('input[name="row[jg_amount]"]').val(amount);
                })
                $(document).on('keyup','input[name="row[jg_count]"]',function () {
                    var price = $(this).val();
                    price==''?price=0:'';
                    price = parseFloat(price);
                    var count = $('input[name="row[price]"]').val();
                    count==''?count=0:'';
                    count = parseFloat(count);
                    var amount = (price*count).toFixed(2);
                    $('input[name="row[jg_amount]"]').val(amount);
                })
            }
        }
    };
    return Controller;
});