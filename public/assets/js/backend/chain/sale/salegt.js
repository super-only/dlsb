define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    nowtime = typeof nowtime=="undefined"?"":nowtime;

    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: {},
                height: 250,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'SCD_TPNum', title: '杆塔号'},
                        {field: 'BiaoDuan', title: '标段'},
                        {field: 'SCD_lineName', title: '线路'},
                        {field: 'TD_Pressure', title: '电压等级'},
                        {field: 'TD_TypeName', title: '塔型'},
                        {field: 'TH_Height', title: '呼高'},
                        {field: 'SCD_ProductName', title: '产品名称'},
                        {field: 'SCD_Unit', title: '单位'},
                        {field: 'SCD_Count', title: '数量'},
                        {field: 'SCD_ALength', title: 'A腿长(米)'},
                        {field: 'SCD_BLength', title: 'B腿长(米)'},
                        {field: 'SCD_CLength', title: 'C腿长(米)'},
                        {field: 'SCD_DLength', title: 'D腿长(米)'},
                        {field: 'SCD_Weight', title: '单重(kg)'},
                        {field: 'Sum_Weight', title: '总重'},
                        {field: 'SCD_UnitPrice', title: '单价(元)'},
                        {field: 'SCD_Corner', title: '转角度数'},
                        {field: 'SCD_BStyle', title: '基础形式'},
                        {field: 'SCD_MountPoint', title: '挂点'},
                        {field: 'SCD_SendDate', title: '交货日期'},
                        {field: 'SCD_BGK', title: '基础半根开'},
                        {field: 'SCD_StoneBoltZJ', title: '地脚螺栓直径'},
                        {field: 'SCD_Memo', title: '备注'}
                    ]
                ]
            });

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/salegt/index' + location.search,
                    add_url: 'chain/sale/salegt/add',
                    edit_url: 'chain/sale/salegt/edit',
                    del_url: 'chain/sale/salegt/del',
                    multi_url: 'chain/sale/salegt/multi',
                    import_url: 'chain/sale/salegt/import',
                    table: 'salegt',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SGT_ID',
                sortName: 'WriteDate',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                // height: 500,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'c.PC_Num', title: '工程编号', operate: 'LIKE', visible:false},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'c.produce_type', title: '产品类型', searchList: Config.produce_type,visible: false},
                        {field: 'produce_type', title: '产品类型', operate: false,formatter:function(value){return Config.produce_type[value];}},
                        {field: 'C_Num', title: __('C_num'), operate: 'LIKE'},
                        {field: 'c.Customer_Name', title: '工程编号', operate: 'LIKE', visible:false},
                        {field: 'Customer_Name', title: '客户', operate: false},
                        {field: 'c.C_Project', title: '工程名称', operate: 'LIKE', visible:false},
                        {field: 'C_Project', title: '工程名称', operate: false},
                        {field: 'c.C_SortProject', title: '工程简称', operate: 'LIKE', visible:false},
                        {field: 'C_SortProject', title: '工程简称', operate: false},
                        {field: 'Writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'WriteDate', title: __('Writedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'Auditor', title: __('Auditor'), operate: false},
                        {field: 'AuditDate', title: __('Auditdate'), operate:false, addclass:'datetimerange', autocomplete:false},
                        {field: 'Memo', title: __('Memo'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var ids = $element.SGT_ID;
                if(ids == '') return false;
                $.ajax({
                    url: 'chain/sale/salegt/contrast',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:ids},
                    success: function (ret) {
                        var content = {};
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $(document).on("click", "#updatePurchase", function () {
                var SGT_ID = $("#c-SGT_ID").val();
                $.ajax({
                    url: 'chain/sale/salegt/eipUpload',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {SGT_ID: SGT_ID},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#author", function () {
                var C_Num = $("#c-SGT_ID").val();
                if(C_Num == false){
                    layer.confirm('请先保存！');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/sale/salegt/auditor",C_Num);
            });
            $(document).on("click", "#giveup", function () {
                var C_Num = $("#c-SGT_ID").val();
                if(C_Num == false){
                    layer.confirm('请重试！');
                    return false;
                }
                check('确定弃审？',"chain/sale/salegt/giveUp",C_Num);
                
            });
            function check(msg,url,num){
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {SGT_ID: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                // Layer.msg(ret.msg);
                                if (ret.content == 1) {
                                    $('#giveup').removeAttr("disabled");
                                    $('.btn-success').attr("disabled",true);
                                    $('#author').attr("disabled",true);
                                }else{
                                    $('#giveup').attr("disabled",true);
                                    $('.btn-success').removeAttr("disabled");
                                    $('#author').removeAttr("disabled");
                                }
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        selectcompact: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/salegt/selectcompact' + location.search,
                    table: 'compact',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'C_Num',
                sortName: 'C_WriteDate',
                sortOrder: 'DESC',
                clickToSelect: true,
                singleSelect: true,
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'produce_type', title: '产品类型', searchList: Config.produce_type,formatter:function(value){return Config.produce_type[value]??'';}},
                        {field: 'C_Num', title: __('C_num'), operate: 'LIKE'},
                        {field: 'C_Name', title: __('C_name'), operate: 'LIKE'},
                        {field: 'Customer_Name', title: __('Customer_name'), operate: 'LIKE'},
                        {field: 'PC_Num', title: __('Pc_num'), operate: 'LIKE'},
                        {field: 'C_Project', title: __('C_project'), operate: 'LIKE'},
                        {field: 'C_SortProject', title: __('C_sortproject'), operate: 'LIKE'},
                        {field: 'C_SaleMan', title: __('C_saleman'), operate: 'LIKE'},
                        
                    ]
                ]
            });
            
            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var formData=table.bootstrapTable('getAllSelections');
                Fast.api.close(formData[0]);
            });
        },
        api: {
            bindevent: function (field='edit') {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data; 
                    }else{
                        window.location.reload();
                    }
                    return false;
                });
                var fieldArr = Config.fieldArr;
                // var tableWidth = $(".table").width();
                // var thContent = $("table thead tr th");
                // var tdNum = thContent.length;
                // var width = tableWidth/tdNum;
                // $.each(thContent,function(index,e){
                //     if(e.attributes.width.value<width) e.attributes.width.value = width;
                // }); 
                $(document).on('click', "#table_add", function(){
                    let c_num = $("#c-C_Num").val();
                    if(!c_num){
                        layer.msg("请先选择合同");
                    }else{
                        var field_append='<tr><td style="width:90px"><input type="button" class="table_del" value="删除" readonly></td>';
                        for(var i=0;i<fieldArr.length;i++){
                            if(fieldArr[i][1]=='SCD_ID'){
                                field_append += '<td hidden><input type="text" name="table_row['+fieldArr[i][1]+'][]" value="0" class="small_input"></td>';
                            }else if(fieldArr[i][1]=='SCD_SendDate'){
                                field_append += '<td><input type="'+fieldArr[i][2]+'" class="datetimepicker" data-date-format="YYYY-MM-DD HH:mm:ss" data-use-current="true" '+fieldArr[i][3]+' name="table_row['+fieldArr[i][1]+'][]"  value="'+nowtime+'"></td>';
                            }else{
                                field_append += '<td><input class="small_input" type="'+fieldArr[i][2]+'" '+fieldArr[i][3]+' name="table_row['+fieldArr[i][1]+'][]" value="'+fieldArr[i][4]+'"></td>';
                            }
                        }
                        field_append += '</tr>';
                        $("table>tbody").append(field_append);
                    }
                });
        
                $(document).on('click', ".table_del", function(e){
                    $( e.target ).closest("tr").remove();

                });
                $(document).on("click", "#selectProject", function () {
                    var url = "chain/sale/salegt/selectCompact";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("input[name='row[C_Num]']").val(value.C_Num);
                            $("#c-project").val(value.C_Project);
                            $("#c-shortProject").val(value.C_SortProject);
                            fieldArr[7][4] = Config.produce_type[value.produce_type];
                        }
                    };
                    Fast.api.open(url,"选择合同",options);
                });
                $(document).on("click", "#selectCopy", function () {
                    var C_Num = $("input[name='row[C_Num]']").val();
                    if(C_Num == false){
                        layer.confirm('请先选择合同！');
                        return false;
                    }
                    var index = layer.confirm('拷贝明细会覆盖原有，是否确认拷贝？', {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: "chain/sale/salegt/detail",
                            type: 'post',
                            dataType: 'json',
                            data: {C_Num: C_Num},
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    if (ret.code === 1) {
                                        // console.log(ret.data);return false;
                                        $("table>tbody").html();
                                        var addHtml = "";
                                        for(key in ret.data){
                                            addHtml +='<tr><td style="width:90px"><input type="button" class="table_del" value="删除" readonly></td>';
                                            for(var i=0;i<fieldArr.length;i++){
                                                var field = fieldArr[i][1];
                                                if(fieldArr[i][1]=='SCD_ID'){
                                                    addHtml += '<td hidden><input class="small_input" type="'+fieldArr[i][2]+'" '+fieldArr[i][3]+' name="table_row['+fieldArr[i][1]+'][]" value="'+(typeof ret.data[key][field]!="undefined"?ret.data[key][field]:fieldArr[i][4])+'"></td>';
                                                }else if(fieldArr[i][1]=='SCD_SendDate'){
                                                    addHtml += '<td><input type="'+fieldArr[i][2]+'" class="datetimepicker" data-date-format="YYYY-MM-DD" data-use-current="true" '+fieldArr[i][3]+' name="table_row['+fieldArr[i][1]+'][]"  value="'+nowtime+'"></td>';
                                                }else{
                                                    addHtml += '<td><input class="small_input" type="'+fieldArr[i][2]+'" '+fieldArr[i][3]+' name="table_row['+fieldArr[i][1]+'][]" value="'+(typeof ret.data[key][field]!="undefined"?ret.data[key][field]:fieldArr[i][4])+'"></td>';
                                                }
                                            }
                                            addHtml += '</tr>';
                                        }
                                        $("table>tbody").html(addHtml);
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                });
            }
        }
    };
    return Controller;
});