define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'dcadata'], function ($, undefined, Backend, Table, Form) {
    function col_index(div_content = "#tbshow"){
        var col = 0;
        $(div_content).find("tr").each(function () {
            col++;
            $(this).children('td').eq(0).text(col);
        });
    }
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/draft/index' + location.search,
                    add_url: 'chain/sale/draft/add',
                    edit_url: 'chain/sale/draft/edit',
                    del_url: 'chain/sale/draft/del',
                    multi_url: 'chain/sale/draft/multi',
                    // import_url: 'chain/sale/draft/import',
                    table: 'draft',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'ID',
                sortName: 'ID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ID', title: __('Id')},
                        {field: 'type', title: __('Type'),formatter: function(value, row, index){
                            var type_list = Config.type_list;
                            return type_list[value];
                        },searchList: Config.type_list},
                        {field: 'project_name', title: __('Project_name'), operate: 'LIKE'},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on('click', ".import", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["60%","40%"],
                };
				Fast.api.open('chain/sale/draft/importType',"导入",options);
            });
        },
        importtype: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        add: function () {
            Controller.api.bindevent("add");
            var fieldHideContent = Config.fieldHideContent;
            $(document).on('click', "#table_add", function(){
                var col_count = $("#tbshow").find("tr").length;

                var content = "";
                content += '<tr><td>'+col_count+'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td><td><input type="checkbox"></td><td hidden><input class="small_input" type="text" name="ID" value=""></td>';
                $.each(fieldHideContent,function(index,e){
                    if(e["son"].length==0) content += '<td><input class="small_input" type="text" name="'+e[1]+'" value=""></td>';
                    else{
                        $.each(e["son"],function(e_index,e_e){
                            content += '<td><input class="small_input" type="text" name="'+e_e[1]+'" value=""></td>';
                        });
                    }
                });
                content += "</tr>";
                $("#tbshow tr").last().before(content);
                col_index("#tbshow");
            });
            $(document).on('change', "#c-type", function(){
                if(!$(this).val()) return false;
                type = $(this).val();
                $.ajax({
                    url: 'chain/sale/draft/theadField/type/'+type,
                    type: 'post',
                    dataType: 'json',
                    data: {type: type},
                    success: function (ret) {
                        if(ret.code == 1){
                            $("#show").html(ret.data.html);
                            fieldHideContent = ret.data.fieldHideContent;
                        }else{
                            layer.msg(ret.msg);
                        }
                        // var last_html = $("#tbshow tr").last().html();
                        // $("#tbshow").html(last_html);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        edit: function () {
            $("#print").click(function () {
                window.top.Fast.api.open('chain/sale/draft/print/ids/' + Config.ids, '打草案', {
                    area: ["100%", "100%"]
                });
            });
            Controller.api.bindevent("edit");
            var fieldHideContent = Config.fieldHideContent;
            var type = Config.type;
            var choose_field = localStorage.getItem("true_field/"+type);
            var ss = [];
            if(choose_field){
                ss = choose_field.split(",");
                $.each(ss,function(choose_index,choose_e){
                    $("#hidde_operation").find("input[data-name='"+choose_e+"']").prop("checked",false);
                    $("#show tr:first th:nth-child("+fieldHideContent[choose_e]["first"]+")").hide();
                    if(fieldHideContent[choose_e]["son"].length!=0){
                        $.each(fieldHideContent[choose_e]["son"],function(index,e){
                            $("#show tr:nth-child(2) th:nth-child("+e["second"]+")").hide();
                        }); 
                    }
                    for(var i=fieldHideContent[choose_e]["start"];i<=fieldHideContent[choose_e]["end"];i++){
                        $("#show tr td:nth-child("+i+")").hide();
                    }
                }); 
            }
            $(document).on("click", "#hidde_operation input", function(e){
                var true_field=[];
                $.each($("#hidde_operation input"),function(index,e){
                    if(!$(e).prop("checked")) true_field.push($(e).data("name"));
                }); 
                localStorage.setItem("true_field/"+type,true_field);
                var name = $(this).data("name");
                var flag = $(this).prop("checked");
                if(!flag){
                    $("#show tr:first th:nth-child("+fieldHideContent[name]["first"]+")").hide();
                    if(fieldHideContent[name]["son"].length!=0){
                        $.each(fieldHideContent[name]["son"],function(index,e){
                            $("#show tr:nth-child(2) th:nth-child("+e["second"]+")").hide();
                        }); 
                    }
                    for(var i=fieldHideContent[name]["start"];i<=fieldHideContent[name]["end"];i++){
                        $("#show tr td:nth-child("+i+")").hide();
                    }
                }else{
                    $("#show tr:first th:nth-child("+fieldHideContent[name]["first"]+")").show();
                    if(fieldHideContent[name]["son"].length!=0){
                        $.each(fieldHideContent[name]["son"],function(index,e){
                            $("#show tr:nth-child(2) th:nth-child("+e["second"]+")").show();
                        }); 
                    }
                    for(var i=fieldHideContent[name]["start"];i<=fieldHideContent[name]["end"];i++){
                        $("#show tr td:nth-child("+i+")").show();
                    }
                    
                }
                choose_field = localStorage.getItem("true_field/"+type);
                ss = choose_field.split(",");
            });
            $(document).on('click', "#table_add", function(){
                var content = fieldHideContent;
                if(ss.length!=0){
                    $.each(ss,function(index,e){
                        delete content[e];
                    }); 
                }
                var col_count = $("#tbshow").find("tr").length;
                var content = "";
                content += '<tr><td>'+col_count+'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td><td><input type="checkbox"></td><td hidden><input class="small_input" type="text" name="ID" value=""></td>';
                $.each(fieldHideContent,function(index,e){
                    if(e["son"].length==0) content += '<td><input class="small_input" type="text" name="'+e[1]+'" value=""></td>';
                    else{
                        $.each(e["son"],function(e_index,e_e){
                            content += '<td><input class="small_input" type="text" name="'+e_e[1]+'" value=""></td>';
                        });
                    }
                });
                content += "</tr>";
                $("#tbshow tr").last().before(content);
                col_index("#tbshow");
            });
            $(document).on("click", "#export", function () {
                if((fieldHideContent.length - ss.length) <= 2){
                    layer.msg("显示内容必须大于两列");
                }else window.open('/admin.php/chain/sale/draft/export/ids/'+Config.ids+'/field/'+choose_field);
            });
        },
        selectdraft: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/draft/selectDraft/type/' + Config.type + location.search,
                    table: 'draft',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'ID',
                sortName: 'ID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ID', title: __('Id'), operate: false},
                        {field: 'draft_id', title: '代码'},
                        {field: 'tower_type', title: '塔型', operate: 'LIKE'},
                        {field: 'pole_number', title: '杆号', operate: 'LIKE'},
                        {field: 'unit', title: '单位', operate: false},
                        {field: 'height', title: '呼高', operate: false},
                        {field: 'count', title: '数量', operate: false},
                        {field: 'sum_weight', title: '单重', operate: false},
                        {field: 'weight', title: '总重', operate: false},
                        {field: 'project_name', title: '工程名称', operate: 'LIKE'},
                        {field: 'OD_Memo', title: '备注', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var formData=table.bootstrapTable('getAllSelections');
                Fast.api.close(formData);
            });
        },
        print: function() {
            let row = Config.row;
            let list = Config.list;
            // console.log('row', row);
            // console.log('list', list);
            var choose_field = localStorage.getItem("true_field/"+row['type']);
            // console.log('choose_field', choose_field);
            // const hide_ref = [
            //     pole_number,tower_number,tower_type,height,body_height,leg_extension,count,connection_type,leaping_dance,segment,body,leg,update_add_weight,hang_point_weight,doublt_bolt_weight,bolt_add_weight,wall_add_weight,foot_add_weight,other_weight,total
            // ]
            
            if(row['type']==3){
                let countSum = list.reduce((pre, cur) => {
                    console.log('cur', cur);
                    if(cur['count']){
                        pre+=Number(cur['count']);
                    }
                    return pre;
                }, 0);
                
                list[list.length-1]['count'] = countSum;
            }
            const type_ref = {
                1: {
                    temp: complexdata,
                    data_tmp:{
                        gcmc: row['project_name'],
                        remark: row['remark'],
                        zb: row['writer'],
                        rq: row['writer_time'],
                        tb: list.map(e => {
                            return {
                                pole_number: e['pole_number'],
                                tower_number: e['tower_number'],
                                tower_type: e['tower_type'],
                                height: e['height'],
                                body_height: e['body_height'],
                                leg_a: e['leg_extension_a'] || '',
                                leg_b: e['leg_extension_b'] || '',
                                leg_c: e['leg_extension_c'] || '',
                                leg_d: e['leg_extension_d'] || '',
                                count: e['count'] || '',
                                connection_type: e['connection_type'] || '',
                                leaping_dance: e['leaping_dance'] || '',
                                bt: e['segment_body'] || '',
                                jt: e['segment_leg'] || '',
                                bt_tc: e['body_material'] || '',
                                bt_ls: e['body_bolt'] || '',
                                bt_xj: Number(e['body_weight']).toFixed(1) || '',
                                jt_tc: e['leg_material'] || '',
                                jt_ls: e['leg_bolt'] || '' ,
                                jt_xj: Number(e['leg_weight']).toFixed(1) || '',
                                update_add_weight: e['update_add_weight'] || '',
                                hang_point_weight: e['hang_point_weight'] || '',
                                doublt_bolt_weight: e['doublt_bolt_weight'] || '',
                                bolt_add_weight: e['bolt_add_weight'] || '',
                                wall_add_weight: e['wall_add_weight'] || '',
                                other_weight: e['other_weight'] || '',
                                hj_tc: Number(e['total_material']).toFixed(1) || '',
                                hj_ls: Number(e['total_bolt']).toFixed(1) || '',
                                hj_xj: Number(e['sum_weight']).toFixed(1) || '',
                                remark: e['remark'],
                                construction_section: e['construction_section']
                            }
                        })
                    }
                },
                2: {
                    temp: simpledata,
                    data_tmp:{
                        gcmc: row['project_name'],
                        remark: row['remark'],
                        zb: row['writer'],
                        rq: row['writer_time'],
                        tb: list.map(e => {
                            return {
                                tower_type: e['tower_type'],
                                pole_number: e['pole_number'],
                                tower_number: e['tower_number'],
                                height: e['height'] || '',
                                body_height: e['body_height'] || '',
                                body_weight: e['body_weight'] || '',
                                tower_leg_weight: e['tower_leg_weight'] || '',
                                update_add_weight: e['update_add_weight'] || '',
                                bolt_add_weight: e['bolt_add_weight'] || '',
                                other_weight: e['other_weight'] || '',
                                sum_weight: e['sum_weight'] || '',
                                count: e['count'] || '',
                                weight: Number(e['weight']).toFixed(1) || '',
                                remark: e['remark'],
                                segment_form: e['segment_form'],
                                leg_a_length: e['leg_extension_a_length']|| '',
                                leg_b_length: e['leg_extension_b_length'] || '',
                                leg_c_length: e['leg_extension_c_length']|| '',
                                leg_d_length: e['leg_extension_d_length']|| '',
                                leg_a_weight: e['leg_extension_a_weight']|| '',
                                leg_b_weight: e['leg_extension_b_weight']|| '',
                                leg_c_weight: e['leg_extension_c_weight']|| '',
                                leg_d_weight: e['leg_extension_d_weight']|| '',
                            }
                        })
                    }
                },
                3:{
                    temp: tfjdata,
                    data_tmp:{
                        gcdm: row['ID'],
                        ggdw: row['factory'],
                        gcmc: row['project_name'],
                        sm: row['remark'],
                        zb: row['writer'],
                        sh: row['auditor'],
                        rq: row['writer_time'],
                        tb: list.map(e => {
                            return {
                                tower_type: e['tower_type'],
                                unit: e['unit'],
                                count: e['count'] || '',
                                dz: Number(e['sum_weight']).toFixed(1) || '',
                                zz: Number(e['weight']).toFixed(1) || '',
                                remark: e['remark'],
                            }
                        })
                    }
                }
            }
            
            let type_data = type_ref[row['type']];
            let data_tmp=type_data['data_tmp'];
            let temp = type_data['temp'];

            let tb = temp['panels'][0]['printElements'].find((e) => {
                return e['options']['field']=='tb';
            })
            let hide_fields = typeof(choose_field)=="string"? choose_field.split(','): [];
            let specialField = {};
            
            if(row['type']==1){
                specialField = {
                    'leg_extension': ['leg_a', 'leg_b', 'leg_c', 'leg_d'],
                    'segment': ['bt', 'jt'],
                    'body': ['bt_tc', 'bt_ls', 'bt_xj'],
                    'leg': ['jt_tc', 'jt_ls', 'jt_xj'],
                    'total':['hj_tc', 'hj_ls', 'hj_xj']
                };
                hide_fields.forEach((e) => {
                    delArrayEle(e+'_top', tb['options']['columns'][0]);
                    if(specialField[e]!=undefined){
                        specialField[e].forEach((sf) => {
                            delArrayEle(sf, tb['options']['columns'][1]);
                        })
                    }else{
                        delArrayEle(e, tb['options']['columns'][1]);
                    }
                })
                
            }
            else if(row['type']==2){
                specialField = {
                    'leg_extension': ['leg_a_length', 'leg_a_weight', 'leg_b_length', 'leg_b_weight', 'leg_c_length', 'leg_c_weight', 'leg_d_length', 'leg_d_weight'],
                };
                hide_fields.forEach((e) => {
                    delArrayEle(e+'_top', tb['options']['columns'][0]);
                    if(specialField[e]!=undefined){
                        specialField[e].forEach((sf) => {
                            delArrayEle(sf, tb['options']['columns'][1]);
                        })
                    }else{
                        delArrayEle(e, tb['options']['columns'][1]);
                    }
                    
                })
                
            }
            else if(row['type']==3){
                
                specialField = {
                    'weight_total': ['dz', 'zz'],
                };
                hide_fields.forEach((e) => {
                    
                    if(specialField[e]!=undefined){
                        specialField[e].forEach((sf) => {
                            delArrayEle(sf, tb['options']['columns'][0]);
                        })
                    }else{
                        delArrayEle(e, tb['options']['columns'][0]);
                    }
                    
                })
                
            }
            function delArrayEle(field, inArray){
                let colIndex = inArray.findIndex((col) => {
                    return col['field']==field;
                })
                if(colIndex>-1){
                    inArray.splice(colIndex, 1)
                }
                return inArray;
            }
            

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            var page = 0;
            window.pageto=function(id,num){
                page = num;
                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    // console.log(v, $(v).html());
                    $(v).hide();
                }

                // $("td[column-id='tower_type_top']").attr('rowspan', 2);
                
                // $('#19').remove();

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: temp});

            //渲染html
            $('#p_mx1').html(htemp.getHtml(data_tmp));
            

            // 处理单元格合并
            tb['options']['columns'][0].forEach((e) => {
                let secondField = e['field'].substring(0, e['field'].length-4);
                if(specialField[secondField]==undefined){
                    $("td[column-id='"+e['field']+"']").attr('rowspan', 2);
                    $("td[column-id='"+e['field']+"']").css('width', e['width']+'pt');

                    $("td[column-id='"+secondField+"']").remove();
                }
            })
            

            // 保存打印所需的html
            let rowspanHtml = $('#p_mx1').html();

            // html进行分页显示处理
            pageto('#p_mx1',0);
            
            $("#handleprintmx1").click(function(){
                htemp.printByHtml(rowspanHtml);
            });
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                    }
                    return false;
                });

                $(document).on('click', "#tbshow input[type='checkbox']", function(e){
                    var checkbox_div = $(e.target);
                    if(checkbox_div.is(':checked')){
                        $(e.target).parents("tr").css("background-color","#E0FFFF");
                    }else{
                        $(e.target).parents("tr").removeAttr("style");
                    }
                });

                var table_height = document.body.clientHeight-255;
                $("#show").parent("div").height(table_height);
                var type = Config.type;
                $(document).on('click', "#copy_add", function(){
                    var copyChoose,text;
                    $("#tbshow").find("tr").each(function () {
                        if($(this).children('td').eq(2).find('input').prop("checked")){
                            $(this).children('td').eq(2).find('input').prop("checked","");
                            copyChoose = $(this).html();
                            text += "<tr style='background-color: #E0FFFF'>"+copyChoose.replace(/name\=\"ID\" value\=\"\d*\"/,'name="ID" value=""')+"</tr>";
                        }
                    });
                    $("#tbshow tr").last().before(text);
                    col_index("#tbshow");
                });
                $(document).on('click', "#save", function(){
                    var form = field == "add"?$("#add-form"):$("#edit-form");
                    var formData = form.serializeArray();
                    $.ajax({
                        url: 'chain/sale/draft/save/ids/'+Config.ids,
                        type: 'post',
                        dataType: 'json',
                        data: {data: JSON.stringify(formData)},
                        success: function (ret) {
                            if(ret.code==1){
                                layer.msg(ret.msg, {
                                    icon: 1,
                                    time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                  }, function(){
                                    window.location.href = "edit/ids/"+ret.data;
                                }); 
                            }else{
                                layer.msg(ret.msg, {
                                    icon: 2,
                                    time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                }); 
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });
                

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="ID"]').val();
                    if(num!=''){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/sale/draft/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                        col_index("#tbshow");
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                        col_index("#tbshow");
                    }
                });
                $(document).on("click", "#author", function () {
                    check('审核之后无法修改，确定审核？',"chain/sale/draft/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    check('确定弃审？',"chain/sale/draft/giveUp",Config.ids);
                });
                function check(msg,url,num){
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if(ret.code==1){
                                    layer.msg(ret.msg, {
                                        icon: 1,
                                        time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                      }, function(){
                                        window.location.href = "edit/ids/"+Config.ids;
                                    }); 
                                }else{
                                    layer.msg(ret.msg, {
                                        icon: 2,
                                        time: 2000 //2秒关闭（如果不配置，默认是3秒）
                                    }); 
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }

                //计算
                $(document).on("keyup", "table input", function () {
                    if($(this).attr("type")=="text") $(this).attr("value",$(this).val());
                    var input_name = $(this).attr("name");
                    if(type==1){
                        if($.inArray(input_name,["body_material","body_bolt"])!=-1){
                            var index = $(this).parents('tr').index();
                            var body_material = $(this).parents('tbody').find('tr').eq(index).find('td input[name="body_material"]').val().trim();
                            var body_bolt = $(this).parents('tbody').find('tr').eq(index).find('td input[name="body_bolt"]').val().trim();
                            body_material = body_material?parseFloat(body_material):0;
                            body_bolt = body_bolt?parseFloat(body_bolt):0;
                            var body_weight = parseFloat(body_material+body_bolt).toFixed(2);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="body_weight"]').val(body_weight);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="body_weight"]').attr("value",body_weight);
                            if(input_name == "body_material"){
                                var leg_material = $(this).parents('tbody').find('tr').eq(index).find('td input[name="leg_material"]').val().trim();
                                leg_material = leg_material?parseFloat(leg_material):0;
                                var total_bolt = $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_bolt"]').val().trim();
                                total_bolt = total_bolt?parseFloat(total_bolt):0;
                                var total_material = parseFloat(body_material+leg_material).toFixed(2);
                                var sum_weight = parseFloat(body_material+leg_material+total_bolt).toFixed(2);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_material"]').val(total_material);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_material"]').attr("value",total_material);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').val(sum_weight);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').attr("value",sum_weight);
                            }else{
                                var leg_bolt = $(this).parents('tbody').find('tr').eq(index).find('td input[name="leg_bolt"]').val().trim();
                                leg_bolt = leg_bolt?parseFloat(leg_bolt):0;
                                var total_material = $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_material"]').val().trim();
                                total_material = total_material?parseFloat(total_material):0;
                                var total_bolt = parseFloat(body_bolt+leg_bolt).toFixed(2);
                                var sum_weight = parseFloat(total_material+body_bolt+leg_bolt).toFixed(2);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_bolt"]').val(total_bolt);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_bolt"]').attr("value",total_bolt);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').val(sum_weight);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').attr("value",sum_weight);
                            }
                        }
                        if($.inArray(input_name,["leg_material","leg_bolt"])!=-1){
                            var index = $(this).parents('tr').index();
                            var leg_material = $(this).parents('tbody').find('tr').eq(index).find('td input[name="leg_material"]').val().trim();
                            var leg_bolt = $(this).parents('tbody').find('tr').eq(index).find('td input[name="leg_bolt"]').val().trim();
                            leg_material = leg_material?parseFloat(leg_material):0;
                            leg_bolt = leg_bolt?parseFloat(leg_bolt):0;
                            var leg_weight = (leg_material+leg_bolt).toFixed(2);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="leg_weight"]').val(leg_weight);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="body_weight"]').attr("value",body_weight);
                            if(input_name == "leg_material"){
                                var body_material = $(this).parents('tbody').find('tr').eq(index).find('td input[name="body_material"]').val().trim();
                                body_material = body_material?parseFloat(body_material):0;
                                var total_bolt = $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_bolt"]').val().trim();
                                total_bolt = total_bolt?parseFloat(total_bolt):0;
                                var total_material = parseFloat(body_material+leg_material).toFixed(2);
                                var sum_weight = parseFloat(body_material+leg_material+total_bolt).toFixed(2);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_material"]').val(total_material);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_material"]').attr("value",total_material);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').val(sum_weight);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').attr("value",sum_weight);
                            }else{
                                var body_bolt = $(this).parents('tbody').find('tr').eq(index).find('td input[name="body_bolt"]').val().trim();
                                body_bolt = body_bolt?parseFloat(body_bolt):0;
                                var total_material = $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_material"]').val().trim();
                                total_material = total_material?parseFloat(total_material):0;
                                var total_bolt = parseFloat(body_bolt+leg_bolt).toFixed(2);
                                var sum_weight = parseFloat(total_material+body_bolt+leg_bolt).toFixed(2);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_bolt"]').val(total_bolt);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="total_bolt"]').attr("value",total_bolt);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').val(sum_weight);
                                $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').attr("value",sum_weight);
                            }
                        }
                    }
                    if(type==2){
                        if($.inArray(input_name,["body_weight","leg_extension_a_weight","leg_extension_b_weight","leg_extension_c_weight","leg_extension_d_weight","tower_leg_weight","update_add_weight","bolt_add_weight","other_weight","count"])!=-1){
                            var index = $(this).parents('tr').index();
                            var body_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="body_weight"]').val().trim();
                            var update_add_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="update_add_weight"]').val().trim();
                            var other_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="other_weight"]').val().trim();
                            var leg_extension_a_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="leg_extension_a_weight"]').val().trim();
                            var leg_extension_b_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="leg_extension_b_weight"]').val().trim();
                            var leg_extension_c_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="leg_extension_c_weight"]').val().trim();
                            var leg_extension_d_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="leg_extension_d_weight"]').val().trim();
                            var tower_leg_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="tower_leg_weight"]').val().trim();
                            var bolt_add_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="bolt_add_weight"]').val().trim();
                            var count = $(this).parents('tbody').find('tr').eq(index).find('td input[name="count"]').val().trim();
                            leg_extension_a_weight = leg_extension_a_weight?parseFloat(leg_extension_a_weight):0;
                            leg_extension_b_weight = leg_extension_b_weight?parseFloat(leg_extension_b_weight):0;
                            leg_extension_c_weight = leg_extension_c_weight?parseFloat(leg_extension_c_weight):0;
                            leg_extension_d_weight = leg_extension_d_weight?parseFloat(leg_extension_d_weight):0;
                            tower_leg_weight = tower_leg_weight?parseFloat(tower_leg_weight):0;
                            bolt_add_weight = bolt_add_weight?parseFloat(bolt_add_weight):0;
                            body_weight = body_weight?parseFloat(body_weight):0;
                            update_add_weight = update_add_weight?parseFloat(update_add_weight):0;
                            other_weight = other_weight?parseFloat(other_weight):0;
                            count = count?parseFloat(count):0;
                            var sum_weight = parseFloat(body_weight+update_add_weight+other_weight+leg_extension_a_weight+leg_extension_b_weight+leg_extension_c_weight+leg_extension_d_weight+tower_leg_weight+bolt_add_weight).toFixed(2);
                            var weight = (count*sum_weight).toFixed(2);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').val(sum_weight);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').attr("value",sum_weight);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="weight"]').val(weight);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="weight"]').attr("value",weight);
                        }
                        if(input_name == "sum_weight"){
                            var index = $(this).parents('tr').index();
                            var sum_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').val().trim();
                            sum_weight = sum_weight?parseFloat(sum_weight):0;
                            var count = $(this).parents('tbody').find('tr').eq(index).find('td input[name="count"]').val().trim();
                            count = count?parseFloat(count):0;
                            var weight = (count*sum_weight).toFixed(2);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="weight"]').val(weight);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="weight"]').attr("value",weight);
                        }
                    }
                    if(type==3){
                        if($.inArray(input_name,["count","sum_weight"])!=-1){
                            var index = $(this).parents('tr').index();
                            var count = $(this).parents('tbody').find('tr').eq(index).find('td input[name="count"]').val().trim();
                            count = count?parseFloat(count):0;
                            var sum_weight = $(this).parents('tbody').find('tr').eq(index).find('td input[name="sum_weight"]').val().trim();
                            sum_weight = sum_weight?parseFloat(sum_weight):0;
                            var weight = (count*sum_weight).toFixed(2);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="weight"]').val(weight);
                            $(this).parents('tbody').find('tr').eq(index).find('td input[name="weight"]').attr("value",weight);
                        }
                    }
                });
                
            }
        }
    };
    return Controller;
});