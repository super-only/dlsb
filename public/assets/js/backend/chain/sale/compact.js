define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {
    function col_index(div_content = "#suibian"){
        var col = 0;
        $(div_content).find("tr").each(function () {
            col++;
            $(this).children('td').eq(0).text(col);
        });
    }
    var Controller = {
        index: function () {
            contractLeft();
            contractRight();
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/compact/index' + location.search,
                    add_url: 'chain/sale/compact/add',
                    edit_url: 'chain/sale/compact/edit',
                    del_url: 'chain/sale/compact/del',
                    multi_url: 'chain/sale/compact/multi',
                    import_url: 'chain/sale/compact/import',
                    table: 'compact',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'C_Num',
                sortName: 'C_WriteDate',
                sortOrder: 'DESC',
                // height: 500,
                clickToSelect: true,
                singleSelect: true,
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'C_Num', title: __('C_num'), operate: 'LIKE'},
                        // {field: 'C_Name', title: __('C_name'), operate: 'LIKE'},
                        {field: 'produce_type', title: '产品类型', searchList: Config.produce_type,formatter:function(value){return Config.produce_type[value]??'';}},
                        // {field: 'poItemId', title: '行项目ID', operate: 'LIKE'},
                        {field: 'poNo', title: '采购订单号', operate: 'LIKE'},
                        {field: 'Customer_Name', title: __('Customer_name'), operate: 'LIKE'},
                        {field: 'PC_Num', title: __('Pc_num'), operate: 'LIKE'},
                        {field: 'C_Project', title: __('C_project'), operate: 'LIKE'},
                        {field: 'C_SortProject', title: __('C_sortproject'), operate: 'LIKE'},
                        {field: 'C_BidTime', title: __('C_bidtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'C_ProtocolTime', title: __('C_protocoltime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'C_GraphTime', title: __('C_graphtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'C_ifGraphFinish', title: __('C_ifgraphfinish'), operate: false},
                        {field: 'C_Sumprice', title: __('C_sumprice'), operate:false},
                        {field: 'C_Sumweight', title: __('C_sumweight'), operate:false},
                        {field: 'C_SaleType', title: __('C_saletype'), operate: false},
                        {field: 'UAB_JLUnitName', title: __('Uab_jlunitname'), operate: false},
                        {field: 'C_DeliverDate', title: __('C_deliverdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'C_Contractor', title: __('C_contractor'), operate: 'LIKE'},
                        {field: 'C_SaleMan', title: __('C_saleman'), operate: 'LIKE'},
                        {field: 'C_Phone', title: __('C_phone'), operate: false},
                        {field: 'C_Writer', title: __('C_writer'), operate: 'LIKE'},
                        {field: 'C_WriteDate', title: __('C_writedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Auditor', title: __('Auditor'), operate:false},
                        {field: 'AuditorDate', title: __('Auditordate'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });
            
            // 为表格绑定事件
            Table.api.bindevent(table);
            
            table.on('click-row.bs.table',function(row, $element){
                var ids = $element.C_Num;
                if(ids == '') return false;
                $.ajax({
                    url: 'chain/sale/compact/contrast',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:ids},
                    success: function (ret) {
                        var leftContent = rightContent = {};
                        if (ret.code === 1) {
                            leftContent = ret.leftList;
                            rightContent = ret.rightList;
                        }
                        $("#contrast_left").bootstrapTable('load',leftContent);
                        $("#contrast_right").bootstrapTable('load',rightContent);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
            function contractLeft(leftContent = {}){
                $("#contrast_left").bootstrapTable({
                    data: leftContent,
                    height: 300,
                    search: false,
                    pagination: false,
                    columns: [
                        [
                            {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                            {field: 'OD_LineNum', title: '杆塔号'},
                            {field: 'OD_LineName', title: '线路名称'},
                            {field: 'OD_Pressure', title: '电压等级'},
                            {field: 'OD_TypeName', title: '塔型'},
                            {field: 'OD_Height', title: '呼高'},
                            {field: 'OD_Count', title: '数量'},
                            {field: 'OD_Weight', title: '单重'},
                            {field: 'OD_SumWeight', title: '总重'},
                            {field: 'DeliverPlace', title: '送货地点'},
                            {field: 'OD_Memo', title: '备注'}
                        ]
                    ]
                });
            }
            function contractRight(rightContent = {}){
                $("#contrast_right").bootstrapTable({
                    data: rightContent,
                    height: 300,
                    search: false,
                    pagination: false,
                    columns: [
                        [
                            {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                            {field: 'SCD_TPNum', title: '杆塔号'},
                            {field: 'TD_Pressure', title: '电压等级'},
                            {field: 'TD_TypeName', title: '塔型'},
                            {field: 'TH_Height', title: '呼高'},
                            {field: 'SCD_Count', title: '数量'},
                            {field: 'SCD_Weight', title: '单重'},
                            {field: 'SCD_SumWeight', title: '总重'}
                        ]
                    ]
                });
            }
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
        },
        selectproject: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/sale/compact/selectProject' + location.search,
                    table: 'projectcatalog',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PC_Num',
                sortName: 'PC_Num',
                sortOrder: 'DESC',
                clickToSelect: true,
                singleSelect: true,
                showToggle: false, 
                showExport: false,
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'ST_Name', title: __('St_num'), operate: false},
                        {field: 'PC_type', title: '产品类型', searchList: Config.produce_type,formatter:function(value){return Config.produce_type[value];}},
                        {field: 'PC_Num', title: __('Pc_num'), operate: 'LIKE'},
                        {field: 'C_Name', title: '客户名称', operate: 'LIKE'},
                        {field: 'PC_ProjectName', title: __('Pc_projectname'), operate: 'LIKE'},
                        {field: 'PC_ShortName', title: __('Pc_shortname'), operate: 'LIKE'},
                        // {field: 'PC_Employer', title: __('Pc_employer'), operate: 'LIKE'},
                        {field: 'PC_Pressure', title: __('Pc_pressure'), operate: 'LIKE'},
                        {field: 'PC_Place', title: __('Pc_place'), operate: 'LIKE'},
                        // {field: 'PC_StartPlace', title: __('Pc_startplace'), operate: 'LIKE'},
                        {field: 'PC_StartDate', title: __('Pc_startdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'PC_gross', title: __('Pc_gross'), operate:false},
                        {field: 'PC_EditDate', title: __('Pc_editdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'PC_SalesMan', title: __('Pc_salesman'), operate: 'LIKE'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var formData=table.bootstrapTable('getAllSelections');
                Fast.api.close(formData[0]);
            });
        },
        api: {
            bindevent: function (field='edit') {
                Form.api.bindevent($("form[role=form]"), function(data, ret){
                    if(field=="add"){
                        window.location.href = "edit/ids/"+data; 
                    }else{
                        window.location.reload();
                    }
                    return false;
                });

                // 更新采购订单
                $(document).on("click", "#updatePurchase", function () {
                    $.ajax({
                        url: 'api/purchase_api/purchaseApiCopy',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        success: function (ret) {
                            layer.msg(ret.msg);
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                });
                //选择对应采购订单
                $(document).on("click", "#selectPurchase", function () {
                    var url = "api/purchase_api/index";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            // $("input[name='row[poItemNo]']").val(value.poItemNo);
                            $("input[name='row[poItemId]']").val(value.poItemId);
                        }
                    };
                    Fast.api.open(url,"采购订单选择",options);
                });
            
                // text文本框改变值功能
                $(document).on("keyup", "#suibian input[type='text']", function(e){
                    var change_val = $(this).val();
                    $(e.target).attr("value",change_val);
                });

                // 编辑颜色变化
                $(document).on("focus", "#suibian input", function(e){
                    $("#suibian tr").removeAttr("style");
                    $(this).parents("tr").css("background-color","#E0FFFF");
                });

                // 表头修改 
                var tableField = Config.tableField;
                var table_count = $("#suibian").find("tr").length;
                $("#count").text(table_count);
                sumWeight();
                sumPrice();
                function add_table(type=1,div_content=$("#suibian")){
                    var c_price = $("#c-C_SinglePrice").val();
                    var TD_Pressure = $("#c-TD_Pressure").val();
                    // 名称 要求 type 默认值
                    var son_arr = tableField;
                        
                    var field_append='<tr><td>0</td><td style="width:90px">'
                                +'<a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a><a href="javascript:;" class="btn btn-xs btn-success insert"><i class="fa fa-plus"></i></a></td>';
                    for(var i=0;i<son_arr.length;i++){
                        if(son_arr[i][0] == "OD_ID" || son_arr[i][0] == "draft_detail_id") field_append += '<td hidden><input type="'+son_arr[i][2]+'" '+son_arr[i][1]+' name="table_row['+son_arr[i][0]+'][]" value="'+son_arr[i][3]+'" class="small_input"></td>';
                        else if(son_arr[i][0] == "price_way") field_append += '<td><select class="form-control" name="table_row[price_way][]" autocomplete="off"><option value="1" selected="selected">按重量</option><option value="2">按数量</option></select></td>';
                        else if(son_arr[i][0] == "C_Price") field_append += '<td><input type="'+son_arr[i][2]+'" '+son_arr[i][1]+' name="table_row['+son_arr[i][0]+'][]" value="'+c_price+'" class="small_input"></td>';
                        else if(son_arr[i][0] == "OD_Pressure") field_append += '<td><input type="'+son_arr[i][2]+'" '+son_arr[i][1]+' name="table_row['+son_arr[i][0]+'][]" value="'+TD_Pressure+'" class="small_input"></td>';
                        else field_append += '<td><input type="'+son_arr[i][2]+'" '+son_arr[i][1]+' name="table_row['+son_arr[i][0]+'][]" value="'+son_arr[i][3]+'" class="small_input"></td>';
                    }
                    field_append += '</tr>';
                    if(type==1) div_content.append(field_append);
                    else div_content.before(field_append);
                    col_index();
                    var table_count = $("#suibian").find("tr").length;
                    $("#count").text(table_count);
                }
                $(document).on('click', "#table_add", function(){
                    add_table();
                    sumWeight();
                    sumPrice();
                });
                $(document).on('click', ".plus", function(){
                    $("#suibian tr").removeAttr("style");
                    var text;
                    var tbody_tr = $(this).parents("tr");
                    var copyChoose = tbody_tr.html();
                    text = '<tr style="background-color: #E0FFFF">'+copyChoose.replace(/name\=\"table_row\[OD\_ID\]\[\]\" value\=\"\d+\"/,'name="table_row[OD_ID][]" value="0"')+"</tr>";
                    $("#suibian").append(text);
                    col_index();
                    var table_count = $("#suibian").find("tr").length;
                    $("#count").text(table_count);
                    sumWeight();
                    sumPrice();
                });
        
                $(document).on('click', ".del", function(e){
                    $( e.target ).closest("tr").remove();
                    var table_count = $("#suibian").find("tr").length;
                    $("#count").text(table_count);
                    col_index();
                    var table_count = $("#suibian").find("tr").length;
                    $("#count").text(table_count);
                    sumWeight();
                    sumPrice();
                });

                $(document).on('click', ".insert", function(){
                    add_table(2, $(this).parents("tr"));
                    sumWeight();
                    sumPrice();
                });

                $(document).on("click", "#selectProject", function () {
                    var url = "chain/sale/compact/selectProject";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            // console.log(value);
                            // return false;
                            $("input[name='row[PC_Num]']").val(value.PC_Num);
                            $("input[name='row[C_Project]']").val(value.PC_ProjectName);
                            $("input[name='row[C_SortProject]']").val(value.PC_ShortName);
                            $("input[name='row[Customer_Name]']").val(value.C_Name);
                            $("input[name='row[C_SaleMan]']").val(value.PC_SalesMan);
                            $('select[name="row[C_SaleType]"]').selectpicker('val', value.ST_Name);
                            $('select[name="row[produce_type]"]').selectpicker('val', value.PC_type);
                            if(value.PC_type==3){
                                tableField[4][3] = '铁附件';
                                $("#c_sumprice").hide();
                                $("#table_thead th:nth-child(8)").text("*型号");
                            }else{
                                tableField[4][3] = Config.produce_type[value.PC_type];
                                $("#c_sumprice").show();
                                $("#table_thead th:nth-child(8)").text("*塔型");
                            }
                            // type_flag = value.PC_type;
                            $("#suibian").html("");
                            //下拉选中有问题
                            // $('select[name="row[C_SaleType]"]:checked').val(value.ST_Name)
                            // $("select[name='row[C_SaleType]'] option").attr("selected",false);
                            // $("select[name='row[C_SaleType]']").find("option:contains('"+value.ST_Name+"')").attr("selected",true);
                            // $("select[name='row[C_SaleType]']").value = value.ST_Name;
                        }
                    };
                    Fast.api.open(url,"工程目录选择框",options);
                });
                $(document).on("click", "#selectSale", function () {
                    var url = "chain/sale/project_cate_log/selectSaleMan";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("input[name='row[C_SaleMan]']").val(value.E_Name);
                        }
                    };
                    Fast.api.open(url,"员工选择",options);
                });

                function sumWeight(){
                    var totalRow = 0 ;
                    var find_field = 'td input[name="table_row[OD_SumWeight][]"]';
                    //if(type_flag==3) find_field = 'td input[name="table_row[OD_Count][]"]';
                    $('#suibian tr').each(function() { 
                        $(this).find(find_field).each(function(){
                            totalRow += isNaN(parseFloat($(this).val()))?0:parseFloat($(this).val()); 
                        }); 
                    });
                    if(isNaN(totalRow)) totalRow=0;
                    $("#sumWeightW").text(totalRow.toFixed(3));
                    $("#c-C_Sumweight").val(totalRow.toFixed(3));
                }
                function sumPrice(){
                    var totalRow = 0 ;
                    $('#suibian tr').each(function() { 
                        $(this).find('td input[name="table_row[C_Sumprice][]"]').each(function(){ 
                            totalRow += isNaN(parseFloat($(this).val()))?0:parseFloat($(this).val()); 
                        }); 
                    }); 
                    if(isNaN(totalRow)) totalRow=0;
                    $("#sumPriceP").text(totalRow.toFixed(3));
                    $("#c-C_Sumprice").val(totalRow.toFixed(3));
                }

                //新增部分
                // $(document).on('change', "#c-produce_type", function(){
                //     var select_text = $(this).find("option:selected").text();
                //     tableField[3][3] = select_text;
                //     if($(this).find("option:selected").val()==3) $("#c_sumprice").hide();
                //     else $("#c_sumprice").show();
                // });

                //选择草案
                $(document).on('click', "#selectDraft", function(){
                    var product_type_id = $("#c-produce_type").find("option:selected").val();
                    var product_type = $("#c-produce_type").find("option:selected").text();
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var c_price = $("#c-C_SinglePrice").val();
                            var TD_Pressure = $("#c-TD_Pressure").val();
                            tableField[12][3] = c_price;
                            tableField[5][3] = TD_Pressure;
                            tableField[4][3] = product_type;
                            var field_append="";
                            $.each(value,function(index,e){
                                field_append+='<tr><td>0</td><td style="width:90px">'
                                    +'<a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a><a href="javascript:;" class="btn btn-xs btn-success plus"><i class="fa fa-plus"></i></a></td>';
                                for(var i=0;i<tableField.length;i++){
                                    if(tableField[i][0] == "OD_ID" || tableField[i][0] == "draft_detail_id") field_append += '<td hidden><input type="'+tableField[i][2]+'" '+tableField[i][1]+' name="table_row['+tableField[i][0]+'][]" value="'+(typeof e[tableField[i][5]]!="undefined"?e[tableField[i][5]]:tableField[i][3])+'" class="small_input"></td>';
                                    else if(tableField[i][0] == "price_way") field_append += '<td><select class="form-control" name="table_row[price_way][]" autocomplete="off"><option value="1" selected="selected">按重量</option><option value="2">按数量</option></select></td>';
                                    else if(tableField[i][0] == "C_Sumprice") field_append += '<td><input type="'+tableField[i][2]+'" '+tableField[i][1]+' name="table_row['+tableField[i][0]+'][]" value="'+(typeof e["weight"]!="undefined"?(e["weight"]*c_price).toFixed(2):0)+'" class="small_input"></td>';
                                    else field_append += '<td><input type="'+tableField[i][2]+'" '+tableField[i][1]+' name="table_row['+tableField[i][0]+'][]" value="'+(typeof e[tableField[i][5]]!="undefined"?e[tableField[i][5]]:tableField[i][3])+'" class="small_input"></td>';
                                }
                                field_append += '</tr>';
                            });
                            $("table>tbody").append(field_append);
                            col_index();
                        }
                    };
                    Fast.api.open('chain/sale/draft/selectDraft/type/'+product_type_id,"导入草案",options);
                });

                $(document).on("click", "#selectSync", function () {
                    var single_price = $("#c-C_SinglePrice").val();
                    single_price = (single_price==''?0:parseFloat(single_price));
                    var tableContent = $("#suibian").find("tr");
                    $.each(tableContent,function(index,e){
                        var type = $(e).find("td select[name='table_row[price_way][]']").val();
                        var this_price = $(e).find("td input[name='table_row[C_Price][]']").val();
                        this_price = (this_price==''?0:parseFloat(this_price));
                        if(!this_price){
                            $(e).find("td input[name='table_row[C_Price][]']").val(single_price);
                            if(type==1){
                                var weight = $(e).find("td input[name='table_row[OD_SumWeight][]']").val();
                                weight = (weight==''?0:parseFloat(weight));
                                var sumPrice = (single_price*weight).toFixed(2);
                                $(e).find("td input[name='table_row[C_Sumprice][]']").val(sumPrice);
                            }else{
                                var count = $(e).find("td input[name='table_row[OD_Count][]']").val();
                                count = (count==''?0:parseFloat(count));
                                var sumPrice = (single_price*count).toFixed(2);
                                $(e).find("td input[name='table_row[C_Sumprice][]']").val(sumPrice);
                            }
                        }
                    });
                });
                $(document).on("click", "#author", function () {
                    var C_Num = $("#c-C_Num").val();
                    if(C_Num == false){
                        layer.confirm('请先保存！');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/sale/compact/auditor",C_Num);
                });
                $(document).on("click", "#giveup", function () {
                    var C_Num = $("#c-C_Num").val();
                    if(C_Num == false){
                        layer.confirm('请重试！');
                        return false;
                    }
                    check('确定弃审？',"chain/sale/compact/giveUp",C_Num);
                    
                });
                function check(msg,url,num){
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {C_Num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    // Layer.msg(ret.msg);
                                    if (ret.content == 1) {
                                        $('#giveup').removeAttr("disabled");
                                        $('.btn-success').attr("disabled",true);
                                        $('#author').attr("disabled",true);
                                    }else{
                                        $('#giveup').attr("disabled",true);
                                        $('.btn-success').removeAttr("disabled");
                                        $('#author').removeAttr("disabled");
                                    }
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
            
                
                $('#suibian').on('keyup','td input[name="table_row[OD_Count][]"]',function () {
                    var index = $(this).parents('tr').index();
                    var select = $(this).parents('tbody').find('tr').eq(index).find('td select[name="table_row[price_way][]"]').val();
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_Count][]"]').val().trim();
                    var strong = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_Weight][]"]').val().trim();
                    var money = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Price][]"]').val().trim();
                    num==''?num=0:'';
                    strong==''?strong=0:'';
                    money==''?money=0:'';
                    num = parseFloat(num);
                    strong = parseFloat(strong);
                    money = parseFloat(money);
                    var sumWeightValue = (num*strong).toFixed(3);
                    var sumMoneyValue = 0;
                    if(select==1) sumMoneyValue = (sumWeightValue*money).toFixed(2);
                    else sumMoneyValue = (num*money).toFixed(2);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_SumWeight][]"]').attr("value",sumWeightValue);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Sumprice][]"]').attr("value",sumMoneyValue);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_SumWeight][]"]').val(sumWeightValue);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Sumprice][]"]').val(sumMoneyValue);
                    sumWeight();
                    sumPrice();
                    
                })
                $('#suibian').on('keyup','td input[name="table_row[OD_Weight][]"]',function () {
                    var index = $(this).parents('tr').index();
                    var select = $(this).parents('tbody').find('tr').eq(index).find('td select[name="table_row[price_way][]"]').val();
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_Count][]"]').val().trim();
                    var strong = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_Weight][]"]').val().trim();
                    var money = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Price][]"]').val().trim();
                    num==''?num=0:'';
                    strong==''?strong=0:'';
                    money==''?money=0:'';
                    num = parseFloat(num);
                    strong = parseFloat(strong);
                    var sumWeightValue = (num*strong).toFixed(3);
                    var sumMoneyValue = 0;
                    if(select==1) sumMoneyValue = (sumWeightValue*money).toFixed(2);
                    else sumMoneyValue = (num*money).toFixed(2);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_SumWeight][]"]').attr("value",sumWeightValue);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Sumprice][]"]').attr("value",sumMoneyValue);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_SumWeight][]"]').val(sumWeightValue);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Sumprice][]"]').val(sumMoneyValue);
                    sumWeight();
                    sumPrice();
                })
                $('#suibian').on('keyup','td input[name="table_row[C_Price][]"]',function () {
                    var index = $(this).parents('tr').index();
                    var select = $(this).parents('tbody').find('tr').eq(index).find('td select[name="table_row[price_way][]"]').val();
                    var sumWeightValue = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_SumWeight][]"]').val().trim();
                    var money = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Price][]"]').val().trim();
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_Count][]"]').val().trim();
                    num==''?num=0:'';
                    sumWeightValue==''?sumWeightValue=0:'';
                    money==''?money=0:'';
                    num = parseFloat(num);
                    sumWeightValue = parseFloat(sumWeightValue);
                    money = parseFloat(money);
                    var sumMoneyValue = 0;
                    if(select==1) sumMoneyValue = (sumWeightValue*money).toFixed(2);
                    else sumMoneyValue = (num*money).toFixed(2);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Sumprice][]"]').attr("value",sumMoneyValue);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Sumprice][]"]').val(sumMoneyValue);
                    sumWeight();
                    sumPrice();
                    $("#c-C_SinglePrice").val(money);
                })
                $('#suibian').on('change','td select[name="table_row[price_way][]"]',function () {
                    var index = $(this).parents('tr').index();
                    var select = $(this).parents('tbody').find('tr').eq(index).find('td select[name="table_row[price_way][]"]').val();
                    var sumWeightValue = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_SumWeight][]"]').val().trim();
                    var money = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Price][]"]').val().trim();
                    var num = $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[OD_Count][]"]').val().trim();
                    num==''?num=0:'';
                    sumWeightValue==''?sumWeightValue=0:'';
                    money==''?money=0:'';
                    num = parseFloat(num);
                    sumWeightValue = parseFloat(sumWeightValue);
                    money = parseFloat(money);
                    var sumMoneyValue = 0;
                    if(select==1) sumMoneyValue = (sumWeightValue*money).toFixed(2);
                    else sumMoneyValue = (num*money).toFixed(2);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Sumprice][]"]').attr("value",sumMoneyValue);
                    $(this).parents('tbody').find('tr').eq(index).find('td input[name="table_row[C_Sumprice][]"]').val(sumMoneyValue);
                    sumWeight();
                    sumPrice();
                    $("#c-C_SinglePrice").val(money);
                })
            }
        }
    };
    return Controller;
});