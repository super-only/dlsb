define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree', 'hpbundle', 'qgddata','ui'], function ($, undefined, Backend, Table, Form, undefined) {

    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: [],
                height: 250,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'AD_ID', title: '请购明细ID'},
                        {field: 'AL_Num', title: '请购单号'},
                        {field: 'IM_Num', title: '存货编码'},
                        {field: 'sortNum', title: '存货类别'},
                        {field: 'IM_Class', title: '存货名称'},
                        {field: 'IM_Spec', title: '规格'},
                        {field: 'IR_Unit', title: '计量单位'},
                        {field: 'AD_Count', title: '数量'},
                        {field: 'AD_Memo', title: '备注'}
                    ]
                ]
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/applylist_jwl/index' + location.search,
                    add_url: 'chain/machine/applylist_jwl/add',
                    edit_url: 'chain/machine/applylist_jwl/edit',
                    del_url: 'chain/machine/applylist_jwl/del',
                    auditor_url: 'chain/machine/applylist_jwl/auditor',
                    giveup_url: 'chain/machine/applylist_jwl/giveUp',
                    multi_url: 'chain/machine/applylist_jwl/multi',
                    import_url: 'chain/machine/applylist_jwl/import',
                    table: 'applylist',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'AL_ID',
                sortName: 'AL_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                // showExport: false,
                height: 500,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'AL_ID', title: __('Al_id'),visible:false,operate: false},
                        {field: 'AL_Num', title: __('Al_num'), operate: 'LIKE'},
                        {field: 'AL_PurchaseType', title: __('Al_purchasetype'), searchList: Config.purchaseList},
                        {field: 'AL_ArriveDate', title: __('Al_arrivedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'DD_Num', title: __('Dd_num'), searchList: Config.deptList},
                        {field: 'AL_ApplyPepo', title: __('Al_applypepo'), operate: 'LIKE'},
                        {field: 'AL_WriteDate', title: __('Al_writedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'AL_ProjectName', title: '工程名称', operate: 'LIKE'},
                        // {field: 'al_c_num', title: '合同号', operate: 'LIKE'},
                        // {field: 'al_t_num', title: '任务单号', operate: 'LIKE'},
                        {field: 'AL_Writer', title: __('Al_writer'), operate: 'LIKE'},
                        {field: 'AL_WriterDate', title: __('Al_writerdate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Modifyer', title: '审核人', operate: 'LIKE'},
                        {field: 'ModifyTime', title: '审核时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var AL_Num = $element.AL_Num;
                if(AL_Num == '') return false;
                $.ajax({
                    url: 'chain/machine/applylist_jwl/requisitionDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:AL_Num},
                    success: function (ret) {
                        var content = [];
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $("#printDetail").click(function () {
                window.top.Fast.api.open('chain/machine/applylist_jwl/printDetail/ids/' + Config.ids, '原材料请购单', {
                    area: ["100%", "100%"]
                });
            });
            
            $(document).on("click", "#export", function () {
                window.open('/admin.php/chain/machine/applylist_jwl/export/ids/' + Config.ids);
            });
        },
        syncrequisition: function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/applylist_jwl/syncRequisition' + location.search,
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'AD_ID',
                sortName: 'AD_ID',
                search: false,
                // clickToSelect: true,
                // singleSelect: true,
                showToggle: false,
                // showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'AL_Num', title: '请购编号', operate: false},
                        {field: 'AD_ID', title: '请购ID', operate: 'LIKE'},
                        {field: 'IM_Num', title: '材料编号', operate: false},
                        {field: 'sortNum', title: '材料类型', searchList: Config.classList,formatter:function(value){
                            return Config.classList[value];
                        }},
                        {field: 'IR_Name', title: '材料名称', operate: 'LIKE'},
                        {field: 'IR_Spec', title: '材料规格', operate: 'LIKE'},
                        {field: 'IR_Unit', title: '单位', operate: false},
                        // {field: 'AD_Length', title: '长度(米)', operate:'=1000'},
                        // {field: 'AD_Width', title: '宽度(米)', operate: '=1000'},
                        {field: 'AD_Count', title: '请购数量', operate: false},
                        {field: 'AD_BuyCount', title: '采购数量', operate: false},
                        {field: 'sy_count', title: '剩余未采购数量', operate: false},
                        // {field: 'AD_Weight', title: '请购重量', operate: false},
                        // {field: 'AD_BuyWeight', title: '采购重量', operate: false},
                        // {field: 'sy_weight', title: '剩余未采购重量', operate: false},
                        {field: 'AL_Writer', title: '请购人', operate: 'LIKE'},
                        {field: 'AL_WriterDate', title: '请购时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var tableContent=table.bootstrapTable('getAllSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                $(document).on("click", "#selectAccpect", function () {
                    var url = "chain/purchase/requisition/selectOperator";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("input[name='row[al_reciever]']").val(value[0].id);
                            $("#al_reciever_name").val(value[0].username);
                        }
                    };
                    Fast.api.open(url,"接收人选择",options);
                });

                $(document).on('click', "#chooseMaterial", function(e){
                    var AL_ID = $("#AL_ID").val();
                    if(!AL_ID){
                        layer.msg("请先保存主信息！");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $.ajax({
                                url: 'chain/machine/applylist_jwl/addMaterial',
                                type: 'post',
                                dataType: 'json',
                                data: {data:JSON.stringify(value)},
                                success: function (ret) {
                                    var content = '';
                                    if(ret.code==1){
                                        content = ret.data;
                                    }
                                    $("#tbshow").append(content);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            })
                        }
                    };
                    Fast.api.open('chain/machine/wj_store_in/chooseMaterial',"选择材料",options);
                })
                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(1).find('input').val();
                    if(num != false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/machine/applylist_jwl/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                });
                $(document).on("click", "#auditor", function () {
                    if(Config.flag) return false;
                    check('审核之后无法修改，确定审核？',"chain/machine/applylist_jwl/auditor",Config.ids);
                });
                $(document).on("click", "#giveup", function () {
                    if(!Config.flag) return false;
                    check('确定弃审？',"chain/machine/applylist_jwl/giveUp",Config.ids);
                    
                });
                function check(msg,url,num){
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {ids: num},
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    Layer.msg(ret.msg);
                                    if (ret.code === 1) {
                                        window.location.reload();
                                    }
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                };

                // var kj_limberList = Config.kj_limberList;
                // $('#tbshow').on('keyup','td input[name="table_row[L_Name][]"]',function () {
                //     var text_count = $(this).parents('tr').find("input[name='table_row[L_Name][]']").val().trim();
                //     if(kj_limberList[text_count] != undefined ) $(this).parents('tr').find("input[name='table_row[L_Name][]']").val(kj_limberList[text_count])
                // })

            }
        }
    };
    return Controller;
});