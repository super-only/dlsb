define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree', 'hpbundle', 'jwlrkdata'], function ($, undefined, Backend, Table, Form, undefined) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/wj_store_in/index' + location.search,
                    add_url: 'chain/machine/wj_store_in/add',
                    edit_url: 'chain/machine/wj_store_in/edit',
                    // del_url: 'chain/machine/wj_store_in/del',
                    multi_url: 'chain/machine/wj_store_in/multi',
                    import_url: 'chain/machine/wj_store_in/import',
                    table: 'wjstorein',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'RK_ID',
                sortName: 'RK_Date',
                search: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'RK_Date', title: __('Rk_date'), defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD"},
                        {field: 'IR_Num', title: __('Wjstoreindetail.ir_num'), operate: false},
                        {field: 'sortNum', title: '材料', searchList: Config.sortList},
                        {field: 'wir.IR_Num', title: __('Wjstoreindetail.ir_num'), operate: 'LIKE', visible: false},
                        {field: 'IR_Name', title: '存货名称', operate: 'LIKE'},
                        {field: 'IR_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'IR_Unit', title: '计量单位', operate: 'LIKE'},
                        {field: 'RKDe_Count', title: __('Wjstoreindetail.rkde_count'), operate:false},
                        {field: 'RKDe_NotaxPrice', title: __('Wjstoreindetail.rkde_notaxprice'), operate:false},
                        {field: 'RKDe_NotaxMoney', title: __('Wjstoreindetail.rkde_notaxmoney'), operate:false},
                        {field: 'RKDe_Price', title: __('Wjstoreindetail.rkde_price'), operate:false},
                        {field: 'RKDe_Money', title: __('Wjstoreindetail.rkde_money'), operate:false},
                        {field: 'v.V_Name', title: '供应商', operate: 'LIKE', visible: false},
                        {field: 'WC_Num', title: '仓库', searchList:Config.warehouse_list, visible: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            $("#print").click(function () {
                window.top.Fast.api.open('chain/machine/wj_store_in/print/ids/' + Config.ids, '进货单', {
                    area: ["100%", "100%"]
                });
            });
            Controller.api.bindevent("edit");
            $(document).on("click", "#offerU", function () {
                $.ajax({
                    url: 'chain/machine/wj_store_in/offerU',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {id: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        print: function() {
            let row = Config.row;
            let list = Config.list;
            let wslist = Config.wslist;
            
            let tb_tmp = [];
            let countSum = 0;
            let jeSum = 0;
            for(let i=0;i<list.length;i++){
                countSum += list[i]['RKDe_Count'];
                
                let tmp={
                    'chmc':list[i]['IR_Name'],
                    'gg':list[i]['IR_Spec'],
                    'dw':list[i]['IR_Unit'].trim(),
                    'sl':list[i]['RKDe_Count'],
                    'dj':list[i]['RKDe_NotaxPrice'],
                    'je':list[i]['RKDe_NotaxMoney'],
                    'bz':list[i]['RKDe_Memo']
                };
                jeSum += Number(tmp.je);
                tb_tmp.push(tmp)
            }

            const pageSize = 10;
            let eptNum = pageSize-list.length%pageSize;
            if(eptNum!=pageSize){
                for(let i=0;i<(eptNum-1);i++){
                    tb_tmp.push({
                        'chmc':'',
                        'gg':'',
                        'dw':'',
                        'sl':'',
                        'dj':'',
                        'je':'',
                        'bz':'',
                    });
                }
            }

            let tmp2={
                'chmc':'合 计：',
                'gg':'',
                'dw':'',
                'sl':countSum,
                'dj':'',
                'je':jeSum.toFixed(2),
            };
            
            tb_tmp.push(tmp2);
            

            let data_tmp={
                rkdh:row['RK_Num'],
                rkrq:row['RK_Date'].split(' ')[0],
                ck:wslist[row['WC_Num']],
                ghdw:row['V_Name'],
                bm:row['RK_Department'],
                ywy:row['RK_Operator'],
                dhrq:row['RK_DHDate'],
                zdr: row['Writer'],
                zdrq: row['WriteDate'].split(' ')[0],
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){
                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: jwlrkdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            pageto('#p_mx1',0);
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
        },
        // choosematerial: function () {
        //     var height = document.body.clientHeight/2;
        //     $("#chooseTable").bootstrapTable({
        //         data: [],
        //         height: height-100,
        //         search: false,
        //         pagination: false,
        //         columns: [
        //             [
        //                 {field: 'operate', title: '删除',
        //                     formatter: function(value,row,index){return '<button class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></button>';}
        //                 },
        //                 {field: 'IR_Num', title: '存货编码'},
        //                 {field: 'IR_Name', title: '存货名称'},
        //                 {field: 'IR_Spec', title: '规格'},
        //                 {field: 'IR_Unit', title: '计量单位'},
        //                 {field: 'sortNum', title: '所属类别'},
        //                 {field: 'IR_TaxRate', title: '税率'}
        //             ]
        //         ]
        //     });

        //     $(document).on('click', ".del", function(e){
        //         var num = $(this).parents("tr").find('td').eq(1).text();
        //         $("#chooseTable").bootstrapTable('remove', {field:"IR_Num",values:[num]});
        //     });
        //     Table.api.bindevent($("#chooseTable"));
        //     $('#treeview').jstree({
        //         'core' : {
        //                 'data' : {
        //                 'url' : 'chain/machine/wj_inventory_sort/companytree',
        //                 'data' : function (node) {
        //                     return { 'id' : node.id };
        //                 }
        //             }
        //         }
        //     });
        //     // 初始化表格参数配置
        //     Table.api.init({
        //         extend: {
        //             index_url: 'chain/machine/wj_store_in/chooseMaterial' + location.search,
        //             table: 'WjInventoryRecord',
        //         }
        //     });

        //     var table = $("#table");

        //     // 初始化表格
        //     table.bootstrapTable({
        //         url: $.fn.bootstrapTable.defaults.extend.index_url,
        //         pk: 'IR_Num',
        //         sortName: 'IR_Num',
        //         sortOrder: 'ASC',
        //         search: false,
        //         showToggle: false,
        //         showExport: false,
        //         height:height+40,
        //         pagination: false,
        //         limit: 10000,
        //         searchFormVisible: true,
        //         clickToSelect: true,
        //         singleSelect: true,
        //         columns: [
        //             [
        //                 {checkbox: true},
        //                 {field: 'IR_Num', title: '存货编码', operate: 'LIKE'},
        //                 {field: 'IR_Name', title: '存货名称',operate: 'LIKE'},
        //                 {field: 'IR_Spec', title: '规格', operate: 'LIKE'},
        //                 {field: 'IR_Unit', title: '计量单位', operate: false},
        //                 {field: 'sortNum', title: '所属类别', operate:false},
        //                 {field: 'IR_TaxRate', title: '税率', operate: false}
        //             ]
        //         ]
        //     });


        //     // 为表格绑定事件
        //     Table.api.bindevent(table);
        //     $('#treeview').on('changed.jstree', function (e, data) {
        //         var dd_num = data.selected[0];
        //         var options = table.bootstrapTable('getOptions');
        //         options.pageNumber = 1;
        //         options.queryParams = function (params) {
        //             return {
        //                 search: params.search,
        //                 sort: params.sort,
        //                 order: params.order,
        //                 offset: params.offset,
        //                 limit: params.limit,
        //                 filter: JSON.stringify({'IR_Num': dd_num}),
        //                 op: JSON.stringify({'IR_Num': 'LIKE %'}),
        //             };
        //         };
        //         table.bootstrapTable('refresh', {});
        //         return false;
                
        //     }).jstree();

        //     table.on("dbl-click-row.bs.table",function(row, $element){
        //         $("#chooseTable").bootstrapTable("append",$element);
        //     });

        //     $(document).on("click", "#sure", function () {
        //         var tableContent = $("#chooseTable").bootstrapTable('getData');
        //         if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
        //         else Fast.api.close(tableContent);
        //     });
        // },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                $(document).on('click', "#chooseMaterial", function(e){
                    let v_num = $("#c-V_Num").val();
                    if(!v_num){
                        layer.msg("请先选择供货单位!")
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var tableField = Config.tableField;
                            var content = "";
                            var col = $("#tbshow>tr").length;
                            $.each(value,function(v_index,v_e){
                                col = col+1;
                                content += '<tr><td>'+col+'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                $.each(tableField,function(t_index,e){
                                    content += '<td '+e[4]+'><input type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?e[3]:v_e[e[1]])+'"></td>';
                                });
                                content += "</tr>";
                            });
                            $("#tbshow").append(content);
                        }
                    };
                    Fast.api.open('chain/machine/materialnote_jwl/chooseMaterial/v_num/'+v_num,"选择材料",options);
                })
                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[RKDe_ID][]"]').val();
                    if(num != false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/machine/wj_store_in/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                        reSort();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                        reSort();
                    }
                });
                $(document).on('click', ".ghdw", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#c-V_Num").val(value.V_Num);
                            $("#c-V_Name").val(value.V_Name);
                        }
                    };
                    Fast.api.open('chain/material/material_note/chooseVendor',"供应商选择",options);
                });
                $('#tbshow').on('keyup','td input[name="table_row[RKDe_NotaxPrice][]"]',function () {
                    var index = $(this).parents('tr').index();
                    var RKDe_NotaxPrice = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_NotaxPrice][]"]').val().trim();
                    var num = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_Count][]"]').val().trim();
                    var fax = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_TaxRate][]"]').val().trim();
                    RKDe_NotaxPrice==''?RKDe_NotaxPrice=0:'';
                    fax==''?fax=0:'';
                    num==''?num=0:'';
                    RKDe_NotaxPrice = parseFloat(RKDe_NotaxPrice);
                    fax = parseFloat(fax);
                    num = parseFloat(num);
                    var RKDe_Price = (RKDe_NotaxPrice*(1+fax)).toFixed(2);
                    var RKDe_Money = (RKDe_Price*num).toFixed(2);
                    var RKDe_NotaxMoney = (RKDe_NotaxPrice*num).toFixed(2);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_Price][]"]').val(RKDe_Price);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_Money][]"]').val(RKDe_Money);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_NotaxMoney][]"]').val(RKDe_NotaxMoney);
                })
                $('#tbshow').on('keyup','td input[name="table_row[RKDe_Price][]"]',function () {
                    var index = $(this).parents('tr').index();
                    var RKDe_Price = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_Price][]"]').val().trim();
                    var num = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_Count][]"]').val().trim();
                    var fax = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_TaxRate][]"]').val().trim();
                    RKDe_Price==''?RKDe_Price=0:'';
                    fax==''?fax=0:'';
                    num==''?num=0:'';
                    RKDe_Price = parseFloat(RKDe_Price);
                    fax = parseFloat(fax);
                    num = parseFloat(num);
                    var RKDe_NotaxPrice = (RKDe_Price/(1+fax)).toFixed(2);
                    var RKDe_Money = (RKDe_Price*num).toFixed(2);
                    var RKDe_NotaxMoney = (RKDe_NotaxPrice*num).toFixed(2);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_NotaxPrice][]"]').val(RKDe_NotaxPrice);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_Money][]"]').val(RKDe_Money);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_NotaxMoney][]"]').val(RKDe_NotaxMoney);
                })
                $('#tbshow').on('keyup','td input[name="table_row[RKDe_NotaxMoney][]"]',function () {
                    var index = $(this).parents('tr').index();
                    var RKDe_NotaxMoney = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_NotaxMoney][]"]').val().trim();
                    var num = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_Count][]"]').val().trim();
                    var fax = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_TaxRate][]"]').val().trim();
                    RKDe_NotaxMoney==''?RKDe_NotaxMoney=0:'';
                    fax==''?fax=0:'';
                    num==''?num=0:'';
                    RKDe_NotaxMoney = parseFloat(RKDe_NotaxMoney);
                    fax = parseFloat(fax);
                    num = parseFloat(num);
                    var RKDe_NotaxPrice = num?(RKDe_NotaxMoney/num).toFixed(2):0;
                    var RKDe_Price = (RKDe_NotaxPrice*(1+fax)).toFixed(2);
                    var RKDe_Money = (RKDe_Price*num).toFixed(2);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_Price][]"]').val(RKDe_Price);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_NotaxPrice][]"]').val(RKDe_NotaxPrice);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[RKDe_Money][]"]').val(RKDe_Money); 
                })
                $(document).on("click", "#author", function () {
                    var num = Config.ids;
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/machine/wj_store_in/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = Config.ids;
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/machine/wj_store_in/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
                function reSort()
                {
                    var col = 0;
                    $("#tbshow").find("tr").each(function () {
                        col++;
                        $(this).children('td').eq(0).text(col);
                    });
                }
                $(document).on('click', "#allDel", function(env){
                    var num = Config.ids;
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/machine/wj_store_in/allDel',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        parent.location.reload();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        layer.msg("删除失败");
                    }
                })
            }
        }
    };
    return Controller;
});