define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/con_statements/index' + location.search,
                    import_url: 'chain/machine/con_statements/import',
                    // table: 'wjstore',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                // sortName: 'S_ID',
                search: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'S_ID', title: __('S_id')},
                        {field: 'DJ_Num', title: '单据编码', operate: false},
                        {field: 'DJ_Date', title: "发生日期", defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD"},
                        {field: 'WC_Num', title: "仓库", searchList: Config.warehouse_list, visible: false},
                        {field: 'Department', title: "相关部门", operate: 'LIKE'},
                        {field: 'WJ_Sort', title: "进出类别", operate: false},
                        {field: 'IR_Num', title: "存货编码", operate: 'LIKE'},
                        {field: 'IR_Name', title: "存货名称", operate: false},
                        {field: 'Sort', title: "存货类别", operate: 'LIKE', visible: false},
                        {field: 'IR_Spec', title: "规格", operate:false},
                        {field: 'IR_Unit', title: "计量单位", operate:false},
                        {field: 'RKDe_Count', title: "入库数量", operate:false},
                        {field: 'RKDe_NotaxPrice', title: "入库单价(元)", operate:false},
                        {field: 'RKDe_NotaxMoney', title: "入库金额(元)", operate:false},
                        {field: 'CKDe_Count', title: "出库数量", operate:false},
                        {field: 'ckde_PriceNoTax', title: "出库单价(元)", operate: false},
                        {field: 'ckde_MoneyNoTax', title: "出库金额(元)", operate:false},
                        {field: 'memo', title: "摘要", operate:false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});