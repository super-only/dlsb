define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree', 'hpbundle', 'jwlckdata'], function ($, undefined, Backend, Table, Form, undefined, undefined, undefined) {
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/wj_store_out/index' + location.search,
                    add_url: 'chain/machine/wj_store_out/add',
                    edit_url: 'chain/machine/wj_store_out/edit',
                    // del_url: 'chain/machine/wj_store_out/del',
                    multi_url: 'chain/machine/wj_store_out/multi',
                    import_url: 'chain/machine/wj_store_out/import',
                    table: 'wjstoreout',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'CK_ID',
                sortName: 'CK_Date',
                search: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CK_Date', title: __('Ck_date'), defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime,datetimeFormat:"YYYY-MM-DD"},
                        {field: 'IR_Num', title: '存货编码', operate: false},
                        {field: 'wir.IR_Num', title: '存货编码', operate: 'LIKE',visible: false},
                        {field: 'sortNum', title: '材料', searchList: Config.sortList, formatter:function(value,row,index){
                            return Config.sortList[value];
                        }},
                        {field: 'IR_Name', title: '存货名称', operate: 'LIKE'},
                        {field: 'IR_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'IR_Unit', title: '计量单位', operate: 'LIKE'},
                        {field: 'CKDe_Count', title: '领用数量', operate:false},
                        {field: 'ckde_PriceNoTax', title: '不含税单价', operate:false},
                        {field: 'ckde_MoneyNoTax', title: '不含税金额', operate:false},
                        {field: 'CK_TakeDept', title: '领用部门', searchList: Config.deptList},
                        {field: 'WC_Num', title: '仓库', searchList:Config.warehouse_list, visible: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            $("#print").click(function () {
                window.top.Fast.api.open('chain/machine/wj_store_out/print/ids/' + Config.ids, '出库单', {
                    area: ["100%", "100%"]
                });
            });
            Controller.api.bindevent("edit");
            $(document).on("click", "#offerU", function () {
                $.ajax({
                    url: 'chain/machine/wj_store_out/offerU',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {id: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        choosematerial: function () {
            var height = document.body.clientHeight/2;
            $("#chooseTable").bootstrapTable({
                data: [],
                height: height-100,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'operate', title: '删除',
                            formatter: function(value,row,index){return '<button class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></button>';}
                        },
                        {field: 'IR_Num', title: '存货编码'},
                        {field: 'IR_Name', title: '存货名称'},
                        {field: 'sortNum', title: '存货类别',visible: false},
                        {field: 'IR_Spec', title: '规格'},
                        {field: 'IR_Unit', title: '计量单位'},
                        {field: 'S_Count', title: '库存数量'},
                        {field: 'S_Price', title: '库存均价(元)'},
                        {field: 'S_LastPrice', title: '上月结存均价'},
                        {field: 'S_Money', title: '库存金额(元)'},
                        {field: 'WC_Num', title: '仓库',visible: false},
                        {field: 'IR_TaxRate', title: '税率', operate: false, visible: false}
                    ]
                ]
            });

            $(document).on('click', ".del", function(e){
                var num = $(this).parents("tr").find('td').eq(1).text();
                $("#chooseTable").bootstrapTable('remove', {field:"IR_Num",values:[num]});
            });
            Table.api.bindevent($("#chooseTable"));
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'chain/machine/wj_inventory_sort/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/wj_store_out/chooseMaterial/wc_num/' + Config.wc_num + location.search,
                    table: 'WjStore',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'IR_Num',
                sortName: 'IR_Num',
                sortOrder: 'ASC',
                search: false,
                showToggle: false,
                showExport: false,
                height:height+40,
                pagination: false,
                limit: 10000,
                searchFormVisible: true,
                clickToSelect: true,
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'IR_Num', title: '存货编码', operate: false},
                        {field: 'ws.IR_Num', title: '存货编码', operate: 'LIKE', visible: false},
                        {field: 'IR_Name', title: '存货名称',operate: 'LIKE'},
                        {field: 'sortNum', title: '存货类别',operate: false},
                        {field: 'IR_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'IR_Unit', title: '计量单位', operate: false},
                        {field: 'S_Count', title: '库存数量', operate:false},
                        {field: 'S_Price', title: '库存均价(元)', operate: false},
                        {field: 'S_LastPrice', title: '上月结存均价', operate: false},
                        {field: 'S_Money', title: '库存金额(元)', operate: false},
                        {field: 'WC_Num', title: '仓库', operate: false, visible: false},
                        {field: 'IR_TaxRate', title: '税率', operate: false, visible: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    return {
                        search: params.search,
                        sort: params.sort,
                        order: params.order,
                        offset: params.offset,
                        limit: params.limit,
                        filter: JSON.stringify({'sortNum': dd_num}),
                        op: JSON.stringify({'sortNum': 'LIKE %'}),
                    };
                };
                table.bootstrapTable('refresh', {});
                return false;
                
            }).jstree();

            table.on("dbl-click-row.bs.table",function(row, $element){
                $("#chooseTable").bootstrapTable("append",$element);
            });

            $(document).on("click", "#sure", function () {
                var tableContent = $("#chooseTable").bootstrapTable('getData');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        print: function() {
            console.log('row', Config.row);
            console.log('list', Config.list);
            
            let row = Config.row;
            let list = Config.list;
            let wslist = Config.wslist;
            
            let tb_tmp = [];
            let countSum = 0;
            let jeSum = 0;
            for(let i=0;i<list.length;i++){
                countSum += list[i]['CKDe_Count'];
                jeSum += list[i]['CKDe_Money']*100;
                let tmp={
                    'chbm': list[i]['IR_Num'],
                    'chmc':list[i]['IR_Name'],
                    'gg':list[i]['IR_Spec'],
                    'dw':list[i]['IR_Unit'],
                    'sl':list[i]['CKDe_Count'],
                    'dj':list[i]['CKDe_Price'],
                    'je':list[i]['CKDe_Money'],
                };
                tb_tmp.push(tmp)
            }

            let tmp2={
                'chbm': '',
                'chmc':'合计：',
                'gg':'',
                'dw':'',
                'sl':countSum,
                'dj':'',
                'je':jeSum/100,
            };
            
            tb_tmp.push(tmp2);
            

            let data_tmp={
                ckdh:row['CK_Num'],
                ckrq:row['CK_Date'].split(' ')[0],
                ck:wslist[row['WC_Num']],
                cklb: row['CK_CKSort'],
                lyr: row['CK_TakeMan'],
                lybm: row['CK_TakeDept'],
                lybz: row['CK_BanZu'],
                bz: row['CK_Memo'],
                gc: row['CK_Project'],
                zdr: row['Writer'],
                zdrq: row['WriteDate'].split(' ')[0],
                shr: row['Approver'],
                shrq: row['ApproveDate'].split(' ')[0],
                pzr: row['Assessor'],
                pzrq: row['AssessDate'].split(' ')[0],
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){
                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: jwlckdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            pageto('#p_mx1',0);
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                $(document).on('click', ".lyr", function(e){
                    var url = "chain/sale/project_cate_log/selectSaleMan";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#c-CK_TakeMan").val(value.E_Name);
                        }
                    };
                    Fast.api.open(url,"选择领用人",options);
                })
                $(document).on('click', "#chooseMaterial", function(e){
                    var wc_num = $("#c-WC_Num").val();
                    if(!wc_num) layer.msg("请先选择仓库！");
                    else{
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                var tableField = Config.tableField;
                                var content = "";
                                var col = $("#tbshow>tr").length;
                                $.each(value,function(v_index,v_e){
                                    col = col+1;
                                    content += '<tr><td>'+col+'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                    $.each(tableField,function(t_index,e){
                                        content += '<td '+e[4]+'><input type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?e[3]:v_e[e[1]])+'"></td>';
                                    });
                                    content += "</tr>";
                                });
                                $("#tbshow").append(content);
                            }
                        };
                        Fast.api.open('chain/machine/wj_store_out/chooseMaterial/wc_num/'+wc_num,"选择存货",options);
                    }
                })
                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[CKDe_ID][]"]').val();
                    if(num != false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/machine/wj_store_out/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                        reSort();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                        reSort();
                    }
                });
                $(document).on("click", "#author", function () {
                    var num = Config.ids;
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/machine/wj_store_out/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = Config.ids;
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/machine/wj_store_out/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
                function reSort()
                {
                    var col = 0;
                    $("#tbshow").find("tr").each(function () {
                        col++;
                        $(this).children('td').eq(0).text(col);
                    });
                }
                $(document).on('click', "#allDel", function(env){
                    var num = Config.ids;
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/machine/wj_store_out/allDel',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        parent.location.reload();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        layer.msg("删除失败");
                    }
                })
                $('#tbshow').on('keyup','td input[name="table_row[CKDe_Price][]"]',function () {
                    var index = $(this).parents('tr').index();
                    var CKDe_Price = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[CKDe_Price][]"]').val().trim();
                    var num = $("#tbshow").find('tr').eq(index).find('td input[name="table_row[CKDe_Count][]"]').val().trim();
                    CKDe_Price==''?CKDe_Price=0:'';
                    num==''?num=0:'';
                    CKDe_Price = parseFloat(CKDe_Price);
                    num = parseFloat(num);
                    var CKDe_Money = (CKDe_Price*num).toFixed(2);
                    $("#tbshow").find('tr').eq(index).find('td input[name="table_row[CKDe_Money][]"]').val(CKDe_Money);
                })
            }
        }
    };
    return Controller;
});