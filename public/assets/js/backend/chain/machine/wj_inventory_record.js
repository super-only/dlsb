define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {
    var Controller = {
        index: function () {
            //修改顶部弹窗大小
            $(".btn-add").data("area",["80%","80%"]);
            $(".btn-edit").data("area",["80%","80%"]);

            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'chain/machine/wj_inventory_sort/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/wj_inventory_record/index' + location.search,
                    add_url: 'chain/machine/wj_inventory_record/add',
                    edit_url: 'chain/machine/wj_inventory_record/edit',
                    del_url: 'chain/machine/wj_inventory_record/del',
                    multi_url: 'chain/machine/wj_inventory_record/multi',
                    import_url: 'chain/machine/wj_inventory_record/import',
                    table: 'wjinventoryrecord',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'IR_Num',
                sortName: 'IR_Num',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'IR_Num', title: __('Ir_num'), operate: 'LIKE'},
                        {field: 'IR_Name', title: __('Ir_name'), operate: 'LIKE'},
                        {field: 'IR_Spec', title: __('Ir_spec'), operate: 'LIKE'},
                        {field: 'py', title: '拼音编码', operate: false},
                        {field: 'sortNum', title: __('Sortnum'), operate: false},
                        {field: 'IR_Unit', title: __('Ir_unit'), operate: 'LIKE'},
                        {field: 'IR_TaxRate', title: __('Ir_taxrate'), operate:'BETWEEN'},
                        {field: 'IR_Symbol', title: __('Ir_symbol'), operate: 'LIKE'},
                        {field: 'IR_PricePlan', title: __('Ir_priceplan'), operate:'BETWEEN'},
                        {field: 'IR_Proportion', title: __('Ir_proportion'), operate:'BETWEEN'},
                        {field: 'IR_LowAmount', title: __('Ir_lowamount'), operate:'BETWEEN'},
                        {field: 'IR_HighAmount', title: __('Ir_highamount'), operate:'BETWEEN'},
                        {field: 'IR_ifCheck', title: __('Ir_ifcheck')},
                        {field: 'Writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'WriteDate', title: __('Writedate')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table',function(){
                $(".btn-editone").data("area",["80%","80%"]);
            });
            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.queryParams = function(op){
                    op.tree = dd_num;
                    return op;
                }
                table.bootstrapTable('refresh',{})
                Table.api.init({
                    extend: {
                        index_url: 'chain/machine/wj_inventory_record/index' + location.search,
                        add_url: 'chain/machine/wj_inventory_record/add?tree='+dd_num,
                        edit_url: 'chain/machine/wj_inventory_record/edit',
                        del_url: 'chain/machine/wj_inventory_record/del',
                        multi_url: 'chain/machine/wj_inventory_record/multi',
                        import_url: 'chain/machine/wj_inventory_record/import',
                        table: 'wjinventoryrecord'
                    }
                });
                
            }).jstree();
        },
        add: function () {
            Controller.api.bindevent();
            $(document).on("change", "#c-sortNum", function () {
                var text = $("#c-sortNum").val();
                $.ajax({
                    url: "chain/machine/wj_inventory_record/stockTypeCode",
                    type: 'post',
                    data: {value: text},
                    dataType: 'json',
                    success: function (ret) {
                        $("#c-IR_Num").val(ret.data);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    parent.location.reload();
                });
            }
        }
    };
    return Controller;
});