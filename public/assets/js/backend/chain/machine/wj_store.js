define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/wj_store/index' + location.search,
                    // add_url: 'chain/machine/wj_store/add',
                    // edit_url: 'chain/machine/wj_store/edit',
                    // del_url: 'chain/machine/wj_store/del',
                    // multi_url: 'chain/machine/wj_store/multi',
                    import_url: 'chain/machine/wj_store/import',
                    table: 'wjstore',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'S_ID',
                // sortName: 'S_ID',
                search: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'S_ID', title: __('S_id')},
                        {field: 'm.WC_Num', title: '仓库', searchList: Config.warehouse_list,visible: false},
                        {field: 'WC_Name', title: '仓库', operate: false},
                        {field: 'IS_Name', title: "存货类别", operate: 'LIKE'},
                        {field: 'IR_Num', title: "存货编码", operate: false},
                        {field: 'wir.IR_Num', title: "存货编码", operate: 'LIKE', visible: false},
                        {field: 'IR_Name', title: __('存货名称'), operate: 'LIKE'},
                        {field: 'IR_Spec', title: __('规格'), operate: 'LIKE'},
                        {field: 'IR_Unit', title: __('计量单位'), operate: false},
                        {field: 'S_Count', title: __('S_count'), operate:false},
                        {field: 'S_Money', title: '含税金额', operate:false},
                        {field: 'S_No_Money', title: '不含税金额', operate:false},
                        {field: 'S_LastCount', title: __('S_lastcount'), operate:false},
                        {field: 'S_LastPrice', title: __('S_lastprice'), operate:false},
                        {field: 'S_LastMoney', title: __('S_lastmoney'), operate:false},
                        {field: 'S_LastDate', title: __('S_lastdate'), operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});