define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree','input-tag'], function ($, undefined, Backend, Table, Form, undefined, InputTag) {

    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: [],
                height: 250,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'MGN_ID', title: '到货明细ID'},
                        {field: 'IM_Num', title: '材料编码'},
                        {field: 'IR_Name', title: '材料名称'},
                        {field: 'sortNum', title: '材料类别',formatter: function(value){
                            return Config.wjClassList[value];
                        }},
                        {field: 'IR_Spec', title: '材料规格'},
                        // {field: 'MGN_Length', title: '到货长度(m)'},
                        // {field: 'MGN_Width', title: '到货宽度(m)'},
                        {field: 'MGN_Count', title: '实际到货数量'},
                        // {field: 'MGN_Weight', title: '实际到货重量(kg)'},
                        // {field: 'mgn_price', title: '到货单件(元/kg)'},
                        {field: 'MGN_Destination', title: '到货去向'},
                        // {field: 'mgn_testnum', title: '试验编号'},
                        {field: 'MGN_Maker', title: '生产厂家'},
                        {field: 'MGN_Memo', title: '备注'},
                        // {field: 'mgn_DuiFangDian', title: '堆放点'}
                    ]
                ]
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/materialnote_jwl/index' + location.search,
                    add_url: 'chain/machine/materialnote_jwl/add',
                    edit_url: 'chain/machine/materialnote_jwl/edit',
                    del_url: 'chain/machine/materialnote_jwl/del',
                    multi_url: 'chain/machine/materialnote_jwl/multi',
                    import_url: 'chain/machine/materialnote_jwl/import',
                    table: 'materialnote_jwl',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'MN_ID',
                sortName: 'WriteTime',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                // showExport: false,
                height: 500,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'MN_Num', title: __('Mn_num'), operate: 'LIKE'},
                        {field: 'MN_ID', title: __('Mn_id'), operate: false,visible:false},
                        // {field: 'V_Num', title: __('V_num'), operate: false},
                        {field: 'V_Name', title: __('V_num'), operate: 'LIKE'},
                        {field: 'MN_Date', title: __('Mn_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'ST_Name', title: __('St_num'), operate: 'LIKE'},
                        {field: 'mn_delivernum', title: __('Mn_delivernum'), operate: 'LIKE'},
                        {field: 'mn_license', title: __('Mn_license'), operate: 'LIKE'},
                        {field: 'Writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'WriteTime', title: __('Writetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'MN_Company', title: __('Mn_company'), operate: 'LIKE'},
                        {field: 'MN_Charger', title: __('Mn_charger'), operate: 'LIKE'},
                        {field: 'MN_Operater', title: __('Mn_operater'), operate: false},
                        {field: 'is_check', title: '状态', searchList:{1:"未审核",2:"已审核"}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var MN_Num = $element.MN_Num;
                if(MN_Num == '') return false;
                $.ajax({
                    url: 'chain/machine/materialnote_jwl/requisitionDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:MN_Num},
                    success: function (ret) {
                        var content = [];
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
        },
        add: function () {
            Controller.api.bindevent("add");
            $('#V_Name').focus(function(){
                $(".box").show();
            });
            $('#V_Name').blur(function(e){
                $(document).on('click', "#table-1 tbody tr", function(e){
                    var vendor_id = $(this).find("td").eq(0)[0].innerText;
                    var vendor_name = $(this).find("td").eq(1)[0].innerText;
                    $("#V_Num").val(vendor_id);
                    $("#V_Name").val(vendor_name);
                    $(".box").hide();
                });
            })
            $("#V_Name").on('keyup', function () {
                var table1 = $("#table-1");
                var input = $(this);
                Backend.search(table1, input);
                
            })
            
            $(document).on('click', "#table-1 tbody tr", function(e){
                var vendor_id = $(this).find("td").eq(0)[0].innerText;
                var vendor_name = $(this).find("td").eq(1)[0].innerText;
                $("#V_Num").val(vendor_id);
                $("#V_Name").val(vendor_name);
                $(".box").hide();
            });

        },
        edit: function () {
            Controller.api.bindevent("field");
        },
        choosematerial: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/materialnote_jwl/chooseMaterial/v_num/'+Config.v_num+ location.search,
                    table: 'materialnote_jwl',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'WriteTime',
                sortName: 'WriteTime',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'MN_Num', title: '到货单号', operate: 'LIKE'},
                        {field: 'DHID', title: '到货ID', operate: 'LIKE'},
                        {field: 'IR_Num', title: '存货编码', operate: 'LIKE'},
                        {field: 'sortNum', title: '存货类别', searchList: Config.wjClassList, formatter: function(value){
                            return Config.wjClassList[value];
                        }},
                        {field: 'IR_Name', title: '存货名称', operate: 'LIKE'},
                        {field: 'IR_Spec', title: '存货规格', operate: 'LIKE'},
                        {field: 'IR_Unit', title:'单位', operate: 'LIKE'},
                        {field: 'RKDe_Count', title: '入库数量', operate: false},
                        {field: 'RKDe_NotaxPrice', title: '不含税单价', operate: false},
                        {field: 'RKDe_NotaxMoney', title: '不含税金额', operate: false},
                        {field: 'RKDe_Price', title: '含税单价', operate: false},
                        {field: 'RKDe_Money', title: '含税金额', operate: false},
                        {field: 'RKDe_TaxRate', title: '税率', operate: false},
                        // {field: 'way', title: '计算价格方式', operate: false,formatter:function(value){
                        //     return value==1?'按重量':'按数量';
                        // }},
                        // {field: 'price', title: '单价', operate: false},
                        {field: 'Writer', title: __('Writer'), operate: false},
                        {field: 'WriteTime', title: '制表时间', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", ".data-return", function () {
                var tableContent=table.bootstrapTable('getAllSelections');
                if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
                else Fast.api.close(tableContent);
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        layer.msg(ret.msg);
                        window.location.reload();
                    }
                    return false;
                });
                $(document).on('click', "#chooseVendor", function(e){
                    let v_num = $("#V_Num").val();
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            if(v_num && v_num!=value.V_Num){
                                $("#tbshow").html('');
                            }
                            $("#V_Num").val(value.V_Num);
                            $("#V_Name").val(value.V_Name);
                        }
                    };
                    Fast.api.open('chain/material/material_note/chooseVendor',"供应商选择",options);
                });
                $(document).on('click', "#chooseMaterial", function(e){
                    let v_num = $("#V_Num").val();
                    if(!v_num){
                        layer.msg("请先选择供应商");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var tableField = Config.tableField;
                            var limber = Config.wjClassList;
                            var content = "";
                            $.each(value,function(v_index,v_e){
                                content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                $.each(tableField,function(t_index,e){
                                    let input_value = (typeof(v_e[e[6]]) == 'undefined'?e[4]:v_e[e[6]]);
                                    if(e[1]=="sortNum"){
                                        input_value = limber[input_value];
                                    }
                                    
                                    content += '<td '+e[5]+'><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+input_value+'"></td>';
                                    
                                });
                                content += "</tr>";
                            });
                            $("#tbshow").append(content);
                        }
                    };
                    Fast.api.open('logisticsmatter/purchase/procurement_main_wj/selectProcurementList/v_num/'+v_num,"选择材料",options);
                    // Fast.api.open('chain/purchase/requisition/chooseMaterial',"选择材料",options);
                })

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(1).find('input').val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/machine/materialnote_jwl/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                });

                $(document).on("click", "#author", function () {
                    var num = $("#MN_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/machine/materialnote_jwl/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#MN_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/machine/materialnote_jwl/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
            }
        }
    };
    return Controller;
});