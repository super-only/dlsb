define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree', 'hpbundle', 'jwljcdata'], function ($, undefined, Backend, Table, Form, undefined) {

    var Controller = {
        index: function () {
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'chain/machine/wj_store_balance/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/wj_store_balance/index' + location.search,
                    add_url: 'chain/machine/wj_store_balance/add',
                    edit_url: 'chain/machine/wj_store_balance/edit',
                    // del_url: 'chain/machine/wj_store_balance/del',
                    multi_url: 'chain/machine/wj_store_balance/multi',
                    import_url: 'chain/machine/wj_store_balance/import',
                    table: 'wjstorebalance',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'BL_Num',
                // sortName: 'BL_ID',
                search: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'BL_ID', title: __('Bl_id')},
                        {field: 'WC_Name', title: '仓库', searchList: Config.warehouse_list},
                        {field: 'IS_Name', title: "存货类别", operate: 'LIKE'},
                        {field: 'IR_Num', title: "存货编码", operate: 'LIKE'},
                        {field: 'IR_Name', title: __('存货名称'), operate: 'LIKE'},
                        {field: 'IR_Spec', title: __('规格'), operate: 'LIKE'},
                        {field: 'IR_Unit', title: __('计量单位'), operate: false},
                        {field: 'BL_LastCount', title: __('Bl_lastcount'), operate:false},
                        {field: 'BL_LastPrice', title: __('Bl_lastprice'), operate:false},
                        {field: 'BL_LastMoney', title: __('Bl_lastmoney'), operate:false},
                        {field: 'BL_RKCount', title: __('Bl_rkcount'), operate:false},
                        {field: 'BL_RKPrice', title: __('Bl_rkprice'), operate:false},
                        {field: 'BL_RKMoney', title: __('Bl_rkmoney'), operate:false},
                        {field: 'BL_CKCount', title: __('Bl_ckcount'), operate:false},
                        {field: 'BL_CKPrice', title: __('Bl_ckprice'), operate:false},
                        {field: 'BL_CKMoney', title: __('Bl_ckmoney'), operate:false},
                        {field: 'BL_EndCount', title: __('Bl_endcount'), operate:false},
                        {field: 'BL_EndPrice', title: __('Bl_endprice'), operate:false},
                        {field: 'BL_EndMoney', title: __('Bl_endmoney'), operate:false},
                        {field: 'BL_Date', title: __('Bl_date'), operate:false},
                        {field: 'Assessor', title: __('Assessor'), operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.queryParams = function(op){
                    op.tree = dd_num;
                    return op;
                }
                table.bootstrapTable('refresh',{})
                Table.api.init({
                    extend: {
                        index_url: 'chain/machine/wj_store_balance/index' + location.search,
                        add_url: 'chain/machine/wj_store_balance/add',
                        edit_url: 'chain/machine/wj_store_balance/edit',
                        // del_url: 'chain/machine/wj_store_balance/del',
                        multi_url: 'chain/machine/wj_store_balance/multi',
                        import_url: 'chain/machine/wj_store_balance/import',
                        table: 'wjstorebalance',
                    }
                });
                
            }).jstree();

            table.on("refresh.bs.table", function () {
                $('#treeview').jstree({
                    'core' : {
                            'data' : {
                            'url' : 'chain/machine/wj_store_balance/companytree',
                            'data' : function (node) {
                                return { 'id' : node.id };
                            }
                        }
                    }
                });
            })
            // $("ul").css({"overflow":"auto","height":document.body.clientHeight});
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            $("#print").click(function () {
                window.top.Fast.api.open('chain/machine/wj_store_balance/print/ids/' + Config.ids, '汇总表', {
                    area: ["100%", "100%"]
                });
            });
            Controller.api.bindevent("edit");
        },
        print: function() {
            console.log('row', Config.row);
            console.log('list', Config.list);
            
            let mainInfos = Config.row;
            let list = Config.list;
            let wslist = Config.wslist;
            
            tb_tmp = list.map((e) => {
                let tmp = {
                    mc: e['IR_Name'],
                    gg: e['IR_Spec'],
                    dw: e['IR_Unit'],
                    
                    sqjc_sl: e['BL_LastCount']=='0'? '': e['BL_LastCount'],
                    sqjc_dj: e['BL_LastPrice']=='0'? '': e['BL_LastPrice'],
                    sqjc_je: e['BL_LastMoney']=='0'? '': e['BL_LastMoney'],

                    bqrk_sl: e['BL_RKCount']=='0'? '': e['BL_RKCount'],
                    bqrk_dj: e['BL_RKPrice']=='0'? '': e['BL_RKPrice'],
                    bqrk_je: e['BL_RKMoney']=='0'? '': e['BL_RKMoney'],

                    bqck_sl: e['BL_CKCount']=='0'? '': e['BL_CKCount'],
                    bqck_dj: e['BL_CKPrice']=='0'? '': e['BL_CKPrice'],
                    bqck_je: e['BL_CKMoney']=='0'? '': e['BL_CKMoney'],

                    bqjc_sl: e['BL_EndCount']=='0'? '': e['BL_EndCount'],
                    bqjc_dj: e['BL_EndPrice']=='0'? '': e['BL_EndPrice'],
                    bqjc_je: e['BL_EndMoney']=='0'? '': e['BL_EndMoney'],
                }
                return tmp;
            }).filter((e) => {
                return e.sqjc_sl || e.sqjc_dj || e.sqjc_je ||
                        e.bqrk_sl || e.bqrk_dj || e.bqrk_je || 
                        e.bqck_sl || e.bqck_dj || e.bqck_je ||
                        e.bqjc_sl || e.bqjc_dj || e.bqjc_je;
            });
            
            let data_tmp={
                zbr:mainInfos['Writer'],
                zbrq: mainInfos['WriteDate'],
                shr: mainInfos['Assessor'],
                shrq: mainInfos['AssessDate']=='-0001-11-30'?'':mainInfos['AssessDate'],
                jcrq: mainInfos['BL_Date'],
                ck: wslist[mainInfos['WC_Num']],
                dyrq:mainInfos['PrintDate'],
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){
                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: jwljcdata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            pageto('#p_mx1',0);
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
        },
        api: {
            bindevent: function (field) {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                    }
                    return false;
                });
                var table = $("#table");

                // 初始化表格
                table.bootstrapTable({
                    data:Config.table_data,
                    height: document.body.clientHeight-200,
                    columns: [
                        [
                            {field:"id",title:"",formatter:function(value,row,index){return ++index;}},
                            // {field: 'BL_ID', title: __('Bl_id')},
                            {field: 'WC_Name', title: '仓库', searchList: Config.warehouse_list},
                            {field: 'IS_Name', title: "存货类别", operate: 'LIKE'},
                            {field: 'IR_Num', title: "存货编码", operate: 'LIKE'},
                            {field: 'IR_Name', title: __('存货名称'), operate: 'LIKE'},
                            {field: 'IR_Spec', title: __('规格'), operate: 'LIKE'},
                            {field: 'IR_Unit', title: __('计量单位'), operate: false},
                            {field: 'BL_LastCount', title: __('Bl_lastcount'), operate:false},
                            {field: 'BL_LastPrice', title: __('Bl_lastprice'), operate:false},
                            {field: 'BL_LastMoney', title: __('Bl_lastmoney'), operate:false},
                            {field: 'BL_RKCount', title: __('Bl_rkcount'), operate:false},
                            {field: 'BL_RKPrice', title: __('Bl_rkprice'), operate:false},
                            {field: 'BL_RKMoney', title: __('Bl_rkmoney'), operate:false},
                            {field: 'BL_CKCount', title: __('Bl_ckcount'), operate:false},
                            {field: 'BL_CKPrice', title: __('Bl_ckprice'), operate:false},
                            {field: 'BL_CKMoney', title: __('Bl_ckmoney'), operate:false},
                            {field: 'BL_EndCount', title: __('Bl_endcount'), operate:false},
                            {field: 'BL_EndPrice', title: __('Bl_endprice'), operate:false},
                            {field: 'BL_EndMoney', title: __('Bl_endmoney'), operate:false}
                        ]
                    ]
                });

                $(document).on("click", "#summary", function () {
                    var BL_Date = $("#c-BL_Date").val();
                    var WC_Num = $("#c-WC_Num").val();
                    if(WC_Num=='' || BL_Date=='') layer.msg("结存日期和仓库必填");
                    else{
                        var row = {};
                        row.BL_Date = BL_Date;
                        row.WC_Num = WC_Num;
                        $.ajax({
                            url: 'chain/machine/wj_store_balance/summary',
                            type: 'post',
                            dataType: 'json',
                            async: false,
                            data: {row:JSON.stringify(row)},
                            success: function (ret) {
                                if(ret.code==1){
                                    $("#table").bootstrapTable('load',ret.data);
                                }
                                
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    }
                })

                $(document).on("click", "#author", function () {
                    var num = Config.ids;
                    check('审核之后无法修改，确定审核？',"chain/machine/wj_store_balance/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = Config.ids;
                    check('确定弃审？',"chain/machine/wj_store_balance/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
                $(document).on('click', "#allDel", function(env){
                    var num = Config.ids;
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/machine/wj_store_balance/allDel',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        parent.location.reload();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        layer.msg("删除失败");
                    }
                })
            }
        }
    };
    return Controller;
});