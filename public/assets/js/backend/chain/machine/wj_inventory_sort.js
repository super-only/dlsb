define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {

    var Controller = {
        index: function () {
            //修改顶部弹窗大小
            $(".btn-add").data("area",["80%","80%"]);
            $(".btn-edit").data("area",["80%","80%"]);
            $('#treeview').jstree({
                'core' : {
                        'data' : {
                        'url' : 'chain/machine/wj_inventory_sort/companytree',
                        'data' : function (node) {
                            return { 'id' : node.id };
                        }
                    }
                }
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/machine/wj_inventory_sort/index' + location.search,
                    add_url: 'chain/machine/wj_inventory_sort/add',
                    edit_url: 'chain/machine/wj_inventory_sort/edit',
                    del_url: 'chain/machine/wj_inventory_sort/del',
                    multi_url: 'chain/machine/wj_inventory_sort/multi',
                    import_url: 'chain/machine/wj_inventory_sort/import',
                    table: 'wjinventorysort',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'IS_Num',
                sortName: 'IS_Num',
                sortOrder: 'ASC',
                search: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'IS_Num', title: __('Is_num'), operate: 'LIKE'},
                        {field: 'IS_Name', title: __('Is_name'), operate: 'LIKE'},
                        {field: 'ParentNum', title: __('Parentnum'), operate: 'LIKE'},
                        {field: 'Writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('post-body.bs.table',function(){
                $(".btn-editone").data("area",["80%","80%"]);
            });

            $('#treeview').on('changed.jstree', function (e, data) {
                var dd_num = data.selected[0];
                var options = table.bootstrapTable('getOptions');
                options.queryParams = function(op){
                    console.log(op);
                    // op.tree = dd_num;
                    // return op;
                }
                table.bootstrapTable('refresh',{})
                Table.api.init({
                    extend: {
                        index_url: 'chain/machine/wj_inventory_sort/index' + location.search,
                        add_url: 'chain/machine/wj_inventory_sort/add?tree='+dd_num,
                        edit_url: 'chain/machine/wj_inventory_sort/edit',
                        del_url: 'chain/machine/wj_inventory_sort/del',
                        multi_url: 'chain/machine/wj_inventory_sort/multi',
                        import_url: 'chain/machine/wj_inventory_sort/import',
                        table: 'wjinventorysort'
                    }
                });
                
            }).jstree();
        },
        add: function () {
            Controller.api.bindevent();
            $(document).on("change", "#c-ParentNum", function () {
                var text = $("#c-ParentNum").val();
                $.ajax({
                    url: "chain/machine/wj_inventory_sort/stockTypeCode",
                    type: 'post',
                    data: {value: text},
                    dataType: 'json',
                    success: function (ret) {
                        $("#c-IS_Num").val(ret.data);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    parent.location.reload();
                });
                
            }
        }
    };
    return Controller;
});