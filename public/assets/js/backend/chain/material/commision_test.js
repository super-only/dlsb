define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'clbyddata'], function ($, undefined, Backend, Table, Form) {

    function check(msg,url,num){
        // console.log(url,msg,num);return false;
        var index = layer.confirm(msg, {
            btn: ['确定', '取消'],
        }, function(data) {
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {num: num},
                success: function (ret) {
                    if (ret.code == 1) {
                        window.location.reload();
                    }else{
                        Layer.msg(ret.msg);
                    }
                }, error: function (e) {
                    Backend.api.toastr.error(e.message);
                }
            });
            layer.close(index);
        })
    }
    var Controller = {
        index: function () {
            $("#no_issued").bootstrapTable({
                data: [],
                height: 250,
                search: false,
                pagination: false,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'CTD_Pi', title: '批次'},
                        {field: 'CTD_testnum', title: '试验编号'},
                        {field: 'MN_Num', title: '到货单号'},
                        {field: 'IM_Class', title: '材料'},
                        {field: 'CTD_Spec', title: '规格'},
                        {field: 'L_Name', title: '材质'},
                        {field: 'MGN_Length', title: '长度(m)'},
                        {field: 'MGN_Width', title: '宽度(m)'},
                        {field: 'CTD_FactCount', title: '到货数量'},
                        {field: 'CTD_Weight', title: '到货重量(kg)'},
                        // {field: 'CTD_Result', title: '检验结果'},
                        {field: 'CTD_ShapeSizeRes', title: '外形尺寸结果'},
                        // {field: 'mgn_DuiFangDian', title: '试验日期'},
                        {field: 'CTD_ShowQualityRes', title: '外观质量结果'},
                        {field: 'MGN_Maker', title: '生产厂家'},
                        {field: 'LuPiHao', title: '炉号'},
                        {field: 'PiHao', title: '批号'},
                        {field: 'JiaoGangNum', title: '角钢编号'},
                        {field: 'CTD_Project', title: '试验项目'}
                    ]
                ]
            });
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/commision_test/index' + location.search,
                    add_url: 'chain/material/commision_test/add',
                    edit_url: 'chain/material/commision_test/edit',
                    del_url: 'chain/material/commision_test/del',
                    multi_url: 'chain/material/commision_test/multi',
                    import_url: 'chain/material/commision_test/import',
                    table: 'commisiontest',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'CT_Num',
                sortName: 'WriteTime',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                // showExport: false,
                height: 500,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CT_Num', title: __('Ct_num'), operate: 'LIKE'},
                        {field: 'V_Name', title: __('V_num'), operate: 'LIKE'},
                        {field: 'CT_Type', title: __('Ct_type'), operate: 'LIKE'},
                        {field: 'CT_Date', title: __('Ct_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'CT_TakeDate', title: __('Ct_takedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'CT_Shape', title: __('Ct_shape'), operate: 'LIKE'},
                        {field: 'CT_AcceptPepo', title: __('Ct_acceptpepo'), operate: 'LIKE'},
                        {field: 'CT_RequestPepo', title: __('Ct_requestpepo'), operate: 'LIKE'},
                        {field: 'CT_Standard', title: __('Ct_standard'), operate: 'LIKE'},
                        {field: 'Writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'WriteTime', title: __('Writetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Auditor', title: __('Auditor'), operate: false},
                        {field: 'AuditTime', title: __('Audittime'), operate:false},
                        {field: 'CT_Memo', title: __('Ct_memo'), operate: false},
                        {field: 'is_check', title: '状态', searchList:{1:"未审核",2:"已审核"}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on('click-row.bs.table',function(row, $element){
                var CT_Num = $element.CT_Num;
                if(CT_Num == '') return false;
                $.ajax({
                    url: 'chain/material/commision_test/requisitionDetail',
                    type: 'post',
                    dataType: 'json',
                    data: {ids:CT_Num},
                    success: function (ret) {
                        var content = [];
                        if (ret.code === 1) {
                            content = ret.data;
                        }
                        $("#no_issued").bootstrapTable('load',content);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                })
            })
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            $("#printDetail").click(function () {
                window.top.Fast.api.open('chain/material/commision_test/printDetail/ids/' + Config.ids, '材料报验单', {
                    area: ["100%", "100%"]
                });
            });
            Controller.api.bindevent("edit");
        },
        warranty: function () {
            Form.api.bindevent($("form[role=form]"),function(data,ret){
                layer.msg(ret.msg);
                return false;
            });
            $(document).on("click", "#selectCopy", function () {
                var num = Config.ids;
                var url = "chain/material/commision_test/selectCopy/ids/"+num;
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["90%","90%"],
                    callback:function(value){
                        if(value){
                            $.each(Config.field,function(index,e){
                                $("#"+e).val(value[e]);
                            });
                        }
                    }
                };
                Fast.api.open(url,"复制质保情况",options);
            });
            $(document).on("click", "#author", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到编号，请稍后重试');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/material/commision_test/warrantyAud",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到编号，请稍后重试');
                    return false;
                }
                check('确定弃审？',"chain/material/commision_test/warrantyGiv",num);
                
            });
        },
        selectcopy: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/commision_test/selectCopy/ids/'+Config.ids + location.search,
                    table: 'commisiontest',
                }
            });
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'CTD_ID',
                sortName: 'CTD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'CTD_ID', title: '编号', operate: false},
                        {field: 'CTD_QualityNo', title: '质保书号', operate: '=', defaultValue:Config.row["CTD_QualityNo"]},
                        {field: 'LuPiHao', title: '炉号', operate: '=', defaultValue:Config.row["LuPiHao"]},
                        {field: 'PiHao', title: '批号', operate: '=', defaultValue:Config.row["PiHao"]},
                        {field: 'CTD_testnum', title: '实验编号', operate:'='},
                        {field: 'CTD_Spec', title: '规格', operate: '='},
                        {field: 'L_Name', title: '材质', operate: '='},
                        {field: 'MGN_Length', title: '长度', operate: 'LIKE'},
                        {field: 'MGN_Width', title: '宽度', operate: 'LIKE'},
                        {field: 'MGN_Maker', title: '生产厂家', operate: 'LIKE'},
                        {field: 'JiaoGangNum', title: '角钢编号', operate:'RANGE'},
                        {field: 'CTD_C', title: 'C(%)', operate: false},
                        {field: 'CTD_Si', title: 'Si(%)', operate: false},
                        {field: 'CTD_Mn', title: 'Mn(%)', operate: false},
                        {field: 'CTD_P', title: 'P(%)', operate: false},
                        {field: 'CTD_S', title: 'S(%)', operate: false},
                        {field: 'CTD_V', title: 'V(%)', operate: false},
                        {field: 'CTD_Nb', title: 'Nb(%)', operate: false},
                        {field: 'CTD_Ti', title: 'Ti(%)', operate: false},
                        {field: 'CTD_Rel', title: '屈服强度(MPa)', operate: false},
                        {field: 'CTD_Rm', title: '抗拉强度(MPa)', operate: false},
                        {field: 'CTD_Percent', title: '伸长率(%)', operate: false},
                        {field: 'CTD_attack_temp', title: '冲击温度(℃)', operate: false},
                        {field: 'CTD_attack_first', title: '冲击1', operate: false},
                        {field: 'CTD_attack_second', title: '冲击2', operate: false},
                        {field: 'CTD_attack_third', title: '冲击3', operate: false},
                        {field: 'CTD_attack_average', title: '冲击均值', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        choosedetail: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/commision_test/chooseDetail/supplier/' + Config.supplier + location.search,
                    table: 'materialgetnote',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'MGN_ID',
                sortName: 'WriteTime',
                search: false,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'MGN_ID', title: "MGN_ID", operate: false, visible: false},
                        {field: 'CTD_testnum', title: "试验编号", operate: false,},
                        {field: 'mgn.mgn_testnum', title: "试验编号", operate: 'LIKE', visible: false},
                        {field: 'MN_Num', title: '到货单号', operate: false},
                        {field: 'mgn.MN_Num', title: "到货单号", operate: 'LIKE', visible: false},
                        {field: 'V_Num', title: '供应商名称ID', operate: false, visible:false},
                        {field: 'V_Name', title: '供应商名称', operate: 'LIKE'},
                        {field: 'MN_Date', title: '到货日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'MGN_Destination', title: '到货去向', operate: 'LIKE'},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'CTD_Spec', title: '规格', operate: false},
                        {field: 'im.IM_Spec', title: '规格', operate: false, visible:false},
                        {field: 'L_Name', title: '材质', operate: false},
                        {field: 'MGN_Length', title: '长度(mm)', operate: false},
                        {field: 'MGN_Width', title: '宽度(mm)', operate: false},
                        {field: 'CTD_FactCount', title: '数量', operate: false},
                        {field: 'CTD_Weight', title: '重量', operate: false},
                        {field: 'CTD_GuoBanWeight', title: '重量', operate: false,visible: false},
                        {field: 'IM_PerWeight', title: '比重', operate: false,visible: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        printdetail: function() {
            // console.log('limberList',Config.limberList);
            
            let list = Config.list;
            console.log('list',Config.list);
            let mainInfos = Config.mainInfos;
            let tb_tmp = [];
            // let weightSum = 0;

            for(let index=0;index<list.length;index++){
                
                let tmp={
                    'clmc':list[index]['IM_Class'], //材料名称
                    'czgg':list[index]['L_Name']+list[index]['CTD_Spec'],//材质规格
                    'cd':list[index]['MGN_Length'],//长度
                    'sccj':list[index]['MGN_Maker'],//生产厂家
                    'dhsl':list[index]['CTD_FactCount'], //到货数量
                    'dhrq':list[index]['MN_Date'],//到货日期
                    'ybsl':list[index]['CTD_Count'],
                    'syr':list[index]['CTD_QYRen'],
                    'sysj':list[index]['CT_Date'],
                    'dhzl':list[index]['CTD_GuoBanWeight'],//备注
                    'qycj':list[index]['CTD_Dept'],
                    'lh':list[index]['LuPiHao'],
                    'yzph':list[index]['PiHao'],
                };
                // weightSum+=Math.floor(list[index]['AD_Weight']*100)/100;
                
                tb_tmp.push(tmp);    
            }

            console.log('tb_tmp',tb_tmp);
            tb_tmp.push({
                'clmc':'委托人:', //材料名称
                'czgg':mainInfos['CT_RequestPepo'],
                'cd':'',
                'sccj':'',
                'dhsl':'',
                'dhzl':'委托单位主管:',
                'dhrq':mainInfos['CT_RequestPepo'],
                'ybsl':'',
                'syr':'',
                'sysj':'签收人:',
                'lh':'',
                'yzph':'',
            });
            let data_tmp={
                bydbh:mainInfos['CT_Num'],
                gys:mainInfos['V_Name'],
                qycj:list[0]['CTD_Dept'],
                tb:tb_tmp
            };
            

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: clbyddata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        detailimport: function () {
            Form.api.bindevent($("form[role=form]"));
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                $(document).on("click", "#export", function () {
                    window.open('/admin.php/chain/material/commision_test/export/ids/' + Config.ids);
                });
                $(document).on('click', "#chooseDetail", function(env){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var tableField = Config.tableField;
                            var content = "";
                            var supplierName = (typeof(value[0]["V_Name"]) == 'undefined'?"":value[0]["V_Name"]);
                            var supplierNum = (typeof(value[0]["V_Num"]) == 'undefined'?"":value[0]["V_Num"]);
                            $.each(value,function(v_index,v_e){
                                if(v_e['V_Num'] != supplierNum){
                                    layer.msg("供应商有误！");
                                    return false;
                                }
                                content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td><td></td>';
                                $.each(tableField,function(t_index,e){
                                    if(e[1]=="CTD_QualityNum") content += '<td '+e[5]+'><a href="javascript:;" class="btn btn-xs btn-info detailImport"><i class="fa fa-upload"></i></a></td>';
                                    else if(e[2]=="select") content += '<td '+e[5]+'><select class="form-control small_input" name="table_row['+e[1]+'][]"><option value="合格">合格</option><option value="不合格">不合格</option></select></td>';
                                    else if(e[2]=="datetimerange") content += '<td '+e[5]+'><input data-date-format="YYYY-MM-DD" class="small_input datetimepicker" data-use-current="true" name="table_row['+e[1]+'][]" type="text" value="'+Config.time+'"></td>';
                                    else if(e[1] == 'MGN_Length' || e[1] == 'MGN_Width') content += '<td '+e[5]+'><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+(v_e[e[1]]/1000).toFixed(3)+'"></td>';
                                    else content += '<td '+e[5]+'><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?e[4]:v_e[e[1]])+'"></td>';
                                });
                                content += "</tr>";
                            });
                            $("#tbshow").append(content);
                            $("#V_Num").val(supplierNum);
                            $("#V_Name").val(supplierName);
                        }
                    };
                    var supplier = $("input[name='row[V_Num]']").val();
                    Fast.api.open('chain/material/commision_test/chooseDetail/supplier/'+supplier,"原材料到货选择",options);
                })

                $(document).on('click', ".btn-zjupdate", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[CTD_ID][]"]').val();
                    if(num!=''){
                        var url = "chain/material/commision_test/warranty/ids/"+num;
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["90%","90%"]
                        };
                        Fast.api.open(url,"填写质保书详情",options);
                    }
                });

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td input[name="table_row[CTD_ID][]"]').val();
                    if(num!=''){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/material/commision_test/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                });

                $(document).on('click', '#chooseVendor', function (e){
                    var url = "chain/material/material_note/chooseVendor";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#V_Num").val(value.V_Num);
                            $("#V_Name").val(value.V_Name);
                            // $("#c-V_Name").val(value.C_Name);
                        }
                    };
                    Fast.api.open(url,"选择单位名称",options);
                })

                $('#tbshow').on('keyup','td input[name="table_row[CTD_FactCount][]"]',function () {
                    var dz = $(this).parents('tr').find("input[name='table_row[dz][]']").val().trim();
                    dz = dz==''?0:parseFloat(dz);
                    var number = $(this).parents('tr').find("input[name='table_row[CTD_FactCount][]']").val().trim();
                    number = number==''?0:parseInt(number);
                    var weight=0;
                    weight = (dz*number).toFixed(2);
                    $(this).parents('tr').find("input[name='table_row[CTD_Weight][]']").val(weight);
                })
                $('#tbshow').on('keyup','td input[name="table_row[CTD_UnqualityCount][]"]',function () {
                    var dz = $(this).parents('tr').find("input[name='table_row[dz][]']").val().trim();
                    dz = dz==''?0:parseFloat(dz);
                    var number = $(this).parents('tr').find("input[name='table_row[CTD_UnqualityCount][]']").val().trim();
                    number = number==''?0:parseInt(number);
                    var weight=0;
                    weight = (dz*number).toFixed(2);
                    $(this).parents('tr').find("input[name='table_row[CTD_UnqualityWeight][]']").val(weight);
                })

                $(document).on("click", "#author", function () {
                    var num = $("#CT_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/material/commision_test/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#CT_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/material/commision_test/giveUp",num);
                    
                });

                $(document).on("click", ".detailImport", function () {
                    var CTD_QualityNo = $(this).parents("tr").find("td input[name='table_row[CTD_QualityNo][]']").val();
                    if(CTD_QualityNo){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["500px","300px"],
                        };
                        Fast.api.open('chain/material/commision_test/detailImport/qn/'+CTD_QualityNo,"上传质保书",options);
                    }else{
                        layer.msg("请先填写质保书编号");
                    }
                    
                });
            }
        }
    };
    return Controller;
});