define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/nesting_loss/index' + location.search,
                    add_url: 'chain/material/nesting_loss/add',
                    edit_url: 'chain/material/nesting_loss/edit',
                    del_url: 'chain/material/nesting_loss/del',
                    multi_url: 'chain/material/nesting_loss/multi',
                    import_url: 'chain/material/nesting_loss/import',
                    table: 'nesting_loss',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'order',
                sortOrder: 'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'l_name', title: __('L_name'), operate: 'LIKE'},
                        {field: 'spec_down', title: __('Spec_down')},
                        {field: 'spec_up', title: __('Spec_up')},
                        {field: 'thickness_down', title: __('Thickness_down')},
                        {field: 'thickness_up', title: __('Thickness_up')},
                        {field: 'loss', title: __('Loss')},
                        {field: 'tail', title: __('Tail')},
                        {field: 'order', title: __('Order')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});