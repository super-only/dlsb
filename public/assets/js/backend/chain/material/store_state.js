define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/store_state/index/type/'+Config.type + location.search,
                    // add_url: 'chain/material/store_state/add',
                    // edit_url: 'chain/material/store_state/edit',
                    // del_url: 'chain/material/store_state/del',
                    // multi_url: 'chain/material/store_state/multi',
                    // import_url: 'chain/material/store_state/import',
                    table: 'storestate',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SS_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                height: document.body.clientHeight,
                // showExport: false,
                onlyInfoPagination: true,
                onLoadSuccess:function(){
                    // 这里就是数据渲染结束后的回调函数
                    $(".btn-editone").data("area", ['60%','80%']);
                },
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}, operate: false},
                        {field: 'SS_ID', title: __('Ss_id'), visible: false, operate: false},
                        {field: 'SS_WareHouse', title: __('Ss_warehouse'), searchList: Config.wareclassList},
                        {field: 'IM_Num', title: __('Im_num'), operate: false},
                        {field: 'IM_Class', title: '存货名称', operate: "="},
                        {field: 'L_Name', title: __('L_name'), operate: false},
                        {field: 'm.L_Name', title: __('L_name'), searchList: Config.limberList,visible: false},
                        {field: 'IM_Spec', title: __('Im_spec'), operate: false},
                        {field: 'm.IM_Spec', title: __('Im_spec'), operate: "=",visible: false},
                        {field: 'SS_Length', title: __('Ss_length'), operate:'='},
                        {field: 'SS_Width', title: __('Ss_width'), operate:'='},
                        {field: 'SS_Count', title: __('Ss_count'), operate: false},
                        {field: 'SS_Weight', title: __('Ss_weight'), operate:false},
                        {field: 'ASF_Count', title: '虚拟数量', operate: 'BETWEEN'},
                        {field: 'ASF_Weight', title: '虚拟重量', operate: 'BETWEEN'},
                        // {field: 'ASF_Count', title: __('Asf_count'), operate:false},
                        // {field: 'ASF_Weight', title: __('Asf_weight'), operate:false},
                        // {field: 'operate', title: '修改配料后库存',table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                        // {field: 'SF_Count', title: __('Sf_count'), operate:false},
                        // {field: 'SF_Weight', title: __('Sf_weight'), operate:false},
                        // {field: 'ZM_Price', title: '账面单价', operate: false},
                        // {field: 'SS_SumPrice', title: __('Ss_sumprice'), operate: false},
                        // {field: 'ifSuanByCount', title: '按数量金额计算', operate: false,visible: false},
                        // {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.off('dbl-click-row.bs.table');
            $(document).on("click", ".btn-arrangement", function(e){
                window.location.href = 'store_state/index/type/1';
            });
            table.on("dbl-click-cell.bs.table",function(field, value, row, $element){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        return false;
                    }
                };
                Fast.api.open('chain/material/store_state/detail/ids/'+$element.SS_ID,"详情",options);
            });
            $(document).on('click', ".btn-right", function(env){
                var index = layer.confirm("确定校正？", {
                    btn: ['确定', '取消'],
                }, function(data) {
                    layer.close(index);
                    $.ajax({
                        url: 'chain/material/store_state/right',
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        success: function (ret) {
                            Layer.msg(ret.msg);
                            if(ret.code==1){
                                window.location.reload();
                            }
                            
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                })
            })
        },
        // edit: function () {
        //     Controller.api.bindevent();
        //     $(document).on("keyup","#c-ASF_Count",function(e){
        //         var all_count = $("#c-SS_Count").val();
        //         var pl_count = $("#c-ASF_Count").val();
        //         var zm_count = all_count - pl_count;
        //         $("#c-SF_Count").val(zm_count);
        //     })
        //     $(document).on("keyup","#c-ASF_Weight",function(e){
        //         var all_weight = $("#c-SS_Weight").val();
        //         var pl_weight = $("#c-ASF_Weight").val();
        //         var zm_weight = all_weight - pl_weight;
        //         var price = $("#c-ZM_Price").val();
        //         $("#c-SF_Weight").val(zm_weight);
        //         $("#c-SS_SumPrice").val((price*zm_weight).toFixed(2));
        //     })
        // },
        detail: function () {
            Controller.api.bindevent();
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                data: Config.this_rk,
                columns: [
                    [
                        {field: 'id', title: "", formatter: function(value,row,index){return ++index;}},
                        {field: 'SID_ID', title: '入库ID', visible: false},
                        {field: 'SI_OtherID', title: '入库编码'},
                        {field: 'SI_InDate', title: '入库日期'},
                        {field: 'MN_Date', title: '到货日期'},
                        {field: 'SID_testnum', title: '试验编号'},
                        {field: 'SID_RestCount', title: '数量'},
                        {field: 'SID_RestWeight', title: '重量(kg)'},
                        // {field: 'SID_CTD_Pi', title: '进货批次'},
                        // {field: 'SS_Width', title: '堆放点'},
                        {field: 'LuPiHao', title: '炉号'},
                        {field: 'PiHao', title: '批号'},
                        {field: 'V_Name', title: '供应商'},
                        // {field: 'order', title: '排序(0优先级最高)'},
                        {field: 'operate', title: '置顶操作',
                            buttons: [{name: 'topping',
                            text: '置顶',
                            title: '置顶',
                            classname: 'btn btn-xs btn-danger btn-view btn-ajax',
                            // icon: 'fa fa-trash',
                            url: 'chain/material/store_state/topping/sid_id/{SID_ID}',
                            success: function(){
                                console.log(1);
                                window.location.reload();
                            }}],
                            table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate},
                    ]
                ]
            });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});