define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/store_out_detail/index' + location.search,
                    add_url: 'chain/material/store_out_detail/add',
                    edit_url: 'chain/material/store_out_detail/edit',
                    import_url: 'chain/material/wjg/import',
                    table: 'storeoutdetail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SO_Num',
                sortName: 'SO_TakeDate',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                // showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'offer', title: '是否传U8', searchList:{0:"否",1:"是"}},
                        {field: 'SOD_ID', title: __('Sod_id'), operate: false, visible: false},
                        {field: 'SO_TakeDate', title: '发料日期', defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'IM_Class', title: '材料名称', operate: 'LIKE'},
                        {field: 'L_Name', title: __('L_name'), operate: false},
                        {field: 'sod.L_Name', title: __('L_name'), operate: 'LIKE', visible: false},
                        {field: 'IM_Spec', title: '规格', operate: '='},
                        {field: 'SOD_Length', title: __('Sod_length'), operate:'=1000'},
                        {field: 'SOD_Width', title: __('Sod_width'), operate: false},
                        {field: 'SOD_Count', title: __('Sod_count'), operate: false},
                        {field: 'SOD_Weight', title: __('Sod_weight'), operate: false},
                        {field: 'SO_Writer', title: '制表人', operate: false},
                        {field: 'SO_WriteDate', title: '制表日期', operate: false},
                        {field: 'SO_Auditor', title: '审批人', operate: false},
                        {field: 'SO_AudiDate', title: '审批日期', operate: false},
                        {field: 'SO_Num', title: '发料编码', operate: false},
                        {field: 'so.SO_Num', title: '发料编码', operate: 'LIKE', visible: false},
                        {field: 'SO_ProjectName', title: '工程', operate: false},
                        {field: 'so.SO_ProjectName', title: '工程', operate: 'LIKE', visible: false},
                        {field: 'SO_C_Name', title: '客户单位', operate: 'LIKE'},
                        {field: 'SO_TowerType', title: '塔型', operate: false},
                        {field: 'so.SO_TowerType', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'so.PC_Num', title: '工程编号', operate: 'LIKE', visible: false},
                        {field: 'SO_EType', title: '电压等级', operate: false},
                        {field: 'so.SO_EType', title: '电压等级', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务通知单号', operate: false},
                        {field: 'so.T_Num', title: '任务通知单号', operate: 'LIKE', visible: false},
                        {field: 'PT_Num', title: '生产下达单号', operate: 'LIKE'},
                        {field: 'SID_TestResult', title: '生产厂家', operate: false},
                        {field: 'LuPiHao', title: '炉号', operate: false},
                        {field: 'sod.LuPiHao', title: '炉号', operate: 'LIKE', visible: false},
                        {field: 'PiHao', title: '批号', operate: false},
                        {field: 'sod.PiHao', title: '批号', operate: 'LIKE', visible: false},
                        {field: 'SID_CTD_Pi', title: '进货批次', operate: 'LIKE'},
                        {field: 'SO_Memo', title: '主表备注', operate: false},
                        {field: 'SO_Take', title: '领用车间', operate: 'LIKE'},
                        {field: 'SO_WareHouse', title: '所在仓库', operate: 'LIKE'},
                        {field: 'SO_Flag', title: '出库类别', operate: false},
                        {field: 'SO_Flag_use', title: '出库类别', searchList: Config.ck_type,visible: false},
                        {field: 'SOD_Memo', title: '备注', operate: false},
                        {field: 'Hand_OutNum', title: '自编号', operate: 'LIKE'},
                        {field: 'V_Name', title: '供应商', operate: 'LIKE'},
                        {field: 'C_Num', title: '合同号', operate: 'LIKE'},
                        {field: 'E_Name', title: '领用人', operate: 'LIKE'},
                        {field: 'purpose', title: '用途', operate: 'LIKE'},
                        {field: 'E_NameBao', title: '保管员', operate: 'LIKE'},
                        {field: 'SID_testnum', title: '试验编号', operate: 'LIKE'},
                        {field: 'is_check', title: '状态', searchList:{1:"未审核",2:"已审核"}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $(document).on("click", "#updatePurchase", function () {
                $.ajax({
                    url: 'chain/material/store_out_detail/eipUpload',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {id: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#updateWg", function () {
                $.ajax({
                    url: 'chain/material/store_out_detail/upload',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {num: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#offerU", function () {
                $.ajax({
                    url: 'chain/material/store_out_detail/offerU',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {id: Config.ids},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
        },
        choosework: function () {
            var height = document.body.clientHeight*0.5;
            var detail_table = $("#chooseTable");
            detail_table.bootstrapTable({
                data: [],
                height: height,
                idField: 'WRD_ID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'WRD_ID', title: 'ID', visible: false},
                        {field: 'WRD_WareHouse', title: '所在仓库'},
                        {field: 'WRD_Place', title: '区域'},
                        {field: 'WR_Num', title: '领料单号'},
                        {field: 'IM_Class', title: '名称'},
                        {field: 'IM_Spec', title: '规格'},
                        {field: 'D_IM_Spec', title: '代领规格'},
                        {field: 'L_Name', title: '材质'},
                        {field: 'D_L_Name', title: '代领材质'},
                        {field: 'WRD_Length', title: '长(mm)'},
                        {field: 'WRD_Width', title: '宽(mm)'},
                        {field: 'WRD_Unit', title: '单位'},
                        {field: 'WRD_RealCount', title: '实领数量'},
                        {field: 'WRD_RealWeight', title: '实领重量'},
                        {field: 'WRD_ResetCount', title: '剩余数量'},
                        {field: 'WRD_ResetWeight', title: '剩余重量'},
                        {field: 'WRD_Memo', title: '备注'}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(detail_table);

            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/store_out_detail/chooseWork/warehouse/'+Config.warehouse + location.search,
                    table: 'workreceive',
                }
            });

            var table = $("#table");
            
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'WR_Num',
                sortName: 'WR_Date',
                sortOrder: 'DESC',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                height: height,
                columns: [
                    [
                        {checkbox: true,field: 'choose'},
                        {field: 'WR_Send', title: '发料情况', operate: false},
                        {field: 'WR_Date', title: '领料日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'WR_Num', title: '领料单号', operate: 'LIKE'},
                        {field: 'WR_WareHouse', title: '所在仓库', operate: 'LIKE'},
                        {field: 'taskdetail.T_Num', title: '工程编号', operate: 'LIKE', visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: false},
                        {field: 'compact.PC_Num', title: '工程名称', operate: 'LIKE', visible: false},
                        {field: 't_project', title: '工程名称', operate: false},
                        {field: 'task.t_shortproject', title: '工程简称', operate: 'LIKE', visible: false},
                        {field: 't_shortproject', title: '工程简称', operate: false},
                        {field: 'workreceive.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'taskdetail.TD_Pressure', title: '电压等级', operate: 'LIKE', visible: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'Customer_Name', title: '客户', operate: false},
                        {field: 'WR_Person', title: '领料人', operate: 'LIKE'},
                        {field: 'WR_Dept', title: '领料班组', operate: 'LIKE'},
                        {field: 'Writer', title: '制单人', operate: false},
                        {field: 'WriteDate', title: '制单日期', operate: false, defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'PT_Num', title: '下达单号', operate: 'LIKE'},
                        {field: 'WR_Memo', title: '备注', operate: false},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            table.on("check.bs.table",function(row, $element){
                // var check_index = $(this).index();
                // table.bootstrapTable('check', check_index);
                
                // console.log($element);
                // if($element.choose==false){
                    var num = $element.WR_Num;
                    $.ajax({
                        url: 'chain/material/store_out_detail/detailWork',
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            var content = [];
                            if(ret.code==1){
                                content = ret.data;
                            }
                            detail_table.bootstrapTable('load',content);
                        }, error: function (e) {
                            detail_table.bootstrapTable('load',[]);
                            Backend.api.toastr.error(e.message);
                        }
                    });
                // }else{
                    // detail_table.bootstrapTable('load',[]);
                // }
            });
            table.on("uncheck.bs.table",function(row, $element){
                detail_table.bootstrapTable('load',[]);
            });
            $(document).on("click", "#sure", function () {
                var tableContent = detail_table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择出库材料！");
                }else{
                    var mainContent = table.bootstrapTable('getSelections');
                    if(mainContent.length == 0) layer.msg("请先选择领料单！");
                    else{
                        var value = [];
                        value.push(tableContent);
                        value.push(mainContent[0]);
                        Fast.api.close(value);
                    }
                }
            });
            multiple_checks("chooseTable");
            function multiple_checks(table_id='chooseTable')
            {
                var mousedown=false;var check = 0;var choose;
                var allData;
                $("body").on("mousedown", "#"+table_id+" tbody tr",function(e){
					allData = $("#"+table_id).bootstrapTable("getData");
                    mousedown=true;
                    check = $(this).index();
                    var check_element =allData[check];
                    choose = check_element[0];
                });
                $("body").on("mouseup", "#"+table_id+" tbody tr",function(e){
                    if(mousedown){
                        var mousedown_check = $(this).index();
                        var big = check>mousedown_check?check:mousedown_check;
                        var small = check<mousedown_check?check:mousedown_check;
                        if(big == small){
                            if(choose) $("#"+table_id).bootstrapTable('uncheck', big);
                            else $("#"+table_id).bootstrapTable('check', big);
                        }else{
                            var id_list = [];
                            for( var i=small;i<=big;i++){
                                id_list.push(allData[i].WRD_ID);
                            }
                            if(choose){
                                if(id_list.length>0) $("#"+table_id).bootstrapTable("uncheckBy", {field:"WRD_ID", values:id_list})
                            }else{
                                if(id_list.length>0) $("#"+table_id).bootstrapTable("checkBy", {field:"WRD_ID", values:id_list})
                            }
                        }
                    }
                    mousedown=false;
                });
            }
        },
        xddh: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/store_out_detail/xddh' + location.search,
                    table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PT_Num',
                // sortName: 'TD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PT_Num', title: '下达单号', operate: false},
                        {field: 'p.PT_Num', title: '下达单号', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 'd.T_Num', title: '任务单号', operate: 'LIKE',visible: false},
                        {field: 'TD_ID', title: 'TD_ID', operate: false, visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'C_ProjectName', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'd.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'PT_Number', title: '基数', operate: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: 'LIKE'},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    if(tableContent[0]["WR_Num"]!=0){
                        var index = layer.confirm("该下达单号已做过领料车间，是否继续？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            Fast.api.close(tableContent[0]);
                            layer.close(index);
                        })
                    }else{
                        Fast.api.close(tableContent[0]);
                    }
                }
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                $(document).on('click', "#allDel", function(env){
                    var num = Config.ids;
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/material/store_out_detail/allDel',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        parent.location.reload();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        layer.msg("删除失败");
                    }
                })
                $(document).on('click', "#xddh", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback: function (value){
                            $("#c-SO_ProjectName").val(value.C_ProjectName);
                            $("#c-C_Num").val(value.C_Num);
                            $("#c-PC_Num").val(value.PC_Num);
                            $("#c-T_Num").val(value.T_Num);
                            $("#c-SO_TowerType").val(value.TD_TypeName);
                            $("#c-SO_EType").val(value.TD_Pressure);
                            $("#c-PT_Num").val(value.PT_Num);
                        }
                    };
                    Fast.api.open('chain/material/store_out_detail/xddh',"选择下达单号",options);
                })
                $(document).on('click', "#lyr", function(e){
                    var url = "chain/sale/project_cate_log/selectSaleMan";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            // console.log(value);
                            $("#c-E_Name").val(value.E_Name);
                        }
                    };
                    Fast.api.open(url,"选择领料人",options);
                })
                $(document).on('click', "#bgy", function(e){
                    var url = "chain/sale/project_cate_log/selectSaleMan";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            console.log(value);
                            $("#c-E_NameBao").val(value.E_Name);
                        }
                    };
                    Fast.api.open(url,"选择领料人",options);
                })
                //选择入库单
                $(document).on('click', "#chooseIn", function(e){
                    var warehouse = $("#c-SO_WareHouse").val();
                    if(warehouse == false){
                        layer.msg("请选择仓库！");
                        return false;
                    }
                    var url='chain/material/store_state_check/chooseMaterialWare/warehouse/'+warehouse;
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var content = "";
                            var tableField = Config.tableField;
                            $.each(value,function(v_index,v_e){
                                content += '<tr><td>0</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                $.each(tableField,function(t_index,e){
                                    if(e[6]=="SSCD_Length" || e[6]=="SSCD_Width") content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[6]]) == 'undefined'?e[7]:(v_e[e[6]])/1000)+'"></td>';
                                    else content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[6]]) == 'undefined'?e[7]:v_e[e[6]])+'"></td>';
                                });
                                content += "</tr>";
                            });
                            $("#tbshow").append(content);
                            var col = 0;
                            $("#tbshow").find("tr").each(function () {
                                col++;
                                $(this).children('td').eq(0).text(col);
                            });
                        }
                    };
                    Fast.api.open(url,"选择材料",options);
                })
                
                $(document).on('click', "#chooseWork", function(e){
                    var warehouse = $("#c-SO_WareHouse").val();
                    if(warehouse == false){
                        layer.msg("请选择仓库！");
                        return false;
                    }
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback: function (value){
                            $("#tbshow").html("");
                            $("#c-E_Name").val(value[1]['WR_Person']??'');
                            $("#c-PC_Num").val(value[1]['PC_Num']);
                            $("#c-SO_ProjectName").val(value[1]['t_project']);
                            $("#c-C_Num").val(value[1]['C_Num']);
                            $("#c-T_Num").val(value[1]['T_Num']);
                            $("#c-SO_EType").val(value[1]['TD_Pressure']);
                            $("#c-SO_TowerType").val(value[1]['TD_TypeName']);
                            $("#c-PT_Num").val(value[1]['PT_Num']);
                            $("#c-SO_C_Name").val(value[1]['Customer_Name']);
                            $.ajax({
                                url: 'chain/material/store_out_detail/detailWorkIn',
                                type: 'post',
                                dataType: 'json',
                                data: {data: JSON.stringify(value[0])},
                                success: function (ret) {
                                    var content_array = [];
                                    var tableField = Config.tableField;
                                    var content="";
                                    if(ret.code==1){
                                        content_array = ret.data;
                                    }
                                    $.each(content_array,function(v_index,v_e){
                                        content += '<tr><td>0</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                        $.each(tableField,function(t_index,e){
                                            if(e[1]=="SOD_Length" || e[1]=="SOD_Width") content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?e[7]:(v_e[e[1]])/1000)+'"></td>';
                                            else content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?e[7]:v_e[e[1]])+'"></td>';
                                        });
                                        content += "</tr>";
                                    });
                                    $("#tbshow").append(content);
                                    var col = 0;
                                    $("#tbshow").find("tr").each(function () {
                                        col++;
                                        $(this).children('td').eq(0).text(col);
                                    });
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }
                    };
                    Fast.api.open('chain/material/store_out_detail/chooseWork/warehouse/'+warehouse,"选择车间领料单",options);
                })

                $(document).on('click', ".del", function(e){
                    // var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tr').find("input[name='table_row[SOD_ID][]']").val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/material/store_out_detail/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                        var col = 0;
                                        $("#tbshow").find("tr").each(function () {
                                            col++;
                                            $(this).children('td').eq(0).text(col);
                                        });
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                        var col = 0;
                        $("#tbshow").find("tr").each(function () {
                            col++;
                            $(this).children('td').eq(0).text(col);
                        });
                    }
                });

                $('#tbshow').on('keyup','td input[name="table_row[SOD_Count][]"]',function () {
                    get_weight($(this));
                })

                function get_weight(content){
                    var weight = 0;
                    var restCount = content.parents('tr').find("input[name='table_row[SID_RestCount][]']").val().trim();
                    var restWeight = content.parents('tr').find("input[name='table_row[SID_RestWeight][]']").val().trim();
                    var number = content.parents('tr').find("input[name='table_row[SOD_Count][]']").val().trim();
                    number = number==''?0:parseFloat(number);
                    if(restCount>0 && restWeight>0){
                        weight = (restWeight/restCount*number).toFixed(2);
                    }else{
                        var length = content.parents('tr').find("input[name='table_row[SOD_Length][]']").val().trim();
                        var width = content.parents('tr').find("input[name='table_row[SOD_Width][]']").val().trim();
                        
                        var bz = content.parents('tr').find("input[name='table_row[IM_PerWeight][]']").val().trim();
                        var IM_Class = content.parents('tr').find("input[name='table_row[IM_Class][]']").val().trim();
                        var IM_Spec = content.parents('tr').find("input[name='table_row[IM_Spec][]']").val().trim();
                        length = length==''?0:parseFloat(length);
                        width = width==''?0:parseFloat(width);
                        
                        bz = bz==''?0:parseFloat(bz);
                        if(IM_Class == '角钢'){
                            weight = (length*bz*number).toFixed(2);
                        }else if(IM_Class == "槽钢"){
                            weight = (length*bz*number).toFixed(2);
                        }else if(IM_Class == "钢板"){
                            weight = (length*width*Math.abs(IM_Spec)*bz*number).toFixed(2);
                        }else if(IM_Class == "钢管"){
                            var rep = /\d+\.?\d+/g;
                            var bj = rep.exec(IM_Spec);
                            var big_r = typeof(bj[0]) == 'undefined'?0:(bj[0]/2*0.001);
                            var small_r = typeof(bj[1]) == 'undefined'?0:(bj[1]/2*0.001);
                            weight = (length*3.14159*(big_r*big_r-small_r*small_r)*7850*number).toFixed(2);
                        }else if(IM_Class == "圆钢"){
                            var rep = /\d+\.?\d+/g;
                            var bj = rep.exec(IM_Spec);
                            var big_r = typeof(bj[0]) == 'undefined'?0:bj[0];
                            weight = (big_r*big_r*length*0.00617).toFixed(2);
                        }else if(IM_Class == "格栅板"){
                            // var rou = {"G255/40/100W":32.1,"G253/40/100W":21.3};
                            var rou_one = IM_Spec=="G255/40/100W"?32.1:21.3;
                            weight = round(rou_one * length*width,4);
                        }
                    }
                    content.parents('tr').find("input[name='table_row[SOD_Weight][]']").val(weight);
                    content.parents('tr').find("input[name='table_row[SOD_BWeight][]']").val(weight);
                }
                $(document).on("click", "#author", function () {
                    var num = Config.ids;
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/material/store_out_detail/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = Config.ids;
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/material/store_out_detail/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
            }
        }
    };
    return Controller;
});