define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree'], function ($, undefined, Backend, Table, Form, undefined) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/store_state_check/index' + location.search,
                    add_url: 'chain/material/store_state_check/add',
                    edit_url: 'chain/material/store_state_check/edit',
                    // del_url: 'chain/material/store_state_check/del',
                    multi_url: 'chain/material/store_state_check/multi',
                    import_url: 'chain/material/store_state_check/import',
                    table: 'storestatecheck',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SSC_Num',
                sortName: 'SSC_Date',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                // showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'SSC_Num', title: __('Ssc_num'), operate: false},
                        {field: 'ssc.SSC_Num', title: __('Ssc_num'), operate: 'LIKE',visible: false},
                        {field: 'SSC_Flag', title: "盘点类型", searchList:Config.ssc_list},
                        {field: 'SSC_Date', title: __('Ssc_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'SSC_WareHouse', title: __('Ssc_warehouse'), searchList:Config.wareclassList},
                        {field: 'IM_Class', title: '材料名称', searchList:Config.inventoryList},
                        {field: 'L_Name', title: '材质', searchList:Config.limberList},
                        {field: 'IM_Spec', title: '规格', operate: '='},
                        {field: 'SSCD_Length', title: '长度(mm)', operate: false},
                        {field: 'SSCD_Width', title: '宽度(mm)', operate: false},
                        {field: 'SSCD_Count', title: '数量', operate: false},
                        {field: 'SSCD_Weight', title: '重量(kg)', operate: false},
                        // {field: 'SSCD_Price', title: '含税单价(元/吨)', operate: false},
                        // {field: 'SSCD_NoPrice', title: '不含税单价(元/吨)', operate: false},
                        // {field: 'SSCD_Money', title: '含税金额(元)', operate: false},
                        // {field: 'SSCD_NoMoney', title: '不含税金额(元)', operate: false},
                        {field: 'LuPiHao', title: '炉号', operate: 'LIKE'},
                        {field: 'PiHao', title: '批号', operate: 'LIKE'},
                        {field: 'testnum', title: '试验编号', operate: 'LIKE'},
                        {field: 'SSCD_Memo', title: '备注', operate: false},
                        {field: 'is_check', title: '状态', searchList:{1:"未审核",2:"已审核"}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            Controller.api.bindevent("edit");
        },
        choosematerialware: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/store_state_check/chooseMaterialWare/warehouse/'+ Config.warehouse + location.search,
                    table: 'storein',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                // pk: 'SSC_Num',
                // sortName: 'SSC_Date',
                search: false,
                showToggle: false,
                showExport: false,
                onlyInfoPagination: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'SID_ID', title: '入库id', operate: false},
                        {field: 'SI_WareHouse', title: '所在仓库', operate: false,visible: false},
                        {field: 'SI_InDate', title: "入库日期", defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'IM_Class', title: '材料名称', searchList:Config.inventoryList},
                        {field: 'L_Name', title: '材质', searchList:Config.limberList},
                        {field: 'IM_Spec', title: '规格', operate: '='},
                        {field: 'SID_Length', title: '长度(mm)', operate: "="},
                        {field: 'SID_Width', title: '宽度(mm)', operate: "="},
                        {field: 'SSCD_Count', title: '数量', operate: false},
                        {field: 'SSCD_Weight', title: '重量(kg)', operate: false},

                        {field: 'SID_RestCount', title: '剩余数量', operate: false},
                        {field: 'SID_RestWeight', title: '剩余重量(kg)', operate: false},
                        // {field: 'SSCD_Price', title: '含税单价(元/吨)', operate: false},
                        // {field: 'SSCD_NaxPrice', title: '不含税单价(元/吨)', operate: false},
                        // {field: 'SSCD_Money', title: '含税金额(元)', operate: false},
                        // {field: 'SSCD_NaxPrice', title: '不含税金额(元)', operate: false},
                        {field: 'LuPiHao', title: '炉号', operate: 'LIKE'},
                        {field: 'PiHao', title: '批号', operate: 'LIKE'},
                        {field: 'testnum', title: '试验编号', operate: 'LIKE'},
                        // {field: 'IM_Rax', title: '税率', operate: 'LIKE'},
                        {field: 'IM_PerWeight', title: '比重', operate: false, visible: false},
                        {field: 'IM_Num', title: '存货编码', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        // choosematerialkc: function () {
        //     $('#treeview').jstree({
        //         'core' : {
        //                 'data' : {
        //                 'url' : 'jichu/ch/inventory_material/companytree',
        //                 'data' : function (node) {
        //                     return { 'id' : node.id };
        //                 }
        //             }
        //         }
        //     });
        //     // 初始化表格参数配置
        //     Table.api.init({
        //         extend: {
        //             index_url: 'chain/material/store_state_check/chooseMaterialKc/warehouse/'+ Config.warehouse + location.search,
        //             table: 'storestate',
        //         }
        //     });

        //     var table = $("#table");

        //     // 初始化表格
        //     table.bootstrapTable({
        //         url: $.fn.bootstrapTable.defaults.extend.index_url,
        //         // pk: 'ID',
        //         sortName: 'IM_Num',
        //         sortOrder: 'ASC',
        //         search: false,
        //         showToggle: false,
        //         showExport: false,
        //         pagination: false,
        //         limit: 10000,
        //         columns: [
        //             [
        //                 {checkbox: true},
        //                 {field: 'SS_WareHouse', title: '所在仓库', operate: false},
        //                 {field: 'IM_Num', title: '存货编码', operate: false},
        //                 {field: 'IM_Class', title: '存货名称', operate: false},
        //                 {field: 'L_Name', title: '材质', operate: false},
        //                 {field: 'IM_Spec', title: '规格', operate: false},
        //                 {field: 'SSCD_Length', title: '长度(mm)', operate: false},
        //                 {field: 'SSCD_Width', title: '宽度(mm)',operate: false},
        //                 {field: 'SS_Count', title: '库存数量',operate: false},
        //                 {field: 'SS_Weight', title: '库存重量',operate: false}
        //             ]
        //         ]
        //     });


        //     // 为表格绑定事件
        //     Table.api.bindevent(table);
        //     $('#treeview').on('changed.jstree', function (e, data) {
        //         console.log(data);
        //         var dd_num = data.selected[0];
        //         var options = table.bootstrapTable('getOptions');
        //         options.pageNumber = 1;
        //         options.queryParams = function (params) {
        //             return {
        //                 search: params.search,
        //                 sort: params.sort,
        //                 order: params.order,
        //                 offset: params.offset,
        //                 limit: params.limit,
        //                 filter: JSON.stringify({'im.IM_Num': dd_num}),
        //                 op: JSON.stringify({'im.IM_Num': 'LIKE'}),
        //             };
        //         };
        //         table.bootstrapTable('refresh', {});
        //         return false;
                
        //     }).jstree();
        //     $(document).on("click", "#sure", function () {
        //         var tableContent = table.bootstrapTable('getSelections');
        //         if(tableContent.length == 0) layer.msg('请选择其中一项再确定提交');
        //         else Fast.api.close(tableContent);
        //     });

        // },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                    }
                    return false;
                });
                var tableField = Config.tableField;
                $(document).on('change', "#c-SSC_Flag", function(e){
                    // console.log(e);
                    var choose_flag = $("#c-SSC_Flag").val();
                    $.ajax({
                        url: 'chain/material/store_state_check/tableHead',
                        type: 'post',
                        dataType: 'json',
                        data: {num: choose_flag},
                        success: function (ret) {
                            var tableFieldContent = "";
                            if(ret.code == 1){
                                tableFieldContent = ret.data.tableFieldContent;
                                tableField = ret.data.tableField;
                            }
                            $("#tbhead").html(tableFieldContent);
                            $("#tbshow").html("");
                            
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                })

                $(document).on('click', "#allDel", function(env){
                    var num = Config.ids;
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/material/store_state_check/allDel',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        parent.location.reload();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        layer.msg("删除失败");
                    }
                })

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(1).find('input').val();
                    if(num!=false){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/material/store_state_check/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                });

                $(document).on('click', "#chooseMaterial", function(e){
                    var type = $("#c-SSC_Flag").val();
                    var warehouse = $("#c-SSC_WareHouse").val();
                    if(warehouse == false){
                        layer.msg("请选择仓库！");
                        return false;
                    }
                    if( type == 0 || type == 2){
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                var limber = Config.limberList;
                                var content = "";
                                $.each(value,function(v_index,v_e){
                                    content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                    $.each(tableField,function(t_index,e){
                                        if(e[1]=="L_Name"){
                                            content += '<td '+e[3]+'><input type="text" class="small_input" list="typelist" name="table_row['+e[1]+'][]" value="" placeholder="请选择"><datalist id="typelist">';
                                            $.each(limber,function(l_index,l_e){
                                                content += '<option value="'+ l_e +'">'+ l_e +'</option>';
                                            })
                                            content += "</datalist></td>";
                                        }
                                        else content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?'':v_e[e[1]])+'"></td>';
                                    });
                                    content += "</tr>";
                                });
                                $("#tbshow").append(content);
                            }
                        };
                        Fast.api.open('chain/purchase/requisition/chooseMaterial',"选择材料",options);
                    }else if( type == 1){
                        var url;
                        if(type == 1) url='chain/material/store_state_check/chooseMaterialWare/warehouse/'+warehouse;
                        // else url='chain/material/store_state_check/chooseMaterialKc/warehouse/'+warehouse;
                        var options = {
                            shadeClose: false,
                            shade: [0.3, '#393D49'],
                            area: ["100%","100%"],
                            callback:function(value){
                                var content = "";
                                $.each(value,function(v_index,v_e){
                                    content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                    $.each(tableField,function(t_index,e){
                                        content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?'':v_e[e[1]])+'"></td>';
                                    });
                                    content += "</tr>";
                                });
                                $("#tbshow").append(content);
                            }
                        };
                        Fast.api.open(url,"选择材料",options);
                    }
                })

                $(document).on('click', "#examine", function(e){
                    var num = Config.SSC_Num;
                    if(num!=false){
                        var index = layer.confirm("确定审核？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/material/store_state_check/examine',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    // $('#examine').removeAttr("disabled");
                                    // $('.btn-success').attr("disabled",true);
                                    // $('#abandonment').attr("disabled",true);
                                    if(ret.code==1){
                                        window.location.reload();
                                    }
                                    
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }
                });
                $(document).on('click', "#abandonment", function(e){
                    var num = Config.SSC_Num;
                    if(num!=false){
                        var index = layer.confirm("确定弃审？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/material/store_state_check/abandonment',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        window.location.reload();
                                    }
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }
                });

                $('#tbshow').on('keyup','td input[name="table_row[SSCD_Length][]"]',function () {
                    get_weight($(this));
                })
                $('#tbshow').on('keyup','td input[name="table_row[SSCD_Width][]"]',function () {
                    get_weight($(this));
                })
                $('#tbshow').on('keyup','td input[name="table_row[SSCD_Count][]"]',function () {
                    get_weight($(this));
                })

                function get_weight(content){
                    var length = content.parents('tr').find("input[name='table_row[SSCD_Length][]']").val().trim();
                    var width = content.parents('tr').find("input[name='table_row[SSCD_Width][]']").val().trim();
                    var number = content.parents('tr').find("input[name='table_row[SSCD_Count][]']").val().trim();
                    var bz = content.parents('tr').find("input[name='table_row[IM_PerWeight][]']").val().trim();
                    var IM_Class = content.parents('tr').find("input[name='table_row[IM_Class][]']").val().trim();
                    var IM_Spec = content.parents('tr').find("input[name='table_row[IM_Spec][]']").val().trim();
                    length = length==''?0:parseFloat(length)*0.001;
                    width = width==''?0:parseFloat(width)*0.001;
                    number = number==''?0:parseInt(number);
                    bz = bz==''?0:parseFloat(bz);
                    var weight=0;
                    if(IM_Class == '角钢'){
                        weight = (length*bz*number).toFixed(2);
                    }else if(IM_Class == "槽钢"){
                        weight = (length*bz*number).toFixed(2);
                    }else if(IM_Class == "钢板"){
                        weight = (length*width*Math.abs(IM_Spec)*bz*number).toFixed(2);
                    }else if(IM_Class == "钢管"){
                        var rep = /\d+\.?\d+/g;
                        var bj = rep.exec(IM_Spec);
                        var big_r = typeof(bj[0]) == 'undefined'?0:(bj[0]/2*0.001);
                        var small_r = typeof(bj[1]) == 'undefined'?0:(bj[1]/2*0.001);
                        weight = (length*3.14159*(big_r*big_r-small_r*small_r)*7850*number).toFixed(2);
                    }else if(IM_Class == "圆钢"){
                        var rep = /\d+\.?\d+/g;
                        var bj = rep.exec(IM_Spec);
                        var big_r = typeof(bj[0]) == 'undefined'?0:bj[0];
                        weight = (big_r*big_r*length*0.00617).toFixed(2);
                    }else if(IM_Class == "格栅板"){
                        // var rou = {"G255/40/100W":32.1,"G253/40/100W":21.3};
                        var rou_one = IM_Spec=="G255/40/100W"?32.1:21.3;
                        weight = round(rou_one * length*width,4);
                    }
                    content.parents('tr').find("input[name='table_row[SSCD_Weight][]']").val(weight);
                }

                var kj_limberList = Config.kj_limberList;
                $('#tbshow').on('keyup','td input[name="table_row[L_Name][]"]',function () {
                    var text_count = $(this).parents('tr').find("input[name='table_row[L_Name][]']").val().trim();
                    if(kj_limberList[text_count] != undefined ) $(this).parents('tr').find("input[name='table_row[L_Name][]']").val(kj_limberList[text_count])
                })
            }
        }
    };
    return Controller;
});