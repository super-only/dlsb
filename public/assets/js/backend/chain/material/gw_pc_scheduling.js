define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/gw_pc_scheduling/index' + location.search,
                    add_url: 'chain/material/gw_pc_scheduling/add',
                    edit_url: 'chain/material/gw_pc_scheduling/edit',
                    del_url: 'chain/material/gw_pc_scheduling/del',
                    multi_url: 'chain/material/gw_pc_scheduling/multi',
                    import_url: 'chain/material/gw_pc_scheduling/import',
                    table: 'gw_pc_scheduling',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'g_pc_num',
                sortName: 'g_pc_num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'g_pc_num', title: __('G_pc_num'), operate: 'LIKE'},
                        {field: 'PT_Num', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'planStartDate', title: __('Planstartdate')},
                        {field: 'planFinishDate', title: __('Planfinishdate')},
                        {field: 'write_time', title: __('Write_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'actualStartDate', title: __('Actualstartdate')},
                        {field: 'actualFinishDate', title: __('Actualfinishdate')},
                        {field: 'remark', title: "备注", operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
            $(document).on('click', "#selectPtNum", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback: function (value){
                        $("#c-PT_Num").val(value.PT_Num);
                    }
                };
                Fast.api.open('chain/material/nesting/xddh',"选择下达单号",options);
            })
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});