define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'tldata', 'clzldata'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/nesting/index' + location.search,
                    add_url: 'chain/material/nesting/add',
                    edit_url: 'chain/material/nesting/edit',
                    del_url: 'chain/material/nesting/del',
                    multi_url: 'chain/material/nesting/multi',
                    import_url: 'chain/material/nesting/import',
                    table: 'nesting',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'pt_num', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'warehouse', title: __('Warehouse'), searchList:Config.wareList},
                        {field: 'TD_TypeName', title: '塔型名称', operate: 'LIKE'},
                        // {field: 'pc_flag', title: '是否排产', searchList:{0:"否",1:"是"},formatter:function(value,row,index){
                        //     return value?"是":"否";
                        // }},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        // {field: 'auditor', title: __('Auditor'), operate: false},
                        // {field: 'auditor_time', title: __('Auditor_time'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on('click', "#editLoss", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"]
                };
                Fast.api.open('chain/material/nesting_loss/index',"修改刀损",options);
            })

        },
        add: function () {
            Controller.api.bindevent("add");
            $(document).on('click', "#xddh", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback: function (value){
                        $("#c-tower_name").val(value.TD_TypeName);
                        $("#c-pt_num").val(value.PT_Num);
                        $("#c-td_id").val(value.TD_ID);
                        $("#c-trnm_num").val('');
                    }
                };
                Fast.api.open('chain/material/nesting/xddh',"选择下达单号",options);
            })
            $(document).on('click', "#bjdh", function(e){
                var pt_num  = $("#c-pt_num").val();
                var td_id  = $("#c-td_id").val();
                if(!pt_num){
                    layer.msg("请先选择下达单号！");
                    return false;
                }
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback: function (value){
                        $("#c-trnm_num").val(value.TRNM_Num);
                    }
                };
                Fast.api.open('chain/material/nesting/bjdh/td_id/'+td_id,"选择补件单号",options);
            })
        },
        edit: function () {
            Controller.api.bindevent("edit");
                // var search_content = $("#chooseTable").bootstrapTable('getData');
                // $.each(search_content,function(e,index){
                //     if(e==this_index) search_content.splice(e,1);
                // })
            function getNoTable($type=0)
            {
                var left_height = pp_table.bootstrapTable('getScrollPosition');
                var l_name = $("#select-lname").val();
                var spec = $("#select-spec").val();
                Fast.api.ajax({
                    url: "chain/material/nesting/detailNoTable", 
                    data: {
                        "ids":Config.ids,
                        "l_name":l_name,
                        "spec":spec
                    }
                }, function(data, ret){
                    table.bootstrapTable('load',data.notable);
                    pp_table.bootstrapTable('load',data.ptable);
                    if($type) pp_table.bootstrapTable('scrollTo', left_height);
                    // else pp_table.bootstrapTable('scrollTo', 'bottom');
                    multiple_checks("no_table");
                    multiple_checks("table");
                });
            }
            var table = $("#no_table");
            var pp_table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: [],
                // data: Config.no_table,
                height:document.body.clientHeight-170,
                columns: [
                    [
                        {checkbox:true, field: 'choose'},
                        {field: 'id', title: 'id',visible: false},
                        {field: 'DtMD_sPartsID', title: '部件号', sortable: true},
                        {field: 'DtMD_sStuff', title: '名称', sortable: true},
                        {field: 'DtMD_sMaterial', title: '材质', sortable: true},
                        {field: 'DtMD_sSpecification', title: '规格', sortable: true},
                        {field: 'DtMD_iLength', title: '长度', sortable: true},
                        {field: 'count', title: '剩余数量', sortable: true},
                    ]
                ]
            });
            // 初始化表格
            pp_table.bootstrapTable({
                data: [],
                // data: Config.no_table,
                height:document.body.clientHeight-170,
                columns: [
                    [
                        {checkbox:true, field: 'choose'},
                        {field: 'id', title: 'ID',visible: false},
                        {field: 'no', title: '序号',formatter: function(value,row,index){
                            return ++index;
                        }},
                        {field: 'pc_flag', title: '排产',formatter: function(value,row,index){
                            return value==1?"是":"否";
                        }},
                        // {field: 'im_num', title: '存货编号'},
                        {field: 'stuff', title: '材料名称'},
                        {field: 'material', title: '材质'},
                        {field: 'specification', title: '规格'},
                        {field: 'length', title: '长度(mm)'},
                        {field: 'count', title: '数量'},
                        {field: 'mate', title: '匹配结果(部件号/长度*数量)'},
                        {field: 'act_mate', title: '匹配结果(长度*数量)'},
                        {field: 'ds', title: '刀数'},
                        {field: 'dssh', title: '刀数损耗'},
                        {field: 'sycd', title: '使用长度'},
                        {field: 'yl', title: '余量'},
                        {field: 'sh', title: '损耗'},
                        {field: 'lyv', title: '利用率(%)'},
                        {field: 'shv', title: '损耗率(%)'},
                        // {field: 'remark', title: '备注'},
                    ]
                ]
            });
            getNoTable();
            $(document).on('click', "#generMaterial", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        Fast.api.ajax({
                            url: "chain/material/nesting/saveMaterial", 
                            data: {"ids":Config.ids,"tableField":JSON.stringify(value)}
                        }, function(data, ret){
                            getNoTable();
                        });
                    }
                };
                Fast.api.open('chain/material/nesting/generMaterial/ids/'+Config.ids,"选择材料",options);
            })
            $(document).on('click', "#auto_tl", function(e){
                var noTable = table.bootstrapTable("getSelections");
                if(noTable.length==0) return false;
                Fast.api.ajax({
                    url: "chain/material/nesting/autoNesting", 
                    data: {"ids":Config.ids,"tableField":JSON.stringify(noTable)}
                }, function(data, ret){
                    getNoTable();
                });
            })
            $(document).on('click', "#manual_tl", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        if(value.length==0) return false;
                        
                        Fast.api.ajax({
                            url: "chain/material/nesting/manualNesting", 
                            data: {"ids":Config.ids,"tableField":JSON.stringify(value)}
                        }, function(data, ret){
                            getNoTable();
                            pp_table.bootstrapTable('scrollTo', 'bottom');
                        });
                    }
                };
                Fast.api.open('chain/material/nesting/manualMaterial/ids/'+Config.ids,"选择材料",options);
            })

            $(document).on('click', "#select-sure", function(e){
                getNoTable(1);
            })

            pp_table.on('dbl-click-row.bs.table',function(row,$elemente){
                choose_table = $elemente;
                var value_length = choose_table["length"];
                var value_count = choose_table["count"];
                var mate = choose_table["mate"];
                var mate_table = mate.split('+');
                var string_text = '';
                var xg_index,xh_index;
                //找到最后一个/和最后一个*
                $.each(mate_table,function(index,value){
                    xg_index = value.lastIndexOf('/');
                    xh_index = value.lastIndexOf('*');
                    string_text += value.substring(0,xg_index)+','+value.substring(xh_index+1)+'\n';
                });
                layer.open({
                    //formType: 2,//这里依然指定类型是多行文本框，但是在下面content中也可绑定多行文本框在这里插入代码片
                    title: '修改',
                    area: ['50%', '60%'],
                    btnAlign: 'c',
                    closeBtn:'1',//右上角的关闭
                    content: `<div><p>请填入长度:</p><input id="cl_length" type="text" style="width:100%" value="`+value_length+`"></input><p>请填入数量:</p><input id="cl_count" type="text" style="width:100%" value="`+value_count+`"></input><p>请填入(编号,数量):</p><textarea name="txt_remark" id="remark" style="width:100%">`+string_text+`</textarea></div>`,
                    btn:['确认','取消','关闭'],
                    yes: function (index, layero) {
                        value_length = $('#cl_length').val();
                        value_count = $('#cl_count').val();
                        value1 = $('#remark').val();//获取多行文本框的值
                        if(value1.length == 0 || !value1){
                            alert('请输入值');
                            return false;
                        }else{
                            var re = new RegExp("，","g"); //定义正则表达式
                            //第一个参数是要替换掉的内容，第二个参数"g"表示替换全部（global）。
                            value1 = value1.replace(re, ",");
                            var value_arr = value1.split('\n');
                            var part_number_arr = [];
                            var part_str='';
                            var material='';
                            var specification='';
                            var name = ''
                            var flag = 1;
                            // var 
                            if(value_arr.length == 0){
                                layer.msg('请输入正确值');
                                flag=0;
                                // return false;
                            }else{
                                $.each(value_arr,function(index,value){
                                    if(value.length != 0){
                                        part_number_arr = value.split(',');
                                        if(part_number_arr==''){
                                            layer.msg('部件号不能为空');
                                            flag=0;
                                            // return false;
                                        }else if(Config.partList[part_number_arr[0]] == undefined){
                                            layer.msg('不存在部件号'+part_number_arr[0]+',请输入正确部件号');
                                            flag=0;
                                            // return false;
                                        }else{
                                            // console.log(part_number_arr,part_number_arr[0],Config.partList[part_number_arr[0]]);
                                            material = material?material:Config.partList[part_number_arr[0]]["material"];
                                            specification = specification?specification:Config.partList[part_number_arr[0]]["specification"];
                                            name = name?name:Config.partList[part_number_arr[0]]["IM_Class"];
                                            
                                            if(name!=Config.partList[part_number_arr[0]]["IM_Class"]){
                                                layer.msg('输入部件号不是同一种名称，请检查后重新输入');
                                                // console.log(name,Config.partList[part_number_arr[0]]["IM_Class"]);
                                                flag=0;
                                                // return false;
                                            }
                                            if(material!=Config.partList[part_number_arr[0]]["material"]){
                                                layer.msg('输入部件号不是同一种材质，请检查后重新输入');
                                                // console.log(material,Config.partList[part_number_arr[0]]["material"]);
                                                flag=0;
                                                // return false;
                                            }
                                            if(specification!=Config.partList[part_number_arr[0]]["specification"]){
                                                layer.msg('输入部件号不是同一种规格，请检查后重新输入');
                                                // console.log(specification,Config.partList[part_number_arr[0]]["specification"]);
                                                flag=0;
                                                // return false;
                                            }

                                            if(part_number_arr[1]==undefined || !part_number_arr[1]) number = '1';
                                            else number = part_number_arr[1];
                                            part_str += '+'+part_number_arr[0]+'/'+ Config.partList[part_number_arr[0]]["length"]+'*'+number;
                                        }
                                    }
                                })
                                if(part_str.length!=0) part_str = part_str.substring(1);
                            }
                            if(flag){
                                var table_append = {
                                    "stuff" : name,
                                    "material" : material,
                                    "specification" : specification,
                                    "length" : value_length,
                                    "count" : value_count,
                                    "mate" : part_str,
                                    "remark" : ''
                                };

                                Fast.api.ajax({
                                    url: "chain/material/nesting/editUpdate", 
                                    data: {"nesting_ids":choose_table["id"],"tableField":JSON.stringify(table_append)}
                                }, function(data, ret){
                                    if(ret.code == 1) getNoTable(1);
                                    else layer.msg(ret.msg);
                                    layer.close(index);
                                });
                            }
                        }
                        
                    },
                    no:function(index){
                        layer.close(index);
                        return false;//点击按钮按钮不想让弹层关闭就返回false
                    },
                    close:function(index){
                        return false;//点击按钮按钮不想让弹层关闭就返回false
                    }
                    
                });
            });

            $(document).on('click', "#del", function(e){
                var choose_table = pp_table.bootstrapTable('getSelections');
                if(choose_table.length == 0) return false;
                else if(choose_table.length == 1 && choose_table[0]["pc_flag"]==1){
                    var index = layer.confirm("该套料信息已经排产，确定删除？", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        Fast.api.ajax({
                            url: "chain/material/nesting/editPcDel", 
                            data: {"tableField":JSON.stringify(choose_table)}
                        }, function(data, ret){
                            if(ret.code != 1) layer.msg(ret.msg);
                            getNoTable(1);
                        });
                        layer.close(index);
                    })
                }else{
                    Fast.api.ajax({
                        url: "chain/material/nesting/editDel", 
                        data: {"tableField":JSON.stringify(choose_table)}
                    }, function(data, ret){
                        if(ret.code != 1) layer.msg(ret.msg);
                        getNoTable(1);
                    });
                }
                
            });
            $(document).on("click", "#pc", function () {
                var choose_table = pp_table.bootstrapTable('getSelections');
                if(choose_table.length == 0) return false;
                else{
                    Fast.api.ajax({
                        url: "chain/material/nesting/schedulingRelease", 
                        data: {"ids": Config.ids,"tableField":JSON.stringify(choose_table)}
                    }, function(data, ret){
                    });
                }
            });
            $(document).on("click", "#total", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                };
                Fast.api.open('chain/material/nesting/total/ids/'+Config.ids,"材料整理",options);
            });
            $(document).on("click", "#print", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                };
                Fast.api.open('chain/material/nesting/print/ids/'+Config.ids,"套料打印",options);
            });



            $(document).on('click', "#chooseMaterial", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        var tableField = Config.field;
                        var limber = Config.limber_list;
                        var content = "";
                        $.each(value,function(v_index,v_e){
                            content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                            $.each(tableField,function(t_index,e){
                                if(e[2]=="select"){
                                    content += '<td><input type="text" class="small_input" list="typelist" name="table_row['+e[1]+'][]" value="" placeholder="请选择"><datalist id="typelist">';
                                    $.each(limber,function(l_index,l_e){
                                        content += '<option value="'+ l_e +'">'+ l_e +'</option>';
                                    })
                                    content += "</datalist></td>";
                                }else{
                                    var table_field_value = '';
                                    if(e[1]=='im_num') table_field_value = (typeof(v_e['IM_Num']) == 'undefined'?e[4]:v_e['IM_Num']);
                                    else if(e[1]=='stuff') table_field_value = (typeof(v_e['IM_Class']) == 'undefined'?e[4]:v_e['IM_Class']);
                                    else if(e[1]=='specification') table_field_value = (typeof(v_e['IM_Class']) == 'undefined'?e[4]:v_e['IM_Spec']);
                                    content += '<td><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+table_field_value+'"></td>';
                                }
                            });
                            content += "</tr>";
                        });
                        $("#tbshow").append(content);
                    }
                };
                Fast.api.open('chain/purchase/requisition/chooseMaterial',"选择材料",options);
            })

            function multiple_checks(table_id='leftPartTable')
            {
                var mousedown=false;var check = 0;var choose;
                var allData;
                $("body").on("mousedown", "#"+table_id+" tbody tr",function(e){
					allData = $("#"+table_id).bootstrapTable("getData");
                    mousedown=true;
                    check = $(this).index();
                    var check_element =allData[check];
                    choose = check_element.choose == undefined?false:check_element.choose;
                });
                $("body").on("mouseup", "#"+table_id+" tbody tr",function(e){
                    if(mousedown){
                        var mousedown_check = $(this).index();
                        var big = check>mousedown_check?check:mousedown_check;
                        var small = check<mousedown_check?check:mousedown_check;
                        if(big == small){
                            if(choose) $("#"+table_id).bootstrapTable('uncheck', big);
                            else $("#"+table_id).bootstrapTable('check', big);
                        }else{
                            var id_list = [];
                            for( var i=small;i<=big;i++){
                                id_list.push(allData[i].id);
                            }
                            if(choose){
                                if(id_list.length>0) $("#"+table_id).bootstrapTable("uncheckBy", {field:"id", values:id_list})
                            }else{
                                if(id_list.length>0) $("#"+table_id).bootstrapTable("checkBy", {field:"id", values:id_list})
                            }
                        }
                    }
                    mousedown=false;
                });
            }


            $(document).on("click", "#save-detail", function () {
                var filterData = $("#edit-form").serializeArray();
                $.ajax({
                    url: 'chain/material/nesting/edit/ids/'+Config.ids,
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(filterData)},
                    async: false,
                    success: function (ret) {
                        if(ret.code==1){
                            window.location.href = "edit/ids/"+Config.ids;
                        }else{
                            layer.msg(ret.msg); 
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#author", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/material/nesting/auditor",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('确定弃审？',"chain/material/nesting/giveUp",num);
                
            });
            function check(msg,url,num){
                // console.log(url,msg,num);return false;
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                window.location.reload();
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        xddh: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/nesting/xddh' + location.search,
                    table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PT_Num',
                // sortName: 'TD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PT_Num', title: '下达单号', operate: false},
                        {field: 'p.PT_Num', title: '下达单号', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 'd.T_Num', title: '任务单号', operate: 'LIKE',visible: false},
                        {field: 'TD_ID', title: 'TD_ID', operate: false, visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'C_ProjectName', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'd.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        bjdh: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/nesting/bjdh/td_id/'+Config.td_id + location.search,
                    table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PT_Num',
                // sortName: 'TD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'TRNM_Num', title: '补件单号', operate: false},
                        {field: 'trm.TRNM_Num', title: '补件单号', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 'd.T_Num', title: '任务单号', operate: 'LIKE',visible: false},
                        {field: 'TD_ID', title: 'TD_ID', operate: false, visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'C_Project', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'd.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        total: function () {
            var table = $("#table");
            table.bootstrapTable({
                data: Config.list,
                height: document.body.clientHeight-50,
                columns: [
                    [
                        // {checkbox: true},
                        {field: 'id', title: '序号',formatter: function(value,row,index){
                            return ++index;
                        }},
                        {field: 'stuff', title: '材料名称'},
                        {field: 'material', title: '材质'},
                        {field: 'specification', title: '规格'},
                        {field: 'length', title: '长度(mm)'},
                        {field: 'width', title: '宽度(mm)'},
                        {field: 'count', title: '数量'}
                    ]
                ]
            });
            $(document).on("click", "#print", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                };
                Fast.api.open('chain/material/nesting/zlprint/ids/'+Config.ids,"套料整理打印",options);
            });
        },
        manualmaterial: function ()  {
            var manual_table = $("#manual_table");
            var partList = Config.partList;
            var partResult = Config.part_result;
            var kcArr = Config.kcArr;
            manual_table.bootstrapTable({
                data: [],
                height: document.body.clientHeight /2,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'xh', title: '序号'},
                        {field: 'stuff', title: '材料名称'},
                        {field: 'material', title: '材质'},
                        {field: 'specification', title: '规格'},
                        {field: 'length', title: '长度(mm)'},
                        {field: 'count', title: '数量'},
                        {field: 'mate', title: '匹配结果(部件号/长度*数量)'},
                        {field: 'remark', title: '备注'},
                    ]
                ]
            });
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: [],
                search: false,
                showToggle: false,
                showExport: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight /2,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DtMD_sPartsID', title: '部件编号', operate: false, sortable: true},
                        {field: 'IM_Class', title: '材料名称', searchList:{"角钢":"角钢","钢板":"钢板"}, sortable: true},
                        {field: 'material', title: '材质', searchList:{"Q235B":"Q235B","Q235C":"Q235C","Q355B":"Q355B","Q355C":"Q355C","Q420B":"Q420B","Q420C":"Q420C"}, sortable: true},
                        {field: 'specification', title: '规格', operate: '=', sortable: true},
                        {field: 'length', title: '长度', operate: false, sortable: true},
                        // {field: 'width', title: '宽度', operate: false},
                        {field: 'count', title: '数量', operate: false, sortable: true},
                        {field: 'mate', title: '匹配结果', operate: false, visible: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            getNoTable();
            function getNoTable()
            {
                var l_name = $("#select-lname").val();
                var spec = $("#select-spec").val();
                Fast.api.ajax({
                    url: "chain/material/nesting/manualNoTable", 
                    data: {
                        "ids":Config.ids,
                        "l_name":l_name,
                        "spec":spec
                    }
                }, function(gtndata, gtnret){
                    partResult = gtndata.part_result;
                    eachCompact();
                });
            }
            $(document).on('click', "#select-sure", function(e){
                getNoTable();
            })
            function addLayerOpen(field='',keyfield=''){
                var openContent = `<form class="form-horizontal"><div class="form-group col-xs-7"><fieldset><legend>填写</legend><p>请填入长度:</p><input id="cl_length" type="text" style="width:100%"></input><p>请填入数量:</p><input id="cl_count" type="text" style="width:100%"></input><p>请填入(编号,数量):</p><textarea name="txt_remark" id="remark" style="width:100%">`+field+`</textarea></fieldset></div>`;
                if(field!=''){
                    openContent += `<div class="form-group col-xs-1"></div><div class="form-group col-xs-4"><fieldset><legend>库存情况(长度：数量)</legend><div style="height:250px;overflow-y: scroll;">`;
                    if(kcArr[keyfield]!=undefined){
                        for(var ki=0;ki<kcArr[keyfield].length;ki++){
                            openContent += "<p>"+kcArr[keyfield][ki][0]+":   "+kcArr[keyfield][ki][1]+"</p>";
                        }
                    }
                    openContent += `</div></fieldset></div>`;
                }
                openContent += `</form>`;
                layer.open({
                    //formType: 2,//这里依然指定类型是多行文本框，但是在下面content中也可绑定多行文本框在这里插入代码片
                    title: '添加',
                    area: ['50%', '60%'],
                    offset:'t',
                    btnAlign: 'c',
                    closeBtn:'1',//右上角的关闭
                    content: openContent,
                    btn:['确认','取消','关闭'],
                    yes: function (index, layero) {
                        var value_length = $('#cl_length').val();
                        var value_count = $('#cl_count').val();
                        var value1 = $('#remark').val();//获取多行文本框的值
                        if(value_length.length == 0 || value_count.length==0){
                            alert('请输入值');
                            return false;
                        }
                        if(value1.length == 0 || !value1){
                            alert('请输入值');
                            return false;
                        }else{
                            var re = new RegExp("，","g"); //定义正则表达式
                            //第一个参数是要替换掉的内容，第二个参数"g"表示替换全部（global）。
                            value1 = value1.replace(re, ",");
                            var value_arr = value1.split('\n');
                            var part_number_arr = [];
                            var part_str='';
                            var material='';
                            var specification='';
                            var name = ''
                            var flag = 1;
                            // var 
                            if(value_arr.length == 0){
                                alert('请输入正确值');
                                flag=0;
                                // return false;
                            }else{
                                $.each(value_arr,function(index,value){
                                    if(value.length != 0){
                                        part_number_arr = value.split(',');
                                        if(part_number_arr==''){
                                            alert('部件号不能为空');
                                            flag=0;
                                            // return false;
                                        }else if(partList[part_number_arr[0]] == undefined){
                                            alert('不存在部件号'+part_number_arr[0]+',请输入正确部件号');
                                            flag=0;
                                            // return false;
                                        }else{
                                            // console.log(part_number_arr,part_number_arr[0],partList[part_number_arr[0]]);
                                            material = material?material:partList[part_number_arr[0]]["material"];
                                            specification = specification?specification:partList[part_number_arr[0]]["specification"];
                                            name = name?name:partList[part_number_arr[0]]["IM_Class"];
                                            
                                            if(name!=partList[part_number_arr[0]]["IM_Class"]){
                                                alert('输入部件号不是同一种名称，请检查后重新输入');
                                                // console.log(name,partList[part_number_arr[0]]["IM_Class"]);
                                                flag=0;
                                                // return false;
                                            }
                                            if(material!=partList[part_number_arr[0]]["material"]){
                                                alert('输入部件号不是同一种材质，请检查后重新输入');
                                                // console.log(material,partList[part_number_arr[0]]["material"]);
                                                flag=0;
                                                // return false;
                                            }
                                            if(specification!=partList[part_number_arr[0]]["specification"]){
                                                alert('输入部件号不是同一种规格，请检查后重新输入');
                                                // console.log(specification,partList[part_number_arr[0]]["specification"]);
                                                flag=0;
                                                // return false;
                                            }

                                            if(part_number_arr[1]==undefined || !part_number_arr[1]) number = '1';
                                            else number = part_number_arr[1];
                                            part_str += '+'+part_number_arr[0]+'/'+ partList[part_number_arr[0]]["length"]+'*'+number;
                                        }
                                    }
                                })
                                if(part_str.length!=0) part_str = part_str.substring(1);
                            }
                            if(flag){
                                var table_append = [{
                                    "xh" : manual_table.bootstrapTable("getData").length +1,
                                    "stuff" : name,
                                    "material" : material,
                                    "specification" : specification,
                                    "length" : value_length,
                                    "count" : value_count,
                                    "mate" : part_str,
                                    "remark" : ''
                                }];
                                manual_table.bootstrapTable("append",table_append);
                                table.bootstrapTable("uncheckAll");
                                eachCompact();
                                // console.log(value_arr);
                                layer.close(index);
                            }
                        }
                        
                    },
                    no:function(index){
                        layer.close(index);
                        return false;//点击按钮按钮不想让弹层关闭就返回false
                    },
                    close:function(index){
                        return false;//点击按钮按钮不想让弹层关闭就返回false
                    }
                    
                });
            }
            $(document).on("click", "#manual_add", function () {
                addLayerOpen();
            });
            $(document).on("click", "#select-compact", function () {
                var tableGet = table.bootstrapTable("getAllSelections");
                if(tableGet.length==0) layer.msg("请选中后组合");
                else{
                    let fieldList=[];
                    let l_name = tableGet[0]["material"];
                    let im_spec = tableGet[0]["specification"];
                    $.each(tableGet,function(index,row){
                        fieldList.push(row.DtMD_sPartsID);
                    })
                    addLayerOpen(fieldList.join(",\n"),l_name+','+im_spec);
                }
            });
            $(document).on("click","#manual_update", function () {
                var choose_table = manual_table.bootstrapTable('getSelections');
                if(choose_table.length == 1){
                    choose_table = choose_table[0];
                    var xh = choose_table["xh"];
                    var value_length = choose_table["length"];
                    var value_count = choose_table["count"];
                    var mate = choose_table["mate"];
                    var mate_table = mate.split('+');
                    var string_text = '';
                    var xg_index,xh_index;
                    //找到最后一个/和最后一个*
                    $.each(mate_table,function(index,value){
                        xg_index = value.lastIndexOf('/');
                        xh_index = value.lastIndexOf('*');
                        string_text += value.substring(0,xg_index)+','+value.substring(xh_index+1)+'\n';
                    });
                    var openContent = `<form class="form-horizontal"><div class="form-group col-xs-7"><fieldset><legend>填写</legend><p>请填入长度:</p><input id="cl_length" type="text" style="width:100%" value="`+value_length+`"></input><p>请填入数量:</p><input id="cl_count" type="text" style="width:100%" value="`+value_count+`"></input><p>请填入(编号,数量):</p><textarea name="txt_remark" id="remark" style="width:100%">`+string_text+`</textarea></fieldset></div>`;
                    // if(field!=''){
                    openContent += `<div class="form-group col-xs-1"></div><div class="form-group col-xs-4"><fieldset><legend>库存情况(长度：数量)</legend><div style="height:250px;overflow-y: scroll;">`;
                    if(kcArr[choose_table["material"]+','+choose_table["specification"]]!=undefined){
                        for(var ki=0;ki<kcArr[choose_table["material"]+','+choose_table["specification"]].length;ki++){
                            openContent += "<p>"+kcArr[choose_table["material"]+','+choose_table["specification"]][ki][0]+":   "+kcArr[choose_table["material"]+','+choose_table["specification"]][ki][1]+"</p>";
                        }
                    }
                    openContent += `</div></fieldset></div>`;
                    // }
                    openContent += `</form>`;
                    layer.open({
                        //formType: 2,//这里依然指定类型是多行文本框，但是在下面content中也可绑定多行文本框在这里插入代码片
                        title: '修改',
                        area: ['50%', '60%'],
                        offset:'t',
                        btnAlign: 'c',
                        closeBtn:'1',//右上角的关闭
                        content: openContent,
                        btn:['确认','取消','关闭'],
                        yes: function (index, layero) {
                            value_length = $('#cl_length').val();
                            value_count = $('#cl_count').val();
                            if(value_length.length == 0 || value_count.length==0){
                                alert('请输入值');
                                return false;
                            }
                            value1 = $('#remark').val();//获取多行文本框的值
                            if(value1.length == 0 || !value1){
                                alert('请输入值');
                                return false;
                            }else{
                                var re = new RegExp("，","g"); //定义正则表达式
                                //第一个参数是要替换掉的内容，第二个参数"g"表示替换全部（global）。
                                value1 = value1.replace(re, ",");
                                var value_arr = value1.split('\n');
                                var part_number_arr = [];
                                var part_str='';
                                var material='';
                                var specification='';
                                var name = ''
                                var flag = 1;
                                // var 
                                if(value_arr.length == 0){
                                    alert('请输入正确值');
                                    flag=0;
                                    // return false;
                                }else{
                                    $.each(value_arr,function(index,value){
                                        if(value.length != 0){
                                            part_number_arr = value.split(',');
                                            if(part_number_arr==''){
                                                alert('部件号不能为空');
                                                flag=0;
                                                // return false;
                                            }else if(partList[part_number_arr[0]] == undefined){
                                                alert('不存在部件号'+part_number_arr[0]+',请输入正确部件号');
                                                flag=0;
                                                // return false;
                                            }else{
                                                // console.log(part_number_arr,part_number_arr[0],partList[part_number_arr[0]]);
                                                material = material?material:partList[part_number_arr[0]]["material"];
                                                specification = specification?specification:partList[part_number_arr[0]]["specification"];
                                                name = name?name:partList[part_number_arr[0]]["IM_Class"];
                                                
                                                if(name!=partList[part_number_arr[0]]["IM_Class"]){
                                                    alert('输入部件号不是同一种名称，请检查后重新输入');
                                                    // console.log(name,partList[part_number_arr[0]]["IM_Class"]);
                                                    flag=0;
                                                    // return false;
                                                }
                                                if(material!=partList[part_number_arr[0]]["material"]){
                                                    alert('输入部件号不是同一种材质，请检查后重新输入');
                                                    // console.log(material,partList[part_number_arr[0]]["material"]);
                                                    flag=0;
                                                    // return false;
                                                }
                                                if(specification!=partList[part_number_arr[0]]["specification"]){
                                                    alert('输入部件号不是同一种规格，请检查后重新输入');
                                                    // console.log(specification,partList[part_number_arr[0]]["specification"]);
                                                    flag=0;
                                                    // return false;
                                                }
    
                                                if(part_number_arr[1]==undefined || !part_number_arr[1]) number = '1';
                                                else number = part_number_arr[1];
                                                part_str += '+'+part_number_arr[0]+'/'+ partList[part_number_arr[0]]["length"]+'*'+number;
                                            }
                                        }
                                    })
                                    if(part_str.length!=0) part_str = part_str.substring(1);
                                }
                                if(flag){
                                    var table_append = {
                                        "xh" : xh,
                                        "stuff" : name,
                                        "material" : material,
                                        "specification" : specification,
                                        "length" : value_length,
                                        "count" : value_count,
                                        "mate" : part_str,
                                        "remark" : ''
                                    };
                                    manual_table.bootstrapTable("updateRow",{index:xh-1,row:table_append});
                                    eachCompact();
                                    // console.log(value_arr);
                                    layer.close(index);
                                }
                            }
                            
                        },
                        no:function(index){
                            layer.close(index);
                            return false;//点击按钮按钮不想让弹层关闭就返回false
                        },
                        close:function(index){
                            return false;//点击按钮按钮不想让弹层关闭就返回false
                        }
                        
                    });
                }else{
                    layer.msg("只能选择单个进行修改");
                }
            })
            $(document).on("click","#manual_del", function () {
                var choose_table = manual_table.bootstrapTable('getData');
                var count = 1;
                var change_table = [];
                $.each(choose_table,function (index,value){
                    if(value[0] == undefined){
                        value['xh'] = count;
                        change_table.push(value);
                        count++;
                    }
                })
                manual_table.bootstrapTable('load',change_table);
                // getNoTable();
            })
            $(document).on("click","#manual_save", function () {
                var choose_table = manual_table.bootstrapTable('getData');
                if(choose_table.length == 0) return false;
                else Fast.api.close(choose_table);
            })
            function eachCompact()
            {
                let noSave = manual_table.bootstrapTable('getData');
                // let ppExist = partResult;
                let ppExist = [];
                let sumList={};
                $.each(noSave,function(index,row){
                    let partNum = (parseFloat)(row["count"]);
                    let partTable = row["mate"].split('+');
                    $.each(partTable,function(pindex,pvalue){
                        let xg_index = pvalue.lastIndexOf('/');
                        let xh_index = pvalue.lastIndexOf('*');
                        let partName = pvalue.substring(0,xg_index);
                        let partValue = (parseFloat)(pvalue.substring(xh_index+1));
                        sumList[partName]==undefined?sumList[partName] = partValue*partNum:sumList[partName] += partValue*partNum;
                    });
                })
                let wzIndex = [];
                $.each(partResult,function(ppindex,pprow){
                    ppExist[ppindex] = {
                        DtMD_sPartsID: pprow['DtMD_sPartsID'],
                        DtS_Name: pprow['DtS_Name'],
                        IM_Class: pprow['IM_Class'],
                        bh: pprow['bh'],
                        clsort: pprow['clsort'],
                        count: pprow['count'],
                        id: pprow['id'],
                        length: pprow['length'],
                        material: pprow['material'],
                        name_number: pprow['name_number'],
                        number: pprow['number'],
                        specification: pprow['specification'],
                    };
                    if(pprow!=undefined && sumList[pprow["DtMD_sPartsID"]]!=undefined){
                        let jnum = pprow["count"]-sumList[pprow["DtMD_sPartsID"]];
                        ppExist[ppindex]["count"] = jnum;
                        if(jnum==0) wzIndex.push(ppindex);
                    }
                })
                for(var zi=wzIndex.length-1;zi>=0;zi--){
                    ppExist.splice(wzIndex[zi],1);
                }
                table.bootstrapTable("load",ppExist);
            }
        },
        genermaterial: function ()  {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/nesting/generMaterial/ids/'+Config.ids + location.search,
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                // sortName: 'TD_ID',
                search: false,
                showToggle: false,
                showExport: false,
                onlyInfoPagination: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'DtMD_sPartsID', title: '部件编号', operate: false},
                        {field: 'IM_Class', title: '材料名称', searchList:{"角钢":"角钢","钢板":"钢板"}},
                        {field: 'material', title: '材质', searchList:{"Q235B":"Q235B","Q235C":"Q235C","Q355B":"Q355B","Q355C":"Q355C","Q420B":"Q420B","Q420C":"Q420C"}},
                        {field: 'specification', title: '规格', operate: '='},
                        {field: 'length', title: '长度', operate: false},
                        // {field: 'width', title: '宽度', operate: false},
                        {field: 'count', title: '数量', operate: false},
                        {field: 'mate', title: '匹配结果', operate: false, visible: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        print: function() {
            let list = Config.list;
            let tb_tmp = [];
            for (const key in list) {
                if (Object.hasOwnProperty.call(list, key)) {
                    const ele = list[key];
                    tb_tmp.push({
                        id: ele['id'],
                        material: ele['material'],
                        specification: ele['specification'],
                        length: ele['length'],
                        count: ele['count'],
                        mate: ele['mate'],
                        yl: ele['yl'],
                        lyv: ele['lyv']
                    })
                }
            }          
            
            let data_tmp={
                tb:tb_tmp,
                title: Config.rows['title'],
                gcmc: Config.rows['gcmc'],
                xddh: Config.rows['xddh'],
                tx: Config.rows['tx'],
                sl: Config.rows['sl'],
            };
            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: tldata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        zlprint: function() {
            let list = Config.list;
            let tb_tmp = [];
            for (const key in list) {
                if (Object.hasOwnProperty.call(list, key)) {
                    const ele = list[key];
                    tb_tmp.push({
                        id: Number(key)+1,
                        stuff: ele['stuff'],
                        material: ele['material'],
                        specification: ele['specification'],
                        length: ele['length'],
                        count: ele['count'],
                    })
                }
            }          
            
            let data_tmp={
                tb:tb_tmp,
                title: Config.rows['title'],
                gcmc: Config.rows['gcmc'],
                xddh: Config.rows['xddh'],
                tx: Config.rows['tx'],
                sl: Config.rows['sl'],
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: clzldata});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        layer.msg(ret.msg);
                        window.location.reload(true);
                    }
                    return false;
                });
            }
        }
    };
    return Controller;
});