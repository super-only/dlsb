define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/scheduling/index' + location.search,
                    edit_url: 'chain/material/scheduling/edit',
                    // del_url: 'chain/material/scheduling/del',
                    multi_url: 'chain/material/scheduling/multi',
                    import_url: 'chain/material/scheduling/import',
                    table: 'pc_scheduling',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'ID',
                sortName: 'PtNum',
                singleSelect: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', operate: false, formatter: function(value,row,index){
                            return ++index;
                        }},
                        {field: 'PtNum', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'orderId', title: '工序', searchList:Config.wareList},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: false},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            // Controller.api.bindevent();
            // // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/scheduling/edit/ids/' +Config.ids + location.search,
                }
            });
            $(document).on("click", "#updatePurchase", function () {
                $.ajax({
                    url: 'chain/material/scheduling/eipUpload',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    data: {PC_Num: Config.PC_Num},
                    success: function (ret) {
                        layer.msg(ret.msg);
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'p_id',
                uniqueId: 'p_id',
                height: document.body.clientHeight-70,
                onPreBody: function (data) {
                    mergeCells(data, "p_id", $("#table"),["check","id","p_id","IMName","IMStd","IMMaterial","IMLength","Num","nrecord","MachineName"]);
                },
                columns: [
                    [
                        {checkbox: true,field:'check'},
                        {field: 'id', title: '序号', formatter: function(value,row,index){
                            return ++index;
                        }},
                        {field: 'p_id', title:'父级id', visible: false},
                        {field: 'IMName', title: '材料名称'},
                        {field: 'IMStd', title: '规格型号'},
                        {field: 'IMMaterial', title: '材质'},
                        {field: 'IMLength', title: '长度'},
                        {field: 'Num', title: '材料数量'},
                        {field: 'nrecord', title: '已加工数量'},
                        {field: 'MachineName', title: '设备名称'},
                        {field: 'PartName', title: '件号',operate:false},
                        {field: 'PartNCPath', title: '件号标识',operate:false},
                        {field: 'PartLength', title: '件号长度',operate:false},
                        {field: 'SingleQTY', title: '单根计划件数',operate:false},
                        {field: 'QTY', title: '总计划件数',operate:false},
                        // {field: 'IfDone', title: '是否已加工完',formatter: function(value,row,index){
                        //     return value?"是":"否";
                        // }},
                        // {field: 'nrecord', title: '已加工数量'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            /**
             * 合并相同列
             * @param data  原始数据（在服务端完成排序）
             * @param fieldName 合并属性数组
             * @param target    目标表格对象
             */
            function mergeCells(data, fieldName, target, fieldList) {
                setTimeout(function () {
                    if (data.length == 0) {
                        return;
                    }
                    var numArr = [];
                    var number=0;
                    // var classzhi='dan';
                    if( data.length>1){
                        for (let i = 0; i < data.length; i++) {
                            if(data[i][fieldName]!='' && data[i][fieldName]!='-'){
                                if(data[i-1]){
                                    if(data[i-1][fieldName]!='' && data[i-1][fieldName]!='-'){
                                        if(data[i-1][fieldName]==data[i][fieldName]){
                                            number++
                                        }
                                        else{
                                            number=number+1
                                            numArr.push({index:i-number,number:number,pan:'1'})
                                            number=0
                                        }
                                    }
                                }
                                if(!data[i+1]){
                                    number=number
                                    numArr.push({index:i-number,number:number+1,pan:'2'})
                                    number=0
                                }else{
                                    if(data[i+1][fieldName]=='' || data[i+1][fieldName]=='-'){
                                        number=number
                                        numArr.push({index:i-number,number:number+1,pan:'3'})
                                        number=0
                                    }
                                }
                            }else{
                                numArr.push({index:i,number:1,pan:'4'})
                            }
                        }
                    }else{
                        numArr.push({index:0,number:1,pan:'5'})
                    }
                    // console.log(numArr);
                    for (let x = 0; x < numArr.length; x++) {
                        if(x%2){
                            for(let y=0;y<numArr[x]['number'];y++){
                                $(target).children('tbody').children('tr').eq(numArr[x]['index']+y).css('background','#ccc')
                            }
                        }else{
                            for(let y=0;y<numArr[x]['number'];y++){
                                $(target).children('tbody').children('tr').eq(numArr[x]['index']+y).css('background','#FFF')
                            }
                        }
                        $(target).bootstrapTable('mergeCells', { index: numArr[x]['index'], field: fieldName, colspan: 1, rowspan: numArr[x]['number']});
                        for(let j=0;j<fieldList.length;j++){
                            $(target).bootstrapTable('mergeCells', { index: numArr[x]['index'], field: fieldList[j], colspan: 1, rowspan: numArr[x]['number']});
                        }
                    }
                },0)
            }

            //点击导出
            $(document).on("click", "#machine", function () {
                var select_html = Config.machineList;
                var ids = Table.api.selectedids(table);
                if(ids.length==0) return false;
                Layer.confirm("确定修改机器设备<form action='" + Fast.api.fixurl("chain/material/scheduling/edit/ids/"+Config.ids) + "' method='post'><input type='hidden' name='ids' value='' />"+select_html+"</form>", {
                    title: '修改机器设备',
                    btn: ["确定", "取消"],
                    yes: function (index, layero) {
                        //发送一个POST请求，并附带参数`name`和`age`
                        Fast.api.ajax({
                            url: "chain/material/scheduling/edit/ids/"+Config.ids, 
                            data: {"ids":ids, "machine":$("select[name='machine']").val()}
                        }, function(data, ret){
                            window.location.reload(true);
                            //成功的回调
                        });
                        // submitForm(ids.join(","), layero);
                        // return false;
                    }
                })
            });

            $(document).on("click", "#author", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/material/scheduling/auditor",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('确定弃审？',"chain/material/scheduling/giveUp",num);
                
            });
            function check(msg,url,num){
                // console.log(url,msg,num);return false;
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                window.location.reload();
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    return false;
                });
            }
        }
    };
    return Controller;
});