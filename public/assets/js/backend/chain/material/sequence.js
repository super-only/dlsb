define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/sequence/index' + location.search,
                    add_url: 'chain/material/sequence/add',
                    edit_url: 'chain/material/sequence/edit',
                    del_url: 'chain/material/sequence/del',
                    multi_url: 'chain/material/sequence/multi',
                    import_url: 'chain/material/sequence/import',
                    table: 'sequence',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'pc_id',
                sortName: 'pc_id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'pc_id', title: __('Pc_id')},
                        {field: 'pt_num', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'td_id', title: __('Td_id')},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: 'LIKE'},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
            $(document).on('click', "#selectPtNum", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback: function (value){
                        $("#c-tower_name").val(value.TD_TypeName);
                        $("#c-pt_num").val(value.PT_Num);
                        $("#c-td_id").val(value.TD_ID);
                        $("#c-trnm_num").val('');
                    }
                };
                Fast.api.open('chain/material/nesting/xddh',"选择下达单号",options);
            })
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        layer.msg(ret.msg);
                        window.location.reload(true);
                    }
                    return false;
                });
                
                var table = $("#table");
                // 初始化表格
                table.bootstrapTable({
                    data: [],
                    height: document.body.clientHeight-150,
                    columns: [
                        [
                            {checkbox: true,field:'check'},
                            {field: 'id', title: '序号', formatter: function(value,row,index){
                                return ++index;
                            }},
                            {field: 'IMName', title: '材料名称'},
                            {field: 'IMStd', title: '规格型号'},
                            {field: 'IMMaterial', title: '材质'},
                            {field: 'IMLength', title: '长度'},
                            {field: 'Num', title: '材料数量'},
                            {field: 'MachineName', title: '设备名称',formatter: function (value,row,index) {
                                return Config.machineArr[value];
                            }}
                        ]
                    ]
                });

                // 为表格绑定事件
                Table.api.bindevent(table);
            }
        }
    };
    return Controller;
});