define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'llddata'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/work_receive/index' + location.search,
                    add_url: 'chain/material/work_receive/add',
                    edit_url: 'chain/material/work_receive/edit',
                    del_url: 'chain/material/work_receive/del',
                    multi_url: 'chain/material/work_receive/multi',
                    import_url: 'chain/material/work_receive/import',
                    table: 'workreceive',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'WR_Num',
                sortName: 'WR_Date',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'WR_Date', title: __('Wr_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'WR_Num', title: __('Wr_num'), operate: 'LIKE'},
                        {field: 'WR_WareHouse', title: __('Wr_warehouse'), operate: 'LIKE'},
                        {field: 'TD_ID', title: __('Td_id'),operate: false, visible: false},
                        {field: 'PC_Num', title: '工程编号'},
                        {field: 't_project', title: '工程名称'},
                        {field: 'TD_TypeName', title: __('Td_typename'), operate: false},
                        {field: 'td.TD_TypeName', title: __('Td_typename'), operate: 'LIKE', visible: false},
                        {field: 'WR_Person', title: __('Wr_person'), operate: 'LIKE'},
                        {field: 'WR_Dept', title: __('Wr_dept'), operate: 'LIKE'},
                        {field: 'WR_Memo', title: __('Wr_memo'), operate: 'LIKE'},
                        {field: 'Writer', title: __('Writer'), operate: false},
                        {field: 'wr.Writer', title: __('Writer'), operate: 'LIKE', visible: false},
                        {field: 'WriteDate', title: __('Writedate'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'Auditor', title: __('Auditor'), operate: false},
                        {field: 'AudiDate', title: __('Audidate'), operate: false},
                        {field: 'T_Num', title: '任务单号',operate: false},
                        // {field: 'd.T_Num', title: '任务单号',operate: 'LIKE', visible: false},
                        {field: 'PT_Num', title: __('Pt_num'), operate: false},
                        {field: 'is_check', title: '状态', searchList:{1:"未审核",2:"已审核"}},
                        // {field: 'p.PT_Num', title: __('Pt_num'),operate: 'LIKE', visible: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
        },
        edit: function () {
            $("#a4print").click(function () {
                window.top.Fast.api.open('chain/material/work_receive/a4Print/ids/' + Config.ids, '车间领料单A4', {
                    area: ["100%", "100%"]
                });
            });
            $("#a5print").click(function () {
                window.top.Fast.api.open('chain/material/work_receive/a5Print/ids/' + Config.ids, '车间领料单A5', {
                    area: ["100%", "100%"]
                });
            });
            Controller.api.bindevent("edit");
        },
        xddh: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/work_receive/xddh' + location.search,
                    table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PT_Num',
                // sortName: 'TD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PT_Num', title: '下达单号', operate: false},
                        {field: 'p.PT_Num', title: '下达单号', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 'd.T_Num', title: '任务单号', operate: 'LIKE',visible: false},
                        {field: 'TD_ID', title: 'TD_ID', operate: false, visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'C_ProjectName', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'd.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'PT_Number', title: '基数', operate: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: 'LIKE'},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'},
                        {field: 'WriterDate', title: "制单时间", visible: false, defaultValue:Config.defaultTime, operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    if(tableContent[0]["WR_Num"]!=0){
                        var index = layer.confirm("该下达单号已做过领料车间，是否继续？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            Fast.api.close(tableContent[0]);
                            layer.close(index);
                        })
                    }else{
                        Fast.api.close(tableContent[0]);
                    }
                }
            });
        },
        a4print: function() {
            const colNum = 34;
            let list = Config.detailList;
            let mainInfos = Config.mainInfos;
            let tb_tmp = [];
            let weightSum = 0;

            list = list.map(p=>{
                p['IM_Spec'] = p['D_IM_Spec'];
                p['L_Name'] = p['D_L_Name']
                return p;
            })
            //过滤掉所有70一下的规格
            list = list.filter((item)=>{
                let spec = item['IM_Spec'];
                if(spec.indexOf('∠')>-1 && spec.substr(1).split('*')[0]>=70){
                    return item;
                }
            })
            
            // 数据分类
            let typeObj = {};
            
            list.forEach(el => {
                weightSum+=el['WRD_RealWeight']*100;

                if(!typeObj[el['L_Name']]){
                    typeObj[el['L_Name']] = {};
                }
                if(!typeObj[el['L_Name']][el['IM_Spec']]){
                    typeObj[el['L_Name']][el['IM_Spec']] = [];
                }
                typeObj[el['L_Name']][el['IM_Spec']].push(el);
            });
            // console.log('typeObj', typeObj);

            for (const L_Name in typeObj) {
                tb_tmp.push({
                    'gg1':L_Name, 
                    'sl1':'',
                    'zl1':'',
                    'gg2':'',
                    'sl2':'',
                    'zl2':'',
                });
                for (const IM_Spec in typeObj[L_Name]) {
                    let countWeight = 0;
                    for (const item of typeObj[L_Name][IM_Spec]) {
                        countWeight+=item['WRD_RealWeight']*100;
                    }
                    tb_tmp.push({
                        'gg1':IM_Spec, 
                        'sl1':'',
                        'zl1':countWeight/100+' Kg',
                        'gg2':'',
                        'sl2':'',
                        'zl2':'',
                    });
                    for (const el of typeObj[L_Name][IM_Spec]) {
                        tb_tmp.push({
                            'gg1':el['WRD_Length']+'米', 
                            'sl1':el['WRD_RealCount']+'支',
                            'zl1':el['WRD_RealWeight']*100/100,
                            // 'zl1': index,
                            'gg2':'',
                            'sl2':'',
                            'zl2':'',
                        });
                    }
                }
            }

            // 按每页两列划分数据
            let doubleArr = [];
            let pageArr = [];
            for(let i=0;i<tb_tmp.length;i++){
                pageArr.push(tb_tmp[i]);
                if((i+1)%(colNum*2)==0 || (i+1)==tb_tmp.length){
                    doubleArr.push(pageArr);
                    pageArr = [];
                }
            }
            
            // 组合每页数据，并将所有完成的页数据拼接到一起
            let double_tb = doubleArr.map((item)=>{
                let newItem = [];
                
                for(let i=1;i<=item.length;i++){
                
                    if(i<=colNum){
                        newItem.push(item[i-1]);
                    }else{
                        newItem[i-1-colNum]['gg2'] = item[i-1]['gg1'];
                        newItem[i-1-colNum]['sl2'] = item[i-1]['sl1'];
                        newItem[i-1-colNum]['zl2'] = item[i-1]['zl1'];
                    }
                }
                return newItem;
            }).reduce((pre, cur)=>{
                return pre.concat(cur);
            },[]);
         
            // 水印属性
            // "left": 250.5,
			// 	"top": 365,
			// 	"height": 84,
			// 	"width": 20.5,
			// 	"title": "国标",
			// 	"fontSize": 36,
			// 	"fontWeight": "800",
			// 	"color": "#543d3d",
			// 	"lineHeight": 46,
			// 	"fixed": true
            const sy_img = {
                '国网':'/assets/img/gw_sy.png',
                '南网':'/assets/img/nw_sy.png',
                'C级钢':'/assets/img/cjg_sy.png',
                '国标':'/assets/img/gb_sy.png',
                '国网正公差': '/assets/img/gwgzc_sy.png',
            }

            let data_tmp={
                gcmc:mainInfos['t_project'],
                tx:mainInfos['TD_TypeName'],
                xddh:mainInfos['PT_Num'],
                llrq:mainInfos['WR_Date'],
                ljzzl:weightSum/100,
                sy: sy_img[mainInfos['WR_SY']] ? sy_img[mainInfos['WR_SY']]: '/assets/img/empty_sy.png',
                bz: mainInfos['WR_Memo'],
                tb:double_tb
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: a4print});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        a5print: function() {
            let list = Config.detailList;
            let mainInfos = Config.mainInfos;
            let tb_tmp = [];
            let weightSum = 0;
            let countSum = 0;
            list = list.map(p=>{
                if(p['D_IM_Spec']) p['IM_Spec'] = p['D_IM_Spec'];
                if(p['D_L_Name']) p['L_Name'] = p['D_L_Name']
                return p;
            })

            for(let index=0;index<list.length;index++){
                weightSum+=list[index]['WRD_RealCount']*100;
                countSum+=Number(list[index]['WRD_RealCount']);
                let tmp={
                    'chmc':list[index]['IM_Class'], 
                    'cz':list[index]['L_Name'],
                    'gg':list[index]['IM_Spec'],
                    'width':'0',
                    'length':'0',
                    'slsl':list[index]['WRD_RealCount'],
                    'zl':list[index]['WRD_RealWeight'],
                    'dw':"kg",
                };
                
                tb_tmp.push(tmp);    
            }

            tb_tmp.push({
                'chmc':'总计：',
                'cz':'',
                'gg':'',
                'width':'',
                'length':'',
                'slsl':weightSum/100,
                'zl':countSum,
                'dw':"kg",
            })


            let data_tmp={
                lldh:mainInfos['WR_Num'],
                llcj:mainInfos['WR_Dept'],
                szck:mainInfos['WR_WareHouse'],
                bz:mainInfos['WR_Memo'],
                rwdh:mainInfos['T_Num'],
                gcmc:mainInfos['t_project'],
                tx:mainInfos['TD_TypeName'],
                xddh:mainInfos['PT_Num'],
                llrq:mainInfos['WR_Date'],
                zdr:mainInfos['Writer'],
                zdrq:mainInfos['WriteDate'],
                shr:mainInfos['Auditor'],
                shrq:mainInfos['AudiDate'],
                pzr:mainInfos['Approver'],
                pzrq:mainInfos['AppDate'],
                llr:mainInfos['WR_Person'],
                tb:tb_tmp
            };

            let pg_mx= {
                "#p_mx1": 1,
            }; //明细，当前页
            //翻页
            window.pageto=function(id,num){

                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };
            
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: a5print});

            $('#p_mx1').html(htemp.getHtml(data_tmp));
            $("#handleprintmx1").click(function(){
                htemp.print(data_tmp);
            });
            
            pageto('#p_mx1',0);
            
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        window.location.reload();
                        layer.msg(ret.msg);
                    }
                    return false;
                });
                $(document).on("click", "#export", function () {
                    window.open('/admin.php/chain/material/work_receive/export/ids/' + Config.ids);
                });

                $(document).on('click', "#chooseMaterial", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            var tableField = Config.tableField;
                            var limber = Config.limberList;
                            var content = "";
                            var col = $("#tbshow>tr").length;
                            $.each(value,function(v_index,v_e){
                                col = col+1;
                                content += '<tr><td>'+col+'</td><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                                $.each(tableField,function(t_index,e){
                                    if(e[1]=="D_L_Name" || e[1]=="L_Name"){

                                        content += '<td '+e[3]+'><input type="text" class="small_input" list="typelist" name="table_row['+e[1]+'][]" value="" placeholder="请选择"><datalist id="typelist">';
                                        $.each(limber,function(l_index,l_e){
                                            content += '<option value="'+ l_e +'">'+ l_e +'</option>';
                                        })
                                        content += "</datalist></td>";
                                    }else if(e[1]=="WRD_Unit") content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="kg"></td>';
                                    // else if(e[1]=="D_IM_Spec") content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e['IM_Spec']) == 'undefined'?'':v_e['IM_Spec'])+'"></td>';
                                    else if(e[1]=="WRD_PlanTime") content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+Config.time+'"></td>';
                                    else content += '<td '+e[3]+'><input class="small_input" type="text" '+e[2]+' name="table_row['+e[1]+'][]" value="'+(typeof(v_e[e[1]]) == 'undefined'?'':v_e[e[1]])+'"></td>';
                                });
                                content += "</tr>";
                            });
                            $("#tbshow").append(content);
                        }
                    };
                    Fast.api.open('chain/purchase/requisition/chooseMaterial',"选择材料",options);
                })

                $(document).on('click', ".del", function(e){
                    var indexChoose = $(this).parents('tr').index();
                    var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(2).find('input').val();
                    if(num!=''){
                        var index = layer.confirm("确定删除？", {
                            btn: ['确定', '取消'],
                        }, function(data) {
                            $.ajax({
                                url: 'chain/material/work_receive/delDetailContent',
                                type: 'post',
                                dataType: 'json',
                                data: {num: num},
                                success: function (ret) {
                                    Layer.msg(ret.msg);
                                    if(ret.code==1){
                                        $( e.target ).closest("tr").remove();
                                    }
                                    
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                            layer.close(index);
                        })
                    }else{
                        $( e.target ).closest("tr").remove();
                    }
                    var col = 0;
                    $("#tbshow").find("tr").each(function () {
                        col++;
                        $(this).children('td').eq(0).text(col);
                    });
                });

                $(document).on('click', "#xddh", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback: function (value){
                            $("#c-t_project").val(value.C_ProjectName);
                            $("#c-TD_ID").val(value.TD_ID);
                            $("#c-PC_Num").val(value.PC_Num);
                            $("#c-T_Num").val(value.T_Num);
                            $("#c-TD_TypeName").val(value.TD_TypeName);
                            $("#c-PT_Num").val(value.PT_Num);
                            //AJAX请求要表格内容
                            $.ajax({
                                url: 'chain/material/work_receive/organizeMaterials',
                                type: 'post',
                                dataType: 'json',
                                data: {num: value.PT_Num},
                                success: function (ret) {
                                    var content = "";
                                    if (ret.code === 1) {
                                        content = ret.data;
                                    }
                                    $("#tbshow").html(content);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }
                    };
                    Fast.api.open('chain/material/work_receive/xddh',"选择下达单号",options);
                })

                $(document).on('click', "#llr", function(e){
                    var url = "chain/sale/project_cate_log/selectSaleMan";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            console.log(value);
                            $("#c-WR_Person").val(value.E_Name);
                        }
                    };
                    Fast.api.open(url,"选择领料人",options);
                })

                $(document).on('click', "#dwmc", function(e){
                    var url = "chain/sale/project_cate_log/selectCustomer";
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["100%","100%"],
                        callback:function(value){
                            $("#c-V_Name").val(value.C_Name);
                        }
                    };
                    Fast.api.open(url,"选择单位名称",options);
                })

                $(document).on('click', "#setting", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["60%","70%"]
                    };
                    Fast.api.open('chain/material/work_receive/setting',"设置损耗",options);
                })
                $(document).on('click', "#settingSpec", function(e){
                    var options = {
                        shadeClose: false,
                        shade: [0.3, '#393D49'],
                        area: ["60%","70%"]
                    };
                    Fast.api.open('chain/material/work_receive/settingSpec',"设置角钢规格范围",options);
                })

                $('#tbshow').on('keyup','td input[name="table_row[WRD_Length][]"]',function () {
                    var IM_Class = $(this).parents('tr').find("input[name='table_row[IM_Class][]']").val().trim();
                    if(IM_Class=="角钢"){
                        var plan_weight = $(this).parents('tr').find("input[name='table_row[WRD_PlanWeight][]']").val().trim();
                        plan_weight = plan_weight==false?0:parseFloat(plan_weight);
                        var length = $(this).parents('tr').find("input[name='table_row[WRD_Length][]']").val().trim();
                        length = length==false?0:parseFloat(length);
                        if(length == 0){
                            layer.msg("请填写长度");
                            return false;
                        }
                        var bz = $(this).parents('tr').find("input[name='table_row[IM_PerWeight][]']").val().trim();
                        bz = bz==false?0:parseFloat(bz);
                        var plan_count = length==0?0:plan_weight/bz/length;
                        $(this).parents('tr').find("input[name='table_row[WRD_PlanCount][]']").val(Math.ceil(plan_count));
                        $(this).parents('tr').find("input[name='table_row[WRD_RealCount][]']").val(Math.ceil(plan_count));
                        $(this).parents('tr').find("input[name='table_row[WRD_RealWeight][]']").val(Math.ceil(plan_weight));
                    }
                })
                $('#tbshow').on('keyup','td input[name="table_row[WRD_RealCount][]"]',function () {
                    var IM_Class = $(this).parents('tr').find("input[name='table_row[IM_Class][]']").val().trim();
                    if(IM_Class=="角钢"){
                        var real_weight = 0;
                        var real_count = $(this).parents('tr').find("input[name='table_row[WRD_RealCount][]']").val().trim();
                        real_count = real_count==false?0:parseFloat(real_count);
                        var length = $(this).parents('tr').find("input[name='table_row[WRD_Length][]']").val().trim();
                        length = length==false?0:parseFloat(length);
                        var bz = $(this).parents('tr').find("input[name='table_row[IM_PerWeight][]']").val().trim();
                        bz = bz==false?0:parseFloat(bz);

                        var D_IM_Spec = $(this).parents('tr').find("input[name='table_row[D_IM_Spec][]']").val().trim();
                        var content = $(this);
                        if(D_IM_Spec){
                            $.ajax({
                                url: 'chain/material/work_receive/getWeight',
                                type: 'post',
                                dataType: 'json',
                                data: {IM_Spec: D_IM_Spec,IM_Class: IM_Class},
                                success: function (ret) {
                                    if(ret.code==1){
                                        bz = ret.data;
                                        real_weight = (real_count*length*bz).toFixed(2);
                                        content.parents('tr').find("input[name='table_row[WRD_RealWeight][]']").val(Math.ceil(real_weight));
                                    }else layer.msg(ret.msg);
                                }, error: function (e) {
                                    Backend.api.toastr.error(e.message);
                                }
                            });
                        }else{
                            var plan_weight = $(this).parents('tr').find("input[name='table_row[WRD_PlanWeight][]']").val().trim();
                            plan_weight = plan_weight==false?0:parseFloat(plan_weight);
                            var plan_count = $(this).parents('tr').find("input[name='table_row[WRD_PlanCount][]']").val().trim();
                            plan_count = plan_count==false?0:parseFloat(plan_count);
                            if(plan_weight || plan_count){
                                var dz = (plan_weight/plan_count).toFixed(2);
                                real_weight = (dz*real_count).toFixed(2);
                            }else real_weight = (real_count*length*bz).toFixed(2);
                            $(this).parents('tr').find("input[name='table_row[WRD_RealWeight][]']").val(Math.ceil(real_weight));
                        }
                    }
                })

                
                var kj_limberList = Config.kj_limberList;
                $('#tbshow').on('keyup','td input[name="table_row[D_L_Name][]"]',function () {
                    var text_count = $(this).parents('tr').find("input[name='table_row[D_L_Name][]']").val().trim();
                    if(kj_limberList[text_count] != undefined ) $(this).parents('tr').find("input[name='table_row[D_L_Name][]']").val(kj_limberList[text_count])
                })
                $('#tbshow').on('keyup','td input[name="table_row[L_Name][]"]',function () {
                    var text_count = $(this).parents('tr').find("input[name='table_row[L_Name][]']").val().trim();
                    if(kj_limberList[text_count] != undefined ) $(this).parents('tr').find("input[name='table_row[L_Name][]']").val(kj_limberList[text_count])
                })

                
                $(document).on('click', "#materialSub", function(e){
                    var index = layer.confirm('请确保已保存当前页面', {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        layer.close(index);
                        $.ajax({
                            url: 'chain/material/work_receive/materialSub',
                            type: 'post',
                            dataType: 'json',
                            data: {num: Config.ids},
                            success: function (ret) {
                                if (ret.code === 1) {
                                    var options = {
                                        shadeClose: false,
                                        shade: [0.3, '#393D49'],
                                        area: ["100%","100%"],
                                    };
                                    //修改跳转链接
                                    Fast.api.open('chain/lofting/material_replace/edit/ids/'+ret.data,"材料代用申请",options);
                                }else layer.msg(ret.msg);
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                    })
                })

                $(document).on("click", "#author", function () {
                    var num = $("#c-WR_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('审核之后无法修改，确定审核？',"chain/material/work_receive/auditor",num);
                });
                $(document).on("click", "#giveup", function () {
                    var num = $("#c-WR_Num").val();
                    if(num == false){
                        layer.confirm('没有获取到单号，请稍后重试');
                        return false;
                    }
                    check('确定弃审？',"chain/material/work_receive/giveUp",num);
                    
                });
                function check(msg,url,num){
                    // console.log(url,msg,num);return false;
                    var index = layer.confirm(msg, {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                if (ret.code == 1) {
                                    window.location.reload();
                                }else{
                                    Layer.msg(ret.msg);
                                }
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }
            }
        }
    };
    return Controller;
});