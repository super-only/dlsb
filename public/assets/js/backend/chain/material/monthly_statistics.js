define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/monthly_statistics/index/'+ location.search,
                    // add_url: 'chain/material/store_state/add',
                    // edit_url: 'chain/material/store_state/edit',
                    // del_url: 'chain/material/store_state/del',
                    // multi_url: 'chain/material/store_state/multi',
                    // import_url: 'chain/material/store_state/import',
                    // table: 'storestate',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                // pk: 'SS_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                // showExport: false,
                onlyInfoPagination: true,
                height: document.body.clientHeight,
                onLoadSuccess:function(){
                    // 这里就是数据渲染结束后的回调函数
                    $(".btn-editone").data("area", ['60%','80%']);
                },
                columns: [
                    [
                        // {checkbox: true},
                        // {field: 'PIM_Num', title: '父级Num', operate: false, visible: false},
                        {field: 'IM_Num', title: '材料编号', operate: false, visible: false},
                        //可能名字有误
                        {field: 'SS_WareHouse', title: '所在仓库', searchList: Config.wareclassList},
                        {field: 'IM_Class', title: '材料', searchList: Config.inventoryList},
                        {field: 'IM_Spec', title: '规格', operate: 'LIKE'},
                        {field: 'L_Name', title: '材质', searchList: Config.limberList},
                        {field: 'length', title: '长度', operate: 'LIKE'},
                        {field: 'width', title: '宽度', operate: 'LIKE'},
                        {field: 'qc_count', title: '期初数量', operate: false},
                        {field: 'qc_weight', title: '期初重量', operate: false},
                        // {field: 'LastMoney', title: '之前结存金额', operate: false},
                        {field: 'rk_count', title: '入库数量', operate: false},
                        {field: 'rk_weight', title: '入库重量', operate: false},
                        // {field: 'InMoney', title: '入库金额', operate: false},
                        // {field: 'AvgPrice', title: '单价', operate: false},
                        // {field: 'LZWeight', title: '累总重量', operate: false},
                        // {field: 'DB_InCount', title: '调拨入库数', operate: false},
                        // {field: 'DB_InWeight', title: '调拨入库重量', operate: false},
                        {field: 'ck_count', title: '出库数量', operate: false},
                        {field: 'ck_weight', title: '出库重量', operate: false},
                        // {field: 'OutMoney', title: '出库金额', operate: false},
                        // {field: 'DB_OutCount', title: '调拨出库数', operate: false},
                        // {field: 'DB_OutWeight', title: '调拨出库重量', operate: false},
                        // {field: 'CheckMoney', title: '盘点冲账', operate: false},
                        {field: 'qm_count', title: '期末数量', operate: false},
                        {field: 'qm_weight', title: '期末重量', operate: false},
                        // {field: 'AvgMoney', title: '结存金额', operate: false},
                        {field: 'dj_time', title: '查询日期', operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime, visible: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});