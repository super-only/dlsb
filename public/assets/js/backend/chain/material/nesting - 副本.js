define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/nesting/index' + location.search,
                    add_url: 'chain/material/nesting/add',
                    edit_url: 'chain/material/nesting/edit',
                    del_url: 'chain/material/nesting/del',
                    multi_url: 'chain/material/nesting/multi',
                    import_url: 'chain/material/nesting/import',
                    table: 'nesting',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'pt_num', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'warehouse', title: __('Warehouse'), searchList:Config.wareList},
                        {field: 'TD_TypeName', title: '塔型名称', operate: 'LIKE'},
                        {field: 'pc_flag', title: '是否排产', searchList:{0:"否",1:"是"},formatter:function(value,row,index){
                            return value?"是":"否";
                        }},
                        {field: 'writer', title: __('Writer'), operate: 'LIKE'},
                        {field: 'writer_time', title: __('Writer_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'auditor', title: __('Auditor'), operate: false},
                        {field: 'auditor_time', title: __('Auditor_time'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent("add");
            $(document).on('click', "#xddh", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback: function (value){
                        $("#c-tower_name").val(value.TD_TypeName);
                        $("#c-pt_num").val(value.PT_Num);
                        $("#c-td_id").val(value.TD_ID);
                        $("#c-trnm_num").val('');
                    }
                };
                Fast.api.open('chain/material/nesting/xddh',"选择下达单号",options);
            })
            $(document).on('click', "#bjdh", function(e){
                var pt_num  = $("#c-pt_num").val();
                var td_id  = $("#c-td_id").val();
                if(!pt_num){
                    layer.msg("请先选择下达单号！");
                    return false;
                }
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback: function (value){
                        $("#c-trnm_num").val(value.TRNM_Num);
                    }
                };
                Fast.api.open('chain/material/nesting/bjdh/td_id/'+td_id,"选择补件单号",options);
            })
        },
        edit: function () {
            Controller.api.bindevent("edit");
            $("#show").parent("div").height(document.body.clientHeight-150);
            var table = $("#no_table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.no_table,
                height:document.body.clientHeight-170,
                columns: [
                    [
                        // {field: 'DtMD_ID_PK', title: 'ID'},
                        {field: 'DtMD_sPartsID', title: '部件号'},
                        {field: 'DtMD_sStuff', title: '名称'},
                        {field: 'DtMD_sMaterial', title: '材质'},
                        {field: 'DtMD_sSpecification', title: '规格'},
                        {field: 'DtMD_iLength', title: '长度'}
                    ]
                ]
            });
            $(document).on('click', "#chooseMaterial", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        var tableField = Config.field;
                        var limber = Config.limber_list;
                        var content = "";
                        $.each(value,function(v_index,v_e){
                            content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                            $.each(tableField,function(t_index,e){
                                if(e[2]=="select"){
                                    content += '<td><input type="text" class="small_input" list="typelist" name="table_row['+e[1]+'][]" value="" placeholder="请选择"><datalist id="typelist">';
                                    $.each(limber,function(l_index,l_e){
                                        content += '<option value="'+ l_e +'">'+ l_e +'</option>';
                                    })
                                    content += "</datalist></td>";
                                }else{
                                    var table_field_value = '';
                                    if(e[1]=='im_num') table_field_value = (typeof(v_e['IM_Num']) == 'undefined'?e[4]:v_e['IM_Num']);
                                    else if(e[1]=='stuff') table_field_value = (typeof(v_e['IM_Class']) == 'undefined'?e[4]:v_e['IM_Class']);
                                    else if(e[1]=='specification') table_field_value = (typeof(v_e['IM_Class']) == 'undefined'?e[4]:v_e['IM_Spec']);
                                    content += '<td><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+table_field_value+'"></td>';
                                }
                            });
                            content += "</tr>";
                        });
                        $("#tbshow").append(content);
                    }
                };
                Fast.api.open('chain/purchase/requisition/chooseMaterial',"选择材料",options);
            })
            $(document).on('click', "#generMaterial", function(e){
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["100%","100%"],
                    callback:function(value){
                        var tableField = Config.field;
                        var limber = Config.limber_list;
                        var content = "";
                        $.each(value,function(v_index,v_e){
                            content += '<tr><td><a href="javascript:;" class="btn btn-xs btn-danger del"><i class="fa fa-trash"></i></a></td>';
                            $.each(tableField,function(t_index,e){
                                if(e[2]=="select"){
                                    content += '<td><input type="text" class="small_input" list="typelist" name="table_row['+e[1]+'][]" value="'+v_e['material']+'" placeholder="请选择"><datalist id="typelist">';
                                    $.each(limber,function(l_index,l_e){
                                        content += '<option value="'+ l_e +'">'+ l_e +'</option>';
                                    })
                                    content += "</datalist></td>";
                                }else{
                                    var table_field_value = '';
                                    if(e[1]=='im_num') table_field_value = (typeof(v_e['IM_Num']) == 'undefined'?e[4]:v_e['IM_Num']);
                                    else if(e[1]=='stuff') table_field_value = (typeof(v_e['IM_Class']) == 'undefined'?e[4]:v_e['IM_Class']);
                                    else if(e[1]=='specification') table_field_value = (typeof(v_e['IM_Class']) == 'undefined'?e[4]:v_e['IM_Spec']);
                                    else if(e[1]=='mate') table_field_value = (typeof(v_e['mate']) == 'undefined'?e[4]:v_e['mate']);
                                    else if(e[1]=='count') table_field_value = (typeof(v_e['count']) == 'undefined'?e[4]:v_e['count']);
                                    else if(e[1]=='length') table_field_value = (typeof(v_e['length']) == 'undefined'?e[4]:v_e['length']);
                                    else if(e[1]=='width') table_field_value = (typeof(v_e['width']) == 'undefined'?e[4]:v_e['width']);
                                    content += '<td><input class="small_input" type="'+e[2]+'" '+e[3]+' name="table_row['+e[1]+'][]" value="'+table_field_value+'"></td>';
                                }
                            });
                            content += "</tr>";
                        });
                        $("#tbshow").append(content);
                    }
                };
                Fast.api.open('chain/material/nesting/generMaterial/ids/'+Config.ids,"选择材料",options);
            })
            $(document).on("click", "#save-detail", function () {
                var filterData = $("#edit-form").serializeArray();
                $.ajax({
                    url: 'chain/material/nesting/edit/ids/'+Config.ids,
                    type: 'post',
                    dataType: 'json',
                    data: {data: JSON.stringify(filterData)},
                    async: false,
                    success: function (ret) {
                        if(ret.code==1){
                            window.location.href = "edit/ids/"+Config.ids;
                        }else{
                            layer.msg(ret.msg); 
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on('click', ".del", function(e){
                var indexChoose = $(this).parents('tr').index();
                var num = $(this).parents('tbody').find('tr').eq(indexChoose).find('td').eq(1).find('input').val();
                if(num!=false){
                    var index = layer.confirm("确定删除？", {
                        btn: ['确定', '取消'],
                    }, function(data) {
                        $.ajax({
                            url: 'chain/material/nesting/delDetailContent',
                            type: 'post',
                            dataType: 'json',
                            data: {num: num},
                            success: function (ret) {
                                Layer.msg(ret.msg);
                                if(ret.code==1){
                                    $( e.target ).closest("tr").remove();
                                }
                                
                            }, error: function (e) {
                                Backend.api.toastr.error(e.message);
                            }
                        });
                        layer.close(index);
                    })
                }else{
                    $( e.target ).closest("tr").remove();
                }
            });
            $(document).on("click", "#pc", function () {
                $.ajax({
                    url: 'chain/material/nesting/schedulingRelease',
                    type: 'post',
                    dataType: 'json',
                    data: {ids: Config.ids},
                    async: false,
                    success: function (ret) {
                        if(ret.code==1){
                            window.location.href = "edit/ids/"+Config.ids;
                        }else{
                            layer.msg(ret.msg); 
                        }
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            });
            $(document).on("click", "#author", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('审核之后无法修改，确定审核？',"chain/material/nesting/auditor",num);
            });
            $(document).on("click", "#giveup", function () {
                var num = Config.ids;
                if(num == false){
                    layer.confirm('没有获取到单号，请稍后重试');
                    return false;
                }
                check('确定弃审？',"chain/material/nesting/giveUp",num);
                
            });
            function check(msg,url,num){
                // console.log(url,msg,num);return false;
                var index = layer.confirm(msg, {
                    btn: ['确定', '取消'],
                }, function(data) {
                    $.ajax({
                        url: url,
                        type: 'post',
                        dataType: 'json',
                        data: {num: num},
                        success: function (ret) {
                            if (ret.code == 1) {
                                window.location.reload();
                            }else{
                                Layer.msg(ret.msg);
                            }
                        }, error: function (e) {
                            Backend.api.toastr.error(e.message);
                        }
                    });
                    layer.close(index);
                })
            }
        },
        xddh: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/nesting/xddh' + location.search,
                    table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PT_Num',
                // sortName: 'TD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PT_Num', title: '下达单号', operate: false},
                        {field: 'p.PT_Num', title: '下达单号', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 'd.T_Num', title: '任务单号', operate: 'LIKE',visible: false},
                        {field: 'TD_ID', title: 'TD_ID', operate: false, visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'C_ProjectName', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'd.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        bjdh: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/nesting/bjdh/td_id/'+Config.td_id + location.search,
                    table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PT_Num',
                // sortName: 'TD_ID',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'TRNM_Num', title: '补件单号', operate: false},
                        {field: 'trm.TRNM_Num', title: '补件单号', operate: 'LIKE', visible: false},
                        {field: 'T_Num', title: '任务单号', operate: false},
                        {field: 'd.T_Num', title: '任务单号', operate: 'LIKE',visible: false},
                        {field: 'TD_ID', title: 'TD_ID', operate: false, visible: false},
                        {field: 'PC_Num', title: '工程编号', operate: 'LIKE'},
                        {field: 'C_Project', title: '工程名称', operate: 'LIKE'},
                        {field: 'TD_TypeName', title: '塔型', operate: false},
                        {field: 'd.TD_TypeName', title: '塔型', operate: 'LIKE', visible: false},
                        {field: 'TD_Pressure', title: '电压等级', operate: false},
                        {field: 'C_Num', title: '合同号', operate: false},
                        {field: 'c.C_Num', title: '合同号', operate: 'LIKE', visible: false},
                        {field: 'Customer_Name', title: '客户名称', operate: 'LIKE'}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent[0]);
                }
            });
        },
        genermaterial: function ()  {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/material/nesting/generMaterial/ids/'+Config.ids + location.search,
                    // table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                // sortName: 'TD_ID',
                search: false,
                showToggle: false,
                showExport: false,
                onlyInfoPagination: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'IM_Num', title: '存货编号', operate: false},
                        {field: 'IM_Class', title: '材料名称', operate: '='},
                        {field: 'material', title: '规格', operate: false},
                        {field: 'length', title: '长度', operate: false},
                        {field: 'width', title: '宽度', operate: false},
                        {field: 'count', title: '数量', operate: false},
                        {field: 'mate', title: '匹配结果', operate: false, visible: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#sure", function () {
                var tableContent = table.bootstrapTable('getSelections');
                if(tableContent.length == 0) {
                    layer.msg("请选择！");
                    return false;
                }else{
                    Fast.api.close(tableContent);
                }
            });
        },
        api: {
            bindevent: function (field="edit") {
                Form.api.bindevent($("form[role=form]"),function(data,ret){
                    if(field == "add"){
                        window.location.href = "edit/ids/"+data;
                    }else{
                        layer.msg(ret.msg);
                        window.location.reload(true);
                    }
                    return false;
                });
            }
        }
    };
    return Controller;
});