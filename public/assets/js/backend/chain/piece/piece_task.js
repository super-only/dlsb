define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'jstree', 'hpbundle', 'lcpdata','scmxdata','zhdata'], function ($, undefined, Backend, Table, Form, undefined, hp, undefined,undefined,undefined) {
    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'chain/piece/piece_task/index' + location.search,
                    table: 'producetask',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PT_Num',
                sortName: 'WriterDate',
                search: false,
                clickToSelect: true,
                singleSelect: true,
                sortOrder: 'DESC',
                showToggle: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.buttons,
                        buttons:[
                            {
                                name: '打印',
                                text: '打印',
                                title: '打印',
                                classname: 'btn btn-xs btn-primary btn-dialog',
                                icon: 'fa fa-list',
                                url: 'chain/piece/piece_task/showJgPrint/PT_Num/{PT_Num}',
                            }
                        ]},
                        {field: 'PT_Num', title: __('Pt_num'), operate: 'LIKE'},
                        {field: 'PT_Company', title: __('Pt_company'), operate: false},
                        {field: 'T_Num', title: __('T_Num'), operate:false, visible:false},
                        {field: 'd.T_Num', title: __('T_Num'), operate: 'LIKE'},
                        {field: 'C_ProjectName', title: __('C_projectname'), operate: 'LIKE'},
                        {field: 'Customer_Name', title: __('Customer_Name'), operate: false},
                        {field: 'P_Name', title: __('P_Name'), operate: false},
                        {field: 'TD_TypeName', title: __('TD_TypeName'), operate: 'LIKE'},
                        // {field: 'PT_Flag', title: __('Pt_flag'), operate: 'LIKE'},
                        {field: 'PT_Number', title: __('Pt_number'), operate: false},
                        // {field: 'PT_Urgent', title: __('Pt_urgent'),operate: false},
                        {field: 'PT_sumWeight', title: __('Pt_sumweight'), operate:false},
                        {field: 'PT_Memo', title: __('Pt_memo'),operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        showjgprint:function(){
            hiprint.init();
            //初始化模板
            let htemp =  new hiprint.PrintTemplate({template: zdata});
            let htemp2 =  new hiprint.PrintTemplate({template: zdata2});
            let htempmx1 =  new hiprint.PrintTemplate({template: mx1data});

            // let htempmx2 =  new hiprint.PrintTemplate({template: mx2data});
            // let htempmx3 =  new hiprint.PrintTemplate({template: mx3data});
            let htempmx4 =  new hiprint.PrintTemplate({template: mx4data});

            //获取表头数据
            let ptask=Config.ptask;
            let dhzarr=Config.dhzarr; //呼高段数汇总
            let dbtjstr=Config.dbtjstr; //段别统计
            let szzdltjstr=Config.szzdltjstr; //试组装段落统计
            let szzdlzlstr=Config.szzdlzlstr; //试组装段落重量
            let mxdata1=Config.mxdata1;
            // let mxdata2=Config.mxdata2;
            // let mxdata3=Config.mxdata2;
            let mxdata4=Config.mxdata3;
            // let zssum=Config.zssum;
            // let zlsum=Config.zlsum;
			let DtMD_sType=mxdata1[0]['DtMD_sType']!=0?mxdata1[0]['DtMD_sType']:'';
            // console.log('dbtj',dbtj);
            //显示html数据
            //封面
            let zdatamb={
                PT_Num:ptask.PT_Num,
                customer_name:ptask.customer_name,
                C_ProjectName:ptask.C_ProjectName,
                TD_TypeName:ptask.TD_TypeName,
                PT_DHmemo:ptask.PT_DHmemo,
                PT_Memo:ptask.PT_Memo,
                dzm:ptask.PT_GYMemo,
                PT_sumWeight:ptask.PT_sumWeight+'Kg',
                tb:[],
                dbtj:dbtjstr,
                dltj:szzdltjstr,
                dlzl:szzdlzlstr,
                Writer:ptask.Writer,
                Auditor:ptask.Auditor,
                idate:ptask.idate,
				DtMD_sType:DtMD_sType
            };
            // dhzarr=dhzarr.concat(dhzarr);
            // dhzarr=dhzarr.concat(dhzarr);
            // dhzarr=dhzarr.concat(dhzarr);
            for(let i in dhzarr){
                // if(i>4){
                //     break;
                // }
                zdatamb['tb'].push({
                    hz:dhzarr[i]
                });
            }
			zdatamb['tb'].push({
                hz:"<div style='display: flex;flex-direction: column;padding:10px;'>" +
                    "<div style='display:flex;flex-direction:row;'><div>备注：</div><div></div></div>" +
                    "<div style='display:flex;flex-direction:row;'><div style='width: 5em'>·总重：</div><div>"+zdatamb['PT_sumWeight']+"</div></div>" +
                    "<div style='display:flex;flex-direction:row;'><div style='width: 7em'>·段别统计：</div><div>"+dbtjstr+"</div></div>" +
                    "<div style='display:flex;flex-direction:row;'><div style='width: 10em'>·试组装段落统计：</div><div>"+szzdltjstr+"</div></div>" +
                    "<div style='display:flex;flex-direction:row;'><div style='width:25em;'>·试组装段落重量：</div><div>"+szzdlzlstr+"</div></div>" +
                    "</div>"
            });
            $('#p_fm').html(htemp.getHtml(zdatamb));

            //呼高段数汇总页面
            let zdatamb2={
                PT_Num:ptask.PT_Num,
                customer_name:ptask.customer_name,
                C_ProjectName:ptask.C_ProjectName,
                tb:[],
            };
            for(let i in dhzarr){
                zdatamb2['tb'].push({
                    hz:dhzarr[i]
                });
            }
            $('#p_fm2').html(htemp2.getHtml(zdatamb2));

            let pg_mx= {
                "#p_mx1": 1,
                "#p_mx2": 1,
                "#p_mx3": 1,
                "#p_mx4": 1
            }; //明细，当前页

            //翻页
            window.pageto=function(id,num){
                let pcount=$(id+' .hiprint-printPanel .hiprint-printPaper').length; //明细，总页数
                let pmx=pg_mx[id];
                pmx=pmx+num;
                $(id).parent().parent().find(".pgnxt").removeClass('disabled');
                $(id).parent().parent().find(".pgpre").removeClass('disabled');
                if(pmx<=1){
                    pmx=1;
                    $(id).parent().parent().find(".pgpre").addClass('disabled');
                }
                if(pmx>=pcount){
                    pmx=pcount;
                    // console.log($(id+" .pgnxt"))
                    $(id).parent().parent().find(".pgnxt").addClass('disabled');
                }
                pg_mx[id]=pmx;

                // console.log(id+' .hiprint-printPanel .hiprint-printPaper',pg_mx1-1)
                // $(id+' .hiprint-printPanel .hiprint-printPaper')[pg_mx1-1].scrollIntoView();

                let mxpage=$(id+' .hiprint-printPanel .hiprint-printPaper');
                for(let v of mxpage){
                    $(v).hide();
                }

                $(mxpage[pmx-1]).show();
                $(id).show();
            };

            //原材料汇总 处理数值
            function pval1(v,key){
                let val=v[key];
                val = parseFloat(val);
                return val?parseFloat(val).toFixed(1):'';
            }
            //明细1
            let mx1datamb={
                TD_TypeName:ptask.TD_TypeName,
                C_ProjectName:ptask.C_ProjectName,
                gyh:ptask.PT_GYMemo,
                DtMD_sType:DtMD_sType,
                PT_DHmemo:ptask.PT_DHmemo
            };
            let tb=[];
            // console.log('mx1datamb', mx1datamb);
            for(let v of mxdata1){
                // console.log(v);
                let lszl=v['zl']-v['dh'];
                if(lszl){
                    lszl=lszl.toFixed(1);
                    if(lszl==0){
                        lszl='';
                    }
                }else{
                    lszl='';
                }
                let tmp={
                    'gg':v['gg'],
                    'cz':v['cz'],
                    'zl':pval1(v,'zl'),
                    'lszl':lszl,//((v['zl']-v['dh']) && (v['zl']-v['dh'])>0.1)?(v['zl']-v['dh']).toFixed(1):'',
                    'dhzl':pval1(v,'dh'),
                    'zjks':v['zjks']!=null?v['zjks']:'',
                    'hq':pval1(v,'hq'),
                    'qj':pval1(v,'qj'),
                    'cb':pval1(v,'cb'),
                    'khj':pval1(v,'khj'),
                    'yb':pval1(v,'yb'),
                    'qg':pval1(v,'qg'),
                    'gh':pval1(v,'gh'),
                    'zjzl':v['zjzl']!=null?v['zjzl']:''
                };
                tb.push(tmp);
            }
            mx1datamb['tb']=tb;

            $('#p_mx1').hide();
            $('#p_mx1').html(htempmx1.getHtml(mx1datamb));
            pageto('#p_mx1',0);
            // let mx1page=$('#p_mx1 .hiprint-printPanel .hiprint-printPaper');
            // for(let v of mx1page){
            //     $(v).hide();
            // }
            // $('#p_mx1').show();
            // $(mx1page[0]).show();
            //明细4
            let mx4datamb={
                tx:ptask.TD_TypeName,
                dw:'',
                
                rq:ptask.idate,
                zb:ptask.Writer,
                sh:ptask.Auditor,
                // ljzsl:mainInfos['number'],
                // ljzzl:mainInfos['weight'],
                // ljzks:mainInfos['knumber'],
            };
            let tb4=[];
    
            for(let d of mxdata4){
                // console.log(v);
                let lszl=d['zl']-d['dh'];
                if(lszl){
                    lszl=lszl.toFixed(1);
                    if(lszl==0){
                        lszl='';
                    }
                }else{
                    lszl='';
                }
                let tmp={
                    'ljbh':d['bjbh'], //零件编号
                    'cz':d['cz']=="Q235B"?"":d['cz'],//材质
                    'gg':d['gg'],//规格
                    'cd':Number(d['cd']) || '',//长度
                    'kd':Number(d['kd']) || '',//宽度
                    'DtMD_iTorch': Number(d['DtMD_iTorch']) || '',
                    'djjs':d['DD_Count'],//单段件数
                    'zsl':d['sl'],//总数量
                    'djzl':d['zl'],//重量（kg）
                    'ks':d['zjks'],//单件孔数
                    'dh': Number(d['DtMD_iWelding']) || '',//电焊
                    'wq': Number(d['DtMD_iFireBending']) || '',//弯曲
                    'qj': Number(d['DtMD_iCuttingAngle']) || '',//切角
                    'cb': Number(d['DtMD_fBackOff']) || '',//铲背
                    'qg': Number(d['DtMD_iBackGouging']) || '',//清根
                    'db': Number(d['DtMD_DaBian']) || '',//打扁
                    'khj':Number(d['DtMD_KaiHeJiao']) || '',//开合角
                    'zk': Number(d['DtMD_ZuanKong']) || '',//钻孔
                    'gh': Number(d['DtMD_GeHuo']) || '',
                    'bz':d['bz'],//备注
                };
                tb4.push(tmp);
            }
            mx4datamb['tb']=tb4;

            $('#p_mx4').hide();
            
            $('#p_mx4').html(htempmx4.getHtml(mx4datamb));
            pageto('#p_mx4',0);

            $("#handleprintfm").click(function(){
                htemp.print(zdatamb);
            });

            $("#handlewordfm").click(function(){
                // $('#p_fm').html($('#p_fm').html().replaceAll('pt', 'px'))
                // console.log($('#p_fm').html());
                // $("#p_fm").wordExport("加工明细表封面-"+ptask.PT_Num);
                // console.log(zdatamb)
                //
                // htemp.toPdf(zdatamb,'测试导出1')
                // console.log(1)

                // let data=encodeURI(JSON.stringify({"data":zdatamb}));
                // Fast.api.ajax({
                //     url:'chain/lofting/produce_task/printlck',
                //     contentType: 'application/json',
                //     data:JSON.stringify({js:data,pt:ptask.PT_Num,itype:'doc',fr:'Fm'}),
                // },function(_,ret){
                //     window.open(ret.url);
                //     console.log('ok',arguments)
                // },function(_,ret){
                //     console.log('err',arguments)
                // })

            });

            $("#handleprintfm2").click(function(){
                htemp2.print(zdatamb2);
            });

            $("#handleprintmx1").click(function(){
                htempmx1.print(mx1datamb);
            });

            $("#handleprintmx4").click(function(){
                htempmx4.print(mx4datamb);
            });


            // console.log($('#mx1 div.well.btn-bar'))
            // console.log($('div.well.btn-bar').height())
            // console.log($('body').height(),$('ul.nav.nav-tabs').height(),$('div.well.btn-bar').height(),$('body').height()-(15+$('ul.nav.nav-tabs').height()+6+$('#mx1 div.well.btn-bar').height()+6+5+15+65));
            $(".lcprint .well").height($('body').height()-(15+$('ul.nav.nav-tabs').height()+6+$('div.well.btn-bar').height()+6+5+15+45));
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
