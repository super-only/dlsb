define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/api/sales_order/index' + location.search,
                    table: 'sales_order_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'C_Num',
                sortName: 'C_Num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'C_Num', title: '销售合同号', operate: 'LIKE'},
                        {field: 'Customer_Name', title: '客户单位', operate: 'LIKE'},
                        {field: 'poItemId', title: '采购订单行项目id', operate: 'LIKE'},
                        {field: 'poNo', title: '采购订单编码', operate: 'LIKE'},
                        {field: 'poItemNo', title: '采购订单行项目号', operate: false},
                        {field: 'eipUpload', title: '上传情况', searchList:{0:'未上传',1:'已上传'},formatter: function (value){ return value?'已上传':'未上传';}},
                        {
                            field: 'buttons',
                            title: '详情',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'detail',
                                    title: '查看详情',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'eipapi/api/sales_order/detail',
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {
                            field: 'buttons',
                            title: '上传/更新',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '上传/更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/api/sales_order/eipUpload',
                                    confirm: '确认发送',
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg + ",返回数据：" + JSON.stringify(data));
                                        table.bootstrapTable('refresh',{silent: true});
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    },
                                    error: function (data, ret) {
                                        return false;
                                    }
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                height: document.body.clientHeight-80,
                columns: [
                    [
                        {field: 'soItemNo', title: '销售订单行项目号'},
                        {field: 'categoryCode', title: '客户单位'},
                        {field: 'subclassCode', title: '采购订单行项目id'},
                        {field: 'poItemId', title: '采购订单行项目ID'},
                        {field: 'productCode', title: '物资编码'},
                        {field: 'productName', title: '物资名称'},
                        {field: 'productUnit', title: '物资单位'},
                        {field: 'productAmount', title: '物资数量'},
                    ]
                ]
            });
            $("pre").css("height",document.body.clientHeight-80);
        }
    };
    return Controller;
});