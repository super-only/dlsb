define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/api/product_schedule/index' + location.search,
                    table: 'gw_pc_scheduling',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'sNum',
                sortName: 'sNum',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'eipUpload', title: '上传情况', searchList:{0:'未上传',1:'已上传'},formatter: function (value){ return value?'已上传':'未完全上传';}},
                        {
                            field: 'buttons',
                            title: '详情',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '更新',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/api/product_schedule/detail'
                                }
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                        {
                            field: 'buttons',
                            title: '上传/更新',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '上传/更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/api/product_schedule/eipUpload',
                                    confirm: '确认发送',
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg + ",返回数据：" + JSON.stringify(data));
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    }
                                }
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'poItemId', title: '采购订单行项目 id', operate: 'LIKE'},
                        {field: 'sNum', title: '排产计划编码', operate: 'LIKE'},
                        {field: 'planPeriod', title: '计划工期（天数）', operate: false},
                        {field: 'dueDate', title: '交付日期', operate: false},
                        {field: 'PlanStartTime', title: '计划开始日期', operate: false},
                        {field: 'PlanFinishTime', title: '计划完成日期', operate: false},
                        {field: 'ActStartTime', title: '实际开始日期', operate: false},
                        {field: 'ActFinishTime', title: '实际完成日期', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/api/product_schedule/detail/ids/' + Config.ids + location.search,
                    table: 'gw_pc_scheduling',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'sdNum',
                sortName: 'sdNum',
                columns: [
                    [
                        {checkbox: true},
                        // {field: 'orderId', title: '工序号', operate: 'LIKE'},
                        {field: 'orderId', title: '工序名称', operate: 'LIKE'},
                        {field: 'PlanStartTime', title: '计划开始日期', operate: false},
                        {field: 'PlanFinishTime', title: '计划完成日期', operate: false},
                        {field: 'ActStartTime', title: '实际开始日期', operate: false},
                        {field: 'ActFinishTime', title: '实际完成日期', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});