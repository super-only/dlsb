define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/api/key_row_stock/index' + location.search,
                    table: 'product_schedule_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'SOD_ID',
                sortName: 'SOD_ID',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'eipUpload', title: '上传情况', searchList:{0:'未完全上传',1:'已上传'},formatter: function (value){ return value?'已上传':'未完全上传';}},
                        {
                            field: 'buttons',
                            title: '上传/更新',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '上传/更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/api/key_row_stock/eipUpload',
                                    confirm: '确认发送',
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg + ",返回数据：" + JSON.stringify(data));
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    }
                                }
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'PT_Num', title: '工单', operate: 'LIKE'},
                        {field: 'matName', title: '原材料名称', operate: 'LIKE'},
                        {field: 'matCode', title: '原材料编码', operate: 'LIKE'},
                        {field: 'matNum', title: '原材料库存数量', operate: false},
                        {field: 'matUnit', title: '原材料单位', operate: false},
                        {field: 'speModels', title: '规格型号', operate: false},
                        {field: 'matVoltageLevel', title: '原材料电压等级', operate: false},
                        {field: 'matProdAddr', title: '原材料产地', operate: false},
                        {field: 'putStorageTime', title: '入库时间', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});