define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/api/work_report/index' + location.search,
                    table: 'product_schedule_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'eipUpload', title: '上传情况', searchList:{0:'未上传',1:'已上传'},formatter: function (value){ return value?'已上传':'未上传';}},
                        {
                            field: 'buttons',
                            title: '上传/更新',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '上传/更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/api/work_report/eipUpload',
                                    confirm: '确认发送',
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg + ",返回数据：" + JSON.stringify(data));
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    }
                                }
                            ],
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'ipoNo', title: '生产订单编号', operate: 'LIKE'},
                        {field: 'insideNo', title: '产品内部ID号', operate: 'LIKE'},
                        {field: 'device_name', title: '设备编号', operate: 'LIKE'},
                        {field: 'productBatchNo', title: '生产批次号', operate: false},
                        {field: 'processName', title: '工序名称', operate: false,formatter: function (value,row){ return Config.orderArr[row['order']]}},
                        {field: 'order', title: '工序编码', operate: 'LIKE'},
                        {field: 'woNo', title: '生产工单编号', operate: 'LIKE'},
                        {field: 'buyerProvince', title: '客户所属省份', operate: 'LIKE'},
                        {field: 'planStartDate', title: '计划开始日期', operate: false},
                        {field: 'planFinishDate', title: '计划完成日期', operate: false},
                        {field: 'record_time', title: '实际开始日期', operate: false},
                        {field: 'auditor_time', title: '实际完成日期', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});