define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/api/cg_purchase/index' + location.search,
                    table: 'cg_purchase',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'poItemId',
                sortName: 'poItemId',
                columns: [
                    [
                        {field: 'poNo', title: __('Pono'), operate: 'LIKE'},
                        {field: 'poItemNo', title: __('Poitemno'), operate: 'LIKE'},
                        {checkbox: true},
                        {field: 'poItemId', title: __('Poitemid'), operate: 'LIKE'},
                        {field: 'subclassCode', title: __('Subclasscode'),defaultValue:'60001' , operate: 'LIKE'},
                        {field: 'subclassName', title: __('Subclassname'), operate: 'LIKE'},
                        {field: 'conCode', title: __('Concode'), operate: 'LIKE'},
                        {field: 'conName', title: __('Conname'), operate: 'LIKE'},
                        {field: 'buyerName', title: __('Buyername'), operate: 'LIKE'},
                        {field: 'buyerCode', title: __('Buyercode'), operate: 'LIKE'},
                        {field: 'materialCode', title: __('Materialcode'), operate: 'LIKE'},
                        {field: 'materialDesc', title: __('Materialdesc'), operate: 'LIKE'},
                        {field: 'amount', title: __('Amount'), operate:'BETWEEN'},
                        {field: 'measUnit', title: __('Measunit'), operate: 'LIKE'},
                        {field: 'sellerConCode', title: __('Sellerconcode'), operate: 'LIKE'},
                        {field: 'serialNumber', title: __('Serialnumber'), operate: 'LIKE'},
                        {field: 'sellerSignTime', title: __('Sellersigntime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'conType', title: __('Contype')},
                        {field: 'prjCode', title: __('Prjcode'), operate: 'LIKE'},
                        {field: 'prjName', title: __('Prjname'), operate: 'LIKE'},
                        {field: 'matCode', title: __('Matcode'), operate: 'LIKE'},
                        {field: 'fixedTechId', title: __('Fixedtechid')},
                        {field: 'pkgNo', title: __('Pkgno')},
                        {field: 'bidBatCode', title: __('Bidbatcode')},
                        {field: 'extDes', title: __('Extdes')},
                        {field: 'matMaxCode', title: __('Matmaxcode'), operate: 'LIKE'},
                        {field: 'matMedCode', title: __('Matmedcode'), operate: 'LIKE'},
                        {field: 'matMinCode', title: __('Matmincode'), operate: 'LIKE'},
                        {field: 'matMaxName', title: __('Matmaxname'), operate: 'LIKE'},
                        {field: 'matMedName', title: __('Matmedname'), operate: 'LIKE'},
                        {field: 'matMinName', title: __('Matminname'), operate: 'LIKE'},
                        {field: 'modifyTime', title: __('Modifytime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'virFlag', title: __('Virflag'), operate: 'LIKE', formatter: Table.api.formatter.flag},
                        {field: 'conValidTime', title: __('Convalidtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on('click', ".btn-update", function(e){
                $.ajax({
                    url: 'api/purchase_api/purchaseApiCopy',
                    type: 'post',
                    dataType: 'json',
                    async: false,
                    success: function (ret) {
                        layer.msg(ret.msg, {
                            icon: 1,
                            time: 1000 //2秒关闭（如果不配置，默认是3秒）
                          }, function(){
                            table.bootstrapTable('refresh', {silent: true});
                        }); 
                    }, error: function (e) {
                        Backend.api.toastr.error(e.message);
                    }
                });
            })
        }
    };
    return Controller;
});