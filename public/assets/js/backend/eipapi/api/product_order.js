define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/api/product_order/index' + location.search,
                    table: 'product_order_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'T_Num',
                sortName: 'T_Num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'eipUpload', title: '上传情况', searchList:{0:'未上传',1:'已上传'},formatter: function (value){ return value?'已上传':'未上传';}},
                        {field: 'status', title: '生产情况',formatter: function (value,row,index){
                            return row.actualStartDate=='0000-00-00 00:00:00'?'待生产':(row.actualFinishDate=='0000-00-00 00:00:00'?'生产中':'已完成');
                        },operate: false},
                        {
                            field: 'buttons',
                            title: '更新实际完成时间',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/api/product_order/updateFinish',
                                    // success: function (data, ret) {
                                    //     $(".btn-refresh").trigger("click");
                                    //     return false;
                                    // },
                                    refresh: true
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {
                            field: 'buttons',
                            title: '上传/更新',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '上传/更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/api/product_order/eipUpload',
                                    confirm: '确认发送',
                                    success: function (data, ret) {
                                        Layer.alert(ret.msg + ",返回数据：" + JSON.stringify(data));
                                        table.bootstrapTable('refresh',{silent: true});
                                        //如果需要阻止成功提示，则必须使用return false;
                                        //return false;
                                    }
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'T_Num', title: '生产订单号', operate: 'LIKE'},
                        {field: 'poNo', title: '采购订单号', operate: 'LIKE'},
                        {field: 'poItemId', title: '采购订单行项目id', operate: 'LIKE'},
                        {field: 'dataType', title: '数据关联类型', operate: false, formatter: function () {return 1;}},
                        {field: 'materialsCode', title: '厂家物料编码', operate: false, formatter: function () {return '铁塔';}},
                        {field: 'materialsName', title: '厂家物料名称', operate: false, formatter: function () {return '铁塔';}},
                        {field: 'materialsUnit', title: '厂家物资单位', operate: false, formatter: function () {return '基';}},
                        {field: 'materialsDesc', title: '厂家物料描述', operate: 'LIKE',
                        formatter : function(value, row, index, field){
                            return "<span style='display: block;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;' title='" + value + "'>" + value + "</span>";
                        },
                        // 固定列最大宽度，超出隐藏
                        cellStyle : function(value, row, index, field){
                            return {
                                css: {
                                    "white-space": "nowrap",
                                    "text-overflow": "ellipsis",
                                    "overflow": "hidden",
                                    "max-width":"200px"
                                }
                            };
                        }},
                        {field: 'amount', title: '生产数量', operate: false},
                        {field: 'unit', title: '计量单位', operate: false, formatter: function () {return '基';}},
                        {field: 'planStartDate', title: '计划开始日期', operate: false},
                        {field: 'planFinishDate', title: '计划完成日期', operate: false},
                        {field: 'actualStartDate', title: '实际开始日期', operate: false},
                        {field: 'actualFinishDate', title: '实际完成日期', operate: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});