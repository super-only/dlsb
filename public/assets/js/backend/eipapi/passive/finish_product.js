define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/passive/finish_product/index' + location.search,
                    table: 'product_work_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PROD_WORK_ORDER',
                sortName: 'PT_Num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PROD_WORK_ORDER', title: '生产工单号', operate: false},
                        {field: 'PT_Num', title: '生产工单号', operate: 'LIKE', visible: false},
                        {field: 'status', title: '成品状态', operate: false, formatter: function (value) { return Config.statusList[value]}},
                        {field: 'cp_upload', title: '上传状态', searchList:Config.fiUploadList,formatter: function (value){ return Config.fiUploadList[value]}},
                        {
                            field: 'buttons',
                            title: '成品上传',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '允许上传',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/passive/finish_product/allowUpload',
                                    confirm: '确认允许上传？',
                                    refresh: true,
                                    visible: function (row) {
                                        return (row['status']>=2 || row['status']==4)?true:false;
                                    }
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'ORDER_NO', title: '国网采购订单号', operate: false},
                        {field: 'poNo', title: '国网采购订单号', operate: 'LIKE', visible: false},
                        {field: 'PO_ITEM_ID', title: '国网侧行项目ID', operate: false},
                        {field: 'poItemId', title: '国网侧行项目ID', operate: 'LIKE', visible: false},
                        {field: 'PROD_ORDER_NO', title: '生产订单号', operate: false},
                        {field: 'PT_Num', title: '生产订单号', operate: 'LIKE', visible: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            $(document).on('click', "#export", function(){
                var tableChoose = table.bootstrapTable('getAllSelections');
                var idList=[];
                $.each(tableChoose,function(i_index,i_e){
                    idList.push(i_e["PROD_WORK_ORDER"]);
                });
                Fast.api.ajax({
                    url: 'eipapi/passive/finish_product/allowUpload/ids/'+idList.toString()
                });
            });
        }
    };
    return Controller;
});