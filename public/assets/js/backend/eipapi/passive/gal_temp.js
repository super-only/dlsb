define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/passive/gal_temp/index' + location.search,
                    table: 'pro_process_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '生产过程ID', operate: false},
                        {field: 'dx_upload', title: '上传情况', searchList:Config.status,formatter: function (value){ return Config.status[value]}},
                        {
                            field: 'buttons',
                            title: '上传状态',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '上传/更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/passive/gal_temp/allowUpload',
                                    confirm: '确认允许上传该生产单？',
                                    refresh: true
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {
                            field: 'buttons',
                            title: '具体详情',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '具体详情',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'eipapi/passive/gal_temp/detail'
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'ORDER_NO', title: '国网采购订单号', operate: false},
                        {field: 'pwv.poNo', title: '国网采购订单号', operate: 'LIKE', visible: false},
                        {field: 'PO_ITEM_ID', title: '国网侧行项目ID', operate: false},
                        {field: 'pwv.poItemId', title: '国网侧行项目ID', operate: 'LIKE', visible: false},
                        {field: 'PROD_ORDER_NO', title: '生产订单号', operate: false},
                        {field: 'pwv.T_Num', title: '生产订单号', operate: 'LIKE', visible: false},
                        {field: 'PROD_WORK_ORDER', title: '生产工单号', operate: false},
                        {field: 'pwv.PT_Num', title: '生产工单号', operate: 'LIKE', visible: false},
                        {field: 'DETECTION_TIME', title: '检测时间', operate: false},
                        {field: 'DETECTION_USER', title: '检测人员', operate: false},
                        {field: 'EXAMINE_USER', title: '审核人员', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                pk: 'id',
                sortName: 'sj',
                sortOrder: 'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '序号', operate: false, formatter: function (value,row,index) { return ++index;}},
                        {field: 'temp', title: '温度', operate: false},
                        {field: 'time', title: '时间', operate: false, addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});