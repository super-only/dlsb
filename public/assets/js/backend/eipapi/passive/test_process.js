define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/passive/test_process/index' + location.search,
                    table: 'test_process_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '生产过程ID', operate: false},
                        {field: 'upload', title: '上传情况', searchList:{0:'未允许',1:'已允许'},formatter: function (value){ return value?'已允许':'未允许';}},
                        {
                            field: 'buttons',
                            title: '是否允许上传',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '上传/更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/passive/test_process/allowUpload',
                                    confirm: '确认允许上传该生产单？',
                                    refresh: true
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {
                            field: 'buttons',
                            title: '具体详情',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '具体详情',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'eipapi/passive/test_process/detail'
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'ORDER_NO', title: '国网采购订单号', operate: false},
                        {field: 'pwv.poNo', title: '国网采购订单号', operate: 'LIKE', visible: false},
                        {field: 'PO_ITEM_ID', title: '国网侧行项目ID', operate: false},
                        {field: 'pwv.poItemId', title: '国网侧行项目ID', operate: 'LIKE', visible: false},
                        {field: 'PROD_ORDER_NO', title: '生产订单号', operate: false},
                        {field: 'pwv.T_Num', title: '生产订单号', operate: 'LIKE', visible: false},
                        {field: 'PROD_WORK_ORDER', title: '生产工单号', operate: false},
                        {field: 'pwv.PT_Num', title: '生产工单号', operate: 'LIKE', visible: false},
                        {field: 'url', title: '附件', operate: false, formatter: Table.api.formatter.url},
                        {field: 'DETECTION_TIME', title: '检测时间', operate: false},
                        {field: 'DETECTION_USER', title: '检测人员', operate: false},
                        {field: 'EXAMINE_USER', title: '审核人员', operate: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            Table.api.init({
                extend: {
                    index_url: 'eipapi/passive/test_process/detail/ids/' + Config.ids + location.search,
                    table: 'test_process_detail',
                }
            });
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder: "asc",
                pagination: true,
                onlyInfoPagination: true,
                columns: [
                    [
                        {field: 'id', title: 'id', operate: false},
                        {field: 'DtMD_ID_PK', title: 'DtMD_ID_PK', operate: false, visible: false},
                        {field: 'sect', title: '段号', operate: 'LIKE'},
                        {field: 'parts', title: '零件部号', operate: 'LIKE'},
                        {field: 'stuff', title: '材料名称', operate: 'LIKE'},
                        {field: 'material', title: '材质', operate: 'LIKE'},
                        {field: 'specification', title: '规格', operate: 'LIKE'},
                        {field: 'length', title: '长度', operate: false},
                        {field: 'fire', title: '焊接质量等级', operate: false},
                        {field: 'hg_count', title: '无损检测', operate: false},
                        {field: 'processor', title: '检测者', operate: false},
                        {field: 'ave_thick', title: '镀锌层平均厚度', operate: false},
                        {field: 'min_thick', title: '镀锌层最小厚度', operate: false},
                        {field: 'adhesion', title: '镀锌层附着力', operate: false},
                        {field: 'uniformity', title: '镀锌层均匀性', operate: false},
                        {field: 'con_por', title: '试装同心孔率', operate: false},
                        {field: 'place_rate', title: '试装构件就位率', operate: false},
                        {field: 'main_control', title: '主要控制尺寸', operate: false}
                    ]
                ]
            });
        }
    };
    return Controller;
});