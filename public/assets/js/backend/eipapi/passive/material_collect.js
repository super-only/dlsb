define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'hpbundle', 'fjbgdata'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/passive/material_collect/index' + location.search,
                    table: 'product_work_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PROD_WORK_ORDER',
                sortName: 'PT_Num',
                columns: [
                    [
                        { checkbox: true },
                        { field: 'PROD_WORK_ORDER', title: '生产工单号', operate: false},
                        { field: 'PT_Num', title: '生产工单号', operate: 'LIKE', visible: false},
                        { field: 'cl_upload', title: '上传状态', searchList: Config.fiUploadList, formatter: function (value) { return Config.fiUploadList[value] } },
                        {
                            field: 'buttons',
                            title: '是否允许上传',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '上传/更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/passive/material_collect/allowUpload',
                                    confirm: '确认允许上传该生产单？',
                                    refresh: true
                                }
                            ], 
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {
                            field: 'buttons',
                            title: '具体详情',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '具体详情',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'eipapi/passive/material_collect/detail'
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        { field: 'ORDER_NO', title: '国网采购订单号', operate: false},
                        { field: 'poNo', title: '国网采购订单号', operate: 'LIKE', visible: false},
                        { field: 'PO_ITEM_ID', title: '国网侧行项目ID', operate: false},
                        { field: 'poItemId', title: '国网侧行项目ID', operate: 'LIKE', visible: false},
                        { field: 'PROD_ORDER_NO', title: '生产订单号', operate: false},
                        { field: 'T_Num', title: '生产订单号', operate: 'LIKE', visible: false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                data: Config.list,
                columns: [
                    [
                        { field: 'ID', title: '序号',formatter:function(value, row, index){return ++index;}},
                        { field: 'PT_Num', title: '工单' },
                        { field: 'MATFLG', title: '原材料区分' },
                        { field: 'IM_Class', title: '材料' },
                        { field: 'MATMATERIAL', title: '材质' },
                        { field: 'MATSPEC', title: '规格' },
                        { field: 'INSPSTD', title: '检验标准' },
                        { field: 'LuPiHao', title: '炉/批号' },
                        { field: 'LuPiHao', title: '原材料复检批次号' },
                        { field: 'MTD_C', title: 'C', farmatter: function (value) { return (parseFloat)(value).toFixed(2) } },
                        { field: 'MTD_Si', title: 'Si', farmatter: function (value) { return (parseFloat)(value).toFixed(2) } },
                        { field: 'MTD_Mn', title: 'Mn', farmatter: function (value) { return (parseFloat)(value).toFixed(2) } },
                        { field: 'MTD_P', title: 'P', farmatter: function (value) { return (parseFloat)(value).toFixed(3) } },
                        { field: 'MTD_S', title: 'S', farmatter: function (value) { return (parseFloat)(value).toFixed(3) } },
                        { field: 'MTD_V', title: 'V', farmatter: function (value) { return (parseFloat)(value).toFixed(3) } },
                        { field: 'MTD_Nb', title: 'Nb' },
                        { field: 'MTD_Ti', title: 'Ti' },
                        { field: 'MTD_Cr', title: 'Cr' },
                        { field: 'MTD_Czsl', title: '层状撕裂检验' },
                        { field: 'MTD_Rm', title: '抗拉强度' },
                        { field: 'MTD_Rel', title: '屈服强度' },
                        { field: 'MTD_Percent', title: '断后伸长率' },
                        { field: 'MTD_Cj', title: '冲击检测' },
                        { field: 'MTD_Wq', title: '弯曲试验' },
                        { field: 'MTD_Wg', title: '尺寸外观' },
                        { field: 'MTD_Mader', title: '生产厂家' },
                        { field: 'MTD_Conclusion', title: '检测结论' },
                        { field: 'mn_deliverdate', title: '原材料出厂日期' },
                        { field: 'type', title: '辅材类型' },
                    ]
                ]
            });

            let list = Config.list;
            let tb_tmp = [];
            let data_tmp = [];
            for (const key in list) {
                if (Object.hasOwnProperty.call(list, key)) {
                    const ele = list[key];
                    tb_tmp = [{
                        no: 1,
                        MTD_Mader: ele['MTD_Mader'],
                        MATSPEC: ele['MATSPEC'],
                        MATMATERIAL: ele['MATMATERIAL'],
                        LuPiHao: ele['LuPiHao'],
                        MTD_C: ele['MTD_C'],
                        MTD_Si: ele['MTD_Si'],
                        MTD_Mn: ele['MTD_Mn'],
                        MTD_S: ele['MTD_S'],
                        MTD_V: ele['MTD_V'],
                        MTD_Rel: ele['MTD_Rel'],
                        MTD_Rm: ele['MTD_Rm'],
                        MTD_Percent: ele['MTD_Percent'],
                        MTD_Wq: ele['MTD_Wq'],
                        MTD_Cj: ele['MTD_Cj'],
                        MTD_Conclusion: ele['MTD_Conclusion']
                    }];
                    data_tmp.push(
                        {
                            pz: ele['MATSPEC'],
                            no: ele['LuPiHao'],
                            mn: ele['MTD_Mn'],
                            s: ele['MTD_S'],
                            p: ele['MTD_P'],
                            si: ele['MTD_Si'],
                            qf: ele['MTD_Rel'],
                            c: ele['MTD_C'],
                            kl: ele['MTD_Rm'],
                            lph: ele['LuPiHao'],
                            scl: ele['MTD_Percent'],
                            cz: ele['MATMATERIAL'],
                            lw: ele['MTD_Wq'],
                            gg: ele['MATSPEC'],
                            cj: ele['MTD_Cj'],
                            zzc: ele['MTD_Mader'],
                            jl: ele['MTD_Conclusion'],
                            gf: ele['INSPSTD']+"碳素结构钢及本工程合同技术规范",
                            syy: "阮观祥",
                            sh: "王金淼"
                        }
                    );
                }
            }
            $(document).on("click", "#toPdf", function () {
                Fast.api.ajax({
                    url: 'eipapi/passive/material_collect/judgePd',
                    type: 'post',
                    dataType: 'json',
                    data: {ids: Config.ids},
                },function (data, ret) {
                    if(ret.code == 1){
                        hiprint.init();
                        //初始化模板
                        let htemp = new hiprint.PrintTemplate({ template: fjbgdata });
                        $('.ly-loading').addClass('open');
                        var i = 0;
                        var length = data_tmp.length
                        let exportPdf_Inter = setInterval(() => {
                            if (i > length || i == length) {
                                $('.ly-loading').removeClass('open');
                                clearInterval(exportPdf_Inter);
                            } else {
                                var value = data_tmp[i];
                                htemp.toPdf(value, value['no'], { scale: 1 });
                                i++;
                            }
                        }, 200)
                    }
                    else layer.confirm(ret.msg);
                });
            });

            $(document).on("click", "#toSave", function () {
                var options = {
                    shadeClose: false,
                    shade: [0.3, '#393D49'],
                    area: ["500px", "300px"],
                };
                Fast.api.open('eipapi/passive/material_collect/partImport/ids/' + Config.ids, "上传pdf", options);
            });
        },
        partimport: function () {
            Form.api.bindevent($("form[role=form]"));
        }
    };
    return Controller;
});