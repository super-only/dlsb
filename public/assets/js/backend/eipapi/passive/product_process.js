define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/passive/product_process/index' + location.search,
                    table: 'pro_process_main',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: '生产过程ID', operate: false},
                        {field: 'upload', title: '上传情况', searchList:{0:'未允许',1:'已允许'},formatter: function (value){ return value?'已允许':'未允许';}},
                        {
                            field: 'buttons',
                            title: '是否允许上传',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '上传/更新',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/passive/product_process/allowUpload',
                                    confirm: '确认允许上传该生产单？',
                                    refresh: true
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {
                            field: 'buttons',
                            title: '具体详情',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '具体详情',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-list',
                                    url: 'eipapi/passive/product_process/detail'
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'ORDER_NO', title: '国网采购订单号', operate: false},
                        {field: 'pwv.poNo', title: '国网采购订单号', operate: 'LIKE', visible: false},
                        {field: 'PO_ITEM_ID', title: '国网侧行项目ID', operate: false},
                        {field: 'pwv.poItemId', title: '国网侧行项目ID', operate: 'LIKE', visible: false},
                        {field: 'PROD_ORDER_NO', title: '生产订单号', operate: false},
                        {field: 'pwv.T_Num', title: '生产订单号', operate: 'LIKE', visible: false},
                        {field: 'PROD_WORK_ORDER', title: '生产工单号', operate: false},
                        {field: 'pwv.PT_Num', title: '生产工单号', operate: 'LIKE', visible: false},
                        {field: 'url', title: '附件', operate: false, formatter: Table.api.formatter.url},
                        {field: 'DETECTION_TIME', title: '检测时间', operate: false,},
                        {field: 'DETECTION_USER', title: '检测人员', operate: false,},
                        {field: 'EXAMINE_USER', title: '审核人员', operate: false,}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        detail: function () {
            Table.api.init({
                extend: {
                    index_url: 'eipapi/passive/product_process/detail/ids/' + Config.ids + location.search,
                    table: 'pro_process_detail',
                }
            });
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                sortOrder: "asc",
                pagination: true,
                onlyInfoPagination: true,
                columns: [
                    [
                        {field: 'id', title: 'id', operate: false},
                        {field: 'DtMD_ID_PK', title: 'DtMD_ID_PK', operate: false, visible: false},
                        {field: 'sect', title: '段号', operate: 'LIKE'},
                        {field: 'parts', title: '零件部号', operate: 'LIKE'},
                        {field: 'stuff', title: '材料名称', operate: 'LIKE'},
                        {field: 'material', title: '材质', operate: 'LIKE'},
                        {field: 'specification', title: '规格', operate: 'LIKE'},
                        {field: 'length', title: '长度', operate: false},
                        {field: 'width', title: '宽度', operate: false},
                        {field: 'xl_count', title: '下料件数', operate: false},
                        {field: 'xl_type', title: '构件类型', operate: false},
                        {field: 'xl_creat_time', title: '生产时间', operate: false},
                        {field: 'xl_hg_count', title: '合格/抽检', operate: false},
                        {field: 'zk_count', title: '制孔件数', operate: false},
                        {field: 'zk_kong', title: '制孔工艺', operate: false},
                        {field: 'zk_sum_hole', title: '孔数', operate: false},
                        {field: 'zk_hg_count', title: '合格/抽检', operate: false},
                        {field: 'zw_count', title: '制弯件数', operate: false},
                        {field: 'zw_bending', title: '制弯工艺', operate: false},
                        {field: 'zw_hg_count', title: '合格/抽检', operate: false},
                        {field: 'hj_fire', title: '焊接质量等级', operate: false},
                        {field: 'hj_processor', title: '加工者', operate: false},
                        {field: 'hj_hg_count', title: '合格/抽检', operate: false},
                        {field: 'dx_sum_weight', title: '镀锌前重量', operate: false},
                        {field: 'dx_late_weight', title: '镀锌后重量', operate: false},
                        {field: 'dx_factory', title: '锌锭厂家', operate: false},
                        {field: 'dx_gal_time', title: '镀锌日期', operate: false},
                    ]
                ]
            });
        }
    };
    return Controller;
});