define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'eipapi/passive/bom_txt/index' + location.search,
                    table: 'product_work_view',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'PROD_WORK_ORDER',
                sortName: 'PT_Num',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'PROD_WORK_ORDER', title: '生产工单号', operate: false},
                        {field: 'PT_Num', title: '生产工单号', operate: 'LIKE', visible: false},
                        {
                            field: 'buttons',
                            title: '导出BOM',
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'ajax',
                                    title: '导出BOM',
                                    classname: 'btn btn-xs btn-success btn-magic btn-ajax',
                                    icon: 'fa fa-upload',
                                    url: 'eipapi/passive/bom_txt/export',
                                    confirm: '确认导出BOM？',
                                    success: function (data, ret) {
                                        exportRaw(ret["pt_num"]+'.txt', data);
                                        return false;
                                    }
                                }
                            ],
                            operate: false,
                            formatter: Table.api.formatter.buttons
                        },
                        {field: 'ORDER_NO', title: '国网采购订单号', operate: false},
                        {field: 'poNo', title: '国网采购订单号', operate: 'LIKE', visible: false},
                        {field: 'PO_ITEM_ID', title: '国网侧行项目ID', operate: false},
                        {field: 'poItemId', title: '国网侧行项目ID', operate: 'LIKE', visible: false},
                        {field: 'poItemNo', title: '国网侧行项目', operate: 'LIKE'},
                        {field: 'PROD_ORDER_NO', title: '生产订单号', operate: false},
                        {field: 'T_Num', title: '生产订单号', operate: 'LIKE', visible: false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            function fakeClick(obj) {
                var ev = document.createEvent("MouseEvents");
                ev.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
                obj.dispatchEvent(ev);
            }
    
            function exportRaw(name, data) {
                var urlObject = window.URL || window.webkitURL || window;
                var export_blob = new Blob([data]);
                var save_link = document.createElementNS("http://www.w3.org/1999/xhtml", "a")
                save_link.href = urlObject.createObjectURL(export_blob);
                save_link.download = name;
                fakeClick(save_link);
            }

            $(document).on('click', "#export", function(){
                var tableChoose = table.bootstrapTable('getAllSelections');
                if(tableChoose.length == 0){
                    layer.msg("请选择！");
                }else{
                    $('.ly-loading').addClass('open');
                    var i = 0;
                    var length = tableChoose.length
                    let exportPdf_Inter = setInterval(() => {
                        if (i > length || i == length) {
                            $('.ly-loading').removeClass('open');
                            clearInterval(exportPdf_Inter);
                        } else {
                            var value = tableChoose[i];
                            Fast.api.ajax({
                                url: 'eipapi/passive/bom_txt/export/ids/'+value["PROD_WORK_ORDER"]
                            }, function(data, ret){
                                exportRaw(ret["pt_num"]+'.txt', data);
                                return false;
                            });
                            i++;
                        }
                    }, 200);
                }
            });
        }
    };
    return Controller;
});