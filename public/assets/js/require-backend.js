require.config({
    urlArgs: "v=" + requirejs.s.contexts._.config.config.site.version,
    packages: [{
        name: 'moment',
        location: '../libs/moment',
        main: 'moment'
    }],
    //在打包压缩时将会把include中的模块合并到主文件中
    // include: ['css', 'layer', 'toastr', 'fast', 'backend', 'backend-init', 'table', 'form', 'dragsort', 'drop', 'addtabs', 'selectpage'],
    include: ['css', 'layer', 'toastr', 'fast', 'backend', 'backend-init', 'table', 'form', 'dragsort', 'drag', 'drop', 'addtabs', 'selectpage'],
    paths: {
        'lang': "empty:",
        'form': 'require-form',
        'table': 'require-table',
        'upload': 'require-upload',
        'drag': 'jquery.drag.min',
        'drop': 'jquery.drop.min',
        'dropzone': 'dropzone.min',
        'echarts': 'echarts.min',
        'echarts-theme': 'echarts-theme',
        'adminlte': 'adminlte',
        'bootstrap-table-commonsearch': 'bootstrap-table-commonsearch',
        'bootstrap-table-template': 'bootstrap-table-template',
        'ui': 'jquery-ui.min',
        'input-tag': 'inputTag',
        //
        // 以下的包从bower的libs目录加载
        'jquery': '../libs/jquery/dist/jquery.min',
        'bootstrap': '../libs/bootstrap/dist/js/bootstrap.min',
        'bootstrap-datetimepicker': '../libs/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min',
        'bootstrap-daterangepicker': '../libs/bootstrap-daterangepicker/daterangepicker',
        'bootstrap-select': '../libs/bootstrap-select/dist/js/bootstrap-select.min',
        'bootstrap-select-lang': '../libs/bootstrap-select/dist/js/i18n/defaults-zh_CN',
        'bootstrap-table': '../libs/bootstrap-table/dist/bootstrap-table.min',
        'bootstrap-table-export': '../libs/bootstrap-table/dist/extensions/export/bootstrap-table-export.min',
        'bootstrap-table-fixed-columns': '../libs/bootstrap-table/dist/extensions/fixed-columns/bootstrap-table-fixed-columns',
        'bootstrap-table-mobile': '../libs/bootstrap-table/dist/extensions/mobile/bootstrap-table-mobile',
        'bootstrap-table-lang': '../libs/bootstrap-table/dist/locale/bootstrap-table-zh-CN',
        'bootstrap-table-jumpto': '../libs/bootstrap-table/dist/extensions/page-jumpto/bootstrap-table-jumpto',
        'bootstrap-slider': '../libs/bootstrap-slider/bootstrap-slider',
        'tableexport': '../libs/tableExport.jquery.plugin/tableExport.min',
        'dragsort': '../libs/fastadmin-dragsort/jquery.dragsort',
        'sortable': '../libs/Sortable/Sortable.min',
        'addtabs': '../libs/fastadmin-addtabs/jquery.addtabs',
        'slimscroll': '../libs/jquery-slimscroll/jquery.slimscroll',
        'validator': '../libs/nice-validator/dist/jquery.validator',
        'validator-lang': '../libs/nice-validator/dist/local/zh-CN',
        'toastr': '../libs/toastr/toastr',
        'jstree': '../libs/jstree/dist/jstree',
        'layer': '../libs/fastadmin-layer/dist/layer',
        'cookie': '../libs/jquery.cookie/jquery.cookie',
        'cxselect': '../libs/fastadmin-cxselect/js/jquery.cxselect',
        'template': '../libs/art-template/dist/template-native',
        'selectpage': '../libs/fastadmin-selectpage/selectpage',
        'citypicker': '../libs/fastadmin-citypicker/dist/js/city-picker.min',
        'citypicker-data': '../libs/fastadmin-citypicker/dist/js/city-picker.data',
        'fixed-deader-table-master': '../libs/fixed-deader-table-master/jquery.fixedheadertable.min',
        'hpbundle':'../libs/hiprint/hiprint.bundle',
        'hppolyfill':'../libs/hiprint/polyfill.min',
        'hpphiwprint':'../libs/hiprint/plugins/jquery.hiwprint',
        'hppminicolors':'../libs/hiprint/plugins/jquery.minicolors.min',
        'hpjq':'../libs/hiprint/plugins/jq-3.31',
        'blddata':'./backend/pdata/blddata',
        'dysqdata':'./backend/pdata/dysqdata',
        'bzqddata':'./backend/pdata/bzqddata',
        'jgjrkdata':'./backend/pdata/jgjrkdata',
        'jgjyjdata':'./backend/pdata/jgjyjdata',
        'cprkdata':'./backend/pdata/cprkdata',
        'cpckddata':'./backend/pdata/cpckddata',
        'cpytjdata':'./backend/pdata/cpytjdata',
        'scrwddata':'./backend/pdata/scrwddata',
        'fhdddata':'./backend/pdata/fhdddata',
        'lcpdata':'./backend/pdata/lcpdata',
        'scmxdata':'./backend/pdata/scmxdata',
        'zhdata':'./backend/pdata/zhdata',
        'lsdata':'./backend/pdata/lsdata',
        'fytxkdata': './backend/pdata/fytxkdata',
        'fytxsjdata': './backend/pdata/fytxsjdata',
        'txjckdata': './backend/pdata/txjckdata',
        'qgddata': './backend/pdata/qgddata',
        'tzsjpddata': './backend/pdata/tzsjpddata',
        'rkddata': './backend/pdata/rkddata',
        'llddata': './backend/pdata/llddata',
        'clbyddata': './backend/pdata/clbyddata',
        'jwlrkdata': './backend/pdata/jwlrkdata',
        'jwlckdata': './backend/pdata/jwlckdata',
        'jwljcdata': './backend/pdata/jwljcdata',
        'tldata': './backend/pdata/tldata',
        'clzldata': './backend/pdata/clzldata',
        'dcadata': './backend/pdata/dcadata',
        'scsygcdata': './backend/pdata/scsygcdata',
        'sygcdata': './backend/pdata/sygcdata',
        'fjbgdata': './backend/pdata/fjbgdata',
        'socketio':'../libs/hiprint/plugins/socket.io',
        'qrcode':'../libs/hiprint/plugins/qrcode',
        'JsBarcode':'../libs/hiprint/plugins/JsBarcode.all.min',
        'canvas2image':'../libs/hiprint/plugins/jspdf/canvas2image',
        'canvg':'../libs/hiprint/plugins/jspdf/canvg.min',
        'html2canvas':'../libs/hiprint/plugins/jspdf/html2canvas.min',
        'jspdf':'../libs/hiprint/plugins/jspdf/jspdf.debug',
        'jquerybase64':'jquery.base64',
        // 'table-cell':'../libs/table-merge-cell/js/tablesMergeCell',
    },
    // shim依赖配置
    shim: {
        // 'hpbundle':['hppolyfill','hppminicolors','hpphiwprint','css!../libs/hiprint/css/hiprint.css','css!../libs/hiprint/css/print-lock.css'],
        'hpbundle':['canvg','html2canvas','jspdf','hppolyfill','hppminicolors','hpphiwprint','css!../libs/hiprint/css/hiprint.css','css!../libs/hiprint/css/print-lock.css'],
        // 'hpbundle':['canvg','html2canvas','jspdf','hppolyfill','hppminicolors','hpphiwprint','socketio','css!../libs/hiprint/css/hiprint.css','css!../libs/hiprint/css/print-lock.css'],
        'addons': ['backend'],
        'bootstrap': ['jquery'],
        'bootstrap-table': {
            deps: ['bootstrap'],
            exports: '$.fn.bootstrapTable'
        },
        'bootstrap-table-lang': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-export': {
            deps: ['bootstrap-table', 'tableexport'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-fixed-columns': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-mobile': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-advancedsearch': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-commonsearch': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-template': {
            deps: ['bootstrap-table', 'template'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'bootstrap-table-jumpto': {
            deps: ['bootstrap-table'],
            exports: '$.fn.bootstrapTable.defaults'
        },
        'tableexport': {
            deps: ['jquery'],
            exports: '$.fn.extend'
        },
        'slimscroll': {
            deps: ['jquery'],
            exports: '$.fn.extend'
        },
        'adminlte': {
            deps: ['bootstrap', 'slimscroll'],
            exports: '$.AdminLTE'
        },
        'bootstrap-daterangepicker': [
            'moment/locale/zh-cn'
        ],
        'bootstrap-datetimepicker': [
            'moment/locale/zh-cn',
        ],
        'bootstrap-select-lang': ['bootstrap-select'],
        'jstree': ['css!../libs/jstree/dist/themes/default/style.css'],
        'validator-lang': ['validator'],
        'citypicker': ['citypicker-data', 'css!../libs/fastadmin-citypicker/dist/css/city-picker.css'],
        'fixed-deader-table-master': ['css!../libs/fixed-deader-table-master/css/defaultTheme.css'],
    },
    baseUrl: requirejs.s.contexts._.config.config.site.cdnurl + '/assets/js/', //资源基础路径
    map: {
        '*': {
            'css': '../libs/require-css/css.min'
        }
    },
    waitSeconds: 60,
    charset: 'utf-8' // 文件编码
});

require(['jquery', 'bootstrap'], function ($, undefined) {
    //初始配置
    var Config = requirejs.s.contexts._.config.config;
    //将Config渲染到全局
    window.Config = Config;
    // 配置语言包的路径
    var paths = {};
    paths['lang'] = Config.moduleurl + '/ajax/lang?callback=define&controllername=' + Config.controllername + '&lang=' + Config.language + '&v=' + Config.site.version;
    // 避免目录冲突
    paths['backend/'] = 'backend/';
    require.config({paths: paths});

    // 初始化
    $(function () {
        require(['fast'], function (Fast) {
            require(['backend', 'backend-init', 'addons'], function (Backend, undefined, Addons) {
                //加载相应模块
                if (Config.jsname) {
                    require([Config.jsname], function (Controller) {
                        if (Controller.hasOwnProperty(Config.actionname)) {
                            Controller[Config.actionname]();
                        } else {
                            if (Controller.hasOwnProperty("_empty")) {
                                Controller._empty();
                            }
                        }
                    }, function (e) {
                        console.error(e);
                        // 这里可捕获模块加载的错误
                    });
                }
            });
        });
    });
});
