define(['fast', 'template', 'moment'], function (Fast, Template, Moment) {
    var Backend = {
        api: {
            sidebar: function (params) {
                colorArr = ['red', 'green', 'yellow', 'blue', 'teal', 'orange', 'purple'];
                $colorNums = colorArr.length;
                badgeList = {};
                $.each(params, function (k, v) {
                    $url = Fast.api.fixurl(k);

                    if ($.isArray(v)) {
                        $nums = typeof v[0] !== 'undefined' ? v[0] : 0;
                        $color = typeof v[1] !== 'undefined' ? v[1] : colorArr[(!isNaN($nums) ? $nums : $nums.length) % $colorNums];
                        $class = typeof v[2] !== 'undefined' ? v[2] : 'label';
                    } else {
                        $nums = v;
                        $color = colorArr[(!isNaN($nums) ? $nums : $nums.length) % $colorNums];
                        $class = 'label';
                    }
                    //必须nums大于0才显示
                    badgeList[$url] = $nums > 0 ? '<small class="' + $class + ' pull-right bg-' + $color + '">' + $nums + '</small>' : '';
                });
                $.each(badgeList, function (k, v) {
                    var anchor = top.window.$("li a[addtabs][url='" + k + "']");
                    if (anchor) {
                        top.window.$(".pull-right-container", anchor).html(v);
                        top.window.$(".nav-addtabs li a[node-id='" + anchor.attr("addtabs") + "'] .pull-right-container").html(v);
                    }
                });
            },
            addtabs: function (url, title, icon) {
                var dom = "a[url='{url}']"
                var leftlink = top.window.$(dom.replace(/\{url\}/, url));
                if (leftlink.size() > 0) {
                    leftlink.trigger("click");
                } else {
                    url = Fast.api.fixurl(url);
                    leftlink = top.window.$(dom.replace(/\{url\}/, url));
                    if (leftlink.size() > 0) {
                        var event = leftlink.parent().hasClass("active") ? "dblclick" : "click";
                        leftlink.trigger(event);
                    } else {
                        var baseurl = url.substr(0, url.indexOf("?") > -1 ? url.indexOf("?") : url.length);
                        leftlink = top.window.$(dom.replace(/\{url\}/, baseurl));
                        //能找到相对地址
                        if (leftlink.size() > 0) {
                            icon = typeof icon !== 'undefined' ? icon : leftlink.find("i").attr("class");
                            title = typeof title !== 'undefined' ? title : leftlink.find("span:first").text();
                            leftlink.trigger("fa.event.toggleitem");
                        }
                        var navnode = top.window.$(".nav-tabs ul li a[node-url='" + url + "']");
                        if (navnode.size() > 0) {
                            navnode.trigger("click");
                        } else {
                            //追加新的tab
                            var id = Math.floor(new Date().valueOf() * Math.random());
                            icon = typeof icon !== 'undefined' ? icon : 'fa fa-circle-o';
                            title = typeof title !== 'undefined' ? title : '';
                            top.window.$("<a />").append('<i class="' + icon + '"></i> <span>' + title + '</span>').prop("href", url).attr({
                                url: url,
                                addtabs: id
                            }).addClass("hide").appendTo(top.window.document.body).trigger("click");
                        }
                    }
                }
            },
            closetabs: function (url) {
                if (typeof url === 'undefined') {
                    top.window.$("ul.nav-addtabs li.active .close-tab").trigger("click");
                } else {
                    var dom = "a[url='{url}']"
                    var navlink = top.window.$(dom.replace(/\{url\}/, url));
                    if (navlink.size() === 0) {
                        url = Fast.api.fixurl(url);
                        navlink = top.window.$(dom.replace(/\{url\}/, url));
                        if (navlink.size() === 0) {
                        } else {
                            var baseurl = url.substr(0, url.indexOf("?") > -1 ? url.indexOf("?") : url.length);
                            navlink = top.window.$(dom.replace(/\{url\}/, baseurl));
                            //能找到相对地址
                            if (navlink.size() === 0) {
                                navlink = top.window.$(".nav-tabs ul li a[node-url='" + url + "']");
                            }
                        }
                    }
                    if (navlink.size() > 0 && navlink.attr('addtabs')) {
                        top.window.$("ul.nav-addtabs li#tab_" + navlink.attr('addtabs') + " .close-tab").trigger("click");
                    }
                }
            },
            replaceids: function (elem, url) {
                //如果有需要替换ids的
                if (url.indexOf("{ids}") > -1) {
                    var ids = 0;
                    var tableId = $(elem).data("table-id");
                    if (tableId && $("#" + tableId).size() > 0 && $("#" + tableId).data("bootstrap.table")) {
                        var Table = require("table");
                        ids = Table.api.selectedids($("#" + tableId)).join(",");
                    }
                    url = url.replace(/\{ids\}/g, ids);
                }
                return url;
            },
            refreshmenu: function () {
                top.window.$(".sidebar-menu").trigger("refresh");
            },
            gettablecolumnbutton: function (options) {
                if (typeof options.tableId !== 'undefined' && typeof options.fieldIndex !== 'undefined' && typeof options.buttonIndex !== 'undefined') {
                    var tableOptions = $("#" + options.tableId).bootstrapTable('getOptions');
                    if (tableOptions) {
                        var columnObj = null;
                        $.each(tableOptions.columns, function (i, columns) {
                            $.each(columns, function (j, column) {
                                if (typeof column.fieldIndex !== 'undefined' && column.fieldIndex === options.fieldIndex) {
                                    columnObj = column;
                                    return false;
                                }
                            });
                            if (columnObj) {
                                return false;
                            }
                        });
                        if (columnObj) {
                            return columnObj['buttons'][options.buttonIndex];
                        }
                    }
                }
                return null;
            },
        },
        init: function () {
            $.ajaxSetup ({
                cache: false //close AJAX cache
            });
            //公共代码
            //添加ios-fix兼容iOS下的iframe
            if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
                $("html").addClass("ios-fix");
            }
            $('form:not([autocomplete]),input:not([autocomplete]),textarea:not([autocomplete]),select:not([autocomplete])').attr('autocomplete', 'off');
            //配置Toastr的参数
            Toastr.options.positionClass = Config.controllername === 'index' ? "toast-top-right-index" : "toast-top-right";
            //点击包含.btn-dialog的元素时弹出dialog
            $(document).on('click', '.btn-dialog,.dialogit', function (e) {
                var that = this;
                var options = $.extend({}, $(that).data() || {});
                var url = Backend.api.replaceids(that, $(that).data("url") || $(that).attr('href'));
                var title = $(that).attr("title") || $(that).data("title") || $(that).data('original-title');
                var button = Backend.api.gettablecolumnbutton(options);
                if (button && typeof button.callback === 'function') {
                    options.callback = button.callback;
                }
                if (typeof options.confirm !== 'undefined') {
                    Layer.confirm(options.confirm, function (index) {
                        Backend.api.open(url, title, options);
                        Layer.close(index);
                    });
                } else {
                    window[$(that).data("window") || 'self'].Backend.api.open(url, title, options);
                }
                return false;
            });
            //点击包含.btn-addtabs的元素时新增选项卡
            $(document).on('click', '.btn-addtabs,.addtabsit', function (e) {
                var that = this;
                var options = $.extend({}, $(that).data() || {});
                var url = Backend.api.replaceids(that, $(that).data("url") || $(that).attr('href'));
                var title = $(that).attr("title") || $(that).data("title") || $(that).data('original-title');
                var icon = $(that).attr("icon") || $(that).data("icon");
                if (typeof options.confirm !== 'undefined') {
                    Layer.confirm(options.confirm, function (index) {
                        Backend.api.addtabs(url, title, icon);
                        Layer.close(index);
                    });
                } else {
                    Backend.api.addtabs(url, title, icon);
                }
                return false;
            });
            //点击包含.btn-ajax的元素时发送Ajax请求
            $(document).on('click', '.btn-ajax,.ajaxit', function (e) {
                var that = this;
                var options = $.extend({}, $(that).data() || {});
                if (typeof options.url === 'undefined' && $(that).attr("href")) {
                    options.url = $(that).attr("href");
                }
                options.url = Backend.api.replaceids(this, options.url);
                var success = typeof options.success === 'function' ? options.success : null;
                var error = typeof options.error === 'function' ? options.error : null;
                delete options.success;
                delete options.error;
                var button = Backend.api.gettablecolumnbutton(options);
                
                if (button) {
                    if (typeof button.success === 'function') {
                        success = button.success;
                    }
                    if (typeof button.error === 'function') {
                        error = button.error;
                    }
                }
                //如果未设备成功的回调,设定了自动刷新的情况下自动进行刷新
                if (!success && typeof options.tableId !== 'undefined' && typeof options.refresh !== 'undefined' && options.refresh) {
                    success = function () {
                        $("#" + options.tableId).bootstrapTable('refresh');
                    }
                }
                if (typeof options.confirm !== 'undefined') {
                    Layer.confirm(options.confirm, function (index) {
                        Backend.api.ajax(options, success, error);
                        Layer.close(index);
                    });
                } else {
                    Backend.api.ajax(options, success, error);
                }
                return false;
            });
            $(document).on('click', '.btn-click,.clickit', function (e) {
                var that = this;
                var options = $.extend({}, $(that).data() || {});
                var row = {};
                if (typeof options.tableId !== 'undefined') {
                    var index = parseInt(options.rowIndex);
                    var data = $("#" + options.tableId).bootstrapTable('getData');
                    row = typeof data[index] !== 'undefined' ? data[index] : {};
                }
                var button = Backend.api.gettablecolumnbutton(options);
                var click = typeof button.click === 'function' ? button.click : $.noop;

                if (typeof options.confirm !== 'undefined') {
                    Layer.confirm(options.confirm, function (index) {
                        click.apply(that, [options, row, button]);
                        Layer.close(index);
                    });
                } else {
                    click.apply(that, [options, row, button]);
                }
                return false;
            });
            //修复含有fixed-footer类的body边距
            if ($(".fixed-footer").size() > 0) {
                $(document.body).css("padding-bottom", $(".fixed-footer").outerHeight());
            }
            //修复不在iframe时layer-footer隐藏的问题
            if ($(".layer-footer").size() > 0 && self === top) {
                $(".layer-footer").show();
            }
            //tooltip和popover
            if (!('ontouchstart' in document.documentElement)) {
                $('body').tooltip({selector: '[data-toggle="tooltip"]'});
            }
            $('body').popover({selector: '[data-toggle="popover"]'});


            $(document).on("keyup", ".init-table-head", function(event){
                // 判断当前操作是否在table内
                // input.selectionEnd = input.value.length;
                // console.log(document.activeElement.tagName.toLowerCase());
                var input = document.activeElement;
                if(!input) return false;
                var length = input.value.length;
                var now_length = input.selectionStart;
                
                var nextmove;
                //行 列
                var col = $(input).parents('tr').index();
                var row = $(input).parents("tr").find("td").index($(input).parents("td"));
                if(event.keyCode == 38){
                    if(col > 0){
                        $(this).find("tbody").find("tr").eq(col-1).find('td').eq(row).find('input').select();
                    }
                }
                if(event.keyCode == 40){
                    var all_col = $(input).parents('tbody').find("tr").length;
                    if(col < all_col){
                        $(this).find("tbody").find("tr").eq(col+1).find('td').eq(row).find('input').select();
                    }
                }
                if(event.keyCode == 37){
                    var real_row = row-1;

                    if(now_length <= 0){
                        
                        while  (real_row > 0){
                            var content = $(this).find("tbody").find("tr").eq(col).find('td').eq(real_row).find('input');
                            if($(content).attr('type') == 'text' && !$(content).parents("td").is(":hidden") && !$(content).attr("readonly")){
                                let nextmove = localStorage.getItem("nextleftmove"); 
                                if  (nextmove==2)
                                {
                                    $(this).find("tbody").find("tr").eq(col).find('td').eq(real_row).find('input').select();
                                    localStorage.setItem("nextleftmove",1);
                                }
                                else
                                {
                                    localStorage.setItem("nextleftmove",2);
                                }    
                                return false;
                            }else{
                                real_row--;
                            }
                        }
                    }
                }
                if(event.keyCode == 39){
                    var real_row = row+1;
                    var all_row = $(input).parents('tr').find("td").length; 
                    if(now_length >= length){
                        while  (real_row <= all_row){
                      
                            var content = $(this).find("tbody").find("tr").eq(col).find('td').eq(real_row).find('input');
                            if($(content).attr('type') == 'text' && !$(content).parents("td").is(":hidden") && !$(content).attr("readonly")){
                                let   nextmove = localStorage.getItem("nextmove"); 
                                if  (nextmove==2)
                                {
                                    $(this).find("tbody").find("tr").eq(col).find('td').eq(real_row).find('input').select();
                                    localStorage.setItem("nextmove",1);
                                }
                                else
                                {
                                    localStorage.setItem("nextmove",2);
                                }  
                                return false;
                            }else{
                                real_row++;
                            }  

                        }
                    }
                    // console.log(now_length,length,nextmove);
                }
            });
            var table_list = $(".init-table-head");
            $.each(table_list,function(index,e){
                var id = $(e).attr("id");
                readwidth(id);
            }); 
            function readwidth(id)
            {
                var rwidth = atob(localStorage.getItem(window.location.pathname+id)); 
                var splitswidth=rwidth.split(','); 
                if(splitswidth.length > 1){
                    var rtable = document.getElementById(id); 
                
                    for (j = 0; j < rtable.rows[0].cells.length; j++) {
                        rtable.rows[0].cells[j].width = splitswidth[j] ;
                    } 
                }
                

            }
            var table;
            $(".init-table-head th").mouseover(function (e) {
                var table_id = $(this).parents("table").attr("id");
                var table = document.getElementById(table_id); 
                // console.log($(this).parents("table").attr("id"));return false;
                if (($(this).find("div").length <= 0)) {
                    $(this).append("<div class='th-sisehandler'></div>")
                    $(".th-sisehandler").mousedown(function (evt) {
                        let dragTh = $(this).parent()
                        let oldClientX = evt.clientX;
                        let oldWidth = dragTh.width();
                        let changeSizeLayer = $('<div class="siselayer"></div>');
                        $("body").append(changeSizeLayer);
                        var table;
                        changeSizeLayer.on('mousemove', function (evt) {
                            var newWidth =evt.clientX - oldClientX + oldWidth;
                            dragTh.attr('width',Math.max(newWidth,1));
                        });
        
                        changeSizeLayer.on('mouseup', function (evt) {
                            savewidth(table_id);
                            // var newWidth =evt.clientX - oldClientX + oldWidth;
                            changeSizeLayer.remove();
                            dragTh.find('.th-sisehandler').remove();
                        });
                    })
                }
        
                $(this).mouseleave(function () {
                    $(this).find("div").remove()
                })

                function savewidth(id)
                {
                    var swidth="";   
                    var rtable = document.getElementById(id); 
                    swidth= table.rows[0].cells[0].width;  
                    for (j = 1; j < rtable.rows[0].cells.length; j++) { 
                        swidth=swidth+","+  table.rows[0].cells[j].width;  
                    }
                    localStorage.setItem(window.location.pathname+id,btoa(swidth));
                    
                }
            })
        },
        search: function (objId, inputId, color) {
            /* 方法说明
                此方法依赖于 jquery 开发的，必须先导入 jquery
                下拉框筛选时默认以第一个 option 的值 代表所有，即当选择第一个 option 时，返回所有数据，不进行搜索
           
              *@param{String} objId 需要被搜索内容表格的id或class
              *@param{String} inputId 搜索框的id或class || 下拉框的id或class
              *@param{String} color 搜索内容以什么颜色返回，不传默认为红色
             */
          
          
          
            // 表格搜索
            this.tableSearch = function () {
              $('#content-null').remove(); // 每次进入先移出掉上次搜索产生的tr
              this.objId.find('tr span').css({ // 每次搜索开始，先把所有字体颜色恢复初始状态
                'color': "black",
                'font-weight': 'normal'
              });
              
              var searchKey = this.objId.find("thead tr th.searchKey")[0].cellIndex+1;
              var tableTrTdContent = this.objId.find('tr td:nth-child('+searchKey+'):contains("' + this.inpIdContents + '")'); // 获取所有含有搜索内容的td，类似于集合存储       
              if (this.inpIdContents != '') { // 如果搜索内容为空，就不用去更改样式，直接还原所有
          
                if (tableTrTdContent.length == 0) { // 判断集合长度是否为0，为0则表示搜索的内容在表格里不存在
          
          
                  this.objId.find('tr:not(:eq(0))').css({ // 先将所有tr隐藏
                    display: "none"
                  })
          
                  var tableColspanNumber = this.objId.find('tr').eq(0).find('th').length || this.objId.find('tr').eq(0).find('td').length; // 获取表头的列数 
                  var tr = $(`
              <tr id="content-null">
              <td colspan='${tableColspanNumber}' style="text-align: center;">暂无你搜索的内容</td>
              </tr>
              `); // 创建搜索不到时，显示的tr
                  this.objId.append(tr)
                } else if (tableTrTdContent.length > 0) { // 集合长度不为0，则表示搜索的内容在表格里
                  $('#content-null').remove();
          
                  this.objId.find('tr:not(:eq(0))').css({ // 先将所有tr隐藏
                    display: "none"
                  })
          
          
                  for (var a = 0; a < tableTrTdContent.length; a++) { // 遍历找到的td集合，进行每个渲染颜色
                    tableTrTdContent[a].parentNode.style.display = "table-row"; // 让含有搜索内容的 tr 进行显示
                    var contents = tableTrTdContent.eq(a).text(); // 获取到含有搜索内容的td里的集体内容，即字符串
                    var contentsArr = contents.split(this.inpIdContents); // 以搜索框中的内容将td的值进行分割成数组
                    var contentArrFirst = contentsArr[0]; // 将数组里的第一个值取出
                    for (var j = 1; j < contentsArr.length; j++) { // 将分割出来的内容进行染色后重新组合在一起
                      contentArrFirst += `<span style=';color:${this.color};font-weight:bolder'>` + this.inpIdContents + "</span>" + contentsArr[j];
                    };
                    tableTrTdContent.eq(a).html(contentArrFirst); // 将td里的值从新解析成html
                  }
          
          
                }
              } else {
                this.objId.find('tr:not(:eq(0))').css({
                  display: "table-row"
                });
          
                $('#content-null').remove();
              }
          
          
            }
          
            // ul 搜索
            this.ulSearch = function () {
              $('#content-null').remove(); // 每次进入先移出掉上次搜索产生的tr
              this.objId.find('li span').css({ // 每次搜索开始，先把所有字体颜色恢复初始状态
                'color': "black",
                'font-weight': 'normal'
              });
              var liContent = this.objId.find('li:contains("' + this.inpIdContents + '")'); // 获取所有含有搜索内容的td，类似于集合存储
              if (this.inpIdContents != '') { // 如果搜索内容为空，就不用去更改样式，直接还原所有
          
                if (liContent.length == 0) { // 判断集合长度是否为0，为0则表示搜索的内容在li里不存在
          
          
                  this.objId.find('li').css({ // 先将所有tr隐藏
                    display: "none"
                  })
          
                 
                  var tr = $(`
            
              <li id="content-null">暂无你搜索的内容</li>
            
              `); // 创建搜索不到时，显示的tr
                  this.objId.append(tr)
                } else if (liContent.length > 0) { // 集合长度不为0，则表示搜索的内容在表格里
                  // console.log("我在")
                  // console.log(liContent)
          
                  $('#content-null').remove();
          
                  this.objId.find('li').css({ // 先将所有tr隐藏
                    display: "none"
                  })
          
                  for (var a = 0; a < liContent.length; a++) { // 遍历找到的li集合，进行每个渲染颜色
                    // console.log(liContent[a].parentNode)
                    liContent[a].style.display = "block"; // 让含有搜索内容的 li 进行显示
                    var contents = liContent.eq(a).text(); // 获取到含有搜索内容的li里的集体内容，即字符串
                    var contentsArr = contents.split(this.inpIdContents); // 以搜索框中的内容将td的值进行分割成数组
                    var contentArrFirst = contentsArr[0]; // 将数组里的第一个值取出
                    for (var j = 1; j < contentsArr.length; j++) { // 将分割出来的内容进行染色后重新组合在一起
                      contentArrFirst += `<span style=';color:${this.color};font-weight:bolder'>` + this.inpIdContents + "</span>" + contentsArr[j];
                    };
                    liContent.eq(a).html(contentArrFirst); // 将td里的值从新解析成html
                  }
          
          
                }
              } else {
                this.objId.find('li').css({
                  display: "block"
                });
          
                $('#content-null').remove();
              }
            }
            // 初始化，判断需要搜索标签的类型
            this.init = function () {
              this.color = color || 'red';
              if (typeof $ == "undefined") { // 判断是否引入 jquery
                throw new Error("该搜索功能依赖于jquery插件，需要引入jquery");
              }
          
          
              if (typeof objId[0] == "undefined") { // 判断是通过jquery获取的id还是原生获取的id,需要把原生的转换成jquery
                this.objId = $(objId); // 需要搜索的对象的id       
          
              } else {
                this.objId = objId; // 需要搜索的对象的id          
          
              }
          
              if (typeof inputId[0] == "undefined") { // 判断搜索框获取的方式，转换成jquery获取
                var inp = $(inputId);
              } else {
                var inp = inputId;
              }
          
          
              if (inp[0].tagName == "SELECT") { // 如果是以下拉框来筛选,则下拉框第一个选项为显示所有信息，即不搜索
                if (inp.val().trim() == inp.find('option:first').val()) {
                  this.inpIdContents = '' // 搜索的内容为空
                } else {
                  this.inpIdContents = inp.val().trim() // 获取搜索框里的值,去除首尾空格
          
                }
          
              } else {
                this.inpIdContents = inp.val().trim() // 获取搜索框里的值,去除首尾空格
          
              }
              // console.log(inp[0].tagName)
              // console.log(this.inpIdContents)
              // console.log(typeof inp)
              this.objType = this.objId[0].tagName; // 获取需要被搜索对象的标签类型,将jquey转化为原生js获取标签类型
              // console.log(this.objId.find("tr th").length)
              // if (this.objId.find("tr th").length == 0) {// 判断表格是否有表头,没有就创建错误信息
              //   throw new Error("你要搜索的表格没有表头 <th> 标签，请规范好表格");
              // }
          
              switch (this.objType) { // 判断搜索对象
                case 'TABLE':
                  this.tableSearch();
                  break; // 对象是表格，进行表格搜索
                case 'UL':
                  this.ulSearch();
                  break; // 对象是ul，进行ul搜索
              }
            }
            this.init()
        }
    };
    Backend.api = $.extend(Fast.api, Backend.api);
    //将Template渲染至全局,以便于在子框架中调用
    window.Template = Template;
    //将Moment渲染至全局,以便于在子框架中调用
    window.Moment = Moment;
    //将Backend渲染至全局,以便于在子框架中调用
    window.Backend = Backend;

    Backend.init();
    return Backend;
});